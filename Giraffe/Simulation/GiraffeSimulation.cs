﻿// this source file contains the business logic for GiraffeSimulation; use this class to
// perform all simulations -- do not use the SimulationEngine class directly.

using System;
using System.ComponentModel;
using System.Text;
using System.IO;
using System.Threading;

namespace Giraffe
{
    /// <summary>
    /// Use this class to perform all simulations.
    /// </summary>
    public class GiraffeSimulation : ISimulationEvents
    {
        // just a rough estimate
        const int LoopPerSimulation = 3000;

        /// <summary>
        /// pass thru for simulation errors
        /// </summary>
        public event ErrorEventDel ErrorEvent;

        /// <summary>
        /// pass thru for file repairing events
        /// </summary>
        public event RepairingFileDel RepairingFile;

        /// <summary>
        /// pass thru for time step completions
        /// </summary>
        public event ProgressDel Progress;

        /// <summary>
        /// pass thru for simulation completions
        /// </summary>
        public event SimulationCompleteDel SimulationComplete;

        /// <summary>
        /// pass thru for removing negative pressures
        /// </summary>
        public event RemovingNegativePressureDel RemovingNegativePressure;

        /// <summary>
        /// pass thru for simulation continuations
        /// </summary>
        public event ResumingSimulationDel ResumingSimulation;

        /// <summary>
        /// pass thru for simulation starts
        /// </summary>
        public event SimulationStartedDel SimulationStarted;

        /// <summary>
        /// the simulation handle, regardless of category
        /// </summary>
        public SimulationEngine sim { get; set; }

        private ParameterDefines ParamDefines { get; set; }

        public SimulationParameters SimulationData { get; set; }

        /// <summary>
        /// if true -- the simulation failed
        /// </summary>
        public bool Failed { get; set; }

        /// <summary>
        /// goes with the Cancelled property
        /// </summary>
        private bool _cancelled = false;
        /// <summary>
        /// if true the simulation was cancelled
        /// </summary>
        public bool Cancelled 
        { 
            get { return _cancelled; }

            set
            {
                if (simulator != null)
                    simulator.StopSimulation = true;
                _cancelled = value;
            }
        }

        /// <summary>
        /// use these private members to avoid parameter passing
        /// and maintain the original flow to reason
        /// </summary>
        private SimulationEngine simulator;
        private bool convergenceMet;
        private GiraffeEventArgs args = new GiraffeEventArgs();
        // these properties apply only to MonteCarlo Flexible
        // better to consolidate -- place them here            
        private int minimumForConvergence;
        private ConvergeCriteria myOverall0;
        private ConvergeCriteria myOverall1;
        private int simulationCount;
        private int numberOfSimulations;
        private SimulationType simulationType;

        // depending on the simulation type -- these object which 
        // hold the serviceability at time 0 and time 24 need to 
        // be used across all simulations
        private Serviceabilities _Serviceabilities0 { get; set; }
        private Serviceabilities _Serviceabilities1 { get; set; }

        private BackgroundWorker Worker { get; set; }
        private Timer _workerCancelWatcher;

        public GiraffeSimulation(ParameterDefines parameterDefines, SimulationParameters simulationParameters)
        {
            ParamDefines = parameterDefines;
            SimulationData = simulationParameters;

            Failed = false;
            Cancelled = false;
            _Serviceabilities0 = new Serviceabilities();
            _Serviceabilities1 = new Serviceabilities();
            simulator = null;
            convergenceMet = false;
            args = new GiraffeEventArgs();    
            // use the hard coded value of the original (10)
            // as the default value
            minimumForConvergence = 10;
            myOverall0 = null;
            myOverall1 = null;

            // Create the file watcher timer
            _workerCancelWatcher = new Timer(WatcherCallback, null, 1000, Timeout.Infinite);
        }

        /// <summary>
        /// Used to handle the "worker cancelled" timer callback.
        /// </summary>
        /// <param name="state"></param>
        private void WatcherCallback(Object state)
        {
            if (Worker != null)
            {
                // If the BackgroundWorker has been cancelled...
                if (Worker.CancellationPending)
                {
                    // ...cancel the simulation algorithm
                    Cancelled = true;
                }
            }

            // If we're not in a cancelled state...
            if (!Cancelled)
            {
                // ... restart the watcher timer
                _workerCancelWatcher.Change(1000, Timeout.Infinite);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="worker"></param>
        /// <param name="e"></param>
        public void ExecuteSimulation(System.ComponentModel.BackgroundWorker worker,
            System.ComponentModel.DoWorkEventArgs e)
        {
            // Expose the worker internally for event handlers
            Worker = worker;

            args.Context = "";

            // show some color -- relax the user
            Worker.ReportProgress(1);

            try
            {    
                string convergenceInfo = string.Empty;                            

                // Create the full output path ONCE (because timestamp must be captured once only)
                SimulationData.FullOutputPath = GetOutputDirectoryFromSettings(SimulationData.OutputPath, SimulationData.ProjectName, SimulationData.Timestamp, SimulationData.Category);
                
                do // assume we want to do at least one simulation
                {
                    simulationCount++;

                    // we re-instantiate to ensure proper simulation intialization
                    simulator = new SimulationEngine(ParamDefines, SimulationData, simulationCount);
                    numberOfSimulations = SimulationData.NumberOfSimulations;
                    simulationType = SimulationData.Category;

                    // span the serviceability updates across simulations
                    simulator._Serviceabilities0 = _Serviceabilities0;
                    simulator._Serviceabilities1 = _Serviceabilities1;

                    if ((simulator.SimulationData.Category == SimulationType.MonteFlexible))
                    {
                        if (minimumForConvergence == 10)
                            minimumForConvergence = simulator.SimulationData.MinimumRequiredForConvergence;

                        simulator.SimulationData.NumberOfSimulations = Convert.ToInt32(simulator.ParamDefines.Default("MaxNSimu"));
                    }

                    // wire up for events
                    simulator.SimulationComplete += _SimulationComplete;
                    simulator.ErrorEvent += _ErrorEvent;
                    simulator.RemovingNegativePressure += _RemovingNegativePressure;
                    simulator.RepairingFile += _RepairingFile;
                    simulator.ResumingSimulation += _ResumingSimulation;
                    simulator.SimulationStarted += _SimulationStarted;
                    simulator.Progress += simulator_Progress;

                    // Verify and persist full output path for re-use
                    SimulationData.FullOutputPath = Utilities.VerifyDirectory(SimulationData.FullOutputPath);


                    // rock and roll
                    simulator.ExecuteSimulation();

                    // if we are Monte Carlo flexible, check to see if we are done
                    if (simulator.SimulationData.Category == SimulationType.MonteFlexible)
                    {
                        // if we are done -- the "complete" status message is returned
                        convergenceInfo = CheckForConvergence(simulationCount);
                    }
                
                } while ((!convergenceMet) && ((simulationCount < simulator.SimulationData.NumberOfSimulations) && 
                    (!Failed) && (!Cancelled)));

                if ((simulator.SimulationSucceeded == SuccessCategories.Success) && (!Cancelled))
                {
                    // we will output the resuls in a better fashion -- as well we do not clear
                    // the information in this next call                        
                    string filesWritten = simulator.WriteServiceCapability(simulationCount);

                    // return the names of the files that hold the simulation data, each file (2) seperated
                    // by a semicolon ';'
                    args.Context = filesWritten;

                    if (convergenceMet && (convergenceInfo.Length != 0))
                    {
                        // put the convergence completion information in the
                        // last servieability result file
                        string[] lastResult = filesWritten.Split(';');
                        if (lastResult.Length == 2)
                        {
                            // include the convergence info in the results file
                            StringBuilder allIncluded = new StringBuilder();
                            allIncluded.AppendFormat("{0}{2}{2}{1}", convergenceInfo,
                                Convert.ToString(File.ReadAllText(lastResult[1].Trim())), Environment.NewLine);
                            File.Move(lastResult[1].Trim(), lastResult[1].Trim() + "orig");
                            File.WriteAllText(lastResult[1].Trim(), allIncluded.ToString().Trim());
                        }
                    }
                }
                else if (simulationCount >= SimulationData.NumberOfSimulations)
                    // we did not reach convergence
                    Failed = true;
            }
            catch (SystemException sysex)
            {
                Failed = true;                
                args.Context = "ConvergenceSimulation::ExecuteSimulation failed!";
                args.SysEx = sysex;
                Console.WriteLine(sysex.Message);
            }                       
                      
            var temp = SuccessCategories.Success;               

            if (Failed)
            {
                temp = SuccessCategories.Failure;
                // Stuff the exception into the worker result
                e.Result = args.SysEx;
            }
            if (Cancelled)
            {
                temp = SuccessCategories.Cancelled;
                // Mark the worker as cancelled
                e.Cancel = true;
            }
          
            // report done, regardless of success
            _SimulationComplete(temp, args);
        }

        
       

        /// <summary>
        /// follow original output directory creation format
        /// </summary>
        /// <returns></returns>
        public string GetOutputDirectoryFromSettings(string outputPath, string projectName, bool timestamp, SimulationType simType)
        {
            string extFolder;

            switch (simType)
            {
                case SimulationType.Deterministic:
                    extFolder = "det";
                    break;

                case SimulationType.MonteFixed:
                    extFolder = "fix";
                    break;

                case SimulationType.MonteFlexible:
                    extFolder = "flex";
                    break;

                default:
                    extFolder = "unknown";
                    break;
            }

            StringBuilder projectBuilder = new StringBuilder();
            projectBuilder.Append(projectName.Trim());

            if (timestamp)
            {
                projectBuilder.Append("_");
                projectBuilder.Append(DateTime.Now.ToString("yyyyMMddHHmmss"));
            }

            // create the output directory for this simulation run (per loop)
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("{0}\\{1}\\{2}\\", outputPath, projectBuilder, extFolder);

            return builder.ToString();
        }

        /// <summary>
        /// cpoied directly from original code -- maintain original flow
        /// </summary>
        /// <param name="simulationCount"></param>
        public string CheckForConvergence(int simulationCount)
        {
            string returnValue = string.Empty;

            if (!Failed)
            {
                if (simulationCount == minimumForConvergence)
                {                    
                    if (minimumForConvergence == simulator.SimulationData.MinimumRequiredForConvergence)
                    {
                        myOverall0 = simulator.CalculateOverall(simulationCount, simulator.SimulationData.NumberOfHours);
                        convergenceMet = false;
                    }
                    else
                    {
                        myOverall1 = simulator.CalculateOverall(simulationCount, simulator.SimulationData.NumberOfHours);

                        if ((Math.Abs(myOverall1.Mean - myOverall0.Mean) <= Convert.ToDouble(simulator.ParamDefines.Default("MeanFlexible")))
                            && (Math.Abs(myOverall1.COV - myOverall0.COV) <= Convert.ToDouble(simulator.ParamDefines.Default("COVFlexible"))))
                        {
                            StringBuilder toWrite = new StringBuilder();
                            toWrite.AppendFormat("{0}{13}{1}{13}{2}{13}{3}{13}{4}{5}{13}{6}{7}{13}{8}{13}{9}{10}{13}{11}{12}",
                                "Successful Run!", "Number of Simulation:", simulationCount, "Convergence Criteria", "Mean: ",
                                String.Format(simulator.ParamDefines.Default("MeanFlexible"), "0000.00000"),
                                "COV: ", String.Format(simulator.ParamDefines.Default("COVFlexible"), "0000.00000"), "Results",
                                "Mean: ", Math.Abs(myOverall1.Mean - myOverall0.Mean), "COV: ", Math.Abs(myOverall1.COV - myOverall0.COV), Environment.NewLine);

                            returnValue = toWrite.ToString().Trim();

                            convergenceMet = true;
                        }
                        else
                        {
                            myOverall0 = myOverall1;
                            myOverall1 = new ConvergeCriteria();
                            convergenceMet = false;
                        }

                    } // we are not doing the first check 

                    // original had 10 as the base, += 5 hard coded -- just use half the minimum as 
                    // the incrementor
                    minimumForConvergence += (int)(minimumForConvergence / 2);

                }  // minimum for convergence                

            } // end if not failed

            return returnValue;
        }
        
        #region pass thru events

        void simulator_Progress(int loops, int numberCompleted, int maximum)
        {
            if (Progress != null)
                Progress.BeginInvoke(loops, numberCompleted, maximum, null, null);

            if (SimulationData.NumberOfSimulations == 1)
            {
                float percentComplete = ((float)loops / (float)(numberOfSimulations * LoopPerSimulation)) * 100;
                // do not allow this method to show a percentge greater than 85%
                Worker.ReportProgress((int)Math.Min(85, percentComplete));                
            }
            else
            {
                float percentComplete = (((float)simulationCount - 1) / (float)(SimulationData.NumberOfSimulations)) * 100;
                Worker.ReportProgress((int)percentComplete);
            }

        }

        void _LoopsComplete(int loops)
        {
            
        }        

        void _SimulationStarted(int hours, int steps)
        {
            if (SimulationStarted != null)
                SimulationStarted.BeginInvoke(hours, steps, null, null);
            Worker.ReportProgress(0, "Simulation Started!");    // NEW
        }

        void _ResumingSimulation(int loopCount)
        {
            if (ResumingSimulation != null)
                ResumingSimulation.BeginInvoke(loopCount, null, null);
            Worker.ReportProgress(-1, "Resuming Simulation...");   // NEW
        }

        void _RepairingFile(GiraffeEventArgs args)
        {
            if (RepairingFile != null)
                RepairingFile.BeginInvoke(args, null, null);
            Worker.ReportProgress(-1, args.Context);  // NEW
        }

        void _RemovingNegativePressure(int callCount)
        {
            if (RemovingNegativePressure != null)
                RemovingNegativePressure.BeginInvoke(callCount, null, null);
            Worker.ReportProgress(-1, "Removing Negative Pressure...");    // NEW
        }

        void _ErrorEvent(GiraffeEventArgs args)
        {
            if (ErrorEvent != null)
                ErrorEvent.BeginInvoke(args, null, null);
            Worker.ReportProgress(-1, "ERROR: " + args.SysEx.Message);
        }

        void _SimulationComplete(SuccessCategories successful, GiraffeEventArgs args)
        {
            string message = "Simulation Successful!";

            if (successful == SuccessCategories.Failure)
            {
                Failed = true;
                message = "Simulation Failed!";
            }
            else if (successful == SuccessCategories.Cancelled)
            {
                Cancelled = true;
                message = "Simulation Cancelled!";
            }
            else
            {
                // we were successful -- but are we done?
                if (simulationType == SimulationType.Deterministic)
                    // we are one and done
                    Worker.ReportProgress(100, message);
                else if ((simulationType == SimulationType.MonteFixed) && (simulationCount >= SimulationData.NumberOfSimulations))
                    // we did all that we needed to
                    Worker.ReportProgress(100, message);
                else if (simulationType == SimulationType.MonteFixed)
                    Worker.ReportProgress(simulationCount);
                
            }
            if (SimulationComplete != null)           
                SimulationComplete.BeginInvoke(successful, args, null, null);
            Worker.ReportProgress(100, message);
        }

        #endregion
    }
}