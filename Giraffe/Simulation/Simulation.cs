﻿// This file contains the business logic of the GIRAFFE simulator. The code is based upon CLI/C++ delivered to Elwyn & Palmer
// and then delivered to the software development team. It is important to note that while many of the subroutines appear as though they
// could use some refactoring - there are many nunances and subtleties that make this approach not wise. During the re-write, there were
// times when efficiencies were added, routine lenghts decreased and the system would "break." Unless the developer understands all of
// aspects of the code they are addressing, do not deviate in the logic.

using System;
using System.IO;
using System.Collections.Specialized;
using System.Collections;
using System.Linq;
using System.Text;
using Epanet;

namespace Giraffe
{
    /// <summary>
    /// this is the object to be used to perform simulations; based upon 
    /// 5.4's Simulation::RunSimulation()
    /// </summary>
    [Serializable]
    public class SimulationEngine : ISimulationEvents
    {
        public const string TITLE = @"TITLE";
        public const string END = @"END";
        private Random newRand;

        public int Type { get; set; }

        #region events

        /// <summary>
        /// supports the events that signals an error has occurred - inspect the args
        /// for details, especially the system exception information
        /// </summary>
        /// <param name="args"></param>
        public event ErrorEventDel ErrorEvent;

        /// <summary>
        /// supports reporting that we are repairing the input file to EPANet
        /// </summary>
        /// <param name="args"></param>
        public event RepairingFileDel RepairingFile;

        /// <summary>
        /// The noted time step has completed
        /// </summary>
        /// <param name="args"></param>
        public event ProgressDel Progress;

        /// <summary>
        /// the simulation has ended -- success or failure indicated
        /// use this style implementation to suppress warnings -- change
        /// if this event is ever used
        /// </summary>
        /// <param name="successful"></param>
        /// <param name="args"></param>
        public event SimulationCompleteDel SimulationComplete
        {
            add { }
            remove { }
        }

        /// <summary>
        /// negative pressures are being eliminated
        /// </summary>
        /// <param name="callCount">an indicator on the number of calls to EPANet so far</param>
        public event RemovingNegativePressureDel RemovingNegativePressure;

        /// <summary>
        /// We are continuing on "normal" simulation looping
        /// </summary>
        public event ResumingSimulationDel ResumingSimulation;

        /// <summary>
        /// Simulation has begun
        /// </summary>
        /// <param name="hours">the number of hours to simulate</param>
        /// <param name="steps">the number of steps per hour</param>
        public event SimulationStartedDel SimulationStarted;

        #endregion

        #region privates from original
        /// <summary>
        /// this section wil hold all the global variables from Giraffe 08;
        /// refactor if time permits
        /// </summary>
        private string fileOutputRpt;

        private string outputPathFull;
        #endregion

        private ConvergeCriteria myOverall0 = new ConvergeCriteria();
        private ConvergeCriteria myOverall1 = new ConvergeCriteria();

        /// <summary>
        /// create, hold the singleton
        /// </summary>
        private EpaPort epa = EpaPort.GetEpaInstance();

        /// <summary>
        /// use this counter when we are executing more than one simulation
        /// like in the cases of monte carlo fixed
        /// </summary>
        private int simulationCounter = 0;

        /// <summary>
        /// This is represented by the variable Base in the old system, the 
        /// minimum number of simulations before checking convergence criteria
        /// </summary>
        private int MinimumNumberForConvergenceCheck { get; set; }

        /// <summary>
        /// setting this will cause a "factory-like" determination and set the
        /// proper private members
        /// </summary>
        public SimulationParameters SimulationData { get; set; }

        /// <summary>
        /// the system parameters in which to use -- use default if not specified
        /// </summary>
        public ParameterDefines ParamDefines = new ParameterDefines();

        #region EPANET Components
      
        public Demands _Demands { get; set; }
        public Demands _OriginalDemands { get; set; }
       
        public Tanks _Tanks { get; set; }
        
        public Pipes _Pipes { get; set; }
       
        public Junctions _Junctions { get; set; }
       
        public Reservoirs _Reservoirs { get; set; }
        
        public Pumps _Pumps { get; set; }
      
        public Valves _Valves { get; set; }
       
        public Curves _Curves { get; set; }
       
        public Patterns _Patterns { get; set; }
      
        public Statuses _Statuses { get; set; }
      
        public Controls _Controls { get; set; }
       
        public Sources _Sources { get; set; }
      
        public Qualities _Qualities { get; set; }
        
        public Reactions _Reactions { get; set; }
        
        public ReportOptions _ReportOptions { get; set; }
      
        public Options _Options { get; set; }
       
        public Coordinates _Coordinates { get; set; }
     
        public Vertices _Vertices { get; set; }

        #endregion
       
        public RepairRates _RepairRates { get; set; }
       
        public NodePressures _NodePressures { get; set; }
        
        public LeakRatios _LeakRatios { get; set; }
       
        public PipeBreaks _PipeBreaks { get; set; }
        
        public PipeLeaks _PipeLeaks { get; set; }

        public Serviceabilities _Serviceabilities0 { get; set; }
        public Serviceabilities _Serviceabilities1 { get; set; }
       
        public SumDemands _SumDemands { get; set; }

        /// <summary>
        /// if true -- simulation will stop
        /// </summary>
        public bool StopSimulation { get; set; }

        /// <summary>
        /// did we work?
        /// </summary>
        public SuccessCategories SimulationSucceeded { get; set; }

        /// <summary>
        /// c'tor
        /// </summary>
        public SimulationEngine()
        {
            Init();
        }

        /// <summary>
        /// c'tor
        /// </summary>
        /// <param name="parameterDefines">parameters used to define and run the simulation</param>
        /// <param name="simulationParameters">specifics for the actual simulation</param>
        /// <param name="simulationNumber">X of N simulations to be performed</param>
        public SimulationEngine(ParameterDefines parameterDefines, SimulationParameters simulationParameters, int simulationNumber = 1)
        {
            // Persist the simulation data for re-use within this class
            SimulationData = simulationParameters;

            StopSimulation = false;
            MinimumNumberForConvergenceCheck = 10;

            // we need this for correct directory creation and file storage
            simulationCounter = simulationNumber;

            // Assign the ParameterDefines
            ParamDefines = parameterDefines;

            Init();
            
            // init any collection needing it
            InitCollections();

            // load all files, create all needed data structures            
            CreateNodeCollections(simulationParameters.SystemFile);

            // load the repair rate file
            if (simulationParameters.Category != SimulationType.Deterministic)
                LoadRepairRates(simulationParameters.PipeRepairRateFile);

            // initialize with the chosen random seed, per original code
            newRand = new Random(simulationParameters.RandomSeed + (12 * simulationCounter));            

            // if we are calibrating
            if (simulationParameters.CalibrateNodalDemand)
            {
                LoadPressureRates(simulationParameters.MeanPressureFile);
                CalibrateDemand(simulationParameters.RegressionEquation);
            }

            if (SimulationData.Category == SimulationType.Deterministic)
                ReadPipeDamageFile();
            else            
                CreateLeakDamage();

            AddPipeDamage(SimulationData.FullOutputPath);
        }
        
        /// <summary>
        /// instantiate all required structures
        /// </summary>
        public void Init()
        {
            _Junctions = new Junctions();
            _Reservoirs = new Reservoirs();
            _Pipes = new Pipes();
            _Controls = new Controls();
            _Curves = new Curves();
            _Demands = new Demands();
            _OriginalDemands = new Demands();
            _Patterns = new Patterns();
            _Pumps = new Pumps();
            _Statuses = new Statuses();
            _Tanks = new Tanks();
            _Valves = new Valves();
            _Sources = new Sources();
            _Qualities = new Qualities();
            _Reactions = new Reactions();
            _Options = new Options();
            _ReportOptions = new ReportOptions();
            _Coordinates = new Coordinates();
            _Vertices = new Vertices();
            _RepairRates = new RepairRates();
            _NodePressures = new NodePressures();
            _LeakRatios = new LeakRatios();
            _PipeBreaks = new PipeBreaks();
            _PipeLeaks = new PipeLeaks();
            _Reactions = new Reactions();
            //_Serviceabilities0 = new Serviceabilities();
            //_Serviceabilities1 = new Serviceabilities();
            _SumDemands = new SumDemands();
        }

        /// <summary>
        /// this is it ... simulate
        /// </summary>
        public void ExecuteSimulation()
        {            
            int whileCount1 = 0;
            int repairCalls = 0;
            SimulationSucceeded = SuccessCategories.Success;
            GiraffeEventArgs overallArgs = new GiraffeEventArgs();
            overallArgs.Context = "Success";

            if (SimulationStarted != null)
                SimulationStarted.BeginInvoke(this.SimulationData.NumberOfHours, this.SimulationData.NumberOfStepsPerHour, null, null);

            StringBuilder builder = new StringBuilder();
            string lastCreatedEpaNetFile = string.Empty;

            // Get persisted full output path from SimulationData
            outputPathFull = SimulationData.FullOutputPath;
            outputPathFull += simulationCounter.ToString().Trim() + "\\";
            outputPathFull = Utilities.VerifyDirectory(outputPathFull);

            // there was an "ignore epa error" call here that did nothing

            for (int count = 0; count <= SimulationData.NumberOfHours; count += SimulationData.NumberOfStepsPerHour)
            {
                if (StopSimulation) break;

                try
                {
                    if (Progress != null)
                        Progress.BeginInvoke(whileCount1, simulationCounter, SimulationData.NumberOfSimulations, null, null);

                    // if not the first loop; this section below is the heart of the simulation
                    // it sets up and causes the hydraulic analysis
                    if (count > 1)
                    {
                        OpenEpanet(lastCreatedEpaNetFile);
                        GenerateReport();
                        /*
                        * change by Amanda 5_29_2007: I changed the below from SimulationTime to
                        * SimulationStep so calculations for volume in ;
                        * "UpdateTankLevel" increment by time step (e.g. 1 hr, 6hrs, etc...), not the
                        * entire simulation time (e.g. 24hrs). ;
                        * UpdateTankLevel(xValues->SimulationTime);
                        */
                        UpdateTankLevel(SimulationData.NumberOfStepsPerHour);
                        /* end of change by Amanda */

                        // close the epaNet handle
                        epa.Close();
                    }

                    // write out the modified input file to use for the hyfraulic simulation
                    builder.Clear();
                    builder.AppendFormat("{0}{1}", outputPathFull, "ModifiedOutput.inp");
                    lastCreatedEpaNetFile = builder.ToString().Trim();
                    File.WriteAllText(lastCreatedEpaNetFile, this.NewSystemDefinitionFileContent, ASCIIEncoding.ASCII);

                    // statusMsgsQ.Enqueue("Printing System Damage Files");
                    builder.Clear();
                    builder.AppendFormat("{0}{1}{2}{3}{4}", outputPathFull, "Damage_System_Time", count, _Pipes.PipeFilesPrinted, ".inp");
                    File.Copy(lastCreatedEpaNetFile, builder.ToString().Trim(), true);

                    bool done = false;                    
                    while (!done)
                    {
                        if (StopSimulation) break;

                        if (Progress != null)
                            Progress.BeginInvoke(whileCount1, simulationCounter, SimulationData.NumberOfSimulations, null, null);

                        bool negativePressures = true;
                        bool repair = false;
                        while ((negativePressures) && (!repair))
                        {

                            whileCount1++;

                            if (whileCount1 == 114)
                                whileCount1 = 114;
                      
                            epa.Close();

                            // write out the data object data
                            File.WriteAllText(lastCreatedEpaNetFile, this.NewSystemDefinitionFileContent, ASCIIEncoding.ASCII);
                            if (this.SimulationData.Debug)
                                File.Copy(lastCreatedEpaNetFile, lastCreatedEpaNetFile + whileCount1.ToString(), true);

                            if (ResumingSimulation != null)
                                ResumingSimulation.BeginInvoke(whileCount1, null, null);

                            if (OpenEpanet(lastCreatedEpaNetFile))
                            {
                                if (GenerateReport())
                                {
                                    if (RemovingNegativePressure != null)
                                        RemovingNegativePressure.BeginInvoke(whileCount1, null, null);

                                    // was FindNegativePressures
                                    if (NegativePressuresEliminated(this.SimulationData.MinimumPressureToEliminate))
                                    {
                                        //builder.AppendFormat("{0}{1}{2}{3}{4}", outputPathFull, "Modified_System_Time", count, _Pipes.PipeFilesPrinted, ".inp");
                                        string mod = outputPathFull + "Modified_System_Time" + count.ToString() + _Pipes.PipeFilesPrinted.ToString() + ".inp";
                                        File.Copy(lastCreatedEpaNetFile, mod, true);

                                        // write out some stats 
                                        WritePostProcessing(count);
                                        File.WriteAllText(builder.ToString().Trim(), this.NewSystemDefinitionFileContent, ASCIIEncoding.ASCII);

                                        if ((count == 0) || (count == this.SimulationData.NumberOfHours)) // make sure this is correct
                                        {
                                            CalculateServiceability(count);
                                        }

                                        epa.Close();

                                        File.WriteAllText(lastCreatedEpaNetFile, this.NewSystemDefinitionFileContent, ASCIIEncoding.ASCII);
                                        if (this.SimulationData.Debug)
                                            File.Copy(lastCreatedEpaNetFile, lastCreatedEpaNetFile + whileCount1.ToString() + "loopDone", true);

                                        negativePressures = false;
                                    }
                                    else
                                    {
                                        negativePressures = true;
                                    }
                                }
                                else
                                {
                                    repair = true;
                                }
                            }
                            else
                            {
                                repair = true;
                            }
                        }
                        
                        if (repair)
                        {
                            epa.Close();

                            ++repairCalls;

                            if (RepairingFile != null)
                            {
                                var rargs = new GiraffeEventArgs();
                                rargs.Context = "Repairing model file, loop number " + whileCount1.ToString().Trim();
                                RepairingFile.BeginInvoke(rargs, null, null);
                            }                            

                            if (!(RepairModel(fileOutputRpt)))
                            {
                                // original says we are in deadlock.                                                                
                                var issue = new SystemException("Unable to repair model " + fileOutputRpt);
                                throw issue;
                            }

                            if (SimulationData.Debug)
                                File.Copy(fileOutputRpt, fileOutputRpt + whileCount1.ToString().Trim());
                        }
                        else
                        {
                            done = true;
                        }
                    }                   
                }
                catch (SystemException sysex)
                {
                    SimulationSucceeded = SuccessCategories.Failure;

                    GiraffeEventArgs args = new GiraffeEventArgs();
                    args.Context = "Exception in ExecuteSimulation.";
                    args.SysEx = sysex;
                    overallArgs = args;
                    if (ErrorEvent != null)
                        ErrorEvent.BeginInvoke(args, null, null);
                }

                if (!StopSimulation)
                {
                    if (SimulationSucceeded == SuccessCategories.Success) 
                        // printing system damage files
                        File.WriteAllText(lastCreatedEpaNetFile, this.NewSystemDefinitionFileContent);
                }
            }            
            
        } /**/

        #region Methods

        /// <summary>
        /// load the various hash tables with their appropriate node sub-classes
        /// </summary>
        /// <param name="fileName"></param>
        public void CreateNodeCollections(string fileName)
        {
            bool titleFound = false;

            try
            {
                string fileBody = File.ReadAllText(fileName);

                string[] sections = fileBody.Split(Node.NODE_TYPE_SEPARATOR_1.ToCharArray());

                foreach (string section in sections)
                {
                    if (section != string.Empty)
                    {
                        string[] sectionAndBody = section.Split(Node.NODE_TYPE_SEPARATOR_2.ToCharArray());
                        if (sectionAndBody.Length > 1)
                        {
                            if (!titleFound)
                                if (sectionAndBody[0].ToString().ToUpper().Trim() == "TITLE")
                                    titleFound = true;
                            if (titleFound)
                                FillInCorrectHashTable(sectionAndBody[0], sectionAndBody[1]);
                            else
                                throw new SystemException("Invalid system file");
                        }
                    }
                }
            }
            catch (FileLoadException fex)
            {
                var temp = "Unable to load system file " + fileName + " " + fex.Message;
                throw new SystemException(temp);
            }
            catch (SystemException sysex)
            {
                Console.WriteLine(sysex.Message);
                var temp = "Unable to load system file " + fileName + " " + sysex.Message;
                throw new SystemException(temp);
            }
        }

        /// <summary>
        /// double Ratio Method that returns the difference of LeakRatio - LeakPreRatio
        /// lifted from Giraffe08
        /// </summary>
        /// <param name="LeakRatio"></param>
        /// <param name="LeakPreRatio"></param>
        /// <returns></returns>
        public float GetRatio(float leakRatio, float leakPreRatio)
        {
            float ratio = leakRatio - leakPreRatio;

            return GlobalRatio(ratio);
        }

        /// <summary>
        /// double Method that maintained
        /// certain minimum values necessary for adding the damage
        /// lifted from Giraffe08
        /// </summary>
        /// <param name="Ratio"></param>
        /// <returns></returns>
        public float GlobalRatio(float ratio)
        {
            if (ratio < 0.001)
            {
                return 0.001f;
            }
            else if (ratio > 0.999)
            {
                return 0.999f;
            }
            else
            {
                return ratio;
            }
        }

        /// <summary>
        /// if given a file to load - load the reapir rates which play into created
        /// pipe damage
        /// </summary>
        /// <param name="repairRateFile"></param>
        public void LoadRepairRates(string repairRateFile)
        {
            try
            {
                _RepairRates.LoadRepairRates(repairRateFile);
            }
            catch (SystemException sysex)
            {
                Console.WriteLine(sysex.Message);
                throw;
            }
        }

        /// <summary>
        /// if desired - load the pressures used to calibrate the nodes in
        /// the system
        /// </summary>
        /// <param name="pressureFile"></param>
        public void LoadPressureRates(string pressureFile)
        {
            try
            {
                _NodePressures.LoadNodePressures(pressureFile);
            }
            catch (SystemException sysex)
            {
                Console.WriteLine(sysex.Message);
                throw;
            }
        }

        /// <summary>
        /// fill in the hash table identified by the nodeTypeID
        /// </summary>
        /// <param name="nodeTypeID"></param>
        /// <param name="elements"></param>
        public void FillInCorrectHashTable(string nodeTypeID, string elements)
        {
            try
            {
                // since the results of any load are flexible, no <T> classes allowed
                // use this variable to hold the desired results
                OrderedDictionary temp = new OrderedDictionary();

                switch (nodeTypeID)
                {
                    case Junction.NODE_ID:
                        _Junctions.LoadJunctions(elements);
                        Console.WriteLine(_Junctions.Count.ToString() + " _Junctions");
                        break;

                    case Reservoir.NODE_ID:
                        _Reservoirs.LoadReservoirs(elements);
                        Console.WriteLine(_Reservoirs.Count.ToString() + " _Reservoirs");
                        break;

                    case Tank.NODE_ID:
                        _Tanks.LoadTanks(elements);
                        Console.WriteLine(_Tanks.Count.ToString() + " _Tanks");
                        break;

                    case Pipe.NODE_ID:
                        _Pipes.LoadPipes(elements);
                        Console.WriteLine(_Pipes.Count.ToString() + " _Pipes");
                        break;

                    case Pump.NODE_ID:
                        _Pumps.LoadPumps(elements);
                        Console.WriteLine(_Pumps.Count.ToString() + " _Pumps");
                        break;

                    case Valve.NODE_ID:
                        _Valves.LoadValves(elements);
                        Console.WriteLine(_Valves.Count.ToString() + " _Valves");
                        break;

                    case Demand.NODE_ID:
                        _Demands.LoadDemands(elements);
                        _OriginalDemands.LoadDemands(elements);
                        Console.WriteLine(_Demands.Count.ToString() + " _Demands");
                        break;

                    case Curve.NODE_ID:
                        _Curves.LoadCurves(elements);
                        Console.WriteLine(_Curves.Count.ToString() + " _Curves");
                        break;

                    case Pattern.NODE_ID:
                        _Patterns.LoadPatterns(elements);
                        Console.WriteLine(_Patterns.Count.ToString() + " _Patterns");
                        break;

                    case Status.NODE_ID:
                        _Statuses.LoadStatuses(elements);
                        Console.WriteLine(_Statuses.Count.ToString() + " _Statuses");
                        break;

                    case Control.NODE_ID:
                        _Controls.LoadControls(_Pipes, _Valves, elements);
                        Console.WriteLine(_Controls.Count.ToString() + " _Controls");
                        break;

                    case Reaction.NODE_ID:
                        _Reactions.LoadReactions(elements);
                        Console.WriteLine(_Reactions.Count.ToString() + " _Reactions");
                        break;

                    case Option.NODE_ID:
                        _Options.LoadOptions(elements);
                        Console.WriteLine(_Options.Count.ToString() + " _Options");
                        break;

                    case ReportOption.NODE_ID:
                        _ReportOptions.LoadReportOptions(elements);
                        Console.WriteLine(_ReportOptions.Count.ToString() + " _ReportOptions");
                        break;

                    case Coordinate.NODE_ID:
                        _Coordinates.LoadCoordinates(elements);
                        Console.WriteLine(_Coordinates.Count.ToString() + " _Coordinates");
                        break;

                    case Vertice.NODE_ID:
                        _Vertices.LoadVertices(elements);
                        Console.WriteLine(_Vertices.Count.ToString() + " _Vertices");
                        break;           

                    default:
                        // log
                        break;
                }
            }
            catch (SystemException sysex)
            {
                Console.WriteLine(sysex.Message);
            }
        }

        /// <summary>
        /// According to the desired confidence and parameter defines, calibrate the node pressures
        /// </summary>
        public void CalibrateDemand(RegressionType calType)
        {
            double iSD = 0;
            double sSD = 0;
            double iMP = 0;
            double sMP = 0;

            float _mRRCap = Convert.ToSingle(ParamDefines.Default("mRRCap"));
            float _MiiSD = Convert.ToSingle(ParamDefines.Default("MiiSD"));
            float _MisSD = Convert.ToSingle(ParamDefines.Default("MisSD"));
            float _MsiSD = Convert.ToSingle(ParamDefines.Default("MsiSD"));
            float _MssSD = Convert.ToSingle(ParamDefines.Default("MssSD"));
            float _MiiMP = Convert.ToSingle(ParamDefines.Default("MiiMP"));
            float _MisMP = Convert.ToSingle(ParamDefines.Default("MisMP"));
            float _MsiMP = Convert.ToSingle(ParamDefines.Default("MsiMP"));
            float _MssMP = Convert.ToSingle(ParamDefines.Default("MssMP"));
            float _UiiMP = Convert.ToSingle(ParamDefines.Default("UiiMP"));
            float _UisMP = Convert.ToSingle(ParamDefines.Default("UisMP"));
            float _UsiMP = Convert.ToSingle(ParamDefines.Default("UsiMP"));
            float _UssMP = Convert.ToSingle(ParamDefines.Default("UssMP"));

            foreach (DictionaryEntry de in _NodePressures)
            {
                NodePressure pressure = (NodePressure)(de.Value);
                if (pressure.NodePressureG_RR > _mRRCap)
                {
                    if (calType == RegressionType.Percent90)
                    {
                        RandomNormal _random = RandomNormal.RandomNormalFunc(newRand);
                        iSD = (double)((_MiiSD + _MisSD * pressure.NodePressureAve_Pressure) * _random.Normal1);
                        sSD = (double)((_MsiSD + _MssSD * pressure.NodePressureAve_Pressure) * _random.Normal2);
                        iMP = (double)(_MiiMP + _MisMP * pressure.NodePressureAve_Pressure + iSD);
                        sMP = (double)(_MsiMP + _MssMP * pressure.NodePressureAve_Pressure + sSD);
                    }
                    else // Mean
                    {
                        iMP = (double)(_UiiMP + _UisMP * pressure.NodePressureAve_Pressure);
                        sMP = (double)(_UsiMP + _UssMP * pressure.NodePressureAve_Pressure);
                    }

                    if (_Demands.Contains(pressure.ID))
                    {
                         ((Demand)(_Demands[pressure.ID])).DemandBase *= (float)(iMP + sMP * pressure.NodePressureG_RR);
                    }
                }
            }
        }

        /// <summary>
        /// this method translated directly from Giraffe 08
        /// </summary>
        public void CreateLeakDamage()
        {            
            float breakPro;
            float _repairRate = 0;
            float _StlLeakRatio = Convert.ToSingle(ParamDefines.Default("STLLeakRatio"));
            RepairRate rate;
            int repairNumber = 0;
            int randCalled = 0;
            float lowestRand = 0;
            float largestRand = 0;
            double sumRand = 0;
            double avgRand = 0;

            try
            {
                foreach (DictionaryEntry de in _RepairRates)
                {
                    rate = (RepairRate)de.Value;                   
                                      
                    _repairRate = Convert.ToSingle(rate.RepairRateRR);

                    if (rate.RepairRateMaterial.ToUpper().Trim() == "STL")
                        _repairRate = (float)(rate.RepairRateRR * _StlLeakRatio);

                    repairNumber = 0;

                    if (_repairRate > 0.00001)
                    {
                        LeakRatio _leakRatio;

                        do
                        {
                            _leakRatio = new LeakRatio();
                            try
                            {
                                float _random = newRand.Next(RandomNormal.RAND_MAX);

                                if (SimulationData.Debug)
                                {
                                    ++randCalled;
                                    if (_random > largestRand) largestRand = _random;
                                    if (_random < lowestRand) lowestRand = _random;
                                    sumRand += _random;
                                }

                                //Console.WriteLine("Random.Next produced {0} maximum = {1}", _random, RandomNormal.RAND_MAX);
                                _random /= RandomNormal.RAND_MAX;
                                //Console.WriteLine("/= RandomNormal.RAND_MAX = {0}", _random);
                                if (_random == 1.0000f)
                                    _leakRatio.Ratio = 0.0f;
                                else
                                {
                                    // LEAVE this equation alone -- do not alter in any way to mimic Giraffe08
                                    var temp = -1.0f / (_repairRate * rate.RepairRateLength) * Math.Log(1.0f - _random);
                                    _leakRatio.Ratio = (float)temp;
                                }
                            }
                            catch (DivideByZeroException)
                            {
                                Console.WriteLine("Divide by zero exception caught on rate ID = " + rate.ID);
                                // hop out of this while loop
                                break;
                            }

                            if (repairNumber > 0)
                            {
                                object _ocurrent = _LeakRatios[_LeakRatios.Count - 1];
                                LeakRatio _current = (LeakRatio)_ocurrent;
                                _leakRatio.Ratio += _current.Ratio;
                            }

                            if (_leakRatio.Ratio <= 1.0f)
                            {
                                // seems we should be associating this leak with the pipe ID from the repair rate input
                                // wy are we doing this??
                                _leakRatio.ID = rate.ID;
                                // use a GUID to satisfy the need for a unique ID, can not use the ID from the repair rate
                                _LeakRatios.Add(System.Guid.NewGuid(), _leakRatio);
                                ++repairNumber;
                            }

                        } while (_leakRatio.Ratio <= 1.0f);

                        // per Giraffe 08 code
                        int preIndex = 0;
                        float preRatio = 0.0f;
                        int leakNumber = 0;
                        int breakNumber = 0;

                        try
                        {
                            foreach (DictionaryEntry de2 in _LeakRatios)
                            {
                                float randomValue = newRand.Next(RandomNormal.RAND_MAX);
                                randomValue /= RandomNormal.RAND_MAX;

                                LeakRatio _leakRatio2 = (LeakRatio)de2.Value;

                                breakPro = RepairRates.GetBreakPro(ParamDefines, rate);

                                // if true -- we need to break the pipe
                                if (breakPro > randomValue)
                                {
                                    PipeBreak newBreak = new PipeBreak();
                                    newBreak.ID = rate.ID;
                                    newBreak.PipeBreakRatio = _leakRatio2.Ratio;
                                    newBreak.PipeBreakRepairNo = repairNumber;
                                    newBreak.PipeBreakNo = ++breakNumber;
                                    newBreak.PipeBreakLeakNo = leakNumber;
                                    newBreak.PipeBreakPreIndex = preIndex;
                                    newBreak.PipeBreakPreRatio = preRatio;

                                    preIndex = 1;
                                    preRatio = _leakRatio2.Ratio;
                                    _PipeBreaks.Add(System.Guid.NewGuid(), newBreak);
                                }
                                else if (breakPro <= randomValue)
                                {
                                    PipeLeak newLeak = new PipeLeak();
                                    newLeak.ID = rate.ID;
                                    newLeak.PipeLeakRatio = _leakRatio2.Ratio;
                                    newLeak.PipeLeakRepairNo = repairNumber;
                                    newLeak.PipeLeakBreakNo = breakNumber;
                                    newLeak.PipeLeakNo = ++leakNumber;
                                    newLeak.PipeLeakPreIndex = preIndex;
                                    newLeak.PipeLeakPreRatio = preRatio;

                                    preIndex = 0;
                                    preRatio = _leakRatio2.Ratio;
                                    _PipeLeaks.Add(System.Guid.NewGuid(), newLeak);
                                }
                            }
                        }
                        catch(SystemException sysex)
                        {
                            Console.WriteLine(sysex.Message);
                        }
                    }

                    // clear the damage structures for the next iteration
                    _LeakRatios.Clear();
                }

                int counter = 0;
                while (counter < _PipeLeaks.Count) // use the count on the collection rather than a counter
                {
                    PipeLeak leak = (PipeLeak)_PipeLeaks[counter];

                    // verify that the pipe ID in the leak information exists in the pipe collection
                    if (_Pipes.Contains(leak.ID) && _RepairRates.Contains(leak.ID))
                    {
                        float randomValue = newRand.Next(RandomNormal.RAND_MAX);
                        randomValue /= RandomNormal.RAND_MAX;
                        Pipe tempPipe = (Pipe)_Pipes[leak.ID];
                        RepairRate tempRepair = (RepairRate)_RepairRates[leak.ID];
                        leak.PipeLeakD = PipeLeak.LeakSize(tempPipe, ParamDefines, tempRepair, Convert.ToDouble(randomValue));
                    }
                    else
                    {
                        // in giraffe 08 this value is set in LeakSize -- but we are doing it below to ensure the parameters to LeakSize
                        // are valid
                        leak.PipeLeakD = 0;
                    }

                    ++counter;
                }
            }
            catch (SystemException sysex)
            {
                Console.WriteLine(sysex.Message);
            }

            try
            {
                if (randCalled > 0) avgRand = sumRand / (double)randCalled;
            }
            catch(SystemException sysex)
            {
                Console.WriteLine(sysex.Message);
            }

            if (SimulationData.Debug)
            {
                Console.WriteLine("Highest random number = " + largestRand.ToString());
                Console.WriteLine("Lowest random number = " + lowestRand.ToString());
                Console.WriteLine("Average random number = " + avgRand.ToString());
                Console.WriteLine("Random number summation = " + sumRand.ToString());
                Console.WriteLine("Random number called = " + randCalled.ToString());
            }
        }

        /// <summary>
        /// get in string form, the simulation contents in the format expected of a system
        /// definition file
        /// </summary>
        public string NewSystemDefinitionFileContent
        {
            get
            {
                StringBuilder builder = new StringBuilder();
                builder.AppendFormat("[{0}]{1}", TITLE, Environment.NewLine);

                // keep the same order as received (Clenega)
                // add the Junctions
                builder.AppendFormat("{0}{1}", Environment.NewLine, _Junctions.ToString());

                // add the Reservoirs
                builder.AppendFormat("{0}{1}", Environment.NewLine, _Reservoirs.ToString());

                // add the Tanks
                builder.AppendFormat("{0}{1}", Environment.NewLine, _Tanks.ToString());

                // add the Pipes
                builder.AppendFormat("{0}{1}", Environment.NewLine, _Pipes.ToString());

                // Add the Pumps
                builder.AppendFormat("{0}{1}", Environment.NewLine, _Pumps.ToString());

                // Add the Valves
                builder.AppendFormat("{0}{1}", Environment.NewLine, _Valves.ToString());

                // Add the Demands
                builder.AppendFormat("{0}{1}", Environment.NewLine, _Demands.ToString());

                // Add the Curves (pumps)
                builder.AppendFormat("{0}{1}", Environment.NewLine, _Curves.ToString());

                // Add the Patterns
                builder.AppendFormat("{0}{1}", Environment.NewLine, _Patterns.ToString(SimulationData.Wsort));

                // Add Statuses
                builder.AppendFormat("{0}{1}", Environment.NewLine, _Statuses.ToString());
                
                // if true passed -- weird sort
                builder.AppendFormat("{0}{1}", Environment.NewLine, _Controls.ToString(SimulationData.Wsort));

                // the following sections are not used - but we need to duplicate old giraffe output
                // for debugging so these are added
                // beginning
                builder.AppendFormat("{0}{1}{2}{2}{3}{4}", Environment.NewLine, "[SOURCES]", Environment.NewLine, "[QUALITY]", Environment.NewLine);
                // end

                builder.AppendFormat("{0}{1}", Environment.NewLine, _Reactions.ToString());

                // the following sections are not used - but we need to duplicate old giraffe output
                // for debugging so these are added
                // beginning
                builder.AppendFormat("{0}{1}{2}", Environment.NewLine, "[ENERGY]", Environment.NewLine);
                // end

                builder.AppendFormat("{0}{1}", Environment.NewLine, _Options.ToString());

                builder.AppendFormat("{0}{1}", Environment.NewLine, _ReportOptions.ToString());

                builder.AppendFormat("{0}{1}", Environment.NewLine, _Coordinates.ToString());

                builder.AppendFormat("{0}{1}", Environment.NewLine, _Vertices.ToString());

                builder.AppendFormat("{0}[{1}]{2}", Environment.NewLine, END, Environment.NewLine);

                return builder.ToString();
            }
        }

        /// <summary>
        /// add the manfactured damage to the system, same as old system
        /// </summary>
        public void AddPipeDamage(string outputDirectory)
        {
            string step = "all good";

            try
            {
                step = "PrintPipeFile";
                Utilities.VerifyDirectory(outputDirectory + simulationCounter.ToString().Trim() + "\\");
                _Pipes.PrintPipeFile(outputDirectory + simulationCounter.ToString().Trim() + "\\", _PipeBreaks, _PipeLeaks);
                step = "AddPipeLeakJunctions";
                // not sure why the old giraffe removes all the original vertices from the system
                // but it does -- follow suit with an eye to this situation TODO: Frank
                _Vertices.Clear();
                AddPipeLeakJunctions();
                step = "AddPipeBreak";
                AddPipeBreak(_PipeBreaks);
                step = "AddStatusControl";
                AddStatusControl();
                step = "DeletePipes";
                DeletePipes();
                step = "DeleteStatus";
                DeleteStatus();
                step = "DeleteControl";
                DeleteControl();
                step = "FreePipeList";
                FreePipeList();
            }
            catch (SystemException sysex)
            {
                string temp = "Error performing " + step + " " + sysex.Message.Trim();
                throw new SystemException(temp);
            }
        }

        /// <summary>
        /// part of adding pipe damage to the system, same as old system
        /// </summary>
        public void AddPipeLeakJunctions()
        {
            // hop ove the first leak per Giraffe08
            foreach (DictionaryEntry deleak in _PipeLeaks)
            {
                PipeLeak leak = (PipeLeak)deleak.Value;

                if (leak.ID == "CC616")
                    leak.ID = "CC616";

                if (_Pipes.Contains(leak.ID))
                {
                    Pipe pipe = (Pipe)_Pipes[leak.ID];

                    float elevationStart = FindElevationNodeValue(pipe.StartNode);
                    float elevationEnd = FindElevationNodeValue(pipe.EndNode);

                    if ((elevationStart != 0f) && (elevationEnd != 0f))
                    {
                        float elevationFinal = elevationStart + (elevationEnd - elevationStart) * leak.PipeLeakRatio;

                        // add junction
                        Junction newJunction = new Junction();
                        newJunction.ID = "A" + leak.PipeLeakNo.ToString().Trim() + "J" + leak.ID.Trim();
                        newJunction.JunctionElevation = elevationFinal;
                        _Junctions.Add(newJunction.ID, newJunction);

                        // add reservoir
                        Reservoir newReservoir = new Reservoir();

                        int mID = leak.PipeLeakBreakNo * 2 + leak.PipeLeakNo;
                        newReservoir.ID = ("A" + mID.ToString().Trim() + "R" + leak.ID).Trim();
                        newReservoir.ReservoirTotalHead = elevationFinal;
                        _Reservoirs.Add(newReservoir.ID, newReservoir);

                        // add pipes -- original code use memcmp to compare numericals, I think we can avoid this and
                        // just use numerical values. If the results are not as expected re-address
                        if (leak.PipeLeakRepairNo == 1)
                        {
                            Pipe newPipe1 = new Pipe();
                            newPipe1.ID = "A10" + pipe.ID;
                            newPipe1.StartNode = pipe.StartNode;
                            newPipe1.EndNode = "A1J" + leak.ID;
                            newPipe1.PipeLength = leak.PipeLeakRatio * pipe.PipeLength;
                            newPipe1.PipeDiameter = pipe.PipeDiameter;
                            newPipe1.PipeRoughness = pipe.PipeRoughness;
                            newPipe1.PipeLossCoeff = pipe.PipeLossCoeff;
                            _Pipes.Add(newPipe1.ID, newPipe1);

                            Pipe newPipe2 = new Pipe();
                            newPipe2.ID = "A1L" + pipe.ID;
                            newPipe2.StartNode = "A1J" + pipe.ID;
                            newPipe2.EndNode = "A1R" + pipe.ID;
                            newPipe2.PipeLength = (float)PipeLeaks.LEAKL;
                            newPipe2.PipeDiameter = leak.PipeLeakD;
                            newPipe2.PipeRoughness = PipeLeaks.LEAKC;
                            newPipe2.PipeLossCoeff = PipeLeaks.LEAKM;
                            newPipe2.PipeExtra = "CV";
                            _Pipes.Add(newPipe2.ID, newPipe2);

                            Pipe newPipe3 = new Pipe();
                            newPipe3.ID = "A20" + pipe.ID;
                            newPipe3.StartNode = "A1J" + pipe.ID;
                            newPipe3.EndNode = pipe.EndNode;
                            newPipe3.PipeLength = (1 - leak.PipeLeakRatio) * pipe.PipeLength;
                            newPipe3.PipeDiameter = pipe.PipeDiameter;
                            newPipe3.PipeRoughness = pipe.PipeRoughness;
                            newPipe3.PipeLossCoeff = pipe.PipeLossCoeff;
                            _Pipes.Add(newPipe3.ID, newPipe3);
                        }
                        else if (leak.PipeLeakRepairNo > 1)
                        {
                            if ((leak.PipeLeakNo == 1) && (leak.PipeLeakBreakNo == 0))
                            {
                                Pipe newPipe1 = new Pipe();
                                newPipe1.ID = "A10" + pipe.ID;
                                newPipe1.StartNode = pipe.StartNode;
                                newPipe1.EndNode = "A1J" + leak.ID;
                                newPipe1.PipeLength = leak.PipeLeakRatio * pipe.PipeLength;
                                newPipe1.PipeDiameter = pipe.PipeDiameter;
                                newPipe1.PipeRoughness = pipe.PipeRoughness;
                                newPipe1.PipeLossCoeff = pipe.PipeLossCoeff;
                                _Pipes.Add(newPipe1.ID, newPipe1);

                                Pipe newPipe2 = new Pipe();

                                newPipe2.ID = "A1L" + pipe.ID;
                                newPipe2.StartNode = "A1J" + pipe.ID;
                                newPipe2.EndNode = "A1R" + pipe.ID;
                                newPipe2.PipeLength = (float)PipeLeaks.LEAKL;
                                newPipe2.PipeDiameter = leak.PipeLeakD;
                                newPipe2.PipeRoughness = PipeLeaks.LEAKC;
                                newPipe2.PipeLossCoeff = PipeLeaks.LEAKM;
                                newPipe2.PipeExtra = "CV";
                                _Pipes.Add(newPipe2.ID, newPipe2);
                            }
                            else
                            {
                                //mID = atoi(xTempPipeLeak->PipeLeakBreakNo.c_str()) * 2 + atoi(xTempPipeLeak->PipeLeakNo.c_str());
                                mID = leak.PipeLeakBreakNo * 2 + leak.PipeLeakNo;
                                if (leak.PipeLeakRepairNo == (leak.PipeLeakBreakNo + leak.PipeLeakNo))
                                {
                                    /*
                                     * if the preindex is = 0, the previous damage is a leak and m identifies the
                                     * location of the reservior for this last leak, i.e. in the naming scheme, A4R22,
                                     * m=4 identifying it is the 4th reservoir along the length of pipe 22. Remainder
                                     * of the loop assigns parameters to the new sections of pipe added around the
                                     * leak
                                     */
                                    if (leak.PipeLeakPreIndex == 0)
                                    {
                                        Pipe newPipe1 = new Pipe();
                                        newPipe1.ID = "A" + (leak.PipeLeakBreakNo + leak.PipeLeakNo) + "0" + pipe.ID;
                                        newPipe1.StartNode = "A" + (leak.PipeLeakNo - 1).ToString().Trim() + "J" + pipe.ID;
                                        newPipe1.EndNode = "A" + leak.PipeLeakNo.ToString().Trim() + "J" + pipe.ID;
                                        newPipe1.PipeLength = pipe.PipeLength * GetRatio(leak.PipeLeakRatio, leak.PipeLeakPreRatio);
                                        newPipe1.PipeDiameter = pipe.PipeDiameter;
                                        newPipe1.PipeRoughness = pipe.PipeRoughness;
                                        newPipe1.PipeLossCoeff = pipe.PipeLossCoeff;
                                        _Pipes.Add(newPipe1.ID, newPipe1);

                                        Pipe newPipe2 = new Pipe();
                                        newPipe2.ID = "A" + leak.PipeLeakNo + "L" + pipe.ID;
                                        newPipe2.StartNode = "A" + leak.PipeLeakNo.ToString().Trim() + "J" + pipe.ID;
                                        newPipe2.EndNode = "A" + mID.ToString().Trim() + "R" + pipe.ID;
                                        newPipe2.PipeLength = (float)PipeLeaks.LEAKL;
                                        newPipe2.PipeDiameter = leak.PipeLeakD;
                                        newPipe2.PipeRoughness = PipeLeaks.LEAKC;
                                        newPipe2.PipeLossCoeff = PipeLeaks.LEAKM;
                                        newPipe2.PipeExtra = "CV";
                                        _Pipes.Add(newPipe2.ID, newPipe2);

                                        Pipe newPipe3 = new Pipe();
                                        newPipe3.ID = "A" + (leak.PipeLeakBreakNo + leak.PipeLeakNo + 1).ToString().Trim() + "0" + pipe.ID;
                                        newPipe3.StartNode = "A" + leak.PipeLeakNo.ToString().Trim() + "J" + pipe.ID;
                                        newPipe3.EndNode = pipe.EndNode;
                                        newPipe3.PipeLength = (1 - leak.PipeLeakRatio) * pipe.PipeLength;
                                        newPipe3.PipeDiameter = pipe.PipeDiameter;
                                        newPipe3.PipeRoughness = pipe.PipeRoughness;
                                        newPipe3.PipeLossCoeff = pipe.PipeLossCoeff;
                                        _Pipes.Add(newPipe3.ID, newPipe3);
                                    }
                                    else
                                    {
                                        int nID = mID;
                                        mID = nID - 1;
                                        Pipe newPipe1 = new Pipe();
                                        newPipe1.ID = "A" + (leak.PipeLeakBreakNo + leak.PipeLeakNo).ToString().Trim() + "0" + pipe.ID;
                                        newPipe1.StartNode = "A" + leak.PipeLeakNo.ToString().Trim() + "J" + pipe.ID;
                                        newPipe1.EndNode = "A" + mID.ToString().Trim() + "R" + pipe.ID;
                                        newPipe1.PipeLength = pipe.PipeLength * GetRatio(leak.PipeLeakRatio, leak.PipeLeakPreRatio);
                                        newPipe1.PipeDiameter = pipe.PipeDiameter;
                                        newPipe1.PipeRoughness = pipe.PipeRoughness;
                                        newPipe1.PipeLossCoeff = pipe.PipeLossCoeff + 1;
                                        newPipe1.PipeExtra = "CV";
                                        _Pipes.Add(newPipe1.ID, newPipe1);

                                        Pipe newPipe2 = new Pipe();
                                        newPipe2.ID = "A" + leak.PipeLeakNo.ToString().Trim() + "L" + pipe.ID;
                                        newPipe2.StartNode = "A" + leak.PipeLeakNo.ToString().Trim() + "J" + pipe.ID;
                                        newPipe2.EndNode = "A" + nID.ToString().Trim() + "R" + pipe.ID;
                                        newPipe2.PipeLength = (float)PipeLeaks.LEAKL;
                                        newPipe2.PipeDiameter = leak.PipeLeakD;
                                        newPipe2.PipeRoughness = PipeLeaks.LEAKC;
                                        newPipe2.PipeLossCoeff = PipeLeaks.LEAKM;
                                        newPipe2.PipeExtra = "CV";
                                        _Pipes.Add(newPipe2.ID, newPipe2);

                                        Pipe newPipe3 = new Pipe();
                                        newPipe3.ID = "A" + (leak.PipeLeakBreakNo + leak.PipeLeakNo + 1).ToString().Trim() + "0" + pipe.ID;
                                        newPipe3.StartNode = "A" + leak.PipeLeakNo.ToString().Trim() + "J" + pipe.ID;
                                        newPipe3.EndNode = pipe.EndNode;
                                        newPipe3.PipeLength = (1 - leak.PipeLeakRatio) * pipe.PipeLength;
                                        newPipe3.PipeDiameter = pipe.PipeDiameter;
                                        newPipe3.PipeRoughness = pipe.PipeRoughness;
                                        newPipe3.PipeLossCoeff = pipe.PipeLossCoeff;
                                        _Pipes.Add(newPipe3.ID, newPipe3);
                                    }
                                }
                            }
                        }

                        // insert = true;

                        // Add Coordinates
                        DrawVertices drawVert = new DrawVertices();

                        drawVert.PreRatio = leak.PipeLeakPreRatio;
                        drawVert.PreIndex = leak.PipeLeakPreIndex;
                        AddCoordinates(pipe, GlobalRatio(leak.PipeLeakRatio),
                                                leak.PipeLeakBreakNo,
                                                leak.PipeLeakNo,
                                                "Leak",
                                                drawVert);
                    }
                    else
                    {
                        // logging maybe
                    }
                }
            }
        }

        /// <summary>
        /// NULL Method that adds the new coordinates to the system, this 
        /// method is called after the damage is added to the system; same as
        /// old system
        /// </summary>
        /// <param name="pipe"></param>
        /// <param name="ratio"></param>
        /// <param name="breakNo"></param>
        /// <param name="leakNo"></param>
        /// <param name="type"></param>
        /// <param name="drawVert"></param>
        public void AddCoordinates(Pipe pipe, float ratio, int breakNo, int leakNo, string type, DrawVertices drawVert)
        {
            double angle = 0.0;
            double pipeL = 0.0;
            double startNodeX = 0.0;
            double endNodeX = 0.0;
            double startNodeY = 0.0;
            double endNodeY = 0.0;
            int mID = 0;
            int nID = 0;

            //while(tempCoor->NextPtrSibling() != NULL)
            foreach (DictionaryEntry tempCoorde in _Coordinates)
            {                
                Coordinate tempCoor = (Coordinate)tempCoorde.Value;

                if (tempCoor.ID == pipe.StartNode)
                {
                    startNodeX = tempCoor.XCoordinate;
                    startNodeY = tempCoor.YCoordinate;
                }
                else if (tempCoor.ID == pipe.EndNode)
                {
                    endNodeX = tempCoor.XCoordinate;
                    endNodeY = tempCoor.YCoordinate;
                }

                if (startNodeX != 0.0 && endNodeY != 0.0)
                {
                    if (type.ToLower() == "break")
                    {
                        angle = Coordinates.CalculateAngle((float)startNodeX, (float)endNodeX, (float)startNodeY, (float)endNodeY);
                        pipeL = Math.Sqrt((startNodeX - endNodeX) * (startNodeX - endNodeX) + (startNodeY - endNodeY) * (startNodeY - endNodeY));
                        mID = breakNo * 2 - 1 + leakNo;
                        nID = breakNo * 2 + leakNo;

                        Coordinate coord = new Coordinate();
                        coord.ID = "A" + mID.ToString().Trim() + "R" + pipe.ID;
                        var tempx = startNodeX + (ratio - 0.05f) * pipeL * Math.Cos(angle) + 0.1f * pipeL * Math.Cos(Math.PI / 2 + angle);
                        coord.XCoordinate = (float)tempx;
                        var tempy = startNodeY + (ratio - 0.05f) * pipeL * Math.Sin(angle) + 0.1f * pipeL * Math.Sin(Math.PI / 2 + angle);
                        coord.YCoordinate = (float)tempy;
                        _Coordinates.Add(coord.ID, coord);

                        Coordinate coord2 = new Coordinate();
                        coord2.ID = "A" + nID.ToString().Trim() + "R" + pipe.ID;
                        tempx = startNodeX + (ratio + 0.05f) * pipeL * Math.Cos(angle) + 0.1f * pipeL * Math.Cos(Math.PI / 2 + angle);
                        coord2.XCoordinate = (float)tempx;
                        tempy = startNodeY + (ratio + 0.05f) * pipeL * Math.Sin(angle) + 0.1f * pipeL * Math.Sin(Math.PI / 2 + angle);
                        coord2.YCoordinate = (float)tempy;
                        _Coordinates.Add(coord2.ID, coord2);

                        //Add Vertices
                        drawVert.Angle = (float)angle;
                        drawVert.BreakNo = breakNo;
                        drawVert.LeakNo = leakNo;
                        drawVert.PipeL = (float)pipeL;
                        drawVert.Ratio = ratio;
                        drawVert.UpNodeX = (float)startNodeX;
                        drawVert.UpNodeY = (float)startNodeY;

                        /* Add the new vertices */
                        _Vertices.AddVertices(pipe, drawVert, "Break");
                        break;
                    }
                    else if (type.ToLower() == "leak")
                    {
                        try
                        {
                            angle = Math.Atan((endNodeY - startNodeY) / (endNodeX - startNodeX));
                        }

                        catch (DivideByZeroException divex)
                        {
                            Console.WriteLine(divex.Message);
#if ORIG_CODE_ANAMOLY
					        // Mimic original program functionality and ignore divide by zero errors
#else
                            throw;
#endif
                        }

                        pipeL = Math.Sqrt((startNodeX - endNodeX) * (startNodeX - endNodeX) + (startNodeY - endNodeY) * (startNodeY - endNodeY));
                        mID = breakNo * 2 + leakNo;
                        nID = leakNo;

                        Coordinate coord1 = new Coordinate();
                        coord1.ID = "A" + nID.ToString().Trim() + "J" + pipe.ID;
                        var tempx = startNodeX + ratio * (endNodeX - startNodeX);
                        coord1.XCoordinate = (float)tempx;
                        var tempy = startNodeY + ratio * (endNodeY - startNodeY);
                        coord1.YCoordinate = (float)tempy;
                        _Coordinates.Add(coord1.ID, coord1);

                        Coordinate coord2 = new Coordinate();
                        coord2.ID = "A" + mID.ToString().Trim() + "R" + pipe.ID;
                        tempx = startNodeX + ratio * (endNodeX - startNodeX) + 0.1f * pipeL * Math.Cos(Math.PI / 2 + angle);
                        coord2.XCoordinate = (float)tempx;
                        tempy = startNodeY + ratio * (endNodeY - startNodeY) + 0.2f * pipeL * Math.Sin(Math.PI / 2 + angle);
                        coord2.YCoordinate = (float)tempy;
                        _Coordinates.Add(coord2.ID, coord2);

                        /**
                            *  Add Vertices  */
                        drawVert.Angle = (float)angle;
                        drawVert.BreakNo = breakNo;
                        drawVert.LeakNo = leakNo;
                        drawVert.PipeL = (float)pipeL;
                        drawVert.Ratio = ratio;
                        drawVert.UpNodeX = (float)startNodeX;
                        drawVert.UpNodeY = (float)startNodeY;

                        /* Add the new vertices */
                        _Vertices.AddVertices(pipe, drawVert, "Leak");
                        break;
                    }
                }                
            }
        }

        /// <summary>
        /// the node id could belog to a juntion, tank or reservoir -- 
        /// find out which and return the proper elevation
        /// </summary>
        /// <param name="nodeID"></param>
        /// <returns></returns>
        public float FindElevationNodeValue(string nodeID)
        {
            float returnValue = 0;

            if (_Junctions.Contains(nodeID))
                returnValue = ((Junction)(_Junctions[nodeID])).JunctionElevation;

            else if (_Tanks.Contains(nodeID))
                returnValue = ((Tank)(_Tanks[nodeID])).TankElevation;

            else if (_Reservoirs.Contains(nodeID))
            {
                Reservoir reservoir = (Reservoir)_Reservoirs[nodeID];
                if ((reservoir.Pattern == null) || (reservoir.Pattern == string.Empty)
                                                || (!(_Patterns.Contains(reservoir.Pattern))))
                    returnValue = ((Reservoir)(_Reservoirs[nodeID])).ReservoirTotalHead;

                else if (_Patterns.Contains(reservoir.Pattern))
                {
                    Pattern pattern = (Pattern)_Patterns[reservoir.Pattern];
                    returnValue = pattern.Sum();
                }
            }

            return returnValue;
        }

        /// <summary>
        /// add the pipe break damage to the system
        /// </summary>
        public void AddPipeBreak(OrderedDictionary pipeBreaks)
        {
            int mID = 0;
            int nID = 0;            
            foreach (DictionaryEntry breakde in pipeBreaks)
            {                
                PipeBreak pipeBreak = (PipeBreak)breakde.Value;

                if (_Pipes.Contains(pipeBreak.ID))
                {
                    // there really is no way we should not pass this if -- in case
                    Pipe pipe = (Pipe)_Pipes[pipeBreak.ID];

                    float elevationStart = FindElevationNodeValue(pipe.StartNode);
                    float elevationEnd = FindElevationNodeValue(pipe.EndNode);
                    float elevationFinal = 0;

                    if ((elevationStart != 0.0) && (elevationEnd != 0.0))
                    {
                        elevationFinal = elevationStart + (elevationEnd - elevationStart) * pipeBreak.PipeBreakRatio;
                        mID = pipeBreak.PipeBreakNo * 2 - 1 + pipeBreak.PipeBreakLeakNo;
                        nID = pipeBreak.PipeBreakNo * 2 + pipeBreak.PipeBreakLeakNo;

                        // add reservoir
                        Reservoir reservoir = new Reservoir();
                        reservoir.ID = ("A" + mID.ToString().Trim() + "R" + pipeBreak.ID).Trim();
                        reservoir.ReservoirTotalHead = elevationFinal;
                        _Reservoirs.Add(reservoir.ID, reservoir);

                        Reservoir reservoir2 = new Reservoir();
                        reservoir2.ID = ("A" + nID.ToString().Trim() + "R" + pipeBreak.ID).Trim();
                        reservoir2.ReservoirTotalHead = elevationFinal;
                        _Reservoirs.Add(reservoir2.ID, reservoir2);

                        // add new pipes
                        if (pipeBreak.PipeBreakRepairNo == 1)
                        {
                            Pipe pipe1 = new Pipe();
                            pipe1.ID = "A10" + pipe.ID;
                            pipe1.StartNode = pipe.StartNode;
                            pipe1.EndNode = "A1R" + pipe.ID;
                            pipe1.PipeLength = pipe.PipeLength * GetRatio(pipeBreak.PipeBreakRatio, pipeBreak.PipeBreakPreRatio);
                            pipe1.PipeDiameter = pipe.PipeDiameter;
                            pipe1.PipeRoughness = pipe.PipeRoughness;
                            pipe1.PipeLossCoeff = pipe.PipeLossCoeff + 1;
                            pipe1.PipeExtra = "CV";
                            _Pipes.Add(pipe1.ID, pipe1);

                            Pipe pipe2 = new Pipe();
                            pipe2.ID = "A20" + pipe.ID;
                            pipe2.StartNode = pipe.EndNode;
                            pipe2.EndNode = "A2R" + pipe.ID;
                            pipe2.PipeLength = pipe.PipeLength * (1 - GetRatio(pipeBreak.PipeBreakRatio, pipeBreak.PipeBreakPreRatio));
                            pipe2.PipeDiameter = pipe.PipeDiameter;
                            pipe2.PipeRoughness = pipe.PipeRoughness;
                            pipe2.PipeLossCoeff = pipe.PipeLossCoeff + 1;
                            pipe2.PipeExtra = "CV";
                            _Pipes.Add(pipe2.ID, pipe2);
                        }
                        else if (pipeBreak.PipeBreakRepairNo > 1)
                        {
                            /* If there is 1 location of damage that is a pipe break, make 2 pipes from the one original
                                pipe and assign appropriate parameters to these 2 new pipes 
                                * For the first break, add only the first of the two new pipes to be added on
                                * either side of the break -- original comments.
                                */
                            if ((pipeBreak.PipeBreakNo == 1) && (pipeBreak.PipeBreakLeakNo == 0))
                            {
                                Pipe pipe1 = new Pipe();
                                pipe1.ID = "A10" + pipe.ID;
                                pipe1.StartNode = pipe.StartNode;
                                pipe1.EndNode = "A1R" + pipe.ID;
                                pipe1.PipeLength = pipe.PipeLength * GetRatio(pipeBreak.PipeBreakRatio, pipeBreak.PipeBreakPreRatio);
                                pipe1.PipeDiameter = pipe.PipeDiameter;
                                pipe1.PipeRoughness = pipe.PipeRoughness;
                                pipe1.PipeLossCoeff = pipe.PipeLossCoeff + 1;
                                pipe1.PipeExtra = "CV";
                                _Pipes.Add(pipe1.ID, pipe1);
                            }
                            else
                            {
                                /* For the last break. */
                                mID = pipeBreak.PipeBreakNo * 2 - 1 + pipeBreak.PipeBreakLeakNo;

                                if (pipeBreak.PipeBreakRepairNo == (pipeBreak.PipeBreakNo + pipeBreak.PipeBreakLeakNo))
                                {

                                    /*
                                        * If the preindex is = 0, the previous damage is a leak and m identifies the
                                        * location of the reservior for this last break, i.e. in the naming scheme,
                                        * A4R22, m=4 identifying it is the 4th reservoir along the length of pipe 22.
                                        * Remainder of the loop assigns parameters to the new sections of pipe on either
                                        * side of this last break
                                        */
                                    if (pipeBreak.PipeBreakPreIndex == 0)
                                    {
                                        Pipe pipe1 = new Pipe();
                                        pipe1.ID = "A" + (pipeBreak.PipeBreakNo + pipeBreak.PipeBreakLeakNo).ToString().Trim() + "0" + pipe.ID;
                                        pipe1.StartNode = "A" + pipeBreak.PipeBreakLeakNo.ToString().Trim() + "J" + pipe.ID;
                                        pipe1.EndNode = "A" + mID.ToString().Trim() + "R" + pipe.ID;
                                        pipe1.PipeLength = pipe.PipeLength * GetRatio(pipeBreak.PipeBreakRatio, pipeBreak.PipeBreakPreRatio);
                                        pipe1.PipeDiameter = pipe.PipeDiameter;
                                        pipe1.PipeRoughness = pipe.PipeRoughness;
                                        pipe1.PipeLossCoeff = pipe.PipeLossCoeff + 1;

                                        if (pipeBreak.PipeBreakNo > 1)
                                        {
                                            pipe1.PipeExtra = "";
                                        }
                                        else
                                        {
                                            pipe1.PipeExtra = "CV";
                                        }

                                        Pipe pipe2 = new Pipe();
                                        pipe2.ID = "A" + (pipeBreak.PipeBreakNo + pipeBreak.PipeBreakLeakNo + 1).ToString().Trim() + "0" + pipe.ID;
                                        pipe2.StartNode = pipe.EndNode;
                                        pipe2.EndNode = "A" + (mID + 1).ToString().Trim() + "R" + pipe.ID;
                                        pipe2.PipeLength = pipe.PipeLength * (1 - pipeBreak.PipeBreakRatio);
                                        pipe2.PipeDiameter = pipe.PipeDiameter;
                                        pipe2.PipeRoughness = pipe.PipeRoughness;
                                        pipe2.PipeLossCoeff = pipe.PipeLossCoeff + 1;
                                        pipe2.PipeExtra = "CV";

                                        _Pipes.Add(pipe1.ID, pipe1);
                                        _Pipes.Add(pipe2.ID, pipe2);
                                    }
                                    else
                                    {
                                        mID = (pipeBreak.PipeBreakNo - 1) * 2 + pipeBreak.PipeBreakLeakNo;
                                        nID = pipeBreak.PipeBreakNo * 2 + pipeBreak.PipeBreakLeakNo - 1;

                                        Pipe pipe1 = new Pipe();
                                        pipe1.ID = "A" + (pipeBreak.PipeBreakNo + pipeBreak.PipeBreakLeakNo).ToString().Trim() + "0" + pipe.ID;
                                        pipe1.StartNode = "A" + mID.ToString().Trim() + "R" + pipe.ID;
                                        pipe1.EndNode = "A" + nID.ToString().Trim() + "R" + pipe.ID;
                                        pipe1.PipeLength = pipe.PipeLength * GetRatio(pipeBreak.PipeBreakRatio, pipeBreak.PipeBreakPreRatio);
                                        pipe1.PipeDiameter = pipe.PipeDiameter;
                                        pipe1.PipeRoughness = pipe.PipeRoughness;
                                        pipe1.PipeLossCoeff = pipe.PipeLossCoeff + 1;

                                        if (pipeBreak.PipeBreakNo > 1)
                                        {                                                                                           /* Si es mayor */
                                            pipe1.PipeExtra = "";
                                        }
                                        else
                                        {
                                            pipe1.PipeExtra = "CV";
                                        }

                                        Pipe pipe2 = new Pipe();
                                        pipe2.ID = "A" + (pipeBreak.PipeBreakNo + pipeBreak.PipeBreakLeakNo + 1).ToString().Trim() + "0" + pipe.ID;
                                        pipe2.StartNode = pipe.EndNode;
                                        pipe2.EndNode = "A" + (nID + 1).ToString().Trim() + "R" + pipe.ID;
                                        pipe2.PipeLength = pipe.PipeLength * (1 - pipeBreak.PipeBreakRatio);
                                        pipe2.PipeDiameter = pipe.PipeDiameter;
                                        pipe2.PipeRoughness = pipe.PipeRoughness;
                                        pipe2.PipeLossCoeff = pipe.PipeLossCoeff + 1;
                                        pipe2.PipeExtra = "CV";

                                        _Pipes.Add(pipe1.ID, pipe1);
                                        _Pipes.Add(pipe2.ID, pipe2);
                                    }
                                }
                                else
                                {
                                    if (pipeBreak.PipeBreakPreIndex == 0)
                                    {
                                        Pipe pipe1 = new Pipe();
                                        pipe1.ID = "A" + (pipeBreak.PipeBreakNo + pipeBreak.PipeBreakLeakNo).ToString().Trim() + "0" + pipe.ID;
                                        pipe1.StartNode = "A" + pipeBreak.PipeBreakLeakNo.ToString().Trim() + "J" + pipe.ID;
                                        pipe1.EndNode = "A" + mID.ToString().Trim() + "R" + pipe.ID;
                                        pipe1.PipeLength = pipe.PipeLength * GetRatio(pipeBreak.PipeBreakRatio, pipeBreak.PipeBreakPreRatio);
                                        pipe1.PipeDiameter = pipe.PipeDiameter;
                                        pipe1.PipeRoughness = pipe.PipeRoughness;
                                        pipe1.PipeLossCoeff = pipe.PipeLossCoeff + 1;

                                        if (pipeBreak.PipeBreakNo > 1)
                                        {                                                                                           /* Si es mayor */
                                            pipe1.PipeExtra = "";
                                        }
                                        else
                                        {
                                            pipe1.PipeExtra = "CV";
                                        }
                                        _Pipes.Add(pipe1.ID, pipe1);

                                    }
                                    else
                                    {
                                        nID = mID;
                                        mID = (pipeBreak.PipeBreakNo - 1) * 2 + pipeBreak.PipeBreakLeakNo;

                                        Pipe pipe1 = new Pipe();
                                        pipe1.ID = "A" + (pipeBreak.PipeBreakNo + pipeBreak.PipeBreakLeakNo).ToString().Trim() + "0" + pipe.ID;
                                        pipe1.StartNode = "A" + mID.ToString().Trim() + "R" + pipe.ID;
                                        pipe1.EndNode = "A" + nID.ToString().Trim() + "R" + pipe.ID;
                                        pipe1.PipeLength = pipe.PipeLength * GetRatio(pipeBreak.PipeBreakRatio, pipeBreak.PipeBreakPreRatio);
                                        pipe1.PipeDiameter = pipe.PipeDiameter;
                                        pipe1.PipeRoughness = pipe.PipeRoughness;
                                        pipe1.PipeLossCoeff = pipe.PipeLossCoeff + 1;

                                        if (pipeBreak.PipeBreakNo > 1)
                                        {                                                                                           /* Si es mayor */
                                            pipe1.PipeExtra = "";
                                        }
                                        else
                                        {
                                            pipe1.PipeExtra = "CV";
                                        }

                                        _Pipes.Add(pipe1.ID, pipe1);
                                    }
                                }
                            }
                        }

                        DrawVertices drawVert = new DrawVertices();
                        drawVert.RepairNode = pipeBreak.PipeBreakRepairNo;
                        drawVert.PreRatio = pipeBreak.PipeBreakPreRatio;
                        drawVert.PreIndex = pipeBreak.PipeBreakPreIndex;

                        AddCoordinates(pipe,
                                        GlobalRatio(pipeBreak.PipeBreakRatio),
                                        pipeBreak.PipeBreakNo,
                                        pipeBreak.PipeBreakLeakNo,
                                        "Break",
                                        drawVert);
                    }
                    else { /* fatal error?? */ }                    
                }
            }
        }

        /// <summary>
        /// for the breaks introduced add a closed status
        /// </summary>
        public void AddStatusControl()
        {            
            foreach (DictionaryEntry breakde in _PipeBreaks)
            {                
                PipeBreak pipeBreak = (PipeBreak)breakde.Value;

                if (pipeBreak.PipeBreakNo > 1)
                {
                    Status stat = new Status();
                    int temp = pipeBreak.PipeBreakNo + pipeBreak.PipeBreakLeakNo;
                    stat.ID = "A" + temp.ToString().Trim() + "0" + pipeBreak.ID;
                    stat.StatusOption = BinaryValveStatus.Closed.ToString();
                    _Statuses.Add(stat.ID, stat);
                }                
            }
        }

        /// <summary>
        /// Method that frees the pipebreak & pipeleak structures, this method must be 
        /// called once the damage is added to the system (original comment)
        /// </summary>
        public void DeletePipes()
        {
            foreach (DictionaryEntry leakde in _PipeLeaks)
            {
                // original did hop over if needed
                PipeLeak pipeLeak = (PipeLeak)leakde.Value;

                if (_Pipes.Contains(pipeLeak.ID))
                {
                    _Pipes.Remove(pipeLeak.ID);
                }
            }

            // Delete pipe breaks
            foreach (DictionaryEntry pipebreakde in _PipeBreaks)
            {
                // hop over again -- ignore for now
                PipeBreak pipeBreak = (PipeBreak)pipebreakde.Value;

                if (_Pipes.Contains(pipeBreak.ID))
                {
                    _Pipes.Remove(pipeBreak.ID);
                }
            }
        }

        /// <summary>
        /// Method that deletes statuses for pipes that no longer exist 
        /// they were deleted earlier
        /// </summary>
        public void DeleteStatus()
        {
            foreach (DictionaryEntry leakde in _PipeLeaks)
            {
                // original did hop over if needed
                PipeLeak pipeLeak = (PipeLeak)leakde.Value;

                if (_Statuses.Contains(pipeLeak.ID))
                {                    
                    _Statuses.Remove(pipeLeak.ID);
                }
            }

            // Delete pipe breaks
            foreach (DictionaryEntry pipebreakde in _PipeBreaks)
            {
                // hop over again -- ignore for now
                PipeBreak pipeBreak = (PipeBreak)pipebreakde.Value;

                if (_Statuses.Contains(pipeBreak.ID))
                {                    
                    _Statuses.Remove(pipeBreak.ID);
                }
            }
        }

        /// <summary>
        /// Method that deletes controls for pipes that no longer exist 
        /// they were deleted earlier
        /// </summary>
        public void DeleteControl()
        {
            foreach (DictionaryEntry leakde in _PipeLeaks)
            {
                // original did hop over if needed
                PipeLeak pipeLeak = (PipeLeak)leakde.Value;

                if (_Controls.Contains(pipeLeak.ID))
                {
                    _Controls.Remove(pipeLeak.ID);
                }
            }

            // Delete pipe breaks
            foreach (DictionaryEntry pipebreakde in _PipeBreaks)
            {
                // hop over again -- ignore for now
                PipeBreak pipeBreak = (PipeBreak)pipebreakde.Value;

                if (_Controls.Contains(pipeBreak.ID))
                {
                    _Controls.Remove(pipeBreak.ID);
                }
            }
        }

        /// <summary>
        /// this method clears the leak and breaks collections, horrible name but keep the
        /// same until working
        /// </summary>
        public void FreePipeList()
        {
            if (_PipeBreaks != null)
                _PipeBreaks.Clear();

            if (_PipeLeaks != null)
                _PipeLeaks.Clear();
        }

        /// <summary>
        /// make sure the directories are created/exists and if need be
        /// are empty and ready to go
        /// </summary>
        public void PrepareDirectory(string directory)
        {
            try
            {
                if (Directory.Exists(directory))
                    Directory.Delete(directory, true);

                Directory.CreateDirectory(directory);
            }
            catch (SystemException sysex)
            {
                var temp = "Directory preparation; " + directory + " failed. " + sysex.Message.Trim();
                throw new SystemException(temp);
            }
        }

        /// <summary>
        /// Eliminate from the system nodes that have negative pressure, negative defines by the system
        /// as below the minimum. Algorithm identical to old code
        /// </summary>
        /// <param name="minPressure"></param>
        /// <returns></returns>
        public bool NegativePressuresEliminated(int minPressure)
        {
            bool returnValue = true;
            int numberOfNodes = epa.NodeCount(CountType.Node);
            float minPressureSystem = 0f;
            int indexNegativeP = -1;
            int totalNegatives = 0;
            //StringBuilder builder = new StringBuilder();

            //	statusMsgsQ.Enqueue("Please Wait.");
            for (int i = 1; i <= numberOfNodes; i++)
            {
                float fvalue;
                
                try
                {                    
                    fvalue = epa.GetNodeValue(i, NodeValue.Pressure);
                    //builder.AppendFormat("{0}, {1}, {2}", i, epa.GetNodeID(i), fvalue.ToString("F10"));
                    //Console.WriteLine(builder.ToString());
                    //builder.Clear();
                    if ((fvalue < (float)minPressure) && (fvalue < minPressureSystem))
                    {                                                                            
                        minPressureSystem = fvalue;
                        indexNegativeP = i;
                        totalNegatives++;
                    }

                } /* Get Pressure Value code11 */
                catch (SystemException sysex)
                {
                    /* return codes are ignored on EPANet calls -- console print */
                    Console.WriteLine("In FindNegativePressures, " + sysex.Message.Trim());
                }
            }
           
            Console.WriteLine("Total negatives = " + totalNegatives.ToString());

            if (indexNegativeP != -1)
            { /*We have negative Pressure, Delete it (them,re: note above)*/

                /*Delete the node that does not satisfy the minpressure for the system*/
                DeleteNegativeNode(indexNegativeP);

                /*Delete the link attached to the node*/
                DeleteNegativeLink(indexNegativeP);

                /*Delete any status/control/demand linked to the node that does not satisfy*/
                DeleteOther(indexNegativeP);

                // there may be more negative values -- keep current code flow
                returnValue = false;
            }
            else
                Console.WriteLine("Negative pressures eliminated!");

            // none found - they have been eliminated
            return returnValue;

        }

        /// <summary>
        /// delete a negative pressure node, same as old code
        /// </summary>
        /// <param name="index"></param>
        public void DeleteNegativeNode(int index)
        {
            NodeType type = epa.GetNodeType(index);
            string id = epa.GetNodeID(index);
            

            if ((id != null) && (id != string.Empty))
            {
                switch (type)
                {
                    case NodeType.Junction:
                        if (_Junctions.Contains(id))
                            _Junctions.Remove(id);
                        break;

                    case NodeType.Reservoir:
                        if (_Reservoirs.Contains(id))
                            _Reservoirs.Remove(id);
                        break;

                    case NodeType.Tank:
                        if (_Tanks.Contains(id))
                            _Tanks.Remove(id);
                        break;

                    default:
                        Console.WriteLine("DeleteNegativeNode unknown type = " + type.ToString());
                        break;
                }

                if (_Coordinates.Contains(id))
                    _Coordinates.Remove(id);
            }
            else
                Console.WriteLine("DeleteNegativeNode Node ID unknown, index = " + index.ToString());
        }

        /// <summary>
        /// delete a link; negative pressure, same as old code
        /// </summary>
        /// <param name="index"></param>
        public void DeleteNegativeLink(int index)
        {
            int totalNodes = epa.NodeCount(CountType.Link);

            for (int count = 1; count <= totalNodes; count++)
            {
                // init to remove error -- means nothing
                LinkType type = LinkType.CVPipe;
                string lid;

                EpaPort.StartEnd startEnd = epa.GetLinkNodes(count);
                if ((startEnd.StartNodeIndex == index) || (startEnd.EndNodeIndex == index))
                {
                    type = epa.GetLinkType(count);
                    lid = epa.GetLinkID(count);

                    if ((int)type < 2)
                    {                        
                        if (_Pipes.Contains(lid))
                            _Pipes.Remove(lid);
                    }
                    else if ((int)type == 2)
                    {                        
                        if (_Pumps.Contains(lid))
                            _Pumps.Remove(lid);
                    }
                    else
                    {        
                        // we have a valve
                        if (_Valves.Contains(lid))
                            _Valves.Remove(lid);                            
                        else
                            Console.WriteLine("DeleteNegativeLink Node type unknown, type = " + type.ToString());                            
                    }

                    if (epa.GetLinkValue(count, LinkValue.InitStatus) == 0)
                    {                       
                        if (_Statuses.Contains(lid))
                            _Statuses.Remove(lid);
                    }

                    int nindex;
                    if (epa.GetControl(count, out nindex) == ErrorCode.Ok)
                    {
                        if (_Controls.Contains(lid))
                            _Controls.Remove(lid);

                        // this code is marked as a question in the original code
                        string newId = epa.GetNodeID(nindex);
                        if (_Controls.Contains(newId))
                            _Controls.Remove(newId);
                    }

                    if (_Vertices.Contains(lid))
                        _Vertices.Remove(lid);
                }
            }
        }

        /// <summary>
        /// delete associated demand
        /// </summary>
        /// <param name="index"></param>
        public void DeleteOther(int index)
        {
            float demand = epa.GetNodeValue(index, NodeValue.Demand);
            string nodeID = epa.GetNodeID(index);

            if (demand != 0)
            {
                if (_Demands.Contains(nodeID))
                    _Demands.Remove(nodeID);
            }
        }

        /// <summary>
        /// Write out a snapshot of the system midway in a simulation -- same as old code
        /// </summary>
        /// <param name="time"></param>
        public void WritePostProcessing(int time)
        {
            if (epa.Opened)
            {
                string nodeResults_Time = outputPathFull + "NodeResults_Time" + time + ".out";
                string junctionResults_Time = outputPathFull + "JunctionResults_Time" + time + ".out";
                string tankResults_Time = outputPathFull + "TankResults_Time" + time + ".out";
                string valveResults_Time = outputPathFull + "ValveResults_Time" + time + ".out";
                string linkResults_Time = outputPathFull + "LinkResults_Time" + time + ".out";
                string pipeResults_Time = outputPathFull + "PipeResults_Time" + time + ".out";
                string pumpResults_Time = outputPathFull + "PumpResults_Time" + time + ".out";

                StringBuilder builder = new StringBuilder();
                StringBuilder nodeResults = new StringBuilder();
                StringBuilder linkResults = new StringBuilder();

                // link header
                linkResults.AppendFormat("{0}\t{1}\t{2}\t{3}\t{4}{5}", "LinkID", "Flow_gpm", "Velocity_ft", "Headloss_/1000ft", "Type", Environment.NewLine);
                // node header
                nodeResults.AppendFormat("{0}\t{1}\t{2}\t{3}{4}", "NodeID", "Demand_gpm", "Head_ft", "Pressure_psi", Environment.NewLine);

                // Junction info
                builder.AppendFormat("{0}\t{1}\t{2}\t{3}{4}", "JunctionID", "Demand_gpm", "Head_ft", "Pressure_psi", Environment.NewLine);
                builder.AppendFormat("{0}{1}", _Junctions.ToEPAString(), Environment.NewLine);
                // save Junctions results
                File.WriteAllText(junctionResults_Time, builder.ToString());
                nodeResults.AppendFormat("{0}{1}", _Junctions.ToEPAString(Junction.NODE_ID), Environment.NewLine);

                // Tank info
                builder.Clear();
                builder.AppendFormat("{0}\t{1}\t{2}\t{3}\t{4}", "TankID", "Demand_gpm", "Head_ft", "Pressure_psi", Environment.NewLine);
                builder.AppendFormat("{0}{1}", _Tanks.ToEPAString(), Environment.NewLine);
                // save reservoirs to the tank file
                builder.AppendFormat("{0}{1}", _Reservoirs.ToEPAString(), Environment.NewLine);
                // save Tank results
                File.WriteAllText(tankResults_Time, builder.ToString());
                nodeResults.AppendFormat("{0}{1}", _Tanks.ToEPAString(Tank.NODE_ID), Environment.NewLine);
                nodeResults.AppendFormat("{0}{1}", _Reservoirs.ToEPAString(Reservoir.NODE_ID), Environment.NewLine);
                
                // write out "Nodes"
                File.WriteAllText(nodeResults_Time, nodeResults.ToString());

                // Pipe info
                builder.Clear();
                builder.AppendFormat("{0}\t{1}\t{2}\t{3}{4}", "PipeID", "Flow_gpm", "Velocity_ft", "Headloss_/1000ft", Environment.NewLine);
                builder.AppendFormat("{0}{1}", _Pipes.ToEPAString(), Environment.NewLine);
                // save pipes results
                File.WriteAllText(pipeResults_Time, builder.ToString());
                linkResults.AppendFormat("{0}{1}", _Pipes.ToEPAString(Pipe.NODE_ID), Environment.NewLine);

                // Pump info
                builder.Clear();
                builder.AppendFormat("{0}\t{1}\t{2}\t{3}{4}", "PumpID", "Flow_gpm", "Velocity_ft", "Headloss_/1000ft", Environment.NewLine);
                builder.AppendFormat("{0}{1}", _Pumps.ToEPAString(), Environment.NewLine);
                // save pump results
                File.WriteAllText(pumpResults_Time, builder.ToString());
                linkResults.AppendFormat("{0}{1}", _Pumps.ToEPAString(Pump.NODE_ID), Environment.NewLine);

                // Valve info
                builder.Clear();
                builder.AppendFormat("{0}\t{1}\t{2}\t{3}{4}", "ValveID", "Flow_gpm", "Velocity_ft", "Headloss_/1000ft", Environment.NewLine);
                builder.AppendFormat("{0}{1}", _Valves.ToEPAString(), Environment.NewLine);
                // save valve results
                File.WriteAllText(valveResults_Time, builder.ToString());
                linkResults.AppendFormat("{0}{1}", _Valves.ToEPAString(Valve.NODE_ID), Environment.NewLine);

                // write out "Links"
                File.WriteAllText(linkResults_Time, linkResults.ToString());
            }
        }

        /// <summary>
        /// add a SumDemand to the appropriate collection -- special handling
        /// if the ID already exists
        /// </summary>
        /// <param name="sdemand"></param>
        public void AddSumDemand(SumDemand sdemand)
        {
            if (_SumDemands.Contains(sdemand.ID))
            {
                ((SumDemand)(_SumDemands[sdemand.ID])).value += " " + sdemand.value.Trim();
            }
            else
                _SumDemands.Add(sdemand.ID, sdemand);
        }

        /// <summary>
        /// Write out the results, the serviceability percentages, same as old code
        /// </summary>
        /// <param name="actualSimulationsRan"></param>
        /// <returns></returns>
        public string WriteServiceCapability(int actualSimulationsRan)
        {
            string returnResults = string.Empty;
            float demandDis = 0.0f;
            float avgDemand = 0.0f;	        
            double sumAvgTem = 0.0f;
            string tempDemand;
            string printDemand = "";
            SumDemand sumDemand;
            _SumDemands.Clear();

            try
            {

                StringBuilder builder = new StringBuilder();
                builder.AppendFormat("{0}{1}{2}{3}", outputPathFull, "Serviceability", 0, ".out");
                string fileName = builder.ToString().Trim();

                builder.Clear();
                builder.AppendFormat("{0}\t{1}\t", "Node_ID", "Demand");

                for (int i = 1; i <= actualSimulationsRan; i++)
                    builder.AppendFormat("{0}\t", i);

                builder.AppendFormat("{0}{1}{1}{1}", "Node_Serviceability", Environment.NewLine);

                IDictionary sorted = MyOrderedDictionary.OrderedByID(_Serviceabilities0);
                
                //  Calculate 
                foreach (DictionaryEntry de in sorted)
                {
                    // hop over
                    //xSNode = xSNode->NextPtrSibling();
                    Serviceability serv = (Serviceability)de.Value;

                    tempDemand = serv.Value;
                    sumAvgTem += serv.DemandBase;

                    float temp = serv.DemandBase;
                    builder.AppendFormat("{0}\t{1}\t", serv.ID, temp.ToString("F5"));

                    for (int j = 1; j <= actualSimulationsRan; j++)
                    {
                        sumDemand = new SumDemand();

                        sumDemand.ID = j.ToString();
                        printDemand = (tempDemand.Split(' '))[j-1];
                        sumDemand.value = printDemand;
                        AddSumDemand(sumDemand);

                        builder.AppendFormat("{0}\t", printDemand);
                    }

                    builder.AppendFormat("{0}{1}", (serv.Mean / (serv.DemandBase * actualSimulationsRan)).ToString("F4"), Environment.NewLine);
                }

                builder.AppendFormat("{0}{0}{1}\t\t", Environment.NewLine, "Sum");

                foreach (DictionaryEntry de in _SumDemands)
                {                    
                    SumDemand sdemand = (SumDemand)de.Value;
                    string[] demValues = sdemand.value.Split(' ');

                    int totalValues = demValues.Length;
                    
                    // was using _Demands.Count for loop
                    for (int k = 0; k < totalValues; k++)
                    {                        
                        demandDis += Convert.ToSingle(demValues[k]);
                    }
                    
                    avgDemand += (float)(demandDis / sumAvgTem);
                    builder.AppendFormat("{0}\t", (demandDis / sumAvgTem).ToString("F5"));
                    demandDis = 0.0f;
                }

                builder.AppendFormat("\t{0}{1}", avgDemand / actualSimulationsRan, Environment.NewLine);
                File.WriteAllText(fileName, builder.ToString().Trim());
                returnResults += fileName + ";";

                if (this.SimulationData.NumberOfHours != 0)
                {

                    /* Service Time Step */
                    avgDemand = 0.0f;
                    demandDis = 0.0f;
                    sumAvgTem = 0.0f;
                    printDemand = "";

                    _SumDemands.Clear();

                    builder.Clear();
                    builder.AppendFormat("{0}{1}{2}{3}", outputPathFull, "Serviceability", this.SimulationData.NumberOfHours, ".out");
                    fileName = builder.ToString();
                    builder.Clear();

                    //  Title 
                    builder.AppendFormat("{0}\t{1}\t", "Node_ID", "Demand");

                    for (int i = 1; i <= actualSimulationsRan; i++)
                    {
                        builder.AppendFormat("{0}\t", i);
                    }
                    
                    builder.AppendFormat("{0}{1}{1}{1}", "Node_Serviceability", Environment.NewLine);

                    //  Calculate  */
                    sorted = MyOrderedDictionary.OrderedByID(_Serviceabilities1);
                    foreach (DictionaryEntry de in sorted)
                    {
                        // hop over
                        //xSNode = xSNode->NextPtrSibling();
                        Serviceability serv = (Serviceability)de.Value;

                        tempDemand = serv.Value;
                        sumAvgTem += serv.DemandBase;

                        builder.AppendFormat("{0}\t{1}\t", serv.ID, serv.DemandBase.ToString("F5"));

                        for (int j = 1; j <= actualSimulationsRan; j++)
                        {
                            sumDemand = new SumDemand();
                            sumDemand.ID = j.ToString("0000000000");
                            printDemand = (tempDemand.Split(' '))[j-1];
                            sumDemand.value = printDemand;
                            AddSumDemand(sumDemand);
                            builder.AppendFormat("{0}\t", printDemand);
                        }

                        builder.AppendFormat("{0}{1}", (serv.Mean / (serv.DemandBase * actualSimulationsRan)).ToString("F5"), Environment.NewLine);
                    }
                    
                    builder.AppendFormat("{0}{0}{1}\t\t", Environment.NewLine, "Sum");

                    foreach (DictionaryEntry de in _SumDemands)
                    {                                 
                        SumDemand sdemand = (SumDemand)de.Value;
                        string[] demValues = sdemand.value.Split(' ');

                        int totalValues = demValues.Length;
                       
                        // was using _Demands.Count for loop
                        for (int k = 0; k < totalValues; k++)
                        {
                            demandDis += Convert.ToSingle(demValues[k]);
                        }

                        avgDemand += (float)(demandDis / sumAvgTem);
                        builder.AppendFormat("{0}\t", (demandDis / sumAvgTem).ToString("F5"));
                        demandDis = 0.0f;
                    }

                    builder.AppendFormat("\t{0}{1}", (avgDemand / actualSimulationsRan).ToString("F5"), Environment.NewLine);
                    try 
                    { 
                        File.WriteAllText(fileName, builder.ToString().Trim());
                        returnResults += fileName;
                    }
                    catch
                    {
                        Console.WriteLine("bad file write " + fileName);
                        // in case
                        string failSafeFile = @"C:\temp\results" + Guid.NewGuid().ToString();
                        returnResults += failSafeFile;
                        File.WriteAllText(failSafeFile, builder.ToString().Trim());
                    }
                }

                _SumDemands.Clear();
            }
            catch (SystemException sysex)
            {
                Console.WriteLine(sysex.Message);
            }

            return returnResults;
        }
        
        /// <summary>
        /// this is a crazy routine where we open an error output file from EPANet and then try
        /// to clean up the model according to the errors received. This approach needs to be noted
        /// and communicated as an improper model signifies (maybe) some logic that needs to be
        /// addressed
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="type"></param>
        /// <param name="update"></param>
        /// <returns></returns>
        public string GetNodeErrorInOutPutFile(string fileName, out NodeTypes type, out bool update)
        {
            string returnValue = string.Empty;
            update = false;
            type = NodeTypes.basenode;
            string fileBody = "";

            try
            {

                if (File.Exists(fileName))
                {
                    // until we can put a read lock on -- try
                    bool open = false;
                    while (!open)
                    {
                        try
                        {
                            FileStream myStream = File.Open(fileName, FileMode.Open, FileAccess.Write, FileShare.None);
                            open = true;
                            myStream.Close();
                        }
                        catch { }
                    }
                    
                    fileBody = File.ReadAllText(fileName);

                    // hop over the first ten lines as they are EPA header                
                    string[] fileBodysegs = fileBody.Split(Environment.NewLine.ToCharArray());
                    int count = 15;
                    if (fileBodysegs.Count() > count)
                    {
                        for (int startingIndex = count; startingIndex < fileBodysegs.Count(); startingIndex++)
                        {
                            string line = fileBodysegs[startingIndex];
                            if (line.Trim() != "")
                            {
                                int found = line.IndexOf("233:");
                                if (found != -1)
                                {
                                    int foundNode = 0;
                                    if ((foundNode = line.IndexOf("Node ")) != -1)
                                    {
                                        foundNode += "Node ".Length;
                                        int next = line.IndexOf(" ", foundNode);
                                        returnValue = (line.Substring(foundNode, next - foundNode + 1)).Trim();
                                    }
                                    else
                                    {
                                        found += "233:".Length + 1;
                                        int next = line.IndexOf(" ", found);
                                        returnValue = (line.Substring(found, next - found + 1)).Trim();
                                    }

                                    type = NodeTypes.basenode;
                                    break;
                                }
                                else if ((found = line.IndexOf("203: Pipe")) != -1)
                                {
                                    found += "203: Pipe".Length + 1;
                                    int next = line.IndexOf(" ", found);
                                    returnValue = (line.Substring(found, next - found + 1)).Trim();
                                    type = NodeTypes.pipe;
                                    break;
                                }
                                else if ((found = line.IndexOf("203: Valve")) != -1)
                                {
                                    found += "203: Valve".Length + 1;
                                    int next = line.IndexOf(" ", found);
                                    returnValue = (line.Substring(found, next - found + 1)).Trim();
                                    type = NodeTypes.valve;
                                    break;
                                }
                                else if (line.IndexOf("203: Control  refers to undefined node") != -1)
                                {
                                    string nextLine = "";
                                    int nextCount = 0;
                                    while (nextLine.Trim() == "")
                                    {
                                        ++nextCount;
                                        nextLine = fileBodysegs[startingIndex + nextCount];
                                    }

                                    int first = nextLine.IndexOf("Node ");
                                    if (first != -1)
                                    {
                                        nextLine = nextLine.Remove(0, first + 5);
                                        int next = nextLine.IndexOf("\t", 0);
                                        returnValue = nextLine.Substring(0, next + 1).Trim();
                                        type = NodeTypes.controldetail;
                                        break;
                                    }
                                }
                                else if (line.IndexOf("204: Control  refers to undefined link") != -1)
                                {
                                    string nextLine = "";
                                    int nextCount = 0;
                                    while (nextLine.Trim() == "")
                                    {
                                        ++nextCount;
                                        nextLine = fileBodysegs[startingIndex + nextCount];
                                    }
                                    nextLine = nextLine.Remove(0, 7);
                                    returnValue = (nextLine.Split('\t')[0]).Trim();
                                    type = NodeTypes.control;
                                    break;
                                }
                                else if ((found = line.IndexOf("203: Pump")) != -1)
                                {
                                    found += "203: Pump".Length + 1;
                                    int next = line.IndexOf(" ", found);
                                    returnValue = (line.Substring(found, next - found + 1)).Trim();
                                    type = NodeTypes.pump;
                                    break;
                                }
                                else if (line.IndexOf("ill-conditioned") != -1)
                                {
                                    line = line.Remove(0, 45);
                                    returnValue = (line.Split('\n')[0]).Trim();
                                    type = NodeTypes.basenode;
                                    break;
                                }
                                else if ((found = line.IndexOf("202: Pipe")) != -1)
                                {
                                    if (line.IndexOf("illegal numeric value") != -1)
                                    {
                                        update = true;
                                        found += "202: Pipe".Length + 1;
                                        int next = line.IndexOf(" ", found);
                                        returnValue = (line.Substring(found, next - found + 1)).Trim();
                                        type = NodeTypes.pipe;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (SystemException sysex)
            {
                // we need this in case we have errors not anticipated/seen
                var newEx = new Exception("Error repairing model; " + sysex.Message + " Text being parsed = " + fileBody.Trim());
                throw newEx;
            }

            return returnValue;
        }

        /// <summary>
        /// delete this "generic" node -- logic as delivered
        /// </summary>
        /// <param name="nodeId"></param>
        /// <returns></returns>
        public bool DeleteBaseNode(string nodeId)
        {
            bool ret = true;

            if (_Junctions.Contains(nodeId))
                _Junctions.Remove(nodeId);
            else if (_Tanks.Contains(nodeId))
                _Tanks.Remove(nodeId);
            else if (_Reservoirs.Contains(nodeId))
                _Reservoirs.Remove(nodeId);
            else
                ret = false;           

            if (_Coordinates.Contains(nodeId))
            {
                _Coordinates.Remove(nodeId);
                ret = true;
            }

            if (_Demands.Contains(nodeId))
            {
                _Demands.Remove(nodeId);
                ret = true;
            }

            if (_Controls.DeleteDetail(nodeId))
                ret = true;
                       
            DeleteLinkNonNode(nodeId);
            return (ret);
        }

        /// <summary>
        /// more search for things to delete based off a node that supposedly
        /// is not associated with anything; logic as delivered
        /// </summary>
        /// <param name="nodeID"></param>
        public void DeleteLinkNonNode(string nodeID)
        {
            bool flag = false;
            
            foreach(DictionaryEntry de in _Pipes)
            {
                // hop over -- check if results bad
                //xPNode = xPNode->NextPtrSibling();
                Pipe pipe = (Pipe)de.Value;
                if ((pipe.StartNode == nodeID) || (pipe.EndNode == nodeID))                
                {
                    if (flag == false)
                    {
                        if (_Statuses.Contains(pipe.ID))
                        {                           
                            _Statuses.Remove(pipe.ID);
                        }
                        if (_Controls.Contains(pipe.ID))
                            _Controls.Remove(pipe.ID);
                        if (_Vertices.Contains(pipe.ID))
                            _Vertices.Remove(pipe.ID);                        
                        flag = true;
                    }

                    _Pipes.Remove(pipe.ID);
                    break;
                }
            }
            
            foreach(DictionaryEntry de in _Pumps)
            {
                // hop over
                //xPuNode = xPuNode->NextPtrSibling();
                Pump pump = (Pump)de.Value;

                if ((nodeID == pump.StartNode) || (nodeID == pump.EndNode))
                {
                    if (flag == false)
                    {
                        if (_Statuses.Contains(pump.ID))
                        {                            
                            _Statuses.Remove(pump.ID);
                        }
                        if (_Controls.Contains(pump.ID))
                            _Controls.Remove(pump.ID);
                        if (_Vertices.Contains(pump.ID))
                            _Vertices.Remove(pump.ID);
                        flag = true;                        
                    }

                    _Pumps.Remove(nodeID);
                    break;
                }
            }

            foreach(DictionaryEntry de in _Valves)
            {
                // hop over
                //xVNode = xVNode->NextPtrSibling();

                Valve valve = (Valve)de.Value;
                if ((valve.StartNode == nodeID) || (valve.EndNode == nodeID))
                {                
                    if (flag == false)
                    {
                        if (_Statuses.Contains(valve.ID))
                        {                            
                            _Statuses.Remove(valve.ID);
                        }
                        if (_Controls.Contains(valve.ID))
                            _Controls.Remove(valve.ID);
                        if (_Vertices.Contains(valve.ID))
                            _Vertices.Remove(valve.ID);                        
                        flag = true;
                    }

                    _Valves.Remove(nodeID);
                    break;
                }
            }
        }

        /// <summary>
        /// we have errors -- correct the model based on errors in the output file
        /// logic as delivered
        /// </summary>
        /// <param name="fileOutputRpt"></param>
        /// <returns></returns>
        public bool RepairModel(string fileOutputRpt)
        {            
            epa.Close();
            NodeTypes type;
            bool update;
            
            // this is insane - there may be more than on error in this file but calling
            // in this fashion mimics original flow, address latr if desired by client     
            string nodeID = GetNodeErrorInOutPutFile(fileOutputRpt, out type, out update);

			bool changed = false;

            if (nodeID != string.Empty)
            {
                switch (type)
                {
                    case NodeTypes.basenode:
                        changed = DeleteBaseNode(nodeID);
                        break;

                    case NodeTypes.pipe:
                        if (_Pipes.Contains(nodeID))
                        {
                            changed = true;

                            if (update)
                            {                                
                                Pipe temp = (Pipe)_Pipes[nodeID];
                                temp.PipeLength = 0.1f;

                            }
                            else
                                _Pipes.Remove(nodeID);
                        }
                        break;

                    case NodeTypes.valve:
                        if (_Valves.Contains(nodeID))
                        {
                            _Valves.Remove(nodeID);
                            changed = true;
                        }
                        break;

                    case NodeTypes.control:
                        if (_Statuses.Contains(nodeID))
                        {                            
                            _Statuses.Remove(nodeID);
                        }
                        if (_Controls.Contains(nodeID))
                        {
                            _Controls.Remove(nodeID);
                            changed = true;
                        }
                        break;

                    case NodeTypes.controldetail:
                        changed = _Controls.DeleteDetail(nodeID);                        
                        break;

                    case NodeTypes.pump:
                        if (_Pumps.Contains(nodeID))
                        {
                            _Pumps.Remove(nodeID);
                            changed = true;
                        }
                        if (_Statuses.Contains(nodeID))
                        {                            
                            _Statuses.Remove(nodeID);
                            changed = true;
                        }
                        break;

                    default:
                        Console.WriteLine("No changes applied as a result of " + fileOutputRpt.Trim() + ". Deadlock");
                        break;
                }
            }
            else
                Console.WriteLine(fileOutputRpt.Trim() + " not found, repairs not applied; Deadlock");

            return changed;            
        }

        /// <summary>
        /// mimic original code -- refactor if time permits
        /// </summary>
        public bool OpenEpanet(string inputFile)
        {
            epa.Close();

            string inputFileString = inputFile;

            string[] result = inputFileString.Split('\\');
            result[result.Length - 1] = "OutputFileTest.rpt";

            string targetString = String.Join("\\", result);
            fileOutputRpt = targetString;

            string tempDir = System.IO.Path.GetTempPath();

            return (epa.Open(inputFile, targetString, tempDir + "\\TMP.bin"));         
        }

        /// <summary>
        /// Method that solves the active hydraulic system, generating a report 
        /// file, if there is a error during the report generation then return it
        /// </summary>
        public bool GenerateReport()
        {
            bool returnValue = true;

            try
            {
                epa.SolveHydraulic();

                // per original code
                if (epa.SaveHydraulic() == ErrorCode.Err104)
                    return false;

                epa.SaveReport();
            }
            catch (SystemException sysex)
            {
                returnValue = false;
                Console.WriteLine("Error in GenerateReport; err = " + sysex.Message);
            }

            return returnValue;
        }

        /// <summary>
        /// return the needed coordinate based upon the passed level
        /// </summary>
        /// <param name="curve"></param>
        /// <param name="initLevel"></param>
        /// <returns></returns>
        public CurveCoordinate[] FindCurve(Curve curve, float initLevel)
        {
            CurveCoordinate[] coord = null;

            foreach (DictionaryEntry de in curve.Coordinates)
            {
                CurveCoordinate maybe = (CurveCoordinate)de.Value;
                if (maybe.CurveX > initLevel)
                {
                    coord[1] = maybe;
                    break;
                }
                else
                {
                    if (coord == null)
                    {
                        coord = new CurveCoordinate[2];
                        coord[0] = null;
                        coord[1] = null;
                    }

                    coord[0] = maybe;
                }
            }

            return coord;
        }

        /// <summary>
        /// updates the water level in a tank. logic as delivered
        /// </summary>
        /// <param name="simulationStep"></param>
        public void UpdateTankLevel(int timeStep)
        {
            float demandValue;            
            float Area = 0.0f;
            float Volumen = 0.0f;
            float temp = 0.0f;

            try
            {
                int numberOfNodes = epa.NodeCount(CountType.Node);
                for (int counter = 1; counter <= numberOfNodes; counter++)
                {
                    demandValue = epa.GetNodeValue(counter, NodeValue.Demand);            

                    if (Math.Abs(demandValue) >= 1) // negative number - look into file diffs, etc as before.
                    {
                        // get the node type according to EPA Net
                        NodeType epaType = epa.GetNodeType(counter);

                        if (epaType == NodeType.Tank)
                        {
                            string nodeID = epa.GetNodeID(counter);    

                            // always make sure we have this tank in our collection
                            if (_Tanks.Contains(nodeID.Trim()))
                            {
                                // above -- demandValue = epa.GetNodeValue(counter, NodeValue.Demand);
                                Tank tank = (Tank)_Tanks[nodeID.Trim()];                              

                                if (tank.TankDiameter != 10.0f)
                                {
                                    temp = (tank.TankDiameter / 2);
                                    Area = (float)(Math.PI * temp * temp);
                                    temp = demandValue / Tank.TANKUPDATE;
                                    temp = temp * Tank.UPDATETIME;
                                    temp = temp * timeStep;
                                    Volumen = (Area * tank.TankInitLevel) + temp;

                                    if (Volumen <= (Area * tank.TankMinLevel))
                                    {
                                        Volumen = Area * tank.TankMinLevel;
                                    }
                                    else if (Volumen >= (Area * tank.TankMaxLevel))
                                    {
                                        Volumen = Area * tank.TankMaxLevel;
                                    }

                                    tank.TankInitLevel = Volumen / Area;

                                }
                                else
                                {
                                    if (_Curves.Contains(tank.CurveID))
                                    {
                                        Curve curveTank = (Curve)_Curves[tank.CurveID];

                                        CurveCoordinate[] coord = FindCurve(curveTank, tank.TankInitLevel);

                                        if (coord != null)
                                        {
                                            temp = (tank.TankInitLevel - coord[0].CurveX) / (coord[1].CurveX - coord[0].CurveX);
                                            Volumen = coord[0].CurveY + (temp * (coord[1].CurveY - coord[0].CurveY));
                                            Area = demandValue / Tank.TANKUPDATE;
                                            Area = Area * Tank.UPDATETIME;
                                            Area = Area * timeStep;
                                            Volumen = Volumen + Area;

                                            if (Volumen <= 0)
                                            {
                                                tank.TankInitLevel = tank.TankMinLevel;
                                            }
                                            else
                                            {
                                                if (coord[1].CurveY >= Volumen)
                                                {
                                                    temp = (Volumen - coord[0].CurveY) / (coord[1].CurveY - coord[0].CurveY);
                                                    tank.TankInitLevel = coord[0].CurveX + (temp * (coord[1].CurveX - coord[0].CurveX));
                                                }
                                                else if (coord[1].CurveX == 0 && coord[1].CurveY == 0)
                                                {
                                                    tank.TankInitLevel = tank.TankMaxLevel;
                                                }

                                                tank.TankInitLevel =
                                                    (tank.TankInitLevel < tank.TankMinLevel) ? tank.TankMinLevel :
                                                    (tank.TankInitLevel > tank.TankMaxLevel) ? tank.TankMaxLevel : tank.TankInitLevel;
                                            }
                                        }
                                    }
                                }
                            }
                            else
                                Console.WriteLine("Bad tank detected - node counter = " + counter);
                        }
                    }
                }
            }
            catch (SystemException sysex)
            {
                Console.WriteLine(sysex.Message);
            }
        }

        /// <summary>
        /// Calculate the serviceability.
        /// This gets interesting here. The Old system uses the entries from the original load
        /// of the system file -- regardless if the demand has been removed from the system. The
        /// "Erase" flag again. We need to do the same -- but we also need to output the demand data to the
        /// file we submit to EPANet. For this reason, maintain two Demands instances.
        /// </summary>
        /// <param name="timeStep"></param>
        public void CalculateServiceability(int timeStep)
        {
            try
            {
                // we need to check -- just in case
                if (_Serviceabilities0 == null) _Serviceabilities0 = new Serviceabilities();
                if (_Serviceabilities1 == null) _Serviceabilities1 = new Serviceabilities();

                // square up the values in the collection undergoing alterations and the 
                // original read in set
                foreach (DictionaryEntry de in _Demands)
                {
                    Demand demand = (Demand)de.Value;
                    _OriginalDemands[demand.ID] = demand;
                }

                foreach (DictionaryEntry de in _OriginalDemands)
                {
                    Demand demand = (Demand)de.Value;

                    // update the serviceability
                    Serviceability serv = new Serviceability();
                    serv.ID = demand.ID;

                    // if the demand has been deleted -- do not include its demand in the service value
                    if (_Demands.Contains(demand.ID))
                        if (demand.DemandBase == 0) serv.Value = 0.ToString().Trim(); else serv.Value = demand.DemandBaseTemp.ToString().Trim();
                    else
                        serv.Value = 0.ToString().Trim();

                    if (demand.DemandBase == 0) serv.Mean = 0; else serv.Mean = demand.DemandBaseTemp;
                    serv.DemandBase = demand.DemandBaseTemp;

                    // we will be looking for this value, time step
                    serv.TimeStep = timeStep;

                    AddServiceability(serv, timeStep);
                }
            }
            catch (SystemException sysex)
            {
                Console.WriteLine(sysex.Message + " " + sysex.StackTrace);
            }
        }

        /// <summary>
        /// This routine is confusing -- but really all that is happening is the construction of display
        /// strings and summing mean values; logic as delivered
        /// </summary>
        /// <param name="serv"></param>
        /// <param name="timeStep"></param>
        public void AddServiceability(Serviceability serv, int timeStep)
        {
            if (timeStep == 0)
            {
                // if the instance exists -- update else add
                if (_Serviceabilities0.Contains(serv.ID))
                {
                    ((Serviceability)(_Serviceabilities0[serv.ID])).Value += " " + serv.Value.ToString().Trim();                    
                    Console.WriteLine("serv ID = " + serv.ID + " has " + (((Serviceability)(_Serviceabilities0[serv.ID])).Value.Split(' ').Length.ToString() + " elements"));
                    ((Serviceability)(_Serviceabilities0[serv.ID])).Mean += serv.Mean;
                }
                else
                    _Serviceabilities0.Add(serv.ID, serv);
            }
            else
            {
                // if the instance exists -- update else add
                if (_Serviceabilities1.Contains(serv.ID))
                {
                    ((Serviceability)(_Serviceabilities1[serv.ID])).Value += " " + serv.Value.ToString().Trim();
                    ((Serviceability)(_Serviceabilities1[serv.ID])).Mean += serv.Mean;
                }
                else
                    _Serviceabilities1.Add(serv.ID, serv);
            }
        }

        /// <summary>
        /// clear all used collections - start fresh
        /// </summary>
        public void InitCollections()
        {
            _Junctions.Clear();
            _Reservoirs.Clear();
            _Pipes.Clear();
            _Controls.Clear();
            _Curves.Clear();
            _Demands.Clear();
            _OriginalDemands.Clear();
            _Patterns.Clear();
            _Pumps.Clear();
            _Statuses.Clear();
            _Tanks.Clear();
            _Valves.Clear();
            _Sources.Clear();
            _Qualities.Clear();
            _Reactions.Clear();
            _Options.Clear();
            _ReportOptions.Clear();
            _Coordinates.Clear();
            _Vertices.Clear();
            _RepairRates.Clear();
            _NodePressures.Clear();
            _LeakRatios.Clear();
            _PipeBreaks.Clear();
            _PipeLeaks.Clear();
            _Reactions.Clear();
            //_Serviceabilities0.Clear();
            //_Serviceabilities1.Clear();
            _SumDemands.Clear();
        }

        /// <summary>
        /// read from a file the pipe and leak damage
        /// </summary>
        public void ReadPipeDamageFile()
        {
            string fileBody = File.ReadAllText(this.SimulationData.PipeDamageFile);

            string[] sections = fileBody.Split(Node.NODE_TYPE_SEPARATOR_1.ToCharArray());

            foreach (string section in sections)
            {
                if (section != string.Empty)
                {
                    string[] sectionAndBody = section.Split(Node.NODE_TYPE_SEPARATOR_2.ToCharArray());
                    if (sectionAndBody.Length > 1)
                    {
                        if (sectionAndBody[0].ToUpper() == PipeBreak.FROM_FILE_ID.ToUpper())
                            _PipeBreaks.Load(sectionAndBody[1]);

                        else if (sectionAndBody[0].ToUpper() == PipeLeak.FROM_FILE_ID.ToUpper())
                            _PipeLeaks.Load(sectionAndBody[1]);

                        else
                            throw new SystemException("Invalid pipe damage file");
                    }
                }
            }  
        }

        /// <summary>
        /// algorithm identical to original
        /// </summary>
        /// <param name="times"></param>
        /// <param name="timeStep"></param>
        /// <returns></returns>
        public ConvergeCriteria CalculateOverall(int times, int timeStep)
        {	        
            OrderedDictionary standardDeviations = new OrderedDictionary();
            Serviceabilities temp = null;
            int deviationsAdded = 0;
            _SumDemands.Clear();

            if(timeStep == 0)            
                temp = this._Serviceabilities0;
            else
                temp = this._Serviceabilities1;
            
            string TempDemand, PrintDemand = string.Empty;

            ConvergeCriteria myCriteria = new ConvergeCriteria();
            
            float SumAvgTem = 0, DemandDis = 0, AvgDemand = 0;
            
            foreach(DictionaryEntry de in temp)
            {
                Serviceability sde = (Serviceability)de.Value;
                TempDemand = sde.Value;
                string[] demValues = TempDemand.Split(' ');

                SumAvgTem += sde.DemandBase;

                for(int j = 1; j <= times; j++)
                {
                    SumDemand demand = new SumDemand();                    
                    demand.ID = j.ToString();                                       
                    demand.value = demValues[j - 1];
                    AddSumDemand(demand);
                }

                TempDemand = "";
            }
                        
            foreach(DictionaryEntry rde in _SumDemands)
            {
                StandardDeviation standard = new StandardDeviation();
                SumDemand rsum = (SumDemand)rde.Value;

                string[] sumValues = rsum.value.Split(' ');
                for(int k = 0; k < sumValues.Length; k++)
                {
                    DemandDis += Convert.ToSingle(sumValues[k]);
                }
		                       
                if (SumAvgTem != 0)
                {
                    AvgDemand += (DemandDis / SumAvgTem);
                    standard.ValueStandard = DemandDis / SumAvgTem;
                }
                else
                    standard.ValueStandard = 0; // added this -- do not cause ignored exceptions		       
                    //catch(System::DivideByZeroException ^ e){
                    //    fSimGlobal->WriteErrorLog("Divide by Zero -- Random ");		       

                DemandDis = 0.0f;
                deviationsAdded++;
                standardDeviations.Add(deviationsAdded, standard);
            }

            try
            {
                DemandDis = AvgDemand / (float)times;
                SumAvgTem = 0.0f;

                myCriteria.Mean = DemandDis;

                foreach (DictionaryEntry de in standardDeviations)
                {
                    StandardDeviation sde = (StandardDeviation)de.Value;
                    SumAvgTem += Convert.ToSingle(Math.Pow((sde.ValueStandard - DemandDis), 2));
                }

                if (times != 1)
                    SumAvgTem = Convert.ToSingle(Math.Sqrt((SumAvgTem / (times - 1.0f))));
                else
                    SumAvgTem = 0;

                if (DemandDis != 0)
                    myCriteria.COV = SumAvgTem / DemandDis;
                else
                    myCriteria.COV = 0;
            }
            catch (SystemException sysex)
            {
                Console.WriteLine("CalculateOverall - " + sysex.Message);
            }

            _SumDemands.Clear();

            return(myCriteria);
        
        } // end of CalculateOverall

#endregion
    }
}
    
