﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Giraffe;
using System.IO;

namespace SystemFileVerifier
{
    public partial class Verify : Form
    {
        SimulationEngine masterSim = new SimulationEngine();
        SimulationEngine compareSim = new SimulationEngine();
        
        public Verify()
        {
            InitializeComponent();
        }

        private void btnBrowseMaster_Click(object sender, EventArgs e)
        {
            openFile.ShowDialog();
            this.txtMaster.Text = openFile.FileName;
        }

        private void btnBrowseCompare_Click(object sender, EventArgs e)
        {
            openFile.ShowDialog();
            this.txtCompare.Text = openFile.FileName;
        }

        private void btnVerify_Click(object sender, EventArgs e)
        {
            CreateNodeCollections(txtMaster.Text, masterSim);
            CreateNodeCollections(txtCompare.Text, compareSim);
            txtDiff.Text = string.Empty;
            txtMasterOnly.Text = string.Empty;
            txtCompareOnly.Text = string.Empty;

            CompareJunctions();
            CompareReservoirs();
            CompareTanks();
            ComparePipes();
            ComparePumps();
            CompareValves();
            CompareDemands();
            CompareCurves();
            ComparePatterns();
            CompareStatus();
            CompareControls();
            CompareReactions();
            CompareOptions();
            CompareReport();
            CompareCoordinates();
            CompareVertices();
            LineByLineBaby();
        }

        /// <summary>
        /// load the various hash tables with their appropriate node sub-classes
        /// </summary>
        /// <param name="fileName"></param>
        private void CreateNodeCollections(string fileName, SimulationEngine sim)
        {
            try
            {
                string fileBody = File.ReadAllText(fileName);

                string[] sections = fileBody.Split(Node.NODE_TYPE_SEPARATOR_1.ToCharArray());

                foreach (string section in sections)
                {
                    if (section != string.Empty)
                    {
                        string[] sectionAndBody = section.Split(Node.NODE_TYPE_SEPARATOR_2.ToCharArray());
                        if (sectionAndBody.Length > 1)
                        {
                            sim.FillInCorrectHashTable(sectionAndBody[0], sectionAndBody[1]);
                        }
                    }
                }
            }
            catch (SystemException sysex)
            {

            }
        }

        public void CompareJunctions()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("{0}{1}", "[Junctions]", Environment.NewLine);
            StringBuilder builder2 = new StringBuilder();
            builder2.AppendFormat("{0}{1}", "[Junctions]", Environment.NewLine);
            StringBuilder builder3 = new StringBuilder();
            builder3.AppendFormat("{0}{1}", "[Junctions]", Environment.NewLine);            

            // look for master only
            foreach (DictionaryEntry de in masterSim._Junctions)
            {
                Junction j = (Junction)de.Value;

                if (!(compareSim._Junctions.Contains(de.Key)))
                {

                    builder.AppendFormat("{0}{1}", j.ToString(), Environment.NewLine);
                }
                else
                {                                        
                    Junction c = (Junction)compareSim._Junctions[j.ID];

                    if ((c.ID != j.ID) ||
                        (c.JunctionElevation != j.JunctionElevation))
                    {
                        builder2.AppendFormat("{0}{1}{2}", "Master - ", j.ToString().Trim(), Environment.NewLine);
                        builder2.AppendFormat("{0}{1}{2}{3}", "Compare - ", c.ToString().Trim(), Environment.NewLine, Environment.NewLine);
                    }                        
                }
            }

            // look for compare only           
            foreach (DictionaryEntry de in compareSim._Junctions)
            {
                Junction j = (Junction)de.Value;

                if (!(masterSim._Junctions.Contains(de.Key)))
                {

                    builder3.AppendFormat("{0}{1}", j.ToString(), Environment.NewLine);
                }                
            }

            builder.Append(Environment.NewLine);
            builder2.Append(Environment.NewLine);
            builder3.Append(Environment.NewLine);

            txtDiff.Text = builder2.ToString();
            txtMasterOnly.Text = builder.ToString();
            txtCompareOnly.Text = builder3.ToString();
        }

        public void CompareReservoirs()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("{0}{1}", "[Reservoirs]", Environment.NewLine);
            StringBuilder builder2 = new StringBuilder();
            builder2.AppendFormat("{0}{1}", "[Reservoirs]", Environment.NewLine);
            StringBuilder builder3 = new StringBuilder();
            builder3.AppendFormat("{0}{1}", "[Reservoirs]", Environment.NewLine);

            // look for master only
            foreach (DictionaryEntry de in masterSim._Reservoirs)
            {
                Reservoir r = (Reservoir)de.Value;

                if (!(compareSim._Reservoirs.Contains(de.Key)))
                {

                    builder.AppendFormat("{0}{1}", r.ToString(), Environment.NewLine);
                }
                else
                {
                    Reservoir c = (Reservoir)compareSim._Reservoirs[r.ID];

                    if ((c.ID != r.ID) ||
                        (c.Pattern != r.Pattern) ||
                        (c.ReservoirTotalHead != r.ReservoirTotalHead))
                    {
                        builder2.AppendFormat("{0}{1}{2}", "Master - ", r.ToString().Trim(), Environment.NewLine);
                        builder2.AppendFormat("{0}{1}{2}{3}", "Compare - ", c.ToString().Trim(), Environment.NewLine, Environment.NewLine);
                    }
                }
            }

            // look for compare only           
            foreach (DictionaryEntry de in compareSim._Reservoirs)
            {
                Reservoir j = (Reservoir)de.Value;

                if (!(masterSim._Reservoirs.Contains(de.Key)))
                {

                    builder3.AppendFormat("{0}{1}", j.ToString(), Environment.NewLine);
                }
            }

            builder.Append(Environment.NewLine);
            builder2.Append(Environment.NewLine);
            builder3.Append(Environment.NewLine);

            txtDiff.Text += builder2.ToString();
            txtMasterOnly.Text += builder.ToString();
            txtCompareOnly.Text += builder3.ToString();
        }

        public void CompareTanks()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("{0}{1}", "[Tanks]", Environment.NewLine);
            StringBuilder builder2 = new StringBuilder();
            builder2.AppendFormat("{0}{1}", "[Tanks]", Environment.NewLine);
            StringBuilder builder3 = new StringBuilder();
            builder3.AppendFormat("{0}{1}", "[Tanks]", Environment.NewLine);

            // look for master only
            foreach (DictionaryEntry de in masterSim._Tanks)
            {
                Tank t = (Tank)de.Value;

                if (!(compareSim._Tanks.Contains(de.Key)))
                {

                    builder.AppendFormat("{0}{1}", t.ToString(), Environment.NewLine);
                }
                else
                {
                    Tank c = (Tank)compareSim._Tanks[t.ID];

                    if ((c.ID != t.ID) ||
                        (c.CurveID != t.CurveID) ||
                        (c.TankDiameter != t.TankDiameter) ||
                        (c.TankElevation != t.TankElevation) ||
                        (c.TankInitLevel != t.TankInitLevel) ||
                        (c.TankMaxLevel != t.TankMaxLevel) ||
                        (c.TankMinLevel != t.TankMinLevel) ||
                        (c.TankMinVolume != t.TankMinVolume))
                    {
                        builder2.AppendFormat("{0}{1}{2}", "Master - ", t.ToString().Trim(), Environment.NewLine);
                        builder2.AppendFormat("{0}{1}{2}{3}", "Compare - ", c.ToString().Trim(), Environment.NewLine, Environment.NewLine);
                    }
                }
            }

            // look for compare only           
            foreach (DictionaryEntry de in compareSim._Tanks)
            {
                Tank t = (Tank)de.Value;

                if (!(masterSim._Tanks.Contains(de.Key)))
                {

                    builder3.AppendFormat("{0}{1}", t.ToString(), Environment.NewLine);
                }
            }

            builder.Append(Environment.NewLine);
            builder2.Append(Environment.NewLine);
            builder3.Append(Environment.NewLine);

            txtDiff.Text += builder2.ToString();
            txtMasterOnly.Text += builder.ToString();
            txtCompareOnly.Text += builder3.ToString();
        }

        public void ComparePipes()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("{0}{1}", "[Pipes]", Environment.NewLine);
            StringBuilder builder2 = new StringBuilder();
            builder2.AppendFormat("{0}{1}", "[Pipes]", Environment.NewLine);
            StringBuilder builder3 = new StringBuilder();
            builder3.AppendFormat("{0}{1}", "[Pipes]", Environment.NewLine);

            // look for master only
            foreach (DictionaryEntry de in masterSim._Pipes)
            {
                Pipe p = (Pipe)de.Value;

                if (!(compareSim._Pipes.Contains(de.Key)))
                {

                    builder.AppendFormat("{0}{1}", p.ToString(), Environment.NewLine);
                }
                else
                {
                    Pipe c = (Pipe)compareSim._Pipes[p.ID];

                    if ((c.ID != p.ID) ||
                        (c.EndNode != p.EndNode) ||
                        (c.PipeDiameter != p.PipeDiameter) ||
                        (c.PipeExtra != p.PipeExtra) ||
                        (c.PipeLength != p.PipeLength) ||
                        (c.PipeLossCoeff != p.PipeLossCoeff) ||
                        (c.PipeRoughness != p.PipeRoughness) ||
                        (c.StartNode != p.StartNode))
                    {
                        builder2.AppendFormat("{0}{1}{2}", "Master - ", p.ToString().Trim(), Environment.NewLine);
                        builder2.AppendFormat("{0}{1}{2}{3}", "Compare - ", c.ToString().Trim(), Environment.NewLine, Environment.NewLine);
                    }
                }
            }

            // look for compare only           
            foreach (DictionaryEntry de in compareSim._Pipes)
            {
                Pipe p = (Pipe)de.Value;

                if (!(masterSim._Pipes.Contains(de.Key)))
                {

                    builder3.AppendFormat("{0}{1}", p.ToString(), Environment.NewLine);
                }
            }

            builder.Append(Environment.NewLine);
            builder2.Append(Environment.NewLine);
            builder3.Append(Environment.NewLine);

            txtDiff.Text += builder2.ToString();
            txtMasterOnly.Text += builder.ToString();
            txtCompareOnly.Text += builder3.ToString();
        }

        public void ComparePumps()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("{0}{1}", "[Pumps]", Environment.NewLine);
            StringBuilder builder2 = new StringBuilder();
            builder2.AppendFormat("{0}{1}", "[Pumps]", Environment.NewLine);
            StringBuilder builder3 = new StringBuilder();
            builder3.AppendFormat("{0}{1}", "[Pumps]", Environment.NewLine);

            // look for master only
            foreach (DictionaryEntry de in masterSim._Pumps)
            {
                Pump p = (Pump)de.Value;

                if (!(compareSim._Pumps.Contains(de.Key)))
                {

                    builder.AppendFormat("{0}{1}", p.ToString(), Environment.NewLine);
                }
                else
                {
                    Pump c = (Pump)compareSim._Pumps[p.ID];

                    if ((c.ID != p.ID) ||
                        (c.EndNode != p.EndNode) ||
                        (c.PumpPower != p.PumpPower) ||
                        (c.StartNode != p.StartNode))
                    {
                        builder2.AppendFormat("{0}{1}{2}", "Master - ", p.ToString().Trim(), Environment.NewLine);
                        builder2.AppendFormat("{0}{1}{2}{3}", "Compare - ", c.ToString().Trim(), Environment.NewLine, Environment.NewLine);
                    }
                }
            }

            // look for compare only           
            foreach (DictionaryEntry de in compareSim._Pumps)
            {
                Pump p = (Pump)de.Value;

                if (!(masterSim._Pumps.Contains(de.Key)))
                {

                    builder3.AppendFormat("{0}{1}", p.ToString(), Environment.NewLine);
                }
            }

            builder.Append(Environment.NewLine);
            builder2.Append(Environment.NewLine);
            builder3.Append(Environment.NewLine);

            txtDiff.Text += builder2.ToString();
            txtMasterOnly.Text += builder.ToString();
            txtCompareOnly.Text += builder3.ToString();
        }

        public void CompareValves()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("{0}{1}", "[Valves]", Environment.NewLine);
            StringBuilder builder2 = new StringBuilder();
            builder2.AppendFormat("{0}{1}", "[Valves]", Environment.NewLine);
            StringBuilder builder3 = new StringBuilder();
            builder3.AppendFormat("{0}{1}", "[Valves]", Environment.NewLine);

            // look for master only
            foreach (DictionaryEntry de in masterSim._Valves)
            {
                Valve v = (Valve)de.Value;

                if (!(compareSim._Valves.Contains(de.Key)))
                {

                    builder.AppendFormat("{0}{1}", v.ToString(), Environment.NewLine);
                }
                else
                {
                    Valve c = (Valve)compareSim._Valves[v.ID];

                    if ((c.ID != v.ID) ||
                        (c.EndNode != v.EndNode) ||
                        (c.HeadLossCurveID != v.HeadLossCurveID) ||                       
                        (c.StartNode != v.StartNode) ||
                        (c.ValveDiameter != v.ValveDiameter) ||
                        (c.ValveLossCoeff != v.ValveLossCoeff) ||
                        (c.ValveSetting != v.ValveSetting) ||
                        (c.ValveType != v.ValveType))
                    {
                        builder2.AppendFormat("{0}{1}{2}", "Master - ", v.ToString().Trim(), Environment.NewLine);
                        builder2.AppendFormat("{0}{1}{2}{3}", "Compare - ", c.ToString().Trim(), Environment.NewLine, Environment.NewLine);
                    }
                }
            }

            // look for compare only           
            foreach (DictionaryEntry de in compareSim._Valves)
            {
                Valve v = (Valve)de.Value;

                if (!(masterSim._Valves.Contains(de.Key)))
                {

                    builder3.AppendFormat("{0}{1}", v.ToString(), Environment.NewLine);
                }
            }

            builder.Append(Environment.NewLine);
            builder2.Append(Environment.NewLine);
            builder3.Append(Environment.NewLine);

            txtDiff.Text += builder2.ToString();
            txtMasterOnly.Text += builder.ToString();
            txtCompareOnly.Text += builder3.ToString();
        }

        public void CompareDemands()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("{0}{1}", "[Demands]", Environment.NewLine);
            StringBuilder builder2 = new StringBuilder();
            builder2.AppendFormat("{0}{1}", "[Demands]", Environment.NewLine);
            StringBuilder builder3 = new StringBuilder();
            builder3.AppendFormat("{0}{1}", "[Demands]", Environment.NewLine);

            // look for master only
            foreach (DictionaryEntry de in masterSim._Demands)
            {
                Demand d = (Demand)de.Value;

                if (!(compareSim._Demands.Contains(de.Key)))
                {

                    builder.AppendFormat("{0}{1}", d.ToString(), Environment.NewLine);
                }
                else
                {
                    Demand c = (Demand)compareSim._Demands[d.ID];

                    if ((c.ID != d.ID) ||
                        (c.DemandBase != d.DemandBase) ||
                        (c.DemandBaseTemp != d.DemandBaseTemp) ||
                        (c.DemandCategory != d.DemandCategory) ||
                        (c.DemandPattern != d.DemandPattern))
                    {
                        builder2.AppendFormat("{0}{1}{2}", "Master - ", d.ToString().Trim(), Environment.NewLine);
                        builder2.AppendFormat("{0}{1}{2}{3}", "Compare - ", c.ToString().Trim(), Environment.NewLine, Environment.NewLine);
                    }
                }
            }

            // look for compare only           
            foreach (DictionaryEntry de in compareSim._Demands)
            {
                Demand d = (Demand)de.Value;

                if (!(masterSim._Demands.Contains(de.Key)))
                {

                    builder3.AppendFormat("{0}{1}", d.ToString(), Environment.NewLine);
                }
            }

            builder.Append(Environment.NewLine);
            builder2.Append(Environment.NewLine);
            builder3.Append(Environment.NewLine);

            txtDiff.Text += builder2.ToString();
            txtMasterOnly.Text += builder.ToString();
            txtCompareOnly.Text += builder3.ToString();
        }

        public void CompareCurves()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("{0}{1}", "[Curves]", Environment.NewLine);
            StringBuilder builder2 = new StringBuilder();
            builder2.AppendFormat("{0}{1}", "[Curves]", Environment.NewLine);
            StringBuilder builder3 = new StringBuilder();
            builder3.AppendFormat("{0}{1}", "[Curves]", Environment.NewLine);

            // look for master only
            foreach (DictionaryEntry de in masterSim._Curves)
            {
                Curve c = (Curve)de.Value;

                if (!(compareSim._Curves.Contains(de.Key)))
                {

                    builder.AppendFormat("{0}{1}", c.ToString(), Environment.NewLine);
                }
                else
                {
                    Curve c2 = (Curve)compareSim._Curves[c.ID];

                    if ((c2.ID != c.ID) || (c2.Coordinates.Count != c.Coordinates.Count))
                    {
                        builder2.AppendFormat("{0}; count = {1}{2}{3}{4}", "Master - ", c.Coordinates.Count, Environment.NewLine, c.ToString().Trim(), Environment.NewLine);
                        builder2.AppendFormat("{0}; count = {1}{2}{3}{4}", "Compare - ", c2.Coordinates.Count, Environment.NewLine, c2.ToString().Trim(), Environment.NewLine);
                    }                        
                }
            }

            // look for compare only           
            foreach (DictionaryEntry de in compareSim._Curves)
            {
                Curve c = (Curve)de.Value;

                if (!(masterSim._Curves.Contains(de.Key)))
                {

                    builder3.AppendFormat("{0}{1}", c.ToString(), Environment.NewLine);
                }
            }

            builder.Append(Environment.NewLine);
            builder2.Append(Environment.NewLine);
            builder3.Append(Environment.NewLine);

            txtDiff.Text += builder2.ToString();
            txtMasterOnly.Text += builder.ToString();
            txtCompareOnly.Text += builder3.ToString();
        }

        public void CompareStatus()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("{0}{1}", "[Status]", Environment.NewLine);
            StringBuilder builder2 = new StringBuilder();
            builder2.AppendFormat("{0}{1}", "[Status]", Environment.NewLine);
            StringBuilder builder3 = new StringBuilder();
            builder3.AppendFormat("{0}{1}", "[Status]", Environment.NewLine);

            // look for master only
            foreach (DictionaryEntry de in masterSim._Statuses)
            {
                Status s = (Status)de.Value;

                if (!(compareSim._Statuses.Contains(de.Key)))
                {

                    builder.AppendFormat("{0}{1}", s.ToString(), Environment.NewLine);
                }
                else
                {
                    Status c = (Status)compareSim._Statuses[s.ID];

                    if ((c.ID != s.ID) ||
                        (c.StatusOption != s.StatusOption))
                    {
                        builder2.AppendFormat("{0}{1}{2}", "Master - ", s.ToString().Trim(), Environment.NewLine);
                        builder2.AppendFormat("{0}{1}{2}{3}", "Compare - ", c.ToString().Trim(), Environment.NewLine, Environment.NewLine);
                    }
                }
            }

            // look for compare only           
            foreach (DictionaryEntry de in compareSim._Statuses)
            {
                Status s = (Status)de.Value;

                if (!(masterSim._Statuses.Contains(de.Key)))
                {

                    builder3.AppendFormat("{0}{1}", s.ToString(), Environment.NewLine);
                }
            }

            builder.Append(Environment.NewLine);
            builder2.Append(Environment.NewLine);
            builder3.Append(Environment.NewLine);

            txtDiff.Text += builder2.ToString();
            txtMasterOnly.Text += builder.ToString();
            txtCompareOnly.Text += builder3.ToString();
        }

        public void CompareControls()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("{0}{1}", "[Controls]", Environment.NewLine);
            StringBuilder builder2 = new StringBuilder();
            builder2.AppendFormat("{0}{1}", "[Controls]", Environment.NewLine);
            StringBuilder builder3 = new StringBuilder();
            builder3.AppendFormat("{0}{1}", "[Controls]", Environment.NewLine);

            // look for master only
            foreach (DictionaryEntry de in masterSim._Controls)
            {
                Giraffe.Control c = (Giraffe.Control)de.Value;

                if (!(compareSim._Controls.Contains(de.Key)))
                {

                    builder.AppendFormat("{0}{1}", c.ToString(), Environment.NewLine);
                }
                else
                {
                    Giraffe.Control c2 = (Giraffe.Control)compareSim._Controls[c.ID];

                    if ((c2.ID != c.ID) || (c2.Details.Count != c.Details.Count))
                    {
                        builder2.AppendFormat("{0}; count = {1}{2}{3}{4}", "Master - ", c.Details.Count, Environment.NewLine, c.ToString().Trim(), Environment.NewLine);
                        builder2.AppendFormat("{0}; count = {1}{2}{3}{4}", "Compare - ", c2.Details.Count, Environment.NewLine, c2.ToString().Trim(), Environment.NewLine);
                    }
                }
            }

            // look for compare only           
            foreach (DictionaryEntry de in compareSim._Controls)
            {
                Giraffe.Control c = (Giraffe.Control)de.Value;

                if (!(masterSim._Controls.Contains(de.Key)))
                {

                    builder3.AppendFormat("{0}{1}", c.ToString(), Environment.NewLine);
                }
            }

            builder.Append(Environment.NewLine);
            builder2.Append(Environment.NewLine);
            builder3.Append(Environment.NewLine);

            txtDiff.Text += builder2.ToString();
            txtMasterOnly.Text += builder.ToString();
            txtCompareOnly.Text += builder3.ToString();
        }

        public void ComparePatterns()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("{0}{1}", "[Patterns]", Environment.NewLine);
            StringBuilder builder2 = new StringBuilder();
            builder2.AppendFormat("{0}{1}", "[Patterns]", Environment.NewLine);
            StringBuilder builder3 = new StringBuilder();
            builder3.AppendFormat("{0}{1}", "[Patterns]", Environment.NewLine);

            // look for master only
            foreach (DictionaryEntry de in masterSim._Patterns)
            {
                Pattern p = (Pattern)de.Value;

                if (!(compareSim._Patterns.Contains(de.Key)))
                {

                    builder.AppendFormat("{0}{1}", p.ToString(), Environment.NewLine);
                }
                else
                {
                    Pattern c = (Pattern)compareSim._Patterns[p.ID];

                    if ((c.ID != p.ID) || (c.Information.Count != p.Information.Count))
                    {
                        builder2.AppendFormat("{0}; count = {1}{2}{3}{4}", "Master - ", p.Information.Count, Environment.NewLine, p.ToString().Trim(), Environment.NewLine);
                        builder2.AppendFormat("{0}; count = {1}{2}{3}{4}", "Compare - ", c.Information.Count, Environment.NewLine, c.ToString().Trim(), Environment.NewLine);
                    }
                    else
                    {
                        // even is the count is correct - make sure the contents are
                        for (int count = 0; count < c.Information.Count; count++)
                        {
                            string ci = (string)(c.Information[count]);
                            string pi = (string)(p.Information[count]);
                            if (ci != pi)
                            {
                                builder2.AppendFormat("{0}{1}{2}", "Master - ", p.ToString().Trim(), Environment.NewLine);
                                builder2.AppendFormat("{0}{1}{2}", "Compare - ", c.ToString().Trim(), Environment.NewLine);
                                break;
                            }
                        }
                    }
                }
            }

            // look for compare only           
            foreach (DictionaryEntry de in compareSim._Patterns)
            {
                Pattern c = (Pattern)de.Value;

                if (!(masterSim._Patterns.Contains(de.Key)))
                {
                    builder3.AppendFormat("{0}{1}", c.ToString(), Environment.NewLine);
                }
            }

            builder.Append(Environment.NewLine);
            builder2.Append(Environment.NewLine);
            builder3.Append(Environment.NewLine);

            txtDiff.Text += builder2.ToString();
            txtMasterOnly.Text += builder.ToString();
            txtCompareOnly.Text += builder3.ToString();
        }

        public void CompareReactions()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("{0}{1}", "[Reactions]", Environment.NewLine);
            StringBuilder builder2 = new StringBuilder();
            builder2.AppendFormat("{0}{1}", "[Reactions]", Environment.NewLine);
            StringBuilder builder3 = new StringBuilder();
            builder3.AppendFormat("{0}{1}", "[Reactions]", Environment.NewLine);

            // look for master only
            foreach (DictionaryEntry de in masterSim._Reactions)
            {
                Reaction r = (Reaction)de.Value;

                if (!(compareSim._Reactions.Contains(de.Key)))
                {

                    builder.AppendFormat("{0}{1}", r.ToString(), Environment.NewLine);
                }
                else
                {
                    Reaction c = (Reaction)compareSim._Reactions[de.Key];

                    if ((c.ID != r.ID) ||
                        (c.Overrides != r.Overrides) ||
                        (c.ReactionCoefficient != r.ReactionCoefficient) ||
                        (c.SpecificNodeBeingOveridden != r.SpecificNodeBeingOveridden))                        
                    {
                        builder2.AppendFormat("{0}{1}{2}", "Master - ", r.ToString().Trim(), Environment.NewLine);
                        builder2.AppendFormat("{0}{1}{2}{3}", "Compare - ", c.ToString().Trim(), Environment.NewLine, Environment.NewLine);
                    }
                }
            }

            // look for compare only           
            foreach (DictionaryEntry de in compareSim._Reactions)
            {
                Reaction r = (Reaction)de.Value;

                if (!(masterSim._Reactions.Contains(de.Key)))
                {

                    builder3.AppendFormat("{0}{1}", r.ToString(), Environment.NewLine);
                }
            }

            builder.Append(Environment.NewLine);
            builder2.Append(Environment.NewLine);
            builder3.Append(Environment.NewLine);

            txtDiff.Text += builder2.ToString();
            txtMasterOnly.Text += builder.ToString();
            txtCompareOnly.Text += builder3.ToString();
        }

        public void CompareOptions()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("{0}{1}", "[Options]", Environment.NewLine);
            StringBuilder builder2 = new StringBuilder();
            builder2.AppendFormat("{0}{1}", "[Options]", Environment.NewLine);
            StringBuilder builder3 = new StringBuilder();
            builder3.AppendFormat("{0}{1}", "[Options]", Environment.NewLine);

            if (masterSim._Options.Count != compareSim._Options.Count)
            {
                builder2.AppendFormat("{0}; count = {1}{2}{3}{4}", "Master - ", masterSim._Options.Count, Environment.NewLine, masterSim._Options.ToString().Trim(), Environment.NewLine);
                builder2.AppendFormat("{0}; count = {1}{2}{3}{4}", "Compare - ", compareSim._Options.Count, Environment.NewLine, compareSim._Options.ToString().Trim(), Environment.NewLine);
            }
            else
            {
                // just go line by line
                for (int count = 0; count < masterSim._Options.Count; count++)
                {
                    Option o = (Option)masterSim._Options[count];
                    Option c = (Option)compareSim._Options[count];                   
                    
                    if ((c.ID != o.ID) ||
                        (c.OptionDoubleValue != o.OptionDoubleValue) ||
                        (c.OptionValue != o.OptionValue))
                    {
                        builder2.AppendFormat("{0}{1}{2}", "Master - ", o.ToString().Trim(), Environment.NewLine);
                        builder2.AppendFormat("{0}{1}{2}{3}", "Compare - ", c.ToString().Trim(), Environment.NewLine, Environment.NewLine);
                    }                    
                }
            }            

            builder.Append(Environment.NewLine);
            builder2.Append(Environment.NewLine);
            builder3.Append(Environment.NewLine);

            txtDiff.Text += builder2.ToString();
            txtMasterOnly.Text += builder.ToString();
            txtCompareOnly.Text += builder3.ToString();
        }

        public void CompareReport()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("{0}{1}", "[Report]", Environment.NewLine);
            StringBuilder builder2 = new StringBuilder();
            builder2.AppendFormat("{0}{1}", "[Report]", Environment.NewLine);
            StringBuilder builder3 = new StringBuilder();
            builder3.AppendFormat("{0}{1}", "[Report]", Environment.NewLine);

            if (masterSim._ReportOptions.Count != compareSim._ReportOptions.Count)
            {
                builder2.AppendFormat("{0}; count = {1}{2}{3}{4}", "Master - ", masterSim._ReportOptions.Count, Environment.NewLine, masterSim._ReportOptions.ToString().Trim(), Environment.NewLine);
                builder2.AppendFormat("{0}; count = {1}{2}{3}{4}", "Compare - ", compareSim._ReportOptions.Count, Environment.NewLine, compareSim._ReportOptions.ToString().Trim(), Environment.NewLine);
            }
            else
            {
                // just go line by line
                for (int count = 0; count < masterSim._ReportOptions.Count; count++)
                {
                    ReportOption r = (ReportOption)masterSim._ReportOptions[count];
                    ReportOption c = (ReportOption)compareSim._ReportOptions[count];

                    if ((c.ID != r.ID) ||
                        (c.FileName != r.FileName) ||
                        (c.GenerateHydraulicStatus != r.GenerateHydraulicStatus) ||
                        (c.GenerateSummaryTable != r.GenerateSummaryTable) ||
                        (c.Links.Count != r.Links.Count) ||
                        (c.Nodes.Count != r.Nodes.Count))
                    {
                        builder2.AppendFormat("{0}{1}{2}", "Master - ", r.ToString().Trim(), Environment.NewLine);
                        builder2.AppendFormat("{0}{1}{2}{3}", "Compare - ", c.ToString().Trim(), Environment.NewLine, Environment.NewLine);
                    }
                }
            }

            builder.Append(Environment.NewLine);
            builder2.Append(Environment.NewLine);
            builder3.Append(Environment.NewLine);

            txtDiff.Text += builder2.ToString();
            txtMasterOnly.Text += builder.ToString();
            txtCompareOnly.Text += builder3.ToString();
        }

        public void CompareCoordinates()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("{0}{1}", "[Coordinates]", Environment.NewLine);
            StringBuilder builder2 = new StringBuilder();
            builder2.AppendFormat("{0}{1}", "[Coordinates]", Environment.NewLine);
            StringBuilder builder3 = new StringBuilder();
            builder3.AppendFormat("{0}{1}", "[Coordinates]", Environment.NewLine);

            // look for master only
            foreach (DictionaryEntry de in masterSim._Coordinates)
            {
                Coordinate co = (Coordinate)de.Value;

                if (!(compareSim._Coordinates.Contains(de.Key)))
                {

                    builder.AppendFormat("{0}{1}", co.ToString(), Environment.NewLine);
                }
                else
                {
                    Coordinate c = (Coordinate)compareSim._Coordinates[co.ID];

                    if ((c.ID != co.ID) ||
                        (c.XCoordinate != co.XCoordinate) ||
                        (c.YCoordinate != co.YCoordinate))
                    {
                        builder2.AppendFormat("{0}{1}{2}", "Master - ", co.ToString().Trim(), Environment.NewLine);
                        builder2.AppendFormat("{0}{1}{2}{3}", "Compare - ", c.ToString().Trim(), Environment.NewLine, Environment.NewLine);
                    }
                }
            }

            // look for compare only           
            foreach (DictionaryEntry de in compareSim._Coordinates)
            {
                Coordinate co = (Coordinate)de.Value;

                if (!(masterSim._Coordinates.Contains(de.Key)))
                {

                    builder3.AppendFormat("{0}{1}", co.ToString(), Environment.NewLine);
                }
            }

            builder.Append(Environment.NewLine);
            builder2.Append(Environment.NewLine);
            builder3.Append(Environment.NewLine);

            txtDiff.Text += builder2.ToString();
            txtMasterOnly.Text += builder.ToString();
            txtCompareOnly.Text += builder3.ToString();
        }

        public void CompareVertices()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("{0}{1}", "[Vertice]", Environment.NewLine);
            StringBuilder builder2 = new StringBuilder();
            builder2.AppendFormat("{0}{1}", "[Vertice]", Environment.NewLine);
            StringBuilder builder3 = new StringBuilder();
            builder3.AppendFormat("{0}{1}", "[Vertice]", Environment.NewLine);

            // look for master only
            foreach (DictionaryEntry de in masterSim._Vertices)
            {
                Vertice v = (Vertice)de.Value;

                if (!(compareSim._Vertices.Contains(de.Key)))
                {

                    builder.AppendFormat("{0}{1}", v.ToString(), Environment.NewLine);
                }
                else
                {
                    Vertice c = (Vertice)compareSim._Vertices[v.ID];

                    if (c.ID != v.ID)                 
                    {
                        builder2.AppendFormat("{0}{1}{2}", "Master - ", v.ToString().Trim(), Environment.NewLine);
                        builder2.AppendFormat("{0}{1}{2}{3}", "Compare - ", c.ToString().Trim(), Environment.NewLine, Environment.NewLine);                        
                    }
                    else if (c.Details.Count != v.Details.Count)
                    {
                        builder2.AppendFormat("{0}; count = {1}{2}{3}{4}", "Master details - ", v.Details.Count, Environment.NewLine, v.ToString().Trim(), Environment.NewLine);
                        builder2.AppendFormat("{0}; count = {1}{2}{3}{4}", "Compare details - ", c.Details.Count, Environment.NewLine, c.ToString().Trim(), Environment.NewLine);
                    }
                    else
                    {
                        // we need to verify the details
                        for (int count = 0; count < v.Details.Count; count++)
                        {
                            VerticeDetail cd = (VerticeDetail)c.Details[count];
                            VerticeDetail md = (VerticeDetail)v.Details[count];

                        }
                    }
                }
            }

            // look for compare only           
            foreach (DictionaryEntry de in compareSim._Coordinates)
            {
                Coordinate co = (Coordinate)de.Value;

                if (!(masterSim._Coordinates.Contains(de.Key)))
                {

                    builder3.AppendFormat("{0}{1}", co.ToString(), Environment.NewLine);
                }
            }

            builder.Append(Environment.NewLine);
            builder2.Append(Environment.NewLine);
            builder3.Append(Environment.NewLine);

            txtDiff.Text += builder2.ToString();
            txtMasterOnly.Text += builder.ToString();
            txtCompareOnly.Text += builder3.ToString();
        }

        public void LineByLineBaby()
        {
            StringBuilder builder = new StringBuilder();

            string mLine;
            string cLine;
            
            FileInfo mattrib = new FileInfo(this.txtMaster.Text);
            FileInfo cattrib = new FileInfo(this.txtCompare.Text);

            builder.AppendFormat("{0}{1}{2}", "Master length = ", mattrib.Length, Environment.NewLine);
            builder.AppendFormat("{0}{1}{2}{2}", "Compare length = ", cattrib.Length, Environment.NewLine);

            var Mreader = File.OpenText(this.txtMaster.Text);
            var Creader = File.OpenText(this.txtCompare.Text);
            int lineCounter = 0;

            while ((mLine = Mreader.ReadLine()) != null)
            {
                ++lineCounter;

                if ((cLine = Creader.ReadLine()) != null)
                {
                    if (mLine.Trim() != cLine.Trim())
                    {
                        builder.AppendFormat("{0}{1}{2}{3}{4}{5}{6}{7}", "***", Environment.NewLine, "line ", lineCounter, Environment.NewLine, "Master = ", mLine.Trim(), Environment.NewLine);
                        builder.AppendFormat("{0}{1}{2}{3}{4}", "Compare = ", cLine.Trim(), Environment.NewLine, "***", Environment.NewLine);
                    }
                }
                else
                    builder.AppendFormat("{0}{1}{2}", "line does not exist in compare - ", mLine, Environment.NewLine);
            }

            txtLineByLine.Text = builder.ToString().Trim();        
        }
    }
}
