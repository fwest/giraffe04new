﻿namespace SystemFileVerifier
{
    partial class Verify
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtMaster = new System.Windows.Forms.TextBox();
            this.txtCompare = new System.Windows.Forms.TextBox();
            this.openFile = new System.Windows.Forms.OpenFileDialog();
            this.btnBrowseMaster = new System.Windows.Forms.Button();
            this.btnBrowseCompare = new System.Windows.Forms.Button();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabMaster = new System.Windows.Forms.TabPage();
            this.txtMasterOnly = new System.Windows.Forms.TextBox();
            this.tabCompare = new System.Windows.Forms.TabPage();
            this.txtCompareOnly = new System.Windows.Forms.TextBox();
            this.tabDiff = new System.Windows.Forms.TabPage();
            this.txtDiff = new System.Windows.Forms.TextBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.txtLineByLine = new System.Windows.Forms.TextBox();
            this.btnVerify = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tabControl.SuspendLayout();
            this.tabMaster.SuspendLayout();
            this.tabCompare.SuspendLayout();
            this.tabDiff.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtMaster
            // 
            this.txtMaster.Location = new System.Drawing.Point(24, 41);
            this.txtMaster.Name = "txtMaster";
            this.txtMaster.Size = new System.Drawing.Size(354, 22);
            this.txtMaster.TabIndex = 0;
            // 
            // txtCompare
            // 
            this.txtCompare.Location = new System.Drawing.Point(24, 91);
            this.txtCompare.Name = "txtCompare";
            this.txtCompare.Size = new System.Drawing.Size(354, 22);
            this.txtCompare.TabIndex = 2;
            // 
            // openFile
            // 
            this.openFile.FileName = "openFile";
            // 
            // btnBrowseMaster
            // 
            this.btnBrowseMaster.Location = new System.Drawing.Point(385, 41);
            this.btnBrowseMaster.Name = "btnBrowseMaster";
            this.btnBrowseMaster.Size = new System.Drawing.Size(33, 23);
            this.btnBrowseMaster.TabIndex = 1;
            this.btnBrowseMaster.Text = "...";
            this.btnBrowseMaster.UseVisualStyleBackColor = true;
            this.btnBrowseMaster.Click += new System.EventHandler(this.btnBrowseMaster_Click);
            // 
            // btnBrowseCompare
            // 
            this.btnBrowseCompare.Location = new System.Drawing.Point(385, 90);
            this.btnBrowseCompare.Name = "btnBrowseCompare";
            this.btnBrowseCompare.Size = new System.Drawing.Size(33, 23);
            this.btnBrowseCompare.TabIndex = 3;
            this.btnBrowseCompare.Text = "...";
            this.btnBrowseCompare.UseVisualStyleBackColor = true;
            this.btnBrowseCompare.Click += new System.EventHandler(this.btnBrowseCompare_Click);
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabMaster);
            this.tabControl.Controls.Add(this.tabCompare);
            this.tabControl.Controls.Add(this.tabDiff);
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Location = new System.Drawing.Point(24, 146);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(783, 366);
            this.tabControl.TabIndex = 4;
            this.tabControl.TabStop = false;
            // 
            // tabMaster
            // 
            this.tabMaster.Controls.Add(this.txtMasterOnly);
            this.tabMaster.Location = new System.Drawing.Point(4, 25);
            this.tabMaster.Name = "tabMaster";
            this.tabMaster.Padding = new System.Windows.Forms.Padding(3);
            this.tabMaster.Size = new System.Drawing.Size(775, 337);
            this.tabMaster.TabIndex = 0;
            this.tabMaster.Text = "Master only";
            this.tabMaster.UseVisualStyleBackColor = true;
            // 
            // txtMasterOnly
            // 
            this.txtMasterOnly.Location = new System.Drawing.Point(0, 0);
            this.txtMasterOnly.Multiline = true;
            this.txtMasterOnly.Name = "txtMasterOnly";
            this.txtMasterOnly.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtMasterOnly.Size = new System.Drawing.Size(775, 337);
            this.txtMasterOnly.TabIndex = 1;
            // 
            // tabCompare
            // 
            this.tabCompare.Controls.Add(this.txtCompareOnly);
            this.tabCompare.Location = new System.Drawing.Point(4, 25);
            this.tabCompare.Name = "tabCompare";
            this.tabCompare.Padding = new System.Windows.Forms.Padding(3);
            this.tabCompare.Size = new System.Drawing.Size(775, 337);
            this.tabCompare.TabIndex = 1;
            this.tabCompare.Text = "Compare only";
            this.tabCompare.UseVisualStyleBackColor = true;
            // 
            // txtCompareOnly
            // 
            this.txtCompareOnly.Location = new System.Drawing.Point(0, 0);
            this.txtCompareOnly.Multiline = true;
            this.txtCompareOnly.Name = "txtCompareOnly";
            this.txtCompareOnly.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtCompareOnly.Size = new System.Drawing.Size(775, 337);
            this.txtCompareOnly.TabIndex = 2;
            // 
            // tabDiff
            // 
            this.tabDiff.Controls.Add(this.txtDiff);
            this.tabDiff.Location = new System.Drawing.Point(4, 25);
            this.tabDiff.Name = "tabDiff";
            this.tabDiff.Size = new System.Drawing.Size(775, 337);
            this.tabDiff.TabIndex = 2;
            this.tabDiff.Text = "Values different";
            this.tabDiff.UseVisualStyleBackColor = true;
            // 
            // txtDiff
            // 
            this.txtDiff.Location = new System.Drawing.Point(0, 0);
            this.txtDiff.Multiline = true;
            this.txtDiff.Name = "txtDiff";
            this.txtDiff.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDiff.Size = new System.Drawing.Size(775, 337);
            this.txtDiff.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.txtLineByLine);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(775, 337);
            this.tabPage1.TabIndex = 3;
            this.tabPage1.Text = "Line by line";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // txtLineByLine
            // 
            this.txtLineByLine.Location = new System.Drawing.Point(1, 3);
            this.txtLineByLine.Multiline = true;
            this.txtLineByLine.Name = "txtLineByLine";
            this.txtLineByLine.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtLineByLine.Size = new System.Drawing.Size(775, 337);
            this.txtLineByLine.TabIndex = 3;
            // 
            // btnVerify
            // 
            this.btnVerify.Location = new System.Drawing.Point(728, 78);
            this.btnVerify.Name = "btnVerify";
            this.btnVerify.Size = new System.Drawing.Size(75, 35);
            this.btnVerify.TabIndex = 4;
            this.btnVerify.Text = "Verify";
            this.btnVerify.UseVisualStyleBackColor = true;
            this.btnVerify.Click += new System.EventHandler(this.btnVerify_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "Master";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 17);
            this.label2.TabIndex = 6;
            this.label2.Text = "Compare";
            // 
            // Verify
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(844, 541);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnVerify);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.btnBrowseCompare);
            this.Controls.Add(this.btnBrowseMaster);
            this.Controls.Add(this.txtCompare);
            this.Controls.Add(this.txtMaster);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "Verify";
            this.Text = "Verify";
            this.tabControl.ResumeLayout(false);
            this.tabMaster.ResumeLayout(false);
            this.tabMaster.PerformLayout();
            this.tabCompare.ResumeLayout(false);
            this.tabCompare.PerformLayout();
            this.tabDiff.ResumeLayout(false);
            this.tabDiff.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtMaster;
        private System.Windows.Forms.TextBox txtCompare;
        private System.Windows.Forms.OpenFileDialog openFile;
        private System.Windows.Forms.Button btnBrowseMaster;
        private System.Windows.Forms.Button btnBrowseCompare;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabMaster;
        private System.Windows.Forms.TextBox txtMasterOnly;
        private System.Windows.Forms.TabPage tabCompare;
        private System.Windows.Forms.TextBox txtCompareOnly;
        private System.Windows.Forms.TabPage tabDiff;
        private System.Windows.Forms.TextBox txtDiff;
        private System.Windows.Forms.Button btnVerify;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox txtLineByLine;
    }
}

