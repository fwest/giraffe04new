﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Giraffe
{
    public enum SimulationType
    {
        [XmlEnum]
        MonteFixed,
        [XmlEnum]
        MonteFlexible,
        [XmlEnum]
        Deterministic
    }

    public enum RegressionType
    {
        [XmlEnum]
        Mean,
        [XmlEnum]
        Percent90
    }

    [Serializable]
    public class SimulationParameters
    {
        public SimulationParameters()
        {
            ProjectName = string.Empty;
            OutputPath = string.Empty;
            SystemFile = string.Empty;
            MinimumPressureToEliminate = -5;
            NumberOfHours = 24;
            NumberOfStepsPerHour = 24;
            Category = SimulationType.Deterministic;
            PipeRepairRateFile = string.Empty;
            MeanPressureFile = string.Empty;
            NumberOfSimulations = 1;
            RandomSeed = 10;
            CalibrateNodalDemand = false;
            RegressionEquation = RegressionType.Mean;
            PipeDamageFile = string.Empty;
            Debug = false;
            Wsort = true;
            Timestamp = true;
            MinimumRequiredForConvergence = 10;
            FullOutputPath = string.Empty;
        }

        [XmlAttribute] public string ProjectName;
        [XmlAttribute] public string OutputPath;
        [XmlAttribute] public string SystemFile;
        [XmlAttribute] public int MinimumPressureToEliminate;
        [XmlAttribute] public int NumberOfHours;
        [XmlAttribute] public int NumberOfStepsPerHour;
        [XmlAttribute] public SimulationType Category;
        [XmlAttribute] public string PipeRepairRateFile;
        [XmlAttribute] public string MeanPressureFile;
        [XmlAttribute] public int NumberOfSimulations;
        [XmlAttribute] public int RandomSeed;
        [XmlAttribute] public bool CalibrateNodalDemand;
        [XmlAttribute] public RegressionType RegressionEquation;
        [XmlAttribute] public string PipeDamageFile;
        [XmlAttribute] public bool Debug;
        [XmlAttribute] public bool Wsort;
        [XmlAttribute] public bool Timestamp;
        [XmlAttribute] public int MinimumRequiredForConvergence;
        [XmlIgnore] public string FullOutputPath;
    }
}
