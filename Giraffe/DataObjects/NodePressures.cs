﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Giraffe
{
    /// <summary>
    /// represents a node pressure instance
    /// </summary>
    [Serializable]
    public class NodePressure : NodeBase<NodePressure>
    {
        public double NodePressureG_RR { get; set; }

        public double NodePressureAve_Pressure { get; set; }

        /// <summary>
        /// c'tor
        /// </summary>
        public NodePressure()
        {
            Type = NodeTypes.nodepressure;
        }

        /// <summary>
        /// put in string format for output file creation
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Empty; // TBD
        }
    }

    [Serializable]
    public class NodePressures : OrderedDictionary
    {
        public void LoadNodePressures(string pressureFile)
        {
            try
            {
                // wipe out any existing data
                if (this.Count > 0)
                    this.Clear();

                // read in the file
                string fileBody = File.ReadAllText(pressureFile);

                string[] pressurelines = fileBody.Split(Environment.NewLine.ToCharArray());

                foreach (string pressure in pressurelines)
                {
                    string tempPressure = pressure.Trim();

                    if ((tempPressure != string.Empty) && (tempPressure[0] != Node.COMMENT[0]) && (tempPressure.IndexOf("Ave_PRESSURE") == -1))
                    {
                        string[] vals = tempPressure.Split(",".ToCharArray());
                        if ((vals[0] != string.Empty) && (vals.Length >= 3))
                        {
                            NodePressure newPressure = new NodePressure();
                            newPressure.ID = vals[0].Trim('"');
                            newPressure.NodePressureG_RR = Convert.ToDouble(vals[1].Trim('"'));
                            newPressure.NodePressureAve_Pressure = Convert.ToDouble(vals[2].Trim('"'));

                            try { this.Add(newPressure.ID, newPressure); }
                            catch (SystemException sysex)
                            {
                                Console.WriteLine(sysex.Message);
                            }
                        }
                    }
                }
            }
            catch (FileLoadException fex)
            {
                var temp = "Unable to load node pressure file " + pressureFile + " " + fex.Message;
                throw new SystemException(temp);
            }
            catch (SystemException sysex)
            {
                Console.WriteLine(sysex.Message);
                var temp = "Unable to load node pressure file " + pressureFile + " " + sysex.Message;
                throw new SystemException(temp);
            }
        }
    }
}
