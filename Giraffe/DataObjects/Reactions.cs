﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Giraffe
{
    /// <summary>
    /// A chemical reaction that may occur in the network
    /// </summary>
    [Serializable]
    public class Reaction : NodeBase<Reaction>
    {
        public const string NODE_ID = @"REACTIONS";
        public const string LIMITING = @"LIMITING";
        public const string ROUGHNESS = @"ROUGHNESS";
        
        public float ReactionCoefficient { get; set; }

        /// <summary>
        /// The node type being overridden in the system
        /// </summary>
        public GlobalReactions Overrides { get; set; }

        public string SpecificNodeBeingOveridden { get; set; }

        /// <summary>
        /// c'tor
        /// </summary>
        public Reaction()
        {
            Type = NodeTypes.reaction;
            SpecificNodeBeingOveridden = string.Empty;
        }

        /// <summary>
        /// put in string format for output file creation
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("{0} {1} {2}", ID, Overrides, ReactionCoefficient.ToString("F6"));

            return builder.ToString().Trim();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class Reactions : OrderedDictionary
    {
        /// <summary>
        /// Load up the reactions collection
        /// </summary>
        /// <param name="?"></param>
        public void LoadReactions(string elements)
        {
            try
            {
                // wipe out any existing data
                if (this.Count > 0)
                    this.Clear();

                string[] pieces = elements.Split(Environment.NewLine.ToCharArray());
                foreach (string piece in pieces)
                {
                    string tempPiece = piece.Trim();
                    if ((tempPiece != string.Empty) && (tempPiece[0] != Node.COMMENT[0]))
                    {
                        Reaction newReaction = new Reaction();

                        string[] react = tempPiece.Split(' ');
                        newReaction.ID = react[0];
                        if ((newReaction.ID.ToUpper().Trim() == Reaction.LIMITING.ToUpper().Trim()) ||
                            (newReaction.ID.ToUpper().Trim() == Reaction.ROUGHNESS.ToUpper().Trim()))
                        {
                            // append the next string for the node id
                            newReaction.ID += " " + react[1];
                            newReaction.ReactionCoefficient = Convert.ToSingle(react[2]);
                        }
                        else 
                        {
                            GlobalReactions temp = GlobalReactions.NULLVALUE;
                            try { temp = (GlobalReactions)Enum.Parse(typeof(GlobalReactions), newReaction.ID); }
                            catch { }
                            if (temp != GlobalReactions.NULLVALUE)
                            {
                                newReaction.SpecificNodeBeingOveridden = react[1];
                                newReaction.ReactionCoefficient = Convert.ToSingle(react[2]);
                            }
                            else
                            {
                                newReaction.Overrides = (GlobalReactions)Enum.Parse(typeof(GlobalReactions), react[1]);
                                newReaction.ReactionCoefficient = Convert.ToSingle(react[2]);
                            }       
                        }
                        // use the input line to satisfy the need for a unique key
                        this.Add(tempPiece, newReaction);
                    }
                }
            }
            catch (SystemException sysex)
            {
                Console.WriteLine(sysex.Message);
            }
        }

        /// <summary>
        /// put the entire collection into an output string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Node.CollectionToString(Reaction.NODE_ID, this);
        }
    }
}
