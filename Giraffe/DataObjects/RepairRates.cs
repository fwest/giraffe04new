﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Giraffe
{
    [Serializable]
    public class RepairRate : NodeBase<RepairRate>
    {
        /// <summary>
        /// 
        /// </summary>
        public double RepairRateLength { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public double RepairRateRR { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string RepairRateMaterial { get; set; }

        /// <summary>
        /// c'tor
        /// </summary>
        public RepairRate()
        {
            Type = NodeTypes.repairrate;
        }
        
        /// <summary>
        /// put in string format for output file creation
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Empty; // TBD
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class RepairRates : OrderedDictionary
    {
        public void LoadRepairRates(string repairRateFile)
        {
            try
            {
                // wipe out any existing data
                if (this.Count > 0)
                    this.Clear();

                // read in the file
                string fileBody = File.ReadAllText(repairRateFile);

                string[] ratelines = fileBody.Split(Environment.NewLine.ToCharArray());

                foreach (string rate in ratelines)
                {
                    string tempRate = rate.Trim();

                    if ((tempRate != string.Empty) && (tempRate[0] != Node.COMMENT[0]) && (tempRate.IndexOf("PipeID") == -1))
                    {
                        string[] vals = tempRate.Split(",".ToCharArray());
                        if ((vals[0] != string.Empty) && (vals.Length >= 4))
                        {
                            RepairRate newRate = new RepairRate();
                            newRate.ID = vals[0].Trim('"');
                            newRate.RepairRateLength = Convert.ToDouble(vals[1].Trim('"'));
                            newRate.RepairRateRR = Convert.ToDouble(vals[2].Trim('"'));
                            newRate.RepairRateMaterial = vals[3].Trim('"');

                            try { this.Add(newRate.ID, newRate); }
                            catch (SystemException sysex)
                            {
                                Console.WriteLine(sysex.Message);
                            }
                        }
                    }
                }
            }
            catch (FileLoadException fex)
            {
                var temp = "Unable to load repair rate file " + repairRateFile + " " + fex.Message;
                throw new SystemException(temp);
            }
            catch (SystemException sysex)
            {
                Console.WriteLine(sysex.Message);
                var temp = "Unable to load repair rate file " + repairRateFile + " " + sysex.Message;
                throw new SystemException(temp);
            }

            // we did not load any repair rates
            if (this.Count == 0)
               Console.WriteLine("Invalid repair rate file, no repair rates found " + repairRateFile);
        }

        /// <summary>
        /// get this breakage setting from the configuration
        /// </summary>
        /// <param name="rate"></param>
        /// <returns></returns>
        public static float GetBreakPro(ParameterDefines paramDefines, RepairRate rate)
        {
            float returnValue = 0;

            switch (rate.RepairRateMaterial.ToUpper().Trim())
            {
                case "CI":
                    returnValue = Convert.ToSingle(paramDefines.Default("BreakProCI"));
                    break;

                case "DI":
                    returnValue = Convert.ToSingle(paramDefines.Default("BreakProDI"));
                    break;

                case "RS":
                    returnValue = Convert.ToSingle(paramDefines.Default("BreakProRS"));
                    break;

                case "CON":
                    returnValue = Convert.ToSingle(paramDefines.Default("BreakProCON"));
                    break;

                case "STL":
                    returnValue = Convert.ToSingle(paramDefines.Default("BreakProSTL"));
                    break;

                default:
                    // log this unexpected value
                    break;
            }

            return returnValue;
        }
    }
}
