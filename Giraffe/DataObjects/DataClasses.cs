﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Epanet;

namespace Giraffe
{
    public enum SuccessCategories
    {
        Success = 0,
        ConvergenceCriteriaMet,
        Failure,
        Cancelled
    }

    public enum SimulationCategories
    {
        MonteFixed = 0,
        MonteFlex,
        Deterministic
    }

    /// <summary>
    /// this type specifies the confidence level
    /// </summary>
    public enum CalibrationType
    {
        Mean,
        Option // 90% 
    }

    /// <summary>
    /// this type allows for efficient recognition of the nodes
    /// </summary>
    public enum NodeTypes
    {
        title,
        junction,
        reservoir,
        tank,
        pipe,
        pump,
        valve,
        emitter,
        curve,
        pattern,
        energy,
        status,
        control,
        rule,
        demand,
        quality,
        reaction,
        source,
        mixing,
        option,
        time,
        report,
        coordinate,
        vertice,
        label,
        backdrop,
        tag,
        pipebreak,
        pipeleak,
        nodepressure,
        repairrate,
        leakratio,
        montecarlovalue,
        montecarlonode,
        serviceability,
        sumdemand,
        standarddeviation,
        basenode,
        controldetail
    }

    /// <summary>
    /// used to identify valve status
    /// </summary>
    public enum BinaryValveStatus
    {
        Open = 0,
        Closed,
        NotDefined
    }

    /// <summary>
    /// are we above or below a certain pressure?
    /// </summary>
    public enum BinaryPressureLevel
    {
        Above = 0,
        Below
    }

    public enum DayNight
    {
        AM = 0,
        PM,
        NotDefined
    }

    public enum SourceTypes
    {
        CONCEN = 0,
        MASS,
        FLOWPACED,
        SETPOINT
    }

    public enum GlobalReactions
    {
        BULK = 0,
        WALL,
        TANK,
        NULLVALUE
    }

    public enum ReportOptionValues
    {
        YES = 0,
        NO,
        NONE,
        ALL,
        BELOW,
        ABOVE,
        PRECISION,
        NOTDEFINED
    }

    /// <summary>
    /// this class replaces the legacy NodeSysRoot base class.
    /// Many of the properties can be replaced or are already implemented
    /// by the various collections we will be using to represent the legacy
    /// linked lists. There may be node need for the legacy sub-class NodeSysRootType
    /// 
    /// There is also significant code in the legacy class around being able to 
    /// identify the type of the sub-classes that extend this base class. Perhaps
    /// use the typeof capabilities of C#/.Net.
    /// </summary>
    [Serializable]
    public abstract class NodeBase<T> : Node
    {
        #region properties               
        
        /// <summary>
        /// our base class representing all nodes
        /// </summary>
        public bool Original { get; set; }
        
        /// <summary>
        /// not used
        /// </summary>
        public bool Erase { get; set; }       
        
        #endregion

        /// <summary>
        /// this routine will concatenate all strings following the id string
        /// and return the result
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        static public string GetNextString(string[] substrings)
        {
            string result = string.Empty;

            for (int counter = 1; counter < substrings.Length; counter++)
            {
                if (result != string.Empty)
                    result += " " + substrings[counter];
                else
                    result = substrings[counter];
            }

            return result;
        }

        /// <summary>
        /// this routine will take the last value in the passed array
        /// and return its numeric double precision value
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        static public double GetNextDouble(string[] substrings)
        {
            return Convert.ToDouble(substrings[substrings.Length - 1]);
        }

        static public string GetNextDoubleString(string[] substrings)
        {
            return substrings[substrings.Length - 1];
        }
    }

    /// <summary>
    /// use this class if the collection needs order
    /// </summary>
    public class MyOrderedDictionary : OrderedDictionary
    {
        static public IDictionary Ordered(OrderedDictionary original)
        {
            return original.Cast<DictionaryEntry>()
                    .OrderBy(l => l.Key.ToString().Length).ThenBy(k => k.Key)
                    .ToDictionary(k => k.Key, v => v.Value);
        }

        static public IDictionary OrderedByID(OrderedDictionary original)
        {
            return original.Cast<DictionaryEntry>()
                    .OrderBy(l => ((Node)(l.Value)).ID.Length).ThenBy(k => ((Node)(k.Value)).ID)
                    .ToDictionary(k => k.Key, v => v.Value);
        }  
    }

    /// <summary>
    /// class used for EPA Net value based output
    /// </summary>
    public class NodeOrderedDictionary : MyOrderedDictionary
    {
        public string ToEPAString(string id = "")
        {
            StringBuilder builder = new StringBuilder();

            try
            {
                EpaPort epa = EpaPort.GetEpaInstance();

                foreach (DictionaryEntry de in MyOrderedDictionary.Ordered(this))
                {
                    Node node = (Node)de.Value;
                    
                    int epaIndex = 0;
                    try { epaIndex = epa.GetNodeIndex(node.ID); }
                    catch
                    {
                        epaIndex = 0;
                        Console.WriteLine(node.ID + " not a valid NODE ID in model.");
                    }

                    if (epaIndex != 0)
                    {
                        float demand = epa.GetNodeValue(epaIndex, NodeValue.Demand);
                        float pressure = epa.GetNodeValue(epaIndex, NodeValue.Pressure);
                        float head = epa.GetNodeValue(epaIndex, NodeValue.Head);
                        builder.AppendFormat("{0}\t{1}\t{2}\t{3}\t{4}{5}", node.ID, demand.ToString("F2"), head.ToString("F2"), pressure.ToString("F2"), id, Environment.NewLine);
                    }
                    else
                        builder.AppendFormat("{0}\t{1}\t{2}\t{3}\t{4}{5}", node.ID, 0.0, 0.0, 0.0, id, Environment.NewLine);
                }
            }
            catch(SystemException sysex)
            {
                Exception newEx = new Exception("Unable to EPA string " + id + " Message = " + sysex.Message + " Trace: " + sysex.StackTrace.Trim());
                throw newEx;
            }

            return builder.ToString().Trim();
        }
    }

    /// <summary>
    /// class used for EPA Net value based output
    /// </summary>
    public class LinkOrderedDictionary : MyOrderedDictionary
    {
        public string ToEPAString(string id = "")
        {
            try
            {
                EpaPort epa = EpaPort.GetEpaInstance();
                StringBuilder builder = new StringBuilder();

                foreach (DictionaryEntry de in MyOrderedDictionary.Ordered(this))
                {
                    Node node = (Node)de.Value;
                   
                    int epaIndex = 0;
                    try { epaIndex = epa.GetLinkIndex(node.ID); }
                    catch 
                    {
                        epaIndex = 0;
                        Console.WriteLine(node.ID + " not a valid LINK ID in model."); 
                    }

                    if (epaIndex != 0)
                    {
                        float flow = epa.GetLinkValue(epaIndex, LinkValue.Flow);
                        float head = epa.GetLinkValue(epaIndex, LinkValue.HeadLoss);
                        float velocity = epa.GetLinkValue(epaIndex, LinkValue.Velocity);
                        float length = epa.GetLinkValue(epaIndex, LinkValue.Length);

                        var temp = 0.0;
                        if (length > 0)
                            temp = ((head * 1000) / length);
                        else
                            temp = head;

                        builder.AppendFormat("{0}\t{1}\t{2}\t{3}\t{4}{5}", node.ID, flow.ToString("F2"), velocity.ToString("F2"), temp.ToString("F2"), id, Environment.NewLine);
                    }
                    else
                        builder.AppendFormat("{0}\t{1}\t{2}\t{3}\t{4}{5}", node.ID, 0.0, 0.0, 0.0, id, Environment.NewLine);

                }

                return builder.ToString().Trim();
            }
            catch (SystemException sysex)
            {
                Exception newEx = new Exception("Unable to EPA string " + id + " Message = " + sysex.Message + " Trace: " + sysex.StackTrace.Trim());
                throw newEx;
            }        
        }
    }        

    #region MonteCarlo Fix-Runs/Flexible classes
    
    /// <summary>
    /// Use this class to access random number generation functionality
    /// </summary>
    [Serializable]
    public class RandomNormal
    {
        public const int RAND_MAX = 0x7fff;

        /// <summary>
        /// stores one rng determined by RandomNormalFunc
        /// </summary>
        public double Normal1{ get; set; }

        /// <summary>
        /// stores second rng determined by RandomNormalFunc
        /// </summary>
        public double Normal2{ get; set; }

        /// <summary>
        /// return a random number result -- this algorithm is directly from Giraffe 08 sources
        /// with the only differences being C -> C# function ports
        /// </summary>
        /// <returns></returns>
        public static RandomNormal RandomNormalFunc(Random newRand)
        {
            RandomNormal returnValue = new RandomNormal();    
            float R1 = 0, R2 = 0, v = 0, Max = RAND_MAX;
    
            //Random randomGenerator = new Random();

            do
            {
                R1 = newRand.Next(RAND_MAX);
                R1 /= Max;
                R2 = newRand.Next(RAND_MAX);
                R2 /= Max;
                R1 = 2.0f * R1 - 1.0f;
                R2 = 2.0f * R2 - 1.0f;
                // Pow returns a double
                v = (float)Math.Pow(R1, 2) + (float)Math.Pow(R2, 2);
            } while(v >= 1.0f);

	        try
            {
		        if (v != 0.0f)
                    // these Math functions return doubles
                    v = (float)(Math.Sqrt((-2.0f * Math.Log(v)) / v));
		        else
			        v = 0.0f;
	        }
	        catch(SystemException sysex)
            {
                Console.WriteLine("RandomNormalFunc -- " + sysex.Message);
	        }

            returnValue.Normal1 = R1 * v;
            returnValue.Normal2 = R2 * v;

            return returnValue;
        }
    }    

    /// <summary>
    /// represents convergence criteria, when met stop the monte carlo flex
    /// simulation
    /// </summary>
    [Serializable]
    public class ConvergeCriteria
    {
        /// <summary>
        /// the mean of the convergence criteria to meet
        /// </summary>
        public float Mean{ get; set; }

        /// <summary>
        /// convergence criteria as defined by system parameters
        /// </summary>
        public float COV{ get; set; }
    }
       
    /// <summary>
    /// used to store standard deviation values
    /// </summary>
    [Serializable]
    public class StandardDeviation : NodeBase<StandardDeviation>
    {
        /// <summary>
        /// demand/sum
        /// </summary>
        public float ValueStandard{ get; set; }
    
        /// <summary>
        /// c'tor
        /// </summary>
        public StandardDeviation()
        {
            Type = NodeTypes.standarddeviation;
        }

        /// <summary>
        /// put in string format for output file creation
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Empty; // TBD
        }
    }

    #endregion
    
    [Serializable]
    public class DrawVertices
    {       
        public int RepairNode;

        public int BreakNo;

        public int LeakNo;

        public int PreIndex;

        public float PipeL;
    
        public float PreRatio;
     
        public float Ratio;
      
        public float Angle;
       
        public float UpNodeX;

        public float UpNodeY;
    };

    
    [Serializable]
    public abstract class Node
    {
        public const string NODE_TYPE_SEPARATOR_1 = @"[";
        public const string NODE_TYPE_SEPARATOR_2 = @"]";
        public const string NODE_START = @"TITLE";
        public const string NODE_END = @"END";
        public const string COMMENT = @";";
        
        /// <summary>
        /// the unique identifier
        /// </summary>
        public string ID {get; set;}        

        /// <summary>
        /// what type are we?
        /// </summary>
        public NodeTypes Type { get; set; }

        /// <summary>
        /// the warning given on override -- ignore
        /// </summary>
        /// <returns></returns>
        public abstract override string ToString();

        /// <summary>
        /// put a collection of nodes into an output string
        /// </summary>
        /// <returns></returns>
        static public string CollectionToString(string nodeID, IDictionary theCollection)
        {
            StringBuilder builder = new StringBuilder();

            builder.AppendFormat("[{0}]" + Environment.NewLine, nodeID);

            foreach (DictionaryEntry de in theCollection)
            {
                Node item = (Node)de.Value;                
                builder.AppendFormat("{0}{1}", item.ToString(), Environment.NewLine);
            }

            return builder.ToString();
        }
    }
}

