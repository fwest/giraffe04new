﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Giraffe
{
    /// <summary>
    /// represents a node coordinate, location
    /// </summary>
    [Serializable]
    public class Coordinate : NodeBase<Coordinate>
    {
        public const string NODE_ID = @"COORDINATES";

        /// <summary>
        /// the x coordinate on the x,y plane
        /// </summary>
        public float XCoordinate { get; set; }

        /// <summary>
        /// the y coordinate on the x,y plane
        /// </summary>
        public float YCoordinate { get; set; }

        /// <summary>
        /// c'tor
        /// </summary>
        public Coordinate()
        {
            Type = NodeTypes.coordinate;
        }

        /// <summary>
        /// put in string format for output file creation
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();

            double dX = this.XCoordinate;
            double dY = this.YCoordinate;

            builder.AppendFormat("{0}\t{1}\t{2}", this.ID, dX.ToString("F4"), dY.ToString("F4"));

            return builder.ToString().Trim();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class Coordinates : OrderedDictionary
    {
        /// <summary>
        /// Load up the coordinates collection
        /// </summary>
        /// <param name="?"></param>
        public void LoadCoordinates(string elements)
        {
            try
            {
                // wipe out any existing data
                if (this.Count > 0)
                    this.Clear();

                string[] pieces = elements.Split(Environment.NewLine.ToCharArray());
                foreach (string piece in pieces)
                {
                    string tempPiece = piece.Trim();
                    if ((tempPiece != string.Empty) && (tempPiece[0] != Node.COMMENT[0]))
                    {
                        Coordinate newCoordinate = new Coordinate();
                        string[] coords = tempPiece.Split("\t".ToCharArray());
                        if (coords.Length == 3)
                        {
                            newCoordinate.ID = coords[0];
                            newCoordinate.XCoordinate = Convert.ToSingle(coords[1]);
                            newCoordinate.YCoordinate = Convert.ToSingle(coords[2]);
                            // there should only be one line for each node
                            // if there are repeats, ignore after the first
                            try { this.Add(newCoordinate.ID, newCoordinate); }
                            catch (SystemException sysex) { Console.WriteLine(sysex.Message); }
                        }
                    }
                }
            }
            catch (SystemException sysex)
            {
                Console.WriteLine(sysex.Message);
            }
        }

        /// <summary>
        /// calculates the angle between two coordinates, for
        /// display purposes only
        /// </summary>
        /// <param name="startNodeX"></param>
        /// <param name="endNodeX"></param>
        /// <param name="startNodeY"></param>
        /// <param name="endNodeY"></param>
        /// <returns></returns>
        public static float CalculateAngle(float startNodeX, float endNodeX, float startNodeY, float endNodeY)
        {
            float angle = 0.0f;

            try
            {                
                var temp = Math.Atan( Math.Abs( (endNodeY - startNodeY) / (endNodeX - startNodeX) ) );
                angle = (float)temp;
            }

            catch(DivideByZeroException dex)
            {
                Console.WriteLine(dex.Message);
        #if ORIG_CODE_ANAMOLY
        #else               
                throw;
        #endif
            }

            if(endNodeX <= startNodeX)
            {
                if(endNodeY > startNodeY)
                {
                    var temp = Math.PI - angle;
                    angle = (float)temp;

                }
                else if(endNodeY < startNodeY)
                {
                    var temp = Math.PI + angle;
                    angle = (float)temp;
                }
            }
            else if(endNodeY <= startNodeY)
            {                
                var temp = Math.PI * 2 - angle;
                angle = (float)temp;
            }

            return angle;
        }

        private string cache = string.Empty;
        private bool dirty = false;

        /// <summary>
        /// put the entire collection into an output string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (dirty || (cache.Length == 0))
            {
                cache = Node.CollectionToString(Coordinate.NODE_ID, MyOrderedDictionary.Ordered(this));
                dirty = false;
            }

            return cache;
        }

        /// <summary>
        /// cache for performance
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public new void Add(object key, object value)
        {
            dirty = true;
            base.Add(key, value);
        }

        /// <summary>
        /// cache for performance
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public new void Remove(object key)
        {
            dirty = true;
            base.Remove(key);
        }
    }
}
