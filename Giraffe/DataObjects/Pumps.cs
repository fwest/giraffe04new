﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Giraffe
{
    /// <summary>
    /// represents a pump instance
    /// </summary>
    [Serializable]
    public class Pump : NodeBase<Pump>
    {
        public const string NODE_ID = @"PUMPS";
       
        public string StartNode { get; set; }
        
        public string EndNode { get; set; }
        
        public string PumpPower { get; set; }

        /// <summary>
        /// c'tor
        /// </summary>
        public Pump()
        {
            Type = NodeTypes.pump;
        }

        /// <summary>
        /// put in string format for output file creation
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("{0}\t{1}\t{2}\t{3}\t", this.ID, this.StartNode,
                                                    this.EndNode,
                                                    this.PumpPower);
            
            return builder.ToString();           
        }
    }

    public class Pumps : LinkOrderedDictionary
    {
        /// <summary>
        /// Load up the pumps collection
        /// </summary>
        /// <param name="?"></param>
        public void LoadPumps(string elements)
        {
            try
            {
                // wipe out any existing data
                if (this.Count > 0)
                    this.Clear();

                string[] pieces = elements.Split(Environment.NewLine.ToCharArray());
                foreach (string piece in pieces)
                {
                    string tempPiece = piece.Trim();

                    if ((tempPiece != string.Empty) && (tempPiece[0] != Node.COMMENT[0]))
                    {
                        string[] vals = tempPiece.Split("\t".ToCharArray());
                        if (vals[0] != string.Empty)
                        {
                            Pump newPump = new Pump();
                            newPump.ID = vals[0];
                            newPump.StartNode = vals[1]; // a single is a float
                            newPump.EndNode = vals[2];
                            // the rest if the values are "parameters" -- values concatenated
                            // together, separated by a tab -- one string result
                            if (vals.Length >= 4)
                            {
                                string result = vals[3];
                                for (int count = 4; count < vals.Length; count++)
                                {
                                    if (vals[count] != string.Empty)
                                        result += "\t" + vals[count];
                                }
                                newPump.PumpPower = result;
                            }

                            this.Add(newPump.ID, newPump);
                        }
                    }
                }
            }
            catch (SystemException sysex)
            {
                Console.WriteLine(sysex.Message);
            }
        }

        /// <summary>
        /// put the entire collection into an output string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {            
            return Node.CollectionToString(Pump.NODE_ID, MyOrderedDictionary.Ordered(this));
        }
    }
}
