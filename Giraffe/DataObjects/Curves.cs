﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Giraffe
{
    /// <summary>
    /// represents the location of a pump given a power curve
    /// </summary>
    [Serializable]
    public class CurveCoordinate
    {
        /// <summary>
        /// the x coordinate on the x,y plane
        /// </summary>
        public float CurveX { get; set; }

        /// <summary>
        /// the y coordinate on the x,y plane
        /// </summary>
        public float CurveY { get; set; }

    }        

    /// <summary>
    /// represents a pump curve
    /// </summary>
    [Serializable]
    public class Curve : NodeBase<Curve>
    {
        public const string NODE_ID = @"CURVES";       

        /// <summary>
        /// all the possible coordinates belonging to a curve
        /// </summary>
        public OrderedDictionary Coordinates { get; set; }

        /// <summary>
        /// c'tor
        /// </summary>
        public Curve()
        {
            Type = NodeTypes.curve;

            Coordinates = new OrderedDictionary();
        }

        /// <summary>
        /// put in string format for output file creation
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();

            foreach (DictionaryEntry de in this.Coordinates)
            {
                if (builder.Length > 0)
                    builder.AppendFormat("{0}", Environment.NewLine);
                CurveCoordinate coord = (CurveCoordinate)de.Value;
                double x = coord.CurveX;
                double y = coord.CurveY;

                builder.AppendFormat("{0}\t{1}\t{2}", this.ID, x.ToString("F5"), y.ToString("F5"));
            }

            return builder.ToString();
        }
    }

    public class Curves : OrderedDictionary
    {
        private string cache = string.Empty;
        private bool dirty = false;

        /// <summary>
        /// Load up the curves collection
        /// </summary>
        /// <param name="?"></param>
        public void LoadCurves(string elements)
        {
            try
            {
                // wipe out any existing data
                if (this.Count > 0)
                    this.Clear();

                string[] pieces = elements.Split(Environment.NewLine.ToCharArray());
                foreach (string piece in pieces)
                {
                    string tempPiece = piece.Trim();
                    if ((tempPiece != string.Empty) && (tempPiece[0] != Node.COMMENT[0]))
                    {
                        string[] vals = tempPiece.Split("\t".ToCharArray());
                        if (vals[0] != string.Empty)
                        {
                            Curve newCurve = new Curve();
                            newCurve.ID = vals[0];

                            CurveCoordinate newCoordinate = new CurveCoordinate();
                            newCoordinate.CurveX = Convert.ToSingle(vals[1]);
                            newCoordinate.CurveY = Convert.ToSingle(vals[2]);

                            // if this curve already exists in the collection, store off
                            // the coordinates to the ID
                            if (this.Contains(newCurve.ID))
                            {
                                Curve tempCurve = this[newCurve.ID] as Curve;
                                tempCurve.Coordinates.Add(tempPiece, newCoordinate);
                            }
                            else
                            {
                                // new entry
                                newCurve.Coordinates.Add(tempPiece, newCoordinate);
                                this.Add(newCurve.ID, newCurve);
                            }
                        }
                    }
                }
            }
            catch (SystemException sysex)
            {
                Console.WriteLine(sysex.Message);
            }
        }

        private IDictionary sort(IDictionary collection)
        {
            return collection.Cast<DictionaryEntry>()
                    .OrderBy(l => l.Key.ToString().Length).ThenBy(k => k.Key)
                    .ToDictionary(k => k.Key, v => v.Value);
        }

        /// <summary>
        /// put the entire collection into an output string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (dirty || (cache.Length == 0))
            {
                cache = Node.CollectionToString(Curve.NODE_ID, this);
                dirty = false;
            }

            return cache;            
        }

        /// <summary>
        /// cache for performance
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public new void Add(object key, object value)
        {
            dirty = true;
            base.Add(key, value);
        }

        /// <summary>
        /// cache for performance
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public new void Remove(object key)
        {
            dirty = true;
            base.Remove(key);
        }
    }
}
