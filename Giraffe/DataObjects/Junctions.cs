﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Giraffe
{
    /// <summary>
    /// represents a juntion instance
    /// </summary>
    [Serializable]
    public class Junction : NodeBase<Junction>
    {
        public const string NODE_ID = @"JUNCTIONS";

        /// <summary>
        /// the elevation of the junction
        /// </summary>
        public float JunctionElevation { get; set; }
      
        /// <summary>
        /// c'tor
        /// </summary>
        public Junction()
        {
            Type = NodeTypes.junction;
        }

        /// <summary>
        /// put in string format for output file creation
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            double temp = this.JunctionElevation;
            builder.AppendFormat("{0}\t{1}", this.ID, temp.ToString("F5"));
            return builder.ToString();
        }
    }

    public class Junctions : NodeOrderedDictionary
    {
        public void LoadJunctions(string info)
        {
            try
            {
                //Console.Write(info);

                // wipe out any existing data
                if (this.Count > 0)
                    this.Clear();

                string[] pieces = info.Split(Environment.NewLine.ToCharArray());
                foreach (string piece in pieces)
                {
                    string tempPiece = piece.Trim();

                    if ((tempPiece != string.Empty) && (tempPiece[0] != Node.COMMENT[0]))
                    {
                        string[] vals = tempPiece.Split("\t".ToCharArray());                        
                        if ((vals[0] != string.Empty) && (vals.Length == 2))
                        {
                            Junction newJunction = new Junction();
                            newJunction.ID = vals[0];
                            newJunction.JunctionElevation = Convert.ToSingle(vals[1]); // a single is a float
                            this.Add(newJunction.ID, newJunction);
                        }
                    }
                }
            }
            catch (SystemException sysex)
            {
                Console.WriteLine(sysex.Message);
            }            
        }

        /// <summary>
        /// put the entire collection into an output string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Node.CollectionToString(Junction.NODE_ID, MyOrderedDictionary.Ordered(this));
        }
        
    }
}
