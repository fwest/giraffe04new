﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Giraffe
{
    public class ParameterDefinesAlt
    {
        /// <summary>
        /// contains the defaults paramters loaded into the system upon
        /// start up (Main)
        /// </summary>
        private readonly Hashtable _configurationInformation = new Hashtable();

        /// <summary>
        /// Constructor
        /// </summary>
        public ParameterDefinesAlt()
        {
        }

        #region Parameters

        public string BreakProCI { get { return GetValue("BreakProCI"); } set { SetValue("BreakProCI", value); } }
        public string BreakProCON { get { return GetValue("BreakProCON"); } set { SetValue("BreakProCON", value); } }
        public string BreakProDI { get { return GetValue("BreakProDI"); } set { SetValue("BreakProDI", value); } }
        public string BreakProRS { get { return GetValue("BreakProRS"); } set { SetValue("BreakProRS", value); } }
        public string BreakProSTL { get { return GetValue("BreakProSTL"); } set { SetValue("BreakProSTL", value); } }
        public string CIType1D { get { return GetValue("CIType1D"); } set { SetValue("CIType1D", value); } }
        public string CIType2D { get { return GetValue("CIType2D"); } set { SetValue("CIType2D", value); } }
        public string CIType3D { get { return GetValue("CIType3D"); } set { SetValue("CIType3D", value); } }
        public string CIType4D { get { return GetValue("CIType4D"); } set { SetValue("CIType4D", value); } }
        public string CIType5D { get { return GetValue("CIType5D"); } set { SetValue("CIType5D", value); } }
        public string CONType1D { get { return GetValue("CONType1D"); } set { SetValue("CONType1D", value); } }
        public string CONType2D { get { return GetValue("CONType2D"); } set { SetValue("CONType2D", value); } }
        public string CONType3D { get { return GetValue("CONType3D"); } set { SetValue("CONType3D", value); } }
        public string CONType4D { get { return GetValue("CONType4D"); } set { SetValue("CONType4D", value); } }
        public string CONType5D { get { return GetValue("CONType5D"); } set { SetValue("CONType5D", value); } }
        public string COVFlexible { get { return GetValue("COVFlexible"); } set { SetValue("COVFlexible", value); } }
        public string DIType1D { get { return GetValue("DIType1D"); } set { SetValue("DIType1D", value); } }
        public string DIType2D { get { return GetValue("DIType2D"); } set { SetValue("DIType2D", value); } }
        public string DIType3D { get { return GetValue("DIType3D"); } set { SetValue("DIType3D", value); } }
        public string DIType4D { get { return GetValue("DIType4D"); } set { SetValue("DIType4D", value); } }
        public string DIType5D { get { return GetValue("DIType5D"); } set { SetValue("DIType5D", value); } }
        public string MaxNSimu { get { return GetValue("MaxNSimu"); } set { SetValue("MaxNSimu", value); } }
        public string MeanFlexible { get { return GetValue("MeanFlexible"); } set { SetValue("MeanFlexible", value); } }
        public string MiiMP { get { return GetValue("MiiMP"); } set { SetValue("MiiMP", value); } }
        public string MiiSD { get { return GetValue("MiiSD"); } set { SetValue("MiiSD", value); } }
        public string MisMP { get { return GetValue("MisMP"); } set { SetValue("MisMP", value); } }
        public string MisSD { get { return GetValue("MisSD"); } set { SetValue("MisSD", value); } }
        public string MsiMP { get { return GetValue("MsiMP"); } set { SetValue("MsiMP", value); } }
        public string MsiSD { get { return GetValue("MsiSD"); } set { SetValue("MsiSD", value); } }
        public string MssMP { get { return GetValue("MssMP"); } set { SetValue("MssMP", value); } }
        public string MssSD { get { return GetValue("MssSD"); } set { SetValue("MssSD", value); } }
        public string OtherType1D { get { return GetValue("OtherType1D"); } set { SetValue("OtherType1D", value); } }
        public string OtherType2D { get { return GetValue("OtherType2D"); } set { SetValue("OtherType2D", value); } }
        public string OtherType3D { get { return GetValue("OtherType3D"); } set { SetValue("OtherType3D", value); } }
        public string OtherType4D { get { return GetValue("OtherType4D"); } set { SetValue("OtherType4D", value); } }
        public string OtherType5D { get { return GetValue("OtherType5D"); } set { SetValue("OtherType5D", value); } }
        public string RSType1D { get { return GetValue("RSType1D"); } set { SetValue("RSType1D", value); } }
        public string RSType2D { get { return GetValue("RSType2D"); } set { SetValue("RSType2D", value); } }
        public string RSType3D { get { return GetValue("RSType3D"); } set { SetValue("RSType3D", value); } }
        public string RSType4D { get { return GetValue("RSType4D"); } set { SetValue("RSType4D", value); } }
        public string RSType5D { get { return GetValue("RSType5D"); } set { SetValue("RSType5D", value); } }
        public string STLLeakRatio { get { return GetValue("STLLeakRatio"); } set { SetValue("STLLeakRatio", value); } }
        public string STLType1D { get { return GetValue("STLType1D"); } set { SetValue("STLType1D", value); } }
        public string STLType2D { get { return GetValue("STLType2D"); } set { SetValue("STLType2D", value); } }
        public string STLType3D { get { return GetValue("STLType3D"); } set { SetValue("STLType3D", value); } }
        public string STLType4D { get { return GetValue("STLType4D"); } set { SetValue("STLType4D", value); } }
        public string STLType5D { get { return GetValue("STLType5D"); } set { SetValue("STLType5D", value); } }
        public string Type1kD { get { return GetValue("Type1kD"); } set { SetValue("Type1kD", value); } }
        public string Type1tD { get { return GetValue("Type1tD"); } set { SetValue("Type1tD", value); } }
        public string Type2aD { get { return GetValue("Type2aD"); } set { SetValue("Type2aD", value); } }
        public string Type3aD { get { return GetValue("Type3aD"); } set { SetValue("Type3aD", value); } }
        public string Type3kD { get { return GetValue("Type3kD"); } set { SetValue("Type3kD", value); } }
        public string Type4kD { get { return GetValue("Type4kD"); } set { SetValue("Type4kD", value); } }
        public string Type5kD { get { return GetValue("Type5kD"); } set { SetValue("Type5kD", value); } }
        public string Type5wD { get { return GetValue("Type5wD"); } set { SetValue("Type5wD", value); } }
        public string UiiMP { get { return GetValue("UiiMP"); } set { SetValue("UiiMP", value); } }
        public string UisMP { get { return GetValue("UisMP"); } set { SetValue("UisMP", value); } }
        public string UsiMP { get { return GetValue("UsiMP"); } set { SetValue("UsiMP", value); } }
        public string UssMP { get { return GetValue("UssMP"); } set { SetValue("UssMP", value); } }
        public string mRRCap { get { return GetValue("mRRCap"); } set { SetValue("mRRCap", value); } }

        #region Read-only Parameters

        /// <summary>
        /// Leak probability for Cast Iron
        /// </summary>
        public string CILeakRatio
        {
            get { return CalculateLeakProbability(BreakProCI); }
        }

        /// <summary>
        /// Leak probability for Ductile Iron
        /// </summary>
        public string DILeakRatio
        {
            get { return CalculateLeakProbability(BreakProDI); }
        }

        /// <summary>
        /// Leak probability for Riveted Steel
        /// </summary>
        public string RSLeakRatio
        {
            get { return CalculateLeakProbability(BreakProRS); }
        }

        /// <summary>
        /// Leak probability for Concrete
        /// </summary>
        public string CONLeakRatio
        {
            get { return CalculateLeakProbability(BreakProCON); }
        }

        #endregion

        #endregion

        /// <summary>
        /// get the specified element value
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        private string GetValue(string key)
        {
            return (string)(_configurationInformation[key]);
        }

        /// <summary>
        /// set the specified element value
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void SetValue(string key, string value)
        {
            _configurationInformation[key] = value;
        }

        /// <summary>
        /// loads the configuration specified by the user
        /// </summary>
        /// <param name="fileName"></param>
        public void LoadConfiguration(string fileName)
        {
            if ((!String.IsNullOrEmpty(fileName)) && (File.Exists(fileName)))
            {

                string fileBody = File.ReadAllText(fileName);

                string[] lines = fileBody.Split(Environment.NewLine.ToCharArray());

                foreach (string line in lines)
                {
                    try
                    {
                        string[] keyValue = line.Split('=');
                        _configurationInformation[keyValue[0]] = keyValue[1];
                    }
                    catch (SystemException sysex)
                    {
                        Console.WriteLine(sysex.Message);
                    }
                }
            }
        }

        /// <summary>
        /// Writes Parameter Defines to the specified file
        /// </summary>
        /// <param name="fileName"></param>
        public void SaveConfiguration(string fileName)
        {
            if (!String.IsNullOrEmpty(fileName))
            {
                StringBuilder builder = new StringBuilder();
                builder.AppendLine("BreakProCI=" + BreakProCI);
                builder.AppendLine("BreakProCON=" + BreakProCON);
                builder.AppendLine("BreakProDI=" + BreakProDI);
                builder.AppendLine("BreakProRS=" + BreakProRS);
                builder.AppendLine("BreakProSTL=" + BreakProSTL);
                builder.AppendLine("CIType1D=" + CIType1D);
                builder.AppendLine("CIType2D=" + CIType2D);
                builder.AppendLine("CIType3D=" + CIType3D);
                builder.AppendLine("CIType4D=" + CIType4D);
                builder.AppendLine("CIType5D=" + CIType5D);
                builder.AppendLine("CONType1D=" + CONType1D);
                builder.AppendLine("CONType2D=" + CONType2D);
                builder.AppendLine("CONType3D=" + CONType3D);
                builder.AppendLine("CONType4D=" + CONType4D);
                builder.AppendLine("CONType5D=" + CONType5D);
                builder.AppendLine("COVFlexible=" + COVFlexible);
                builder.AppendLine("DIType1D=" + DIType1D);
                builder.AppendLine("DIType2D=" + DIType2D);
                builder.AppendLine("DIType3D=" + DIType3D);
                builder.AppendLine("DIType4D=" + DIType4D);
                builder.AppendLine("DIType5D=" + DIType5D);
                builder.AppendLine("MaxNSimu=" + MaxNSimu);
                builder.AppendLine("MeanFlexible=" + MeanFlexible);
                builder.AppendLine("MiiMP=" + MiiMP);
                builder.AppendLine("MiiSD=" + MiiSD);
                builder.AppendLine("MisMP=" + MisMP);
                builder.AppendLine("MisSD=" + MisSD);
                builder.AppendLine("MsiMP=" + MsiMP);
                builder.AppendLine("MsiSD=" + MsiSD);
                builder.AppendLine("MssMP=" + MssMP);
                builder.AppendLine("MssSD=" + MssSD);
                builder.AppendLine("OtherType1D=" + OtherType1D);
                builder.AppendLine("OtherType2D=" + OtherType2D);
                builder.AppendLine("OtherType3D=" + OtherType3D);
                builder.AppendLine("OtherType4D=" + OtherType4D);
                builder.AppendLine("OtherType5D=" + OtherType5D);
                builder.AppendLine("RSType1D=" + RSType1D);
                builder.AppendLine("RSType2D=" + RSType2D);
                builder.AppendLine("RSType3D=" + RSType3D);
                builder.AppendLine("RSType4D=" + RSType4D);
                builder.AppendLine("RSType5D=" + RSType5D);
                builder.AppendLine("STLLeakRatio=" + STLLeakRatio);
                builder.AppendLine("STLType1D=" + STLType1D);
                builder.AppendLine("STLType2D=" + STLType2D);
                builder.AppendLine("STLType3D=" + STLType3D);
                builder.AppendLine("STLType4D=" + STLType4D);
                builder.AppendLine("STLType5D=" + STLType5D);
                builder.AppendLine("Type1kD=" + Type1kD);
                builder.AppendLine("Type1tD=" + Type1tD);
                builder.AppendLine("Type2aD=" + Type2aD);
                builder.AppendLine("Type3aD=" + Type3aD);
                builder.AppendLine("Type3kD=" + Type3kD);
                builder.AppendLine("Type4kD=" + Type4kD);
                builder.AppendLine("Type5kD=" + Type5kD);
                builder.AppendLine("Type5wD=" + Type5wD);
                builder.AppendLine("UiiMP=" + UiiMP);
                builder.AppendLine("UisMP=" + UisMP);
                builder.AppendLine("UsiMP=" + UsiMP);
                builder.AppendLine("UssMP=" + UssMP);
                builder.AppendLine("mRRCap=" + mRRCap);

                File.WriteAllText(fileName, builder.ToString());
            }
        }

        /// <summary>
        /// Given a pipe breakage probability, calculate the pipe leakage probability
        /// </summary>
        /// <param name="breakProbability">A float value between 0 and 1 (passed as a String type).</param>
        /// <returns>Leak probability using the rule "break + leak = 1.0".</returns>
        public static string CalculateLeakProbability(string breakProbability)
        {
            double breakVal = 0.0;

            if (!String.IsNullOrEmpty(breakProbability))
            {
                breakVal = Convert.ToDouble(breakProbability);
                // Force the break value between 0 and 1
                breakVal = Clamp(breakVal, 0.0, 1.0);
            }

            // Return the difference from 1.0 as the leak probability
            return (1 - breakVal).ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Clamps the given value between the specified minimum and maximum values.
        /// </summary>
        /// <param name="value">The value to be clamped.</param>
        /// <param name="min">The largest minimum value that will be returned.</param>
        /// <param name="max">The smallest maximum value that will be returned.</param>
        /// <returns></returns>
        public static double Clamp(double value, double min, double max)
        {
            return (value < min) ? min : (value > max) ? max : value;
        }
    }
}
