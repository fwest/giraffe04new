﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Giraffe
{
    public class ParameterDefines
    {
        /// <summary>
        /// Stores the output file path for the simulation
        /// </summary>
        static public string _moveFilePath = string.Empty;
        public string MoveFilePath
        {
            get { return _moveFilePath; }
            set { _moveFilePath = value; }
        }
        
        /// <summary>
        /// Stores the convergence criteria message
        /// </summary>
        public string DisplayMessageBox { get; set; }
        
        /// <summary>
        /// default LEAKL parameter
        /// </summary>
        const double LEAKL = 0.5;

        /// <summary>
        /// default LEAKC parameter
        /// </summary>
        const int LEAKC =	1000000;

        /// <summary>
        /// default LEAKD parameter
        /// </summary>
        const int LEAKD	= 2;

        /// <summary>
        /// default LEAKM parameter
        /// </summary>
        const int LEAKM = 1;

        /// <summary>
        /// default output path
        /// </summary>
        const string OUTPUTPATH	= @".\Giraffe_Output\";

        /// <summary>
        /// default output file
        /// </summary>
        const string OUTPUTFILE = @"OutputFileTest.rpt";
        
        /// <summary>
        /// get/set if the appendix process is done
        /// </summary>
        public bool AppendixProcessDone { get; set; }

        /// <summary>
        /// contains the defaults paramters loaded into the system upon
        /// start up (Main)
        /// </summary>
        protected Hashtable ConfigurationInformation = new Hashtable();

        /// <summary>
        /// get the specified element value
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string Default(string key)
        {
            return (string)(ConfigurationInformation[key]);
        }

        /// <summary>
        /// set the specified element value
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void Default(string key, string value)
        {
            ConfigurationInformation[key] = value;
        }

        /// <summary>
        /// does the key exist
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool ContainsKey(string key)
        {
            return ConfigurationInformation.ContainsKey(key);
        }

        /// <summary>
        /// how many elements in the instance?
        /// </summary>
        public int Count
        {
            get { return ConfigurationInformation.Count; }
        }

        /// <summary>
        /// c'tor
        /// </summary>
        public ParameterDefines()
        {
            AppendixProcessDone = false;
        }

        /// <summary>
        /// get the system path
        /// </summary>
        /// <returns></returns>
        static public string GetSystem()
        {
            // return the executing assemblies path if property not set
            if(_moveFilePath == "")
            {
                return GetSystemDefault();
	        }
	        else
            {
		        return _moveFilePath;
	        }

        }

        /// <summary>
        /// return the executing assembly path
        /// </summary>
        /// <returns></returns>
        static public string GetSystemDefault()
        {
            return Directory.GetCurrentDirectory();
        }

        /// <summary>
        /// sets the initial output file path with the location of giraffe.exe
        /// </summary>
        static public void SetPath()
        {            
	        if(!Directory.Exists(OUTPUTPATH))
            {
                Directory.CreateDirectory(OUTPUTPATH);
	        }
        }
        
        
        /// <summary>
        /// loads the configuration specified by the user -- if no configuration is 
        /// specified then load the default configuration
        /// </summary>
        /// <param name="FileName"></param>
        public void LoadConfiguration(string fileName)
        {
            string fileToRead;
            if (fileName == string.Empty)
                fileToRead = GetSystemDefault() + "\\Default.txt";
            else
                fileToRead = fileName;

            string fileBody = System.IO.File.ReadAllText(fileToRead);

            string[] lines = fileBody.Split(Environment.NewLine.ToCharArray());

            foreach(string line in lines)
            {
                try
                {
                    string[] keyValue = line.Split('=');
                    ConfigurationInformation[keyValue[0]] = keyValue[1];
                }
                catch (SystemException sysex) { Console.WriteLine(sysex.Message); }
            }
        }
    }
}
