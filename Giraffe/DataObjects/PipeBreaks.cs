﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Giraffe
{
    /// <summary>
    /// represents a pipe break instance
    /// </summary>
    [Serializable]
    public class PipeBreak : NodeBase<PipeBreak>
    {
        public const string FROM_FILE_ID = "Pipe_Break_Information";

        public float PipeBreakPreRatio { get; set; }

        public float PipeBreakRatio { get; set; }

        public int PipeBreakRepairNo { get; set; }

        public int PipeBreakNo { get; set; }

        public int PipeBreakLeakNo { get; set; }

        public int PipeBreakPreIndex { get; set; }

        /// <summary>
        /// c'tor
        /// </summary>
        public PipeBreak()
        {
            Type = NodeTypes.pipebreak;
        }

        /// <summary>
        /// put in string format for output file creation
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Empty; // TBD
        }
    }

    public class PipeBreaks : OrderedDictionary
    {
        public string PrintPipeBreakInfo()
        {
            StringBuilder fileOutput = new StringBuilder();

            // create the pipe break header
            fileOutput.AppendFormat("{0}{1}", "[Pipe_Break_Information]", Environment.NewLine);

            fileOutput.AppendFormat("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}{7}{8}",
                                    "PipeID",
                                    "PreRatio",
                                    "BreakRatio",
                                    "RepairNo",
                                    "BreakNo",
                                    "LeakNo",
                                    "PreIndex",
                                    Environment.NewLine,
                                    Environment.NewLine);

            // output the pipe break info
            foreach (DictionaryEntry debreak in this)
            {
                PipeBreak pipeBreak = (PipeBreak)debreak.Value;

                fileOutput.AppendFormat("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}{7}",
                                    pipeBreak.ID,
                                    pipeBreak.PipeBreakPreRatio.ToString(),
                                    pipeBreak.PipeBreakRatio.ToString(),
                                    pipeBreak.PipeBreakRepairNo.ToString(),
                                    pipeBreak.PipeBreakNo.ToString(),
                                    pipeBreak.PipeBreakLeakNo.ToString(),
                                    pipeBreak.PipeBreakPreIndex.ToString(),
                                    Environment.NewLine);
            }

            return fileOutput.ToString();
        }

        /// <summary>
        /// from a passed section string load the collection
        /// </summary>
        public void Load(string info)
        {
            string[] lines = info.Split(Environment.NewLine.ToCharArray());

            if (lines.Length > 3)
            {            
                int start = 3;
                while (start < lines.Length)
                {
                    if (lines[start].Trim() != "")
                    {
                        string[] fields = lines[start].Split('\t');
                        if (fields.Length >= 7)
                        {
                            PipeBreak pbreak = new PipeBreak();
                            pbreak.ID = fields[0];
                            pbreak.PipeBreakPreRatio = Convert.ToSingle(fields[1]);
                            pbreak.PipeBreakRatio = Convert.ToSingle(fields[2]);
                            pbreak.PipeBreakRepairNo = Convert.ToInt32(fields[3]);
                            pbreak.PipeBreakNo = Convert.ToInt32(fields[4]);
                            pbreak.PipeBreakLeakNo = Convert.ToInt32(fields[5]);
                            pbreak.PipeBreakPreIndex = Convert.ToInt32(fields[6]);

                            this.Add(System.Guid.NewGuid(), pbreak);                            
                        }                        
                    }

                    ++start;
                }
            }
        }
    }
}
