﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Giraffe
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class VerticeDetail
    {
        /// <summary>
        /// the x coordinate on the x,y plane
        /// </summary>
        public float XCoordinate { get; set; }

        /// <summary>
        /// the y coordinate on the x,y plane
        /// </summary>
        public float YCoordinate { get; set; }
    }

    /// <summary>
    /// represents an instance of a vertice -- optional with EAPNET
    /// </summary>
    [Serializable]
    public class Vertice : NodeBase<Vertice>
    {
        public const string NODE_ID = @"VERTICES";

        /// <summary>
        /// there may be more than one coordinate pair per Link ID
        /// </summary>
        public OrderedDictionary Details { get; set; }

        /// <summary>
        /// c'tor
        /// </summary>
        public Vertice()
        {
            Type = NodeTypes.vertice;
            Details = new OrderedDictionary();
        }

        /// <summary>
        /// put in string format for output file creation
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();

            foreach (DictionaryEntry detailde in Details)
            {
                VerticeDetail detail = (VerticeDetail)detailde.Value;

                //Console.WriteLine(detail.XCoordinate.ToString());
                //Console.WriteLine(detail.XCoordinate.ToString("F5"));
                //double d = detail.YCoordinate;
                //Console.WriteLine(d.ToString("F4"));
                //Console.WriteLine(d);

                double dX = detail.XCoordinate;
                double dY = detail.YCoordinate;

                builder.AppendFormat("{0}\t{1}\t{2}{3}", this.ID, dX.ToString("F4"), dY.ToString("F4"), Environment.NewLine);
            }
            
            return builder.ToString().Trim();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class Vertices : OrderedDictionary
    {
        /// <summary>
        /// Load up the vertices collection
        /// </summary>
        /// <param name="?"></param>
        public void LoadVertices(string elements)
        {
            try
            {
                // wipe out any existing data
                if (this.Count > 0)
                    this.Clear();

                string[] pieces = elements.Split(Environment.NewLine.ToCharArray());
                foreach (string piece in pieces)
                {
                    string tempPiece = piece.Trim();
                    if ((tempPiece != string.Empty) && (tempPiece[0] != Node.COMMENT[0]))
                    {
                        Vertice newVertice = new Vertice();
                        string[] coords = tempPiece.Split("\t".ToCharArray());
                        if (coords.Length == 3)
                        {
                            newVertice.ID = coords[0];
                            if (!(this.Contains(newVertice.ID)))
                                this.Add(newVertice.ID, newVertice);
                            
                            VerticeDetail newDetail = new VerticeDetail();
                            newDetail.XCoordinate = Convert.ToSingle(coords[1]);
                            newDetail.YCoordinate = Convert.ToSingle(coords[2]);
                            ((Vertice)(this[newVertice.ID])).Details.Add(System.Guid.NewGuid(), newDetail);                           
                        }
                    }
                }
            }
            catch (SystemException sysex)
            {
                Console.WriteLine(sysex.Message);
            }
        }

        /// <summary>
        /// Method that add the new vertices to the system, for display purposes only
        /// </summary>
        /// <param name="pipe"></param>
        /// <param name="draw"></param>
        /// <param name="type"></param>
        public void AddVertices(Pipe pipe, DrawVertices draw, string type)
        {           
            if(type.ToLower() == "break")
            {
                if(draw.RepairNode == 1)
                { // here is the issue on the vertice value being off
                    Vertice vert1 = new Vertice();
                    vert1.ID = "A10" + pipe.ID;
                    VerticeDetail vertDetail1 = new VerticeDetail();
                    var tempx = draw.UpNodeX + (draw.Ratio - 0.05f) * draw.PipeL * Math.Cos( draw.Angle );
                    vertDetail1.XCoordinate = (float)tempx;
                    var tempy = draw.UpNodeY + (draw.Ratio - 0.05f) * draw.PipeL * Math.Sin( draw.Angle );
                    vertDetail1.YCoordinate = (float)tempy;
                    vert1.Details.Add(System.Guid.NewGuid(), vertDetail1);                    

                    Vertice vert2 = new Vertice();
                    vert2.ID = "A20" + pipe.ID;
                    VerticeDetail vertDetail2 = new VerticeDetail();
                    tempx = draw.UpNodeX + (draw.Ratio + 0.05f) * draw.PipeL * Math.Cos( draw.Angle );
                    vertDetail2.XCoordinate = (float)tempx;
                    tempy = draw.UpNodeY + (draw.Ratio + 0.05f) * draw.PipeL * Math.Sin( draw.Angle );
                    vertDetail2.YCoordinate = (float)tempy;
                    vert2.Details.Add(System.Guid.NewGuid(), vertDetail2);
                    
                    this.Add(vert1.ID, vert1);
                    this.Add(vert2.ID, vert2);
                 
                }
                else if(draw.RepairNode > 1)
                {
                    if((draw.BreakNo == 1) && (draw.LeakNo == 0))
                    {
                        Vertice vert1 = new Vertice();                        
                        vert1.ID = "A10" + pipe.ID;
                        VerticeDetail detail1 = new VerticeDetail();
                        var tempx = draw.UpNodeX + (draw.Ratio - 0.05f) * draw.PipeL * Math.Cos( draw.Angle );
                        detail1.XCoordinate = (float)tempx;
                        var tempy = draw.UpNodeY + (draw.Ratio - 0.05f) * draw.PipeL * Math.Sin( draw.Angle );
                        detail1.YCoordinate = (float)tempy;
                        vert1.Details.Add(System.Guid.NewGuid(), detail1);
                        this.Add(vert1.ID, vert1);
                    }
                    else 
                    {
                        if(draw.RepairNode == (draw.BreakNo + draw.LeakNo))
                        {
                            Vertice vert1 = new Vertice();                                                       
                            vert1.ID = "A" + (draw.BreakNo + draw.LeakNo).ToString().Trim() + "0" + pipe.ID;
                            VerticeDetail detail1 = new VerticeDetail();
                            var tempx = draw.UpNodeX + (draw.Ratio - 0.05f) * draw.PipeL * Math.Cos( draw.Angle );
                            detail1.XCoordinate = (float)tempx;
                            var tempy = draw.UpNodeY + (draw.Ratio - 0.05f) * draw.PipeL * Math.Sin( draw.Angle );
                            detail1.YCoordinate = (float)tempy;
                            vert1.Details.Add(System.Guid.NewGuid(), detail1);
                            
                            Vertice vert2 = new Vertice();                            
                            vert2.ID = "A" + (draw.BreakNo + draw.LeakNo + 1).ToString().Trim() + "0" + pipe.ID;
                            VerticeDetail detail2 = new VerticeDetail();
                            tempx = draw.UpNodeX + (draw.Ratio + 0.05f) * draw.PipeL * Math.Cos(draw.Angle);
                            detail2.XCoordinate = (float)tempx;
                            tempy = draw.UpNodeY + (draw.Ratio + 0.05f) * draw.PipeL * Math.Sin(draw.Angle);
                            detail2.YCoordinate = (float)tempy;
                            vert2.Details.Add(System.Guid.NewGuid(), detail2);
                            this.Add(vert1.ID, vert1);
                            this.Add(vert2.ID, vert2);
                           
                            if(draw.PreIndex != 0)
                            {
                                Vertice vert3 = new Vertice();
                                VerticeDetail detail3 = new VerticeDetail();
                                vert3.ID = "A" + (draw.BreakNo + draw.LeakNo).ToString().Trim() + "0" + pipe.ID;
                                tempx = draw.UpNodeX + (draw.PreRatio - 0.05f) * draw.PipeL * Math.Cos( draw.Angle );
                                detail3.XCoordinate = (float)tempx;
                                tempy = draw.UpNodeY + (draw.PreRatio - 0.05f) * draw.PipeL * Math.Sin( draw.Angle );
                                detail3.YCoordinate = (float)tempy;
                                vert3.Details.Add(System.Guid.NewGuid(), detail3);
                                this.Add(vert3.ID, vert3);
                            }
                        }
                        else 
                        {
                            Vertice vert4 = new Vertice();
                            VerticeDetail detail4 = new VerticeDetail();

                            vert4.ID = "A" + (draw.BreakNo + draw.LeakNo).ToString().Trim() + "0" + pipe.ID;
                            var tempx = draw.UpNodeX + (draw.Ratio - 0.05f) * draw.PipeL * Math.Cos( draw.Angle );
                            detail4.XCoordinate = (float)tempx;
                            var tempy = draw.UpNodeY + (draw.Ratio - 0.05f) * draw.PipeL * Math.Sin( draw.Angle );
                            detail4.YCoordinate = (float)tempy;
                            vert4.Details.Add(System.Guid.NewGuid(), detail4);
                            this.Add(vert4.ID, vert4);
                          
                            if(draw.PreIndex != 0)
                            {
                                Vertice vert5 = new Vertice();
                                VerticeDetail detail5 = new VerticeDetail();

                                vert5.ID = "A" + (draw.BreakNo + draw.LeakNo + 1).ToString().Trim() + "0" + pipe.ID;
                                tempx = draw.UpNodeX + (draw.Ratio - 0.05f) * draw.PipeL * Math.Cos( draw.Angle );
                                detail5.XCoordinate = (float)tempx;
                                tempy = draw.UpNodeY + (draw.Ratio - 0.05f) * draw.PipeL * Math.Sin( draw.Angle );
                                detail5.YCoordinate = (float)tempy;
                                vert5.Details.Add(System.Guid.NewGuid(), detail5);
                                this.Add(vert5.ID, vert5);
                            }
                        }
                    }
                }
            }
            else 
            {
                if(draw.PreIndex == 1)
                {
                    Vertice vert6 = new Vertice();
                    VerticeDetail detail6 = new VerticeDetail();

                    vert6.ID = "A" + (draw.BreakNo + draw.LeakNo).ToString().Trim() + "0" + pipe.ID;
                    var tempx = draw.UpNodeX + (draw.Ratio - 0.05f) * draw.PipeL * Math.Cos(draw.Angle);
                    detail6.XCoordinate = (float)tempx;
                    var tempy = draw.UpNodeY + (draw.Ratio - 0.05f) * draw.PipeL * Math.Sin(draw.Angle);
                    detail6.YCoordinate = (float)tempy;
                    vert6.Details.Add(System.Guid.NewGuid(), detail6);
                    this.Add(vert6.ID, vert6);
                }                
            }
        }

        private string cache = string.Empty;
        private bool dirty = false;

        /// <summary>
        /// put the entire collection into an output string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {            
            if (dirty || (cache.Length == 0))
            {
                //cache = Node.CollectionToString(Vertice.NODE_ID, this);
                cache = Node.CollectionToString(Vertice.NODE_ID, MyOrderedDictionary.Ordered(this));
                dirty = false;
            }

            return cache;
        }

        /// <summary>
        /// cache for performance
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public new void Add(object key, object value)
        {
            dirty = true;
            base.Add(key, value);
        }

        /// <summary>
        /// cache for performance
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public new void Remove(object key)
        {
            dirty = true;
            base.Remove(key);
        }
    }
}
