﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Giraffe
{
    /// <summary>
    /// represents a leak ratio instance
    /// </summary>
    [Serializable]
    public class LeakRatio : NodeBase<LeakRatio>
    {
        /// <summary>
        /// the actual ratio value
        /// </summary>
        public float Ratio { get; set; }

        /// <summary>
        /// c'tor
        /// </summary>
        public LeakRatio()
        {
            Type = NodeTypes.leakratio;
        }

        /// <summary>
        /// put in string format for output file creation
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Empty; // TBD
        }
    }

    /// <summary>
    /// a collection of leak ratios
    /// </summary>
    public class LeakRatios : OrderedDictionary
    {
    }
}
