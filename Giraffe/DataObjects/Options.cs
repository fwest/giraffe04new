﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Giraffe
{    
    [Serializable]
    public class Option : NodeBase<Option>
    {
        public const string NODE_ID = @"OPTIONS";

        /// <summary>
        /// use this option if we are after a character based value
        /// </summary>
        public string OptionValue { get; set; }       

        /// <summary>
        /// use this property if we are after a double
        /// </summary>
        public string OptionDoubleValue { get; set; }

        /// <summary>
        /// if we need to store a filename as part of the option use this
        /// property
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// c'tor
        /// </summary>
        public Option()
        {
            Type = NodeTypes.option;
        }
        
        /// <summary>
        /// put in string format for output file creation
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            if (OptionValue != null)
                builder.AppendFormat("{0} {1}", ID, OptionValue);
            else
            {
                string dv = OptionDoubleValue;
                builder.AppendFormat("{0} {1}", ID, dv);
            }
            return builder.ToString().Trim();
        }
    }
  
    [Serializable]
    public class Options : OrderedDictionary
    {
        private string originalString = string.Empty;

        /// <summary>
        /// Load up the options collection
        /// </summary>
        /// <param name="?"></param>
        public void LoadOptions(string elements)
        {
            originalString = elements;

            try
            {
                // wipe out any existing data
                if (this.Count > 0)
                    this.Clear();

                string[] pieces = elements.Split(Environment.NewLine.ToCharArray());
                foreach (string piece in pieces)
                {
                    string tempPiece = piece.Trim();
                    if ((tempPiece != string.Empty) && (tempPiece[0] != Node.COMMENT[0]))
                    {
                        string[] options = tempPiece.Split(' ');

                        Option newOption = new Option();                        

                        // check for multi-string identifiers
                        newOption.ID = GetOptionID(options);

                        switch (newOption.ID.ToUpper())
                        {
                            case "UNITS":
                            case "HEADLOSS":
                            case "QUALITY":
                            case "UNBALANCED":
                            case "PATTERN":
                            case "MAP":
                            case "HYDRAULICS":
                                newOption.OptionValue = NodeBase<Option>.GetNextString(options);
                                break;                            

                            default:
                                newOption.OptionValue = null;
                                newOption.OptionDoubleValue = NodeBase<Option>.GetNextDoubleString(options);
                                break;
                        }

                        // use the input line to satisfy the need for a unique key        
                        this.Add(tempPiece, newOption);
                    }
                }
            }
            catch (SystemException sysex)
            {
                Console.WriteLine(sysex.Message);
            }
        }        

        /// <summary>
        /// ID strings may either be single or two string total
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        private string GetOptionID(string[] options)
        {
            string result = options[0];

            switch (result.ToUpper())
            {
                case "SPECIFIC":
                case "DEMAND":
                case "EMITTER":
                    result += " " + options[1];
                    break;

                default:
                    break;
            }

            return result;
        }

        /// <summary>
        /// put the entire collection into an output string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {            
            return Node.CollectionToString(Option.NODE_ID, this);
        }
    }
}
