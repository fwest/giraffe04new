﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Giraffe
{
    [Serializable]
    public enum ControlStatusType
    {
        ValveSetting = 0,           // should be a float value
        ValveBinarySetting,         // Open or Closed
        PipeShutOffValveSetting,    // Open, Closed -- considered part of a pipe (node ID)
        PumpSpeedSetting,
    }

    /// <summary>
    /// For controls -- very important note to self
    /// The links represent pipes, pumps, and control valves. 
    /// The nodes represent junctions, tanks, and reservoirs.
    /// 
    /// They are statements expressed in one of the following three formats: 
    ///     LINK x status IF NODE y ABOVE/BELOW z 
    ///     LINK x status AT TIME t 
    ///     LINK x status AT CLOCKTIME c AM/PM 
    ///
    ///     where: 
    ///
    ///     x 	= 	a link ID label, [Link]
    ///     status = OPEN or CLOSED, a pump speed setting, or a control valve setting [Open] true or false
    ///     y 	= 	a node IDlabel, [Node]
    ///     z 	= 	a pressure for a junction or a water level for a tank, [PressureOrLevel]
    ///     t 	= 	a time since the start of the simulation in decimal hours or in hours:minutes notation, [TimeSince]
    ///     c 	= 	a 24-hour clock time. [ClockTime]
    /// </summary>
    [Serializable]
    public class Control : NodeBase<Control>
    {        
        public const string NODE_ID = @"CONTROLS";
        public const string CONTROL_PREFACE = @"LINK";
        public const string VALVE_PUMP_ID = @"NODE";
        public const string CLOCK_ID = @"CLOCKTIME";
        public const string TIME_ID = @"At Time";
        public const string NODE_IF = @"If Node";

        public ControlStatusType StatusType { get; set; }

        /// <summary>
        /// a specified node can have more than one control per ID -
        /// </summary>
        public OrderedDictionary Details { get; set; }        

        /// <summary>
        /// c'tor
        /// </summary>
        public Control(ControlStatusType type, string controlID)
        {
            Type = NodeTypes.control;
            Details = new OrderedDictionary();
            ID = controlID;
            StatusType = type;
        }

        /// <summary>
        /// put in string format for output file creation
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {                        
            StringBuilder subBuilder = new StringBuilder();
            foreach (DictionaryEntry detailde in this.Details)
            {
                ControlDetail detail = (ControlDetail)detailde.Value;
                
                subBuilder.AppendFormat("{0} {1}\t", CONTROL_PREFACE.Trim(), this.ID.Trim());

                // if we have a numeric setting use that
                if (detail.OpenClose != BinaryValveStatus.NotDefined)
                    subBuilder.AppendFormat("{0}\t", detail.OpenClose.ToString().Trim());
                else
                {
                    float dset = detail.Setting;
                    subBuilder.AppendFormat("{0}\t", dset);
                }
                    
                if (detail.ElapsedTimeBased)
                {
                    double since = detail.TimeSince;
                    subBuilder.AppendFormat("{0}  {1}\t\t", TIME_ID, since.ToString("F6"));                    
                }
                else if (detail.ClockTimeBased)
                {
                    double ctime = detail.ClockTime;
                    subBuilder.AppendFormat("{0}  {1}", CLOCK_ID, ctime.ToString());
                    if (detail.AmPm != DayNight.NotDefined)
                        subBuilder.AppendFormat(" {0}\t\t", detail.AmPm.ToString().Trim());
                    else
                        subBuilder.AppendFormat("\t\t");
                }
                else
                {
                    float pl = detail.PressureOrLevel;
                    subBuilder.AppendFormat("{0}  {1}\t{2}\t{3}", NODE_IF,
                                    detail.NodeID.Trim(),
                                    detail.AboveBelow.ToString().Trim(),
                                    pl.ToString().Trim());
                }

                subBuilder.AppendFormat("{0}", Environment.NewLine);
            }

            return subBuilder.ToString().Trim();
        }
    }

    [Serializable]
    public class ControlDetail
    {
        /// <summary>
        /// stores exactly what we received
        /// </summary>
        public string ControlExpression { get; set; }

        /// <summary>
        /// a link ID
        /// </summary>
        public string Link { get; set; }

        /// <summary>
        /// a node ID
        /// </summary>
        public string NodeID { get; set; }

        /// <summary>
        /// a pressure for a juntion or a water tank level
        /// </summary>
        public float PressureOrLevel { get; set; }

        /// <summary>
        /// the "time since" the start of the simulation in decimal hours
        /// </summary>
        public float TimeSince { get; set; }

        /// <summary>
        /// 24 clock time, e.g. 8 AM, 10 PM
        /// </summary>
        public float ClockTime { get; set; }

        // depending on the StatusType, query these status values

        /// <summary>
        /// use if pump or valve setting
        /// </summary>
        public float Setting { get; set; }        

        /// <summary>
        /// use if StatusType <> (ValveSetting or PumpSpeedSetting)
        /// </summary>
        public BinaryValveStatus OpenClose { get; set; }

        /// <summary>
        /// Above or Below a level or pressure
        /// </summary>
        public BinaryPressureLevel AboveBelow { get; set; }

        /// <summary>
        /// is this control based upon passed time from start?
        /// </summary>
        public bool ElapsedTimeBased { get; set; }

        /// <summary>
        /// is this control based upon a certain clock time?
        /// </summary>
        public bool ClockTimeBased { get; set; }

        /// <summary>
        /// AM or PM if clock time based?
        /// </summary>
        public DayNight AmPm { get; set; }

        /// <summary>
        /// c'tor
        /// </summary>
        public ControlDetail(ControlStatusType type, string info)
        {
            try
            {
                // save off what we received, good for debug
                ControlExpression = info;

                // default value 
                OpenClose = BinaryValveStatus.NotDefined;

                // get x = a link ID label
                string[] tabSegs = info.Split("\t".ToCharArray());
                string[] getX = tabSegs[0].Split(' ');
                Link = getX[1];

                // we are open, closed - or numeric (float)
                if (tabSegs[1].ToUpper().Trim() == BinaryValveStatus.Closed.ToString().ToUpper())
                    OpenClose = BinaryValveStatus.Closed;
                else if (tabSegs[1].ToUpper().Trim() == BinaryValveStatus.Open.ToString().ToUpper())
                    OpenClose = BinaryValveStatus.Open;
                else
                    Setting = Convert.ToSingle(tabSegs[1]);
               
                if ((info.ToUpper().IndexOf(Control.TIME_ID.ToUpper()) > -1)) 
                {
                    // we have an elapsed time control
                    ElapsedTimeBased = true;
                    string[] elapsedTime = tabSegs[2].Split(' ');
                    if (elapsedTime.Length > 2)
                    {
                        for (int counter = 2; counter < elapsedTime.Length; counter++)
                        {
                            if (elapsedTime[counter] != string.Empty)
                            {
                                TimeSince = (float)Convert.ToSingle(elapsedTime[counter]);                                
                                break;
                            }
                        }
                    }           
                }
                else if (info.ToUpper().IndexOf(Control.CLOCK_ID.ToUpper()) > -1)
                {
                    // we have a clock time control
                    ClockTimeBased = true;
                    string[] clockTime = tabSegs[2].Split(' ');
                    if (clockTime.Length > 2)
                    {
                        for (int counter = 2; counter < clockTime.Length; counter++)
                        {
                            if (clockTime[counter] != string.Empty)
                            {
                                ClockTime = (float)Convert.ToSingle(clockTime[counter]);
                                AmPm = DayNight.NotDefined;
                                if ((counter + 1) < clockTime.Length)
                                {
                                    if (clockTime[counter + 1].ToUpper() == DayNight.AM.ToString().ToUpper())
                                        AmPm = DayNight.AM;
                                    else
                                        AmPm = DayNight.PM;
                                }
                            }
                        }
                    } 
                }
                else
                {                        
                    // now -- get the node ID
                    string[] getNode = tabSegs[2].Split(' ');

                    // we need to skip over blanks, could be any number of them
                    for (int counter = 2; counter < getNode.Length; counter++)
                    {
                        if (getNode[counter] != string.Empty)
                        {
                            NodeID = getNode[counter].Trim();
                            break;
                        }
                    }

                    if (tabSegs[3].ToUpper().Trim() == BinaryPressureLevel.Above.ToString().ToUpper())
                        AboveBelow = BinaryPressureLevel.Above;
                    else
                        AboveBelow = BinaryPressureLevel.Below;

                    // get the tank level or valve pressure
                    PressureOrLevel = Convert.ToSingle(tabSegs[4]);
                }
            }
            catch(SystemException sysex)
            {
                Console.WriteLine(sysex.Message);
            }
        }        
    }

    public class Controls : OrderedDictionary
    {
        /// <summary>
        /// Load up the simulation controls
        /// </summary>
        /// <param name="?"></param>
        public void LoadControls(OrderedDictionary pipes, OrderedDictionary valves, string elements)
        {
            try
            {
                // wipe out any existing data
                if (this.Count > 0)
                    this.Clear();

                string[] pieces = elements.Split(Environment.NewLine.ToCharArray());
                foreach (string piece in pieces)
                {
                    string tempPiece = piece.Trim();
                    if ((tempPiece != string.Empty) && (tempPiece[0] != Node.COMMENT[0]))
                    {
                        string[] vals = tempPiece.Split("\t".ToCharArray());

                        // first - get the identifying link ID. A link may be a pipe, pump, or valve
                        // a node is a junction, tank or reservoir. Then -- instantiate the control
                        string[] linkID = vals[0].Split(' ');

                        if (linkID.Length >= 2)
                        {
                            if (!(this.Contains(linkID[1])))
                            {
                                Control newControl;
                                if (pipes.Contains(linkID[1]))
                                    newControl = new Control(ControlStatusType.PipeShutOffValveSetting, linkID[1]);
                                else if (valves.Contains(linkID[1]))
                                {  
                                    if ((tempPiece.ToUpper().IndexOf(BinaryValveStatus.Closed.ToString().ToUpper()) != -1) ||
                                        (piece.ToUpper().IndexOf(BinaryValveStatus.Open.ToString().ToUpper()) != -1))
                                        newControl = new Control(ControlStatusType.ValveBinarySetting, linkID[1]);
                                    else
                                        newControl = new Control(ControlStatusType.ValveSetting, linkID[1]);
                                }
                                else
                                    newControl = new Control(ControlStatusType.PumpSpeedSetting, linkID[1]);

                                this.Add(newControl.ID, newControl);
                            }
                            
                            // add the detail line to this control (one to many)
                            Control tempControl = (Control)this[linkID[1]];
                            ControlDetail newDetail = new ControlDetail(tempControl.StatusType, tempPiece);
                            // the identifying guid satisfies the unique key need, no other correlation
                            tempControl.Details.Add(Guid.NewGuid(), newDetail);                            
                        }
                    }
                }
            }
            catch (SystemException sysex)
            {
                Console.WriteLine(sysex.Message);
            }
        }

        /// <summary>
        /// put the entire collection into an output string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {            
            return Node.CollectionToString(Control.NODE_ID, MyOrderedDictionary.Ordered(this));
        }

        public string ToString(bool weirdSort)
        {
            if ((weirdSort) && (!wsortApplied))
                // apply the weird sorting on the control details
                ApplyWeirdSort();

            return this.ToString();
        }

        private static bool wsortApplied = false;
        /// <summary>
        /// order seems to matter with the tanks as the controls 
        /// </summary>
        public void ApplyWeirdSort()
        {
            wsortApplied = true;
            foreach (DictionaryEntry de in this)
            {
                Control cde = (Control)de.Value;
                if (cde.Details.Count > 2)
                {
                    ControlDetail[] details = new ControlDetail[cde.Details.Count];
                    details[0] = (ControlDetail)cde.Details[0];

                    for (int i = 1; i < cde.Details.Count; i++)
                    {
                        details[i] = (ControlDetail)cde.Details[cde.Details.Count - i];
                    }
                    // out with the old order
                    cde.Details.Clear();
                    for (int j = 0; j < details.Length; j++)
                    {
                        // in with the new order
                        cde.Details.Add(System.Guid.NewGuid(), details[j]);
                    }
                }
            }
        }

        /// <summary>
        /// go though the collection, and if a control detail node matches the
        /// passed ID -- delete that detail. If a Controls detail collection is
        /// empty -- delete that control
        /// </summary>
        /// <param name="nodeID"></param>
        /// <returns></returns>
        public bool DeleteDetail(string nodeID)
        {
            bool returnValue = false;
            List<object> controlsToDelete = new List<object>();
            List<object> detailsToDelete = new List<object>();

            // this is inefficient -- but there are not many controls
            foreach (DictionaryEntry de in this)
            {
                Control control = (Control)de.Value;
                foreach (DictionaryEntry cde in control.Details)
                {
                    ControlDetail detail = (ControlDetail)cde.Value;
                    if (detail.NodeID == nodeID)
                    {
                        //control.Details.Remove(cde.Key);
                        detailsToDelete.Add(cde.Key);
                        returnValue = true;
                    }
                }

                // delete all the details we found that match
                foreach (object dkey in detailsToDelete)
                {
                    if (control.Details.Contains(dkey))
                        control.Details.Remove(dkey);
                }

                if (control.Details.Count == 0)
                    // this.Remove(de.Key);
                    controlsToDelete.Add(de.Key);
            }

            // delete any controls that do not have any details
            foreach (object conKey in controlsToDelete)
            {
                if (this.Contains(conKey))
                    this.Remove(conKey);
            }

            return returnValue;
        }
    }
}
