﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Giraffe
{
    /// <summary>
    /// represents a pipe leak instance
    /// </summary>
    [Serializable]
    public class PipeLeak : NodeBase<PipeLeak>
    {

        public const string FROM_FILE_ID = "Pipe_Leak_Information";

        public float PipeLeakD { get; set; }

        public float PipeLeakPreRatio { get; set; }

        public float PipeLeakRatio { get; set; }

        public int PipeLeakRepairNo { get; set; }

        public int PipeLeakBreakNo { get; set; }

        public int PipeLeakNo { get; set; }

        public int PipeLeakPreIndex { get; set; }

        /// <summary>
        /// c'tor
        /// </summary>
        public PipeLeak()
        {
            Type = NodeTypes.pipeleak;
        }

        /// <summary>
        /// put in string format for output file creation
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Empty; // TBD
        }
        
        /// <summary>
        /// returns the leak size to store in a Pipe Leak instance
        /// Algorithm identical to Giraffe 08
        /// </summary>
        /// <param name="rate"></param>
        /// <param name="randomNum"></param>
        /// <returns></returns>
        public static float LeakSize(Pipe pipe, ParameterDefines paramDefines, RepairRate rate, double randomNum)
        {
            float returnValue = 0;

            try
            {                           
                float _CIType1D = Convert.ToSingle(paramDefines.Default("CIType1D"));
                float _Type1kD = Convert.ToSingle(paramDefines.Default("Type1kD"));
                float _Type1tD = Convert.ToSingle(paramDefines.Default("Type1tD"));
                float _CIType2D = Convert.ToSingle(paramDefines.Default("CIType2D"));
                float _Type2aD = Convert.ToSingle(paramDefines.Default("Type2aD"));
                float _CIType3D = Convert.ToSingle(paramDefines.Default("CIType3D"));
                float _Type3kD = Convert.ToSingle(paramDefines.Default("Type3kD"));
                float _Type3aD = Convert.ToSingle(paramDefines.Default("Type3aD"));
                float _CIType4D = Convert.ToSingle(paramDefines.Default("CIType4D"));
                float _Type4kD = Convert.ToSingle(paramDefines.Default("Type4kD"));
                float _CIType5D = Convert.ToSingle(paramDefines.Default("CIType5D"));
                float _Type5kD = Convert.ToSingle(paramDefines.Default("Type5kD"));
                float _Type5wD = Convert.ToSingle(paramDefines.Default("Type5wD"));
                float _RSType1D = Convert.ToSingle(paramDefines.Default("RSType1D"));
                float _RSType2D = Convert.ToSingle(paramDefines.Default("RSType2D"));
                float _RSType3D = Convert.ToSingle(paramDefines.Default("RSType3D"));
                float _RSType4D = Convert.ToSingle(paramDefines.Default("RSType4D"));
                float _RSType5D = Convert.ToSingle(paramDefines.Default("RSType5D"));
                float _CONType1D = Convert.ToSingle(paramDefines.Default("CONType1D"));
                float _CONType2D = Convert.ToSingle(paramDefines.Default("CONType2D"));
                float _CONType3D = Convert.ToSingle(paramDefines.Default("CONType3D"));
                float _CONType4D = Convert.ToSingle(paramDefines.Default("CONType4D"));
                float _CONType5D = Convert.ToSingle(paramDefines.Default("CONType5D"));
                float _DIType1D = Convert.ToSingle(paramDefines.Default("DIType1D"));
                float _DIType2D = Convert.ToSingle(paramDefines.Default("DIType2D"));
                float _DIType3D = Convert.ToSingle(paramDefines.Default("DIType3D"));
                float _DIType4D = Convert.ToSingle(paramDefines.Default("DIType4D"));
                float _DIType5D = Convert.ToSingle(paramDefines.Default("DIType5D"));
                float _STLType1D = Convert.ToSingle(paramDefines.Default("STLType1D"));
                float _STLType2 = Convert.ToSingle(paramDefines.Default("STLType2"));
                float _STLType2D = Convert.ToSingle(paramDefines.Default("STLType2D"));
                float _STLType3D = Convert.ToSingle(paramDefines.Default("STLType3D"));
                float _STLType4D = Convert.ToSingle(paramDefines.Default("STLType4D"));
                float _STLType5D = Convert.ToSingle(paramDefines.Default("STLType5D"));

                float _OtherType1D = Convert.ToSingle(paramDefines.Default("OtherType1D"));
                float _OtherType2D = Convert.ToSingle(paramDefines.Default("OtherType2D"));
                float _OtherType3D = Convert.ToSingle(paramDefines.Default("OtherType3D"));
                float _OtherType4D = Convert.ToSingle(paramDefines.Default("OtherType4D"));
                float _OtherType5D = Convert.ToSingle(paramDefines.Default("OtherType5D"));

                switch (rate.RepairRateMaterial.ToUpper().Trim())
                {
                    case "CI":                        
                        if(randomNum <= _CIType1D)
                        {
                            returnValue = (float)(2 * Math.Sqrt(_Type1kD * pipe.PipeDiameter * _Type1tD));
                        }
                        else if((_CIType1D < randomNum) && (randomNum <= _CIType2D))
                        {
                            returnValue = (float)(Math.Sqrt(2 * _Type2aD / 180 * Math.PI) * pipe.PipeDiameter);
                        }
                        else if((_CIType2D < randomNum) && (randomNum <= _CIType3D))
                        {
                            returnValue = (float)(2 * Math.Sqrt(_Type3kD * _Type3aD / 180 * pipe.PipeDiameter));
                        }
                        else if((_CIType3D < randomNum) && (randomNum <= _CIType4D))
                        {
                            returnValue = (2 * _Type4kD * pipe.PipeDiameter);
                        }
                        else if((_CIType4D < randomNum) && (randomNum <= _CIType5D))
                        {
                            returnValue = (float)(2 * Math.Sqrt(_Type5kD * _Type5wD * pipe.PipeDiameter));
                        }

                        break;

                    case "RS":
                        if(randomNum <= _RSType1D)
                        {
                            returnValue = (float)(2 * Math.Sqrt(_Type1kD * pipe.PipeDiameter * _Type1tD));
                        }
                        else if((_RSType1D < randomNum) && (randomNum <= _RSType2D))
                        {
                            returnValue = (float)(Math.Sqrt(2 * _Type2aD / 180 * Math.PI) * pipe.PipeDiameter);
                        }
                        else if((_RSType2D < randomNum) && (randomNum <= _RSType3D))
                        {
                            returnValue = (float)(2 * Math.Sqrt(_Type3kD * _Type3aD / 180 * pipe.PipeDiameter));
                        }
                        else if((_RSType3D < randomNum) && (randomNum <= _RSType4D))
                        {
                            returnValue = (2 * _Type4kD * pipe.PipeDiameter);
                        }
                        else if((_RSType4D < randomNum) && (randomNum <= _RSType5D))
                        {
                            returnValue = (float)(2 * Math.Sqrt(_Type5kD * _Type5wD * pipe.PipeDiameter));
                        }
                        break;

                    case "CON":
                        if(randomNum <= _CONType1D)
                        {
                            returnValue = (float)(2 * Math.Sqrt(_Type1kD * pipe.PipeDiameter * _Type1tD));
                        }
                        else if((_CONType1D < randomNum) && (randomNum <= _CONType2D))
                        {
                            returnValue = (float)(Math.Sqrt(2 * _Type2aD / 180 * Math.PI) * pipe.PipeDiameter);
                        }
                        else if((_CONType2D < randomNum) && (randomNum <= _CONType3D))
                        {
                            returnValue = (float)(2 * Math.Sqrt(_Type3kD * _Type3aD / 180 * pipe.PipeDiameter));
                        }
                        else if((_CONType3D < randomNum) && (randomNum <= _CONType4D))
                        {
                            returnValue = (2 * _Type4kD * pipe.PipeDiameter);
                        }
                        else if((_CONType4D < randomNum) && (randomNum <= _CONType5D))
                        {
                            returnValue = (float)(2 * Math.Sqrt(_Type5kD * _Type5wD * pipe.PipeDiameter));
                        }
                        break;

                    case "DI":
                        if(randomNum <= _DIType1D)
                        {
                            returnValue = (float)(2 * Math.Sqrt(_Type1kD * pipe.PipeDiameter * _Type1tD));
                        }
                        else if((_DIType1D < randomNum) && (randomNum <= _DIType2D))
                        {
                            returnValue = (float)(Math.Sqrt(2 * _Type2aD / 180 * Math.PI) * pipe.PipeDiameter);
                        }
                        else if((_DIType2D < randomNum) && (randomNum <= _DIType3D))
                        {
                            returnValue = (float)(2 * Math.Sqrt(_Type3kD * _Type3aD / 180 * pipe.PipeDiameter));
                        }
                        else if((_DIType3D < randomNum) && (randomNum <= _DIType4D))
                        {
                            returnValue = (2 * _Type4kD * pipe.PipeDiameter);
                        }
                        else if((_DIType4D < randomNum) && (randomNum <= _DIType5D))
                        {
                            returnValue = (float)(2 * Math.Sqrt(_Type5kD * _Type5wD * pipe.PipeDiameter));
                        }
                        break;

                    case "STL":
                        if(randomNum <= _STLType1D)
                        {
                            returnValue = (float)(2 * Math.Sqrt(_Type1kD * pipe.PipeDiameter * _Type1tD));
                        }
                        else if((_STLType1D < randomNum) && (randomNum <= _STLType2))
                        {
                            returnValue = (float)(Math.Sqrt(2 * _Type2aD / 180 * Math.PI) * pipe.PipeDiameter);
                        }
                        else if((_STLType2D < randomNum) && (randomNum <= _STLType3D))
                        {
                            returnValue = (float)(2 * Math.Sqrt(_Type3kD * _Type3aD / 180 * pipe.PipeDiameter));
                        }
                        else if((_STLType3D < randomNum) && (randomNum <= _STLType4D))
                        {
                            returnValue = (2 * _Type4kD * pipe.PipeDiameter);
                        }
                        else if((_STLType4D < randomNum) && (randomNum <= _STLType5D))
                        {
                            returnValue = (float)(2 * Math.Sqrt(_Type5kD * _Type5wD * pipe.PipeDiameter));
                        }
                        break;

                    default:
                        // unexpected value
                        if(randomNum <= _OtherType1D)
                        {
                            returnValue = (float)(2 * Math.Sqrt(_Type1kD * pipe.PipeDiameter * _Type1tD));
                        }
                        else if((_OtherType1D < randomNum) && (randomNum <= _OtherType2D))
                        {
                            returnValue = (float)(Math.Sqrt(2 * _Type2aD / 180 * Math.PI) * pipe.PipeDiameter);
                        }
                        else if((_OtherType2D < randomNum) && (randomNum <= _OtherType3D))
                        {
                            returnValue = (float)(2 * Math.Sqrt(_Type3kD * _Type3aD / 180 * pipe.PipeDiameter));
                        }
                        else if((_OtherType3D < randomNum) && (randomNum <= _OtherType4D))
                        {
                            returnValue = (2 * _Type4kD * pipe.PipeDiameter);
                        }
                        else if((_OtherType4D < randomNum) && (randomNum <= _OtherType5D))
                        {
                            returnValue = (float)(2 * Math.Sqrt(_Type5kD * _Type5wD * pipe.PipeDiameter));
                        }
                        break;
                }
            }
            catch (SystemException sysex)
            {
                Console.WriteLine(sysex.Message);
            }

            return returnValue;
        }
    }

    public class PipeLeaks : OrderedDictionary
    {
        public const double LEAKL = 0.5;
        public const int LEAKC = 1000000;
        public const int LEAKD = 2;
        public const int LEAKM = 1;

        public string PrintPipeLeakInfo()
        {
            StringBuilder fileOutput = new StringBuilder();

            fileOutput.AppendFormat("{0}{1}", "[Pipe_Leak_Information]", Environment.NewLine);
            fileOutput.AppendFormat("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}{8}{9}",
                                    "PipeID",
                                    "LeakD",
                                    "PreRatio",
                                    "LeakRatio",
                                    "RepairNo",
                                    "BreakNo",
                                    "LeakNo",
                                    "PreIndex",
                                    Environment.NewLine,
                                    Environment.NewLine);

            // output the pipe leak info
            foreach (DictionaryEntry deleak in this)
            {
                PipeLeak pipeLeak = (PipeLeak)deleak.Value;

                fileOutput.AppendFormat("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}{8}",
                                pipeLeak.ID,
                                pipeLeak.PipeLeakD.ToString(),
                                pipeLeak.PipeLeakPreRatio.ToString(),
                                pipeLeak.PipeLeakRatio.ToString(),
                                pipeLeak.PipeLeakRepairNo.ToString(),
                                pipeLeak.PipeLeakBreakNo.ToString(),
                                pipeLeak.PipeLeakNo.ToString(),
                                pipeLeak.PipeLeakPreIndex.ToString(),
                                Environment.NewLine);
            }

            return fileOutput.ToString();
        }

        /// <summary>
        /// from a passed section string load the collection
        /// </summary>
        public void Load(string info)
        {
            string[] lines = info.Split(Environment.NewLine.ToCharArray());

            if (lines.Length > 3)
            {
                int start = 3;
                while (start < lines.Length)
                {
                    if (lines[start].Trim() != "")
                    {
                        string[] fields = lines[start].Split('\t');
                        if (fields.Length >= 8)
                        {
                            PipeLeak pleak = new PipeLeak();
                            pleak.ID = fields[0];
                            pleak.PipeLeakD = Convert.ToSingle(fields[1]);
                            pleak.PipeLeakPreRatio = Convert.ToSingle(fields[2]);
                            pleak.PipeLeakRatio = Convert.ToSingle(fields[3]);
                            pleak.PipeLeakRepairNo = Convert.ToInt32(fields[4]);
                            pleak.PipeLeakBreakNo = Convert.ToInt32(fields[5]);
                            pleak.PipeLeakNo = Convert.ToInt32(fields[6]);
                            pleak.PipeLeakPreIndex = Convert.ToInt32(fields[7]);
                            this.Add(System.Guid.NewGuid(), pleak);
                        }
                    }

                    ++start;
                }
            }
        }
    }
}
