﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Giraffe
{
    /// <summary>
    /// represents a reservoir instance
    /// </summary>
    [Serializable]
    public class Reservoir : NodeBase<Reservoir>
    {
        public const string NODE_ID = @"RESERVOIRS";

        public float ReservoirTotalHead { get; set; }
        
        public string Pattern { get; set; }

        /// <summary>
        /// c'tor
        /// </summary>
        public Reservoir()
        {
            Type = NodeTypes.reservoir;
        }

        /// <summary>
        /// put in string format for output file creation
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            double temp = this.ReservoirTotalHead;
            builder.AppendFormat("{0}\t{1}", this.ID, temp.ToString("F5"));
            return builder.ToString();
        }
    }

    public class Reservoirs : NodeOrderedDictionary
    {
        /// <summary>
        /// Load up the reservoirs collection
        /// </summary>
        /// <param name="?"></param>
        public void LoadReservoirs(string elements)
        {
            try
            {
                // wipe out any existing data
                if (this.Count > 0)
                    this.Clear();

                string[] pieces = elements.Split(Environment.NewLine.ToCharArray());
                foreach (string piece in pieces)
                {
                    string tempPiece = piece.Trim();

                    if ((tempPiece != string.Empty) && (tempPiece[0] != Node.COMMENT[0]))
                    {
                        string[] vals = tempPiece.Split("\t".ToCharArray());
                        if ((vals[0] != string.Empty) && (vals.Length >= 2)) // account for a pattern, original dropped
                        {
                            Reservoir newReservoir = new Reservoir();
                            newReservoir.ID = vals[0];
                            newReservoir.ReservoirTotalHead = Convert.ToSingle(vals[1]); // a single is a float
                            this.Add(newReservoir.ID, newReservoir);
                        }
                    }
                }
            }
            catch (SystemException sysex)
            {
                Console.WriteLine(sysex.Message);
            }
        }

        /// <summary>
        /// put the entire collection into an output string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {            
            return Node.CollectionToString(Reservoir.NODE_ID, MyOrderedDictionary.Ordered(this));
        }
    }
}
