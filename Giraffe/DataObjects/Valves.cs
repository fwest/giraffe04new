﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Giraffe
{
    /// <summary>
    /// represents a valve instance
    /// </summary>
    [Serializable]
    public class Valve : NodeBase<Valve>
    {
        public const string NODE_ID = @"VALVES";
       
        public string StartNode { get; set; }
       
        public string EndNode { get; set; }
       
        public float ValveDiameter { get; set; }
       
        public string ValveType { get; set; }
       
        public string ValveSetting { get; set; }
        
        public float ValveLossCoeff { get; set; }

        /// <summary>
        /// this value is set if the valve is GPV, a general
        /// purpose valve
        /// </summary>
        public string HeadLossCurveID { get; set; }

        /// <summary>
        /// c'tor
        /// </summary>
        public Valve()
        {
            Type = NodeTypes.valve;
        }

        /// <summary>
        /// put in string format for output file creation
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            double diam = this.ValveDiameter;
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("{0}\t{1}\t{2}\t{3}\t{4}\t", this.ID, this.StartNode,
                                                    this.EndNode,
                                                    diam.ToString("F5"),
                                                    this.ValveType);

            if (this.IsGeneralPurpose())
                builder.AppendFormat("{0}\t", this.HeadLossCurveID);
            else
            {
                string vset = this.ValveSetting;
                builder.AppendFormat("{0}\t", vset.Trim());
            }

            double coeff = this.ValveLossCoeff;
            builder.AppendFormat("{0}", coeff.ToString("F5"));
            
            return builder.ToString();           
        }

        /// <summary>
        /// is this a general purpose valve?
        /// </summary>
        /// <param name="valveType"></param>
        /// <returns></returns>
        public bool IsGeneralPurpose(string valveType)
        {
            if (valveType.ToUpper().Trim() == "GPV")
                return true;
            else
                return false;
        }

        /// <summary>
        /// is this a general purpose valve?
        /// </summary>
        /// <param name="valveType"></param>
        /// <returns></returns>
        public bool IsGeneralPurpose()
        {
            return IsGeneralPurpose(this.ValveType);
        }
    }

    public class Valves : LinkOrderedDictionary
    {
        /// <summary>
        /// Load up the valves collection
        /// </summary>
        /// <param name="?"></param>
        public void LoadValves(string elements)
        {
            try
            {
                // wipe out any existing data
                if (this.Count > 0)
                    this.Clear();

                string[] pieces = elements.Split(Environment.NewLine.ToCharArray());
                foreach (string piece in pieces)
                {
                    string tempPiece = piece.Trim();

                    if ((tempPiece != string.Empty) && (tempPiece[0] != Node.COMMENT[0]))
                    {
                        string[] vals = tempPiece.Split("\t".ToCharArray());
                        if (vals[0] != string.Empty)
                        {
                            Valve newValve = new Valve();
                            newValve.ID = vals[0];
                            newValve.StartNode = vals[1];
                            newValve.EndNode = vals[2];
                            newValve.ValveDiameter = Convert.ToSingle(vals[3]); // a single is a float
                            newValve.ValveType = vals[4];
                            if (newValve.IsGeneralPurpose())
                                newValve.HeadLossCurveID = vals[5];
                            else
                                newValve.ValveSetting = (vals[5]).Trim();
                            newValve.ValveLossCoeff = Convert.ToSingle(vals[6]);

                            this.Add(newValve.ID, newValve);
                        }
                    }
                }
            }
            catch (SystemException sysex)
            {
                Console.WriteLine(sysex.Message);
            }
        }

        /// <summary>
        /// put the entire collection into an output string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {            
            return Node.CollectionToString(Valve.NODE_ID, MyOrderedDictionary.Ordered(this));
        }
    }
}
