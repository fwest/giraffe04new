﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Giraffe
{
    /// <summary>
    /// represents a source instance
    /// </summary>
    [Serializable]
    public class Source : NodeBase<Source>
    {
        public const string NODE_ID = @"SOURCES";

        /// <summary>
        /// the sources type
        /// </summary>
        public SourceTypes SourceType { get; set; }

        /// <summary>
        /// the strenght of the source
        /// </summary>
        public float BaselineStrength { get; set; }

        /// <summary>
        /// optional, time pattern ID
        /// </summary>
        public string PatternID { get; set; }

        /// <summary>
        /// c'tor
        /// </summary>
        public Source()
        {
            Type = NodeTypes.source;
        }

        /// <summary>
        /// put in string format for output file creation
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Empty; // TBD
        }
    }

    /// <summary>
    /// this class does not seem to be used in Giraffe
    /// </summary>
    [Serializable]
    public class Sources : OrderedDictionary
    {
        /// <summary>
        /// Load up the sources collection
        /// </summary>
        /// <param name="?"></param>
        public void LoadSources(string elements)
        {
            try
            {
                // wipe out any existing data
                if (this.Count > 0)
                    this.Clear();

                string[] pieces = elements.Split(Environment.NewLine.ToCharArray());
                foreach (string piece in pieces)
                {
                    string tempPiece = piece.Trim();

                    if ((tempPiece != string.Empty) && (tempPiece[0] != Node.COMMENT[0]))
                    {
                        Source newSource = new Source();

                        string[] src = tempPiece.Split("\t".ToCharArray());
                        newSource.ID = src[0];
                        newSource.SourceType = (SourceTypes)Enum.Parse(typeof(SourceTypes), src[1]);
                        newSource.BaselineStrength = Convert.ToSingle(src[2]);
                        if (src.Length == 4)
                            newSource.PatternID = src[3];

                        this.Add(newSource.ID, newSource);
                    }
                }
            }
            catch (SystemException sysex)
            {
                Console.WriteLine(sysex.Message);
            }
        }
    }
}
