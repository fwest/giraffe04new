﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Giraffe
{
    /// <summary>
    /// represents a quality instance
    /// </summary>
    [Serializable]
    public class Quality : NodeBase<Quality>
    {
        public const string NODE_ID = @"QUALITY";
       
        public string QualityParameter { get; set; }
        
        public string QualityUnit { get; set; }
        
        public float QualityRelativeDiffusity { get; set; }
       
        public float QualityTolerance { get; set; }

        /// <summary>
        /// initial quality as read from input file
        /// </summary>
        public float InitialQuality { get; set; }

        /// <summary>
        /// c'tor
        /// </summary>
        public Quality()
        {
            // intialize these values as all values are not read from the input file
            Type = NodeTypes.quality;
            this.InitialQuality = -1;
            this.QualityParameter = string.Empty;
            this.QualityRelativeDiffusity = -1;
            this.QualityTolerance = -1;
            this.QualityUnit = string.Empty;
        }

        /// <summary>
        /// put in string format for output file creation
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Empty; // TBD
        }
    }

    /// <summary>
    /// Defines initial water quality at nodes
    /// </summary>
    public class Qualities : OrderedDictionary
    {
        /// <summary>
        /// Load up the qualities collection
        /// </summary>
        /// <param name="?"></param>
        public void LoadQualities(string elements)
        {
            try
            {
                // wipe out any existing data
                if (this.Count > 0)
                    this.Clear();

                string[] pieces = elements.Split(Environment.NewLine.ToCharArray());
                foreach (string piece in pieces)
                {
                    if ((piece != string.Empty) && (piece[0] != Node.COMMENT[0]))
                    {
                        Quality newQuality = new Quality();

                        string[] qual = piece.Split("\t".ToCharArray());
                        newQuality.ID = qual[0];
                        newQuality.InitialQuality = Convert.ToSingle(qual[1]);

                        // a node may only have one initial quality -- we hope ...
                        this.Add(newQuality.ID, newQuality);                        
                    }
                }
            }
            catch (SystemException sysex)
            {
                Console.WriteLine(sysex.Message);
            }
        }
    }
}
