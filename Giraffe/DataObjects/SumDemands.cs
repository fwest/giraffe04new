﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Giraffe
{
    /// <summary>
    /// represents a summation of demand values
    /// </summary>
    [Serializable]
    public class SumDemand : NodeBase<SumDemand>
    {       
        public string value { get; set; }

        /// <summary>
        /// c'tor
        /// </summary>
        public SumDemand()
        {
            Type = NodeTypes.sumdemand;
        }

        /// <summary>
        /// put in string format for output file creation
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Empty;
        }
    }

    public class SumDemands : MyOrderedDictionary
    {
    }
}
