﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Giraffe
{
    /// <summary>
    /// represents a demand instance
    /// </summary>
    [Serializable]
    public class Demand : NodeBase<Demand> // legacy DEMAND
    {
        public const string NODE_ID = @"DEMANDS";
        
        public float DemandBase { get; set; }

        public float DemandBaseTemp { get; set; }
        
        public string DemandPattern { get; set; }
        
        public string DemandCategory { get; set; }

        /// <summary>
        /// c'tor
        /// </summary>
        public Demand()
        {
            Type = NodeTypes.demand;
        }

        /// <summary>
        /// put in string format for output file creation
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            double based = this.DemandBase;
            builder.AppendFormat("{0}\t{1}", this.ID, based.ToString("F5"));

            if ((this.DemandPattern != string.Empty) && (this.DemandPattern != null))
            {
                builder.AppendFormat("\t{0}", this.DemandPattern);

                if (this.DemandCategory != string.Empty)
                    builder.AppendFormat("\t{0}", this.DemandCategory);
            }

            return builder.ToString();
        }
    }

    public class Demands : OrderedDictionary
    {
        /// <summary>
        /// Load up the demands collection
        /// </summary>
        /// <param name="?"></param>
        public void LoadDemands(string elements)
        {
            try
            {
                // wipe out any existing data
                if (this.Count > 0)
                    this.Clear();

                string[] pieces = elements.Split(Environment.NewLine.ToCharArray());
                foreach (string piece in pieces)
                {
                    string tempPiece = piece.Trim();
                    if ((tempPiece != string.Empty) && (tempPiece[0] != Node.COMMENT[0]))
                    {
                        string[] vals = tempPiece.Split("\t".ToCharArray());
                        if (vals[0] != string.Empty)
                        {
                            Demand newDemand = new Demand();
                            newDemand.ID = vals[0];
                            newDemand.DemandBase = Convert.ToSingle(vals[1]);
                            newDemand.DemandBaseTemp = newDemand.DemandBase;
                            if (vals.Length >= 3) // optional
                                newDemand.DemandPattern = vals[2];
                            if (vals.Length == 4) // optional
                                newDemand.DemandCategory = vals[3];

                            this.Add(newDemand.ID, newDemand);
                        }
                    }
                }
            }
            catch (SystemException sysex)
            {
                Console.WriteLine(sysex.Message);
            }
        }

        /// <summary>
        /// put the entire collection into an output string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {            
            return Node.CollectionToString(Demand.NODE_ID, MyOrderedDictionary.Ordered(this));
        }

        /// <summary>
        /// make a true copy of the Demands instance to the specified instance
        /// </summary>
        /// <param name="destination"></param>
        public void Copy(Demands destination)
        {
            destination = new Demands();
            foreach (DictionaryEntry de in this)
            {
                Demand copied = (Demand)de.Value;
                destination.Add(copied.ID, copied);
            }
        }
    }
}
