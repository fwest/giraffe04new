﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Giraffe
{
    /// <summary>
    /// represents a pattern instance
    /// </summary>
    [Serializable]
    public class Pattern : NodeBase<Pattern>
    {
        /// <summary>
        /// use this for file output
        /// </summary>
        class myFloat
        {
            public float FloatValue { get; set; }

            public myFloat()
            {
                FloatValue = (float)-1.0;
            }

            public override string ToString()
            {                
                if (FloatValue == -1.0)
                    return string.Empty;
                else
 	                return FloatValue.ToString();
            }
        }

        public const string NODE_ID = @"PATTERNS";
        
        public OrderedDictionary Information { get; set; }

        /// <summary>
        /// c'tor
        /// </summary>
        public Pattern()
        {
            Type = NodeTypes.pattern;

            Information = new OrderedDictionary();
        }

        /// <summary>
        /// put in string format for output file creation
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            // complete re-write from when we were storing floats
            StringBuilder builder = new StringBuilder();

            int count = 0;
            foreach(DictionaryEntry de in this.Information)
            {
                ++count;
            
                if (count < this.Information.Count)
                    builder.AppendFormat("{0}\t{1}{2}", this.ID, (string)de.Value, Environment.NewLine);
                else
                    builder.AppendFormat("{0}\t{1}", this.ID, (string)de.Value);
            }      
      
            return builder.ToString();
        }

        /// <summary>
        /// return the sum of all values assined to this pattern
        /// </summary>
        /// <returns></returns>
        public float Sum()
        {
            float returnValue = 0;

            foreach (DictionaryEntry deval in Information)
            {
                string[] patternValues = ((string)deval.Value).Split('\t');
                
                foreach(string strval in patternValues)
                    returnValue += Convert.ToSingle(strval.Trim());
            }

            return returnValue;
        }
    }

    public class Patterns : OrderedDictionary
    {
        /// <summary>
        /// Load up the patterns collection
        /// </summary>
        /// <param name="?"></param>
        public void LoadPatterns(string elements)
        {
            try
            {
                // wipe out any existing data
                if (this.Count > 0)
                    this.Clear();

                string[] pieces = elements.Split(Environment.NewLine.ToCharArray());
                foreach (string piece in pieces)
                {
                    string tempPiece = piece.Trim();

                    if ((tempPiece != string.Empty) && (tempPiece[0] != Node.COMMENT[0]))
                    {
                        string[] vals = tempPiece.Split("\t".ToCharArray());
                        if (vals[0] != string.Empty)
                        {
                            Pattern newPattern = new Pattern();
                            newPattern.ID = vals[0];

                            // if this pattern already exists in the collection, just addd
                            // the values to it
                            if (!(this.Contains(newPattern.ID)))
                                // new entry
                                this.Add(newPattern.ID, newPattern);

                            Pattern existingPattern = (Pattern)this[newPattern.ID];

                            var tempValue = string.Empty;
                            // keep the patterns as originally delivered -- to support wsorting
                            // simply add the "new" parameters to the new or existing collection
                            for (int counter = 1; counter < vals.Length; counter++)
                            {                                
                                if (vals[counter] != string.Empty)
                                {
                                    tempValue += vals[counter];
                                    if (counter + 1 < vals.Length)
                                        tempValue += '\t';
                                }                                
                            }

                            // simply satisfy the unique key - no real functional value as implemented
                            // order is what counts
                            existingPattern.Information.Add(Guid.NewGuid(), tempValue);
                        }
                    }
                }
            }
            catch (SystemException sysex)
            {
                Console.WriteLine(sysex.Message);
            }
        }

        private string cache = string.Empty;
        private bool dirty = false;

        /// <summary>
        /// put the entire collection into an output string
        /// </summary>
        /// <returns></returns>
        public  string ToString(bool wsort)
        {
            if (dirty || (cache.Length == 0))
            {
                if (wsort)
                    ApplyWeirdSort();
                cache = Node.CollectionToString(Pattern.NODE_ID, MyOrderedDictionary.Ordered(this));
                dirty = false;
            }

            return cache;
        }        

        /// <summary>
        /// cache for performance
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public new void Add(object key, object value)
        {
            dirty = true;
            base.Add(key, value);
        }

        /// <summary>
        /// cache for performance
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public new void Remove(object key)
        {
            dirty = true;
            base.Remove(key);
        }
        
        /// <summary>
        /// order seems to matter with the tanks as the controls 
        /// </summary>
        public void ApplyWeirdSort()
        {         
            foreach (DictionaryEntry de in this)
            {
                Pattern pe = (Pattern)de.Value;
                if (pe.Information.Count > 2)
                {                   
                    string[] info = new string[pe.Information.Count];
                    info[0] = (string)pe.Information[0];                    

                    for (int i = 1; i < pe.Information.Count; i++)
                    {
                        info[i] = (string)pe.Information[pe.Information.Count - i];
                    }
                    // out with the old order
                    ((Pattern)this[de.Key]).Information.Clear();
                    for (int j = 0; j < info.Length; j++)
                    {
                        // in with the new order
                        ((Pattern)this[de.Key]).Information.Add(System.Guid.NewGuid(), info[j]);
                    }
                }
            }
        }
    }
}
