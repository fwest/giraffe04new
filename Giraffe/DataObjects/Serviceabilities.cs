﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Giraffe
{
    /// <summary>
    /// holds information that represents a system's serviceability
    /// </summary>
    [Serializable]
    public class Serviceability : NodeBase<Serviceability>
    {
        public const string NODE_ID = @"SEVICEABILITY";

        public int TimeStep { get; set; }
      
        public string Value {get; set;}

        public float Mean { get; set; }
        
        public float DemandBase { get; set; }

        /// <summary>
        /// c'tor
        /// </summary>
        public Serviceability()
        {
            Type = NodeTypes.serviceability;
        }

        /// <summary>
        /// put in string format for output file creation
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Empty; // TBD
        }
    }

    public class Serviceabilities : MyOrderedDictionary
    {
        /// <summary>
        /// put the entire collection into an output string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Node.CollectionToString(Serviceability.NODE_ID, this);
        }
    }
}
