﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Giraffe
{
     /// <summary>
    /// represents a report option instance
    /// </summary>
    [Serializable]
    public class ReportOption : NodeBase<ReportOption>
    {        
        public const string NODE_ID = @"REPORT";
        
        public int PageSizeValue { get; set; }
       
        public string FileName { get; set; }
     
        public ReportOptionValues GenerateHydraulicStatus { get; set; }
      
        public ReportOptionValues GenerateSummaryTable { get; set; }
      
        public ReportOptionValues EnergyInfoProvided { get; set; }
     
        public OrderedDictionary Nodes { get; set; }
     
        public OrderedDictionary Links { get; set; }
     
        /// <summary>
        /// c'tor
        /// </summary>
        public ReportOption()
        {
            PageSizeValue = -1;
            FileName = string.Empty;
            GenerateHydraulicStatus = ReportOptionValues.NOTDEFINED;
            GenerateSummaryTable = ReportOptionValues.NOTDEFINED;
            EnergyInfoProvided = ReportOptionValues.NOTDEFINED;
            Type = NodeTypes.report;
            Nodes = new OrderedDictionary();
            Links = new OrderedDictionary();
        }

        /// <summary>
        /// put in string format for output file creation
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            if (PageSizeValue != -1)
                builder.AppendFormat("PAGESIZE {0}", PageSizeValue);
            else if (FileName != string.Empty)
                builder.AppendFormat("FILE {0}", FileName);
            else if (GenerateHydraulicStatus != ReportOptionValues.NOTDEFINED)
                builder.AppendFormat("STATUS {0}", GenerateHydraulicStatus);
            else if (GenerateSummaryTable != ReportOptionValues.NOTDEFINED)
                builder.AppendFormat("SUMMARY {0}", GenerateSummaryTable);
            else if (EnergyInfoProvided != ReportOptionValues.NOTDEFINED)
                builder.AppendFormat("ENERGY {0}", EnergyInfoProvided);

            else if (Nodes.Count > 0)
            {
                StringBuilder nbldr = new StringBuilder();
                nbldr.AppendFormat("NODE ");
                foreach (DictionaryEntry nodede in Nodes)
                {
                    string snode = (string)nodede.Value;
                    nbldr.AppendFormat("{0} ", snode);
                }

                builder.AppendFormat("{0}", nbldr.ToString());
            }
            else if (Links.Count > 0)
            {
                StringBuilder nbldr = new StringBuilder();
                nbldr.AppendFormat("LINK ");
                foreach (DictionaryEntry nodede in Links)
                {
                    string lnode = (string)nodede.Value;
                    nbldr.AppendFormat("{0} ", lnode);
                }

                builder.AppendFormat("{0}", nbldr.ToString());
            }

            return builder.ToString();
        }
    }

   
    [Serializable]
    public class ReportOptions : OrderedDictionary
    {
        /// <summary>
        /// Load up the reporting options collection
        /// </summary>
        /// <param name="?"></param>
        public void LoadReportOptions(string elements)
        {
            try
            {
                // wipe out any existing data
                if (this.Count > 0)
                    this.Clear();

                string[] pieces = elements.Split(Environment.NewLine.ToCharArray());
                foreach (string piece in pieces)
                {
                    string tempPiece = piece.Trim();
                    if ((tempPiece != string.Empty) && (tempPiece[0] != Node.COMMENT[0]))
                    {
                        string[] options = tempPiece.Split(' ');

                        ReportOption newReportOption = new ReportOption();

                        // check for multi-string identifiers
                        newReportOption.ID = options[0];

                        switch (newReportOption.ID.ToUpper())
                        {
                            case "FILE":
                                newReportOption.FileName = NodeBase<ReportOption>.GetNextString(options);
                                break;

                            case "PAGESIZE":
                                newReportOption.PageSizeValue = Convert.ToInt32(NodeBase<ReportOption>.GetNextDouble(options));
                                break;

                            case "STATUS":
                                newReportOption.GenerateHydraulicStatus = GetOptionValue(options);
                                break;

                            case "SUMMARY":
                                newReportOption.GenerateSummaryTable = GetOptionValue(options);
                                break;

                            case "ENERGY":
                                newReportOption.EnergyInfoProvided = GetOptionValue(options);
                                break;

                            case "NODE":
                                newReportOption.Nodes = GetNodes(options);
                                break;

                            case "LINK":
                                newReportOption.Links = GetLinks(options);
                                break;

                            default:                                
                                break;
                        }

                        // use the input line to satisfy the need for a unique key
                        this.Add(System.Guid.NewGuid(), newReportOption);
                    }
                }
            }
            catch (SystemException sysex)
            {
                Console.WriteLine(sysex.Message);
            }
        }

        /// <summary>
        /// get the correct value from the enum
        /// </summary>
        /// <param name="substrings"></param>
        /// <returns></returns>
        private ReportOptionValues GetOptionValue(string[] substrings)
        {
            ReportOptionValues result = ReportOptionValues.NONE;

            try
            {
                if (substrings.Length >= 2)
                {
                    result = (ReportOptionValues)Enum.Parse(typeof(ReportOptionValues), substrings[1]);
                }
            }
            catch (SystemException sysex)
            {
                Console.WriteLine(sysex.Message);
            }

            return result;
        }

        private OrderedDictionary GetNodes(string[] nodes)
        {
            return GetTrailingSubstrings(nodes);
        }

        private OrderedDictionary GetLinks(string[] links)
        {
            return GetTrailingSubstrings(links);
        }

        private OrderedDictionary GetTrailingSubstrings(string[] subs)
        {
            OrderedDictionary results = new OrderedDictionary();

            try
            {
                for (int counter = 1; counter < subs.Length; counter++)
                {
                    results.Add(System.Guid.NewGuid(), subs[counter]);
                }
            }
            catch (SystemException sysex)
            {
                Console.WriteLine(sysex.Message);
            }
            return results;
        }

        /// <summary>
        /// put the entire collection into an output string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Node.CollectionToString(ReportOption.NODE_ID, this);
            
        }
    }
}
