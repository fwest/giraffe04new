﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Giraffe
{
    /// <summary>
    /// represents a tank instance
    /// </summary>
    [Serializable]
    public class Tank : NodeBase<Tank> // legacy TANK
    {
        public const float TANKUPDATE = 7.4805f;
        public const float UPDATETIME = 60.0f;
        public const string NODE_ID = @"TANKS";
             
        public float TankElevation { get; set; }
       
        public float TankInitLevel { get; set; }
     
        public float TankMinLevel { get; set; }
      
        public float TankMaxLevel { get; set; }
      
        public float TankDiameter { get; set; }
      
        public float TankMinVolume { get; set; }
        
        public string CurveID { get; set; }
      
        /// <summary>
        /// c'tor
        /// </summary>
        public Tank()
        {
            Type = NodeTypes.tank;
        }

        /// <summary>
        /// put in string format for output file creation
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            double[] temp = new double[6];
            temp[0] = this.TankElevation;
            temp[1] = this.TankInitLevel;
            temp[2] = this.TankMinLevel;
            temp[3] = this.TankMaxLevel;
            temp[4] = this.TankDiameter;
            temp[5] = this.TankMinVolume;

            builder.AppendFormat("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t", this.ID, temp[0].ToString("F5"), 
                                                    temp[1].ToString("F5"),
                                                    temp[2].ToString("F5"),
                                                    temp[3].ToString("F5"),
                                                    temp[4].ToString("F5"),
                                                    temp[5].ToString("F5"));
            if ((this.CurveID != string.Empty) && (this.CurveID != null))
                builder.AppendFormat("{0}", this.CurveID);

            return builder.ToString();
           
        }    
    }

    public class Tanks : NodeOrderedDictionary
    {
        /// <summary>
        /// Load up the tanks collection
        /// </summary>
        /// <param name="?"></param>
        public void LoadTanks(string elements)
        {
            try
            {
                // wipe out any existing data
                if (this.Count > 0)
                    this.Clear();

                string[] pieces = elements.Split(Environment.NewLine.ToCharArray());
                foreach (string piece in pieces)
                {
                    string tempPiece = piece.Trim();
                    if ((tempPiece != string.Empty) && (tempPiece[0] != Node.COMMENT[0]))
                    {
                        string[] vals = tempPiece.Split("\t".ToCharArray());
                        if ((vals[0] != string.Empty) && ((vals.Length == 7) || (vals.Length == 8)))
                        {
                            Tank newTank = new Tank();
                            newTank.ID = vals[0];
                            newTank.TankElevation = Convert.ToSingle(vals[1]); // a single is a float
                            newTank.TankInitLevel = Convert.ToSingle(vals[2]);
                            newTank.TankMinLevel = Convert.ToSingle(vals[3]);
                            newTank.TankMaxLevel = Convert.ToSingle(vals[4]);
                            newTank.TankDiameter = Convert.ToSingle(vals[5]);
                            newTank.TankMinVolume = Convert.ToSingle(vals[6]);
                            // if we have a curve id --
                            if (vals.Length == 8)
                                newTank.CurveID = vals[7];
                            this.Add(newTank.ID, newTank);
                        }
                    }
                }
            }
            catch (SystemException sysex)
            {
                Console.WriteLine(sysex.Message);
            }
        }

        /// <summary>
        /// put the entire collection into an output string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {            
            return Node.CollectionToString(Tank.NODE_ID, MyOrderedDictionary.Ordered(this));
        }
    }
}