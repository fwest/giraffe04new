﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Giraffe
{
    /// <summary>
    /// represents a status instance
    /// </summary>
    [Serializable]
    public class Status : NodeBase<Status>
    {
        public const string NODE_ID = @"STATUS";
        
        public string StatusOption { get; set; }

        /// <summary>
        /// c'tor
        /// </summary>
        public Status()
        {
            Type = NodeTypes.status;
        }

        /// <summary>
        /// put in string format for output file creation
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("{0}\t{1}", this.ID, this.StatusOption.ToString());
            return builder.ToString();
        }
    }

    public class Statuses : OrderedDictionary
    {
        /// <summary>
        /// Load up the link (e.g. valve) status collection
        /// </summary>
        /// <param name="?"></param>
        public void LoadStatuses(string elements)
        {
            try
            {
                // wipe out any existing data
                if (this.Count > 0)
                    this.Clear();

                string[] pieces = elements.Split(Environment.NewLine.ToCharArray());
                foreach (string piece in pieces)
                {
                    string tempPiece = piece.Trim();

                    if ((tempPiece != string.Empty) && (tempPiece[0] != Node.COMMENT[0]))
                    {
                        string[] vals = tempPiece.Split("\t".ToCharArray());
                        // if this string has the expected format size wise
                        if (vals.Length == 2)
                        {
                            if (vals[0] != string.Empty)
                            {
                                Status newStatus = new Status();
                                newStatus.ID = vals[0];

                                if (vals[1] != string.Empty)
                                {                                                                        
                                    newStatus.StatusOption = vals[1].Trim();
                                    this.Add(newStatus.ID, newStatus);
                                }
                            }
                        }
                    }
                }
            }
            catch (SystemException sysex)
            {
                Console.WriteLine(sysex.Message);
            }
        }

        /// <summary>
        /// put the entire collection into an output string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {           
            return Node.CollectionToString(Status.NODE_ID, MyOrderedDictionary.Ordered(this));
        }
    }
}
