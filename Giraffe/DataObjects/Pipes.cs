﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Giraffe
{
    /// <summary>
    /// represents a pipe instance
    /// </summary>
    [Serializable]
    public class Pipe : NodeBase<Pipe> // legacy PIPE
    {
        public const string NODE_ID = @"PIPES";

        public string StartNode { get; set; }

        public string EndNode { get; set; }
        
        public float PipeLength { get; set; }
        
        public float PipeDiameter { get; set; }

        public float PipeRoughness { get; set; }
        
        public float PipeLossCoeff { get; set; }
        
        public string PipeExtra { get; set; }
       
        /// <summary>
        /// c'tor
        /// </summary>
        public Pipe()
        {
            Type = NodeTypes.pipe;
        }

        /// <summary>
        /// put in string format for output file creation
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            double[] temp = new double[4];

            temp[0] = this.PipeLength;
            temp[1] = this.PipeDiameter;
            temp[2] = this.PipeRoughness;
            temp[3] = this.PipeLossCoeff;

            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t", this.ID, this.StartNode,
                                                    this.EndNode,
                                                    temp[0].ToString("F5"),
                                                    temp[1].ToString("F5"),
                                                    temp[2].ToString("F5"),
                                                    temp[3].ToString("F5"));
            if (this.PipeExtra != string.Empty)
                builder.AppendFormat("{0}", this.PipeExtra);

            return builder.ToString();
        }
    }

    public class Pipes : LinkOrderedDictionary
    {
        private const string damageFileTrunk = @"Damage_Info_Dert";
        private const string damgeFileExtension = @".txt";

        /// <summary>
        /// c'tor
        /// </summary>
        public Pipes()
        {
            PipeFilesPrinted = 0;
        }

        /// <summary>
        /// use this counter to create a unique file name in a loop
        /// </summary>
        public int PipeFilesPrinted { get; set; }

        /// <summary>
        /// Load up the pipes collection
        /// </summary>
        /// <param name="?"></param>
        public void LoadPipes(string elements)
        {
            try
            {            
                // wipe out any existing data
                if (this.Count > 0)
                    this.Clear();

                string[] pieces = elements.Split(Environment.NewLine.ToCharArray());
                foreach (string piece in pieces)
                {
                    string tempPiece = piece.Trim();

                    if ((tempPiece != string.Empty) && (tempPiece[0] != Node.COMMENT[0]))
                    {
                        string[] vals = tempPiece.Split("\t".ToCharArray());
                        if ((vals[0] != string.Empty) && ((vals.Length == 7) || (vals.Length == 8)))
                        {
                            Pipe newPipe = new Pipe();
                            newPipe.ID = vals[0];
                            newPipe.StartNode = vals[1]; // a single is a float
                            newPipe.EndNode = vals[2];
                            newPipe.PipeLength = Convert.ToSingle(vals[3]);
                            newPipe.PipeDiameter = Convert.ToSingle(vals[4]);
                            newPipe.PipeRoughness = Convert.ToSingle(vals[5]);
                            newPipe.PipeLossCoeff = Convert.ToSingle(vals[6]);
                            // if we have a curve id --
                            if (vals.Length == 8)
                                newPipe.PipeExtra = vals[7];
                            this.Add(newPipe.ID, newPipe);
                        }
                    }
                }
            }
            catch (SystemException sysex)
            {
                Console.WriteLine(sysex.Message);
            }
        }

        /// <summary>
        /// put the entire collection into an output string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {            
            return Node.CollectionToString(Pipe.NODE_ID, MyOrderedDictionary.Ordered(this));
        }

        /// <summary>
        /// Print out the damage that is about to be added to the system
        /// </summary>
        /// <param name="outputFile"></param>
        public void PrintPipeFile(string outputFilePath, PipeBreaks breaks, PipeLeaks leaks)
        {
            StringBuilder toWrite = new StringBuilder();

            PipeFilesPrinted++;
            toWrite.AppendFormat("{0}{1}{2}{3}", outputFilePath.Trim(), damageFileTrunk, PipeFilesPrinted, damgeFileExtension);

            // create the body of the file
            StringBuilder fileOutput = new StringBuilder();

            // create the pipe break header
            fileOutput.AppendFormat("{0}{1}", breaks.PrintPipeBreakInfo(), Environment.NewLine);            

            // output the pipe leak info
            fileOutput.AppendFormat("{1}{1}{0}{1}", leaks.PrintPipeLeakInfo(), Environment.NewLine);

            File.WriteAllText(toWrite.ToString(), fileOutput.ToString(), Encoding.ASCII);
        }        
    }
}
