<?php
/*
# $Id: viewprofile.php 123 2007-07-27 21:02:15Z klin $

# The PHP interface was originally inspired by PHPBB,
# file/database formats and directory structure are quite similar.

##############################################################################
# 
# Terms and Conditions of Software Use
# ====================================
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Disclaimer of Earthquake Information
# ====================================
# 
# The data and maps provided through this system are preliminary data
# and are subject to revision. They are computer generated and may not
# have received human review or official approval. Inaccuracies in the
# data may be present because of instrument or computer
# malfunctions. Subsequent review may result in significant revisions to
# the data. All efforts have been made to provide accurate information,
# but reliance on, or interpretation of earthquake data from a single
# source is not advised. Data users are cautioned to consider carefully
# the provisional nature of the information before using it for
# decisions that concern personal or public safety or the conduct of
# business that involves substantial monetary or operational
# consequences.
# 
# Disclaimer of Software and its Capabilities
# ===========================================
# 
# This software is provided as an "as is" basis.  Attempts have been
# made to rid the program of software defects and bugs, however the
# U.S. Geological Survey (USGS) have no obligations to provide maintenance, 
# support, updates, enhancements or modifications. In no event shall USGS 
# be liable to any party for direct, indirect, special, incidental or 
# consequential damages, including lost profits, arising out of the use 
# of this software, its documentation, or data obtained though the use 
# of this software, even if USGS or have been advised of the
# possibility of such damage. By downloading, installing or using this
# program, the user acknowledges and understands the purpose and
# limitations of this software.
# 
# Contact Information
# ===================
# 
# Coordination of this effort is under the auspices of the USGS Advanced
# National Seismic System (ANSS) coordinated in Golden, Colorado, which
# functions as the clearing house for development, distribution,
# documentation, and support. For questions, comments, or reports of
# potential bugs regarding this software please contact wald@usgs.gov or
# klin@usgs.gov.  
#
#############################################################################
*/

define('IN_SC', true);
$phpbb_root_path = './';
include($phpbb_root_path . 'extension.inc');
include($phpbb_root_path . 'common.'.$phpEx);

//
// Start initial var setup
//
//
// Start session management
//
$userdata = session_pagestart($user_ip, PAGE_PROFILE_NOTIFY);
init_userprefs($userdata);
//
// End session management
//

//
// Start auth check
//
if ( !$userdata['session_logged_in'] )
{
	redirect(append_sid("login.$phpEx?redirect=viewprofile.$phpEx", true));
}
//
// End auth check
//

if( isset($HTTP_GET_VARS['mode']) || isset($HTTP_POST_VARS['mode']) )
{
	$mode = (isset($HTTP_GET_VARS['mode'])) ? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode'];
	$mode = htmlspecialchars($mode);
}

// Restrict mode input to valid options
$mode = ( in_array($mode, array('update')) ) ? $mode : '';

if( $mode == "update" )
{
	$sql_remove = "DELETE fnr FROM 
		(notification_request as nr LEFT JOIN facility_notification_request as fnr 
			on fnr.notification_request_id = nr.notification_request_id)
		WHERE nr.shakecast_user=". $userdata['user_id']; 
	if  (!($result  = $db->sql_query($sql_remove)) ) {
		message_die(GENERAL_ERROR, "Could not delete user profile information", '', __LINE__, __FILE__, $sql_remove);
	}

	$sql_remove = "DELETE nr FROM 
		(geometry_user_profile as gup LEFT JOIN notification_request as nr 
			on gup.shakecast_user=nr.shakecast_user)
		WHERE nr.shakecast_user=". $userdata['user_id']; 
	if  (!($result  = $db->sql_query($sql_remove)) ) {
		message_die(GENERAL_ERROR, "Could not delete user profile information", '', __LINE__, __FILE__, $sql_remove);
	}

	$sql_remove = "DELETE FROM geometry_user_profile 
		WHERE shakecast_user=". $userdata['user_id']; 
	if  (!($result  = $db->sql_query($sql_remove)) ) {
		message_die(GENERAL_ERROR, "Could not delete user profile information", '', __LINE__, __FILE__, $sql_remove);
	}

	if (!empty($HTTP_POST_VARS['user_profile'])) {
		$profiles = $HTTP_POST_VARS['user_profile'];
		foreach ($profiles as $profile) {
			$sql = " select notification_request_id, notification_type, delivery_method
					  from profile_notification_request
					 where profile_id = $profile";
			if ( !($result = $db->sql_query($sql)) )
			{
				message_die(GENERAL_ERROR, "Could not obtain profile information", '', __LINE__, __FILE__, $sql);
			}
	
			$rowset = $db->sql_fetchrowset($result);
			for ($ind = 0; $ind < count($rowset); $ind++)
			{
				$sql_insert = "INSERT INTO notification_request (
					damage_level, notification_type, event_type, delivery_method, message_format,
					limit_value, user_message, notification_priority, auxiliary_script, disabled,
					product_type, metric, aggregate, aggregation_group, shakecast_user,
					update_username, update_timestamp) SELECT 
					damage_level, notification_type, event_type, delivery_method, message_format,
					limit_value, user_message, notification_priority, auxiliary_script, disabled,
					product_type, metric, aggregate, aggregation_group, ". $userdata['user_id'].
					",'" . $userdata['user_id'] . "',now()
					FROM profile_notification_request
					WHERE profile_id = $profile AND notification_request_id = ".$rowset[$ind]['notification_request_id'];
					
				if  (!($result_insert = $db->sql_query($sql_insert, BEGIN_TRANSACTION)) ) {
					message_die(GENERAL_ERROR, "Could not insert notification request information", '', __LINE__, __FILE__, $sql_insert);
				} 
				
				if ($rowset[$ind]['notification_type'] == 'DAMAGE' || $rowset[$ind]['notification_type'] == 'SHAKING') {
					$new_notfication_id = mysql_insert_id();
					$sql_insert = "INSERT INTO facility_notification_request (
						notification_request_id, facility_id)
						SELECT $new_notfication_id, fnr.facility_id
						FROM geometry_facility_profile fnr
						WHERE fnr.profile_id = $profile";
					if  (!($result_insert = $db->sql_query($sql_insert)) ) {
						message_die(GENERAL_ERROR, "Could not insert facility notification request information", '', __LINE__, __FILE__, $sql_insert);
					} 
				}
			}
			
			$sql_insert = "INSERT INTO geometry_user_profile (shakecast_user, profile_id)
				vALUES (". $userdata['user_id'] . ", $profile)"; 
			if  (!($result_insert = $db->sql_query($sql_insert, END_TRANSACTION)) ) {
				message_die(GENERAL_ERROR, "Could not update user profile information", '', __LINE__, __FILE__, $sql);
			}
		}
		$update_message = "Notification Profile Updated";
	}
}

//
// Query ShakeCast profile inventory
//
$sql = 
	"SELECT profile_id, profile_name, description, geom
	FROM 
		geometry_profile";
if ( !($result = $db->sql_query($sql)) )
{
   message_die(GENERAL_ERROR, 'Could not obtain topic information', '', __LINE__, __FILE__, $sql);
}

$postrow = array();
if ($row = $db->sql_fetchrow($result))
{
	do
	{
		$postrow[] = $row;
	}
	while ($row = $db->sql_fetchrow($result));
	$db->sql_freeresult($result);

	$total_posts = count($postrow);
}

//
// Load templates
//
$template->set_filenames(array(
	'body' => 'viewprofile_body.tpl')
);

//
// Output page header
//
$page_title = 'Notification Profile';
	
$header_tpl = 'viewprofile_header.tpl';
include($phpbb_root_path . 'includes/page_header.'.$phpEx);


//
// Is user watching this thread?
//
if( $userdata['session_logged_in'] )
{
	//$show_submit = '<button id="userSelect">Update notification profiles</button>';
	$show_submit = '<input type="button" id="userSelect" value="Update notification profiles">';
	$user_profile = array();
	$sql = 
		"SELECT gp.profile_id
		FROM 
			geometry_profile gp INNER JOIN geometry_user_profile gup on
			gp.profile_id = gup.profile_id
		WHERE
			gup.shakecast_user = " . $userdata['user_id'];

	if (($result = $db->sql_query($sql)))
	{
		while ($row = $db->sql_fetchrow($result))
		{
			$user_profile[$row['profile_id']] = 1;
		}
	}
	$hidden_fields = '<input type="hidden" name="mode" value="update" />';
}	


//
// Send vars to template
//
$template->assign_vars(array(
	'FORUM_ID' => $forum_id,
    'FORUM_NAME' => "Profile",
    'TOPIC_ID' => $topic_id,
    //'TOPIC_TITLE' => $topic_title,
    'L_PROFILE_MESSAGE' => $update_message,


	"S_HIDDEN_FIELDS" => $hidden_fields,
	"S_NOTIFY_ACTION" => append_sid("viewprofile.$phpEx"),
	//'L_AUTHOR' => $lang['Author'],
	'L_METRIC' =>	$metric_tpl,
	'L_AUTHOR' => $event_id,
	'L_NAME' => "Profile",
	'L_SUBMIT' => "Update notification profiles",
	'L_DESCRIPTION' => "Description")

);

//
// Okay, let's do the loop, yeah come on baby let's do the loop
// and it goes like this ...
//
for($i = 0; $i < $total_posts; $i++)
{
	//
	// Again this will be handled by the templating
	// code at some point
	//
	$row_color = ( !($i % 2) ) ? $theme['td_color1'] : $theme['td_color2'];
	$row_class = ( !($i % 2) ) ? $theme['td_class1'] : $theme['td_class2'];
	$metric_value = 

	$template->assign_block_vars('postrow', array(
		'ROW_COLOR' => '#' . $row_color,
		'ROW_CLASS' => $row_class,
		'CLASS_ID' => (($user_profile[$postrow[$i]['profile_id']]) ? 'row0' : 'row1'),
		//'MESSAGE' => $message,
		'ID' => $postrow[$i]['profile_id'],
		//'ID' => $postrow[$i]['profile_id'],
		'NAME' => $postrow[$i]['profile_name'],
		'METRIC' => "<script type=\"text/javascript\">geom['".$postrow[$i]['profile_id'].
			"']='".$postrow[$i]['geom']."';</script>",
		'DESCRIPTION' => $postrow[$i]['description'],
		'SUBSCRIBE' => (($user_profile[$postrow[$i]['profile_id']]) ? 'checked' : ''),
		//'VALUE' => $postrow[$i]['geom'],
		'SIGNATURE' => $user_sig,
		'EDITED_MESSAGE' => $l_edited_by,


		'U_MINI_POST' => $mini_post_url,
		'U_POST_ID' => $postrow[$i]['post_id'])
	);
}

$template->pparse('body');

include($phpbb_root_path . 'includes/page_tail.'.$phpEx);

?>