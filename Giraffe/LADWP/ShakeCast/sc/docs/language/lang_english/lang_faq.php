<?php
/*
#
### notify: Scan the Notification Queue and Deliver Messages
#
# $Id: notify.pl 184 2007-11-02 16:05:30Z klin $
#
##############################################################################
# 
# Terms and Conditions of Software Use
# ====================================
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Disclaimer of Earthquake Information
# ====================================
# 
# The data and maps provided through this system are preliminary data
# and are subject to revision. They are computer generated and may not
# have received human review or official approval. Inaccuracies in the
# data may be present because of instrument or computer
# malfunctions. Subsequent review may result in significant revisions to
# the data. All efforts have been made to provide accurate information,
# but reliance on, or interpretation of earthquake data from a single
# source is not advised. Data users are cautioned to consider carefully
# the provisional nature of the information before using it for
# decisions that concern personal or public safety or the conduct of
# business that involves substantial monetary or operational
# consequences.
# 
# Disclaimer of Software and its Capabilities
# ===========================================
# 
# This software is provided as an "as is" basis.  Attempts have been
# made to rid the program of software defects and bugs, however the
# U.S. Geological Survey (USGS) and Gatekeeper Systems have no
# obligations to provide maintenance, support, updates, enhancements or
# modifications. In no event shall USGS or Gatekeeper Systems be liable
# to any party for direct, indirect, special, incidental or consequential
# damages, including lost profits, arising out of the use of this
# software, its documentation, or data obtained though the use of this
# software, even if USGS or Gatekeeper Systems have been advised of the
# possibility of such damage. By downloading, installing or using this
# program, the user acknowledges and understands the purpose and
# limitations of this software.
# 
# Contact Information
# ===================
# 
# Coordination of this effort is under the auspices of the USGS Advanced
# National Seismic System (ANSS) coordinated in Golden, Colorado, which
# functions as the clearing house for development, distribution,
# documentation, and support. For questions, comments, or reports of
# potential bugs regarding this software please contact wald@usgs.gov or
# klin@usgs.gov.  
#
#############################################################################
*/
  
$faq[] = array("--","Login and Registration Issues");
$faq[] = array("Why can't I log in?", "Have you registered? Seriously, you must register in order to log in. Have you been banned from the system? (A message will be displayed if you have.) If so, you should contact the administrator to find out why. If you have registered and are not banned and you still cannot log in then check and double-check your username and password. Usually this is the problem; if not, contact the board administrator -- they may have incorrect configuration settings for the system.");
$faq[] = array("Why do I need to register at all?", "You may not have to -- it is up to the administrator as to whether you need to register in order to access the system. However, registration will give you access to additional features not available to guest users such as receiving shaking notifications and access to archived events, etc. It only takes a few minutes to register so it is recommended you do so.");
$faq[] = array("Why do I get logged off automatically?", "If you do not check the <i>Log me in automatically</i> box when you log in, the system will only keep you logged in for a preset time. This prevents misuse of your account by anyone else. To stay logged in, check the box during login. This is not recommended if you access the system from a shared computer, e.g. library, internet cafe, university cluster, etc.");
$faq[] = array("I've lost my password!", "Don't panic! While your password cannot be retrieved it can be reset. To do this go to the login page and click <u>I've forgotten my password</u>. Follow the instructions and you should be back online in no time.");
$faq[] = array("I registered but cannot log in!", "First check that you are entering the correct username and password. If they are okay then maybe your account need activating. Some systems will require all new registrations be activated, either by yourself or by the administrator before you can log on. When you registered it would have told you whether activation was required. If you were sent an email then follow the instructions; if you did not receive the email then check that your email address is valid. One reason activation is used is to reduce the possibility of <i>rogue</i> users abusing the system anonymously. If you are sure the email address you used is valid then try contacting the administrator.");
$faq[] = array("I registered in the past but cannot log in anymore!", "The most likely reasons for this are: you entered an incorrect username or password (check the email you were sent when you first registered) or the administrator has deleted your account for some reason. If it is the latter case then try registering again or contacting the administrator.");

$faq[] = array("--","ShakeCast Damage Assessment and Issues");
$faq[] = array("Why do I see a 'Scenario Earthquake' banner for some earthquakes?", "The 'Scenario Earthquake' banner indicates that the ShakeCast damage summary for the earthquake is not resulted from any recent earthquake but a scenario or test ShakeMap.  Scenario/Test ShakeMaps are designed to perform function tests of the ShakeCast system.  It can be one of the following two cases: a scenario ShakeMap simulating ground shakings if such an event occurs, or a local test ShakeMap from an actual earthquake triggered by the administrator.");
$faq[] = array("Some metrics of ground shaking measures are missing in the ShakeCast summary table!", "Not all ShakeMaps are created equal.  All ShakeMaps come with the basic three metrics for ground shaking measures: Modified Mercalli Intensity (MMI), Peak Horizontal Ground Acceleration (PGA), and Peak Horizontal Ground Velocity (PGV).  Peak Spectral Acceleration at 0.3, 1.0, and 3.0 seconds are optional metrics and are usually computed for earthquakes with magnitude of 4.0 or greater depending on the regions which the ShakeMaps were created.");
$faq[] = array("Why are there more than one ShakeCast summary table for the latest earthquake?", "The creation of ShakeMaps is cumulative immediately after earthquakes and the quality of ShakeMaps improves as source information of the earthquake and more seismic recordings from field become available.  As results, there usually are more than one version of ShakeMap produced for moderate size earthquakes or larger.  The ShakeCast system tracks changes of ground shaking estimates from ShakeMap and generates new damage assessment accordingly.");
$faq[] = array("What is a ShakeCast facility?", "Facility is the term used in ShakeCast for all user defined structure or point/area of interest.  Required information of a ShakeCast facility include a geographic location or an area in latitude and longitude with a defined structure type.  Ground shaking estimates at the location of the facility will be evaluated when there is a ShakeMap available.");
$faq[] = array("How is the damage level of a facility evaluated?", "The ShakeCast administrator defines ground motion thresholds and metrics for different damage states associated with a facility.  Currently there are three common approaches to assessing facility damage: pre-defined HAZUS based fragiity settings, MMI Intensity based settings and customized damage functions.");
$faq[] = array("How many possible damage levels are there for a facility?", "The default ShakeCast configuration consists of four damage state levels: 'GREEN', 'YELLOW', 'ORANGE', and 'RED'.  These can correspond to any states of interest to the user, such as 'Damage Unlikely', 'Minor Damage Possible', and 'Damage Likely', for example.  However, ShakeCast allows the user to specify the number of damge states.");
$faq[] = array("What about the facilities in the summary table without any damage state estimates?", "The two likely reasons for this are: there is no fragility settings associated with the facility or the metric of the ground shaking measures is simply not aviable in the ShakeMap products for ShakeCast to evaluate the damage state.");
$faq[] = array("How do I find out the damage level settings for a facility?", "You may use the search function to learn the detailed information of a facility.  The result of facility search includes facility location, structure type, fragility settings, and ground shaking and damage state estimates from previously processed ShakeMaps.");
 

$faq[] = array("--","User Preferences and settings");
$faq[] = array("How do I change my settings?", "All your settings (if you are registered) are stored in the database. To alter them click the <u>Profile</u> link (generally shown at the top of pages but this may not be the case). This will allow you to change all your settings.");
$faq[] = array("How do I change my contact information?", "Your contact information is grouped under the 'General Settings' tab. To modify the settings shown in the form, edit the content fields then submit the form.  New password will take effect after the current active session is closed, either by closing the browser window or by logging out of the ShakeCast system.");
$faq[] = array("When will I begin receiving ShakeCast notifications?", "In order to receive ShakeCast notifications, you need an active account and have subscribed to at least one notification profile.  Notifications will be sent to your default email address until a custom delivery method is registered with the system.");
$faq[] = array("Why do I need different delivery methods?", "You don't have to -- if your default email address is capable of receiving content rich messages and is easily accessible.  The file size for a comprehensive damage summary could be several hundred kilobytes and can flood the inbox of a mobile device easily.  ");
$faq[] = array("What are the differences between these delivery methods?", "Basicaly file size and HTML support of your viewing application/device.  'HTML Email' is designed for content rich messages, 'Text Email' for plain text version of the same message, and 'Pager or SMS' just for a brief summary.  It is really up to the administrator to decide the formats of notification templates with which will affect the choice for proper delivery methods.");
$faq[] = array("Can I use my cellular mobile phone to receive ShakeCast notifications?", "Quite possible -- most mobile phone carriers assign an email address to the cellphone in the form phonenumber@carrier.com/net, as outlined in the chart below.<p>Cingular: 0001112222@mobile.mycingular.net -- 160 characters - From, Message, Call Me Back At phone number and Subject<br>AT&T Wireless: 0001112222@mobile.att.net -- 110 chars for message. Subject length restriction is not provided by AT&T<br>Nextel: 0001112222@page.nextel.com -- 140 characters for message<br>Sprint PCS: 0001112222@messaging.sprintpcs.com -- 160 characters for message<br>T-Mobile: 0001112222@tmomail.net -- 140 characters for message<br>Verizon Wireless: 0001112222@vtext.com -- 160 characters for message");
$faq[] = array("I entered a custom delivery address but it doesn't work!", "The most likely reason for this is that the new email address has not been validated.  Email validation is required for any new delivery method registered with the system.  Without validation there will be a lock symbol associated with that particular delivery address.  Contact your administrator to unlock the address if the address has been validated.");
$faq[] = array("How do I subscribe for a notification profile?", "All available notification profiles are displayed under the 'Notification Profiles' tab.  To subscribe a profile simply select the profile row in the table then submit the selection.  The selected profile will be shown in the GIS map in the left panel.");
$faq[] = array("How do I unsubscribe a notification profile?", "Unchecked the profile then submit the change.  The unsubscribed profile will be removed from the GIS map.");
$faq[] = array("Can I define a custom notification profile?", "No, end users are not permitted to define their own notification profiles.  Ask your administrator to create a new profile if the current profiles do not meet your needs.");

//
// This ends the FAQ entries
//

?>