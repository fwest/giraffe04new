Subject: Validate your delivery address
Charset: iso-8859-1

Hello {USERNAME},

Your delivery address on "{SITENAME}" has been deactivated, due to changes made to your notification methods. In order to validate your delivery address you must click on the link below:

{U_ACTIVATE}

{EMAIL_SIG}