<?php
/*
# $Id: facility.php 517 2008-10-20 20:30:24Z klin $

# The PHP interface was originally inspired by PHPBB,
# file/database formats and directory structure are quite similar.

##############################################################################
# 
# Terms and Conditions of Software Use
# ====================================
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Disclaimer of Earthquake Information
# ====================================
# 
# The data and maps provided through this system are preliminary data
# and are subject to revision. They are computer generated and may not
# have received human review or official approval. Inaccuracies in the
# data may be present because of instrument or computer
# malfunctions. Subsequent review may result in significant revisions to
# the data. All efforts have been made to provide accurate information,
# but reliance on, or interpretation of earthquake data from a single
# source is not advised. Data users are cautioned to consider carefully
# the provisional nature of the information before using it for
# decisions that concern personal or public safety or the conduct of
# business that involves substantial monetary or operational
# consequences.
# 
# Disclaimer of Software and its Capabilities
# ===========================================
# 
# This software is provided as an "as is" basis.  Attempts have been
# made to rid the program of software defects and bugs, however the
# U.S. Geological Survey (USGS) have no obligations to provide maintenance, 
# support, updates, enhancements or modifications. In no event shall USGS 
# be liable to any party for direct, indirect, special, incidental or 
# consequential damages, including lost profits, arising out of the use 
# of this software, its documentation, or data obtained though the use 
# of this software, even if USGS or have been advised of the
# possibility of such damage. By downloading, installing or using this
# program, the user acknowledges and understands the purpose and
# limitations of this software.
# 
# Contact Information
# ===================
# 
# Coordination of this effort is under the auspices of the USGS Advanced
# National Seismic System (ANSS) coordinated in Golden, Colorado, which
# functions as the clearing house for development, distribution,
# documentation, and support. For questions, comments, or reports of
# potential bugs regarding this software please contact wald@usgs.gov or
# klin@usgs.gov.  
#
#############################################################################
*/

define('IN_SC', true);
$sc_root_path = './';
include($sc_root_path . 'extension.inc');
include($sc_root_path . 'common.'.$phpEx);

//
// Start session management
//
$userdata = session_pagestart($user_ip, $sc_list_id);
init_userprefs($userdata);
//
// End session management
//

//
// Start initial var setup
//
if ( isset($HTTP_GET_VARS['id']) || isset($HTTP_POST_VARS['id']) )
{
	$fac_id = ( isset($HTTP_GET_VARS['id']) ) ? intval($HTTP_GET_VARS['id']) : intval($HTTP_POST_VARS['id']);
}
else
{
	$fac_id = 0;
}

//
// Start auth check
//
if ( !$userdata['session_logged_in'] )
{
	redirect(append_sid("login.$phpEx?redirect=facility.$phpEx&id=$fac_id", true));
}
//
// End auth check
//

if (isset($HTTP_GET_VARS['sort_key']))
{
	$sort_key = $HTTP_GET_VARS['sort_key'];
	$sort_order = ( isset($HTTP_GET_VARS['sort_order']) ) ? $HTTP_GET_VARS['sort_order'] : 'd';
	$default_sort = array($sort_key, $sort_order);
	if ($sort_key != 'damage') 
	{
		$default_sort = array_merge( $default_sort , array('damage', 'd'));
	}
}
else 
{
	$sort_key = 'damage';
	$sort_order = ( isset($HTTP_GET_VARS['sort_order']) ) ? $HTTP_GET_VARS['sort_order'] : 'd';
	$default_sort = array($sort_key, $sort_order);
}

$new_sort_order = ($sort_order == 'd') ? 'a' : 'd';
$img_url = ' <img src="'.$sc_root_path . '/images/' . $sort_order . '.png" border="0" width="10" height="10">';
$start = ( isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;
$start = ($start < 0) ? 0 : $start;
$base_url = append_sid("facility.$phpEx?id=$fac_id");

if (!$fac_id)
{
	message_die(GENERAL_MESSAGE, $lang['No_facility_selected']);
}

//
// Output page header
//
$page_title = $lang['Facility'];
//$active_class = 'C_EARTHQUAKE';
$header_tpl = 'facility_header.tpl';
include($sc_root_path . 'includes/page_header.'.$phpEx);

$template->set_filenames(array(
	"body" => "facility_body.tpl")
);


//
// Go ahead and pull all data for this event
//
$sql = "SELECT facility_id, facility_type, external_facility_id, facility_name, short_name,
			description, lat_min, lat_max, lon_min, lon_max 
		FROM facility WHERE facility_id = $fac_id";
$result = $db->sql_query($sql);
if( !$result )
{
	message_die(GENERAL_ERROR, "Couldn't get facility types.", "", __LINE__, __FILE__, $sql );
}
$facility = $db->sql_fetchrow($result);

$sql = "SELECT ff.facility_fragility_id, ff.damage_level, dl.name, ff.low_limit, ff.high_limit, ff.metric 
		FROM facility_fragility ff INNER JOIN damage_level dl on ff.damage_level = dl.damage_level
		WHERE ff.facility_id = $fac_id";
$result = $db->sql_query($sql);
if($result)
{
	while ($row = $db->sql_fetchrow($result)) {
		$facility_fragility[$row['facility_fragility_id']] = array(
			"damage_level"	=>	$row['damage_level'],
			"name"	=>	$row['name'],
			"low_limit"	=>	$row['low_limit'],
			"high_limit"	=>	$row['high_limit'],
			"metric"	=>	$row['metric']);
	}
}

$sql = "SELECT attribute_name, attribute_value FROM facility_attribute WHERE facility_id = $fac_id";
$result = $db->sql_query($sql);
if($result )
{
	while ($row = $db->sql_fetchrow($result)) {
		$facility_attribute[$row['attribute_name']] = $row['attribute_value'];
	}
}

$template->assign_vars(array(
	"FACILITY_TYPE" => $facility['facility_type'],
	"EXTERNAL_FACILITY_ID" => $facility['external_facility_id'],
	"FACILITY_NAME" => $facility['facility_name'],
	"SHORT_NAME" => $facility['short_name'],
	"DESCRIPTION" => $facility['description'],
	"LAT_MIN" => $facility['lat_min'],
	"LAT_MAX" => $facility['lat_max'],
	"LON_MIN" => $facility['lon_min'],
	"LON_MAX" => $facility['lon_max'],
	"F_HIDDEN_FIELDS" => $hidden_fields,
	"FAC_ID" => $fac_id,
	"FACILITY_ID" => $fac_id,

	"L_FACILITY_ID" => $lang['Fac_Id'],
	"L_FACILITY_TYPE" => $lang['Fac_type'],
	"L_EXTERNAL_FACILITY_ID" => $lang['Ext_Fac_Id'],
	"L_FACILITY_NAME" => $lang['Fac_name'],
	"L_SHORT_NAME" => $lang['Short_name'],
	"L_DESCRIPTION" => $lang['Fac_description'],
	"L_LAT" => $lang['Lat'],
	"L_LON" => $lang['Lon'],

	"L_DAMAGE_LEVEL" => $lang['Damage_level'],
	"L_LOW_LIMIT" => $lang['Low_limit'],
	"L_HIGH_LIMIT" => $lang['High_limit'],
	"L_FRAGILITY_METRIC" => $lang['Metric'],

	"L_FACILITY_TITLE" => $lang['fac_title'],
	"L_FACILITY_TEXT" => $lang['fac_explain'],
	"L_FACILITY_EDIT" => $lang['facility_edit'],
	"L_FACILITY_ATTRIBUTE" => $lang['facility_attribute'],
	"L_SUBMIT" => $lang['Submit'],
	"L_DELETE" => $lang['Delete'],
	"U_DELETE" => append_sid("admin_facility.$phpEx?mode=delete&amp;id=$fac_id"),

	"S_FACILITY_ACTION" => append_sid("admin_facility.$phpEx"),
	"S_HIDDEN_FIELDS" => $s_hidden_fields)
);

// Grab the current list of damage levels
$sql = "SELECT damage_level, name FROM damage_level order by severity_rank";
$result = $db->sql_query($sql);
if( !$result )
{
	message_die(GENERAL_ERROR, "Couldn't get damage levels.", "", __LINE__, __FILE__, $sql );
}
$damage_level = $db->sql_fetchrowset($result);

// Grab the current list of metric
$sql = "SELECT short_name, name FROM metric";
$result = $db->sql_query($sql);
if( !$result )
{
	message_die(GENERAL_ERROR, "Couldn't get metric.", "", __LINE__, __FILE__, $sql );
}
$metric = $db->sql_fetchrowset($result);

for( $index = 0; $index < count($damage_level); $index++ ) {
	$facility_damage_level = $damage_level[$index]['damage_level'];
	$facility_damage_name = $damage_level[$index]['name'];
	$facility_low_limit = '';
	$facility_high_limit = '';
	$facility_metric = '';
	foreach ($facility_fragility as $fac_frag_id => $fac_frag) {
		if ($fac_frag['damage_level'] == $damage_level[$index]['damage_level']) {
			$facility_fragility_id = $fac_frag_id;
			$facility_low_limit = $fac_frag['low_limit'];
			$facility_high_limit = $fac_frag['high_limit'];
			$facility_metric = $fac_frag['metric'];
		}
	}
	
	$metric_select = '';
	for( $i = 0; $i < count($metric); $i++ )
	{
		if (strtoupper($metric[$i]['short_name']) == 'MMI') {
			$metric_name = $metric[$i]['name']; 
		} else {
			$metric_name = $metric[$i]['name'] . " " . $metric_unit[strtoupper($metric[$i]['short_name'])]; 
		}

		if($metric[$i]['short_name'] == $facility_metric)
		{
			$metric_select = $metric_name;
		}
	}

	$row_color = ( !($i % 2) ) ? $theme['td_color1'] : $theme['td_color2'];
	$row_class = ( !($i % 2) ) ? $theme['td_class1'] : $theme['td_class2'];

	$template->assign_block_vars('fragrow', array(
		"ROW_COLOR" => "#" . $row_color,
		"ROW_CLASS" => $row_class,

		"DAMAGE_LEVEL" => $facility_damage_name,
		"DAMAGE" => $facility_damage_level,
		"LOW_LIMIT" => $facility_low_limit,
		"HIGH_LIMIT" => $facility_high_limit,
		"METRIC" => $metric_select)
	);
}

// Grab the current list of facility attributes
$sql = "SELECT attribute_name 
	FROM facility_type_attribute
	WHERE facility_type ='".$facility['facility_type']."'";
$result = $db->sql_query($sql);
if( $result )
{
	while ($row = $db->sql_fetchrow($result)) {
		$attribute_value = ($facility_attribute[$row['attribute_name']]) ? $facility_attribute[$row['attribute_name']] : '';
		if ($row['attribute_name'] != '') {
			$template->assign_block_vars('attrow', array(
				"L_ATTRIB_NAME" => $row['attribute_name'],
				"ATTRIBUTE_VALUE" => $attribute_value)
			);
		}
	}
}

$sql = 
	"SELECT short_name, metric_id
	FROM ". METRIC_TABLE . "
	ORDER BY
		metric_id";
if ( !($result = $db->sql_query($sql)) )
{
   message_die(GENERAL_ERROR, 'Could not obtain ShakeMap metric information', '', __LINE__, __FILE__, $sql);
}
$metric_query = '';
$metric_tpl = '';
$metrics = array();
$facility_damage = array();
$damage_level = array();
while( $row = $db->sql_fetchrow($result) )
{
	$metrics[$row['short_name']] = $row['metric_id'];
	if (strtoupper($row['short_name']) == 'MMI') {
		if ($sort_key == strtoupper($row['short_name']))
		{
			$metric_tpl = "$metric_tpl<th><a href=\"" . 
			append_sid("facility.$phpEx?id=$fac_id&sort_key=".strtoupper($row['short_name'])."&sort_order=" . $new_sort_order)
			. "\">" . strtoupper($row['short_name']) . $img_url. "</a></th>";
		}
		else
		{
			$metric_tpl = "$metric_tpl<th><a href=\"" . 
			append_sid("facility.$phpEx?id=$fac_id&sort_key=".strtoupper($row['short_name'])."&sort_order=" . $sort_order)
			. "\">" . strtoupper($row['short_name']) . "</a></th>";
		}
	} else {
		if ($sort_key == strtoupper($row['short_name']))
		{
			$metric_tpl = "$metric_tpl<th><a href=\"" . 
			append_sid("facility.$phpEx?id=$fac_id&sort_key=".strtoupper($row['short_name'])."&sort_order=" . $new_sort_order)
			. "\">" . strtoupper($row['short_name']) . " " .$metric_unit[strtoupper($row['short_name'])] . $img_url. "</a></th>";
		}
		else
		{
			$metric_tpl = "$metric_tpl<th><a href=\"" . 
			append_sid("facility.$phpEx?id=$fac_id&sort_key=".strtoupper($row['short_name'])."&sort_order=" . $sort_order)
			. "\">" . strtoupper($row['short_name']) . " " .$metric_unit[strtoupper($row['short_name'])] . "</a></th>";
		}
	}
}
$db->sql_freeresult($result);

//
// Okay, let's do the loop, yeah come on baby let's do the loop
// and it goes like this ...
//
$sql = 
	"SELECT g.shakemap_id, g.shakemap_version, g.grid_id
	FROM 
		(((grid g INNER JOIN facility_shaking fs on
			g.grid_id = fs.grid_id) INNER JOIN shakemap s on
			g.shakemap_id = s.shakemap_id AND g.shakemap_version = s.shakemap_version) INNER JOIN event e on
			s.event_id = e.event_id and s.event_version = e.event_version)
	WHERE
		fs.facility_id = \"$fac_id\"";

if ( ($result = $db->sql_query($sql)) )
{
	$shakemap_rows = $db->sql_fetchrowset($result);
}

$postrow = array();
$total_posts = 0;
$facility_damage = array();
$damage_level = array();
for($i = 0; $i < count($shakemap_rows); $i++)
{
	$event_id = $shakemap_rows[$i]['shakemap_id'];
	$event_version = $shakemap_rows[$i]['shakemap_version'];
	$grid_id = $shakemap_rows[$i]['grid_id'];

	$sql = 
		"SELECT g.grid_id, sm.metric, sm.value_column_number
		FROM 
			(grid g INNER JOIN shakemap_metric sm on
				g.shakemap_id = sm.shakemap_id AND g.shakemap_version = sm.shakemap_version)
		WHERE
			g.shakemap_id = \"$event_id\" AND g.shakemap_version = \"$event_version\"
			AND sm.value_column_number IS NOT NULL
		ORDER BY sm.value_column_number";
	if ( !($result = $db->sql_query($sql)) )
	{
	   message_die(GENERAL_ERROR, 'Could not obtain ShakeMap event information', '', __LINE__, __FILE__, $sql);
	}
	$metric_query = '';
	$metric_item = array();
	while( $row = $db->sql_fetchrow($result) )
	{
		$metric_query = ", " . 'value_'.$row['value_column_number'] . " as ". strtoupper($row['metric']) . " $metric_query";
		$metric_item[$metrics[$row['metric']]] = $row['metric'];
			
		$sql = "select ff.damage_level, dl.name, dl.severity_rank, ff.facility_id
			from grid g
			   straight_join shakemap s
			   straight_join event e
			   straight_join facility_shaking sh
			   straight_join facility_fragility ff
			   inner join damage_level dl on ff.damage_level = dl.damage_level
			where ff.facility_id = $fac_id
				and ff.metric = '".$row['metric']."'
				and s.shakemap_id = '".$event_id."'
				and s.shakemap_version = ".$event_version."
				and g.grid_id = ".$grid_id."
				and s.event_id = e.event_id and s.event_version = e.event_version
				and g.grid_id = sh.grid_id
				and (s.shakemap_id = g.shakemap_id and
				s.shakemap_version = g.shakemap_version)
				and sh.facility_id = ff.facility_id
				and sh.value_".$row['value_column_number']." between ff.low_limit and ff.high_limit";
		if (($facility_result = $db->sql_query($sql)) )
		{
			$damage_estimate = $db->sql_fetchrowset($facility_result);
			$db->sql_freeresult($facility_result);
			for ($ind = 0; $ind < count($damage_estimate); $ind++) {
				$facility_damage[$grid_id] = $damage_estimate[$ind]['damage_level'];
				$damage_level[$damage_estimate[$ind]['damage_level']] = $damage_estimate[$ind]['name'];
				$severity_rank[$grid_id] = $damage_estimate[$ind]['severity_rank'];
			}
		}
	}
	$db->sql_freeresult($result);
	

	$sql = "SELECT f.facility_id,f.external_facility_id,f.lat_min, f.lon_min, f.facility_name, f.facility_type, 
				g.grid_id, s.shakemap_id, s.shakemap_version, s.receive_timestamp, s.shakemap_region, 
				e.event_type, e.event_location_description, e.magnitude, e.lat, e.lon, e.event_timestamp
				$metric_query
		FROM ((((" . SHAKEMAP_TABLE ." s INNER JOIN ". EVENT_TABLE . " e on
			s.event_id = e.event_id AND s.event_version = e.event_version) 
			INNER JOIN grid g on	g.shakemap_id = s.shakemap_id AND g.shakemap_version = s.shakemap_version) 
			INNER JOIN facility_shaking fs on fs.grid_id = g.grid_id) 
			INNER JOIN facility f on fs.facility_id = f.facility_id)
		WHERE 
			g.grid_id = \"$grid_id\" AND f.facility_id = \"$fac_id\"";
	if ( !($result = $db->sql_query($sql)) )
	{
		message_die(GENERAL_ERROR, "Could not obtain ShakeCast facility information.", '', __LINE__, __FILE__, $sql);
	}
	
	if ($row = $db->sql_fetchrow($result)) {
		$metric_value = '';
		for ($ind = 1; $ind <= count($metrics); $ind++) {
			if ($metric_item[$ind] != '') {
				$m_value = $metric_item[$ind];
				if (strtoupper($m_value) == 'MMI') {
					$metric_value = "$metric_value<td onclick=\"switch_plot('".$row['facility_id']."', '".$m_value."')\">".$mmi_numeric[intval($row[$m_value])]."</td>";
				} else {
					$metric_value = "$metric_value<td onclick=\"switch_plot('".$row['facility_id']."', '".$m_value."')\">".$row[$m_value]."</td>";
				}
			} else {
				$metric_value = "$metric_value<td>NA</td>";
			}
		}
		$fac_damage = ($severity_rank[$grid_id]) ? $severity_rank[$grid_id] : 0;
	}
	$postrow[] = array_merge($row, array('metric_value' => $metric_value, 'damage' => $fac_damage, 'exceedance' => $exceedance['f'.$row['facility_id']]));

}
switch ($sort_key) {
	case 'event_type':
		$order_arr = array(
			$default_sort, array('shakemap_id', $sort_order), array('shakemap_version', $sort_order));
		break;
	case 'magnitude':
		$order_arr = array(
			$default_sort, array('lat', $sort_order), array('lon', $sort_order));
		break;
	default:
		$order_arr = array($default_sort);
}

$postrow = arfsort( $postrow, $order_arr);
$total_posts = count($postrow);
$pagination = generate_pagination("facility.$phpEx?id=$fac_id", 
	$total_posts, $board_config['topics_per_page'], $start);

//
// Send vars to template
//
$template->assign_vars(array(
    'TOPIC_TITLE' => "ShakeCast Facility Summary",
	'PAGINATION' => $pagination,
	'PAGE_NUMBER' => sprintf($lang['Page_of'], ( floor( $start / $board_config['topics_per_page'] ) + 1 ), ceil( $total_posts / $board_config['topics_per_page'] )), 

	'L_ID' =>	"Mag" . (($sort_key == 'damage') ? $img_url : ''),
	'L_METRIC' =>	$metric_tpl,
	'L_TYPE' => "Type" . (($sort_key == 'event_type') ? $img_url : ''),
	'L_NAME' => "Earthquake" . (($sort_key == 'magnitude') ? $img_url : ''),
	'L_DAMAGE_ESTIMATE' => $lang['Damage_estimate'] . (($sort_key == 'damage') ? $img_url : ''),
	'L_LOCATION' => "Location" . (($sort_key == 'event_location_description') ? $img_url : ''),
	
	'U_TYPE' => $base_url."&sort_key=event_type&sort_order=" . (($sort_key == 'event_type') ? $new_sort_order : $sort_order),
	'U_NAME' => $base_url."&sort_key=magnitude&sort_order=" . (($sort_key == 'magnitude') ? $new_sort_order : $sort_order),
	'U_DAMAGE_ESTIMATE' => $base_url."&sort_key=damage&sort_order=" . (($sort_key == 'damage') ? $new_sort_order : $sort_order),
	'U_LOCATION' => $base_url."&sort_key=event_location_description&sort_order=" . (($sort_key == 'event_location_description') ? $new_sort_order : $sort_order),

	)
);

for($i = $start; $i < $total_posts && $i < ($board_config['topics_per_page'] + $start); $i++)
{
	$grid_id = $postrow[$i]['grid_id'];
	$event_id = $postrow[$i]['shakemap_id'];
	$event_version = $postrow[$i]['shakemap_version'];
	
	$name = 'M'.$postrow[$i]['magnitude'].' ('.htmlspecialchars(sprintf("%7.3f", $postrow[$i]['lat_min']) . '/' . 
			sprintf("%8.3f", $postrow[$i]['lon_min'])). ')<br>' .$postrow[$i]['event_timestamp'];
	$template->assign_block_vars('postrow', array(
		'ID' => $postrow[$i]['magnitude'],
		'FACILITY_ID' => $postrow[$i]['facility_id'],
		'FACILITY_DAMAGE' => $facility_damage[$grid_id],
		'DAMAGE_LEVEL' => ($damage_level[$facility_damage[$grid_id]] ? $damage_level[$facility_damage[$grid_id]] : 'Not Evaluated'),
		'TYPE' => $postrow[$i]['event_type'] . '<br>' . $event_id . ' - ' . $event_version,
		'NAME' => $name.count($shakemap_rows),
		'LATITUDE' => $postrow[$i]['lat'],
		'LONGITUDE' => $postrow[$i]['lon'],
		'METRIC' => $postrow[$i]['metric_value'],
		'DESCRIPTION' => htmlspecialchars($postrow[$i]['event_location_description']),
			
		'U_NAME' => append_sid("event.$phpEx?" . SC_LIST_URL . "=2&" . 
		EVENT_TOPIC_URL . "=$event_id&" . EVENT_TOPIC_VERSION . "=$event_version")			
		)
	);
}

$template->pparse('body');


$shake_plot = '/shakecast/sc/bin/facility_shaking_plot.pl';
$result = exec('perl '.$shake_plot.' '.$fac_id, $output);
include($sc_root_path . 'includes/page_tail.'.$phpEx);

?>