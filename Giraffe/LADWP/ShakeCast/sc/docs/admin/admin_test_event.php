<?php
/*
# $Id: admin_test_event.php 512 2008-10-20 14:51:53Z klin $

# The PHP interface was originally inspired by PHPBB,
# file/database formats and directory structure are quite similar.

##############################################################################
# 
# Terms and Conditions of Software Use
# ====================================
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Disclaimer of Earthquake Information
# ====================================
# 
# The data and maps provided through this system are preliminary data
# and are subject to revision. They are computer generated and may not
# have received human review or official approval. Inaccuracies in the
# data may be present because of instrument or computer
# malfunctions. Subsequent review may result in significant revisions to
# the data. All efforts have been made to provide accurate information,
# but reliance on, or interpretation of earthquake data from a single
# source is not advised. Data users are cautioned to consider carefully
# the provisional nature of the information before using it for
# decisions that concern personal or public safety or the conduct of
# business that involves substantial monetary or operational
# consequences.
# 
# Disclaimer of Software and its Capabilities
# ===========================================
# 
# This software is provided as an "as is" basis.  Attempts have been
# made to rid the program of software defects and bugs, however the
# U.S. Geological Survey (USGS) have no obligations to provide maintenance, 
# support, updates, enhancements or modifications. In no event shall USGS 
# be liable to any party for direct, indirect, special, incidental or 
# consequential damages, including lost profits, arising out of the use 
# of this software, its documentation, or data obtained though the use 
# of this software, even if USGS or have been advised of the
# possibility of such damage. By downloading, installing or using this
# program, the user acknowledges and understands the purpose and
# limitations of this software.
# 
# Contact Information
# ===================
# 
# Coordination of this effort is under the auspices of the USGS Advanced
# National Seismic System (ANSS) coordinated in Golden, Colorado, which
# functions as the clearing house for development, distribution,
# documentation, and support. For questions, comments, or reports of
# potential bugs regarding this software please contact wald@usgs.gov or
# klin@usgs.gov.  
#
#############################################################################
*/

if( !empty($setmodules) )
{
	$file = basename(__FILE__);
	$module['Event_Admin']['Test_Event'] = $file;
	return;
}

define('IN_SC', 1);

//
// Load default header
//
$tester = '/shakecast/sc/bin/tester.pl';
$manage_event = '/shakecast/sc/bin/manage_event.pl';
$sc_root_path = "./../";
require($sc_root_path . 'extension.inc');

$cancel = ( isset($HTTP_POST_VARS['cancel']) ) ? true : false;
$no_page_header = $cancel;

require('./pagestart.' . $phpEx);

@set_time_limit(1200);

if ($cancel)
{
	redirect('admin/' . append_sid("admin_test_event.$phpEx", true));
}

if( isset($HTTP_GET_VARS['mode']) || isset($HTTP_POST_VARS['mode']) )
{
	$mode = (isset($HTTP_GET_VARS['mode'])) ? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode'];
	$mode = htmlspecialchars($mode);
}
else 
{
	//
	// These could be entered via a form button
	//
	if( isset($HTTP_POST_VARS['add_event']) )
	{
		$mode = "add_event";
	}
	else if( isset($HTTP_POST_VARS['save']) )
	{
		$mode = "save";
	}
	else
	{
		$mode = "";
	}
}

if (isset($HTTP_GET_VARS['sort_key']))
{
	$sort_key = $HTTP_GET_VARS['sort_key'];
}
else 
{
	$sort_key = 'event_id';
}
$sort_order = ( isset($HTTP_GET_VARS['sort_order']) ) ? $HTTP_GET_VARS['sort_order'] : 'a';
$default_sort = array($sort_key, $sort_order);

$start = ( isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;
$start = ($start < 0) ? 0 : $start;

// Restrict mode input to valid options
$mode = ( in_array($mode, array('add', 'inject_first', 'inject_next', 'delete', 'add_event')) ) ? $mode : '';

if( $mode != "" )
{
	if( $mode == "edit" || $mode == "add" )
	{
		$word_id = ( isset($HTTP_GET_VARS['id']) ) ? intval($HTTP_GET_VARS['id']) : 0;

		$template->set_filenames(array(
			"body" => "admin/words_edit_body.tpl")
		);

		$word_info = array('word' => '', 'replacement' => '');
		$s_hidden_fields = '';

		if( $mode == "edit" )
		{
			if( $word_id )
			{
				$sql = "SELECT * 
					FROM " . WORDS_TABLE . " 
					WHERE word_id = $word_id";
				if(!$result = $db->sql_query($sql))
				{
					message_die(GENERAL_ERROR, "Could not query words table", "Error", __LINE__, __FILE__, $sql);
				}

				$word_info = $db->sql_fetchrow($result);
				$s_hidden_fields .= '<input type="hidden" name="id" value="' . $word_id . '" />';
			}
			else
			{
				message_die(GENERAL_MESSAGE, $lang['No_word_selected']);
			}
		}

		$template->assign_vars(array(
			"WORD" => $word_info['word'],
			"REPLACEMENT" => $word_info['replacement'],

			"L_WORDS_TITLE" => $lang['test_title'],
			"L_WORDS_TEXT" => $lang['test_explain'],
			"L_WORD_CENSOR" => $lang['Edit_word_censor'],
			"L_WORD" => $lang['Word'],
			"L_REPLACEMENT" => $lang['Replacement'],
			"L_SUBMIT" => $lang['Submit'],

			"S_WORDS_ACTION" => append_sid("admin_words.$phpEx"),
			"S_HIDDEN_FIELDS" => $s_hidden_fields)
		);

		$template->pparse("body");

		include('./page_footer_admin.'.$phpEx);
	}
	else if( $mode == "save" )
	{
		$word_id = ( isset($HTTP_POST_VARS['id']) ) ? intval($HTTP_POST_VARS['id']) : 0;
		$word = ( isset($HTTP_POST_VARS['word']) ) ? trim($HTTP_POST_VARS['word']) : "";
		$replacement = ( isset($HTTP_POST_VARS['replacement']) ) ? trim($HTTP_POST_VARS['replacement']) : "";

		if($word == "" || $replacement == "")
		{
			message_die(GENERAL_MESSAGE, $lang['Must_enter_word']);
		}

		if( $word_id )
		{
			$sql = "UPDATE " . WORDS_TABLE . " 
				SET word = '" . str_replace("\'", "''", $word) . "', replacement = '" . str_replace("\'", "''", $replacement) . "' 
				WHERE word_id = $word_id";
			$message = $lang['Word_updated'];
		}
		else
		{
			$sql = "INSERT INTO " . WORDS_TABLE . " (word, replacement) 
				VALUES ('" . str_replace("\'", "''", $word) . "', '" . str_replace("\'", "''", $replacement) . "')";
			$message = $lang['Word_added'];
		}

		if(!$result = $db->sql_query($sql))
		{
			message_die(GENERAL_ERROR, "Could not insert data into words table", $lang['Error'], __LINE__, __FILE__, $sql);
		}

		$message .= "<br /><br />" . sprintf($lang['Click_return_wordadmin'], "<a href=\"" . append_sid("admin_words.$phpEx") . "\">", "</a>") . "<br /><br />" . sprintf($lang['Click_return_admin_index'], "<a href=\"" . append_sid("index.$phpEx?pane=right") . "\">", "</a>");

		message_die(GENERAL_MESSAGE, $message);
	}
	else if( $mode == "delete" )
	{
		if( isset($HTTP_POST_VARS['id']) ||  isset($HTTP_GET_VARS['id']) )
		{
			$event_id = ( isset($HTTP_POST_VARS['id']) ) ? $HTTP_POST_VARS['id'] : $HTTP_GET_VARS['id'];
		}
		else
		{
			$event_id = 0;
		}

		$confirm = isset($HTTP_POST_VARS['confirm']); 

		if( $event_id && $confirm )
		{
			$result = exec('perl '.$manage_event.' -'.$mode.' '.$event_id, $output);

			/*if(!$result)
			{
				message_die(GENERAL_ERROR, "Could not ".$mode." event", $lang['Error']);
			}*/
			
			$dir = @opendir($board_config['DataRoot']);
			
			while( $file = @readdir($dir) )
			{
				if( preg_match("/^$event_id\-*\d*/", $file) )
				{
					remove_directory($board_config['DataRoot'].'/'.$file);
					rmdir($board_config['DataRoot'].'/'.$file);
				}
			}
			
			@closedir($dir);

			$dir = @opendir($board_config['RootDir'].'/test_data');
			
			while( $file = @readdir($dir) )
			{
				if( preg_match("/^$event_id$/", $file) )
				{
					remove_directory($board_config['RootDir'].'/test_data/'.$file);
					rmdir($board_config['RootDir'].'/test_data/'.$file);
				}
			}
			
			@closedir($dir);

			$message = $lang['event_'.$mode] . "<br /><br />" . sprintf($lang['Click_return_testadmin'], "<a href=\"" . append_sid("admin_test_event.$phpEx") . "\">", "</a>") . "<br /><br />" . sprintf($lang['Click_return_admin_index'], "<a href=\"" . append_sid("index.$phpEx?pane=right") . "\">", "</a>");

			message_die(GENERAL_MESSAGE, $message);
		}
		elseif( $event_id && !$confirm)
		{
			// Present the confirmation screen to the user
			$template->set_filenames(array(
				'body' => 'admin/confirm_body.tpl')
			);

			$hidden_fields = '<input type="hidden" name="mode" value="'.$mode.'" /><input type="hidden" name="id" value="' . $event_id . '" />';

			$template->assign_vars(array(
				'MESSAGE_TITLE' => $lang['Confirm'],
				'MESSAGE_TEXT' => $lang['confirm_event_'.$mode],

				'L_YES' => $lang['Yes'],
				'L_NO' => $lang['No'],

				'S_CONFIRM_ACTION' => append_sid("admin_test_event.$phpEx"),
				'S_HIDDEN_FIELDS' => $hidden_fields)
			);
		}
		else
		{
			message_die(GENERAL_MESSAGE, $lang['No_event_selected']);
		}
	}
	else if( $mode == "inject_first" || $mode == "inject_next")
	{
		if( isset($HTTP_POST_VARS['id']) ||  isset($HTTP_GET_VARS['id']) )
		{
			$event_id = ( isset($HTTP_POST_VARS['id']) ) ? $HTTP_POST_VARS['id'] : $HTTP_GET_VARS['id'];
		}
		else
		{
			message_die(GENERAL_ERROR, "Could not trigger test event", $event_id);
		}

		$confirm = isset($HTTP_POST_VARS['confirm']);

		if( $event_id && $confirm )
		{
			$result = exec('perl '.$tester.' -type '.$mode.' -key '.$event_id, $output);

			if(!$result)
			{
				message_die(GENERAL_ERROR, "Could not trigger test event", $lang['Error']);
			}

			$message = $lang['test_event_injected'] . "<br /><br />" . sprintf($lang['Click_return_testadmin'], "<a href=\"" . append_sid("admin_test_event.$phpEx") . "\">", "</a>") . "<br /><br />" . sprintf($lang['Click_return_admin_index'], "<a href=\"" . append_sid("index.$phpEx?pane=right") . "\">", "</a>");

			message_die(GENERAL_MESSAGE, $message);
		}
		elseif( $event_id && !$confirm)
		{
			// Present the confirmation screen to the user
			$template->set_filenames(array(
				'body' => 'admin/confirm_body.tpl')
			);

			$hidden_fields = '<input type="hidden" name="mode" value="'.$mode.'" /><input type="hidden" name="id" value="' . $event_id . '" />';

			$template->assign_vars(array(
				'MESSAGE_TITLE' => $lang['Confirm'],
				'MESSAGE_TEXT' => $lang['Confirm_test_event'],

				'L_YES' => $lang['Yes'],
				'L_NO' => $lang['No'],

				'S_CONFIRM_ACTION' => append_sid("admin_test_event.$phpEx"),
				'S_HIDDEN_FIELDS' => $hidden_fields)
			);
		}
		else
		{
			message_die(GENERAL_MESSAGE, $lang['No_test_event_selected']);
		}
	}
	else if( $mode == "add_event")
	{
		if( isset($HTTP_POST_VARS['id']) ||  isset($HTTP_GET_VARS['id']) )
		{
			$event_id = ( isset($HTTP_POST_VARS['id']) ) ? $HTTP_POST_VARS['id'] : $HTTP_GET_VARS['id'];
		}

		if( $event_id )
		{
			$result = exec('perl '.$tester.' -type create_test -key '.$event_id, $output);

			if(!$result)
			{
				message_die(GENERAL_ERROR, "Could not create test event", $lang['Error']);
			}

			$message = $lang['test_event_created'] . "<br /><br />" . sprintf($lang['Click_return_testadmin'], "<a href=\"" . append_sid("admin_test_event.$phpEx") . "\">", "</a>") . "<br /><br />" . sprintf($lang['Click_return_admin_index'], "<a href=\"" . append_sid("index.$phpEx?pane=right") . "\">", "</a>");

			message_die(GENERAL_MESSAGE, $message);
		} 
		else
		{
			$template->set_filenames(array(
				"body" => "admin/test_event_create_body.tpl")
			);
		
			$result = exec('perl '.$tester.' -type new_test', $output);
			$field_name = explode("::", $output[0]);
			for($i = 1;$i < count($output);  $i++)
			{
				$fields = explode("::", $output[$i]);
				$row['event_id'] = $fields[0];
				$row['event_timestamp'] = $fields[2];
				$row['magnitude'] = $fields[3];
				$row['lat'] = $fields[4];
				$row['lon'] = $fields[5];
				$row['desc'] = $fields[6];

				if ( $row['event_id'] != '' && $row['magnitude'] != 0 ) {
					$topic_rowset[] = $row;
				}
			}

			$topic_rowset = arfsort( $topic_rowset, array($default_sort));
			$event_count= count($topic_rowset);
				
			$current_page = ( !$event_count ) ? 1 : ceil( $event_count / $board_config['topics_per_page'] );
			$template->assign_vars(array(
				'PAGINATION' => generate_pagination("admin_test_event.$phpEx?" . POST_GROUPS_URL . "=$group_id&mode=add_event&sort_key=$sort_key&sort_order=$sort_order", $event_count, $board_config['topics_per_page'], $start),
				'PAGE_NUMBER' => sprintf($lang['Page_of'], ( floor( $start / $board_config['topics_per_page'] ) + 1 ), $current_page ), 

				'L_GOTO_PAGE' => $lang['Goto_page'])
			);
			$new_sort_order = ($sort_order == 'd') ? 'a' : 'd';
			$img_url = ' <img src="/images/' . $sort_order . '.png" border="0" width="10" height="10">';
			$base_url = append_sid("admin_test_event.$phpEx?" . POST_GROUPS_URL . "=$group_id&mode=add_event");

			$template->assign_vars(array(
				"L_WORDS_TITLE" => "Create ShakeCast Test Event",
				"L_WORDS_TEXT" => $lang['test_explain'],
				"L_EVENT_ID" => $lang[$field_name[0]] . (($sort_key == 'event_id') ? $img_url : ''),
				"L_EVENT_TIMESTAMP" => $lang[$field_name[2]] . (($sort_key == 'event_timestamp') ? $img_url : ''),
				"L_MAGNITUDE" => $lang[$field_name[3]] . (($sort_key == 'magnitude') ? $img_url : ''),
				"L_LATITUDE" => $lang[$field_name[4]] . (($sort_key == 'lat') ? $img_url : ''),
				"L_LONGITUDE" => $lang[$field_name[5]] . (($sort_key == 'lon') ? $img_url : ''),
				"L_DESCRIPTION" => $lang[$field_name[6]] . (($sort_key == 'desc') ? $img_url : ''),
				"L_ADD_EVENT" => "Add",
				"L_ACTION" => $lang['Action'],
		
				'U_EVENT_ID' =>  $base_url."&sort_key=event_id&sort_order=" . (($sort_key == 'event_id') ? $new_sort_order : $sort_order),
				'U_EVENT_TIMESTAMP' =>  $base_url."&sort_key=event_timestamp&sort_order=" . (($sort_key == 'event_timestamp') ? $new_sort_order : $sort_order),
				'U_MAGNITUDE' =>  $base_url."&sort_key=magnitude&sort_order=" . (($sort_key == 'magnitude') ? $new_sort_order : $sort_order),
				'U_LATITUDE' =>  $base_url."&sort_key=lat&sort_order=" . (($sort_key == 'lat') ? $new_sort_order : $sort_order),
				'U_LONGITUDE' =>  $base_url."&sort_key=lon&sort_order=" . (($sort_key == 'lon') ? $new_sort_order : $sort_order),
				'U_DESCRIPTION' =>  $base_url."&sort_key=desc&sort_order=" . (($sort_key == 'desc') ? $new_sort_order : $sort_order),

				"S_WORDS_ACTION" => append_sid("admin_test_event.$phpEx"),
				"S_HIDDEN_FIELDS" => '')
			);
		
			for($i = $start;$i < $event_count && $i < ($board_config['topics_per_page'] + $start);  $i++)
			{
				$event_id = $topic_rowset[$i]['event_id'];
				$event_timestamp = $topic_rowset[$i]['event_timestamp'];
				$magnitude = $topic_rowset[$i]['magnitude'];
				$lat = $topic_rowset[$i]['lat'];
				$lon = $topic_rowset[$i]['lon'];
				$desc = $topic_rowset[$i]['desc'];
		
				$row_color = ( !($i % 2) ) ? $theme['td_color1'] : $theme['td_color2'];
				$row_class = ( !($i % 2) ) ? $theme['td_class1'] : $theme['td_class2'];
		
				$template->assign_block_vars("words", array(
					"ROW_COLOR" => "#" . $row_color,
					"ROW_CLASS" => $row_class,
					"EVENT_ID" => $event_id,
					"EVENT_TIMESTAMP" => $event_timestamp,
					"MAGNITUDE" => $magnitude,
					"LATITUDE" => $lat,
					"LONGITUDE" => $lon,
					"DESCRIPTION" => $desc,
		
					"U_EVENT" => append_sid("admin_test_event.$phpEx?mode=add_event&amp;id=$event_id"))
				);
			}
		}
	}
}
else
{
	$template->set_filenames(array(
		"body" => "admin/test_event_list_body.tpl")
	);

	$exe_query = 'perl '.$tester.' -type event_menu';
	$result = exec($exe_query, $output);
	$field_name = explode("::", $output[0]);
	for($i = 1;$i < count($output);  $i++)
	{
		$fields = explode("::", $output[$i]);
		$row['event_id'] = $fields[0];
		$row['magnitude'] = $fields[1];
		$row['lat'] = $fields[2];
		$row['lon'] = $fields[3];
		$row['desc'] = $fields[4];

		if ( $row['event_id'] != '' && $row['magnitude'] != 0 ) {
			$topic_rowset[] = $row;
		}
	}

	$topic_rowset = arfsort( $topic_rowset, array($default_sort));
	$event_count= count($topic_rowset);

	$current_page = ( !$event_count ) ? 1 : ceil( $event_count / $board_config['topics_per_page'] );
	$template->assign_vars(array(
		'PAGINATION' => generate_pagination("admin_test_event.$phpEx?" . POST_GROUPS_URL . "=$group_id&sort_key=$sort_key&sort_order=$sort_order", $event_count, $board_config['topics_per_page'], $start),
		'PAGE_NUMBER' => sprintf($lang['Page_of'], ( floor( $start / $board_config['topics_per_page'] ) + 1 ), $current_page ), 

		'L_GOTO_PAGE' => $lang['Goto_page'])
	);
	$new_sort_order = ($sort_order == 'd') ? 'a' : 'd';
	$img_url = ' <img src="/images/' . $sort_order . '.png" border="0" width="10" height="10">';
	$base_url = append_sid("admin_test_event.$phpEx?" . POST_GROUPS_URL . "=$group_id");

	$template->assign_vars(array(
		"L_WORDS_TITLE" => $lang['test_title'],
		"L_WORDS_TEXT" => $lang['test_explain'],
		"L_EVENT_ID" => $lang[$field_name[0]] . (($sort_key == 'event_id') ? $img_url : ''),
		"L_MAGNITUDE" => $lang[$field_name[1]] . (($sort_key == 'magnitude') ? $img_url : ''),
		"L_LATITUDE" => $lang[$field_name[2]] . (($sort_key == 'lat') ? $img_url : ''),
		"L_LONGITUDE" => $lang[$field_name[3]] . (($sort_key == 'lon') ? $img_url : ''),
		"L_DESCRIPTION" => $lang[$field_name[4]] . (($sort_key == 'desc') ? $img_url : ''),
		"L_TEST_VERSION1" => $lang['test_version1'],
		"L_TEST_VERSIONN" => $lang['test_versionn'],
		"L_DELETE" => $lang["Delete"],
		"L_ADD_EVENT" => $lang['Add_new_event'],
		"L_ACTION" => $lang['Action'],

		'U_EVENT_ID' =>  $base_url."&sort_key=event_id&sort_order=" . (($sort_key == 'event_id') ? $new_sort_order : $sort_order),
		'U_MAGNITUDE' =>  $base_url."&sort_key=magnitude&sort_order=" . (($sort_key == 'magnitude') ? $new_sort_order : $sort_order),
		'U_LATITUDE' =>  $base_url."&sort_key=lat&sort_order=" . (($sort_key == 'lat') ? $new_sort_order : $sort_order),
		'U_LONGITUDE' =>  $base_url."&sort_key=lon&sort_order=" . (($sort_key == 'lon') ? $new_sort_order : $sort_order),
		'U_DESCRIPTION' =>  $base_url."&sort_key=desc&sort_order=" . (($sort_key == 'desc') ? $new_sort_order : $sort_order),

		"S_WORDS_ACTION" => append_sid("admin_test_event.$phpEx"),
		"S_HIDDEN_FIELDS" => '')
	);


	for($i = $start;$i < $event_count && $i < ($board_config['topics_per_page'] + $start);  $i++)
	{
		$event_id = $topic_rowset[$i]['event_id'];
		$magnitude = $topic_rowset[$i]['magnitude'];
		$lat = $topic_rowset[$i]['lat'];
		$lon = $topic_rowset[$i]['lon'];
		$desc = $topic_rowset[$i]['desc'];
		
		$row_color = ( !($i % 2) ) ? $theme['td_color1'] : $theme['td_color2'];
		$row_class = ( !($i % 2) ) ? $theme['td_class1'] : $theme['td_class2'];

		$template->assign_block_vars("words", array(
			"ROW_COLOR" => "#" . $row_color,
			"ROW_CLASS" => $row_class,
			"EVENT_ID" => $event_id,
			"MAGNITUDE" => $magnitude,
			"LATITUDE" => $lat,
			"LONGITUDE" => $lon,
			"DESCRIPTION" => $desc,

			"U_DELETE" => append_sid("admin_test_event.$phpEx?mode=delete&amp;id=$event_id"),
			"U_TEST_VERSION1" => append_sid("admin_test_event.$phpEx?mode=inject_first&amp;id=$event_id"),
			"U_TEST_VERSIONN" => append_sid("admin_test_event.$phpEx?mode=inject_next&amp;id=$event_id"))
		);
	}
}

$template->pparse("body");

include('./page_footer_admin.'.$phpEx);

?>