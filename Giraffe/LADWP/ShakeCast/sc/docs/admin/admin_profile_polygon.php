<?php
/*
# $Id: admin_profile_polygon.php 171 2007-10-31 18:41:04Z klin $

# The PHP interface was originally inspired by PHPBB,
# file/database formats and directory structure are quite similar.

##############################################################################
# 
# Terms and Conditions of Software Use
# ====================================
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Disclaimer of Earthquake Information
# ====================================
# 
# The data and maps provided through this system are preliminary data
# and are subject to revision. They are computer generated and may not
# have received human review or official approval. Inaccuracies in the
# data may be present because of instrument or computer
# malfunctions. Subsequent review may result in significant revisions to
# the data. All efforts have been made to provide accurate information,
# but reliance on, or interpretation of earthquake data from a single
# source is not advised. Data users are cautioned to consider carefully
# the provisional nature of the information before using it for
# decisions that concern personal or public safety or the conduct of
# business that involves substantial monetary or operational
# consequences.
# 
# Disclaimer of Software and its Capabilities
# ===========================================
# 
# This software is provided as an "as is" basis.  Attempts have been
# made to rid the program of software defects and bugs, however the
# U.S. Geological Survey (USGS) have no obligations to provide maintenance, 
# support, updates, enhancements or modifications. In no event shall USGS 
# be liable to any party for direct, indirect, special, incidental or 
# consequential damages, including lost profits, arising out of the use 
# of this software, its documentation, or data obtained though the use 
# of this software, even if USGS or have been advised of the
# possibility of such damage. By downloading, installing or using this
# program, the user acknowledges and understands the purpose and
# limitations of this software.
# 
# Contact Information
# ===================
# 
# Coordination of this effort is under the auspices of the USGS Advanced
# National Seismic System (ANSS) coordinated in Golden, Colorado, which
# functions as the clearing house for development, distribution,
# documentation, and support. For questions, comments, or reports of
# potential bugs regarding this software please contact wald@usgs.gov or
# klin@usgs.gov.  
#
#############################################################################
*/

if( !empty($setmodules) )
{
	$file = basename(__FILE__);
	$module['Profile_Admin']['Management'] = $file;
	return;
}

define('IN_SC', 1);

//
// Load default header
//
$sc_root_path = "./../";
require($sc_root_path . 'extension.inc');
$manage_profile = '/shakecast/sc/bin/manage_profile.pl';

$cancel = ( isset($HTTP_POST_VARS['cancel']) ) ? true : false;
$no_page_header = $cancel;

$template_file = 'page_gm_header.tpl';
require('./pagestart.' . $phpEx);

if ($cancel)
{
	redirect('admin/' . append_sid("admin_profile_polygon.$phpEx", true));
}

if( isset($HTTP_GET_VARS['mode']) || isset($HTTP_POST_VARS['mode']) )
{
	$mode = (isset($HTTP_GET_VARS['mode'])) ? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode'];
	$mode = htmlspecialchars($mode);
}
else 
{
	//
	// These could be entered via a form button
	//
	if( isset($HTTP_POST_VARS['add_event']) )
	{
		$mode = "add_event";
	}
	else if( isset($HTTP_POST_VARS['save']) )
	{
		$mode = "save";
	}
	else
	{
		$mode = "";
	}
}

// Restrict mode input to valid options
$mode = ( in_array($mode, array('add', 'edit', 'save', 'delete')) ) ? $mode : '';
@set_time_limit(1200);

if( $mode != "" )
{
	if( $mode == "edit" || $mode == "add" )
	{
		$profile_id = ( isset($HTTP_GET_VARS['id']) ) ? intval($HTTP_GET_VARS['id']) : '';

		$template->set_filenames(array(
			"body" => "admin/profile_edit_body.tpl")
		);


		$profile = array('profile_id' => '', 'profile_name' => '', 'description' => '', 'geom' => '');
		$s_hidden_fields = '';

		$gmap = '<div id="map" style="width: 500px; height: 500px"></div>';

		if( $mode == "edit" )
		{
			if( $profile_id )
			{
				$sql = "SELECT profile_name, description, geom
						FROM geometry_profile
						WHERE profile_id = $profile_id";
				$result = $db->sql_query($sql);
				if( !$result )
				{
					message_die(GENERAL_ERROR, "Couldn't get profile information.", "", __LINE__, __FILE__, $sql );
				}
				$profile_settings = $db->sql_fetchrow($result);
				$hidden_fields = '<input type="hidden" name="profile_id" value="'.$profile_id.'" />';
				$profile_name = $profile_settings['profile_name'];
				$description = $profile_settings['description'];
				$geom = $profile_settings['geom'];
			}
			else
			{
				message_die(GENERAL_MESSAGE, "No profile selected for editing ");
			}
		}
		
		$hidden_fields .= '<input type="hidden" name="geom" id="geom" value="'.$geom.'" />';
		// Grab the current list of polygon types
		$poly_type = array('poly' => 'Polygon','rect' => 'Rectangle', 'circle' => 'Circular');
		$poly_type_select = '<select name="poly_type" id="poly" onchange="changePoly(this.id)">';
		foreach( $poly_type as $key => $value )
		{
			if ($key == $profile_settings['poly_type']) {
				$poly_type_select .= '<option value="' . $key . '" selected>' . $value . '</option>';
			} else {
				$poly_type_select .= '<option value="' . $key . '">' . $value . '</option>';
			}
		}
		$poly_type_select .= '</select>';

		$template->assign_vars(array(
			"L_WORDS_TITLE" => $lang['profile_polygon_title'],
			"L_WORDS_TEXT" => $lang['profile_polygon_explain'],
			'GMAP' =>	$gmap,
			'METRIC' => "<script type=\"text/javascript\">var geom =\"".$geom."\";</script>",
//			'SHOW_SELECT' => '<input type="button" widgetId="showSelectButton" id="showSelectButton" value="Commit Profile Changes">',
			'SHOW_SELECT' => '<button dojoType="Button" id="showSelectButton" >Confirm Polygon Changes</button>',
			'SHOW_SELECT' => '<span>Polygon Anchor Points</span>',
//			<input type="button" id="userSelect" value="Update notification profiles">
			"POLY_TYPE" => $poly_type_select,
			"PROFILE_NAME" => $profile_name,
			"DESCRIPTION" => $description,
			"F_HIDDEN_FIELDS" => $hidden_fields,
			"PROFILE_NAME" => '<input type="text" name="profile_name" id="profile_name" value="'.$profile_name.'" />',
			"DESCRIPTION" => '<input type="text" name="description" id="description" value="'.$description.'" />',

			"L_PROFILE_ID" => 'ID '.$profile_id,
			"L_POLY_TYPE" => "Shape Type",
			"L_PROFILE_NAME" => "Profile Name",
			"L_DESCRIPTION" => "Description",
			"L_PROFILE_POLYGON_EDIT" => "Profile Polygon",

			"L_SUBMIT" => $lang['Submit'],

			"S_FACILITY_TYPE_ACTION" => append_sid("admin_profile_polygon.$phpEx"),
			"S_HIDDEN_FIELDS" => $s_hidden_fields)
		);

		$template->pparse("body");

		include('./page_footer_admin.'.$phpEx);
	}
	else if( $mode == "save" )
	{
		$profile_id = ( isset($HTTP_POST_VARS['profile_id']) ) ? intval($HTTP_POST_VARS['profile_id']) : 0;
		if( $profile_id )
		{
			$sql = "UPDATE " . PROFILE_TABLE . " 
				SET profile_name = '".str_replace("\'", "''", $HTTP_POST_VARS['profile_name'])."', 
					description = '".str_replace("\'", "''", $HTTP_POST_VARS['description'])."', 
					geom = '" . str_replace("\'", "''", $HTTP_POST_VARS['geom']) . "',
					updated = NULL
				WHERE profile_id = $profile_id";
			$message = "Profile polygon information has been updated";
			if(!$result = $db->sql_query($sql))
			{
				message_die(GENERAL_ERROR, "Could not insert into geometry profile table", $lang['Error'], __LINE__, __FILE__, $sql);
			}
		}
		else
		{
			$sql = "INSERT INTO " . PROFILE_TABLE . " 
				(profile_name, description, geom, updated) VALUES ('".
					str_replace("\'", "''", $HTTP_POST_VARS['profile_name'])."', '".
					str_replace("\'", "''", $HTTP_POST_VARS['description'])."', '". 
					str_replace("\'", "''", $HTTP_POST_VARS['geom'])."', NULL)"; 
			$message = "Profile polygon information has been added";
			if(!$result = $db->sql_query($sql))
			{
				message_die(GENERAL_ERROR, "Could not insert into geometry profile table", $lang['Error'], __LINE__, __FILE__, $sql);
			}
			$profile_id = $db->sql_nextid();
		}

		$exe_query = "perl $manage_profile -poly ".$HTTP_POST_VARS['geom'];
		$result = exec($exe_query, $output);
		$sql = "DELETE FROM geometry_facility_profile  
				WHERE profile_id = " .$profile_id;
		if(!$result = $db->sql_query($sql))
		{
			message_die(GENERAL_ERROR, "Could not update geometry profile table", $lang['Error'], __LINE__, __FILE__, $sql);
		}
		for($i = 1;$i < count($output);  $i++)
		{
			$fields = explode("::", $output[$i]);
			$sql = "SELECT facility_id 
					FROM geometry_facility_profile
					WHERE profile_id = $profile_id AND facility_id = ".$fields[0];
			if(!$result = $db->sql_query($sql))
			{
				message_die(GENERAL_ERROR, "Could not insert into geometry profile table", $lang['Error'], __LINE__, __FILE__, $sql);
			}
			if(!$db->sql_fetchrow($result)) 
			{
				$sql = "INSERT INTO geometry_facility_profile (profile_id, facility_id) 
						VALUES ($profile_id, ".$fields[0].")";
				if(!$result = $db->sql_query($sql))
				{
					message_die(GENERAL_ERROR, "Could not insert into geometry profile table", $lang['Error'], __LINE__, __FILE__, $sql);
				}
			}
		}

		$message .= "<br /><br />" . sprintf($lang['Click_return_profileadmin'], "<a href=\"" . append_sid("admin_profile_polygon.$phpEx") . "\">", "</a>") . "<br /><br />" . sprintf($lang['Click_return_admin_index'], "<a href=\"" . append_sid("index.$phpEx?pane=right") . "\">", "</a>");

		message_die(GENERAL_MESSAGE, $message);
	}
	else if( $mode == "delete" )
	{
		if( isset($HTTP_POST_VARS['id']) ||  isset($HTTP_GET_VARS['id']) )
		{
			$id = ( isset($HTTP_POST_VARS['id']) ) ? $HTTP_POST_VARS['id'] : $HTTP_GET_VARS['id'];
		}
		else
		{
			$id = 0;
		}

		$confirm = isset($HTTP_POST_VARS['confirm']);

		if( $id && $confirm )
		{
			$sql = "DELETE FROM " . PROFILE_TABLE . " 
				WHERE profile_id = '$id'";

			if(!$result = $db->sql_query($sql))
			{
				message_die(GENERAL_ERROR, "Could not delete profile polygon", $lang['Error']);
			}

			$message = $lang['profile_polygon_deleted'] . "<br /><br />" . sprintf($lang['Click_return_profileadmin'], "<a href=\"" . append_sid("admin_profile_polygon.$phpEx") . "\">", "</a>") . "<br /><br />" . sprintf($lang['Click_return_admin_index'], "<a href=\"" . append_sid("index.$phpEx?pane=right") . "\">", "</a>");

			message_die(GENERAL_MESSAGE, $message);
		}
		elseif( $id && !$confirm)
		{
			// Present the confirmation screen to the user
			$template->set_filenames(array(
				'body' => 'admin/confirm_body.tpl')
			);

			$hidden_fields = '<input type="hidden" name="mode" value="'.$mode.'" /><input type="hidden" name="id" value="' . $id . '" />';

			$template->assign_vars(array(
				'MESSAGE_TITLE' => $lang['Confirm'],
				'MESSAGE_TEXT' => $lang['Confirm_profile_polygon_deleted'],

				'L_YES' => $lang['Yes'],
				'L_NO' => $lang['No'],

				'S_CONFIRM_ACTION' => append_sid("admin_profile_polygon.$phpEx"),
				'S_HIDDEN_FIELDS' => $hidden_fields)
			);
		}
		else
		{
			message_die(GENERAL_MESSAGE, $lang['no_profile_polygon_selected']);
		}
	}
}
else
{
	$template->set_filenames(array(
		"body" => "admin/profile_list_body.tpl")
	);

	if ( !$profile_count )
	{
		//
		// No group members
		//
		$template->assign_block_vars('switch_no_members', array());
		$template->assign_vars(array(
			'L_NO_MEMBERS' => $lang['No_group_members'])
		);
	}

	$sql = "SELECT profile_id, profile_name, description, geom
			FROM geometry_profile";
	if(!$result = $db->sql_query($sql))
	{
		message_die(GENERAL_ERROR, "Could not find damage level settings", $lang['Error']);
	}
	$profiles = $db->sql_fetchrowset($result);

	$template->assign_vars(array(
		"L_WORDS_TITLE" => $lang['profile_title'],
		"L_WORDS_TEXT" => $lang['profile_explain'],
		"L_PROFILE_ID" => "ID",
		"L_PROFILE_NAME" => "Profile Name",
		"L_DESCRIPTION" => "Description",
			"GEOM" => json_encode($profiles),

		"L_EDIT" => "Edit",
		"L_DELETE" => "Delete",
		"L_ADD_EVENT" => "Add new profile",
		"L_ACTION" => $lang['Action'],

		"S_WORDS_ACTION" => append_sid("admin_profile_polygon.$phpEx?mode=add"),
		"S_HIDDEN_FIELDS" => '')
	);

	for($i = 0;$i < count($profiles);  $i++)
	{
		$profile_id = $profiles[$i]['profile_id'];
		$profile_name = $profiles[$i]['profile_name'];
		$geom = $profiles[$i]['geom'];
		$description = $profiles[$i]['description'];

		$row_color = ( !($i % 2) ) ? $theme['td_color1'] : $theme['td_color2'];
		$row_class = ( !($i % 2) ) ? $theme['td_class1'] : $theme['td_class2'];

		$template->assign_block_vars('words', array(
			"ROW_COLOR" => "#" . $row_color,
			"ROW_CLASS" => $row_class,
			"PROFILE_ID" => $profile_id,
			"PROFILE_NAME" => $profile_name,
			"DESCRIPTION" => $description,

			"U_EDIT" => append_sid("admin_profile_polygon.$phpEx?mode=edit&amp;id=$profile_id"),
			"U_DELETE" => append_sid("admin_profile_polygon.$phpEx?mode=delete&amp;id=$profile_id"))
		);
	}
}

$template->pparse("body");

include('./page_footer_admin.'.$phpEx);

?>