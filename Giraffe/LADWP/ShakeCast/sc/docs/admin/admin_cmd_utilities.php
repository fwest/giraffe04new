<?php
/*
# $Id: admin_cmd_utilities.php 358 2008-02-01 20:21:42Z klin $

# The PHP interface was originally inspired by PHPBB,
# file/database formats and directory structure are quite similar.

##############################################################################
# 
# Terms and Conditions of Software Use
# ====================================
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Disclaimer of Earthquake Information
# ====================================
# 
# The data and maps provided through this system are preliminary data
# and are subject to revision. They are computer generated and may not
# have received human review or official approval. Inaccuracies in the
# data may be present because of instrument or computer
# malfunctions. Subsequent review may result in significant revisions to
# the data. All efforts have been made to provide accurate information,
# but reliance on, or interpretation of earthquake data from a single
# source is not advised. Data users are cautioned to consider carefully
# the provisional nature of the information before using it for
# decisions that concern personal or public safety or the conduct of
# business that involves substantial monetary or operational
# consequences.
# 
# Disclaimer of Software and its Capabilities
# ===========================================
# 
# This software is provided as an "as is" basis.  Attempts have been
# made to rid the program of software defects and bugs, however the
# U.S. Geological Survey (USGS) have no obligations to provide maintenance, 
# support, updates, enhancements or modifications. In no event shall USGS 
# be liable to any party for direct, indirect, special, incidental or 
# consequential damages, including lost profits, arising out of the use 
# of this software, its documentation, or data obtained though the use 
# of this software, even if USGS or have been advised of the
# possibility of such damage. By downloading, installing or using this
# program, the user acknowledges and understands the purpose and
# limitations of this software.
# 
# Contact Information
# ===================
# 
# Coordination of this effort is under the auspices of the USGS Advanced
# National Seismic System (ANSS) coordinated in Golden, Colorado, which
# functions as the clearing house for development, distribution,
# documentation, and support. For questions, comments, or reports of
# potential bugs regarding this software please contact wald@usgs.gov or
# klin@usgs.gov.  
#
#############################################################################
*/

define('IN_SC', 1);

if( !empty($setmodules) )
{
	$filename = basename(__FILE__);

	$file_uploads = (@phpversion() >= '4.0.0') ? @ini_get('file_uploads') : @get_cfg_var('file_uploads');

	if( (empty($file_uploads) || $file_uploads != 0) && (strtolower($file_uploads) != 'off') && (@phpversion() != '4.0.4pl1') )
	{
		$module['Facility_Admin']['Upload'] = $filename . "?perform=facility";
		$module['Profile_Admin']['Upload'] = $filename . "?perform=profile";
		$module['Users']['Upload'] = $filename . "?perform=user";
	}

	return;
}

//
// Load default header
//
$no_page_header = TRUE;
$sc_root_path = "./../";
require($sc_root_path . 'extension.inc');
require('./pagestart.' . $phpEx);
include($sc_root_path . 'includes/functions_selects.'.$phpEx);

//
// Set VERBOSE to 1  for debugging info..
//
define("VERBOSE", 0);

//
// Increase maximum execution time, but don't complain about it if it isn't
// allowed.
//
@set_time_limit(1200);


//
// Begin program proper
//
if( isset($HTTP_GET_VARS['perform']) || isset($HTTP_POST_VARS['perform']) )
{
	$perform = (isset($HTTP_POST_VARS['perform'])) ? $HTTP_POST_VARS['perform'] : $HTTP_GET_VARS['perform'];
	// Restrict mode input to valid options
	$perform = ( in_array($perform, array('user', 'facility', 'profile')) ) ? $perform : '';
	if(!isset($HTTP_POST_VARS['restore_start']))
	{
		$s_hidden_fields = "<input type=\"hidden\" name=\"perform\" value=\"$perform\" />";
	}
	else
	{
		$backup_file_name = (!empty($HTTP_POST_FILES['backup_file']['name'])) ? $HTTP_POST_FILES['backup_file']['name'] : "";
		$backup_file_tmpname = ($HTTP_POST_FILES['backup_file']['tmp_name'] != "none") ? $HTTP_POST_FILES['backup_file']['tmp_name'] : "";
		$backup_file_type = (!empty($HTTP_POST_FILES['backup_file']['type'])) ? $HTTP_POST_FILES['backup_file']['type'] : "";

		if($backup_file_tmpname == "" || $backup_file_name == "")
		{
			message_die(GENERAL_MESSAGE, $lang['Restore_Error_no_file']);
		}
		$file_path = phpbb_realpath($backup_file_tmpname);
		$save_path = '/shakecast/sc/data/'.$backup_file_name;
		$data = fread(fopen($file_path, 'r'), filesize($file_path));
		$result = fwrite(fopen($save_path, 'w'), $data);
	}

	if( isset($HTTP_POST_VARS['cmdoption']) ||  isset($HTTP_GET_VARS['cmdoption']) )
	{
		$cmdoption = ( isset($HTTP_POST_VARS['cmdoption']) ) ? $HTTP_POST_VARS['cmdoption'] : $HTTP_GET_VARS['cmdoption'];
		$cmdoption = ($cmdoption == 'default') ? '' : ' -'.$cmdoption;
	}
	else
	{
		$cmdoption = '';
	}

	switch($perform)
	{
		case 'user':
			$manage_util = '/shakecast/sc/bin/manage_user.pl -verbose';
			$click_return = 'Click_return_useradmin';
			$click_url = 'admin_users';
			$cmd_options = cmd_option_select('default', 'cmduser');
			break;
		case 'profile':
			$manage_util = '/shakecast/sc/bin/manage_profile.pl -verbose';
			$cmdoption .= ' -conf';
			$click_return = 'Click_return_profileadmin';
			$click_url = 'admin_profile_polygon';
			$cmd_options = cmd_option_select('default', 'cmdprofile');
			break;
		case 'facility':
			$manage_util = '/shakecast/sc/bin/manage_facility.pl -verbose';
			$click_return = 'Click_return_facadmin';
			$click_url = 'admin_facility';
			$cmd_options = cmd_option_select('default', 'cmdfacility');
			break;
		default :
			message_die(GENERAL_MESSAGE, "Invalid upload option");
	}
}

if(!isset($HTTP_POST_VARS['restore_start']))
{
	//
	// Define Template files...
	//
	include('./page_header_admin.'.$phpEx);

	$template->set_filenames(array(
		"body" => "admin/db_utils_restore_body.tpl")
	);

	$template->assign_vars(array(
		"L_DATABASE_RESTORE" => $lang['Database_Utilities_'.$perform] . " : " . $lang['Upload'],
		"L_RESTORE_EXPLAIN" => $lang['Upload_explain'],
		"L_SELECT_FILE" => $lang['Select_file'],
		"L_START_RESTORE" => $lang['Start_Upload'],

		"CMD_OPTIONS" => $cmd_options,
		"S_DBUTILS_ACTION" => append_sid("admin_cmd_utilities.$phpEx"),
		"S_HIDDEN_FIELDS" => $s_hidden_fields)
	);
	$template->pparse("body");

}
else
{
	//
	// If I file was actually uploaded, check to make sure that we
	// are actually passed the name of an uploaded file, and not
	// a hackers attempt at getting us to process a local system
	// file.
	//
	include('./page_header_admin.'.$phpEx);

	if( file_exists($save_path) )
	{
		$result = @exec('perl '.$manage_util . $cmdoption . ' ' . $save_path, $output);
		unlink($save_path);
		
		if (preg_match("/manage_facility/", $manage_util))
		{
			$png_path = $sc_root_path . '/tiles';
			$dir = @opendir($png_path);
			
			while( $file = @readdir($dir) )
			{
				if( preg_match("/^.*?\.png$/", $file) )
				{
					if( file_exists(phpbb_realpath($png_path . '/' . $file)) )
					{
						unlink(phpbb_realpath($png_path . '/' . $file));
					}
				}
			}
			
			@closedir($dir);
		}
			
	}
	else
	{
		message_die(GENERAL_ERROR, $lang['Restore_Error_uploading']);
	}

	include('./page_header_admin.'.$phpEx);

	$template->set_filenames(array(
		"body" => "admin/admin_message_body.tpl")
	);

	$message = "<br /><br />" . $lang['Upload_success'] . "<br /><br />" . sprintf($lang[$click_return], "<a href=\"" . append_sid("$click_url.$phpEx") . "\">", "</a>") . "<br /><br />" . sprintf($lang['Click_return_admin_index'], "<a href=\"" . append_sid("index.$phpEx?pane=right") . "\">", "</a>");

	$template->assign_vars(array(
		"MESSAGE_TITLE" => $lang['Database_Utilities_'.$perform] . " : " . $lang['Upload'],
		"MESSAGE_TEXT" => implode("<br>",$output) . $message . $cmdoption)
	);

	$template->pparse("body");
}

include('./page_footer_admin.'.$phpEx);

?>
