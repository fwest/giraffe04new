<?php
/*
# $Id: admin_profile_notification.php 458 2008-08-25 14:44:46Z klin $

# The PHP interface was originally inspired by PHPBB,
# file/database formats and directory structure are quite similar.

##############################################################################
# 
# Terms and Conditions of Software Use
# ====================================
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Disclaimer of Earthquake Information
# ====================================
# 
# The data and maps provided through this system are preliminary data
# and are subject to revision. They are computer generated and may not
# have received human review or official approval. Inaccuracies in the
# data may be present because of instrument or computer
# malfunctions. Subsequent review may result in significant revisions to
# the data. All efforts have been made to provide accurate information,
# but reliance on, or interpretation of earthquake data from a single
# source is not advised. Data users are cautioned to consider carefully
# the provisional nature of the information before using it for
# decisions that concern personal or public safety or the conduct of
# business that involves substantial monetary or operational
# consequences.
# 
# Disclaimer of Software and its Capabilities
# ===========================================
# 
# This software is provided as an "as is" basis.  Attempts have been
# made to rid the program of software defects and bugs, however the
# U.S. Geological Survey (USGS) have no obligations to provide maintenance, 
# support, updates, enhancements or modifications. In no event shall USGS 
# be liable to any party for direct, indirect, special, incidental or 
# consequential damages, including lost profits, arising out of the use 
# of this software, its documentation, or data obtained though the use 
# of this software, even if USGS or have been advised of the
# possibility of such damage. By downloading, installing or using this
# program, the user acknowledges and understands the purpose and
# limitations of this software.
# 
# Contact Information
# ===================
# 
# Coordination of this effort is under the auspices of the USGS Advanced
# National Seismic System (ANSS) coordinated in Golden, Colorado, which
# functions as the clearing house for development, distribution,
# documentation, and support. For questions, comments, or reports of
# potential bugs regarding this software please contact wald@usgs.gov or
# klin@usgs.gov.  
#
#############################################################################
*/

if( !empty($setmodules) )
{
	$file = basename(__FILE__);
	$module['Profile_Admin']['Notification_Request'] = $file;
	return;
}

define('IN_SC', 1);

//
// Load default header
//
$manage_profile = '/shakecast/sc/bin/manage_profile.pl';
$sc_root_path = "./../";
require($sc_root_path . 'extension.inc');

$cancel = ( isset($HTTP_POST_VARS['cancel']) ) ? true : false;
$no_page_header = $cancel;
//$template_file = 'profile_notification_header.tpl';

require('./pagestart.' . $phpEx);

if ($cancel)
{
	redirect('admin/' . append_sid("admin_profile_notification.$phpEx", true));
}

if( isset($HTTP_GET_VARS['mode']) || isset($HTTP_POST_VARS['mode']) )
{
	$mode = (isset($HTTP_GET_VARS['mode'])) ? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode'];
	$mode = htmlspecialchars($mode);
}
else 
{
	//
	// These could be entered via a form button
	//
	if( isset($HTTP_POST_VARS['add_event']) )
	{
		$mode = "add";
	}
	else if( isset($HTTP_POST_VARS['add']) )
	{
		$mode = "add";
	}
	else if( isset($HTTP_POST_VARS['edit']) )
	{
		$mode = "edit";
	}
	else if( isset($HTTP_POST_VARS['save']) )
	{
		$mode = "save";
	}
	else
	{
		$mode = "";
	}
}

if( isset( $HTTP_POST_VARS['id'] ) || isset( $HTTP_GET_VARS['id'] ) )
{
	$profile_id = ( isset( $HTTP_POST_VARS['id']) ) ? $HTTP_POST_VARS['id'] : $HTTP_GET_VARS['id'];
	$profile_id = intval($profile_id);
	
	$sql = "SELECT profile_name, description, geom
			FROM " . GEOMETRY_PROFILE_TABLE ." 
			WHERE profile_id = " . $profile_id;
	if(!$result = $db->sql_query($sql))
	{
		message_die(GENERAL_ERROR, "Could not find user notification settings", $lang['Error'], __LINE__, __FILE__, $sql);
	}
	$row = $db->sql_fetchrow($result);
	$profile_name = $row['profile_name'];
	$profile_description = $row['description'];
	$profile_geom = $row['geom'];
}
else
{
	$profile_id = '';
}

if( isset( $HTTP_POST_VARS['nrtype'] ) || isset( $HTTP_GET_VARS['nrtype'] ) )
{
	$nrtype = ( isset( $HTTP_POST_VARS['nrtype']) ) ? $HTTP_POST_VARS['nrtype'] : $HTTP_GET_VARS['nrtype'];
	$nrtype = htmlspecialchars($nrtype);
}
else
{
	$nrtype = '';
}

if( isset( $HTTP_POST_VARS['nkey'] ) || isset( $HTTP_GET_VARS['nkey'] ) )
{
	$nkey = ( isset( $HTTP_POST_VARS['nkey']) ) ? $HTTP_POST_VARS['nkey'] : $HTTP_GET_VARS['nkey'];
}
else
{
	$nkey = '';
}

// Restrict mode input to valid options
$mode = ( in_array($mode, array('add', 'edit', 'save', 'delete', 'update', 'add_update', 'add_profile')) ) ? $mode : '';

if( $mode != "" )
{
	if( $mode == "edit" || $mode == "add" )
	{
		$dl_types = get_damage_level();
		$et_types = get_event_type();
		$m_types = get_metric_type();
		$pt_types = get_product_type();
		$mf_types = get_message_format_type();
		$dm_types = get_delivery_method_type();
		$nt_types = get_notification_type();

		$s_hidden_fields = '<input type="hidden" name="mode" value="save" /><input type="hidden" name="agreed" value="true" />';
		$s_hidden_fields .= '<input type="hidden" name="source_mode" value="' . $mode . '" /><input type="hidden" name="id" value="' . $profile_id . '" /><input type="hidden" name="nkey" value="' . $nkey . '" />';
		if( $nkey != '' || $nrtype != '' )
		{
			$template->set_filenames(array(
				'body' => 'admin/user_notification_edit_body.tpl')
			);
			if( $nkey != '')
			{
				$sql = "SELECT pn.notification_request_id as id, pn.notification_type as type, pn.event_type, pn.delivery_method as delivery, pn.message_format as template, pn.limit_value, pn.damage_level,
							pn.product_type as product, pn.metric, pn.disabled as disable, pn.aggregate, pn.aggregation_group, gp.profile_name
						FROM " . PROFILE_NOTIFICATION_TABLE ." pn INNER JOIN ".GEOMETRY_PROFILE_TABLE." gp ON pn.profile_id = gp.profile_id
						WHERE pn.profile_id = " . $profile_id  ."
							AND pn.notification_request_id = ". $nkey;
				if(!$result = $db->sql_query($sql))
				{
					message_die(GENERAL_ERROR, "Could not find user notification settings", $lang['Error'], __LINE__, __FILE__, $sql);
				}
				$nrt = $db->sql_fetchrow($result);
				$db->sql_freeresult($result);
			}
			
			if ($nrtype == 'SYSTEM')
			{
				$et_select = 'N/A';
				$s_hidden_fields .= '<input type="hidden" name="TXT_EVENT_TYPE" value="" />';
			}
			else
			{
				$et_select = '<select name="TXT_EVENT_TYPE"><option value="">-- Select an event type --</option>';
				foreach ($et_types as $key => $value) {
					if ($key == $nrt['event_type']) {
						$et_select .= '<option value="'. $key . '" selected>'. $value .'</option>';
					} else {
						$et_select .= '<option value="'. $key . '">'. $value .'</option>';
					}
				}
				$et_select .= '</select>';
			}
			$dm_select = '<select name="TXT_DELIVERY_METHOD"><option value="">-- Select a delivery method --</option>';
			foreach ($dm_types as $key => $value) {
				if ($key == $nrt['delivery']) {
					$dm_select .= '<option value="'. $key . '" selected>'. $value .'</option>';
				} else {
					$dm_select .= '<option value="'. $key . '">'. $value .'</option>';
				}
			}
			$dm_select .= '</select>';
			$mf_select = '<select name="TXT_MESSAGE_FORMAT"><option value=""></option>';
			foreach ($mf_types as $key => $value) {
				if ($key == $nrt['template']) {
					$mf_select .= '<option value="'. $key . '" selected>'. $value .'</option>';
				} else {
					$mf_select .= '<option value="'. $key . '">'. $value .'</option>';
				}
			}
			$mf_select .= '</select>';
			$template->assign_vars(array(
				'L_USERNAME' => $lang['Username'],
				"L_USER_TITLE" => $lang['profile_notification_title'],
				"L_USER_EXPLAIN" => $lang['profile_notification_explain'],
				'L_REGISTRATION_INFO' => $lang['profile_notification_request_explain'],
				'PROFILE_NAME' => ($profile_id != '') ? "$profile_name  (ID: $profile_id)" : '',

				'L_SUBMIT' => $lang['Submit'],
				'L_RESET' => $lang['Reset'],
				'L_FULLNAME' => $lang['Fullname'],
				'L_YES' => $lang['Yes'],
				'L_NO' => $lang['No'],
				
				'L_ID' => $lang['id'],
				'L_TYPE' => $lang['type'],
				'L_EVENT_TYPE' => $lang['event_type'],
				'L_DELIVERY' => $lang['delivery'],
				'L_TEMPLATE' => $lang['template'],
				'L_LIMIT_VALUE' => $lang['limit_value'],
				'L_DAMAGE_LEVEL' => $lang['damage_level'],
				'L_PRODUCT' => $lang['product'],
				'L_METRIC' => $lang['metric'],
				'L_DISABLE' => $lang['disable'],
				'L_AGGREGATE' => $lang['aggregate'],
				'L_AGGREGATION_GROUP' => $lang['aggregation_group'],
				'L_FACILITY' => $lang['facility'],
		
				"NID" => $nrt['id'],
				"TYPE" => $nrtype,
				"TYPE_TXT" => $nt_types[$nrtype],
				"EVENT_TYPE" => $et_select,
				"DELIVERY" => $dm_select,
				"TEMPLATE" => $mf_select,
				"DAMAGE_LEVEL" => $dl_types[$nrt['damage_level']],
				"PRODUCT" => $pt_types[$nrt['product']],
				"DISABLE" => ($nrt['disable']) ? 'checked' : '',
				"AGGREGATE" => ($nrt['aggregate']) ? 'checked' : '',
				"AGGREGATION_GROUP" => $nrt['aggregation_group'],

				"L_EDIT" => "Edit",
				"L_DELETE" => "Delete",
				'S_HIDDEN_FIELDS' => $s_hidden_fields,
				'S_PROFILE_ACTION' => append_sid("admin_profile_notification.$phpEx"))
			);
			if ($nrtype == 'UPD_EVENT' || $nrtype == 'NEW_EVENT' || $nrtype == 'CAN_EVENT' || $nrtype == 'SHAKING' || $nrtype == 'NEW_PROD') 
			{
				$template->assign_block_vars('limit_value', array(
				"LIMIT_VALUE" => $nrt['limit_value'])
				);
			}
			if ($nrtype == 'NEW_PROD') 
			{
				$pt_select = '<select name="TXT_PRODUCT_TYPE"><option value=""></option>';
				foreach ($pt_types as $key => $value) {
					if ($key == $nrt['product']) {
						$pt_select .= '<option value="'. $key . '" selected>'. $value .'</option>';
					} else {
						$pt_select .= '<option value="'. $key . '">'. $value .'</option>';
					}
				}
				$pt_select .= '</select>';
				$template->assign_block_vars('product', array(
				"PRODUCT" => $pt_select)
				);
			}
			if ($nrtype == 'SHAKING') 
			{
				$m_select = '<select name="TXT_METRIC"><option value=""></option>';
				foreach ($m_types as $key => $value) {
					if ($key == $nrt['metric']) {
						$m_select .= '<option value="'. $key . '" selected>'. $value .'</option>';
					} else {
						$m_select .= '<option value="'. $key . '">'. $value .'</option>';
					}
				}
				$m_select .= '</select>';
				$template->assign_block_vars('metric', array(
				"METRIC" => $m_select)
				);
			}
			if ($nrtype == 'DAMAGE') 
			{
				$dl_select = '<select name="TXT_DAMAGE_LEVEL"><option value=""></option>';
				foreach ($dl_types as $key => $value) {
					if ($key == $nrt['damage_level']) {
						$dl_select .= '<option value="'. $key . '" selected>'. $value .'</option>';
					} else {
						$dl_select .= '<option value="'. $key . '">'. $value .'</option>';
					}
				}
				$dl_select .= '</select>';
				$template->assign_block_vars('damage_level', array(
				"DAMAGE_LEVEL" => $dl_select)
				);
			}
		}
		else
		{
			$template->set_filenames(array(
				"body" => "admin/profile_notification_list_body.tpl")
			);
		
			$user_tag = '<input type="hidden" name="id" value="'.$profile_id.'" />';
			notification_jumpbox('admin_profile_notification.'.$phpEx, $user_tag);

			$sql = "SELECT nr.notification_request_id as id, nr.notification_type type, nr.event_type, nr.delivery_method as delivery, nr.message_format as template, nr.limit_value, nr.damage_level,
						nr.product_type as product, nr.metric, nr.disabled as disable, nr.aggregate, nr.aggregation_group, count(fnr.profile_id) as fnr_count
					FROM (" . PROFILE_NOTIFICATION_TABLE ." nr 
						LEFT JOIN ". GEOMETRY_FACILITY_PROFILE_TABLE ." fnr on nr.profile_id = fnr.profile_id)
					WHERE nr.profile_id = " . $profile_id  ."
					GROUP BY nr.notification_request_id
					ORDER BY nr.notification_type";
			$result = $db->sql_query($sql);
			if( !$result )
			{
				message_die(GENERAL_ERROR, "Couldn't get profile information.", "", __LINE__, __FILE__, $sql );
			}
		
			$template->assign_vars(array(
				"L_WORDS_TITLE" => $lang['profile_notification_title'],
				"L_WORDS_TEXT" => $lang['profile_notification_explain'],
				'L_REGISTRATION_INFO' => $lang['profile_notification_request_explain'],
				'PROFILE_NAME' => ($profile_id != '') ? "$profile_name  (ID: $profile_id)" : '',

				'L_ID' => $lang['id'],
				'L_TYPE' => $lang['type'],
				'L_EVENT_TYPE' => $lang['event_type'],
				'L_DELIVERY' => $lang['delivery'],
				'L_TEMPLATE' => $lang['template'],
				'L_LIMIT_VALUE' => $lang['limit_value'],
				'L_DAMAGE_LEVEL' => $lang['damage_level'],
				'L_PRODUCT' => $lang['product'],
				'L_METRIC' => $lang['metric'],
				'L_DISABLE' => $lang['disable'],
				'L_AGGREGATE' => $lang['aggregate'],
				'L_AGGREGATION_GROUP' => $lang['aggregation_group'],
				'L_FACILITY' => $lang['facility'],
		
				"L_EDIT" => "Edit",
				"L_DELETE" => "Delete",
				"L_ADD_EVENT" => "Add new profile notification",
				"L_ACTION" => $lang['Action'],
		
				"S_WORDS_ACTION" => append_sid("admin_profile_notification.$phpEx?mode=add"),
				"S_HIDDEN_FIELDS" => $hidden_fields)
			);
		
			while ($row = $db->sql_fetchrow($result))
			//for( $i = 0; $i < count($rows); $i++ )
			{
				$row_color = ( !($i % 2) ) ? $theme['td_color1'] : $theme['td_color2'];
				$row_class = ( !($i % 2) ) ? $theme['td_class1'] : $theme['td_class2'];
				$key = $row['id'];
				
				$template->assign_block_vars('words', array(
					"ROW_COLOR" => "#" . $row_color,
					"ROW_CLASS" => $row_class,
					"ID" => $row['id'],
					"Type" => $nt_types[$row['type']],
					"Event_Type" => $et_types[$row['event_type']],
					"Delivery" => $dm_types[$row['delivery']],
					"Template" => $mf_types[$row['template']],
					"Limit_Value" => $row['limit_value'],
					"Damage_Level" => $dl_types[$row['damage_level']],
					"Product" => $pt_types[$row['product']],
					"Metric" => $m_types[$row['metric']],
					"Disable" => ($row['disable']) ? $lang['Yes'] : '',
					"Aggregate" => ($row['aggregate']) ? $lang['Yes'] : '',
					"Aggregation_Group" => $row['aggregation_group'],
					"Facility_Count" => $row['fnr_count'],
		
					"U_FAC_EDIT" => append_sid("admin_profile_facility.$phpEx?mode=edit&amp;id=$profile_id&nkey=$key&nrtype=".$row['type']),
					"U_EDIT" => append_sid("admin_profile_notification.$phpEx?mode=edit&amp;id=$profile_id&nkey=$key&nrtype=".$row['type']),
					"U_DELETE" => append_sid("admin_profile_notification.$phpEx?mode=delete&amp;id=$profile_id&nkey=$key"))
					//"U_EDIT" => append_sid("admin_profile_notification.$phpEx?mode=edit&amp;notify_id=$notification_request_id&amp;type=$notification_type&amp;event=$event_type&amp;delivery=$delivery_method&amp;message_format=$message_format&amp;damage=$damage_level&amp;metric=$metric&amp;product=$product_type"),
					//"U_DELETE" => append_sid("admin_profile_notification.$phpEx?mode=delete&amp;notify_id=$notification_request_id"))
				);
			}
		}
	}
	else if( $mode == "save" )
	{
		$profile_id = intval($HTTP_POST_VARS['id']);
		if( isset( $HTTP_POST_VARS['source_mode'] ) || isset( $HTTP_GET_VARS['source_mode'] ) )
		{
			$source_mode = ( isset( $HTTP_POST_VARS['source_mode']) ) ? $HTTP_POST_VARS['source_mode'] : $HTTP_GET_VARS['source_mode'];
			$source_mode = htmlspecialchars($source_mode);
		}

		$notification_type = ( !empty($HTTP_POST_VARS['TXT_NOTIFICATION_TYPE']) ) ? "'" . str_replace("\'", "''", trim(strip_tags(htmlspecialchars($HTTP_POST_VARS['TXT_NOTIFICATION_TYPE'])))) ."'": "NULL";
		$event_type = ( !empty($HTTP_POST_VARS['TXT_EVENT_TYPE']) ) ? "'" . str_replace("\'", "''", trim(strip_tags(htmlspecialchars( $HTTP_POST_VARS['TXT_EVENT_TYPE'] ) ))) ."'": "NULL";
		if ($event_type == "NULL" && $notification_type != "'SYSTEM'") 
		{
			message_die(GENERAL_MESSAGE, "Must provide an event type. -$notification_type- ");
		}
		$delivery_method = ( !empty($HTTP_POST_VARS['TXT_DELIVERY_METHOD']) ) ? "'" . str_replace("\'", "''", trim(strip_tags(htmlspecialchars( $HTTP_POST_VARS['TXT_DELIVERY_METHOD'] ) ))) ."'": "NULL";
		if ($delivery_method == "NULL") 
		{
			message_die(GENERAL_MESSAGE, "Must provide a delivery method.");
		}
		$message_format = ( !empty($HTTP_POST_VARS['TXT_MESSAGE_FORMAT']) ) ? "'" . str_replace("\'", "''", trim(strip_tags(htmlspecialchars( $HTTP_POST_VARS['TXT_MESSAGE_FORMAT'] ) ))) ."'": "NULL";
		$limit_value = ( !empty($HTTP_POST_VARS['TXT_LIMIT_VALUE']) ) ? floatval( $HTTP_POST_VARS['TXT_LIMIT_VALUE'] ) : "NULL";
		$disabled = ( !empty($HTTP_POST_VARS['TXT_DISABLED']) ) ? intval( $HTTP_POST_VARS['TXT_DISABLED'] ) : "NULL";
		$aggregate = ( !empty($HTTP_POST_VARS['TXT_AGGREGATE']) ) ? intval( $HTTP_POST_VARS['TXT_AGGREGATE'] ) : "NULL";
		$aggregation_group = ( !empty($HTTP_POST_VARS['TXT_AGGREGATION_GROUP']) ) ? "'" . str_replace("\'", "''", trim(strip_tags(htmlspecialchars( $HTTP_POST_VARS['TXT_AGGREGATION_GROUP'] ) ))) ."'": "NULL";
		$damage_level = ( !empty($HTTP_POST_VARS['TXT_DAMAGE_LEVEL']) ) ? "'" . str_replace("\'", "''", trim(strip_tags(htmlspecialchars( $HTTP_POST_VARS['TXT_DAMAGE_LEVEL'] ) ))) ."'": "NULL";
		$product_type = ( !empty($HTTP_POST_VARS['TXT_PRODUCT_TYPE']) ) ? "'" . str_replace("\'", "''", trim(strip_tags(htmlspecialchars( $HTTP_POST_VARS['TXT_PRODUCT_TYPE'] ) )))."'" : "NULL";
		$metric = ( !empty($HTTP_POST_VARS['TXT_METRIC']) ) ? "'" . str_replace("\'", "''", trim(strip_tags(htmlspecialchars( $HTTP_POST_VARS['TXT_METRIC'] ) ))) ."'"   : "NULL";

		if ( $source_mode == 'edit' )
		{
			$sql = "UPDATE " . PROFILE_NOTIFICATION_TABLE . "
				SET event_type = " . $event_type .", delivery_method = " . $delivery_method . ", message_format = " . $message_format . ",
					metric = " . $metric .", product_type = " . $product_type .", damage_level = " . $damage_level .", aggregation_group = " . $aggregation_group .", 
					disabled = " . $disabled .", aggregate = " . $aggregate .", limit_value = " . $limit_value ."
				WHERE profile_id = ". $profile_id ." 
					AND notification_request_id = ". $nkey;
			if ( !($result = $db->sql_query($sql)) )
			{
				message_die(GENERAL_ERROR, 'Could not update profile notification request table', '', __LINE__, __FILE__, $sql);
			}

			$message .= 'Profile notification setting information has been updated.<br /><br />' . sprintf($lang['Click_return_notifyadmin'], '<a href="' . append_sid("admin_profile_notification.$phpEx?mode=edit&amp;id=$profile_id") . '">', '</a>') . '<br /><br />' . sprintf($lang['Click_return_admin_index'], '<a href="' . append_sid("index.$phpEx?pane=right") . '">', '</a>');

			message_die(GENERAL_MESSAGE, $message);
		}
		else if ( $source_mode == 'add' )
		{
			$sql = "INSERT INTO " . PROFILE_NOTIFICATION_TABLE . " (notification_type, event_type, delivery_method, message_format,
						metric, product_type, damage_level, aggregation_group, disabled, aggregate, limit_value, profile_id) 
					VALUES (" . $notification_type .", " . $event_type .", " . $delivery_method .", " . $message_format .", 
						" . $metric .", " . $product_type .", " . $damage_level .", " . $aggregation_group .", 
						$disabled, $aggregate, $limit_value, $profile_id)";
			if ( !($result = $db->sql_query($sql)) )
			{
				message_die(GENERAL_ERROR, 'Could not update profile notification request table', '', __LINE__, __FILE__, $sql);
			}

			$message .= 'Profile notification setting information has been inserted.<br /><br />' . sprintf($lang['Click_return_notifyadmin'], '<a href="' . append_sid("admin_profile_notification.$phpEx?mode=edit&amp;id=$profile_id") . '">', '</a>') . '<br /><br />' . sprintf($lang['Click_return_admin_index'], '<a href="' . append_sid("index.$phpEx?pane=right") . '">', '</a>');

			message_die(GENERAL_MESSAGE, $message);
		}

	}
	else if( $mode == "delete" )
	{
		$confirm = isset($HTTP_POST_VARS['confirm']);

		if( $profile_id != '' && $confirm )
		{
			if( $nkey != '')
			{
				$sql = "DELETE FROM " . PROFILE_NOTIFICATION_TABLE . "
					WHERE profile_id = $profile_id AND notification_request_id = $nkey";
				if( !$db->sql_query($sql) )
				{
					message_die(GENERAL_ERROR, 'Could not delete profile notification request', '', __LINE__, __FILE__, $sql);
				}
			}
			else 
			{
				$sql = "DELETE FROM " . PROFILE_NOTIFICATION_TABLE . "
					WHERE profile_id = $profile_id";
				if( !$db->sql_query($sql) )
				{
					message_die(GENERAL_ERROR, 'Could not delete profile notification request', '', __LINE__, __FILE__, $sql);
				}
			}
			$message = $lang['profile_notification_deleted'] . "<br /><br />" . sprintf($lang['Click_return_pfadmin'], "<a href=\"" . append_sid("admin_profile_notification.$phpEx") . "\">", "</a>") . "<br /><br />" . sprintf($lang['Click_return_admin_index'], "<a href=\"" . append_sid("index.$phpEx?pane=right") . "\">", "</a>");
			message_die(GENERAL_MESSAGE, $message);
		}
		elseif( $profile_id != '' && !$confirm)
		{
			// Present the confirmation screen to the user
			$template->set_filenames(array(
				'body' => 'admin/confirm_body.tpl')
			);

			$hidden_fields = '<input type="hidden" name="mode" value="'.$mode.'" /><input type="hidden" name="id" value="' . $profile_id . '" />';
			if( $nkey != '')
			{
				$hidden_fields .= '<input type="hidden" name="nkey" value="'.$nkey.'" />';
			}

			$template->assign_vars(array(
				'MESSAGE_TITLE' => $lang['Confirm'],
				'MESSAGE_TEXT' => $lang['Confirm_profile_notification_deleted'],

				'L_YES' => $lang['Yes'],
				'L_NO' => $lang['No'],

				'S_CONFIRM_ACTION' => append_sid("admin_profile_notification.$phpEx"),
				'S_HIDDEN_FIELDS' => $hidden_fields)
			);
		}
		else
		{
			message_die(GENERAL_MESSAGE, $lang['no_profile_notification_selected']);
		}

	}
}
else
{
	$template->set_filenames(array(
		"body" => "admin/profile_facility_list_body.tpl")
	);

	$profile_count = get_db_stat('profilecount');
	if ( !$profile_count )
	{
		//
		// No group members
		//
		$template->assign_block_vars('switch_no_members', array());
		$template->assign_vars(array(
			'L_NO_MEMBERS' => $lang['No_group_members'])
		);
	}

	$sql = "SELECT gp.profile_id, gp.profile_name, gp.description, gp.geom, count(pnr.notification_request_id) as total
			FROM geometry_profile gp LEFT JOIN profile_notification_request pnr on gp.profile_id = pnr.profile_id
			GROUP BY gp.profile_id";
	if(!$result = $db->sql_query($sql))
	{
		message_die(GENERAL_ERROR, "Could not find profile facility settings", $lang['Error']);
	}
	$profiles = $db->sql_fetchrowset($result);

	$template->assign_vars(array(
		"L_WORDS_TITLE" => $lang['profile_notification_title'],
		"L_WORDS_TEXT" => $lang['profile_notification_explain'],
		"L_PROFILE_ID" => "ID",
		"L_PROFILE_NAME" => "Profile Name",
		"L_DESCRIPTION" => "Description",
		"L_TOTAL" => "Notification Settings",

		"L_EDIT" => "Edit",
		"L_DELETE" => "Delete",
		"L_ADD_EVENT" => '<input type="submit" name="add_event" value="Add new profile notification" class="mainoption" />',
		"L_ACTION" => $lang['Action'],

		"S_WORDS_ACTION" => append_sid("admin_profile_notification.$phpEx?mode=add_profile"),
		"S_HIDDEN_FIELDS" => '')
	);

	for($i = 0;$i < count($profiles);  $i++)
	{
		$profile_id = $profiles[$i]['profile_id'];
		$profile_name = $profiles[$i]['profile_name'];
		$geom = $profiles[$i]['geom'];
		$description = $profiles[$i]['description'];
		$total = $profiles[$i]['total'];
		
		$row_color = ( !($i % 2) ) ? $theme['td_color1'] : $theme['td_color2'];
		$row_class = ( !($i % 2) ) ? $theme['td_class1'] : $theme['td_class2'];

		$template->assign_block_vars('words', array(
			"ROW_COLOR" => "#" . $row_color,
			"ROW_CLASS" => $row_class,
			"PROFILE_ID" => $profile_id,
			"PROFILE_NAME" => $profile_name,
			"DESCRIPTION" => $description,
			"TOTAL" => $total,

			"U_EDIT" => append_sid("admin_profile_notification.$phpEx?mode=edit&amp;id=$profile_id"),
			"U_DELETE" => append_sid("admin_profile_notification.$phpEx?mode=delete&amp;id=$profile_id"))
		);
	}
}

$template->pparse("body");

include('./page_footer_admin.'.$phpEx);

?>