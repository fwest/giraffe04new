var shape = gup('shape');
if (!shape) shape = 'poly';

var map, geocoder = null; 
//var centerLatitude = 40.6897;
//var centerLongitude = -95.0446; 
var startZoom = 5;
var deselectCurrent = function() {};
var removePolyline = function() {};
var earthRadius = 6378137; // in metres

var latlngs = [];
var allMarkers = {};

//Create the tile layer object
var detailLayer = new GTileLayer(new GCopyrightCollection(''));

//Method to retrieve the URL of the tile
detailLayer.getTileUrl = function(tile, zoom){
    //pass the x and y position as wel as the zoom
    var tileURL = "../server.php?x="+tile.x+"&y="+tile.y+"&zoom="+zoom;
	//GLog.writeUrl(tileURL);
    return tileURL;
};

detailLayer.isPng = function() {
    //The example uses GIF's
    return true;
}

//add your tiles to the normal map projection
detailMapLayers = G_NORMAL_MAP.getTileLayers();
detailMapLayers.push(detailLayer);

//create the ToolTip overlay object
function ToolTip(marker,html,width) {
	this.html_ = html;
	this.width_ = (width ? width + 'px' : 'auto');
	this.marker_ = marker;
}

ToolTip.prototype = new GOverlay();

ToolTip.prototype.initialize = function(map) {
	var div = document.createElement("div");
	div.style.display = 'none';
	map.getPane(G_MAP_FLOAT_PANE).appendChild(div);
	
	this.map_ = map;
	this.container_ = div;
}

ToolTip.prototype.remove = function() {
	this.container_.parentNode.removeChild(this.container_);
}

ToolTip.prototype.copy = function() {
	return new ToolTip(this.html_);
}

ToolTip.prototype.redraw = function(force) {
	if (!force) return;
	
	var pixelLocation = this.map_.fromLatLngToDivPixel(this.marker_.getPoint());
	var container_left = pixelLocation.x + 10;
	var container_top = pixelLocation.y - 15;
	this.container_.innerHTML = this.html_;
	this.container_.style.position = 'absolute';
	this.container_.style.left = container_left + "px";
	this.container_.style.top = container_top + "px";
	this.container_.style.width = this.width_;
	this.container_.style.font = '10px/10px verdana, arial, sans';
	this.container_.style.border = '1px solid gray';
	this.container_.style.background = '#FFFFCC';
	this.container_.style.padding = '4px';
	
	//one line to desired width
	this.container_.style.whiteSpace = 'nowrap';
	if(this.width_ != 'auto') this.container_.style.overflow = 'hidden';
	this.container_.style.display = 'block';
}

GMarker.prototype.ToolTipInstance = null;

GMarker.prototype.openToolTip = function(content) {
	//don't show the tool tip if there is acustom info window
	if(this.ToolTipInstance == null) {
		this.ToolTipInstance = new ToolTip(this,content)
		map.addOverlay(this.ToolTipInstance);
	}
}

GMarker.prototype.closeToolTip = function() {
	if(this.ToolTipInstance != null) {
		map.removeOverlay(this.ToolTipInstance);
		this.ToolTipInstance = null;
	}
}



// anticipates two 3-element arrays representing 3d vectors, returns a 3-element array representing their cross-product
function crossProduct(a, b) {
	return [(a[1] * b[2]) - (a[2] * b[1]), 
			(a[2] * b[0]) - (a[0] * b[2]), 
			(a[0] * b[1]) - (a[1] * b[0])];
}

// anticipates two 3-element arrays, returns scalar value
function dotProduct(a, b) {
	return (a[0] * b[0]) + (a[1] * b[1]) + (a[2] * b[2]);
}

function spherePointAngle(A, B, C) { // returns angle at B
    return Math.atan2(dotProduct(crossProduct(C, B), A), dotProduct(crossProduct(B, A), crossProduct(B, C)));
}

// returns 3-element array representing cartesian location of a point given by a GLatLng object
function cartesianCoordinates(latlng) {
    var x = Math.cos(latlng.latRadians()) * Math.sin(latlng.lngRadians());
    var y = Math.cos(latlng.latRadians()) * Math.cos(latlng.lngRadians());
    var z = Math.sin(latlng.latRadians());
	return [x, y, z];
}

// Calculate area inside of polygon, in square metres
function polylineArea(latlngs) {
	var id, sum = 0, pointCount = latlngs.length, cartesians = [];
	if (pointCount < 3) return 0;
	
	for (id in latlngs) {
	    cartesians[id] = cartesianCoordinates(latlngs[id]);
	}
	
	// pad out with the first two elements
	cartesians.push(cartesians[0]);
	cartesians.push(cartesians[1]);
		
	for(id = 0; id < pointCount; id++) {
		var A = cartesians[id];
		var B = cartesians[id + 1];
		var C = cartesians[id + 2];
		sum += spherePointAngle(A, B, C);
	}

	var alpha = Math.abs(sum - (pointCount - 2) * Math.PI);
    alpha -= 2 * Math.PI * Math.floor(alpha / (2 * Math.PI));
    alpha = Math.min(alpha, 4 * Math.PI - alpha);    	
    
    return Math.round(alpha * Math.pow(earthRadius, 2));
}


function initializePoint(id) {
	var marker = new GMarker(latlngs[id], { draggable:true });
	var listItem = document.createElement('p');
	var listItemLink = listItem.appendChild(document.createElement('a'));
	listItemLink.href = "#";
	var latitude = parseFloat(latlngs[id].lat());
	var longitude = parseFloat(latlngs[id].lng());
	listItemLink.innerHTML = '<strong>Point ' + (id+1) + '</strong>: (' + latitude.toFixed(4) + ', ' + longitude.toFixed(4) + ')';
	
/*	var focusPoint = function() {
		deselectCurrent();
		listItem.className = 'current';
		deselectCurrent = function() { listItem.className = ''; }
		map.panTo(latlngs[id]);
		return false;
	}
*/
	var focusPoint = function() {
		latlngs.splice(id,1);
		redrawPolyline();
	}

	GEvent.addListener(marker, 'click', focusPoint);
	listItemLink.onclick = focusPoint;

	document.getElementById('sidebar-list').appendChild(listItem);

	map.addOverlay(marker);  
	marker.openToolTip('<strong>' + (id + 1) + '</strong>');
	
	marker.enableDragging();
	GEvent.addListener(marker, 'dragend', function() {
		var latitude = parseFloat(latlngs[id].lat());
		var longitude = parseFloat(latlngs[id].lng());
		listItemLink.innerHTML = '<strong>Point ' + (id+1) + '</strong>: (' + latitude.toFixed(4) + ', ' + longitude.toFixed(4) + ')';
		marker.closeToolTip();
		latlngs[id] = marker.getPoint();
		marker.openToolTip('<strong>' + (id + 1) + '</strong>');
		//GLog.write('dragend: ' + id);
		redrawPolyline();
	});

}

function handleMapClick(marker, latlng) {
	if (!marker) {
		var array_len = latlngs.length;
		if (array_len > 0) {
			if (shape == 'poly') {
				latlngs.push(latlng);
			} else {
				latlngs[1] = latlng;
			}
			redrawPolyline();
		} else {
			latlngs.push(latlng);
			initializePoint(latlngs.length - 1);
		}
	}
}

function gm_profile() {
	var poly_latlngs = [];
	switch (shape) {
		case 'rect' :
			var lat0 = parseFloat(latlngs[0].lat());
			var lng0 = parseFloat(latlngs[0].lng());
			var lat1 = parseFloat(latlngs[1].lat());
			var lng1 = parseFloat(latlngs[1].lng());
			lat0 = lat0.toFixed(4);
			lng0 = lng0.toFixed(4);
			lat1 = lat1.toFixed(4);
			lng1 = lng1.toFixed(4);
			poly_latlngs.push(lat0, lng0, lat0, lng1, lat1, lng1,lat1, lng0);		
			break;
		case 'circle' :
			var lat0 = latlngs[0].latRadians();
			var lng0 = latlngs[0].lngRadians();
			var dist = latlngs[0].distanceFrom(latlngs[1]);
			for (var ind=0; ind < 12; ind++) {
				var brng = ind * Math.PI / 6;
				var lat2 =  Math.asin(Math.sin(lat0)*Math.cos(dist/earthRadius) + 
					Math.cos(lat0)*Math.sin(dist/earthRadius)*Math.cos(brng));
				var lng2 =  lng0 + Math.atan2(Math.sin(brng)*Math.sin(dist/earthRadius)*Math.cos(lat0), 
					Math.cos(dist/earthRadius)-Math.sin(lat0)*Math.sin(lat2));
					//GLog.write('lat: ' + lat2* 180 / Math.PI + ', lng: ' + lng2* 180 / Math.PI);
				lat2 = lat2 * 180 / Math.PI;
				lng2 = lng2 * 180 / Math.PI;
				poly_latlngs.push(lat2.toFixed(4), lng2.toFixed(4));
			}
			break;
		case 'poly' :
			for (var ind=0; ind < latlngs.length; ind++) {
				var lat0 = parseFloat(latlngs[ind].lat());
				var lng0 = parseFloat(latlngs[ind].lng());
				poly_latlngs.push(lat0.toFixed(4), lng0.toFixed(4));
			}
			break;
		default :
			latlngs.push(latlng);
	}
	poly_latlngs.push(poly_latlngs[0], poly_latlngs[1]);
	
	return poly_latlngs.join(",");
}

function clear_pane() {
	map.clearOverlays();
	var markers = document.getElementById('sidebar-list');
	while (markers.hasChildNodes()) {markers.removeChild(markers.lastChild);}
}

function redrawPolyline() {
    var pointCount = latlngs.length;
    var id;
	
	clear_pane();
	for (var latlng_id = 0; latlng_id < latlngs.length; latlng_id++ ) initializePoint(latlng_id);
	/*var	sidebarid = document.getElementById('sidebar-list');
	while (sidebarid.firstChild) {
		sidebarid.removeChild(sidebarid.firstChild);
	}*/
	var poly_latlngs = [];
	switch (shape) {
		case 'rect' :
			var lat0 = latlngs[0].lat();
			var lng0 = latlngs[0].lng();
			var lat1 = latlngs[1].lat();
			var lng1 = latlngs[1].lng();
			poly_latlngs.push(new GLatLng(lat0, lng0), new GLatLng(lat0, lng1), 
				new GLatLng(lat1, lng1), new GLatLng(lat1, lng0));		
			break;
		case 'circle' :
			var lat0 = latlngs[0].latRadians();
			var lng0 = latlngs[0].lngRadians();
			var dist = latlngs[0].distanceFrom(latlngs[1]);
			for (var ind=0; ind < 36; ind++) {
				var brng = ind * Math.PI / 18;
				var lat2 =  Math.asin(Math.sin(lat0)*Math.cos(dist/earthRadius) + 
					Math.cos(lat0)*Math.sin(dist/earthRadius)*Math.cos(brng));
				var lng2 =  lng0 + Math.atan2(Math.sin(brng)*Math.sin(dist/earthRadius)*Math.cos(lat0), 
					Math.cos(dist/earthRadius)-Math.sin(lat0)*Math.sin(lat2));
					//GLog.write('lat: ' + lat2* 180 / Math.PI + ', lng: ' + lng2* 180 / Math.PI);
				poly_latlngs.push(new GLatLng(lat2 * 180 / Math.PI , lng2 * 180 / Math.PI ));
			}
			break;
		case 'poly' :
			for (var ind=0; ind < latlngs.length; ind++) {
				poly_latlngs[ind] = latlngs[ind];
			}
			break;
		default :
			latlngs.push(latlng);
	}
    // Plot polyline, adding the first element to the end, to close the loop.
    poly_latlngs.push(poly_latlngs[0]);
	//GLog.write('latlngs: ' + latlngs.length + ' poly: ' + poly_latlngs.length);
    //var polyline = new GPolyline(latlngs, 'FF6633', 4, 0.8);
    var polyline = new GPolygon(poly_latlngs, 'FF6633', 4, 0.8, 'FF9966', 0.5);
    map.addOverlay(polyline);
}

function windowHeight() {
	// Standard browsers (Mozilla, Safari, etc.)
	if (self.innerHeight)
		return self.innerHeight;
	// IE 6
	if (document.documentElement && document.documentElement.clientHeight)
		return document.documentElement.clientHeight;
	// IE 5
	if (document.body)
		return document.body.clientHeight;
	// Just in case.
	return 0;
}

function handleResize() {
	var height = windowHeight() - document.getElementById('toolbar').offsetHeight - 30;
	document.getElementById('map').style.height = height + 'px';
	document.getElementById('sidebar').style.height = height + 'px';
}

function changeBodyClass(from, to) {
     document.body.className = document.body.className.replace(from, to);
     return false;
}

var tooltip_marker;
//var centerLatitude = 37;
//var centerLongitude = -120;
function handleMouseMove(latlng) {
	tooltip_marker.closeToolTip();
	tooltip_marker.setPoint(latlng);
	tooltip_marker.openToolTip(
		'Lat: ' + latlng.lat().toFixed(4) 
		+ "<br>Lon: " + latlng.lng().toFixed(4));
}

function drawPoly(latlonstring) {
	var latlng = latlonstring.split(",");
	var lat_avg = 0;
	var lon_avg = 0;
	
	for (var ind=0; ind < latlng.length - 2; ind = ind+2) {
		latlngs.push(new GLatLng(latlng[ind], latlng[ind+1]));
		initializePoint(latlngs.length - 1);
		lat_avg = lat_avg + parseFloat(latlng[ind]);
		lon_avg = lon_avg + parseFloat(latlng[ind+1]);
	}
	lat_avg = lat_avg / latlngs.length;
	lon_avg = lon_avg / latlngs.length;
		
	map.panTo(new GLatLng(lat_avg, lon_avg));
	redrawPolyline();
	
}

function changePoly(id) {
	var poly = document.getElementById(id).value;
	shape = poly;
	
	if (latlngs.length > 2 && shape != 'poly') {
		latlngs.splice(1,latlngs.length-2);
	}
	redrawPolyline();
}

function init_gm() {
	_mSvgEnabled = false; // Firefox 1.5.0.4/Mac wasn't rendering the SVG.
	
	//document.getElementById('button-sidebar-hide').onclick = function() { return changeBodyClass('sidebar-right', 'nosidebar'); };
	//document.getElementById('button-sidebar-show').onclick = function() { return changeBodyClass('nosidebar', 'sidebar-right'); };
	//handleResize();
	
	map = new GMap2(document.getElementById("map"));
	
//    map.setMapType(G_HYBRID_MAP);
	map.setCenter(new GLatLng(centerLatitude, centerLongitude), startZoom);	
	map.addControl(new GLargeMapControl());
    map.addControl(new GMapTypeControl());

	var office = new GLatLng(centerLatitude, centerLongitude);
	tooltip_marker = new GMarker(office);
	GEvent.addListener(map, 'mousemove', handleMouseMove);	

	GEvent.addListener(map,'mouseout',function() {
		tooltip_marker.closeToolTip();
	});

	GEvent.addListener(map, 'click', handleMapClick);	

}

function gup( name ) {
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var tmpURL = window.location.href;
  var results = regex.exec( tmpURL );
  if( results == null )
    return "";
  else
    return results[1];
}

function latlon( latlonstring ) {
  var regexS = "([\.\-\d]*)";
  var regex = new RegExp( regexS, "g" );
  var results = regex.exec( latlonstring );
  if( results == null )
    return "";
  else
    return results;
}

/* This is high-level function; REPLACE IT WITH YOUR CODE.
 * It must react to delta being more/less than zero.
function handle(delta) {
	if (delta < 0)
		map.zoomOut();
	else 
		map.zoomIn();
		
}

function wheel(event){
	var delta = 0;
	if (!event) event = window.event;
	if (event.wheelDelta) {
		delta = event.wheelDelta/120; 
		if (window.opera) delta = -delta;
	} else if (event.detail) {
		delta = -event.detail/3;
	}
	if (delta)
		handle(delta);
        if (event.preventDefault)
                event.preventDefault();
        event.returnValue = false;
}
 */

/* Initialization code. 
if (window.addEventListener)
	window.addEventListener('DOMMouseScroll', wheel, false);

window.onmousewheel = document.onmousewheel = wheel;
window.onresize = handleResize;
window.onload = init_gm;
*/
window.onunload = GUnload;


