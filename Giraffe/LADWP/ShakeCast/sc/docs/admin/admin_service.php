<?php
/*
# $Id: admin_service.php 344 2008-01-29 22:45:11Z klin $

# The PHP interface was originally inspired by PHPBB,
# file/database formats and directory structure are quite similar.

##############################################################################
# 
# Terms and Conditions of Software Use
# ====================================
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Disclaimer of Earthquake Information
# ====================================
# 
# The data and maps provided through this system are preliminary data
# and are subject to revision. They are computer generated and may not
# have received human review or official approval. Inaccuracies in the
# data may be present because of instrument or computer
# malfunctions. Subsequent review may result in significant revisions to
# the data. All efforts have been made to provide accurate information,
# but reliance on, or interpretation of earthquake data from a single
# source is not advised. Data users are cautioned to consider carefully
# the provisional nature of the information before using it for
# decisions that concern personal or public safety or the conduct of
# business that involves substantial monetary or operational
# consequences.
# 
# Disclaimer of Software and its Capabilities
# ===========================================
# 
# This software is provided as an "as is" basis.  Attempts have been
# made to rid the program of software defects and bugs, however the
# U.S. Geological Survey (USGS) have no obligations to provide maintenance, 
# support, updates, enhancements or modifications. In no event shall USGS 
# be liable to any party for direct, indirect, special, incidental or 
# consequential damages, including lost profits, arising out of the use 
# of this software, its documentation, or data obtained though the use 
# of this software, even if USGS or have been advised of the
# possibility of such damage. By downloading, installing or using this
# program, the user acknowledges and understands the purpose and
# limitations of this software.
# 
# Contact Information
# ===================
# 
# Coordination of this effort is under the auspices of the USGS Advanced
# National Seismic System (ANSS) coordinated in Golden, Colorado, which
# functions as the clearing house for development, distribution,
# documentation, and support. For questions, comments, or reports of
# potential bugs regarding this software please contact wald@usgs.gov or
# klin@usgs.gov.  
#
#############################################################################
*/

define('IN_SC', 1);

if( !empty($setmodules) )
{
	$file = basename(__FILE__);
	$module[' General Admin']['System Service'] = $file;
	return;
}

//
// Let's set the root dir for phpBB
//
$sc_root_path = "./../";
require($sc_root_path . 'extension.inc');
require('./pagestart.' . $phpEx);
include($sc_root_path . 'includes/functions_selects.'.$phpEx);
$sync_conf = '/shakecast/sc/bin/sync_conf.pl';
//
// Pull all config data
//
$sql = "SELECT *
	FROM " . CONFIG_TABLE;
if(!$result = $db->sql_query($sql))
{
	message_die(CRITICAL_ERROR, "Could not query config information in admin_service", "", __LINE__, __FILE__, $sql);
}
else
{
	while( $row = $db->sql_fetchrow($result) )
	{
		$config_name = $row['config_name'];
		$config_value = $row['config_value'];
		$default_config[$config_name] = isset($HTTP_POST_VARS['submit']) ? str_replace("'", "\'", $config_value) : $config_value;
		
		$new[$config_name] = ( isset($HTTP_POST_VARS[$config_name]) ) ? $HTTP_POST_VARS[$config_name] : $default_config[$config_name];
		if ($new[$config_name] != $default_config[$config_name]) 
		{
			$alert = true;
		}
		
		if ($config_name == 'cookie_name')
		{
			$new['cookie_name'] = str_replace('.', '_', $new['cookie_name']);
		}
		
		// Attempt to prevent a common mistake with this value,
		// http:// is the protocol and not part of the server name
		if ($config_name == 'server_name')
		{
			$new['server_name'] = str_replace('http://', '', $new['server_name']);
		}

		if( isset($HTTP_POST_VARS['submit']) )
		{
			$sql = "UPDATE " . CONFIG_TABLE . " SET
				config_value = '" . str_replace("\'", "''", $new[$config_name]) . "'
				WHERE config_name = '$config_name'";
			if( !$db->sql_query($sql) )
			{
				message_die(GENERAL_ERROR, "Failed to update general configuration for $config_name", "", __LINE__, __FILE__, $sql);
			}
		}
	}

	if( isset($HTTP_POST_VARS['submit']) )
	{
		$sql = "UPDATE " . CONFIG_TABLE . " SET
			config_value = '" . str_replace("\'", "''", $new['Notification_SmtpServer']) . "'
			WHERE config_name = 'smtp_host'";
		if( !$db->sql_query($sql) )
		{
			message_die(GENERAL_ERROR, "Failed to update system service configuration for smtp_host", "", __LINE__, __FILE__, $sql);
		}
		$sql = "UPDATE " . CONFIG_TABLE . " SET
			config_value = '" . str_replace("\'", "''", $new['Notification_Username']) . "'
			WHERE config_name = 'smtp_username'";
		if( !$db->sql_query($sql) )
		{
			message_die(GENERAL_ERROR, "Failed to update system service configuration for smtp_username", "", __LINE__, __FILE__, $sql);
		}
		$sql = "UPDATE " . CONFIG_TABLE . " SET
			config_value = '" . str_replace("\'", "''", $new['Notification_Password']) . "'
			WHERE config_name = 'smtp_password'";
		if( !$db->sql_query($sql) )
		{
			message_die(GENERAL_ERROR, "Failed to update system service configuration for smtp_password", "", __LINE__, __FILE__, $sql);
		}
		$sql = "UPDATE " . CONFIG_TABLE . " SET
			config_value = '" . str_replace("\'", "''", $new['Notification_From']) . "'
			WHERE config_name = 'board_email'";
		if( !$db->sql_query($sql) )
		{
			message_die(GENERAL_ERROR, "Failed to update system service configuration for smtp_password", "", __LINE__, __FILE__, $sql);
		}

		$result = exec('perl '.$sync_conf.' --toconf', $output);
		
		unset($message);
		if (isset($alert)) 
		{
			$message = '<b>Please restart ShakeCast services to reflect setting changes</b><br /><br />';
		}
		$message .= $lang['Config_updated'] . "<br /><br />" . sprintf($lang['Click_return_config'], "<a href=\"" . append_sid("admin_service.$phpEx") . "\">", "</a>") . "<br /><br />" . sprintf($lang['Click_return_admin_index'], "<a href=\"" . append_sid("index.$phpEx?pane=right") . "\">", "</a>");

		message_die(GENERAL_MESSAGE, $message);
	}
}

$loglevel_select = loglevel_select($new['LogLevel'], 'LogLevel');
$dispatcher_logging_select = loglevel_select($new['Dispatcher_LOGGING'], 'Dispatcher_LOGGING');
$poller_logging_select = loglevel_select($new['Poller_LOGGING'], 'Poller_LOGGING');
$poller_msglevel_select = loglevel_select($new['Poller_MSGLEVEL'], 'Poller_MSGLEVEL');
$notifyqueue_loglevel_select = loglevel_select($new['NotifyQueue_LogLevel'], 'NotifyQueue_LogLevel');
$notify_loglevel_select = loglevel_select($new['Notify_LogLevel'], 'Notify_LogLevel');
$rss_logging_select = loglevel_select($new['rss_LOGGING'], 'rss_LOGGING');
$rss_msglevel_select = loglevel_select($new['rss_MSGLEVEL'], 'rss_MSGLEVEL');

$dispatcher_autostart_yes = ( $new['Dispatcher_AUTOSTART'] ) ? "checked=\"checked\"" : "";
$dispatcher_autostart_no = ( !$new['Dispatcher_AUTOSTART'] ) ? "checked=\"checked\"" : "";
$poller_autostart_yes = ( $new['Dispatcher_AUTOSTART'] ) ? "checked=\"checked\"" : "";
$poller_autostart_no = ( !$new['Dispatcher_AUTOSTART'] ) ? "checked=\"checked\"" : "";
$rss_autostart_yes = ( $new['rss_AUTOSTART'] ) ? "checked=\"checked\"" : "";
$rss_autostart_no = ( !$new['rss_AUTOSTART'] ) ? "checked=\"checked\"" : "";
$logrotate_compress_yes = ( $new['Logrotate_compress'] ) ? "checked=\"checked\"" : "";
$logrotate_compress_no = ( !$new['Logrotate_compress'] ) ? "checked=\"checked\"" : "";

$style_select = style_select($new['default_style'], 'default_style', "../templates");
$lang_select = language_select($new['default_lang'], 'default_lang', "language");
$timezone_select = tz_select($new['board_timezone'], 'board_timezone');

$disable_board_yes = ( $new['board_disable'] ) ? "checked=\"checked\"" : "";
$disable_board_no = ( !$new['board_disable'] ) ? "checked=\"checked\"" : "";

$cookie_secure_yes = ( $new['cookie_secure'] ) ? "checked=\"checked\"" : "";
$cookie_secure_no = ( !$new['cookie_secure'] ) ? "checked=\"checked\"" : "";

$html_tags = $new['allow_html_tags'];

$override_user_style_yes = ( $new['override_user_style'] ) ? "checked=\"checked\"" : "";
$override_user_style_no = ( !$new['override_user_style'] ) ? "checked=\"checked\"" : "";

$html_yes = ( $new['allow_html'] ) ? "checked=\"checked\"" : "";
$html_no = ( !$new['allow_html'] ) ? "checked=\"checked\"" : "";

$bbcode_yes = ( $new['allow_bbcode'] ) ? "checked=\"checked\"" : "";
$bbcode_no = ( !$new['allow_bbcode'] ) ? "checked=\"checked\"" : "";

$activation_none = ( $new['require_activation'] == USER_ACTIVATION_NONE ) ? "checked=\"checked\"" : "";
$activation_user = ( $new['require_activation'] == USER_ACTIVATION_SELF ) ? "checked=\"checked\"" : "";
$activation_admin = ( $new['require_activation'] == USER_ACTIVATION_ADMIN ) ? "checked=\"checked\"" : "";

$confirm_yes = ($new['enable_confirm']) ? 'checked="checked"' : '';
$confirm_no = (!$new['enable_confirm']) ? 'checked="checked"' : '';

$allow_autologin_yes = ($new['allow_autologin']) ? 'checked="checked"' : '';
$allow_autologin_no = (!$new['allow_autologin']) ? 'checked="checked"' : '';

$board_email_form_yes = ( $new['board_email_form'] ) ? "checked=\"checked\"" : "";
$board_email_form_no = ( !$new['board_email_form'] ) ? "checked=\"checked\"" : "";

$gzip_yes = ( $new['gzip_compress'] ) ? "checked=\"checked\"" : "";
$gzip_no = ( !$new['gzip_compress'] ) ? "checked=\"checked\"" : "";

$privmsg_on = ( !$new['privmsg_disable'] ) ? "checked=\"checked\"" : "";
$privmsg_off = ( $new['privmsg_disable'] ) ? "checked=\"checked\"" : "";

$prune_yes = ( $new['prune_enable'] ) ? "checked=\"checked\"" : "";
$prune_no = ( !$new['prune_enable'] ) ? "checked=\"checked\"" : "";

$smile_yes = ( $new['allow_smilies'] ) ? "checked=\"checked\"" : "";
$smile_no = ( !$new['allow_smilies'] ) ? "checked=\"checked\"" : "";

$sig_yes = ( $new['allow_sig'] ) ? "checked=\"checked\"" : "";
$sig_no = ( !$new['allow_sig'] ) ? "checked=\"checked\"" : "";

$namechange_yes = ( $new['allow_namechange'] ) ? "checked=\"checked\"" : "";
$namechange_no = ( !$new['allow_namechange'] ) ? "checked=\"checked\"" : "";

$avatars_local_yes = ( $new['allow_avatar_local'] ) ? "checked=\"checked\"" : "";
$avatars_local_no = ( !$new['allow_avatar_local'] ) ? "checked=\"checked\"" : "";
$avatars_remote_yes = ( $new['allow_avatar_remote'] ) ? "checked=\"checked\"" : "";
$avatars_remote_no = ( !$new['allow_avatar_remote'] ) ? "checked=\"checked\"" : "";
$avatars_upload_yes = ( $new['allow_avatar_upload'] ) ? "checked=\"checked\"" : "";
$avatars_upload_no = ( !$new['allow_avatar_upload'] ) ? "checked=\"checked\"" : "";

$smtp_yes = ( $new['smtp_delivery'] ) ? "checked=\"checked\"" : "";
$smtp_no = ( !$new['smtp_delivery'] ) ? "checked=\"checked\"" : "";

$template->set_filenames(array(
	"body" => "admin/service_body.tpl")
);

//
// Escape any quotes in the site description for proper display in the text
// box on the admin page 
//
$new['site_desc'] = str_replace('"', '&quot;', $new['site_desc']);
$new['sitename'] = str_replace('"', '&quot;', strip_tags($new['sitename']));
$template->assign_vars(array(
	"S_CONFIG_ACTION" => append_sid("admin_service.$phpEx"),

	"L_YES" => $lang['Yes'],
	"L_NO" => $lang['No'],
	"L_SYSTEM_SERVICE_TITLE" => $lang['System_Service'],
	"L_SERVICE_EXPLAIN" => $lang['Service_explain'],
	"L_GENERAL_SETTINGS" => $lang['General_service_settings'],
	"L_GENERAL_SETTINGS_EXPLAIN" => $lang['General_service_settings_explain'],
	"L_ROOTDIR" => $lang['RootDir'], 
	"L_DATAROOT" => $lang['DataRoot'], 
	"L_TEMPLATEDIR" => $lang['TemplateDir'], 
	"L_SERVER_PORT_EXPLAIN" => $lang['Server_port_explain'], 
	"L_LOGDIR" => $lang['LogDir'], 
	"L_SCRIPT_PATH_EXPLAIN" => $lang['Script_path_explain'], 
	"L_LOGFILE" => $lang['LogFile'],
	"L_LOGLEVEL" => $lang['LogLevel'],
	"L_THRESHOLD" => $lang['Threshold'],
	"L_THRESHOLD_EXPLAIN" => $lang['Threshold_explain'],
	"L_USERID" => $lang['UserID'], 
	"L_GROUPID" => $lang['GroupID'],
	
	"L_LOCAL_DESTINATION" => $lang['Local_server_destination'],
	"L_LOCAL_DESTINATION_EXPLAIN" => $lang['Local_server_destination_explain'],
	"L_LOCALSERVERID" => $lang['LocalServerId'], 
	"L_Destination_HOSTNAME" => $lang['Destination_Hostname'], 
	"L_DESTINATION_PASSWORD" => $lang['Destination_Password'], 
	
	"L_ACCESS_ADMIN" => $lang['Access_Admin'], 
	"L_ACCESS_ADMIN_EXPLAIN" => $lang['Access_Admin_explain'],
	"L_ADMIN_HTPASSWORDPATH" => $lang['Admin_HtPasswordPath'], 
	"L_ADMIN_SERVERPWDFILE" => $lang['Admin_ServerPwdFile'], 
	"L_ADMIN_USERPWDFILE" => $lang['Admin_UserPwdFile'], 
	
	"L_DISPATCHER" => $lang['Dispatcher'], 
	"L_DISPATCHER_EXPLAIN" => $lang['Dispatcher_explain'],
	"L_DISPATCHER_AUTOSTART" => $lang['Dispatcher_AUTOSTART'],
	"L_DISPATCHER_LOG" => $lang['Dispatcher_LOG'],
	"L_DISPATCHER_LOGGING" => $lang['Dispatcher_LOGGING'],
	"L_DISPATCHER_MAXWORKERS" => $lang['Dispatcher_MaxWorkers'],
	"L_DISPATCHER_MINWORKERS" => $lang['Dispatcher_MinWorkers'], 
	"L_DISPATCHER_POLL" => $lang['Dispatcher_POLL'], 
	"L_DISPATCHER_PORT" => $lang['Dispatcher_PORT'],
	"L_DISPATCHER_PROMPT" => $lang['Dispatcher_PROMPT'], 
	"L_DISPATCHER_REQUESTPORT" => $lang['Dispatcher_RequestPort'], 
	"L_DISPATCHER_SERVICE_NAME" => $lang['Dispatcher_SERVICE_NAME'], 
	"L_DISPATCHER_SERVICE_TITLE" => $lang['Dispatcher_SERVICE_TITLE'], 
	"L_DISPATCHER_SPOLL" => $lang['Dispatcher_SPOLL'], 
	"L_DISPATCHER_WORKERPORT" => $lang['Dispatcher_WorkerPort'], 
	"L_DISPATCHER_WORKERTIMEOUT" => $lang['Dispatcher_WorkerTimeout'], 

	"L_POLLER" => $lang['Poller'], 
	"L_POLLER_EXPLAIN" => $lang['Poller_explain'],
	"L_POLLER_AUTOSTART" => $lang['Poller_AUTOSTART'], 
	"L_POLLER_LOG" => $lang['Poller_LOG'], 
	"L_POLLER_LOGGING" => $lang['Poller_LOGGING'], 
	"L_POLLER_MSGLEVEL" => $lang['Poller_MSGLEVEL'], 
	"L_POLLER_POLL" => $lang['Poller_POLL'],
	"L_POLLER_PORT" => $lang['Poller_PORT'],
	"L_POLLER_PROMPT" => $lang['Poller_PROMPT'],
	"L_POLLER_SERVICE_NAME" => $lang['Poller_SERVICE_NAME'], 
	"L_POLLER_SERVICE_TITLE" => $lang['Poller_SERVICE_TITLE'],
	"L_POLLER_SPOLL" => $lang['Poller_SPOLL'], 

	'L_NOTIFYQUEUE'			=> $lang['NotifyQueue'],
	"L_NOTIFYQUEUE_EXPLAIN" => $lang['NotifyQueue_explain'],
	'L_NOTIFYQUEUE_LOGLEVEL'	=> $lang['NotifyQueue_LogLevel'],
	'L_NOTIFYQUEUE_SCANPERIOD'			=> $lang['NotifyQueue_ScanPeriod'],
	'L_NOTIFYQUEUE_SERVICETITLE'	=> $lang['NotifyQueue_ServiceTitle'],
	'L_NOTIFYQUEUE_SPOLL'			=> $lang['NotifyQueue_Spoll'],

	'L_NOTIFY'			=> $lang['Notify'],
	"L_NOTIFY_EXPLAIN" => $lang['Notify_explain'],
	'L_NOTIFY_LOGLEVEL'	=> $lang['Notify_LogLevel'],
	'L_NOTIFY_SCANPERIOD'			=> $lang['Notify_ScanPeriod'],
	'L_NOTIFY_SERVICETITLE'	=> $lang['Notify_ServiceTitle'],
	'L_NOTIFY_SPOLL'			=> $lang['Notify_Spoll'],

	"L_RSS" => $lang['rss'], 
	"L_RSS_EXPLAIN" => $lang['rss_explain'],
	"L_RSS_AUTOSTART" => $lang['rss_AUTOSTART'], 
	"L_RSS_LOG" => $lang['rss_LOG'],
	"L_RSS_LOGGING" => $lang['rss_LOGGING'],
	"L_RSS_MSGLEVEL" => $lang['rss_MSGLEVEL'],
	"L_RSS_POLL" => $lang['rss_POLL'],
	"L_RSS_PORT" => $lang['rss_PORT'],
	"L_RSS_PROMPT" => $lang['rss_PROMPT'],
	"L_RSS_REGION" => $lang['rss_REGION'],
	"L_RSS_SERVICE_NAME" => $lang['rss_SERVICE_NAME'],
	"L_RSS_SERVICE_TITLE" => $lang['rss_SERVICE_TITLE'],
	"L_RSS_SPOLL" => $lang['rss_SPOLL'],
	"L_RSS_TIME_WINDOW" => $lang['rss_TIME_WINDOW'],
	
	"L_NOTIFICATION" => $lang['Notification'],
	"L_NOTIFICATION_FROM" => $lang['Notification_From'],
	"L_NOTIFICATION_ENVELOPEFROM" => $lang['Notification_EnvelopeFrom'],
	"L_NOTIFICATION_SMTPSERVER" => $lang['Notification_SmtpServer'],
	"L_NOTIFICATION_DEFAULTEMAILTEMPLATE" => $lang['Notification_DefaultEmailTemplate'],
	"L_NOTIFICATION_DEFAULTSCRIPTTEMPLATE" => $lang['Notification_DefaultScriptTemplate'],
	"L_NOTIFICATION_USERNAME" => $lang['Notification_Username'],
	"L_NOTIFICATION_PASSWORD" => $lang['Notification_Password'],
	
	"L_LOGROTATE" => $lang['Logrotate'],
	"L_LOGROTATE_EXPLAIN" => $lang['Logrotate_explain'],
	"L_LOGROTATE_LOGSTATDIR" => $lang['Logrotate_LOGSTATDIR'],
	"L_LOGROTATE_LOGFILE" => $lang['Logrotate_logfile'],
	"L_LOGROTATE_ROTATE-TIME" => $lang['Logrotate_rotate-time'],
	"L_LOGROTATE_MAX_SIZE" => $lang['Logrotate_max_size'],
	"L_LOGROTATE_KEEP-FILES" => $lang['Logrotate_keep-files'],
	"L_LOGROTATE_COMPRESS" => $lang['Logrotate_compress'],
	"L_LOGROTATE_STATUS-FILE" => $lang['Logrotate_status-file'],

	"L_SUBMIT" => $lang['Submit'], 
	"L_RESET" => $lang['Reset'], 
	
	"RootDir" => $new['RootDir'], 
	"DataRoot" => $new['DataRoot'], 
	"TemplateDir" => $new['TemplateDir'], 
	"LogDir" => $new['LogDir'], 
	"LogFile" => $new['LogFile'],
	"LogLevel" => $loglevel_select,
	"Threshold" => $new['Threshold'],
	"UserID" => $new['UserID'], 
	"GroupID" => $new['GroupID'],
	
	"LocalServerId" => $new['LocalServerId'], 
	"Destination_Hostname" => $new['Destination_Hostname'], 
	"Destination_Password" => $new['Destination_Password'], 
	
	"Admin_HtPasswordPath" => $new['Admin_HtPasswordPath'], 
	"Admin_ServerPwdFile" => $new['Admin_ServerPwdFile'], 
	"Admin_UserPwdFile" => $new['Admin_UserPwdFile'], 
	
	"Dispatcher_AUTOSTART" => $dispatcher_autostart_yes,
	"Dispatcher_AUTOSTART_NO" => $dispatcher_autostart_no,
	"Dispatcher_LOG" => $new['Dispatcher_LOG'],
	"Dispatcher_LOGGING" => $dispatcher_logging_select,
	"Dispatcher_MaxWorkers" => $new['Dispatcher_MaxWorkers'],
	"Dispatcher_MinWorkers" => $new['Dispatcher_MinWorkers'], 
	"Dispatcher_POLL" => $new['Dispatcher_POLL'], 
	"Dispatcher_PORT" => $new['Dispatcher_PORT'],
	"Dispatcher_PROMPT" => $new['Dispatcher_PROMPT'], 
	"Dispatcher_RequestPort" => $new['Dispatcher_RequestPort'], 
	"Dispatcher_SERVICE_NAME" => $new['Dispatcher_SERVICE_NAME'], 
	"Dispatcher_SERVICE_TITLE" => $new['Dispatcher_SERVICE_TITLE'], 
	"Dispatcher_SPOLL" => $new['Dispatcher_SPOLL'], 
	"Dispatcher_WorkerPort" => $new['Dispatcher_WorkerPort'], 
	"Dispatcher_WorkerTimeout" => $new['Dispatcher_WorkerTimeout'], 

	"Poller_AUTOSTART" => $poller_autostart_yes, 
	"Poller_AUTOSTART_NO" => $poller_autostart_no,
	"Poller_LOG" => $new['Poller_LOG'], 
	"Poller_LOGGING" => $poller_logging_select, 
	"Poller_MSGLEVEL" => $poller_msglevel_select, 
	"Poller_POLL" => $new['Poller_POLL'],
	"Poller_PORT" => $new['Poller_PORT'],
	"Poller_PROMPT" => $new['Poller_PROMPT'],
	"Poller_SERVICE_NAME" => $new['Poller_SERVICE_NAME'], 
	"Poller_SERVICE_TITLE" => $new['Poller_SERVICE_TITLE'],
	"Poller_SPOLL" => $new['Poller_SPOLL'], 

	'NotifyQueue_LogLevel'	=> $notifyqueue_loglevel_select,
	'NotifyQueue_ScanPeriod'			=> $new['NotifyQueue_ScanPeriod'],
	'NotifyQueue_ServiceTitle'	=> $new['NotifyQueue_ServiceTitle'],
	'NotifyQueue_Spoll'			=> $new['NotifyQueue_Spoll'],

	'Notify_LogLevel'	=> $notify_loglevel_select,
	'Notify_ScanPeriod'			=> $new['Notify_ScanPeriod'],
	'Notify_ServiceTitle'	=> $new['Notify_ServiceTitle'],
	'Notify_Spoll'			=> $new['Notify_Spoll'],

	"rss_AUTOSTART" => $rss_autostart_yes,
	"rss_AUTOSTART_NO" => $rss_autostart_no,
	"rss_LOG" => $new['rss_LOG'],
	"rss_LOGGING" => $rss_logging_select,
	"rss_MSGLEVEL" => $rss_msglevel_select,
	"rss_POLL" => $new['rss_POLL'],
	"rss_PORT" => $new['rss_PORT'],
	"rss_PROMPT" => $new['rss_PROMPT'],
	"rss_REGION" => $new['rss_REGION'],
	"rss_SERVICE_NAME" => $new['rss_SERVICE_NAME'],
	"rss_SERVICE_TITLE" => $new['rss_SERVICE_TITLE'],
	"rss_SPOLL" => $new['rss_SPOLL'],
	"rss_TIME_WINDOW" => $new['rss_TIME_WINDOW'],
	
	"Notification_From" => $new['Notification_From'],
	"Notification_EnvelopeFrom" => $new['Notification_EnvelopeFrom'],
	"Notification_SmtpServer" => $new['Notification_SmtpServer'],
	"Notification_DefaultEmailTemplate" => $new['Notification_DefaultEmailTemplate'],
	"Notification_DefaultScriptTemplate" => $new['Notification_DefaultScriptTemplate'],
	"Notification_Username" => $new['Notification_Username'],
	"Notification_Password" => $new['Notification_Password'],

	"Logrotate_LOGSTATDIR" => $new['Logrotate_LOGSTATDIR'],
	"Logrotate_logfile" => $new['Logrotate_logfile'],
	"Logrotate_rotate-time" => $new['Logrotate_rotate-time'],
	"Logrotate_max_size" => $new['Logrotate_max_size'],
	"Logrotate_keep-files" => $new['Logrotate_keep-files'],
	"Logrotate_compress" => $logrotate_compress_yes,
	"Logrotate_compress_NO" => $logrotate_compress_no,
	"Logrotate_status-file" => $new['Logrotate_status-file']

	)
);

$template->pparse("body");

include('./page_footer_admin.'.$phpEx);

?>
