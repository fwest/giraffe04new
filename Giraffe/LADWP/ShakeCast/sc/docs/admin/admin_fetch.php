<?php
/*
# $Id: admin_fetch.php 525 2008-11-14 14:35:42Z guest $

# The PHP interface was originally inspired by PHPBB,
# file/database formats and directory structure are quite similar.

##############################################################################
# 
# Terms and Conditions of Software Use
# ====================================
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Disclaimer of Earthquake Information
# ====================================
# 
# The data and maps provided through this system are preliminary data
# and are subject to revision. They are computer generated and may not
# have received human review or official approval. Inaccuracies in the
# data may be present because of instrument or computer
# malfunctions. Subsequent review may result in significant revisions to
# the data. All efforts have been made to provide accurate information,
# but reliance on, or interpretation of earthquake data from a single
# source is not advised. Data users are cautioned to consider carefully
# the provisional nature of the information before using it for
# decisions that concern personal or public safety or the conduct of
# business that involves substantial monetary or operational
# consequences.
# 
# Disclaimer of Software and its Capabilities
# ===========================================
# 
# This software is provided as an "as is" basis.  Attempts have been
# made to rid the program of software defects and bugs, however the
# U.S. Geological Survey (USGS) have no obligations to provide maintenance, 
# support, updates, enhancements or modifications. In no event shall USGS 
# be liable to any party for direct, indirect, special, incidental or 
# consequential damages, including lost profits, arising out of the use 
# of this software, its documentation, or data obtained though the use 
# of this software, even if USGS or have been advised of the
# possibility of such damage. By downloading, installing or using this
# program, the user acknowledges and understands the purpose and
# limitations of this software.
# 
# Contact Information
# ===================
# 
# Coordination of this effort is under the auspices of the USGS Advanced
# National Seismic System (ANSS) coordinated in Golden, Colorado, which
# functions as the clearing house for development, distribution,
# documentation, and support. For questions, comments, or reports of
# potential bugs regarding this software please contact wald@usgs.gov or
# klin@usgs.gov.  
#
#############################################################################
*/

if( !empty($setmodules) )
{
	$file = basename(__FILE__);
	$module['Event_Admin']['USGS ShakeMap'] = $file;
	return;
}

define('IN_SC', 1);

//
// Load default header
//
$sc_root_path = "./../";
require($sc_root_path . 'extension.inc');

$cancel = ( isset($HTTP_POST_VARS['cancel']) ) ? true : false;
$no_page_header = $cancel;
//$template_file = "page_gs_inject_header.tpl";
require('./pagestart.' . $phpEx);

@set_time_limit(1200);

if ($cancel)
{
	redirect('admin/' . append_sid("admin_event.$phpEx", true));
}

$scfeed_local = $sc_conf_array['ROOT']['RootDir']."/bin/scfeed_local.pl";
$manage_event = $sc_conf_array['ROOT']['RootDir']."/bin/manage_event.pl";
if (isset($sc_conf_array['ROOT']['ProxyServer']))
{
	// Define a context for HTTP.
	$proxy = preg_replace('`http://`i', 'tcp://', $sc_conf_array['ROOT']['ProxyServer']);
	$aContext = array(
	'http' => array(
		'proxy' => $proxy, // This needs to be the server and the port of the NTLM Authentication Proxy Server.
		'request_fulluri' => True,
		),
	);
	$cxContext = stream_context_create($aContext);
}
	

if( isset($HTTP_GET_VARS['mode']) || isset($HTTP_POST_VARS['mode']) )
{
	$mode = (isset($HTTP_GET_VARS['mode'])) ? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode'];
	$mode = htmlspecialchars($mode);
}
else 
{
	//
	// These could be entered via a form button
	//
	if( isset($HTTP_POST_VARS['add_event']) )
	{
		$mode = "add_event";
	}
	else if( isset($HTTP_POST_VARS['save']) )
	{
		$mode = "save";
	}
	else
	{
		$mode = "";
	}
}

// Restrict mode input to valid options
$mode = ( in_array($mode, array('add', 'info', 'comment', 'realert', 'delete', 'add_event', 'inject', 'download')) ) ? $mode : '';

if( $mode != "" )
{
	if( $mode == "edit" || $mode == "add" )
	{
		$word_id = ( isset($HTTP_GET_VARS['id']) ) ? intval($HTTP_GET_VARS['id']) : 0;

		$template->set_filenames(array(
			"body" => "admin/words_edit_body.tpl")
		);

		$word_info = array('word' => '', 'replacement' => '');
		$s_hidden_fields = '';

		if( $mode == "edit" )
		{
			if( $word_id )
			{
				$sql = "SELECT * 
					FROM " . WORDS_TABLE . " 
					WHERE word_id = $word_id";
				if(!$result = $db->sql_query($sql))
				{
					message_die(GENERAL_ERROR, "Could not query words table", "Error", __LINE__, __FILE__, $sql);
				}

				$word_info = $db->sql_fetchrow($result);
				$s_hidden_fields .= '<input type="hidden" name="id" value="' . $word_id . '" />';
			}
			else
			{
				message_die(GENERAL_MESSAGE, $lang['No_word_selected']);
			}
		}

		$template->assign_vars(array(
			"WORD" => $word_info['word'],
			"REPLACEMENT" => $word_info['replacement'],

			"L_WORDS_TITLE" => $lang['test_title'],
			"L_WORDS_TEXT" => $lang['test_explain'],
			"L_WORD_CENSOR" => $lang['Edit_word_censor'],
			"L_WORD" => $lang['Word'],
			"L_REPLACEMENT" => $lang['Replacement'],
			"L_SUBMIT" => $lang['Submit'],

			"S_WORDS_ACTION" => append_sid("admin_words.$phpEx"),
			"S_HIDDEN_FIELDS" => $s_hidden_fields)
		);

		$template->pparse("body");

		include('./page_footer_admin.'.$phpEx);
	}
	else if( $mode == "save" )
	{
		$word_id = ( isset($HTTP_POST_VARS['id']) ) ? intval($HTTP_POST_VARS['id']) : 0;
		$word = ( isset($HTTP_POST_VARS['word']) ) ? trim($HTTP_POST_VARS['word']) : "";
		$replacement = ( isset($HTTP_POST_VARS['replacement']) ) ? trim($HTTP_POST_VARS['replacement']) : "";

		if($word == "" || $replacement == "")
		{
			message_die(GENERAL_MESSAGE, $lang['Must_enter_word']);
		}

		if( $word_id )
		{
			$sql = "UPDATE " . WORDS_TABLE . " 
				SET word = '" . str_replace("\'", "''", $word) . "', replacement = '" . str_replace("\'", "''", $replacement) . "' 
				WHERE word_id = $word_id";
			$message = $lang['Word_updated'];
		}
		else
		{
			$sql = "INSERT INTO " . WORDS_TABLE . " (word, replacement) 
				VALUES ('" . str_replace("\'", "''", $word) . "', '" . str_replace("\'", "''", $replacement) . "')";
			$message = $lang['Word_added'];
		}

		if(!$result = $db->sql_query($sql))
		{
			message_die(GENERAL_ERROR, "Could not insert data into words table", $lang['Error'], __LINE__, __FILE__, $sql);
		}

		$message .= "<br /><br />" . sprintf($lang['Click_return_wordadmin'], "<a href=\"" . append_sid("admin_words.$phpEx") . "\">", "</a>") . "<br /><br />" . sprintf($lang['Click_return_admin_index'], "<a href=\"" . append_sid("index.$phpEx?pane=right") . "\">", "</a>");

		message_die(GENERAL_MESSAGE, $message);
	}
	else if( $mode == "info")
	{
		if( isset($HTTP_POST_VARS['id']) ||  isset($HTTP_GET_VARS['id']) )
		{
			$event_id = ( isset($HTTP_POST_VARS['id']) ) ? $HTTP_POST_VARS['id'] : $HTTP_GET_VARS['id'];
		}
		else
		{
			message_die(GENERAL_MESSAGE, $lang['No_event_selected']);
		}

		$sql = "SELECT grid_id FROM grid WHERE shakemap_id = \"$event_id\"";
		if ( !($result = $db->sql_query($sql)) )
		{
			message_die(GENERAL_ERROR, 'Could not find ShakeCast event', '', __LINE__, __FILE__, $sql);
		}
	
		if ( $row = $db->sql_fetchrow($result) )
		{
			$grid_sql = "or n.grid_id = ".$row['grid_id'];
		}
		$sql = "SELECT DISTINCT su.email_address 
				FROM shakecast_user su INNER JOIN notification n on su.shakecast_user = n.shakecast_user 
				WHERE n.event_id = \"$event_id\" $grid_sql";
		if ( !($result = $db->sql_query($sql)) )
		{
			message_die(GENERAL_ERROR, 'Could not select group members', '', __LINE__, __FILE__, $sql);
		}
		$template->set_filenames(array(
			"body" => "admin/event_summary_body.tpl")
		);
	
		$result = exec('perl '.$tester.' -type new_test', $output);
		$field_name = explode("::", $output[0]);
		$event_count = count($output);
	
		$template->assign_vars(array(
			"L_WORDS_TITLE" => "ShakeCast Processed Event List",
			"L_WORDS_TEXT" => $lang['test_explain'],
			"L_EVENT_ID" => $lang[$field_name[0]],
			"L_EVENT_TIMESTAMP" => $lang[$field_name[1]],
			"L_MAGNITUDE" => $lang[$field_name[2]],
			"L_LATITUDE" => $lang[$field_name[3]],
			"L_LONGITUDE" => $lang[$field_name[4]],
			"L_DESCRIPTION" => $lang[$field_name[5]],
			"L_REALERT" => "Re-Alert",
			"L_COMMENT" => "Comment",
			"L_DELETE" => "Delete",
			"L_ADD_EVENT" => "Inject ShakeMap Event",
			"L_ACTION" => $lang['Action'],
	
			"S_WORDS_ACTION" => append_sid("admin_event.$phpEx"),
			"S_HIDDEN_FIELDS" => '')
		);
	
		for($i = $event_count -1; $i >= 1; $i--)
		{
			$fields = explode("::", $output[$i]);
			$event_id = $fields[0];
			$event_timestamp = $fields[1];
			$magnitude = $fields[2];
			$lat = $fields[3];
			$lon = $fields[4];
			$desc = $fields[5];
	
			$row_color = ( !($i % 2) ) ? $theme['td_color1'] : $theme['td_color2'];
			$row_class = ( !($i % 2) ) ? $theme['td_class1'] : $theme['td_class2'];
	
			$template->assign_block_vars("words", array(
				"ROW_COLOR" => "#" . $row_color,
				"ROW_CLASS" => $row_class,
				"EVENT_ID" => $event_id,
				"EVENT_TIMESTAMP" => $event_timestamp,
				"MAGNITUDE" => $magnitude,
				"LATITUDE" => $lat,
				"LONGITUDE" => $lon,
				"DESCRIPTION" => $desc,
	
				"U_EVENT" => append_sid("admin_event.$phpEx?mode=info&amp;id=$event_id"),
				"U_REALERT" => append_sid("admin_event.$phpEx?mode=realert&amp;id=$event_id"),
				"U_COMMENT" => append_sid("admin_event.$phpEx?mode=comment&amp;id=$event_id"),
				"U_DELETE" => append_sid("admin_event.$phpEx?mode=delete&amp;id=$event_id"))
			);
		}
	
	}
	else if( $mode == "delete" || $mode == "realert")
	{
		if( isset($HTTP_POST_VARS['id']) ||  isset($HTTP_GET_VARS['id']) )
		{
			$event_id = ( isset($HTTP_POST_VARS['id']) ) ? $HTTP_POST_VARS['id'] : $HTTP_GET_VARS['id'];
		}
		else
		{
			$event_id = 0;
		}

		$confirm = isset($HTTP_POST_VARS['confirm']); 

		if( $event_id && $confirm )
		{
			$result = exec('perl '.$manage_event.' -'.$mode.' '.$event_id, $output);

			if(!$result)
			{
				message_die(GENERAL_ERROR, "Could not ".$mode." event", $lang['Error']);
			}

			$message = $lang['event_'.$mode] . "<br /><br />" . sprintf($lang['Click_return_eventadmin'], "<a href=\"" . append_sid("admin_event.$phpEx") . "\">", "</a>") . "<br /><br />" . sprintf($lang['Click_return_admin_index'], "<a href=\"" . append_sid("index.$phpEx?pane=right") . "\">", "</a>");

			message_die(GENERAL_MESSAGE, $message);
		}
		elseif( $event_id && !$confirm)
		{
			// Present the confirmation screen to the user
			$template->set_filenames(array(
				'body' => 'admin/confirm_body.tpl')
			);

			$hidden_fields = '<input type="hidden" name="mode" value="'.$mode.'" /><input type="hidden" name="id" value="' . $event_id . '" />';

			$template->assign_vars(array(
				'MESSAGE_TITLE' => $lang['Confirm'],
				'MESSAGE_TEXT' => $lang['confirm_event_'.$mode],

				'L_YES' => $lang['Yes'],
				'L_NO' => $lang['No'],

				'S_CONFIRM_ACTION' => append_sid("admin_event.$phpEx"),
				'S_HIDDEN_FIELDS' => $hidden_fields)
			);
		}
		else
		{
			message_die(GENERAL_MESSAGE, $lang['No_event_selected']);
		}
	}
	else if( $mode == "inject" || $mode == "download")
	{
		if( isset($HTTP_POST_VARS['id']) ||  isset($HTTP_GET_VARS['id']) )
		{
			$event_id = ( isset($HTTP_POST_VARS['id']) ) ? $HTTP_POST_VARS['id'] : $HTTP_GET_VARS['id'];
		}
		else
		{
			$event_id = '';
		}

		if( isset($HTTP_POST_VARS['network']) ||  isset($HTTP_GET_VARS['network']) )
		{
			$network = ( isset($HTTP_POST_VARS['network']) ) ? $HTTP_POST_VARS['network'] : $HTTP_GET_VARS['network'];
		}
		else
		{
			$network = '';
		}

		if ($network == '' || $event_id == '') 
		{
			message_die(GENERAL_MESSAGE, $lang['No_event_selected']);
		}

		$template->set_filenames(array(
			"body" => "admin/gs_inject_body.tpl")
		);

		$output_dir = $sc_conf_array['ROOT']['DataRoot']."/$event_id";
		if (!is_dir($output_dir) && !mkdir($output_dir, 0755)) 
		{
			message_die(GENERAL_MESSAGE, $lang['Event_directory_error']);
		}
		
		$status = array();
		$url = "http://earthquake.usgs.gov/eqcenter/shakemap/$network/shake/$event_id/download/";
		$input = (isset($sc_conf_array['ROOT']['ProxyServer'])) ? file_get_contents($url, False, $cxContext) : file_get_contents($url);
		$shakemap = array();
		$shakemap_hash = array();
		$regexp = "<a\s[^>]*href=(\"??)([^\" >]*?)\\1[^>]*>(.*)<\/a>"; 
		if(preg_match_all("/$regexp/siU", $input, $matches, PREG_SET_ORDER)) { 
			$status[] = array("count" => count($matches));

			$template->assign_vars(array(
				"L_FETCH_TITLE" => $lang['FETCH_TITLE'],
				"L_FETCH_TEXT" => $lang['FETCH_TEXT'],
				"L_DOWNLOAD_TASK" => 'Downloading '. (count($matches)-5) ." ShakeMap products from USGS web site")
			);

			for ($ind = 5; $ind < count($matches); $ind++) { 
				$input = (isset($sc_conf_array['ROOT']['ProxyServer'])) ? file_get_contents($url.$matches[$ind][2], False, $cxContext) 
					: file_get_contents($url.$matches[$ind][2]);
				if ($input) {
					file_put_contents("$output_dir/".$matches[$ind][2], $input);
					file_put_contents("$output_dir/status", json_encode($status));
					$status["item"] = $matches[$ind][2];
					
					$template->assign_vars(array(
						"DOWNLOAD_PER" => (round($ind/count($matches) * 500)) ,
						"L_PRODUCT_DOWNLOAD" => $matches[$ind][2].' downloaded')
					);
					$template->pparse("body");
					ob_flush();
					flush();
				}
				# $match[3] = link text 
			} 
		}
	
		$template->assign_vars(array(
			"DOWNLOAD_PER" => 500 ,
			"L_DOWNLOAD_TASK" => 'ShakeMap download completed')
		);
		$template->pparse("body");
		ob_flush();
		flush();

		if( $mode == "inject" )
		{
			echo '<div id="text">Injecting ShakeMap into ShakeCast system</div>';
			echo "<div class='bar blank'></div>\n";
			ob_flush();
			flush();
			if( isset($HTTP_POST_VARS['scenario']) ||  isset($HTTP_GET_VARS['scenario']) )
			{
				$result = exec('perl '.$scfeed_local.' -scenario -event '.$event_id, $output);
			}
			else
			{
				$result = exec('perl '.$scfeed_local.' -event '.$event_id, $output);
			}
	
			if(!$result)
			{
				echo '<div id="text">Error during injection</div>';
			}
			else
			{
				echo '<div id="text">Injection completed</div>';
			}
		}
	}
}
else
{
	if( isset($HTTP_POST_VARS['type']) ||  isset($HTTP_GET_VARS['type']) )
	{
		$type = ( isset($HTTP_POST_VARS['type']) ) ? $HTTP_POST_VARS['type'] : $HTTP_GET_VARS['type'];
	}
	else
	{
		$type = 'x';
	}

	if( isset($HTTP_POST_VARS['network']) ||  isset($HTTP_GET_VARS['network']) )
	{
		$network = ( isset($HTTP_POST_VARS['network']) ) ? $HTTP_POST_VARS['network'] : $HTTP_GET_VARS['network'];
	}

	if( isset($HTTP_POST_VARS['y']) ||  isset($HTTP_GET_VARS['y']) )
	{
		$year = ( isset($HTTP_POST_VARS['y']) ) ? $HTTP_POST_VARS['y'] : $HTTP_GET_VARS['y'];
	}

	$template->set_filenames(array(
		"body" => "admin/gs_list_body.tpl")
	);

	$url = "http://earthquake.usgs.gov/eqcenter/shakemap/list.php?$type=1";
	if( isset($network) )
	{
		if ($network == 'atlas') 
		{
			$url = "http://earthquake.usgs.gov/eqcenter/shakemap/atlas.php?y=$year";
		}
		else
		{
			$url .= "&n=$network";
		}
	}
	$input = (isset($sc_conf_array['ROOT']['ProxyServer'])) ? file_get_contents($url, False, $cxContext) : file_get_contents($url);
	
	$shakemap = array();
	$shakemap_hash = array();
	//<a href="/eqcenter/shakemap/global/shake/2008lraf/">KYRGYZSTAN</a></td><td align="center" nowrap="nowrap">Jan 01 2008</td><td align="center" nowrap="nowrap">06:32:33 UTC</td><td align="right" nowrap="nowrap">40.346</td><td align="right" nowrap="nowrap">72.939</td><td align="center" nowrap="nowrap">Global</td><td nowrap="nowrap">2008lraf</td></tr>
	$regexp = "<a\s[^>]*href=(\"??)([^\" >]*?)\\1[^>]*>(.*)<\/a>"; 
	if(preg_match_all("/$regexp/siU", $input, $matches, PREG_SET_ORDER)) { 
		for ($ind = 0; $ind < count($matches); $ind++) { 
			if(preg_match("/shakemap/i", $matches[$ind][2], $matches_temp)) { 
				$url_element = explode('/', $matches[$ind][2]);
				if (!isset($shakemap_hash[$url_element[3].$url_element[5]]) && $url_element[4] =='shake' && $url_element[5] !='') {
					array_push($shakemap, 
					array('network' => $url_element[3], 
						'id' => $url_element[5],
						'name' => $matches[$ind][3]));
					$shakemap_hash[$url_element[3].$url_element[5]] = true;
				}	
			}
		} 
	}
	
	$event_count = count($shakemap_hash);
	if ( !$event_count )
	{
		//
		// No group members
		//
		$template->assign_block_vars('switch_no_members', array());
		$template->assign_vars(array(
			'L_NO_MEMBERS' => $lang['No_group_members'])
		);
	}

	if( isset($HTTP_POST_VARS['start']) ||  isset($HTTP_GET_VARS['start']) )
	{
		$start = ( isset($HTTP_POST_VARS['start']) ) ? $HTTP_POST_VARS['start'] : $HTTP_GET_VARS['start'];
	}
	$current_page = ( !$event_count ) ? 1 : ceil( $event_count / $board_config['topics_per_page'] );
	$template->assign_vars(array(
		'PAGINATION' => generate_pagination("admin_fetch.$phpEx?type=$type&network=$network&y=$year", $event_count, $board_config['topics_per_page'], $start),
		'PAGE_NUMBER' => sprintf($lang['Page_of'], ( floor( $start / $board_config['topics_per_page'] ) + 1 ), $current_page ), 

		'L_GOTO_PAGE' => $lang['Goto_page'])
	);

	$template->assign_vars(array(
		"L_WORDS_TITLE" => "ShakeMap Event List on the USGS web site",
		"L_WORDS_TEXT" => $lang['fetch_explain'],
		"L_EVENT_ID" => $lang['EVENT_ID'],
		"L_NETWORK" => $lang['NETWORK'],
		"L_DESCRIPTION" => $lang['DESCRIPTION'],
		"L_ADD_EVENT" => "Inject ShakeMap Event",
		"L_INJECT" => "Inject",
		"L_INJECT_SCENARIO" => "Scenario Inject",
		"L_DOWNLOAD" => "Download Only",
		"L_EVENT_LIST" => "Actual Events",
		"L_SCENARIO_LIST" => "Scenario Events",
		"L_ATLAS_LIST" => "Atlas Events",
		"L_SC" => "S. California",
		"L_NC" => "N. California",
		"L_PN" => "Pacific NW",
		"L_NN" => "Nevada",
		"L_UT" => "Utah",
		"L_AK" => "Alaska",
		"L_HV" => "Hawaii",
		"L_GLOBAL" => "Global",
		"L_ACTION" => $lang['Action'],

		"U_EVENT_LIST" => append_sid("admin_fetch.$phpEx?type=x"),
		"U_SCENARIO_LIST" => append_sid("admin_fetch.$phpEx?type=s"),
		"U_ATLAS_LIST" => append_sid("admin_fetch.$phpEx?network=atlas"),
		"S_WORDS_ACTION" => append_sid("admin_event.$phpEx?mode=inject"),
		"S_HIDDEN_FIELDS" => '')
	);

	for($i = $start-1 ;($i < $event_count && $i < $start-1  + $board_config['topics_per_page']);  $i++)
	{
		$event_id = $shakemap[$i]['id'];
		$desc = $shakemap[$i]['name'];
		$network = $shakemap[$i]['network'];

		$row_color = ( !($i % 2) ) ? $theme['td_color1'] : $theme['td_color2'];
		$row_class = ( !($i % 2) ) ? $theme['td_class1'] : $theme['td_class2'];

		if ($event_id != '') {
		$template->assign_block_vars("words", array(
			"ROW_COLOR" => "#" . $row_color,
			"ROW_CLASS" => $row_class,
			"EVENT_ID" => $event_id,
			"NETWORK" => $network,
			"DESCRIPTION" => $desc,

			"U_EVENT" => "http://earthquake.usgs.gov/eqcenter/shakemap/$network/shake/$event_id",
			"U_DOWNLOAD" => append_sid("admin_fetch.$phpEx?mode=download&amp;id=$event_id&network=$network"),
			"U_INJECT" => append_sid("admin_fetch.$phpEx?mode=inject&amp;id=$event_id&network=$network"),
			"U_INJECT_SCENARIO" => append_sid("admin_fetch.$phpEx?mode=inject&amp;id=$event_id&network=$network&scenario=true"))
		);
		}
	}
	
	$template->pparse("body");
}


include('./page_footer_admin.'.$phpEx);

?>