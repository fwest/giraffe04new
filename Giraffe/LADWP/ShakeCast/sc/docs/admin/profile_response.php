<?php
/*
# $Id: profile_response.php 131 2007-08-17 21:55:08Z klin $

# The PHP interface was originally inspired by PHPBB,
# file/database formats and directory structure are quite similar.

##############################################################################
# 
# Terms and Conditions of Software Use
# ====================================
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Disclaimer of Earthquake Information
# ====================================
# 
# The data and maps provided through this system are preliminary data
# and are subject to revision. They are computer generated and may not
# have received human review or official approval. Inaccuracies in the
# data may be present because of instrument or computer
# malfunctions. Subsequent review may result in significant revisions to
# the data. All efforts have been made to provide accurate information,
# but reliance on, or interpretation of earthquake data from a single
# source is not advised. Data users are cautioned to consider carefully
# the provisional nature of the information before using it for
# decisions that concern personal or public safety or the conduct of
# business that involves substantial monetary or operational
# consequences.
# 
# Disclaimer of Software and its Capabilities
# ===========================================
# 
# This software is provided as an "as is" basis.  Attempts have been
# made to rid the program of software defects and bugs, however the
# U.S. Geological Survey (USGS) have no obligations to provide maintenance, 
# support, updates, enhancements or modifications. In no event shall USGS 
# be liable to any party for direct, indirect, special, incidental or 
# consequential damages, including lost profits, arising out of the use 
# of this software, its documentation, or data obtained though the use 
# of this software, even if USGS or have been advised of the
# possibility of such damage. By downloading, installing or using this
# program, the user acknowledges and understands the purpose and
# limitations of this software.
# 
# Contact Information
# ===================
# 
# Coordination of this effort is under the auspices of the USGS Advanced
# National Seismic System (ANSS) coordinated in Golden, Colorado, which
# functions as the clearing house for development, distribution,
# documentation, and support. For questions, comments, or reports of
# potential bugs regarding this software please contact wald@usgs.gov or
# klin@usgs.gov.  
#
#############################################################################
*/

/*if( !empty($setmodules) )
{
	$file = basename(__FILE__);
	$module['Facility_Admin']['Management'] = $file;
	return;
}
*/
define('IN_SC', 1);

//
// Load default header
//
$tester = '/shakecast/sc/bin/tester.pl';
$admin = '/shakecast/sc/bin/admin.pl';
$manage_profile = '/shakecast/sc/bin/manage_profile.pl';
$sc_root_path = "./../";
require($sc_root_path . 'extension.inc');

//$cancel = ( isset($HTTP_POST_VARS['cancel']) ) ? true : false;
//$no_page_header = $cancel;

define('IN_ADMIN', true);
// Include files
include($sc_root_path . 'common.'.$phpEx);

//
// Start session management
//
//$userdata = session_pagestart($user_ip, PAGE_INDEX);
//init_userprefs($userdata);
//
// End session management
//

/*if (!$userdata['session_logged_in'])
{
	message_die(GENERAL_MESSAGE, $lang['Not_admin']);
}
else if ($userdata['user_level'] != ADMIN)
{
	message_die(GENERAL_MESSAGE, $lang['Not_admin']);
}

if ($HTTP_GET_VARS['sid'] != $userdata['session_id'])
{
	message_die(GENERAL_MESSAGE, $lang['Not_admin']);
}

if (!$userdata['session_admin'])
{
	message_die(GENERAL_MESSAGE, $lang['Not_admin']);
}
*/
$mode = "";
if( isset($HTTP_GET_VARS['mode']) || isset($HTTP_POST_VARS['mode']) )
{
	$mode = (isset($HTTP_GET_VARS['mode'])) ? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode'];
	$mode = htmlspecialchars($mode);
}

// Restrict mode input to valid options
$mode = ( in_array($mode, array('add', 'edit', 'save', 'inject_first', 'inject_next', 'delete', 'poly')) ) ? $mode : '';

header('Content-type: text/plain;');

if( $mode == "poly")
{
	if( $mode == "poly" )
	{
		$poly = ( isset($HTTP_GET_VARS['id']) ) ? $HTTP_GET_VARS['id'] : 0;
		//$poly = htmlspecialchars($poly);

		$exe_query = "perl $manage_profile -poly ".$poly;
	//	if ($board_config['topics_per_page']) { $exe_query .= ' -limit '.$board_config['topics_per_page'];}
	//	if ($start) { $exe_query .= " -offset $start";}
		$result = exec($exe_query, $output);
		echo count($output);

/*		foreach ($output as $line) {
			echo "$line\n";
		}
*/
	}
} else {
	echo $HTTP_GET_VARS['id'];
}

//include('./page_footer_admin.'.$phpEx);

?>