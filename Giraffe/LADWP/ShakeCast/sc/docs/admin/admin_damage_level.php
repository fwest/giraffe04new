<?php
/***************************************************************************
 *                              admin_words.php
 *                            -------------------
 *   begin                : Thursday, Jul 12, 2001
 *   copyright            : (C) 2001 The phpBB Group
 *   email                : support@phpbb.com
 *
 *   $Id: admin_damage_level.php 117 2007-07-25 19:03:06Z klin $
 *
 *
 ***************************************************************************/

/***************************************************************************
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

if( !empty($setmodules) )
{
	$file = basename(__FILE__);
	$module['Facility_Admin']['Damage_Level'] = $file;
	return;
}

define('IN_SC', 1);

//
// Load default header
//
$sc_root_path = "./../";
require($sc_root_path . 'extension.inc');

$cancel = ( isset($HTTP_POST_VARS['cancel']) ) ? true : false;
$no_page_header = $cancel;

require('./pagestart.' . $phpEx);

if ($cancel)
{
	redirect('admin/' . append_sid("admin_damage_level.$phpEx", true));
}

if( isset($HTTP_GET_VARS['mode']) || isset($HTTP_POST_VARS['mode']) )
{
	$mode = (isset($HTTP_GET_VARS['mode'])) ? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode'];
	$mode = htmlspecialchars($mode);
}
else 
{
	//
	// These could be entered via a form button
	//
	if( isset($HTTP_POST_VARS['add_event']) )
	{
		$mode = "add_event";
	}
	else if( isset($HTTP_POST_VARS['save']) )
	{
		$mode = "save";
	}
	else
	{
		$mode = "";
	}
}

// Restrict mode input to valid options
$mode = ( in_array($mode, array('add', 'edit', 'save', 'delete')) ) ? $mode : '';

if( $mode != "" )
{
	if( $mode == "edit" || $mode == "add" )
	{
		$damage_level = ( isset($HTTP_GET_VARS['id']) ) ? $HTTP_GET_VARS['id'] : '';

		$template->set_filenames(array(
			"body" => "admin/damage_level_edit_body.tpl")
		);


		$facility = array('damage_level' => '', 'name' => '', 'description' => '',
			'severity_rank' => '', 'is_max_severity' => '');
		$s_hidden_fields = '';

		if( $mode == "edit" )
		{
			if( $damage_level )
			{
				$sql = "SELECT name, description, severity_rank, is_max_severity
						FROM damage_level WHERE damage_level = '".$damage_level."'";
				$result = $db->sql_query($sql);
				if( !$result )
				{
					message_die(GENERAL_ERROR, "Couldn't get facility types.", "", __LINE__, __FILE__, $sql );
				}
				$damage_level_settings = $db->sql_fetchrow($result);
				$damage_level_field = $damage_level.'<input type="hidden" name="damage_level" value="'.$damage_level.'" />';
				$hidden_fields = '<input type="hidden" name="edit" value="yes" />';
			}
			else
			{
				message_die(GENERAL_MESSAGE, "No damage level selected for editing ".$damage_level);
			}
		} else {
				$damage_level_field = '<input class="post" type="text" name="damage_level" value="" />*';
		}
		
		$template->assign_vars(array(
			"L_WORDS_TITLE" => $lang['damage_title'],
			"L_WORDS_TEXT" => $lang['damage_explain'],
			"DAMAGE_LEVEL" => $damage_level_field,
			"NAME" => $damage_level_settings['name'],
			"DESCRIPTION" => $damage_level_settings['description'],
			"SEVERITY_RANK" => $damage_level_settings['severity_rank'],
			"IS_MAX_SEVERITY" => ($damage_level_settings['is_max_severity']) ? 'checked' : '',
			"F_HIDDEN_FIELDS" => $hidden_fields,

			"L_DAMAGE_LEVEL" => "Damage Level",
			"L_NAME" => "Name",
			"L_DESCRIPTION" => "Description",
			"L_SEVERITY_RANK" => "Severity Rank",
			"L_IS_MAX_SEVERITY" => "Max Severity",
			"L_DAMAGE_EDIT" => "Damage Level Settings",

			"L_SUBMIT" => $lang['Submit'],

			"S_DAMAGE_ACTION" => append_sid("admin_damage_level.$phpEx"),
			"S_HIDDEN_FIELDS" => $s_hidden_fields)
		);

		$template->pparse("body");

		include('./page_footer_admin.'.$phpEx);
	}
	else if( $mode == "save" )
	{
		$edit = ( isset($HTTP_POST_VARS['edit']) ) ? $HTTP_POST_VARS['edit'] : '';
		$is_max_severity = ($HTTP_POST_VARS['is_max_severity']) ? 1 : 0;
		if( $edit)
		{
			$sql = "UPDATE " . DAMAGE_LEVEL_TABLE . " 
				SET name = '".str_replace("\'", "''", $HTTP_POST_VARS['name'])."', 
					description = '" . str_replace("\'", "''", $HTTP_POST_VARS['description']) . "', 
					severity_rank = " . $HTTP_POST_VARS['severity_rank'] . ", 
					is_max_severity = " . $is_max_severity . ", 
					update_username = 'webadmin', 
					update_timestamp = now() 
				WHERE damage_level = '".$HTTP_POST_VARS['damage_level']."'";
			$message = "Damage level information has been updated";
		}
		else
		{
			$sql = "INSERT INTO " . DAMAGE_LEVEL_TABLE . " 
				(damage_level, name, description, severity_rank, is_max_severity,
					update_username, update_timestamp) VALUES ('".
					str_replace("\'", "''", $HTTP_POST_VARS['damage_level'])."', '".
					str_replace("\'", "''", $HTTP_POST_VARS['name'])."', '". 
					str_replace("\'", "''", $HTTP_POST_VARS['description'])."', ".
					$HTTP_POST_VARS['severity_rank'].", ".
					$is_max_severity.", ".
					"'webadmin', now())"; 
			$message = "Damage level information has been added";
		}

		if(!$result = $db->sql_query($sql))
		{
			message_die(GENERAL_ERROR, "Could not insert data into words table", $lang['Error'], __LINE__, __FILE__, $sql);
		}
		$message .= "<br /><br />" . sprintf($lang['Click_return_damageadmin'], "<a href=\"" . append_sid("admin_damage_level.$phpEx") . "\">", "</a>") . "<br /><br />" . sprintf($lang['Click_return_admin_index'], "<a href=\"" . append_sid("index.$phpEx?pane=right") . "\">", "</a>");

		message_die(GENERAL_MESSAGE, $message);
	}
	else if( $mode == "delete" )
	{
		if( isset($HTTP_POST_VARS['id']) ||  isset($HTTP_GET_VARS['id']) )
		{
			$event_id = ( isset($HTTP_POST_VARS['id']) ) ? $HTTP_POST_VARS['id'] : $HTTP_GET_VARS['id'];
		}
		else
		{
			$event_id = 0;
		}

		$confirm = isset($HTTP_POST_VARS['confirm']);

		if( $event_id && $confirm )
		{
			$sql = "DELETE FROM " . DAMAGE_LEVEL_TABLE . " 
				WHERE damage_level = '$event_id'";

			if(!$result = $db->sql_query($sql))
			{
				message_die(GENERAL_ERROR, "Could not delete facility", $lang['Error']);
			}

			$message = $lang['damage_level_deleted'] . "<br /><br />" . sprintf($lang['Click_return_damageadmin'], "<a href=\"" . append_sid("admin_damage_level.$phpEx") . "\">", "</a>") . "<br /><br />" . sprintf($lang['Click_return_admin_index'], "<a href=\"" . append_sid("index.$phpEx?pane=right") . "\">", "</a>");

			message_die(GENERAL_MESSAGE, $message);
		}
		elseif( $event_id && !$confirm)
		{
			// Present the confirmation screen to the user
			$template->set_filenames(array(
				'body' => 'admin/confirm_body.tpl')
			);

			$hidden_fields = '<input type="hidden" name="mode" value="'.$mode.'" /><input type="hidden" name="id" value="' . $event_id . '" />';

			$template->assign_vars(array(
				'MESSAGE_TITLE' => $lang['Confirm'],
				'MESSAGE_TEXT' => $lang['Confirm_damage_level'],

				'L_YES' => $lang['Yes'],
				'L_NO' => $lang['No'],

				'S_CONFIRM_ACTION' => append_sid("admin_damage_level.$phpEx"),
				'S_HIDDEN_FIELDS' => $hidden_fields)
			);
		}
		else
		{
			message_die(GENERAL_MESSAGE, $lang['No_facility_selected']);
		}
	}
}
else
{
	$template->set_filenames(array(
		"body" => "admin/damage_level_list_body.tpl")
	);

	if ( !$facility_count )
	{
		//
		// No group members
		//
		$template->assign_block_vars('switch_no_members', array());
		$template->assign_vars(array(
			'L_NO_MEMBERS' => $lang['No_group_members'])
		);
	}

	$sql = "SELECT damage_level, name, description, severity_rank, is_max_severity 
			FROM damage_level ORDER BY severity_rank";
	if(!$result = $db->sql_query($sql))
	{
		message_die(GENERAL_ERROR, "Could not find damage level settings", $lang['Error']);
	}
	$damage_levels = $db->sql_fetchrowset($result);

	$template->assign_vars(array(
		"L_WORDS_TITLE" => $lang['damage_title'],
		"L_WORDS_TEXT" => $lang['damage_explain'],
		"L_DAMAGE_LEVEL" => "Damage Level",
		"L_NAME" => "Name",
		"L_DESCRIPTION" => "Description",
		"L_SEVERITY_RANK" => "Severity Rank",
		"L_IS_MAX_SEVERITY" => "Max Severity",
		"L_EDIT" => "Edit",
		"L_DELETE" => "Delete",
		"L_ADD_EVENT" => "Add new damage level",
		"L_ACTION" => $lang['Action'],

		"S_WORDS_ACTION" => append_sid("admin_damage_level.$phpEx?mode=add"),
		"S_HIDDEN_FIELDS" => '')
	);

	for($i = 0;$i < count($damage_levels);  $i++)
	{
		$damage_level = $damage_levels[$i]['damage_level'];
		$name = $damage_levels[$i]['name'];
		$description = $damage_levels[$i]['description'];
		$severity_rank = $damage_levels[$i]['severity_rank'];
		$is_max_severity = ($damage_levels[$i]['is_max_severity']) ? "Yes" : '';

		$row_color = ( !($i % 2) ) ? $theme['td_color1'] : $theme['td_color2'];
		$row_class = ( !($i % 2) ) ? $theme['td_class1'] : $theme['td_class2'];

		$template->assign_block_vars('words', array(
			"ROW_COLOR" => "#" . $row_color,
			"ROW_CLASS" => $row_class,
			"DAMAGE_LEVEL" => $damage_level,
			"NAME" => $name,
			"DESCRIPTION" => $description,
			"SEVERITY_RANK" => $severity_rank,
			"IS_MAX_SEVERITY" => $is_max_severity,

			"U_EDIT" => append_sid("admin_damage_level.$phpEx?mode=edit&amp;id=$damage_level"),
			"U_DELETE" => append_sid("admin_damage_level.$phpEx?mode=delete&amp;id=$damage_level"))
		);
	}
}

$template->pparse("body");

include('./page_footer_admin.'.$phpEx);

?>