<?php
/*
# $Id: admin_facility_attribute.php 176 2007-10-31 18:49:01Z klin $

# The PHP interface was originally inspired by PHPBB,
# file/database formats and directory structure are quite similar.

##############################################################################
# 
# Terms and Conditions of Software Use
# ====================================
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Disclaimer of Earthquake Information
# ====================================
# 
# The data and maps provided through this system are preliminary data
# and are subject to revision. They are computer generated and may not
# have received human review or official approval. Inaccuracies in the
# data may be present because of instrument or computer
# malfunctions. Subsequent review may result in significant revisions to
# the data. All efforts have been made to provide accurate information,
# but reliance on, or interpretation of earthquake data from a single
# source is not advised. Data users are cautioned to consider carefully
# the provisional nature of the information before using it for
# decisions that concern personal or public safety or the conduct of
# business that involves substantial monetary or operational
# consequences.
# 
# Disclaimer of Software and its Capabilities
# ===========================================
# 
# This software is provided as an "as is" basis.  Attempts have been
# made to rid the program of software defects and bugs, however the
# U.S. Geological Survey (USGS) have no obligations to provide maintenance, 
# support, updates, enhancements or modifications. In no event shall USGS 
# be liable to any party for direct, indirect, special, incidental or 
# consequential damages, including lost profits, arising out of the use 
# of this software, its documentation, or data obtained though the use 
# of this software, even if USGS or have been advised of the
# possibility of such damage. By downloading, installing or using this
# program, the user acknowledges and understands the purpose and
# limitations of this software.
# 
# Contact Information
# ===================
# 
# Coordination of this effort is under the auspices of the USGS Advanced
# National Seismic System (ANSS) coordinated in Golden, Colorado, which
# functions as the clearing house for development, distribution,
# documentation, and support. For questions, comments, or reports of
# potential bugs regarding this software please contact wald@usgs.gov or
# klin@usgs.gov.  
#
#############################################################################
*/

if( !empty($setmodules) )
{
	$file = basename(__FILE__);
	$module['Facility_Admin']['Supplemental_Attribute'] = $file;
	return;
}

define('IN_SC', 1);

//
// Load default header
//
$sc_root_path = "./../";
require($sc_root_path . 'extension.inc');

$cancel = ( isset($HTTP_POST_VARS['cancel']) ) ? true : false;
$no_page_header = $cancel;

require('./pagestart.' . $phpEx);

if ($cancel)
{
	redirect('admin/' . append_sid("admin_facility_attribute.$phpEx", true));
}

if( isset($HTTP_GET_VARS['mode']) || isset($HTTP_POST_VARS['mode']) )
{
	$mode = (isset($HTTP_GET_VARS['mode'])) ? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode'];
	$mode = htmlspecialchars($mode);
}
else 
{
	//
	// These could be entered via a form button
	//
	if( isset($HTTP_POST_VARS['add_event']) )
	{
		$mode = "add_event";
	}
	else if( isset($HTTP_POST_VARS['save']) )
	{
		$mode = "save";
	}
	else
	{
		$mode = "";
	}
}

// Restrict mode input to valid options
$mode = ( in_array($mode, array('add', 'edit', 'save', 'delete')) ) ? $mode : '';

if( $mode != "" )
{
	if( $mode == "edit" || $mode == "add" )
	{
		$facility_type_attribute_id = ( isset($HTTP_GET_VARS['id']) ) ? intval($HTTP_GET_VARS['id']) : '';

		$template->set_filenames(array(
			"body" => "admin/facility_attribute_edit_body.tpl")
		);


		$facility = array('facility_type' => '', 'attribute_name' => '', 'description' => '', 'name' => '');
		$s_hidden_fields = '';

		if( $mode == "edit" )
		{
			if( $facility_type_attribute_id )
			{
				$sql = "SELECT fta.facility_type, fta.attribute_name, fta.description, ft.name
						FROM facility_type_attribute fta INNER JOIN facility_type ft on fta.facility_type = ft.facility_type 
						WHERE fta.facility_type_attribute_id = $facility_type_attribute_id
						ORDER BY facility_type";
				$result = $db->sql_query($sql);
				if( !$result )
				{
					message_die(GENERAL_ERROR, "Couldn't get facility types.", "", __LINE__, __FILE__, $sql );
				}
				$facility_type_settings = $db->sql_fetchrow($result);
				$hidden_fields = '<input type="hidden" name="facility_type_attribute_id" value="'.$facility_type_attribute_id.'" />';
			}
			else
			{
				message_die(GENERAL_MESSAGE, "No facility type attribute selected for editing ");
			}
		}
		
		// Grab the current list of facility types
		$sql = "SELECT facility_type, name FROM facility_type";
		$result = $db->sql_query($sql);
		if( !$result )
		{
			message_die(GENERAL_ERROR, "Couldn't get facility types.", "", __LINE__, __FILE__, $sql );
		}
		$fac_type = $db->sql_fetchrowset($result);
		$fac_type_select = '<select name="facility_type">';
		if( trim($fac_type) )
		{
			for( $i = 0; $i < count($fac_type); $i++ )
			{
				if ($fac_type[$i]['facility_type'] == $facility_type_settings['facility_type']) {
					$fac_type_select .= '<option value="' . $fac_type[$i]['facility_type'] . '" selected>' . $fac_type[$i]['name'] . '</option>';
				} else {
					$fac_type_select .= '<option value="' . $fac_type[$i]['facility_type'] . '">' . $fac_type[$i]['name'] . '</option>';
				}
			}
		}
		$fac_type_select .= '</select>';

		$template->assign_vars(array(
			"L_WORDS_TITLE" => $lang['facility_type_attribute_title'],
			"L_WORDS_TEXT" => $lang['facility_type_attribute_explain'],

			"FACILITY_TYPE" => $fac_type_select,
			"ATTRIBUTE_NAME" => $facility_type_settings['attribute_name'],
			"DESCRIPTION" => $facility_type_settings['description'],
			"F_HIDDEN_FIELDS" => $hidden_fields,

			"L_FACILITY_TYPE_ATTRIBUTE_ID" => $facility_type_attribute_id,
			"L_FACILITY_TYPE" => "Facility Type",
			"L_ATTRIBUTE_NAME" => "Attribute Name",
			"L_DESCRIPTION" => "Description",
			"L_FACILITY_TYPE_EDIT" => "Facility Type Attribute",

			"L_SUBMIT" => $lang['Submit'],

			"S_FACILITY_TYPE_ACTION" => append_sid("admin_facility_attribute.$phpEx"),
			"S_HIDDEN_FIELDS" => $s_hidden_fields)
		);

		$template->pparse("body");

		include('./page_footer_admin.'.$phpEx);
	}
	else if( $mode == "save" )
	{
		$facility_type_attribute_id = ( isset($HTTP_POST_VARS['facility_type_attribute_id']) ) ? intval($HTTP_POST_VARS['facility_type_attribute_id']) : 0;
		if( $facility_type_attribute_id)
		{
			$sql = "UPDATE " . FACILITY_TYPE_ATTRIBUTE_TABLE . " 
				SET facility_type = '".str_replace("\'", "''", $HTTP_POST_VARS['facility_type'])."', 
					attribute_name = '".str_replace("\'", "''", $HTTP_POST_VARS['attribute_name'])."', 
					description = '" . str_replace("\'", "''", $HTTP_POST_VARS['description']) . "', 
					update_username = 'webadmin', 
					update_timestamp = now() 
				WHERE facility_type_attribute_id = $facility_type_attribute_id";
			$message = "Facility type attribute information has been updated";
		}
		else
		{
			$sql = "INSERT INTO " . FACILITY_TYPE_ATTRIBUTE_TABLE . " 
				(facility_type, attribute_name, description, update_username, update_timestamp) VALUES ('".
					str_replace("\'", "''", $HTTP_POST_VARS['facility_type'])."', '".
					str_replace("\'", "''", $HTTP_POST_VARS['attribute_name'])."', '". 
					str_replace("\'", "''", $HTTP_POST_VARS['description'])."', ".
					"'webadmin', now())"; 
			$message = "Facility type attribute information has been added";
		}

		if(!$result = $db->sql_query($sql))
		{
			message_die(GENERAL_ERROR, "Could not insert data into facility type attribute table", $lang['Error'], __LINE__, __FILE__, $sql);
		}
		$message .= "<br /><br />" . sprintf($lang['Click_return_ftaadmin'], "<a href=\"" . append_sid("admin_facility_attribute.$phpEx") . "\">", "</a>") . "<br /><br />" . sprintf($lang['Click_return_admin_index'], "<a href=\"" . append_sid("index.$phpEx?pane=right") . "\">", "</a>");

		message_die(GENERAL_MESSAGE, $message);
	}
	else if( $mode == "delete" )
	{
		if( isset($HTTP_POST_VARS['id']) ||  isset($HTTP_GET_VARS['id']) )
		{
			$id = ( isset($HTTP_POST_VARS['id']) ) ? $HTTP_POST_VARS['id'] : $HTTP_GET_VARS['id'];
		}
		else
		{
			$id = 0;
		}

		$confirm = isset($HTTP_POST_VARS['confirm']);

		if( $id && $confirm )
		{
			$sql = "DELETE FROM " . FACILITY_TYPE_ATTRIBUTE_TABLE . " 
				WHERE facility_type_attribute_id = '$id'";

			if(!$result = $db->sql_query($sql))
			{
				message_die(GENERAL_ERROR, "Could not delete facility", $lang['Error']);
			}

			$message = $lang['facility_attribute_deleted'] . "<br /><br />" . sprintf($lang['Click_return_ftaadmin'], "<a href=\"" . append_sid("admin_facility_attribute.$phpEx") . "\">", "</a>") . "<br /><br />" . sprintf($lang['Click_return_admin_index'], "<a href=\"" . append_sid("index.$phpEx?pane=right") . "\">", "</a>");

			message_die(GENERAL_MESSAGE, $message);
		}
		elseif( $id && !$confirm)
		{
			// Present the confirmation screen to the user
			$template->set_filenames(array(
				'body' => 'admin/confirm_body.tpl')
			);

			$hidden_fields = '<input type="hidden" name="mode" value="'.$mode.'" /><input type="hidden" name="id" value="' . $id . '" />';

			$template->assign_vars(array(
				'MESSAGE_TITLE' => $lang['Confirm'],
				'MESSAGE_TEXT' => $lang['Confirm_attribute_deleted'],

				'L_YES' => $lang['Yes'],
				'L_NO' => $lang['No'],

				'S_CONFIRM_ACTION' => append_sid("admin_facility_attribute.$phpEx"),
				'S_HIDDEN_FIELDS' => $hidden_fields)
			);
		}
		else
		{
			message_die(GENERAL_MESSAGE, $lang['no_attribute_selected']);
		}
	}
}
else
{
	$template->set_filenames(array(
		"body" => "admin/facility_type_list_body.tpl")
	);

	if ( !$facility_count )
	{
		//
		// No group members
		//
		$template->assign_block_vars('switch_no_members', array());
		$template->assign_vars(array(
			'L_NO_MEMBERS' => $lang['No_group_members'])
		);
	}

	$sql = "SELECT fta.facility_type_attribute_id, fta.facility_type, fta.attribute_name, fta.description, ft.name
			FROM facility_type_attribute fta INNER JOIN facility_type ft on fta.facility_type = ft.facility_type 
			ORDER BY facility_type";
	if(!$result = $db->sql_query($sql))
	{
		message_die(GENERAL_ERROR, "Could not find damage level settings", $lang['Error']);
	}
	$facility_types = $db->sql_fetchrowset($result);

	$template->assign_vars(array(
		"L_WORDS_TITLE" => $lang['facility_type_attribute_title'],
		"L_WORDS_TEXT" => $lang['facility_type_attribute_explain'],
		"L_FACILITY_TYPE" => "Facility Type",
		"L_ATTRIBUTE_NAME" => "Attribute Name",
		"L_DESCRIPTION" => "Description",

		"L_EDIT" => "Edit",
		"L_DELETE" => "Delete",
		"L_ADD_EVENT" => "Add new attribute",
		"L_ACTION" => $lang['Action'],

		"S_WORDS_ACTION" => append_sid("admin_facility_attribute.$phpEx?mode=add"),
		"S_HIDDEN_FIELDS" => '')
	);

	for($i = 0;$i < count($facility_types);  $i++)
	{
		$facility_type_attribute_id = $facility_types[$i]['facility_type_attribute_id'];
		$facility_type = $facility_types[$i]['name'];
		$attribute_name = $facility_types[$i]['attribute_name'];
		$description = $facility_types[$i]['description'];

		$row_color = ( !($i % 2) ) ? $theme['td_color1'] : $theme['td_color2'];
		$row_class = ( !($i % 2) ) ? $theme['td_class1'] : $theme['td_class2'];

		$template->assign_block_vars('words', array(
			"ROW_COLOR" => "#" . $row_color,
			"ROW_CLASS" => $row_class,
			"FACILITY_TYPE" => $facility_type,
			"ATTRIBUTE_NAME" => $attribute_name,
			"DESCRIPTION" => $description,

			"U_EDIT" => append_sid("admin_facility_attribute.$phpEx?mode=edit&amp;id=$facility_type_attribute_id"),
			"U_DELETE" => append_sid("admin_facility_attribute.$phpEx?mode=delete&amp;id=$facility_type_attribute_id"))
		);
	}
}

$template->pparse("body");

include('./page_footer_admin.'.$phpEx);

?>