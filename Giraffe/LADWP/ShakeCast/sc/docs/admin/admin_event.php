<?php
/*
# $Id: admin_event.php 511 2008-10-20 14:51:19Z klin $

# The PHP interface was originally inspired by PHPBB,
# file/database formats and directory structure are quite similar.

##############################################################################
# 
# Terms and Conditions of Software Use
# ====================================
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Disclaimer of Earthquake Information
# ====================================
# 
# The data and maps provided through this system are preliminary data
# and are subject to revision. They are computer generated and may not
# have received human review or official approval. Inaccuracies in the
# data may be present because of instrument or computer
# malfunctions. Subsequent review may result in significant revisions to
# the data. All efforts have been made to provide accurate information,
# but reliance on, or interpretation of earthquake data from a single
# source is not advised. Data users are cautioned to consider carefully
# the provisional nature of the information before using it for
# decisions that concern personal or public safety or the conduct of
# business that involves substantial monetary or operational
# consequences.
# 
# Disclaimer of Software and its Capabilities
# ===========================================
# 
# This software is provided as an "as is" basis.  Attempts have been
# made to rid the program of software defects and bugs, however the
# U.S. Geological Survey (USGS) have no obligations to provide maintenance, 
# support, updates, enhancements or modifications. In no event shall USGS 
# be liable to any party for direct, indirect, special, incidental or 
# consequential damages, including lost profits, arising out of the use 
# of this software, its documentation, or data obtained though the use 
# of this software, even if USGS or have been advised of the
# possibility of such damage. By downloading, installing or using this
# program, the user acknowledges and understands the purpose and
# limitations of this software.
# 
# Contact Information
# ===================
# 
# Coordination of this effort is under the auspices of the USGS Advanced
# National Seismic System (ANSS) coordinated in Golden, Colorado, which
# functions as the clearing house for development, distribution,
# documentation, and support. For questions, comments, or reports of
# potential bugs regarding this software please contact wald@usgs.gov or
# klin@usgs.gov.  
#
#############################################################################
*/

if( !empty($setmodules) )
{
	$file = basename(__FILE__);
	$module['Event_Admin']['Processed Event'] = $file;
	return;
}

define('IN_SC', 1);
@set_time_limit(120);

//
// Load default header
//
$tester = '/shakecast/sc/bin/tester.pl';
$manage_event = '/shakecast/sc/bin/manage_event.pl';
$sc_root_path = "./../";
require($sc_root_path . 'extension.inc');

$cancel = ( isset($HTTP_POST_VARS['cancel']) ) ? true : false;
$no_page_header = $cancel;

require('./pagestart.' . $phpEx);

if ($cancel)
{
	redirect('admin/' . append_sid("admin_event.$phpEx", true));
}

if( isset($HTTP_GET_VARS['mode']) || isset($HTTP_POST_VARS['mode']) )
{
	$mode = (isset($HTTP_GET_VARS['mode'])) ? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode'];
	$mode = htmlspecialchars($mode);
}
else 
{
	//
	// These could be entered via a form button
	//
	if( isset($HTTP_POST_VARS['add_event']) )
	{
		$mode = "add_event";
	}
	else if( isset($HTTP_POST_VARS['save']) )
	{
		$mode = "save";
	}
	else
	{
		$mode = "";
	}
}

// Restrict mode input to valid options
$mode = ( in_array($mode, array('add', 'info', 'comment', 'realert', 'delete', 'add_event', 'inject')) ) ? $mode : '';

if( $mode != "" )
{
	if( $mode == "edit" || $mode == "add" )
	{
		$word_id = ( isset($HTTP_GET_VARS['id']) ) ? intval($HTTP_GET_VARS['id']) : 0;

		$template->set_filenames(array(
			"body" => "admin/words_edit_body.tpl")
		);

		$word_info = array('word' => '', 'replacement' => '');
		$s_hidden_fields = '';

		if( $mode == "edit" )
		{
			if( $word_id )
			{
				$sql = "SELECT * 
					FROM " . WORDS_TABLE . " 
					WHERE word_id = $word_id";
				if(!$result = $db->sql_query($sql))
				{
					message_die(GENERAL_ERROR, "Could not query words table", "Error", __LINE__, __FILE__, $sql);
				}

				$word_info = $db->sql_fetchrow($result);
				$s_hidden_fields .= '<input type="hidden" name="id" value="' . $word_id . '" />';
			}
			else
			{
				message_die(GENERAL_MESSAGE, $lang['No_word_selected']);
			}
		}

		$template->assign_vars(array(
			"WORD" => $word_info['word'],
			"REPLACEMENT" => $word_info['replacement'],

			"L_WORDS_TITLE" => $lang['test_title'],
			"L_WORDS_TEXT" => $lang['test_explain'],
			"L_WORD_CENSOR" => $lang['Edit_word_censor'],
			"L_WORD" => $lang['Word'],
			"L_REPLACEMENT" => $lang['Replacement'],
			"L_SUBMIT" => $lang['Submit'],

			"S_WORDS_ACTION" => append_sid("admin_words.$phpEx"),
			"S_HIDDEN_FIELDS" => $s_hidden_fields)
		);

		$template->pparse("body");

		include('./page_footer_admin.'.$phpEx);
	}
	else if( $mode == "save" )
	{
		$word_id = ( isset($HTTP_POST_VARS['id']) ) ? intval($HTTP_POST_VARS['id']) : 0;
		$word = ( isset($HTTP_POST_VARS['word']) ) ? trim($HTTP_POST_VARS['word']) : "";
		$replacement = ( isset($HTTP_POST_VARS['replacement']) ) ? trim($HTTP_POST_VARS['replacement']) : "";

		if($word == "" || $replacement == "")
		{
			message_die(GENERAL_MESSAGE, $lang['Must_enter_word']);
		}

		if( $word_id )
		{
			$sql = "UPDATE " . WORDS_TABLE . " 
				SET word = '" . str_replace("\'", "''", $word) . "', replacement = '" . str_replace("\'", "''", $replacement) . "' 
				WHERE word_id = $word_id";
			$message = $lang['Word_updated'];
		}
		else
		{
			$sql = "INSERT INTO " . WORDS_TABLE . " (word, replacement) 
				VALUES ('" . str_replace("\'", "''", $word) . "', '" . str_replace("\'", "''", $replacement) . "')";
			$message = $lang['Word_added'];
		}

		if(!$result = $db->sql_query($sql))
		{
			message_die(GENERAL_ERROR, "Could not insert data into words table", $lang['Error'], __LINE__, __FILE__, $sql);
		}

		$message .= "<br /><br />" . sprintf($lang['Click_return_wordadmin'], "<a href=\"" . append_sid("admin_words.$phpEx") . "\">", "</a>") . "<br /><br />" . sprintf($lang['Click_return_admin_index'], "<a href=\"" . append_sid("index.$phpEx?pane=right") . "\">", "</a>");

		message_die(GENERAL_MESSAGE, $message);
	}
	else if( $mode == "info")
	{
		if( isset($HTTP_POST_VARS['id']) ||  isset($HTTP_GET_VARS['id']) )
		{
			$event_id = ( isset($HTTP_POST_VARS['id']) ) ? $HTTP_POST_VARS['id'] : $HTTP_GET_VARS['id'];
		}
		else
		{
			message_die(GENERAL_MESSAGE, $lang['No_event_selected']);
		}

		$sql = "SELECT grid_id FROM grid WHERE shakemap_id = \"$event_id\"";
		if ( !($result = $db->sql_query($sql)) )
		{
			message_die(GENERAL_ERROR, 'Could not find ShakeCast event', '', __LINE__, __FILE__, $sql);
		}
	
		if ( $row = $db->sql_fetchrow($result) )
		{
			$grid_sql = "or n.grid_id = ".$row['grid_id'];
		}
		$sql = "SELECT DISTINCT su.email_address 
				FROM shakecast_user su INNER JOIN notification n on su.shakecast_user = n.shakecast_user 
				WHERE n.event_id = \"$event_id\" $grid_sql";
		if ( !($result = $db->sql_query($sql)) )
		{
			message_die(GENERAL_ERROR, 'Could not select group members', '', __LINE__, __FILE__, $sql);
		}
		$template->set_filenames(array(
			"body" => "admin/event_summary_body.tpl")
		);
	
		$result = exec('perl '.$tester.' -type new_test', $output);
		$field_name = explode("::", $output[0]);
		$event_count = count($output);
	
		$template->assign_vars(array(
			"L_WORDS_TITLE" => "ShakeCast Processed Event List",
			"L_WORDS_TEXT" => $lang['test_explain'],
			"L_EVENT_ID" => $lang[$field_name[0]],
			"L_EVENT_TIMESTAMP" => $lang[$field_name[1]],
			"L_MAGNITUDE" => $lang[$field_name[2]],
			"L_LATITUDE" => $lang[$field_name[3]],
			"L_LONGITUDE" => $lang[$field_name[4]],
			"L_DESCRIPTION" => $lang[$field_name[5]],
			"L_REALERT" => "Re-Alert",
			"L_COMMENT" => "Comment",
			"L_DELETE" => "Delete",
			"L_ADD_EVENT" => "Inject ShakeMap Event",
			"L_ACTION" => $lang['Action'],
	
			"S_WORDS_ACTION" => append_sid("admin_event.$phpEx"),
			"S_HIDDEN_FIELDS" => '')
		);
	
		for($i = $event_count -1; $i >= 1; $i--)
		{
			$fields = explode("::", $output[$i]);
			$event_id = $fields[0];
			$event_timestamp = $fields[1];
			$magnitude = $fields[2];
			$lat = $fields[3];
			$lon = $fields[4];
			$desc = $fields[5];
	
			$row_color = ( !($i % 2) ) ? $theme['td_color1'] : $theme['td_color2'];
			$row_class = ( !($i % 2) ) ? $theme['td_class1'] : $theme['td_class2'];
	
			$template->assign_block_vars("words", array(
				"ROW_COLOR" => "#" . $row_color,
				"ROW_CLASS" => $row_class,
				"EVENT_ID" => $event_id,
				"EVENT_TIMESTAMP" => $event_timestamp,
				"MAGNITUDE" => $magnitude,
				"LATITUDE" => $lat,
				"LONGITUDE" => $lon,
				"DESCRIPTION" => $desc,
	
				"U_EVENT" => append_sid("admin_event.$phpEx?mode=info&amp;id=$event_id"),
				"U_REALERT" => append_sid("admin_event.$phpEx?mode=realert&amp;id=$event_id"),
				"U_COMMENT" => append_sid("admin_event.$phpEx?mode=comment&amp;id=$event_id"),
				"U_DELETE" => append_sid("admin_event.$phpEx?mode=delete&amp;id=$event_id"))
			);
		}
	
	}
	else if( $mode == "delete" || $mode == "realert")
	{
		if( isset($HTTP_POST_VARS['id']) ||  isset($HTTP_GET_VARS['id']) )
		{
			$event_id = ( isset($HTTP_POST_VARS['id']) ) ? $HTTP_POST_VARS['id'] : $HTTP_GET_VARS['id'];
		}
		else
		{
			$event_id = 0;
		}

		$confirm = isset($HTTP_POST_VARS['confirm']); 

		if( $event_id && $confirm )
		{
			$result = exec('perl '.$manage_event.' -'.$mode.' '.$event_id, $output);

			if(!$result)
			{
				message_die(GENERAL_ERROR, "Could not ".$mode." event", $lang['Error']);
			}
			
			$dir = @opendir($board_config['DataRoot']);
			
			while( $file = @readdir($dir) )
			{
				if( preg_match("/^$event_id\-*\d*/", $file) )
				{
					remove_directory($board_config['DataRoot'].'/'.$file);
					rmdir($board_config['DataRoot'].'/'.$file);
				}
			}
			
			@closedir($dir);

			$message = $lang['event_'.$mode] . "<br /><br />" . sprintf($lang['Click_return_eventadmin'], "<a href=\"" . append_sid("admin_event.$phpEx") . "\">", "</a>") . "<br /><br />" . sprintf($lang['Click_return_admin_index'], "<a href=\"" . append_sid("index.$phpEx?pane=right") . "\">", "</a>");

			message_die(GENERAL_MESSAGE, $message);
		}
		elseif( $event_id && !$confirm)
		{
			// Present the confirmation screen to the user
			$template->set_filenames(array(
				'body' => 'admin/confirm_body.tpl')
			);

			$hidden_fields = '<input type="hidden" name="mode" value="'.$mode.'" /><input type="hidden" name="id" value="' . $event_id . '" />';

			$template->assign_vars(array(
				'MESSAGE_TITLE' => $lang['Confirm'],
				'MESSAGE_TEXT' => $lang['confirm_event_'.$mode],

				'L_YES' => $lang['Yes'],
				'L_NO' => $lang['No'],

				'S_CONFIRM_ACTION' => append_sid("admin_event.$phpEx"),
				'S_HIDDEN_FIELDS' => $hidden_fields)
			);
		}
		else
		{
			message_die(GENERAL_MESSAGE, $lang['No_event_selected']);
		}
	}
	else if( $mode == "comment")
	{
		if( isset($HTTP_POST_VARS['id']) ||  isset($HTTP_GET_VARS['id']) )
		{
			$event_id = ( isset($HTTP_POST_VARS['id']) ) ? $HTTP_POST_VARS['id'] : $HTTP_GET_VARS['id'];
		}
		else
		{
			$event_id = 0;
		}
		$subject =  'Re: ShakeCast Notification for Event '.$event_id;

		$sql = "SELECT post_subject, post_text FROM " . POSTS_TEXT_TABLE . " WHERE shakemap_id = \"$event_id\"";
		if (($result = $db->sql_query($sql)) )
		{
			if ($row = $db->sql_fetchrow($result)) {
				$db->sql_freeresult($result);
				$subject = $row['post_subject'];
				$message = $row['post_text'];
			}
		} 

		if( isset($HTTP_POST_VARS['message']) )
		{
			$message = stripslashes(trim($HTTP_POST_VARS['message']));
		}

		if( isset($HTTP_POST_VARS['subject']) )
		{
			$subject = stripslashes(trim($HTTP_POST_VARS['subject']));
		} 

		if ( isset($HTTP_POST_VARS['submit']) && isset($HTTP_POST_VARS['comment_email']) )
		{
			$error = FALSE;
			$error_msg = '';
		
			if ( empty($subject) )
			{
				$error = true;
				$error_msg .= ( !empty($error_msg) ) ? '<br />' . $lang['Empty_subject'] : $lang['Empty_subject'];
			}
		
			if ( empty($message) )
			{
				$error = true;
				$error_msg .= ( !empty($error_msg) ) ? '<br />' . $lang['Empty_message'] : $lang['Empty_message'];
			}
		
			$sql = "SELECT grid_id FROM grid WHERE shakemap_id = \"$event_id\"";
			if ( !($result = $db->sql_query($sql)) )
			{
				message_die(GENERAL_ERROR, 'Could not find ShakeCast event', '', __LINE__, __FILE__, $sql);
			}
		
			if ( $row = $db->sql_fetchrow($result) )
			{
				$grid_sql = "or n.grid_id = ".$row['grid_id'];
			}
			$sql = "SELECT DISTINCT su.email_address 
					FROM shakecast_user su INNER JOIN notification n on su.shakecast_user = n.shakecast_user 
					WHERE n.event_id = \"$event_id\" $grid_sql";
			if ( !($result = $db->sql_query($sql)) )
			{
				message_die(GENERAL_ERROR, 'Could not select group members', '', __LINE__, __FILE__, $sql);
			}
		
			if ( $row = $db->sql_fetchrow($result) )
			{
				$bcc_list = array();
				do
				{
					$bcc_list[] = $row['email_address'];
				}
				while ( $row = $db->sql_fetchrow($result) );
		
				$db->sql_freeresult($result);
			}
			else
			{
				$message = ( $group_id != -1 ) ? $lang['Group_not_exist'] : $lang['No_such_user'];
		
				$error = true;
				$error_msg .= ( !empty($error_msg) ) ? '<br />' . $message : $message;
			}
		
			if ( !$error )
			{
				include($sc_root_path . 'includes/emailer.'.$phpEx);
		
				//
				// Let's do some checking to make sure that mass mail functions
				// are working in win32 versions of php.
				//
				if ( preg_match('/[c-z]:\\\.*/i', getenv('PATH')) && !$board_config['smtp_delivery'])
				{
					$ini_val = ( @phpversion() >= '4.0.0' ) ? 'ini_get' : 'get_cfg_var';
		
					// We are running on windows, force delivery to use our smtp functions
					// since php's are broken by default
					$board_config['smtp_delivery'] = 1;
					$board_config['smtp_host'] = @$ini_val('SMTP');
				}
		
				$emailer = new emailer($board_config['smtp_delivery']);
			
				$emailer->from($board_config['board_email']);
				$emailer->replyto($board_config['board_email']);
		
				for ($i = 0; $i < count($bcc_list); $i++)
				{
					$emailer->bcc($bcc_list[$i]);
				}
		
				$email_headers = 'X-AntiAbuse: Board servername - ' . $board_config['server_name'] . "\n";
				$email_headers .= 'X-AntiAbuse: User_id - ' . $userdata['user_id'] . "\n";
				$email_headers .= 'X-AntiAbuse: Username - ' . $userdata['username'] . "\n";
				$email_headers .= 'X-AntiAbuse: User IP - ' . decode_ip($user_ip) . "\n";
		
				$emailer->use_template('admin_send_email');
				$emailer->email_address($board_config['board_email']);
				$emailer->set_subject($subject);
				$emailer->extra_headers($email_headers);
		
				$emailer->assign_vars(array(
					'SITENAME' => $board_config['sitename'], 
					'BOARD_EMAIL' => $board_config['board_email'], 
					'MESSAGE' => $message)
				);
				$emailer->send();
				$emailer->reset();
		
				message_die(GENERAL_MESSAGE, $lang['Email_sent'] . '<br /><br />' . sprintf($lang['Click_return_admin_index'],  '<a href="' . append_sid("index.$phpEx?pane=right") . '">', '</a>'));
			}
		}	
		else if ( isset($HTTP_POST_VARS['submit']) && isset($HTTP_POST_VARS['comment_web']) )
		{
			$error = FALSE;
			$error_msg = '';
		
			if ( empty($subject) )
			{
				$error = true;
				$error_msg .= ( !empty($error_msg) ) ? '<br />' . $lang['Empty_subject'] : $lang['Empty_subject'];
			}
		
			if ( empty($message) )
			{
				$error = true;
				$error_msg .= ( !empty($error_msg) ) ? '<br />' . $lang['Empty_message'] : $lang['Empty_message'];
			}
		
			$sql = "SELECT grid_id FROM " . GRID_TABLE . " WHERE shakemap_id = \"$event_id\"";
			if ( !($result = $db->sql_query($sql)) )
			{
				message_die(GENERAL_ERROR, 'Could not find ShakeCast event', '', __LINE__, __FILE__, $sql);
			}
		
			$sql = "SELECT post_id FROM " . POSTS_TEXT_TABLE . " WHERE shakemap_id = \"$event_id\"";
			if ( !($result = $db->sql_query($sql)) )
			{
				message_die(GENERAL_ERROR, 'Could not find ShakeCast event', '', __LINE__, __FILE__, $sql);
			}
		
			if ( $row = $db->sql_fetchrow($result) )
			{
				$post_id = $row['post_id'];
				$sql = "UPDATE " . POSTS_TEXT_TABLE . " 
						SET post_subject = '" . $subject ."', 
							post_text = '" . $message ."'
						WHERE shakemap_id = '" . $event_id ."'";
			} 
			else
			{
				$post_id = $row['post_id'];
				$sql = "INSERT INTO " . POSTS_TEXT_TABLE . " 
						(shakemap_id, post_subject, post_text) VALUES ('" . $event_id ."', 
						 '" . $subject ."', '" . $message ."')";
			} 

			if ( !($result = $db->sql_query($sql)) )
			{
				message_die(GENERAL_ERROR, 'Could not insert/update comment message', '', __LINE__, __FILE__, $sql);
			}
		
			message_die(GENERAL_MESSAGE, $lang['Comment_sent'] . '<br /><br />' . sprintf($lang['Click_return_admin_index'],  '<a href="' . append_sid("index.$phpEx?pane=right") . '">', '</a>'));
		}	
		
		if ( $error )
		{
			$template->set_filenames(array(
				'reg_header' => 'error_body.tpl')
			);
			$template->assign_vars(array(
				'ERROR_MESSAGE' => $error_msg)
			);
			$template->assign_var_from_handle('ERROR_BOX', 'reg_header');
		}

		//
		// Generate page
		//
		$template->set_filenames(array(
			'body' => 'admin/user_email_body.tpl')
		);
		
		$template->assign_vars(array(
			'MESSAGE' => $message,
			'SUBJECT' => $subject, 
		
			'L_EMAIL_TITLE' => $lang['Comment'],
			'L_EMAIL_EXPLAIN' => $lang['Comment_explain'],
			'L_COMPOSE' => $lang['Compose'],
			'L_RECIPIENTS' => $lang['Recipients'],
			'L_EMAIL_SUBJECT' => $lang['Subject'],
			'L_EMAIL_MSG' => $lang['Message'],
			'L_EMAIL' => $lang['Submit'],
			'L_NOTICE' => $notice,
		
			'S_USER_ACTION' => append_sid("admin_event.$phpEx?mode=comment&amp;id=$event_id"),
			'S_GROUP_SELECT' => 'Notified ShakeCast Users for this event')
		);
		
		$template->assign_block_vars('topicrow', array(
			'ROW_COLOR' => ($i & 1),
			'ROW_CLASS' => $row_class)
		);
	}
}
else
{
	$template->set_filenames(array(
		"body" => "admin/event_list_body.tpl")
	);

	$event_count = get_db_stat('eventcount');
	if ( !$event_count )
	{
		//
		// No group members
		//
		$template->assign_block_vars('switch_no_members', array());
		$template->assign_vars(array(
			'L_NO_MEMBERS' => $lang['No_group_members'])
		);
	}

	if (isset($HTTP_GET_VARS['sort_key']))
	{
		$sort_key = $HTTP_GET_VARS['sort_key'];
	}
	else 
	{
		$sort_key = 'event_timestamp';
	}
	$sort_order = ( isset($HTTP_GET_VARS['sort_order']) ) ? $HTTP_GET_VARS['sort_order'] : 'd';
	$sort_sql = ( $sort_order == 'a' ) ? 'asc' : 'desc';
	
	if( isset($HTTP_POST_VARS['start']) ||  isset($HTTP_GET_VARS['start']) )
	{
		$start = ( isset($HTTP_POST_VARS['start']) ) ? $HTTP_POST_VARS['start'] : $HTTP_GET_VARS['start'];
	}
	$current_page = ( !$event_count ) ? 1 : ceil( $event_count / $board_config['topics_per_page'] );
	$template->assign_vars(array(
		'PAGINATION' => generate_pagination("admin_event.$phpEx?" . POST_GROUPS_URL . "=$group_id&sort_key=$sort_key&sort_order=$sort_order", $event_count, $board_config['topics_per_page'], $start),
		'PAGE_NUMBER' => sprintf($lang['Page_of'], ( floor( $start / $board_config['topics_per_page'] ) + 1 ), $current_page ), 

		'L_GOTO_PAGE' => $lang['Goto_page'])
	);
	$exe_query = "perl $tester -type new_test " . ' -order "' . $sort_key . ' '. $sort_sql . '"';
	if ($board_config['topics_per_page']) { $exe_query .= ' -limit '.$board_config['topics_per_page'];}
	if ($start) { $exe_query .= " -offset $start";}

	$result = exec($exe_query, $output);
	$event_count_display = count($output);
	$field_name = explode("::", $output[0]);

	$new_sort_order = ($sort_order == 'd') ? 'a' : 'd';
	$img_url = ' <img src="/images/' . $sort_order . '.png" border="0" width="10" height="10">';
	$base_url = append_sid("admin_event.$phpEx?" . POST_GROUPS_URL . "=$group_id");

	$template->assign_vars(array(
		"L_WORDS_TITLE" => "ShakeCast Processed Event List",
		"L_WORDS_TEXT" => $lang['test_explain'],
		"L_EVENT_ID" => $lang[$field_name[0]] . (($sort_key == 'event_id') ? $img_url : ''),
		"L_EVENT_TIMESTAMP" => $lang[$field_name[2]] . (($sort_key == 'event_timestamp') ? $img_url : ''),
		"L_MAGNITUDE" => $lang[$field_name[3]] . (($sort_key == 'magnitude') ? $img_url : ''),
		"L_LATITUDE" => $lang[$field_name[4]] . (($sort_key == 'lat') ? $img_url : ''),
		"L_LONGITUDE" => $lang[$field_name[5]] . (($sort_key == 'lon') ? $img_url : ''),
		"L_DESCRIPTION" => $lang[$field_name[6]] . (($sort_key == 'event_location_description') ? $img_url : ''),
		"L_REALERT" => "Re-Alert",
		"L_COMMENT" => "Comment",
		"L_DELETE" => "Delete",
		"L_ADD_EVENT" => "Inject ShakeMap Event",
		"L_ACTION" => $lang['Action'],

		'U_EVENT_ID' =>  $base_url."&sort_key=event_id&sort_order=" . (($sort_key == 'event_id') ? $new_sort_order : $sort_order),
		'U_EVENT_TIMESTAMP' =>  $base_url."&sort_key=event_timestamp&sort_order=" . (($sort_key == 'event_timestamp') ? $new_sort_order : $sort_order),
		'U_MAGNITUDE' =>  $base_url."&sort_key=magnitude&sort_order=" . (($sort_key == 'magnitude') ? $new_sort_order : $sort_order),
		'U_LATITUDE' =>  $base_url."&sort_key=lat&sort_order=" . (($sort_key == 'lat') ? $new_sort_order : $sort_order),
		'U_LONGITUDE' =>  $base_url."&sort_key=lon&sort_order=" . (($sort_key == 'lon') ? $new_sort_order : $sort_order),
		'U_DESCRIPTION' =>  $base_url."&sort_key=event_location_description&sort_order=" . (($sort_key == 'event_location_description') ? $new_sort_order : $sort_order),

		"S_WORDS_ACTION" => append_sid("admin_event.$phpEx?mode=inject"),
		"S_HIDDEN_FIELDS" => '')
	);

	for($i = 1;$i < $event_count_display;  $i++)
//	for($i = $event_count -1; $i >= 1; $i--)
	{
		$fields = explode("::", $output[$i]);
		$event_id = $fields[0];
		$event_version = $fields[1];
		$event_timestamp = $fields[2];
		$magnitude = $fields[3];
		$lat = $fields[4];
		$lon = $fields[5];
		$desc = $fields[6];

		$row_color = ( !($i % 2) ) ? $theme['td_color1'] : $theme['td_color2'];
		$row_class = ( !($i % 2) ) ? $theme['td_class1'] : $theme['td_class2'];

		$template->assign_block_vars("words", array(
			"ROW_COLOR" => "#" . $row_color,
			"ROW_CLASS" => $row_class,
			"EVENT_ID" => $event_id . ' - ' . $event_version,
			"EVENT_TIMESTAMP" => $event_timestamp,
			"MAGNITUDE" => $magnitude,
			"LATITUDE" => $lat,
			"LONGITUDE" => $lon,
			"DESCRIPTION" => $desc,

			"U_EVENT" => append_sid("../event.$phpEx?f=2&amp;event=$event_id&amp;version=$event_version"),
			"U_REALERT" => append_sid("admin_event.$phpEx?mode=realert&amp;id=$event_id"),
			"U_COMMENT" => append_sid("admin_event.$phpEx?mode=comment&amp;id=$event_id"),
			"U_DELETE" => append_sid("admin_event.$phpEx?mode=delete&amp;id=$event_id"))
		);
	}
}

$template->pparse("body");

include('./page_footer_admin.'.$phpEx);

?>