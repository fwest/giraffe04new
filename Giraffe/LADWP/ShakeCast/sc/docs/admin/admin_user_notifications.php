<?php
/*
# $Id: admin_user_notifications.php 458 2008-08-25 14:44:46Z klin $

# The PHP interface was originally inspired by PHPBB,
# file/database formats and directory structure are quite similar.

##############################################################################
# 
# Terms and Conditions of Software Use
# ====================================
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Disclaimer of Earthquake Information
# ====================================
# 
# The data and maps provided through this system are preliminary data
# and are subject to revision. They are computer generated and may not
# have received human review or official approval. Inaccuracies in the
# data may be present because of instrument or computer
# malfunctions. Subsequent review may result in significant revisions to
# the data. All efforts have been made to provide accurate information,
# but reliance on, or interpretation of earthquake data from a single
# source is not advised. Data users are cautioned to consider carefully
# the provisional nature of the information before using it for
# decisions that concern personal or public safety or the conduct of
# business that involves substantial monetary or operational
# consequences.
# 
# Disclaimer of Software and its Capabilities
# ===========================================
# 
# This software is provided as an "as is" basis.  Attempts have been
# made to rid the program of software defects and bugs, however the
# U.S. Geological Survey (USGS) have no obligations to provide maintenance, 
# support, updates, enhancements or modifications. In no event shall USGS 
# be liable to any party for direct, indirect, special, incidental or 
# consequential damages, including lost profits, arising out of the use 
# of this software, its documentation, or data obtained though the use 
# of this software, even if USGS or have been advised of the
# possibility of such damage. By downloading, installing or using this
# program, the user acknowledges and understands the purpose and
# limitations of this software.
# 
# Contact Information
# ===================
# 
# Coordination of this effort is under the auspices of the USGS Advanced
# National Seismic System (ANSS) coordinated in Golden, Colorado, which
# functions as the clearing house for development, distribution,
# documentation, and support. For questions, comments, or reports of
# potential bugs regarding this software please contact wald@usgs.gov or
# klin@usgs.gov.  
#
#############################################################################
*/

define('IN_SC', 1);

if( !empty($setmodules) )
{
	$filename = basename(__FILE__);
	$module['Users']['Notification Request'] = $filename;

	return;
}

$sc_root_path = './../';
$update_password = '/shakecast/sc/bin/update_password.pl';
require($sc_root_path . 'extension.inc');
require('./pagestart.' . $phpEx);
require($sc_root_path . 'includes/functions_validate.'.$phpEx);

$html_entities_match = array('#<#', '#>#');
$html_entities_replace = array('&lt;', '&gt;');

//
// Set mode
//
if( isset( $HTTP_POST_VARS['mode'] ) || isset( $HTTP_GET_VARS['mode'] ) )
{
	$mode = ( isset( $HTTP_POST_VARS['mode']) ) ? $HTTP_POST_VARS['mode'] : $HTTP_GET_VARS['mode'];
	$mode = htmlspecialchars($mode);
}
else
{
	$mode = '';
}

if( isset( $HTTP_POST_VARS['username'] ) || isset( $HTTP_GET_VARS['username'] ) )
{
	$username = ( isset( $HTTP_POST_VARS['username']) ) ? $HTTP_POST_VARS['username'] : $HTTP_GET_VARS['username'];
	$username = htmlspecialchars($username);
}
else
{
	$username = '';
}

if( isset( $HTTP_POST_VARS['nrtype'] ) || isset( $HTTP_GET_VARS['nrtype'] ) )
{
	$nrtype = ( isset( $HTTP_POST_VARS['nrtype']) ) ? $HTTP_POST_VARS['nrtype'] : $HTTP_GET_VARS['nrtype'];
	$nrtype = htmlspecialchars($nrtype);
}
else
{
	$nrtype = '';
}

if( isset( $HTTP_POST_VARS['nkey'] ) || isset( $HTTP_GET_VARS['nkey'] ) )
{
	$nkey = ( isset( $HTTP_POST_VARS['nkey']) ) ? $HTTP_POST_VARS['nkey'] : $HTTP_GET_VARS['nkey'];
}
else
{
	$nkey = '';
}
@set_time_limit(1200);

//
// Begin program
//
// Restrict mode input to valid options
$mode = ( in_array($mode, array('add', 'edit', 'save', 'delete')) ) ? $mode : '';

if( $mode != "" )
{
	if ( $mode == 'edit' || $mode == 'add' )
	{
		if( $username != '' )
		{
			$this_userdata = get_userdata($username, true);
			if( !$this_userdata )
			{
				message_die(GENERAL_MESSAGE, $lang['No_user_id_specified'] );
			}
	
			//
			// Now parse and display it as a template
			//
			$user_id = $this_userdata['user_id'];
			$username = $this_userdata['username'];
			$email = $this_userdata['user_email'];
			$fullname = htmlspecialchars($this_userdata['user_fullname']);
		}
	
		$dl_types = get_damage_level();
		$et_types = get_event_type();
		$m_types = get_metric_type();
		$pt_types = get_product_type();
		$mf_types = get_message_format_type();
		$dm_types = get_delivery_method_type();
		$nt_types = get_notification_type();

		$s_hidden_fields = '<input type="hidden" name="mode" value="save" /><input type="hidden" name="agreed" value="true" />';
		$s_hidden_fields .= '<input type="hidden" name="source_mode" value="' . $mode . '" /><input type="hidden" name="id" value="' . $this_userdata['user_id'] . '" /><input type="hidden" name="nkey" value="' . $nkey . '" />';
		if( $nkey != '' || $nrtype != '' )
		{
			$template->set_filenames(array(
				'body' => 'admin/user_notification_edit_body.tpl')
			);
			if( $nkey != '')
			{
			$sql = "SELECT notification_request_id as id, notification_type as type, event_type, delivery_method as delivery, message_format as template, limit_value, damage_level,
						product_type as product, metric, disabled as disable, aggregate, aggregation_group
					FROM " . NOTIFICATION_REQUEST_TABLE ."  
					WHERE shakecast_user = " . $user_id  ."
						AND notification_request_id = ". $nkey;
			if(!$result = $db->sql_query($sql))
			{
				message_die(GENERAL_ERROR, "Could not find user notification settings", $lang['Error'], __LINE__, __FILE__, $sql);
			}
			$nrt = $db->sql_fetchrow($result);
			$db->sql_freeresult($result);
			}
			
			if ($nrtype == 'SYSTEM')
			{
				$et_select = 'N/A';
				$s_hidden_fields .= '<input type="hidden" name="TXT_EVENT_TYPE" value="ALL" />';
			}
			else
			{
				$et_select = '<select name="TXT_EVENT_TYPE"><option value="">-- Select an event type --</option>';
				foreach ($et_types as $key => $value) {
					if ($key == $nrt['event_type']) {
						$et_select .= '<option value="'. $key . '" selected>'. $value .'</option>';
					} else {
						$et_select .= '<option value="'. $key . '">'. $value .'</option>';
					}
				}
				$et_select .= '</select>';
			}
			$dm_select = '<select name="TXT_DELIVERY_METHOD"><option value="">-- Select a delivery method --</option>';
			foreach ($dm_types as $key => $value) {
				if ($key == $nrt['delivery']) {
					$dm_select .= '<option value="'. $key . '" selected>'. $value .'</option>';
				} else {
					$dm_select .= '<option value="'. $key . '">'. $value .'</option>';
				}
			}
			$dm_select .= '</select>';
			$mf_select = '<select name="TXT_MESSAGE_FORMAT"><option value=""></option>';
			foreach ($mf_types as $key => $value) {
				if ($key == $nrt['template']) {
					$mf_select .= '<option value="'. $key . '" selected>'. $value .'</option>';
				} else {
					$mf_select .= '<option value="'. $key . '">'. $value .'</option>';
				}
			}
			$mf_select .= '</select>';
			$template->assign_vars(array(
				'USERNAME' => $username,
				'EMAIL' => $email,
				'OCCUPATION' => $occupation,
				'ORGANIZATION' => $organization,
				'LOCATION' => $location,
				'FULLNAME' => $fullname,
		
				'L_USERNAME' => $lang['Username'],
				'L_USER_TITLE' => $lang['User_notification_admin'],
				'L_USER_EXPLAIN' => $lang['User_notification_admin_explain'],
				'L_REGISTRATION_INFO' => $lang['User_notification_explain'],
				'PROFILE_NAME' => ($nkey != '') ? "$fullname ($username)" . ', ID: '.$nkey  : '',
				'L_SUBMIT' => $lang['Submit'],
				'L_RESET' => $lang['Reset'],
				'L_FULLNAME' => $lang['Fullname'],
				'L_YES' => $lang['Yes'],
				'L_NO' => $lang['No'],
				
				'L_ID' => $lang['id'],
				'L_TYPE' => $lang['type'],
				'L_EVENT_TYPE' => $lang['event_type'],
				'L_DELIVERY' => $lang['delivery'],
				'L_TEMPLATE' => $lang['template'],
				'L_LIMIT_VALUE' => $lang['limit_value'],
				'L_DAMAGE_LEVEL' => $lang['damage_level'],
				'L_PRODUCT' => $lang['product'],
				'L_METRIC' => $lang['metric'],
				'L_DISABLE' => $lang['disable'],
				'L_AGGREGATE' => $lang['aggregate'],
				'L_AGGREGATION_GROUP' => $lang['aggregation_group'],
				'L_FACILITY' => $lang['facility'],
		
				"ID" => count($nt_types) . $nt_types['shaking'],
				"NID" => $nrt['id'],
				"TYPE" => $nrtype,
				"TYPE_TXT" => $nt_types[$nrtype],
				"EVENT_TYPE" => $et_select,
				"DELIVERY" => $dm_select,
				"TEMPLATE" => $mf_select,
				"DAMAGE_LEVEL" => $dl_types[$nrt['damage_level']],
				"PRODUCT" => $pt_types[$nrt['product']],
				"DISABLE" => ($nrt['disable']) ? 'checked' : '',
				"AGGREGATE" => ($nrt['aggregate']) ? 'checked' : '',
				"AGGREGATION_GROUP" => $nrt['aggregation_group'],

				"L_EDIT" => "Edit",
				"L_DELETE" => "Delete",
				'S_HIDDEN_FIELDS' => $s_hidden_fields,
				'S_PROFILE_ACTION' => append_sid("admin_user_notifications.$phpEx"))
			);
			if ($nrtype == 'UPD_EVENT' || $nrtype == 'NEW_EVENT' || $nrtype == 'CAN_EVENT' || $nrtype == 'SHAKING' || $nrtype == 'NEW_PROD') 
			{
				$template->assign_block_vars('limit_value', array(
				"LIMIT_VALUE" => $nrt['limit_value'])
				);
			}
			if ($nrtype == 'NEW_PROD') 
			{
				$pt_select = '<select name="TXT_PRODUCT_TYPE"><option value=""></option>';
				foreach ($pt_types as $key => $value) {
					if ($key == $nrt['product']) {
						$pt_select .= '<option value="'. $key . '" selected>'. $value .'</option>';
					} else {
						$pt_select .= '<option value="'. $key . '">'. $value .'</option>';
					}
				}
				$pt_select .= '</select>';
				$template->assign_block_vars('product', array(
				"PRODUCT" => $pt_select)
				);
			}
			if ($nrtype == 'SHAKING') 
			{
				$m_select = '<select name="TXT_METRIC"><option value=""></option>';
				foreach ($m_types as $key => $value) {
					if ($key == $nrt['metric']) {
						$m_select .= '<option value="'. $key . '" selected>'. $value .'</option>';
					} else {
						$m_select .= '<option value="'. $key . '">'. $value .'</option>';
					}
				}
				$m_select .= '</select>';
				$template->assign_block_vars('metric', array(
				"METRIC" => $m_select)
				);
			}
			if ($nrtype == 'DAMAGE') 
			{
				$dl_select = '<select name="TXT_DAMAGE_LEVEL"><option value=""></option>';
				foreach ($dl_types as $key => $value) {
					if ($key == $nrt['damage_level']) {
						$dl_select .= '<option value="'. $key . '" selected>'. $value .'</option>';
					} else {
						$dl_select .= '<option value="'. $key . '">'. $value .'</option>';
					}
				}
				$dl_select .= '</select>';
				$template->assign_block_vars('damage_level', array(
				"DAMAGE_LEVEL" => $dl_select)
				);
			}
		}
		else
		{
			//
			// Default user selection box
			//
			$template->set_filenames(array(
				'body' => 'admin/user_notification_select_body2.tpl')
			);
		
			$template->assign_vars(array(
				'L_USER_TITLE' => $lang['User_notification_admin'],
				'L_USER_EXPLAIN' => $lang['User_notification_admin_explain'],
				'L_USER_SELECT' => $lang['Select_a_User'],
				'L_LOOK_UP' => $lang['Look_up_user'],
				'L_FIND_USERNAME' => $lang['Find_username'],
		
				'U_SEARCH_USER' => append_sid("./../search.$phpEx?mode=searchuser"), 
		
				'S_USER_ACTION' => append_sid("admin_user_notifications.$phpEx"),
				'S_USER_SELECT' => $select_list)
			);
		
			$user_tag = '<input type="hidden" name="username" value="'.$username.'" />';
			notification_jumpbox('admin_user_notifications.'.$phpEx, $user_tag);
			//
			// Let's do an overall check for settings/versions which would prevent
			// us from doing file uploads....
			//
			$ini_val = ( phpversion() >= '4.0.0' ) ? 'ini_get' : 'get_cfg_var';
			$form_enctype = ( !@$ini_val('file_uploads') || phpversion() == '4.0.4pl1' || !$board_config['allow_avatar_upload'] || ( phpversion() < '4.0.3' && @$ini_val('open_basedir') != '' ) ) ? '' : 'enctype="multipart/form-data"';
		
			$sql = "SELECT nr.notification_request_id as id, nr.notification_type as type, nr.event_type, nr.delivery_method as delivery, nr.message_format as template, nr.limit_value, nr.damage_level,
						nr.product_type as product, nr.metric, nr.disabled as disable, nr.aggregate, nr.aggregation_group
					FROM " . NOTIFICATION_REQUEST_TABLE ." nr 
					WHERE nr.shakecast_user = " . $user_id  ."
					ORDER BY nr.notification_type";
/*			$sql = "SELECT nr.notification_request_id as id, nr.notification_type type, nr.event_type, nr.delivery_method as delivery, nr.message_format as template, nr.limit_value, nr.damage_level,
						nr.product_type as product, nr.metric, nr.disabled as disable, nr.aggregate, nr.aggregation_group, count(fnr.notification_request_id) as fnr_count
					FROM (" . NOTIFICATION_REQUEST_TABLE ." nr 
						LEFT JOIN ". FACILITY_NOTIFICATION_REQUEST_TABLE ." fnr on nr.notification_request_id = fnr.notification_request_id)
					WHERE nr.shakecast_user = " . $user_id  ."
					GROUP BY nr.notification_request_id
					ORDER BY nr.notification_type";
*/
			if(!$result = $db->sql_query($sql))
			{
				message_die(GENERAL_ERROR, "Could not find notification request settings", $lang['Error'], __LINE__, __FILE__, $sql);
			}
	
			$template->assign_vars(array(
				'USERNAME' => $username,
				'EMAIL' => $email,
				'OCCUPATION' => $occupation,
				'ORGANIZATION' => $organization,
				'LOCATION' => $location,
				'FULLNAME' => $fullname,
				'USER_NAME' => $fullname . ' ('. $username . ')',
		
				'L_USERNAME' => $lang['Username'],
				'L_USER_TITLE' => $lang['User_notification_admin'],
				'L_USER_EXPLAIN' => $lang['User_notification_admin_explain'],
				'L_USER_NOTIFICATION' => $lang['User_notification_explain'],
				'L_NEW_PASSWORD' => $lang['New_password'], 
				'L_PASSWORD_IF_CHANGED' => $lang['password_if_changed'],
				'L_CONFIRM_PASSWORD' => $lang['Confirm_password'],
				'L_PASSWORD_CONFIRM_IF_CHANGED' => $lang['password_confirm_if_changed'],
				'L_SUBMIT' => $lang['Submit'],
				'L_RESET' => $lang['Reset'],
				'L_FULLNAME' => $lang['Fullname'],
				'L_LOCATION' => $lang['Location'],
				'L_OCCUPATION' => $lang['Occupation'],
				'L_ORGANIZATION' => $lang['organization'],
				'L_YES' => $lang['Yes'],
				'L_NO' => $lang['No'],
				
				'L_SPECIAL' => $lang['User_special'],
				'L_SPECIAL_EXPLAIN' => $lang['User_special_explain'],
				'L_USER_ACTIVE' => $lang['User_status'],
				
				'L_ITEMS_REQUIRED' => $lang['Items_required'],
				'L_REGISTRATION_INFO' => $lang['Registration_info'],
				'L_PROFILE_INFO' => $lang['Profile_info'],
				'L_PROFILE_INFO_NOTICE' => $lang['Profile_info_warn'],
				'L_EMAIL_ADDRESS' => $lang['Email_address'],
				'S_FORM_ENCTYPE' => $form_enctype,
		
				'L_DELETE_USER' => $lang['User_delete'],
				'L_DELETE_USER_EXPLAIN' => $lang['User_delete_explain'],
				"L_SELECT_USER_LEVEL" => $lang['User_Level'],
	
				'L_ID' => $lang['id'],
				'L_TYPE' => $lang['type'],
				'L_EVENT_TYPE' => $lang['event_type'],
				'L_DELIVERY' => $lang['delivery'],
				'L_TEMPLATE' => $lang['template'],
				'L_LIMIT_VALUE' => $lang['limit_value'],
				'L_DAMAGE_LEVEL' => $lang['damage_level'],
				'L_PRODUCT' => $lang['product'],
				'L_METRIC' => $lang['metric'],
				'L_DISABLE' => $lang['disable'],
				'L_AGGREGATE' => $lang['aggregate'],
				'L_AGGREGATION_GROUP' => $lang['aggregation_group'],
				'L_FACILITY' => $lang['facility'],
		
				"L_EDIT" => "Edit",
				"L_DELETE" => "Delete",
				'S_HIDDEN_FIELDS' => $s_hidden_fields,
				'S_PROFILE_ACTION' => append_sid("admin_user_notifications.$phpEx"))
			);
		
			while ($row = $db->sql_fetchrow($result))
			//for( $i = 0; $i < count($rows); $i++ )
			{
				//if ($row['type'] == 'SHAKING' || $row['type'] == 'DAMAGE') {
					$sql = "SELECT count(fnr.notification_request_id) as fnr_count
							FROM " . FACILITY_NOTIFICATION_REQUEST_TABLE ." fnr 
							WHERE fnr.notification_request_id = " . $row['id']  ."
							";
	
					if(!$row_count_result = $db->sql_query($sql))
					{
						message_die(GENERAL_ERROR, "Could not find notification request settings", $lang['Error'], __LINE__, __FILE__, $sql);
					}
					$row_count = $db->sql_fetchrow($row_count_result);
				//}

				$row_color = ( !($i % 2) ) ? $theme['td_color1'] : $theme['td_color2'];
				$row_class = ( !($i % 2) ) ? $theme['td_class1'] : $theme['td_class2'];
				$key = $row['id'];
				
				$template->assign_block_vars('words', array(
					"ROW_COLOR" => "#" . $row_color,
					"ROW_CLASS" => $row_class,
					"ID" => $row['id'],
					"Type" => $nt_types[$row['type']],
					"Event_Type" => $et_types[$row['event_type']],
					"Delivery" => $dm_types[$row['delivery']],
					"Template" => $mf_types[$row['template']],
					"Limit_Value" => $row['limit_value'],
					"Damage_Level" => $dl_types[$row['damage_level']],
					"Product" => $pt_types[$row['product']],
					"Metric" => $m_types[$row['metric']],
					"Disable" => ($row['disable']) ? $lang['Yes'] : '',
					"Aggregate" => ($row['aggregate']) ? $lang['Yes'] : '',
					"Aggregation_Group" => $row['aggregation_group'],
					"Facility_Count" => $row_count['fnr_count'] ,
					//"Facility_Count" => ($row['type'] == 'DAMAGE' || $row['type'] == 'SHAKING')
					//	? $row_count['fnr_count'] : '',
		
					"U_FAC_EDIT" => append_sid("admin_user_facility.$phpEx?mode=edit&amp;username=$username&nkey=$key&nrtype=".$row['type']),
					"U_EDIT" => append_sid("admin_user_notifications.$phpEx?mode=edit&amp;username=$username&nkey=$key&nrtype=".$row['type']),
					"U_DELETE" => append_sid("admin_user_notifications.$phpEx?mode=delete&amp;username=$username&nkey=$key"))
				);
			}
		}
	}
	//
	// Ok, the profile has been modified and submitted, let's update
	//
	else if ( $mode == 'save' )
	{
		$user_id = intval($HTTP_POST_VARS['id']);
		$this_userdata = get_userdata($user_id);
		$username = $this_userdata['username'];
		if( isset( $HTTP_POST_VARS['source_mode'] ) || isset( $HTTP_GET_VARS['source_mode'] ) )
		{
			$source_mode = ( isset( $HTTP_POST_VARS['source_mode']) ) ? $HTTP_POST_VARS['source_mode'] : $HTTP_GET_VARS['source_mode'];
			$source_mode = htmlspecialchars($source_mode);
		}

		$notification_type = ( !empty($HTTP_POST_VARS['TXT_NOTIFICATION_TYPE']) ) ? "'" . str_replace("\'", "''", trim(strip_tags(htmlspecialchars($HTTP_POST_VARS['TXT_NOTIFICATION_TYPE'])))) ."'": "NULL";
		$event_type = ( !empty($HTTP_POST_VARS['TXT_EVENT_TYPE']) ) ? "'" . str_replace("\'", "''", trim(strip_tags(htmlspecialchars( $HTTP_POST_VARS['TXT_EVENT_TYPE'] ) ))) ."'": "NULL";
		if ($event_type == "NULL" && $notification_type != "'SYSTEM'") 
		{
			message_die(GENERAL_MESSAGE, "Must provide an event type.");
		}
		$delivery_method = ( !empty($HTTP_POST_VARS['TXT_DELIVERY_METHOD']) ) ? "'" . str_replace("\'", "''", trim(strip_tags(htmlspecialchars( $HTTP_POST_VARS['TXT_DELIVERY_METHOD'] ) ))) ."'": "NULL";
		if ($delivery_method == "NULL") 
		{
			message_die(GENERAL_MESSAGE, "Must provide a delivery method.");
		}
		$message_format = ( !empty($HTTP_POST_VARS['TXT_MESSAGE_FORMAT']) ) ? "'" . str_replace("\'", "''", trim(strip_tags(htmlspecialchars( $HTTP_POST_VARS['TXT_MESSAGE_FORMAT'] ) ))) ."'": "NULL";
		$limit_value = ( !empty($HTTP_POST_VARS['TXT_LIMIT_VALUE']) ) ? floatval( $HTTP_POST_VARS['TXT_LIMIT_VALUE'] ) : "NULL";
		$disabled = ( !empty($HTTP_POST_VARS['TXT_DISABLED']) ) ? intval( $HTTP_POST_VARS['TXT_DISABLED'] ) : "NULL";
		$aggregate = ( !empty($HTTP_POST_VARS['TXT_AGGREGATE']) ) ? intval( $HTTP_POST_VARS['TXT_AGGREGATE'] ) : "NULL";
		$aggregation_group = ( !empty($HTTP_POST_VARS['TXT_AGGREGATION_GROUP']) ) ? "'" . str_replace("\'", "''", trim(strip_tags(htmlspecialchars( $HTTP_POST_VARS['TXT_AGGREGATION_GROUP'] ) ))) ."'": "NULL";
		$damage_level = ( !empty($HTTP_POST_VARS['TXT_DAMAGE_LEVEL']) ) ? "'" . str_replace("\'", "''", trim(strip_tags(htmlspecialchars( $HTTP_POST_VARS['TXT_DAMAGE_LEVEL'] ) ))) ."'": "NULL";
		$product_type = ( !empty($HTTP_POST_VARS['TXT_PRODUCT_TYPE']) ) ? "'" . str_replace("\'", "''", trim(strip_tags(htmlspecialchars( $HTTP_POST_VARS['TXT_PRODUCT_TYPE'] ) )))."'" : "NULL";
		$metric = ( !empty($HTTP_POST_VARS['TXT_METRIC']) ) ? "'" . str_replace("\'", "''", trim(strip_tags(htmlspecialchars( $HTTP_POST_VARS['TXT_METRIC'] ) ))) ."'"   : "NULL";

		if ( $source_mode == 'edit' )
		{
			$sql = "UPDATE " . NOTIFICATION_REQUEST_TABLE . "
				SET event_type = " . $event_type .", delivery_method = " . $delivery_method . ", message_format = " . $message_format . ",
					metric = " . $metric .", product_type = " . $product_type .", damage_level = " . $damage_level .", aggregation_group = " . $aggregation_group .", 
					disabled = " . $disabled .", aggregate = " . $aggregate .", limit_value = " . $limit_value ."
				WHERE shakecast_user = ". $user_id ." 
					AND notification_request_id = ". $nkey;
			if ( !($result = $db->sql_query($sql)) )
			{
				message_die(GENERAL_ERROR, 'Could not update user notification request table', '', __LINE__, __FILE__, $sql);
			}

			$message .= 'User notification request updated.<br /><br />' . sprintf($lang['Click_return_usernotadmin'], '<a href="' . append_sid("admin_user_notifications.$phpEx?mode=edit&amp;username=$username") . '">', '</a>') . '<br /><br />' . sprintf($lang['Click_return_admin_index'], '<a href="' . append_sid("index.$phpEx?pane=right") . '">', '</a>');

		}
		else if ( $source_mode == 'add' )
		{
			$sql = "INSERT INTO " . NOTIFICATION_REQUEST_TABLE . " (notification_type, event_type, delivery_method, message_format,
						metric, product_type, damage_level, aggregation_group, disabled, aggregate, limit_value, shakecast_user) 
					VALUES (" . $notification_type .", " . $event_type .", " . $delivery_method .", " . $message_format .", 
						" . $metric .", " . $product_type .", " . $damage_level .", " . $aggregation_group .", 
						$disabled, $aggregate, $limit_value, $user_id)";
			if ( !($result = $db->sql_query($sql)) )
			{
				message_die(GENERAL_ERROR, 'Could not update user notification request table', '', __LINE__, __FILE__, $sql);
			}

			$message .= 'User notification request inserted.<br /><br />' . sprintf($lang['Click_return_usernotadmin'], '<a href="' . append_sid("admin_user_notifications.$phpEx?mode=edit&amp;username=$username") . '">', '</a>') . '<br /><br />' . sprintf($lang['Click_return_admin_index'], '<a href="' . append_sid("index.$phpEx?pane=right") . '">', '</a>');

		}
		
		$sql = "DELETE FROM " . GEOMETRY_USER_PROFILE_TABLE . " 
					WHERE shakecast_user =  $user_id";
		if ( !($result = $db->sql_query($sql)) )
		{
			message_die(GENERAL_ERROR, 'Could not update user profile table', '', __LINE__, __FILE__, $sql);
		}
		message_die(GENERAL_MESSAGE, $message);
	}
	else if( $mode == "delete" )
	{
		$confirm = isset($HTTP_POST_VARS['confirm']);

		if( $username != '' && $confirm )
		{
			$sql = "SELECT shakecast_user 
				FROM " . SC_USER_TABLE . "   
				WHERE username = '$username'";
			if( !($result = $db->sql_query($sql)) )
			{
				message_die(GENERAL_ERROR, 'Could not obtain information for this user', '', __LINE__, __FILE__, $sql);
			}
	
			$ids = $db->sql_fetchrowset($result);
			for( $i = 0; $i < count($ids); $i++ )
			{
				$user_id = $ids[$i]['shakecast_user'];
				if( $nkey != '')
				{
					$sql = "DELETE FROM " . NOTIFICATION_REQUEST_TABLE . "
						WHERE shakecast_user = $user_id AND notification_request_id = $nkey";
					if( !$db->sql_query($sql) )
					{
						message_die(GENERAL_ERROR, 'Could not delete user notification request', '', __LINE__, __FILE__, $sql);
					}
				}
				else 
				{
					$sql = "DELETE fnr FROM " . FACILITY_NOTIFICATION_REQUEST_TABLE . " as fnr
						INNER JOIN " . NOTIFICATION_REQUEST_TABLE . " as nr
						on fnr.notification_request_id = nr.notification_request_id
						WHERE shakecast_user = $user_id";
					if( !$db->sql_query($sql) )
					{
						message_die(GENERAL_ERROR, 'Could not delete user facility notification request', '', __LINE__, __FILE__, $sql);
					}
		
					$sql = "DELETE FROM " . NOTIFICATION_TABLE . "
						WHERE shakecast_user = $user_id";
					if( !$db->sql_query($sql) )
					{
						message_die(GENERAL_ERROR, 'Could not delete user notification', '', __LINE__, __FILE__, $sql);
					}
		
					$sql = "DELETE FROM " . NOTIFICATION_REQUEST_TABLE . "
						WHERE shakecast_user = $user_id";
					if( !$db->sql_query($sql) )
					{
						message_die(GENERAL_ERROR, 'Could not delete user notification request', '', __LINE__, __FILE__, $sql);
					}
		
				}
	
				$sql = "DELETE FROM " . GEOMETRY_USER_PROFILE_TABLE . "
					WHERE shakecast_user = $user_id";
				if( !$db->sql_query($sql) )
				{
					message_die(GENERAL_ERROR, 'Could not delete user geometry profile', '', __LINE__, __FILE__, $sql);
				}
		
			}

			$message = $lang['user_notification_deleted'] . "<br /><br />" . sprintf($lang['Click_return_usernotadmin'], "<a href=\"" . append_sid("admin_user_notifications.$phpEx") . "\">", "</a>") . "<br /><br />" . sprintf($lang['Click_return_admin_index'], "<a href=\"" . append_sid("index.$phpEx?pane=right") . "\">", "</a>");

			message_die(GENERAL_MESSAGE, $message);
		}
		elseif( $username != '' && !$confirm)
		{
			// Present the confirmation screen to the user
			$template->set_filenames(array(
				'body' => 'admin/confirm_body.tpl')
			);

			$hidden_fields = '<input type="hidden" name="mode" value="'.$mode.'" /><input type="hidden" name="username" value="' . $username . '" />';
			if( $nkey != '')
			{
				$hidden_fields .= '<input type="hidden" name="nkey" value="'.$nkey.'" />';
			}
			$template->assign_vars(array(
				'MESSAGE_TITLE' => $lang['Confirm'],
				'MESSAGE_TEXT' => $lang['Confirm_user_notification'],

				'L_YES' => $lang['Yes'],
				'L_NO' => $lang['No'],

				'S_CONFIRM_ACTION' => append_sid("admin_user_notifications.$phpEx"),
				'S_HIDDEN_FIELDS' => $hidden_fields)
			);
		}
		else
		{
			message_die(GENERAL_MESSAGE, $lang['no_user_selected']);
		}
	}
}
else
{
	//
	// Default user selection box
	//
	$template->set_filenames(array(
		'body' => 'admin/user_notification_select_body.tpl')
	);

	$template->assign_vars(array(
		'L_USER_TITLE' => $lang['User_notification_admin'],
		'L_USER_EXPLAIN' => $lang['User_notification_admin_explain'],
		'L_USER_SELECT' => $lang['Select_a_User'],
		'L_LOOK_UP' => $lang['Look_up_user'],
		'L_FIND_USERNAME' => $lang['Find_username'],

		'U_SEARCH_USER' => append_sid("./../search.$phpEx?mode=searchuser"), 

		'S_USER_ACTION' => append_sid("admin_user_notifications.$phpEx"),
		'S_USER_SELECT' => $select_list)
	);

	$user_count = get_db_stat('usercount');
	if ( !$user_count )
	{
		//
		// No group members
		//
		$template->assign_block_vars('switch_no_members', array());
		$template->assign_vars(array(
			'L_NO_MEMBERS' => $lang['No_group_members'])
		);
	}

	if( isset($HTTP_POST_VARS['start']) ||  isset($HTTP_GET_VARS['start']) )
	{
		$start = ( isset($HTTP_POST_VARS['start']) ) ? $HTTP_POST_VARS['start'] : $HTTP_GET_VARS['start'];
	}
	$current_page = ( !$user_count ) ? 1 : ceil( $user_count / $board_config['topics_per_page'] );
	$template->assign_vars(array(
		'PAGINATION' => generate_pagination("admin_user_notifications.$phpEx?" . POST_GROUPS_URL . "=$group_id", $user_count, $board_config['topics_per_page'], $start),
		'PAGE_NUMBER' => sprintf($lang['Page_of'], ( floor( $start / $board_config['topics_per_page'] ) + 1 ), $current_page ), 

		'L_GOTO_PAGE' => $lang['Goto_page'])
	);

	$sql = "SELECT su.shakecast_user, pu.username, pu.user_fullname, pu.user_email, pu.user_active, pu.user_occ, pu.user_organization, pu.user_level
			FROM (" . USERS_TABLE ." pu INNER JOIN ". SC_USER_TABLE ." su on pu.user_id = su.shakecast_user)
			WHERE pu.user_id <> " . ANONYMOUS ;
	if ($board_config['topics_per_page']) { $sql .= ' LIMIT '.$board_config['topics_per_page'];}
	if ($start) { $sql .= " OFFSET $start";}
	if(!$result = $db->sql_query($sql))
	{
		message_die(GENERAL_ERROR, "Could not find damage level settings", $lang['Error'], __LINE__, __FILE__, $sql);
	}
	$rows = $db->sql_fetchrowset($result);

	$template->assign_vars(array(
		"L_WORDS_TITLE" => $lang['fac_title'],
		"L_WORDS_TEXT" => $lang['fac_explain'],
		"L_USERNAME" => "Username",
		"L_FULL_NAME" => "Full Name",
		"L_EMAIL" => "Email Address",
		"L_USER_STATUS" => "No. Request",
		"L_OCCUPATION" => "No. Profile",
		"L_ORGANIZATION" => "No. Facility",
		"L_USER_LEVEL" => $lang['User_Level'],
		"L_EDIT" => "Edit",
		"L_DELETE" => "Delete",
		"L_ADD_EVENT" => $lang['Add_new_user'],
		"L_ACTION" => $lang['Action'],

		"S_WORDS_ACTION" => append_sid("admin_user_notifications.$phpEx?mode=add"),
		"S_HIDDEN_FIELDS" => '')
	);

	for( $i = 0; $i < count($rows); $i++ )
	{
		$shakecast_user = $rows[$i]['shakecast_user'];
		$username = $rows[$i]['username'];
		$fullname = $rows[$i]['user_fullname'];
		$email = $rows[$i]['user_email'];
/*		$sql = "SELECT count(distinct fnr.facility_id) as fnr_count
				FROM (" . NOTIFICATION_REQUEST_TABLE ." nr 
					INNER JOIN ". FACILITY_NOTIFICATION_REQUEST_TABLE ." fnr on fnr.notification_request_id = nr.notification_request_id)
				WHERE nr.shakecast_user = " . $shakecast_user;
		if(!$result = $db->sql_query($sql))
		{
			message_die(GENERAL_ERROR, "Could not find damage level settings", $lang['Error'], __LINE__, __FILE__, $sql);
		}
		$row = $db->sql_fetchrow($result);
		$fnr_count = $row['fnr_count'];
*/		
		$sql = "SELECT count(notification_request_id) as nr_count
				FROM " . NOTIFICATION_REQUEST_TABLE ." 
				WHERE shakecast_user = " . $shakecast_user;
		if(!$result = $db->sql_query($sql))
		{
			message_die(GENERAL_ERROR, "Could not find damage level settings", $lang['Error'], __LINE__, __FILE__, $sql);
		}
		$row = $db->sql_fetchrow($result);
		$nr_count = $row['nr_count'];

		$sql = "SELECT count(profile_id) as gup_count
				FROM " . GEOMETRY_USER_PROFILE_TABLE ." 
				WHERE shakecast_user = " . $shakecast_user;
		if(!$result = $db->sql_query($sql))
		{
			message_die(GENERAL_ERROR, "Could not find damage level settings", $lang['Error'], __LINE__, __FILE__, $sql);
		}
		$row = $db->sql_fetchrow($result);
		$gup_count = $row['gup_count'];
		
		$row_color = ( !($i % 2) ) ? $theme['td_color1'] : $theme['td_color2'];
		$row_class = ( !($i % 2) ) ? $theme['td_class1'] : $theme['td_class2'];
		
		$template->assign_block_vars('words', array(
			"ROW_COLOR" => "#" . $row_color,
			"ROW_CLASS" => $row_class,
			"USERNAME" => $username,
			"FULL_NAME" => $fullname,
			"EMAIL" => $email,
			"USER_STATUS" => $nr_count,
			"OCCUPATION" => $gup_count,
			"ORGANIZATION" => $fnr_count,

			"U_EDIT" => append_sid("admin_user_notifications.$phpEx?mode=edit&amp;username=$username"),
			"U_DELETE" => append_sid("admin_user_notifications.$phpEx?mode=delete&amp;username=$username"))
		);
	}

}

$template->pparse('body');
include('./page_footer_admin.'.$phpEx);

?>