<?php
/***************************************************************************
 *                              admin_words.php
 *                            -------------------
 *   begin                : Thursday, Jul 12, 2001
 *   copyright            : (C) 2001 The phpBB Group
 *   email                : support@phpbb.com
 *
 *   $Id: admin_facility_type.php 117 2007-07-25 19:03:06Z klin $
 *
 *
 ***************************************************************************/

/***************************************************************************
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

if( !empty($setmodules) )
{
	$file = basename(__FILE__);
	$module['Facility_Admin']['Facility_Type'] = $file;
	return;
}

define('IN_SC', 1);

//
// Load default header
//
$sc_root_path = "./../";
require($sc_root_path . 'extension.inc');

$cancel = ( isset($HTTP_POST_VARS['cancel']) ) ? true : false;
$no_page_header = $cancel;

require('./pagestart.' . $phpEx);

if ($cancel)
{
	redirect('admin/' . append_sid("admin_facility_type.$phpEx", true));
}

if( isset($HTTP_GET_VARS['mode']) || isset($HTTP_POST_VARS['mode']) )
{
	$mode = (isset($HTTP_GET_VARS['mode'])) ? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode'];
	$mode = htmlspecialchars($mode);
}
else 
{
	//
	// These could be entered via a form button
	//
	if( isset($HTTP_POST_VARS['add_event']) )
	{
		$mode = "add_event";
	}
	else if( isset($HTTP_POST_VARS['save']) )
	{
		$mode = "save";
	}
	else
	{
		$mode = "";
	}
}

// Restrict mode input to valid options
$mode = ( in_array($mode, array('add', 'edit', 'save', 'delete')) ) ? $mode : '';

if( $mode != "" )
{
	if( $mode == "edit" || $mode == "add" )
	{
		if( isset($HTTP_GET_VARS['id']) || isset($HTTP_POST_VARS['id']) )
		{
			$id = (isset($HTTP_GET_VARS['id'])) ? $HTTP_GET_VARS['id'] : $HTTP_POST_VARS['id'];
			$id = htmlspecialchars($id);
		}

		$template->set_filenames(array(
			"body" => "admin/facility_type_edit_body.tpl")
		);


		$facility = array('facility_type' => '', 'attribute_name' => '', 'description' => '', 'name' => '');
		$s_hidden_fields = '';

		if( $mode == "edit"  )
		{
			if( $id != '' )
			{
				$sql = "SELECT facility_type, name, description
						FROM facility_type 
						WHERE facility_type = \"$id\"";
				$result = $db->sql_query($sql);
				if( !$result )
				{
					message_die(GENERAL_ERROR, "Couldn't get facility types.", "", __LINE__, __FILE__, $sql );
				}
				$facility_type_settings = $db->sql_fetchrow($result);

/*
				$sql = "SELECT ff.facility_type_fragility_id, ff.damage_level, dl.name, ff.low_limit, ff.high_limit, ff.metric 
						FROM facility_type_fragility ff INNER JOIN damage_level dl on ff.damage_level = dl.damage_level
						WHERE ff.facility_type = $id";
*/
				$sql = "SELECT facility_type_fragility_id, damage_level, low_limit, high_limit, metric 
						FROM facility_type_fragility 
						WHERE facility_type = \"$id\"";
				$result = $db->sql_query($sql);
				if($result)
				{
					while ($row = $db->sql_fetchrow($result)) {
						$facility_fragility[$row['facility_type_fragility_id']] = array(
							"damage_level"	=>	$row['damage_level'],
							"low_limit"	=>	$row['low_limit'],
							"high_limit"	=>	$row['high_limit'],
							"metric"	=>	$row['metric']);
					}
				}
				
				$sql = "SELECT attribute_name, description FROM facility_type_attribute WHERE facility_type = \"$id\"";
				$result = $db->sql_query($sql);
				if($result )
				{
					while ($row = $db->sql_fetchrow($result)) {
						$facility_attribute[$row['attribute_name']] = $row['description'];
					}
				}
				$hidden_fields = '<input type="hidden" name="type_id" value="'.$id.'" />';
			}
			else
			{
				message_die(GENERAL_MESSAGE, "No facility type attribute selected for editing ");
			}
			
			// Grab the current list of facility types
			$sql = "SELECT facility_type, name FROM facility_type";
			$result = $db->sql_query($sql);
			if( !$result )
			{
				message_die(GENERAL_ERROR, "Couldn't get facility types.", "", __LINE__, __FILE__, $sql );
			}
			$fac_type = $db->sql_fetchrowset($result);
			$fac_type_select = '<select name="facility_type" id="facility_type"  onChange="fetch_type_id(this, \'' . append_sid("admin_facility_type.$phpEx?mode=edit") . 
				'\')"><option value="">[Select Type]</option>';
			if( trim($fac_type) )
			{
				for( $i = 0; $i < count($fac_type); $i++ )
				{
					if ($fac_type[$i]['facility_type'] == $facility_type_settings['facility_type']) {
						$fac_type_select .= '<option value="' . $fac_type[$i]['facility_type'] . '" selected>' . $fac_type[$i]['facility_type'] . '</option>';
					} else {
						$fac_type_select .= '<option value="' . $fac_type[$i]['facility_type'] . '">' . $fac_type[$i]['facility_type'] . '</option>';
					}
				}
			}
			$fac_type_select .= '</select>';

		} else {
			$hidden_fields = '<input type="hidden" name="type_id" value="" />';
			$fac_type_select = '<input type="text" name="facility_type" id="facility_type"/>';
		}
		
		$template->assign_vars(array(
			"L_WORDS_TITLE" => $lang['facility_type_title'],
			"L_WORDS_TEXT" => $lang['facility_type_explain'],

			"FACILITY_TYPE" => $fac_type_select,
			"NAME" => $facility_type_settings['name'],
			"DESCRIPTION" => $facility_type_settings['description'],
			"F_HIDDEN_FIELDS" => $hidden_fields,

			"L_FACILITY_TYPE_ATTRIBUTE_ID" => $facility_type_attribute_id,
			"L_FACILITY_TYPE" => "Facility Type",
			"L_NAME" => "Name",
			"L_DESCRIPTION" => "Description",
			"L_FACILITY_TYPE_EDIT" => "Facility Type Information",
			"L_DAMAGE_LEVEL" => "Damage Level",
			"L_LOW_LIMIT" => "Low Limit",
			"L_HIGH_LIMIT" => "High Limit",
			"L_METRIC" => "Metric",


			"L_SUBMIT" => $lang['Submit'],

			"S_FACILITY_TYPE_ACTION" => append_sid("admin_facility_type.$phpEx"),
			"S_HIDDEN_FIELDS" => $s_hidden_fields)
		);

		// Grab the current list of damage levels
		$sql = "SELECT damage_level, name FROM damage_level order by severity_rank";
		$result = $db->sql_query($sql);
		if( !$result )
		{
			message_die(GENERAL_ERROR, "Couldn't get damage levels.", "", __LINE__, __FILE__, $sql );
		}
		$damage_level = $db->sql_fetchrowset($result);
		
		// Grab the current list of metric
		$sql = "SELECT short_name, name FROM metric";
		$result = $db->sql_query($sql);
		if( !$result )
		{
			message_die(GENERAL_ERROR, "Couldn't get metric.", "", __LINE__, __FILE__, $sql );
		}
		$metric = $db->sql_fetchrowset($result);

		for( $index = 0; $index < count($damage_level); $index++ ) {
			$facility_damage_level = $damage_level[$index]['damage_level'];
			$facility_damage_name = $damage_level[$index]['name'];
			$facility_low_limit = '';
			$facility_high_limit = '';
			$facility_metric = '';
			$facility_fragility_id = '';
			foreach ($facility_fragility as $fac_frag_id => $fac_frag) {
				if ($fac_frag['damage_level'] == $damage_level[$index]['damage_level']) {
					$facility_fragility_id = $fac_frag_id;
					$facility_low_limit = $fac_frag['low_limit'];
					$facility_high_limit = $fac_frag['high_limit'];
					$facility_metric = $fac_frag['metric'];
				}
			}
			if (!$facility_fragility_id) {$facility_fragility_id = $index;}
			$fr_hidden_fields = '<input type="hidden" name="frag_id[]" value="'.$facility_fragility_id.'" />';
			$fr_hidden_fields .= '<input type="hidden" name="FR'.$facility_fragility_id.'[damage_level]" value="'.$facility_damage_level.'" />';
			
			$metric_select = '<select name="FR'.$facility_fragility_id.'[metric]"  id="'.$facility_damage_level.'_select" >';
			for( $i = 0; $i < count($metric); $i++ )
			{
				if (strtoupper($metric[$i]['short_name']) == 'MMI') {
					$metric_name = $metric[$i]['name']; 
				} else {
					$metric_name = $metric[$i]['name'] . " " . $metric_unit[strtoupper($metric[$i]['short_name'])]; 
				}
		
				if($metric[$i]['short_name'] == $facility_metric)
				{
					$metric_select .= '<option value="' . $metric[$i]['short_name'] . '" selected>' . $metric_name . '</option>';
				} else {
					$metric_select .= '<option value="' . $metric[$i]['short_name'] . '">' . $metric_name . '</option>';
				}
			}
			$metric_select .= '</select>';

			$row_color = ( !($i % 2) ) ? $theme['td_color1'] : $theme['td_color2'];
			$row_class = ( !($i % 2) ) ? $theme['td_class1'] : $theme['td_class2'];
	
			$template->assign_block_vars('fragrow', array(
				"ROW_COLOR" => "#" . $row_color,
				"ROW_CLASS" => $row_class,

				"FRAG_ID" => $facility_fragility_id,
				"FR_HIDDEN_FIELDS" => $fr_hidden_fields,
				"DAMAGE_LEVEL" => $facility_damage_name,
				"DAMAGE" => $facility_damage_level,
				"LOW_LIMIT" => $facility_low_limit,
				"HIGH_LIMIT" => $facility_high_limit,
				"METRIC" => $metric_select)
			);
		}
		
		$template->pparse("body");

		include('./page_footer_admin.'.$phpEx);
	}
	else if( $mode == "save" )
	{
		$type_id = ( isset($HTTP_POST_VARS['type_id']) ) ? trim($HTTP_POST_VARS['type_id']) : '';
		$sqls = array();

		if( $type_id != '' )
		{
			$sqls[] = "UPDATE " . FACILITY_TYPE_TABLE . " 
				SET facility_type = '".str_replace("\'", "''", $HTTP_POST_VARS['facility_type'])."', 
					name = '" . str_replace("\'", "''", $HTTP_POST_VARS['name']) . "', 
					description = '" . str_replace("\'", "''", $HTTP_POST_VARS['description']) . "', 
					update_username = 'webadmin', 
					update_timestamp = now() 
				WHERE facility_type = \"$type_id\"";
/*
			foreach ($HTTP_POST_VARS['FA'] as $attrib_name => $attrib_value) {
				$sql = "SELECT $attrib_name FROM". FACILITY_ATTRIBUTE_TABLE . "
						WHERE facility_id = $fac_id";
				if($result = $db->sql_query($sql))
				{
					$sqls[] = "UPDATE " . FACILITY_ATTRIBUTE_TABLE . "
						SET attribute_value = '" . str_replace("\'", "''", $attrib_value) . "' 
						WHERE facility_id = $fac_id AND attribute_name = '$attrib_name'";
				} else {
					$sqls[] = "INSERT INTO " . FACILITY_ATTRIBUTE_TABLE . "
						(facility_id, attribute_name, attribute_value) VALUES (
						$fac_id, '" . str_replace("\'", "''", $attrib_name) . "', '" 
						. str_replace("\'", "''", $attrib_value) . "')";
				}
			}
*/
			foreach ($HTTP_POST_VARS['frag_id'] as $frag_id) {
				$fac_frag = $HTTP_POST_VARS['FR'.$frag_id];
				$sql = "SELECT damage_level FROM ". FACILITY_TYPE_FRAGILITY_TABLE . "
						WHERE facility_type = '" . $type_id . "' AND damage_level = '" . $fac_frag['damage_level'] . "'";
				if($result = $db->sql_query($sql))
				{
					$sqls[] = "UPDATE " . FACILITY_TYPE_FRAGILITY_TABLE . "
						SET low_limit = " . $fac_frag['low_limit'] . ", 
							high_limit = " . $fac_frag['high_limit'] . ", 
							metric = '" . str_replace("\'", "''", $fac_frag['metric']) . "', 
							update_username = 'webadmin', 
							update_timestamp = now() 
						WHERE facility_type = '" . $type_id . "' AND damage_level = '" . $fac_frag['damage_level'] . "'";
				} else if ($fac_frag['low_limit'] && $fac_frag['high_limit']) {
					$sqls[] = "INSERT INTO " . FACILITY_TYPE_FRAGILITY_TABLE . "
							(facility_type, damage_level, low_limit, high_limit, metric, update_username, update_timestamp) VALUES ('" . 
							$type_id."', '".
							str_replace("\'", "''", $fac_frag['damage_level'])."', ".
							$fac_frag['low_limit'].", ".
							$fac_frag['high_limit'].", '".
							str_replace("\'", "''", $fac_frag['metric'])."', ".
							"'webadmin', now())"; 
				}
			}
			$message = "Facility information has been updated" . $result;
		}
		else
		{
			$sql_fac = "INSERT INTO " . FACILITY_TYPE_TABLE . " 
				(facility_type, name, description,
					update_username, update_timestamp) VALUES ('".
					str_replace("\'", "''", $HTTP_POST_VARS['facility_type'])."', '".
					str_replace("\'", "''", $HTTP_POST_VARS['name'])."', '".
					str_replace("\'", "''", $HTTP_POST_VARS['description'])."', ".
					"'webadmin', now())"; 
			if(!$result = $db->sql_query($sql_fac))
			{
				message_die(GENERAL_ERROR, "Could not insert data into facility type table", $lang['Error'], __LINE__, __FILE__, $sql);
			}
/*
			$type_id = $db->sql_nextid();
			foreach ($HTTP_POST_VARS['FA'] as $attrib_name => $attrib_value) {
				if($result = $db->sql_query($sql))
				{
					$sqls[] = "INSERT INTO " . FACILITY_ATTRIBUTE_TABLE . "
						(facility_id, attribute_value, attribute_name) VALUES (".
						$type_id.", '".
						str_replace("\'", "''", $attrib_value)."', '".
						$attrib_name."')";
				}
			}
*/
			foreach ($HTTP_POST_VARS['frag_id'] as $frag_id) {
				$fac_frag = $HTTP_POST_VARS['FR'.$frag_id];
				if ($fac_frag['low_limit'] && $fac_frag['high_limit']) {
					$sqls[] = "INSERT INTO " . FACILITY_TYPE_FRAGILITY_TABLE . "
							(facility_type, damage_level, low_limit, high_limit, metric, update_username, update_timestamp) VALUES ('" . 
							str_replace("\'", "''", $HTTP_POST_VARS['facility_type'])."', '".
							str_replace("\'", "''", $fac_frag['damage_level'])."', ".
							$fac_frag['low_limit'].", ".
							$fac_frag['high_limit'].", '".
							str_replace("\'", "''", $fac_frag['metric'])."', ".
							"'webadmin', now())"; 
				}
			}
			$message = "Facility information has been added";
		}

		foreach($sqls as $sql) {
			if(!$result = $db->sql_query($sql))
			{
				message_die(GENERAL_ERROR, "Could not insert data into facility type table", $lang['Error'], __LINE__, __FILE__, $sql);
			}
		}
		$message .= "<br /><br />" . sprintf($lang['Click_return_ftadmin'], "<a href=\"" . append_sid("admin_facility_type.$phpEx") . "\">", "</a>") . "<br /><br />" . sprintf($lang['Click_return_admin_index'], "<a href=\"" . append_sid("index.$phpEx?pane=right") . "\">", "</a>");

		message_die(GENERAL_MESSAGE, $message);
	}
	else if( $mode == "delete" )
	{
		if( isset($HTTP_POST_VARS['id']) ||  isset($HTTP_GET_VARS['id']) )
		{
			$id = ( isset($HTTP_POST_VARS['id']) ) ? $HTTP_POST_VARS['id'] : $HTTP_GET_VARS['id'];
		}
		else
		{
			$id = 0;
		}

		$confirm = isset($HTTP_POST_VARS['confirm']);

		if( $id && $confirm )
		{
			$sql = "DELETE FROM " . FACILITY_TYPE_FRAGILITY_TABLE . " 
				WHERE facility_type = '$id'";

			if(!$result = $db->sql_query($sql))
			{
				message_die(GENERAL_ERROR, "Could not delete facility", $lang['Error']);
			}

			$sql = "DELETE FROM " .  FACILITY_TYPE_TABLE . " 
				WHERE facility_type = '$id'";

			if(!$result = $db->sql_query($sql))
			{
				message_die(GENERAL_ERROR, "Could not delete facility", $lang['Error']);
			}

			$message = $lang['facility_type_deleted'] . "<br /><br />" . sprintf($lang['Click_return_ftadmin'], "<a href=\"" . append_sid("admin_facility_type.$phpEx") . "\">", "</a>") . "<br /><br />" . sprintf($lang['Click_return_admin_index'], "<a href=\"" . append_sid("index.$phpEx?pane=right") . "\">", "</a>");

			message_die(GENERAL_MESSAGE, $message);
		}
		elseif( $id && !$confirm)
		{
			// Present the confirmation screen to the user
			$template->set_filenames(array(
				'body' => 'admin/confirm_body.tpl')
			);

			$hidden_fields = '<input type="hidden" name="mode" value="'.$mode.'" /><input type="hidden" name="id" value="' . $id . '" />';

			$template->assign_vars(array(
				'MESSAGE_TITLE' => $lang['Confirm'],
				'MESSAGE_TEXT' => $lang['Confirm_type_deleted'],

				'L_YES' => $lang['Yes'],
				'L_NO' => $lang['No'],

				'S_CONFIRM_ACTION' => append_sid("admin_facility_type.$phpEx"),
				'S_HIDDEN_FIELDS' => $hidden_fields)
			);
		}
		else
		{
			message_die(GENERAL_MESSAGE, $lang['no_type_selected']);
		}
	}
}
else
{
	$template->set_filenames(array(
		"body" => "admin/facility_type_list_body.tpl")
	);

	$type_count = get_db_stat('facility_type_count');
	if ( !$type_count )
	{
		//
		// No group members
		//
		$template->assign_block_vars('switch_no_members', array());
		$template->assign_vars(array(
			'L_NO_MEMBERS' => $lang['No_group_members'])
		);
	}

	if( isset($HTTP_POST_VARS['start']) ||  isset($HTTP_GET_VARS['start']) )
	{
		$start = ( isset($HTTP_POST_VARS['start']) ) ? $HTTP_POST_VARS['start'] : $HTTP_GET_VARS['start'];
	}
	$current_page = ( !$type_count ) ? 1 : ceil( $type_count / $board_config['topics_per_page'] );
	$template->assign_vars(array(
		'PAGINATION' => generate_pagination("admin_facility_type.$phpEx?", $type_count, $board_config['topics_per_page'], $start),
		'PAGE_NUMBER' => sprintf($lang['Page_of'], ( floor( $start / $board_config['topics_per_page'] ) + 1 ), $current_page ), 

		"U_SEARCH" => append_sid("admin_facility_type.$phpEx?mode=edit"),
		'L_GOTO_PAGE' => $lang['Goto_page'])
	);

	$sql = "SELECT facility_type, name, description
			FROM facility_type ";
	if ($board_config['topics_per_page']) { $sql .= ' limit '.$board_config['topics_per_page'];}
	if ($start) { $sql .= " offset $start";}
	if(!$result = $db->sql_query($sql))
	{
		message_die(GENERAL_ERROR, "Could not find damage level settings", $lang['Error']);
	}
	$facility_types = $db->sql_fetchrowset($result);

	$template->assign_vars(array(
		"L_WORDS_TITLE" => $lang['facility_type_title'],
		"L_WORDS_TEXT" => $lang['facility_type_explain'],
		"L_FACILITY_TYPE" => "Facility Type",
		"L_ATTRIBUTE_NAME" => "Name",
		"L_DESCRIPTION" => "Description",

		"L_EDIT" => "Edit",
		"L_DELETE" => "Delete",
		"L_ADD_EVENT" => "Add new facility type",
		"L_ACTION" => $lang['Action'],

		"S_WORDS_ACTION" => append_sid("admin_facility_type.$phpEx?mode=add"),
		"S_HIDDEN_FIELDS" => '')
	);

	for($i = 0;$i < count($facility_types);  $i++)
	{
		$type = $facility_types[$i]['facility_type'];
		$name = $facility_types[$i]['name'];
		$description = $facility_types[$i]['description'];

		$row_color = ( !($i % 2) ) ? $theme['td_color1'] : $theme['td_color2'];
		$row_class = ( !($i % 2) ) ? $theme['td_class1'] : $theme['td_class2'];

		$template->assign_block_vars('words', array(
			"ROW_COLOR" => "#" . $row_color,
			"ROW_CLASS" => $row_class,
			"FACILITY_TYPE" => $type,
			"ATTRIBUTE_NAME" => $name,
			"DESCRIPTION" => $description,

			"U_EDIT" => append_sid("admin_facility_type.$phpEx?mode=edit&amp;id=$type"),
			"U_DELETE" => append_sid("admin_facility_type.$phpEx?mode=delete&amp;id=$type"))
		);
	}
}

$template->pparse("body");

include('./page_footer_admin.'.$phpEx);

?>