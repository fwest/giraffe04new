<?php
/*
# $Id: admin_template.php 194 2007-11-02 19:13:02Z klin $

# The PHP interface was originally inspired by PHPBB,
# file/database formats and directory structure are quite similar.

##############################################################################
# 
# Terms and Conditions of Software Use
# ====================================
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Disclaimer of Earthquake Information
# ====================================
# 
# The data and maps provided through this system are preliminary data
# and are subject to revision. They are computer generated and may not
# have received human review or official approval. Inaccuracies in the
# data may be present because of instrument or computer
# malfunctions. Subsequent review may result in significant revisions to
# the data. All efforts have been made to provide accurate information,
# but reliance on, or interpretation of earthquake data from a single
# source is not advised. Data users are cautioned to consider carefully
# the provisional nature of the information before using it for
# decisions that concern personal or public safety or the conduct of
# business that involves substantial monetary or operational
# consequences.
# 
# Disclaimer of Software and its Capabilities
# ===========================================
# 
# This software is provided as an "as is" basis.  Attempts have been
# made to rid the program of software defects and bugs, however the
# U.S. Geological Survey (USGS) have no obligations to provide maintenance, 
# support, updates, enhancements or modifications. In no event shall USGS 
# be liable to any party for direct, indirect, special, incidental or 
# consequential damages, including lost profits, arising out of the use 
# of this software, its documentation, or data obtained though the use 
# of this software, even if USGS or have been advised of the
# possibility of such damage. By downloading, installing or using this
# program, the user acknowledges and understands the purpose and
# limitations of this software.
# 
# Contact Information
# ===================
# 
# Coordination of this effort is under the auspices of the USGS Advanced
# National Seismic System (ANSS) coordinated in Golden, Colorado, which
# functions as the clearing house for development, distribution,
# documentation, and support. For questions, comments, or reports of
# potential bugs regarding this software please contact wald@usgs.gov or
# klin@usgs.gov.  
#
#############################################################################
*/

define('IN_SC', 1);

//
// First we do the setmodules stuff for the admin cp.
//
if( !empty($setmodules) )
{
	$filename = basename(__FILE__);
	$module[' General Admin']['Template'] = $filename;

	return;
}

//
// Load default header
//
$sc_root_path = "./../";
require($sc_root_path . 'extension.inc');

$cancel = ( isset($HTTP_POST_VARS['cancel']) ) ? true : false;
$no_page_header = $cancel;

require('./pagestart.' . $phpEx);
$admin_userdata = get_userdata($userdata['session_user_id']);

if ($cancel)
{
	redirect('admin/' . append_sid("admin_template.$phpEx", true));
}

//
// Check to see what mode we should operate in.
//
if( isset($HTTP_POST_VARS['mode']) || isset($HTTP_GET_VARS['mode']) )
{
	$mode = ( isset($HTTP_POST_VARS['mode']) ) ? $HTTP_POST_VARS['mode'] : $HTTP_GET_VARS['mode'];
	$mode = htmlspecialchars($mode);
}
else
{
	$mode = "";
}

$delimeter  = '=+:';

//
// Read a listing of notification types and templates
//
$pt_types = get_product_type();
$mf_types = get_message_format_type();
$dm_types = get_delivery_method_type();
$nt_types = get_notification_type();

include($sc_root_path. "../templates/template_info.cfg");

//
// Select main mode
//
if ( $mode != "" )
{
	if( $mode == "edit" || $mode == "add" )
	{
		$template->set_filenames(array(
			"body" => "admin/template_edit_body.tpl")
		);

		$template_settings = array('message_format' => '', 'name' => '', 'description' => '',
			'file_name' => '');

		$s_hidden_fields = '';
		if( $mode == "edit")
		{
			$temp_id = ( !empty($HTTP_POST_VARS['id']) ) ? $HTTP_POST_VARS['id'] : $HTTP_GET_VARS['id'];
			$nt_type = ( !empty($HTTP_POST_VARS['nt']) ) ? $HTTP_POST_VARS['nt'] : $HTTP_GET_VARS['nt'];
			$dm_type = ( !empty($HTTP_POST_VARS['dm']) ) ? $HTTP_POST_VARS['dm'] : $HTTP_GET_VARS['dm'];
			$indb = ( !empty($HTTP_POST_VARS['indb']) ) ? $HTTP_POST_VARS['indb'] : $HTTP_GET_VARS['indb'];
			$file_prefix = $sc_root_path. "../templates/$nt_type/$dm_type/$temp_id";
			$file_suffix = ($dm_type == "SCRIPT") ? "pl" : "txt";
			$file_header = fread(fopen($file_prefix."_header.".$file_suffix, "r"), filesize($file_prefix."_header.".$file_suffix));
			$file_body = fread(fopen($file_prefix."_body.".$file_suffix, "r"), filesize($file_prefix."_body.".$file_suffix));
			$file_footer = fread(fopen($file_prefix."_footer.".$file_suffix, "r"), filesize($file_prefix."_footer.".$file_suffix));
			$template_caption = "'$nt_type-$dm_type-$temp_id' Template";
			
			$s_hidden_fields .= '<input type="hidden" name="id" value="'. $temp_id .'" />';
			$s_hidden_fields .= '<input type="hidden" name="nt" value="'. $nt_type .'" />';
			$s_hidden_fields .= '<input type="hidden" name="dm" value="'. $dm_type .'" />';
			$s_hidden_fields .= '<input type="hidden" name="mode" value="save" />';
			$s_hidden_fields .= '<input type="hidden" name="indb" value="'.$indb.'" />';

			if ($indb != '')
			{
				$sql = "SELECT message_format, name, description, file_name
						FROM " . MESSAGE_FORMAT_TABLE . " 
						WHERE message_format = ".$indb;
				$result = $db->sql_query($sql);
				if( !$result )
				{
					message_die(GENERAL_ERROR, "Couldn't get template information.", "", __LINE__, __FILE__, $sql );
				}
				$template_settings = $db->sql_fetchrow($result);
			}
		} else {
			$template_caption = "New Template";
			$nt_select = '<select name="TXT_NT_TYPE"><option value="">-- Select a notification type --</option>';
			foreach ($nt_types as $key => $value) {
				$nt_select .= '<option value="'. $key . '">'. $value .'</option>';
			}
			$nt_select .= '</select>';
			$dm_select = '<select name="TXT_DM_TYPE"><option value="">-- Select a delivery method --</option>';
			foreach ($dm_types as $key => $value) {
				$dm_select .= '<option value="'. $key . '">'. $value .'</option>';
			}
			$dm_select .= '</select>';
			$template->assign_block_vars("words", array(
				"L_DELIVERY_METHOD" => $lang['delivery'],
				"L_NOTIFICATION_TYPE" => $lang['notification'],
				
				"DELIVERY_METHOD" => $dm_select,
				"NOTIFICATION_TYPE" => $nt_select)
			);
			
			$s_hidden_fields .= '<input type="hidden" name="mode" value="save" />';
		}

		$template->assign_vars(array(
			"L_TEMPLATE_TITLE" => $lang['template_title'],
			"L_TEMPLATE_EXPLAIN" => $lang['template_desc'],
			"L_TEMPLATE_CONFIG" => $template_caption,
			
			"L_MESSAGE_FORMAT" => "ID",
			"L_NAME" => "Name",
			"L_DESCRIPTION" => "Description",
			"L_FILE_NAME" => "File Name",
			"L_HEADER_CODE" => "Header Section",
			"L_BODY_CODE" => "Body Section",
			"L_FOOTER_CODE" => "Footer Section",
			
			"MESSAGE_FORMAT" => $template_settings['message_format'],
			"NAME" => ($indb) ? $template_settings['name'] : $temp_id,
			"DESCRIPTION" => $template_settings['description'],
			"FILE_NAME" => ($indb) ? $template_settings['file_name'] : $temp_id,


			"L_SUBMIT" => $lang['Submit'],

			"S_SERVER_ACTION" => append_sid("admin_template.$phpEx"),
			"L_TEMPLATE_CODE" => $lang['template_code'],
			"L_SUBMIT" => $lang['Submit'],
			"L_RESET" => $lang['Reset'],

			"FILE_HEADER" => $file_header, 
			"FILE_BODY" => $file_body, 
			"FILE_FOOTER" => $file_footer, 

			"S_TEMPLATE_ACTION" => append_sid("admin_template.$phpEx"),
			"S_HIDDEN_FIELDS" => $s_hidden_fields) 
		);

		$template->pparse("body");
	}
	else if( $mode == "view" )
	{

		$temp_id = ( !empty($HTTP_POST_VARS['id']) ) ? $HTTP_POST_VARS['id'] : $HTTP_GET_VARS['id'];
		$nt_type = ( !empty($HTTP_POST_VARS['nt']) ) ? $HTTP_POST_VARS['nt'] : $HTTP_GET_VARS['nt'];
		$dm_type = ( !empty($HTTP_POST_VARS['dm']) ) ? $HTTP_POST_VARS['dm'] : $HTTP_GET_VARS['dm'];
		$indb = ( !empty($HTTP_POST_VARS['indb']) ) ? $HTTP_POST_VARS['indb'] : $HTTP_GET_VARS['indb'];
		$file_prefix = $sc_root_path. "../templates/$nt_type/$dm_type/$temp_id";
		$file_suffix = ($dm_type == "SCRIPT") ? "pl" : "txt";
		$file_header = fread(fopen($file_prefix."_header.".$file_suffix, "r"), filesize($file_prefix."_header.".$file_suffix));
		$file_body = fread(fopen($file_prefix."_body.".$file_suffix, "r"), filesize($file_prefix."_body.".$file_suffix));
		$file_footer = fread(fopen($file_prefix."_footer.".$file_suffix, "r"), filesize($file_prefix."_footer.".$file_suffix));

		$template->set_filenames(array(
			"body" => "admin/template_view_body.tpl")
		);

		$s_hidden_fields = '<input type="hidden" name="mode" value="edit" />';
		$s_hidden_fields .= '<input type="hidden" name="id" value="'. $temp_id .'" />';
		$s_hidden_fields .= '<input type="hidden" name="nt" value="'. $nt_type .'" />';
		$s_hidden_fields .= '<input type="hidden" name="dm" value="'. $dm_type .'" />';
		$s_hidden_fields .= '<input type="hidden" name="indb" value="'. $indb .'" />';

		$template->assign_vars(array(
		"SMILEY_CODE" => $smile_data['code'],
		"SMILEY_EMOTICON" => $smile_data['emoticon'],

		"L_TEMPLATE_TITLE" => $lang['template_title'],
		"L_TEMPLATE_CONFIG" => "'$nt_type-$dm_type-$temp_id' Template",
		"L_TEMPLATE_EXPLAIN" => $lang['template_desc'],
		"L_TEMPLATE_CODE" => $lang['template_code'],

		"L_SUBMIT" => $lang['Edit'],
		"L_RESET" => $lang['Reset'],

		"FILE_HEADER" => preg_replace("/\n/s", "<br>", $file_header), 
		"FILE_BODY" => preg_replace("/\n/s", "<br>", $file_body), 
		"FILE_FOOTER" => preg_replace("/\n/s", "<br>", $file_footer), 

		"S_HIDDEN_FIELDS" => $s_hidden_fields)
		);

		$template->pparse("body");
	}
	else if( $mode == "save")
	{
		//
		// Get the submitted data, being careful to ensure that we only
		// accept the data we are looking for.
		//
		$temp_id = ( !empty($HTTP_POST_VARS['id']) ) ? $HTTP_POST_VARS['id'] : $HTTP_GET_VARS['id'];
		$nt_type = ( !empty($HTTP_POST_VARS['nt']) ) ? $HTTP_POST_VARS['nt'] : $HTTP_GET_VARS['nt'];
		$dm_type = ( !empty($HTTP_POST_VARS['dm']) ) ? $HTTP_POST_VARS['dm'] : $HTTP_GET_VARS['dm'];
		$indb = ( !empty($HTTP_POST_VARS['indb']) ) ? $HTTP_POST_VARS['indb'] : $HTTP_GET_VARS['indb'];
		$name = ( !empty($HTTP_POST_VARS['name']) ) ? $HTTP_POST_VARS['name'] : '';
		$file_name = ( !empty($HTTP_POST_VARS['file_name']) ) ? $HTTP_POST_VARS['file_name'] : '';
		$description = ( !empty($HTTP_POST_VARS['description']) ) ? $HTTP_POST_VARS['description'] : '';
		// If no code was entered complain ...
		if ($nt_type == '' || $temp_id == '' || $dm_type == '')
		{
			$nt_type = ( !empty($HTTP_POST_VARS['TXT_NT_TYPE']) ) ? $HTTP_POST_VARS['TXT_NT_TYPE'] : '';
			$dm_type = ( !empty($HTTP_POST_VARS['TXT_DM_TYPE']) ) ? $HTTP_POST_VARS['TXT_DM_TYPE'] : '';
			if ($nt_type == '' || $dm_type == '')
			{
				message_die(GENERAL_MESSAGE, $lang['Fields_empty']);
			}
		}
		
		$file_prefix = $sc_root_path. "../templates/$nt_type/$dm_type/";
		$file_suffix = ($dm_type == "SCRIPT") ? "pl" : "txt";
		$file_header = ( !empty($HTTP_POST_VARS['file_header']) ) ? $HTTP_POST_VARS['file_header'] : '';
		$file_body = ( !empty($HTTP_POST_VARS['file_body']) ) ? $HTTP_POST_VARS['file_body'] : '';
		$file_footer = ( !empty($HTTP_POST_VARS['file_footer']) ) ? $HTTP_POST_VARS['file_footer'] : '';


		$fptr = @fopen($file_prefix.$file_name.'_header.'.$file_suffix, 'w');
		$bytes_written = @fwrite($fptr, $file_header);
		@fclose($fptr);
		$fptr = @fopen($file_prefix.$file_name.'_body.'.$file_suffix, 'w');
		$bytes_written = @fwrite($fptr, $file_body);
		@fclose($fptr);
		$fptr = @fopen($file_prefix.$file_name.'_footer.'.$file_suffix, 'w');
		$bytes_written = @fwrite($fptr, $file_footer);
		@fclose($fptr);

		if ($temp_id != 'default' and $temp_id != '')
		{
			unset($notification_template[$nt_type][$dm_type][$temp_id]);
		}
		$notification_template[$nt_type][$dm_type][$file_name] = $file_name;

		$fptr = @fopen($sc_root_path. "../templates/template_info.cfg", 'w');
		$bytes_written = @fwrite($fptr, "<?php\n");
		foreach ($nt_types as $nt_type => $nt_name) 
		{
			foreach ($dm_types as $dm_type => $dm_name) 
			{
				foreach ($notification_template[$nt_type][$dm_type] as $key => $value)
				{
					$bytes_written = @fwrite($fptr, "\$notification_template['".$nt_type."']['".$dm_type."']['".$key."'] = '".$key."';"."\n");
				}
			}
		}
		$bytes_written = @fwrite($fptr, "?>\n");
		@fclose($fptr);

		//
		// Proceed with updating the message_format table.
		//
		$message = $lang['template_edited'] . "<br /><br />" . sprintf($lang['Click_return_templateadmin'], "<a href=\"" . append_sid("admin_template.$phpEx") . "\">", "</a>") . "<br /><br />" . sprintf($lang['Click_return_admin_index'], "<a href=\"" . append_sid("index.$phpEx?pane=right") . "\">", "</a>");
		if ($indb) 
		{
			$sql = "UPDATE " . MESSAGE_FORMAT_TABLE . "
				SET name = '" . str_replace("\'", "''", $name) . "', file_name = '" . str_replace("\'", "''", $file_name) . "', description = '" . str_replace("\'", "''", $description) . "',
					update_username = '".$admin_userdata['username']."', update_timestamp = now()
					WHERE message_format = $indb";
			if( !($result = $db->sql_query($sql)) )
			{
				message_die(GENERAL_ERROR, "Couldn't update teamplate info", "", __LINE__, __FILE__, $sql);
			}
		} 
		else if ($file_name != 'default' and $name != 'default')
		{
			$sql = "INSERT INTO " . MESSAGE_FORMAT_TABLE . " (name, file_name, description, update_username, update_timestamp)
				values ( '" . str_replace("\'", "''", $name) . "', '" . str_replace("\'", "''", $file_name) . "', '" . str_replace("\'", "''", $description) . "',
					 '".$admin_userdata['username']."',  now())";
			if( !($result = $db->sql_query($sql)) )
			{
				message_die(GENERAL_ERROR, "Couldn't add teamplate info", "", __LINE__, __FILE__, $sql);
			}
			$message = $lang['template_added'] . "<br /><br />" . sprintf($lang['Click_return_templateadmin'], "<a href=\"" . append_sid("admin_template.$phpEx") . "\">", "</a>") . "<br /><br />" . sprintf($lang['Click_return_admin_index'], "<a href=\"" . append_sid("index.$phpEx?pane=right") . "\">", "</a>");
		} 
		else if ($file_name != 'default' and $name == 'default')
		{
			$message = "Template saved but database not updated.  Template name cannot be 'default'.<br /><br />" . sprintf($lang['Click_return_templateadmin'], "<a href=\"" . append_sid("admin_template.$phpEx") . "\">", "</a>") . "<br /><br />" . sprintf($lang['Click_return_admin_index'], "<a href=\"" . append_sid("index.$phpEx?pane=right") . "\">", "</a>");
		} 

		message_die(GENERAL_MESSAGE, $message);
	}
	else if( $mode == "delete" )
	{
		//
		// Admin has selected to delete a template.
		//

		$temp_id = ( !empty($HTTP_POST_VARS['id']) ) ? $HTTP_POST_VARS['id'] : $HTTP_GET_VARS['id'];
		$nt_type = ( !empty($HTTP_POST_VARS['nt']) ) ? $HTTP_POST_VARS['nt'] : $HTTP_GET_VARS['nt'];
		$dm_type = ( !empty($HTTP_POST_VARS['dm']) ) ? $HTTP_POST_VARS['dm'] : $HTTP_GET_VARS['dm'];
		$indb = ( !empty($HTTP_POST_VARS['indb']) ) ? $HTTP_POST_VARS['indb'] : $HTTP_GET_VARS['indb'];
		if ($temp_id == 'default' || $temp_id == '')
		{
			message_die(GENERAL_ERROR, "template file not defined or 'default'");
		}

		$confirm = isset($HTTP_POST_VARS['confirm']);

		if( $confirm )
		{
			if ($indb)
			{
				$sql = "DELETE FROM " . MESSAGE_FORMAT_TABLE . "
					WHERE message_format = $indb";
				$result = $db->sql_query($sql);
				if( !$result )
				{
					message_die(GENERAL_ERROR, "Couldn't delete template", "", __LINE__, __FILE__, $sql);
				}
			}

			if ($temp_id != 'default' && $temp_id != '')
			{
				unset($notification_template[$nt_type][$dm_type][$temp_id]);
			}
			else 
			{
				message_die(GENERAL_ERROR, "template file not defined or 'default'", "", __LINE__, __FILE__, $sql);
			}
			$fptr = @fopen($sc_root_path. "../templates/template_info.cfg", 'w');
			$bytes_written = @fwrite($fptr, "<?php\n");
			foreach ($nt_types as $nt_type => $nt_name) 
			{
				foreach ($dm_types as $dm_type => $dm_name) 
				{
					foreach ($notification_template[$nt_type][$dm_type] as $key => $value)
					{
						$bytes_written = @fwrite($fptr, "\$notification_template['".$nt_type."']['".$dm_type."']['".$key."'] = '".$key."';"."\n");
					}
				}
			}
			$bytes_written = @fwrite($fptr, "?>\n");
			@fclose($fptr);
			$file_prefix = $sc_root_path. "../templates/$nt_type/$dm_type/$temp_id";
			$file_suffix = ($dm_type == "SCRIPT") ? "pl" : "txt";
			$file_header = ( !empty($HTTP_POST_VARS['file_header']) ) ? $HTTP_POST_VARS['file_header'] : '';
			unlink($file_prefix.'_header.'.$file_suffix);
			unlink($file_prefix.'_body.'.$file_suffix);
			unlink($file_prefix.'_footer.'.$file_suffix);

			$message = $lang['template_deleted'] . "<br /><br />" . sprintf($lang['Click_return_templateadmin'], "<a href=\"" . append_sid("admin_template.$phpEx") . "\">", "</a>") . "<br /><br />" . sprintf($lang['Click_return_admin_index'], "<a href=\"" . append_sid("index.$phpEx?pane=right") . "\">", "</a>");

			message_die(GENERAL_MESSAGE, $message);
		}
		else
		{
			// Present the confirmation screen to the user
			$template->set_filenames(array(
				'body' => 'admin/confirm_body.tpl')
			);

			$hidden_fields = '<input type="hidden" name="mode" value="delete" />
				<input type="hidden" name="id" value="' . $temp_id . '" />
				<input type="hidden" name="nt" value="' . $nt_type . '" />
				<input type="hidden" name="dm" value="' . $dm_type . '" />
				<input type="hidden" name="indb" value="' . $indb . '" />';

			$template->assign_vars(array(
				'MESSAGE_TITLE' => $lang['Confirm'],
				'MESSAGE_TEXT' => $lang['Confirm_delete_template'],

				'L_YES' => $lang['Yes'],
				'L_NO' => $lang['No'],

				'S_CONFIRM_ACTION' => append_sid("admin_template.$phpEx"),
				'S_HIDDEN_FIELDS' => $hidden_fields)
			);
			$template->pparse('body');
		}
	}
}
else
{
	//
	// This is the main display of the page before the admin has selected
	// any options.
	//
	$message_formats = array();
	$sql = "SELECT message_format, name, description, file_name 
			FROM " . MESSAGE_FORMAT_TABLE;
	if($result = $db->sql_query($sql))
	{
		while ($row = $db->sql_fetchrow($result))
		{
			$message_formats[$row['file_name']] = $row;
		}
	}

	$template->set_filenames(array(
		"body" => "admin/template_list_body.tpl")
	);

	$template->assign_vars(array(
		"L_WORDS_TITLE" => $lang['template_title'],
		"L_WORDS_TEXT" => $lang['template_explain'],
		
		"L_MESSAGE_FORMAT" => "Notification Type",
		"L_NAME" => "Message Type",
		"L_FILE_NAME" => "File Name",
		"L_DESCRIPTION" => "Description",

		"L_VIEW" => "View",
		"L_EDIT" => "Edit",
		"L_DELETE" => "Delete",
		"L_ADD_EVENT" => "Add new template",
		"L_ACTION" => $lang['Action'],

		"S_WORDS_ACTION" => append_sid("admin_template.$phpEx?mode=add"),
		"S_HIDDEN_FIELDS" => '')
	);

	//
	// Loop throuh the rows of smilies setting block vars for the template.
	//
	$color_row = 0;
	foreach ($nt_types as $nt_type => $nt_name) 
	{
		foreach ($dm_types as $dm_type => $dm_name) 
		{
			foreach ($notification_template[$nt_type][$dm_type] as $key => $value)
			{
				//
				// Replace htmlentites for < and > with actual character.
				//
				$color_row = 1-$color_row;
				$row_color = ( !$color_row ) ? $theme['td_color1'] : $theme['td_color2'];
				$row_class = ( !$color_row ) ? $theme['td_class1'] : $theme['td_class2'];
		
				$indb = (isset($message_formats[$notification_template[$nt_type][$dm_type][$key]])) ? 
					$message_formats[$notification_template[$nt_type][$dm_type][$key]]['message_format'] : '';
				$template->assign_block_vars("words", array(
					"ROW_COLOR" => "#" . $row_color,
					"ROW_CLASS" => $row_class,
					
					"NOTIFICATION_REQUEST" =>  $nt_name, 
					"DELIVERY_METHOD" => $dm_name,
					"TEMPLATE_NAME" => $notification_template[$nt_type][$dm_type][$key],
					"DESCRIPTION" => $message_formats[$notification_template[$nt_type][$dm_type][$key]]['description'],
					
					"U_VIEW" => append_sid("admin_template.$phpEx?mode=view&amp;nt=$nt_type&dm=$dm_type&indb=$indb&id=" . $notification_template[$nt_type][$dm_type][$key]), 
					"U_EDIT" => append_sid("admin_template.$phpEx?mode=edit&amp;nt=$nt_type&dm=$dm_type&indb=$indb&id=" . $notification_template[$nt_type][$dm_type][$key]), 
					"U_DELETE" => append_sid("admin_template.$phpEx?mode=delete&amp;nt=$nt_type&dm=$dm_type&indb=$indb&id=" . $notification_template[$nt_type][$dm_type][$key]))
				);
			}
		}
	}
	//
	// Spit out the page.
	//
	$template->pparse("body");
}

//
// Page Footer
//
include('./page_footer_admin.'.$phpEx);

?>