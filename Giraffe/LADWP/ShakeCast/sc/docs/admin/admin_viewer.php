<?php
/*
# $Id: admin_viewer.php 252 2008-01-11 17:59:46Z klin $

# The PHP interface was originally inspired by PHPBB,
# file/database formats and directory structure are quite similar.

##############################################################################
# 
# Terms and Conditions of Software Use
# ====================================
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Disclaimer of Earthquake Information
# ====================================
# 
# The data and maps provided through this system are preliminary data
# and are subject to revision. They are computer generated and may not
# have received human review or official approval. Inaccuracies in the
# data may be present because of instrument or computer
# malfunctions. Subsequent review may result in significant revisions to
# the data. All efforts have been made to provide accurate information,
# but reliance on, or interpretation of earthquake data from a single
# source is not advised. Data users are cautioned to consider carefully
# the provisional nature of the information before using it for
# decisions that concern personal or public safety or the conduct of
# business that involves substantial monetary or operational
# consequences.
# 
# Disclaimer of Software and its Capabilities
# ===========================================
# 
# This software is provided as an "as is" basis.  Attempts have been
# made to rid the program of software defects and bugs, however the
# U.S. Geological Survey (USGS) have no obligations to provide maintenance, 
# support, updates, enhancements or modifications. In no event shall USGS 
# be liable to any party for direct, indirect, special, incidental or 
# consequential damages, including lost profits, arising out of the use 
# of this software, its documentation, or data obtained though the use 
# of this software, even if USGS or have been advised of the
# possibility of such damage. By downloading, installing or using this
# program, the user acknowledges and understands the purpose and
# limitations of this software.
# 
# Contact Information
# ===================
# 
# Coordination of this effort is under the auspices of the USGS Advanced
# National Seismic System (ANSS) coordinated in Golden, Colorado, which
# functions as the clearing house for development, distribution,
# documentation, and support. For questions, comments, or reports of
# potential bugs regarding this software please contact wald@usgs.gov or
# klin@usgs.gov.  
#
#############################################################################
*/

if( !empty($setmodules) )
{
	$filename = basename(__FILE__);
	$module[' General Admin']['Log Viewer'] = $filename;
	return;
}

define('IN_SC', 1);
@set_time_limit(120);

function read_file($file, $lines)
{
       $handle = fopen($file, "r");
       $linecounter = $lines;
       $pos = -2;
       $beginning = false;
       $text = array();
       while ($linecounter > 0) {
         $t = " ";
         while ($t != "\n") {
           if(fseek($handle, $pos, SEEK_END) == -1) {
			$beginning = true; break; }
           $t = fgetc($handle);
           $pos --;
         }
         $linecounter --;
         if($beginning) rewind($handle);
         $text[$lines-$linecounter-1] = fgets($handle);
         if($beginning) break;
       }
       fclose ($handle);
       return array_reverse($text); // array_reverse is optional: you can also just return the $text array which consists of the file's lines.
       //return $text; // array_reverse is optional: you can also just return the $text array which consists of the file's lines.
}

//
// Load default header
//
$sc_root_path = "./../";
require($sc_root_path . 'extension.inc');
require('./pagestart.' . $phpEx);
include($sc_root_path . 'includes/functions_selects.'.$phpEx);

$dir = @opendir($board_config['LogDir']);

while( $file = @readdir($dir) )
{
	if( preg_match("/^.*?\.log$/", $file) )
	{
		$logfiles[$file] = $board_config['LogDir']. '/' . $file;
	}
}

@closedir($dir);

if( isset($HTTP_GET_VARS['mode']) || isset($HTTP_POST_VARS['mode']) )
{
	$mode = (isset($HTTP_GET_VARS['mode'])) ? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode'];
	$mode = htmlspecialchars($mode);
}
else 
{
	$mode = "";
}

// Restrict mode input to valid options
$mode = ( in_array($mode, array('view')) ) ? $mode : '';
$default_lines = 500;
$default_max_lines = 5000;
$default_filter = 'no_filter';

if( $mode == "view" )
{
	$s_hidden_fields = '<input type="hidden" name="mode" value="'.$mode.'" />';
	if( isset($HTTP_POST_VARS['id']) ||  isset($HTTP_GET_VARS['id']) )
	{
		$log_id = ( isset($HTTP_POST_VARS['id']) ) ? $HTTP_POST_VARS['id'] : $HTTP_GET_VARS['id'];
	}
	else
	{
		$log_id = '';
	}
	$s_hidden_fields .= '<input type="hidden" name="id" value="'.$log_id.'" />';

	if( isset($HTTP_POST_VARS['logfilter']) ||  isset($HTTP_GET_VARS['logfilter']) )
	{
		$logfilter = ( isset($HTTP_POST_VARS['logfilter']) ) ? $HTTP_POST_VARS['logfilter'] : $HTTP_GET_VARS['logfilter'];
	}
	else
	{
		$logfilter = $default_filter;
	}

	if( isset($HTTP_POST_VARS['linesselect']) ||  isset($HTTP_GET_VARS['linesselect']) )
	{
		$lines = ( isset($HTTP_POST_VARS['linesselect']) ) ? $HTTP_POST_VARS['linesselect'] : $HTTP_GET_VARS['linesselect'];
		$lines = ($lines == 'All') ? $default_max_lines : $lines;
	}
	else
	{
		$lines = $default_lines;
	}

	$reverse_order = ( isset($HTTP_POST_VARS['reverse_order']) ) ? $HTTP_POST_VARS['reverse_order'] : 0;
	$reverse_order_yes = ( $reverse_order ) ? "checked=\"checked\"" : "";
	$reverse_order_no = ( !$reverse_order ) ? "checked=\"checked\"" : "";

	if ( !isset($logfiles[$log_id]))
	{
		message_die(GENERAL_MESSAGE, $lang['No_event_selected']);
	}
	
	$template->set_filenames(array(
		"body" => "admin/viewer_body.tpl")
	);

	$records = $reverse_order ? array_reverse(read_file($logfiles[$log_id], $lines)) : read_file($logfiles[$log_id], $lines);

	$logfilter_select = logfilter_select($logfilter, 'logfilter');
	$lines_select = lines_select($lines, 'linesselect');
	$template->assign_vars(array(
		"L_WORDS_TITLE" => $lang['Log_Viewer'],
		"L_WORDS_TEXT" => $lang['Log_Viewer_explain'],
		"L_INDEX" => $lang['Index'],
		"L_LOG_ENTRY" => $lang['Log_Entry'],

		"L_DISPLAY" => "Reverse Listing: ",
		"L_YES" => $lang['Yes'],
		"L_NO" => $lang['No'],
		"L_SUBMIT" => $lang['Submit'], 
		"L_RESET" => $lang['Reset'], 
		
		"LOG_FILE" => ": ".$log_id,
		"LogFilter" => "Service Filter: ".$logfilter_select,
		"LinesSelect" => "# Lines: ".$lines_select,
		"reverse_order" => $reverse_order_yes,
		"reverse_order_NO" => $reverse_order_no,
		"S_WORDS_ACTION" => append_sid("admin_viewer.$phpEx?mode=view"),
		"S_HIDDEN_FIELDS" => $s_hidden_fields)
	);

	// now split into records
	//for ($index = $start; $index < $sizerecs && $index < $board_config['topics_per_page'] + $start; $index++) {
	for ($index = 0; $index < sizeof($records); $index++) {

		$record = $records[$index];
		if ($logfilter != $default_filter) 
		{
			preg_match("/(".$logfilter."[d|w]*\[)/", $record, $match);
			if (!$match) {continue;}
		}
		$row_color = ( !($i % 2) ) ? $theme['td_color1'] : $theme['td_color2'];
		$row_class = ( !($i % 2) ) ? $theme['td_class1'] : $theme['td_class2'];

		$template->assign_block_vars("words", array(
			"ROW_COLOR" => "#" . $row_color,
			"ROW_CLASS" => $row_class,
			"INDEX" => ($index+1).'>',
			"LOG_ENTRY" => htmlentities($record))
		);

	}
}
else
{
	$template->set_filenames(array(
		"body" => "admin/viewer_list_body.tpl")
	);

	$template->assign_vars(array(
		"L_WORDS_TITLE" => $lang['Log_Viewer'],
		"L_WORDS_TEXT" => $lang['Log_Viewer_explain'],
		"L_LOG_FILE" => $lang['Log_File'],
		"L_FILE_SIZE" => $lang['File_Size'],
		"L_DESCRIPTION" => $lang['Log_Description'],
		"L_VIEW" => $lang['View'],
		"L_ACTION" => $lang['Action'],

		"S_WORDS_ACTION" => append_sid("admin_viewer.$phpEx"),
		"S_HIDDEN_FIELDS" => '')
	);

	foreach ($logfiles as $logfile => $logpath)
	{
		$flesize = filesize($logpath);
		if( $flesize >= 1048576 )
		{
			$flesize = sprintf("%.2f MB", ( $flesize / 1048576 ));
		}
		else if( $flesize >= 1024 )
		{
			$flesize = sprintf("%.2f KB", ( $flesize / 1024 ));
		}
		else
		{
			$flesize = sprintf("%.2f Bytes", $flesize);
		}

		$sql = "SELECT *
			FROM " . CONFIG_TABLE . "
			WHERE config_value like '%" . $logfile ."%'";
		if(!$result = $db->sql_query($sql))
		{
			message_die(CRITICAL_ERROR, "Could not query config information in admin_service", "", __LINE__, __FILE__, $sql);
		}
		else
		{
			$description = '';
			while( $row = $db->sql_fetchrow($result) )
			{
				$config_name = $row['config_name'];
				$config_value = $row['config_value'];
				$description .= " ".$config_name;
			}
		}
		
		$row_color = ( !($i % 2) ) ? $theme['td_color1'] : $theme['td_color2'];
		$row_class = ( !($i % 2) ) ? $theme['td_class1'] : $theme['td_class2'];

		$template->assign_block_vars("words", array(
			"ROW_COLOR" => "#" . $row_color,
			"ROW_CLASS" => $row_class,

			"LOG_FILE" => $logfile,
			"FILE_SIZE" => $flesize,
			"DESCRIPTION" => $description,

			"U_VIEW" => append_sid("admin_viewer.$phpEx?mode=view&amp;id=$logfile"))
		);
	}

}

$template->pparse("body");

include('./page_footer_admin.'.$phpEx);

?>