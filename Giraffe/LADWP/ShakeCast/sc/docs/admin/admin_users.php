<?php
/*
# $Id: admin_users.php 511 2008-10-20 14:51:19Z klin $

# The PHP interface was originally inspired by PHPBB,
# file/database formats and directory structure are quite similar.

##############################################################################
# 
# Terms and Conditions of Software Use
# ====================================
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Disclaimer of Earthquake Information
# ====================================
# 
# The data and maps provided through this system are preliminary data
# and are subject to revision. They are computer generated and may not
# have received human review or official approval. Inaccuracies in the
# data may be present because of instrument or computer
# malfunctions. Subsequent review may result in significant revisions to
# the data. All efforts have been made to provide accurate information,
# but reliance on, or interpretation of earthquake data from a single
# source is not advised. Data users are cautioned to consider carefully
# the provisional nature of the information before using it for
# decisions that concern personal or public safety or the conduct of
# business that involves substantial monetary or operational
# consequences.
# 
# Disclaimer of Software and its Capabilities
# ===========================================
# 
# This software is provided as an "as is" basis.  Attempts have been
# made to rid the program of software defects and bugs, however the
# U.S. Geological Survey (USGS) have no obligations to provide maintenance, 
# support, updates, enhancements or modifications. In no event shall USGS 
# be liable to any party for direct, indirect, special, incidental or 
# consequential damages, including lost profits, arising out of the use 
# of this software, its documentation, or data obtained though the use 
# of this software, even if USGS or have been advised of the
# possibility of such damage. By downloading, installing or using this
# program, the user acknowledges and understands the purpose and
# limitations of this software.
# 
# Contact Information
# ===================
# 
# Coordination of this effort is under the auspices of the USGS Advanced
# National Seismic System (ANSS) coordinated in Golden, Colorado, which
# functions as the clearing house for development, distribution,
# documentation, and support. For questions, comments, or reports of
# potential bugs regarding this software please contact wald@usgs.gov or
# klin@usgs.gov.  
#
#############################################################################
*/

define('IN_SC', 1);

if( !empty($setmodules) )
{
	$filename = basename(__FILE__);
	$module['Users']['Manage'] = $filename;

	return;
}

$sc_root_path = './../';
$update_password = '/shakecast/sc/bin/update_password.pl';
require($sc_root_path . 'extension.inc');
require('./pagestart.' . $phpEx);
require($sc_root_path . 'includes/functions_validate.'.$phpEx);

$html_entities_match = array('#<#', '#>#');
$html_entities_replace = array('&lt;', '&gt;');

//
// Set mode
//
if( isset( $HTTP_POST_VARS['mode'] ) || isset( $HTTP_GET_VARS['mode'] ) )
{
	$mode = ( isset( $HTTP_POST_VARS['mode']) ) ? $HTTP_POST_VARS['mode'] : $HTTP_GET_VARS['mode'];
	$mode = htmlspecialchars($mode);
}
else
{
	$mode = '';
}

if( isset( $HTTP_POST_VARS['username'] ) || isset( $HTTP_GET_VARS['username'] ) )
{
	$username = ( isset( $HTTP_POST_VARS['username']) ) ? $HTTP_POST_VARS['username'] : $HTTP_GET_VARS['username'];
	$username = htmlspecialchars($username);
}
else
{
	$username = '';
}

//
// Begin program
//
// Restrict mode input to valid options
$mode = ( in_array($mode, array('add', 'edit', 'save', 'delete')) ) ? $mode : '';

if( $mode != "" )
{
	if ( $mode == 'edit' || $mode == 'add' )
	{
		if( $username != '' )
		{
			if( isset( $HTTP_GET_VARS[POST_USERS_URL]) || isset( $HTTP_POST_VARS[POST_USERS_URL]) )
			{
				$user_id = ( isset( $HTTP_POST_VARS[POST_USERS_URL]) ) ? intval( $HTTP_POST_VARS[POST_USERS_URL]) : intval( $HTTP_GET_VARS[POST_USERS_URL]);
				$this_userdata = get_userdata($user_id);
				if( !$this_userdata )
				{
					message_die(GENERAL_MESSAGE, $lang['No_user_id_specified'] );
				}
			}
			else
			{
				$this_userdata = get_userdata($username, true);
				if( !$this_userdata )
				{
					message_die(GENERAL_MESSAGE, $lang['No_user_id_specified'] );
				}
			}
	
			//
			// Now parse and display it as a template
			//
			$user_id = $this_userdata['user_id'];
			$username = $this_userdata['username'];
			$email = $this_userdata['user_email'];
			$password = '';
			$password_confirm = '';
	
			$fullname = htmlspecialchars($this_userdata['user_fullname']);
			$location = htmlspecialchars($this_userdata['user_from']);
			$occupation = htmlspecialchars($this_userdata['user_occ']);
			$organization = htmlspecialchars($this_userdata['user_organization']);
	
			$user_status = $this_userdata['user_active'];
			$user_delivery_method = get_user_delivery_method($user_id);
		}
		$s_user_level = ( $this_userdata['user_level'] == ADMIN ) ? '<option value="admin" selected="selected">' . $lang['Auth_Admin'] . '</option><option value="user">' . $lang['Auth_User'] . '</option>' : '<option value="admin">' . $lang['Auth_Admin'] . '</option><option value="user" selected="selected">' . $lang['Auth_User'] . '</option>';
	
		$s_hidden_fields = '<input type="hidden" name="mode" value="save" /><input type="hidden" name="agreed" value="true" /><input type="hidden" name="coppa" value="' . $coppa . '" />';
		$s_hidden_fields .= '<input type="hidden" name="source_mode" value="' . $mode . '" /><input type="hidden" name="id" value="' . $this_userdata['user_id'] . '" />';
		$dm_types = get_delivery_method_type();
	
		$template->set_filenames(array(
			"body" => "admin/user_edit_body.tpl")
		);
		
		//
		// Let's do an overall check for settings/versions which would prevent
		// us from doing file uploads....
		//
		$ini_val = ( phpversion() >= '4.0.0' ) ? 'ini_get' : 'get_cfg_var';
		$form_enctype = ( !@$ini_val('file_uploads') || phpversion() == '4.0.4pl1' || !$board_config['allow_avatar_upload'] || ( phpversion() < '4.0.3' && @$ini_val('open_basedir') != '' ) ) ? '' : 'enctype="multipart/form-data"';
	
		$template->assign_vars(array(
			'USERNAME' => $username,
			'EMAIL' => $email,
			'OCCUPATION' => $occupation,
			'ORGANIZATION' => $organization,
			'LOCATION' => $location,
			'FULLNAME' => $fullname,
			'USER_ACTIVE_YES' => ($user_status) ? 'checked="checked"' : '',
			'USER_ACTIVE_NO' => (!$user_status) ? 'checked="checked"' : '', 
			'USER_LEVEL_BOX' => $s_user_level,
			'EMAIL_HTML' => $user_delivery_method['EMAIL_HTML'],
			'EMAIL_TEXT' => $user_delivery_method['EMAIL_TEXT'],
			'PAGER' => $user_delivery_method['PAGER'],
	
			'L_USERNAME' => $lang['Username'],
			'L_USER_TITLE' => $lang['User_admin'],
			'L_USER_EXPLAIN' => $lang['User_admin_explain'],
			'L_NEW_PASSWORD' => $lang['New_password'], 
			'L_PASSWORD_IF_CHANGED' => $lang['password_if_changed'],
			'L_CONFIRM_PASSWORD' => $lang['Confirm_password'],
			'L_PASSWORD_CONFIRM_IF_CHANGED' => $lang['password_confirm_if_changed'],
			'L_SUBMIT' => $lang['Submit'],
			'L_RESET' => $lang['Reset'],
			'L_FULLNAME' => $lang['Fullname'],
			'L_LOCATION' => $lang['Location'],
			'L_OCCUPATION' => $lang['Occupation'],
			'L_ORGANIZATION' => $lang['organization'],
			'L_YES' => $lang['Yes'],
			'L_NO' => $lang['No'],
			
			'L_SPECIAL' => $lang['User_special'],
			'L_SPECIAL_EXPLAIN' => $lang['User_special_explain'],
			'L_USER_ACTIVE' => $lang['User_status'],
			
			'L_ITEMS_REQUIRED' => $lang['Items_required'],
			'L_REGISTRATION_INFO' => $lang['Registration_info'],
			'L_PROFILE_INFO' => $lang['Contact_info'],
			'L_PROFILE_INFO_NOTICE' => $lang['Profile_info_warn'],
			'L_EMAIL_ADDRESS' => $lang['Email_address'],

			'L_EMAIL_HTML' => $dm_types['EMAIL_HTML'],
			'L_EMAIL_TEXT' => $dm_types['EMAIL_TEXT'],
			'L_PAGER' => $dm_types['PAGER'],
			'L_DELIVERY_METHOD_INFO' => $lang['Delivery_method_info'],

			'S_FORM_ENCTYPE' => $form_enctype,
	
			'L_DELETE_USER' => $lang['User_delete'],
			'L_DELETE_USER_EXPLAIN' => $lang['User_delete_explain'],
			"L_SELECT_USER_LEVEL" => $lang['User_Level'],
	
			'S_HIDDEN_FIELDS' => $s_hidden_fields,
			'S_PROFILE_ACTION' => append_sid("admin_users.$phpEx"))
		);
	
	}
	//
	// Ok, the profile has been modified and submitted, let's update
	//
	else if ( $mode == 'save' )
	{
		$user_id = intval($HTTP_POST_VARS['id']);
		if( isset( $HTTP_POST_VARS['source_mode'] ) || isset( $HTTP_GET_VARS['source_mode'] ) )
		{
			$source_mode = ( isset( $HTTP_POST_VARS['source_mode']) ) ? $HTTP_POST_VARS['source_mode'] : $HTTP_GET_VARS['source_mode'];
			$source_mode = htmlspecialchars($source_mode);
		}

		//if (!($this_userdata = get_userdata($user_id)) && $source_mode = 'edit')
		//{
		//	message_die(GENERAL_MESSAGE, $lang['No_user_id_specified'] );
		//}

		if( $HTTP_POST_VARS['deleteuser'] && ( $userdata['user_id'] != $user_id ) && $source_mode == 'edit' )
		{
			$sql = "SELECT g.group_id 
				FROM " . USER_GROUP_TABLE . " ug, " . GROUPS_TABLE . " g  
				WHERE ug.user_id = $user_id 
					AND g.group_id = ug.group_id 
					AND g.group_single_user = 1";
			if( !($result = $db->sql_query($sql)) )
			{
				message_die(GENERAL_ERROR, 'Could not obtain group information for this user', '', __LINE__, __FILE__, $sql);
			}

			$row = $db->sql_fetchrow($result);
			
			$sql = "SELECT group_id
				FROM " . GROUPS_TABLE . "
				WHERE group_moderator = $user_id";
			if( !($result = $db->sql_query($sql)) )
			{
				message_die(GENERAL_ERROR, 'Could not select groups where user was moderator', '', __LINE__, __FILE__, $sql);
			}
			
			while ( $row_group = $db->sql_fetchrow($result) )
			{
				$group_moderator[] = $row_group['group_id'];
			}
			
			if ( count($group_moderator) )
			{
				$update_moderator_id = implode(', ', $group_moderator);
				
				$sql = "UPDATE " . GROUPS_TABLE . "
					SET group_moderator = " . $userdata['user_id'] . "
					WHERE group_moderator IN ($update_moderator_id)";
				if( !$db->sql_query($sql) )
				{
					message_die(GENERAL_ERROR, 'Could not update group moderators', '', __LINE__, __FILE__, $sql);
				}
			}

			$sql = "DELETE FROM " . USERS_TABLE . "
				WHERE user_id = $user_id";
			if( !$db->sql_query($sql) )
			{
				message_die(GENERAL_ERROR, 'Could not delete user', '', __LINE__, __FILE__, $sql);
			}

			$sql = "DELETE fnr FROM " . FACILITY_NOTIFICATION_REQUEST_TABLE . " as fnr
				INNER JOIN " . NOTIFICATION_REQUEST_TABLE . " as nr
				on fnr.notification_request_id = nr.notification_request_id
				WHERE shakecast_user = $user_id";
			if( !$db->sql_query($sql) )
			{
				message_die(GENERAL_ERROR, 'Could not delete user facility notification request', '', __LINE__, __FILE__, $sql);
			}

			$sql = "DELETE FROM " . GEOMETRY_USER_PROFILE_TABLE . "
				WHERE shakecast_user = $user_id";
			if( !$db->sql_query($sql) )
			{
				message_die(GENERAL_ERROR, 'Could not delete user geometry profile', '', __LINE__, __FILE__, $sql);
			}

			$sql = "DELETE FROM " . NOTIFICATION_TABLE . "
				WHERE shakecast_user = $user_id";
			if( !$db->sql_query($sql) )
			{
				message_die(GENERAL_ERROR, 'Could not delete user notification', '', __LINE__, __FILE__, $sql);
			}

			$sql = "DELETE FROM " . NOTIFICATION_REQUEST_TABLE . "
				WHERE shakecast_user = $user_id";
			if( !$db->sql_query($sql) )
			{
				message_die(GENERAL_ERROR, 'Could not delete user notification request', '', __LINE__, __FILE__, $sql);
			}

			$sql = "DELETE FROM " . NOTIFICATION_REQUEST_TABLE . "
				WHERE shakecast_user = $user_id";
			if( !$db->sql_query($sql) )
			{
				message_die(GENERAL_ERROR, 'Could not delete user notification request', '', __LINE__, __FILE__, $sql);
			}

			$sql = "DELETE FROM " . USER_DELIVERY_METHOD_TABLE . "
				WHERE shakecast_user = $user_id";
			if( !$db->sql_query($sql) )
			{
				message_die(GENERAL_ERROR, 'Could not delete user delivery method', '', __LINE__, __FILE__, $sql);
			}

			$sql = "DELETE FROM " . SC_USER_TABLE . "
				WHERE shakecast_user = $user_id";
			if( !$db->sql_query($sql) )
			{
				message_die(GENERAL_ERROR, 'Could not delete shakecast user', '', __LINE__, __FILE__, $sql);
			}

			$sql = "DELETE FROM " . USER_GROUP_TABLE . "
				WHERE user_id = $user_id";
			if( !$db->sql_query($sql) )
			{
				message_die(GENERAL_ERROR, 'Could not delete user from user_group table', '', __LINE__, __FILE__, $sql);
			}

			/*$sql = "DELETE FROM " . GROUPS_TABLE . "
				WHERE group_id = " . $row['group_id'];
			if( !$db->sql_query($sql) )
			{
				message_die(GENERAL_ERROR, 'Could not delete group for this user', '', __LINE__, __FILE__, $sql);
			}

			$sql = "DELETE FROM " . AUTH_ACCESS_TABLE . "
				WHERE group_id = " . $row['group_id'];
			if( !$db->sql_query($sql) )
			{
				message_die(GENERAL_ERROR, 'Could not delete group for this user', '', __LINE__, __FILE__, $sql);
			}*/

			$sql = "DELETE FROM " . BANLIST_TABLE . "
				WHERE ban_userid = $user_id";
			if ( !$db->sql_query($sql) )
			{
				message_die(GENERAL_ERROR, 'Could not delete user from banlist table', '', __LINE__, __FILE__, $sql);
			}

			$sql = "DELETE FROM " . SESSIONS_TABLE . "
				WHERE session_user_id = $user_id";
			if ( !$db->sql_query($sql) )
			{
				message_die(GENERAL_ERROR, 'Could not delete sessions for this user', '', __LINE__, __FILE__, $sql);
			}
			
			$sql = "DELETE FROM " . SESSIONS_KEYS_TABLE . "
				WHERE user_id = $user_id";
			if ( !$db->sql_query($sql) )
			{
				message_die(GENERAL_ERROR, 'Could not delete auto-login keys for this user', '', __LINE__, __FILE__, $sql);
			}

			$message = $lang['User_deleted'] . '<br /><br />' . sprintf($lang['Click_return_useradmin'], '<a href="' . append_sid("admin_users.$phpEx") . '">', '</a>') . '<br /><br />' . sprintf($lang['Click_return_admin_index'], '<a href="' . append_sid("index.$phpEx?pane=right") . '">', '</a>');

			message_die(GENERAL_MESSAGE, $message);
		}

		$username = ( !empty($HTTP_POST_VARS['username']) ) ? phpbb_clean_username($HTTP_POST_VARS['username']) : '';
		$email = ( !empty($HTTP_POST_VARS['email']) ) ? trim(strip_tags(htmlspecialchars( $HTTP_POST_VARS['email'] ) )) : '';

		$password = ( !empty($HTTP_POST_VARS['password']) ) ? trim(strip_tags(htmlspecialchars( $HTTP_POST_VARS['password'] ) )) : '';
		$password_confirm = ( !empty($HTTP_POST_VARS['password_confirm']) ) ? trim(strip_tags(htmlspecialchars( $HTTP_POST_VARS['password_confirm'] ) )) : '';

		$fullname = ( !empty($HTTP_POST_VARS['fullname']) ) ? trim(strip_tags( $HTTP_POST_VARS['fullname'] ) ) : '';
		$location = ( !empty($HTTP_POST_VARS['location']) ) ? trim(strip_tags( $HTTP_POST_VARS['location'] ) ) : '';
		$occupation = ( !empty($HTTP_POST_VARS['occupation']) ) ? trim(strip_tags( $HTTP_POST_VARS['occupation'] ) ) : '';
		$organization = ( !empty($HTTP_POST_VARS['organization']) ) ? trim(strip_tags( $HTTP_POST_VARS['organization'] ) ) : '';

		$delivery_methods = array('EMAIL_HTML', 'EMAIL_TEXT', 'PAGER');
		$user_delivery_method = array();
		$user_delivery_method['EMAIL_HTML'] = ( !empty($HTTP_POST_VARS['email_html']) ) ? trim(strip_tags(htmlspecialchars( $HTTP_POST_VARS['email_html'] ) )) : '';
		$user_delivery_method['EMAIL_TEXT'] = ( !empty($HTTP_POST_VARS['email_text']) ) ? trim(strip_tags(htmlspecialchars( $HTTP_POST_VARS['email_text'] ) )) : '';
		$user_delivery_method['PAGER'] = ( !empty($HTTP_POST_VARS['pager']) ) ? trim(strip_tags(htmlspecialchars( $HTTP_POST_VARS['pager'] ) )) : '';
		validate_optional_fields($fullname, $location, $occupation, $organization);

		$user_status = ( !empty($HTTP_POST_VARS['user_status']) ) ? intval( $HTTP_POST_VARS['user_status'] ) : 0;
		$user_rank = ( !empty($HTTP_POST_VARS['user_rank']) ) ? intval( $HTTP_POST_VARS['user_rank'] ) : 0;
		if ( !empty($HTTP_POST_VARS['user_level']) ) 
		{
			$user_level = (trim(strip_tags( $HTTP_POST_VARS['user_level'] ) ) == 'admin' ) ? ADMIN : USER ;
		}

		$error = FALSE;

		if (stripslashes($username) != $this_userdata['username'] && $source_mode == "add")
		{
			unset($rename_user);

			if ( stripslashes(strtolower($username)) != strtolower($this_userdata['username']) ) 
			{
				$result = validate_username($username);
				if ( $result['error'] )
				{
					$error = TRUE;
					$error_msg .= ( ( isset($error_msg) ) ? '<br />' : '' ) . $result['error_msg'];
				}
				else if ( strtolower(str_replace("\\'", "''", $username)) == strtolower($userdata['username']) )
				{
					$error = TRUE;
					$error_msg .= ( ( isset($error_msg) ) ? '<br />' : '' ) . $lang['Username_taken'];
				}
			}

			if (!$error)
			{
				$username_sql = "username = '" . str_replace("\\'", "''", $username) . "', ";
				$rename_user = $username; // Used for renaming usergroup
			}
		}

		$passwd_sql = '';
		if( !empty($password) && !empty($password_confirm) )
		{
			//
			// Awww, the user wants to change their password, isn't that cute..
			//
			if($password != $password_confirm)
			{
				$error = TRUE;
				$error_msg .= ( ( isset($error_msg) ) ? '<br />' : '' ) . $lang['Password_mismatch'];
			}
			else
			{
				$password = md5($password);
				$passwd_sql = "user_password = '$password', ";
			}
		}
		else if( $password && !$password_confirm )
		{
			$error = TRUE;
			$error_msg .= ( ( isset($error_msg) ) ? '<br />' : '' ) . $lang['Password_mismatch'];
		}
		else if( !$password && $password_confirm )
		{
			$error = TRUE;
			$error_msg .= ( ( isset($error_msg) ) ? '<br />' : '' ) . $lang['Password_mismatch'];
		}

		//
		// Update entry in DB
		//
		if( !$error )
		{
			$admin_userdata = get_userdata($userdata['session_user_id']);
			if ( $source_mode == 'edit' )
			{
				$sql = "UPDATE " . SC_USER_TABLE . "
					SET " . $username_sql . "email_address = '" . str_replace("\'", "''", $email) ."', full_name = '" . str_replace("\'", "''", $fullname) . "', password = '" . str_replace("\'", "''", $password_confirm) . "'
					WHERE shakecast_user = $user_id";
				if ( !($result = $db->sql_query($sql)) )
				{
					message_die(GENERAL_ERROR, 'Could not update users table', '', __LINE__, __FILE__, $sql);
				}
	
				foreach ($delivery_methods as $delivery_method) {
					$sql = "UPDATE " . USER_DELIVERY_METHOD_TABLE . "
						SET  delivery_address = '" . str_replace("\'", "''", $user_delivery_method[$delivery_method]) ."', update_username = '".$admin_userdata['username']."', update_timestamp = now(), actkey = '"
						 . (($user_status) ? '' : '1') . "'
						WHERE shakecast_user = $user_id 
						AND delivery_method = '".$delivery_method."'";
					if ( !($result = $db->sql_query($sql)) )
					{
						message_die(GENERAL_ERROR, 'Could not update user delivery methods table', '', __LINE__, __FILE__, $sql);
					}
				}
	
				$exe_query = "perl $update_password -target user -username ".$username." -password ".$password_confirm;
				$result = exec($exe_query, $result_message);
				//$message = "User password information has been updated";
	
				$sql = "UPDATE " . USERS_TABLE . "
					SET " . $username_sql . $passwd_sql . "user_email = '" . str_replace("\'", "''", $email) . "', user_fullname = '" . str_replace("\'", "''", $fullname) . "', user_occ = '" . str_replace("\'", "''", $occupation) . "', user_from = '" . str_replace("\'", "''", $location) . "', user_organization = '" . str_replace("\'", "''", $organization) . "', user_active = $user_status, user_level = $user_level, user_rank = $user_rank
					WHERE user_id = $user_id";
	
				if( $result = $db->sql_query($sql) )
				{
					if( isset($rename_user) )
					{
						$sql = "UPDATE " . GROUPS_TABLE . "
							SET group_name = '".str_replace("\'", "''", $rename_user)."'
							WHERE group_name = '".str_replace("'", "''", $this_userdata['username'] )."'";
						if( !$result = $db->sql_query($sql) )
						{
							message_die(GENERAL_ERROR, 'Could not rename users group', '', __LINE__, __FILE__, $sql);
						}
					}
					
					// Delete user session, to prevent the user navigating the forum (if logged in) when disabled
					if (!$user_status)
					{
						$sql = "DELETE FROM " . SESSIONS_TABLE . " 
							WHERE session_user_id = " . $user_id;
	
						if ( !$db->sql_query($sql) )
						{
							message_die(GENERAL_ERROR, 'Error removing user session', '', __LINE__, __FILE__, $sql);
						}
					}
	
					// We remove all stored login keys since the password has been updated
					// and change the current one (if applicable)
					if ( !empty($passwd_sql) )
					{
						session_reset_keys($user_id, $user_ip);
					}
					
					$message .= $lang['Admin_user_updated'];
				}
				else
				{
					message_die(GENERAL_ERROR, 'Admin_user_fail', '', __LINE__, __FILE__, $sql);
				}
	
				$message .= '<br /><br />' . sprintf($lang['Click_return_useradmin'], '<a href="' . append_sid("admin_users.$phpEx") . '">', '</a>') . '<br /><br />' . sprintf($lang['Click_return_admin_index'], '<a href="' . append_sid("index.$phpEx?pane=right") . '">', '</a>');
	
				message_die(GENERAL_MESSAGE, $message);
			}
			else if ( $source_mode == 'add' )
			{
				$sql = "SELECT MAX(user_id) AS total
					FROM " . USERS_TABLE;
				if ( !($result = $db->sql_query($sql)) )
				{
					message_die(GENERAL_ERROR, 'Could not obtain next user_id information', '', __LINE__, __FILE__, $sql);
				}
	
				if ( !($row = $db->sql_fetchrow($result)) )
				{
					message_die(GENERAL_ERROR, 'Could not obtain next user_id information', '', __LINE__, __FILE__, $sql);
				}
				$user_id = $row['total'] + 1;
	
				//
				// Get current date
				//
				$sql = "INSERT INTO " . USERS_TABLE . "	(user_id, username, user_regdate, user_password, user_email, user_fullname, user_occ, user_from, user_organization, user_avatar_type, user_timezone, user_dateformat, user_lang, user_level, user_active, user_actkey)
					VALUES ($user_id, '" . str_replace("\'", "''", $username) . "', " . time() . ", '" . str_replace("\'", "''", $password) . "', '" . str_replace("\'", "''", $email) . "', '" . str_replace("\'", "''", $fullname) . "', '" . str_replace("\'", "''", $occupation) . "', '" . str_replace("\'", "''", $location) . "', '" . str_replace("\'", "''", $organization) . "', 1, " . $board_config['board_timezone'] . ", '" . str_replace("\'", "''", $board_config['default_dateformat']) . "', '" . str_replace("\'", "''", $board_config['user_lang']) . "', $user_level, 1, '')";
	
				if ( !($result = $db->sql_query($sql, BEGIN_TRANSACTION)) )
				{
					message_die(GENERAL_ERROR, 'Could not insert data into users table', '', __LINE__, __FILE__, $sql);
				}
	
				$sql = "INSERT INTO " . SC_USER_TABLE . "	(shakecast_user, email_address, password, username, full_name, user_type, update_username, update_timestamp)
					VALUES ($user_id , '" .str_replace("\'", "''", $email) . "', '" .str_replace("\'", "''", $password_confirm) . "', '" .str_replace("\'", "''", $username) . "', '" .str_replace("\'", "''", $fullname) . "', 'USER', 'scadmin', now())";
				if ( !($result = $db->sql_query($sql)) )
				{
					message_die(GENERAL_ERROR, 'Could not insert data into shakecast users table', '', __LINE__, __FILE__, $sql);
				}

				foreach ($delivery_methods as $delivery_method) {
					$sql = "INSERT INTO " . USER_DELIVERY_METHOD_TABLE . "	(shakecast_user, delivery_method, delivery_address, update_username, update_timestamp, actkey)
						VALUES (" . $user_id . ", '" .str_replace("\'", "''", $delivery_method) . "', '" .str_replace("\'", "''", $user_delivery_method[$delivery_method]) . "', '".$admin_userdata['username']."', now(), '"
						 .  (($user_status) ? '' : '1') . "')";
					if (!($result = $db->sql_query($sql)))
					{
						message_die(GENERAL_ERROR, "Couldn't insert data into user delivery method table.", "", __LINE__, __FILE__, $sql);
					}
				}

				$sql = "INSERT INTO " . GROUPS_TABLE . " (group_name, group_description, group_single_user, group_moderator)
					VALUES ('', 'Personal User', 1, 0)";
				if ( !($result = $db->sql_query($sql)) )
				{
					message_die(GENERAL_ERROR, 'Could not insert data into groups table', '', __LINE__, __FILE__, $sql);
				}
	
				$group_id = $db->sql_nextid();
	
				$sql = "INSERT INTO " . USER_GROUP_TABLE . " (user_id, group_id, user_pending)
					VALUES ($user_id, $group_id, 0)";
				if( !($result = $db->sql_query($sql, END_TRANSACTION)) )
				{
					message_die(GENERAL_ERROR, 'Could not insert data into user_group table', '', __LINE__, __FILE__, $sql);
				}
	
				$exe_query = "perl $update_password -target user -username ".$username." -password ".$password_confirm;
				$result = exec($exe_query, $message);
				//$message = "Server incoming password information has been updated";
	
				$message = $lang['Account_added'];
				$email_template = 'user_welcome';
	
				include($sc_root_path . 'includes/emailer.'.$phpEx);
				$emailer = new emailer($board_config['smtp_delivery']);
	
				$emailer->from($board_config['board_email']);
				$emailer->replyto($board_config['board_email']);
	
				$emailer->use_template($email_template, stripslashes($user_lang));
				$emailer->email_address($email);
				$emailer->set_subject(sprintf($lang['Welcome_subject'], $board_config['sitename']));
	
				$emailer->assign_vars(array(
					'SITENAME' => $board_config['sitename'],
					'WELCOME_MSG' => sprintf($lang['Welcome_subject'], $board_config['sitename']),
					'USERNAME' => $username,
					'PASSWORD' => $password_confirm,
					'EMAIL_SIG' => str_replace('<br />', "\n", "-- \n" . $board_config['board_email_sig']),
	
					'U_ACTIVATE' => $server_url . '?mode=activate&' . POST_USERS_URL . '=' . $user_id . '&act_key=' . $user_actkey)
				);
	
				$emailer->send();
				$emailer->reset();
		
				$message = $message . '<br /><br />' . sprintf($lang['Click_return_index'],  '<a href="' . append_sid("index.$phpEx") . '">', '</a>');
	
				message_die(GENERAL_MESSAGE, $message);
			} // if mode == register
		}
		else
		{
			$template->set_filenames(array(
				'reg_header' => 'error_body.tpl')
			);

			$template->assign_vars(array(
				'ERROR_MESSAGE' => $error_msg)
			);

			$template->assign_var_from_handle('ERROR_BOX', 'reg_header');

			$username = htmlspecialchars(stripslashes($username));
			$email = stripslashes($email);
			$password = '';
			$password_confirm = '';

			$fullname = htmlspecialchars(stripslashes($fullname));
			$location = htmlspecialchars(stripslashes($location));
			$occupation = htmlspecialchars(stripslashes($occupation));
			$organization = htmlspecialchars(stripslashes($organization));
			$s_user_level = ( $user_level == ADMIN ) ? '<option value="admin" selected="selected">' . $lang['Auth_Admin'] . '</option><option value="user">' . $lang['Auth_User'] . '</option>' : '<option value="admin">' . $lang['Auth_Admin'] . '</option><option value="user" selected="selected">' . $lang['Auth_User'] . '</option>';
				message_die(GENERAL_MESSAGE, $error_msg);
		}
	}
	else if( $mode == "delete" )
	{
		$confirm = isset($HTTP_POST_VARS['confirm']);

		if( $username != '' && $confirm )
		{
			$sql = "SELECT shakecast_user 
				FROM " . SC_USER_TABLE . "   
				WHERE username = '$username'";
			if( !($result = $db->sql_query($sql)) )
			{
				message_die(GENERAL_ERROR, 'Could not obtain group information for this user', '', __LINE__, __FILE__, $sql);
			}
	
			$ids = $db->sql_fetchrowset($result);
			for( $i = 0; $i < count($ids); $i++ )
			{
				$user_id = $ids[$i]['shakecast_user'];
				$sql = "DELETE FROM " . USERS_TABLE . "
					WHERE user_id = $user_id";
				if( !$db->sql_query($sql) )
				{
					message_die(GENERAL_ERROR, 'Could not delete user', '', __LINE__, __FILE__, $sql);
				}
	
				$sql = "DELETE fnr FROM " . FACILITY_NOTIFICATION_REQUEST_TABLE . " as fnr
					INNER JOIN " . NOTIFICATION_REQUEST_TABLE . " as nr
					on fnr.notification_request_id = nr.notification_request_id
					WHERE shakecast_user = $user_id";
				if( !$db->sql_query($sql) )
				{
					message_die(GENERAL_ERROR, 'Could not delete user facility notification request', '', __LINE__, __FILE__, $sql);
				}
	
				$sql = "DELETE FROM " . GEOMETRY_USER_PROFILE_TABLE . "
					WHERE shakecast_user = $user_id";
				if( !$db->sql_query($sql) )
				{
					message_die(GENERAL_ERROR, 'Could not delete user geometry profile', '', __LINE__, __FILE__, $sql);
				}
	
				$sql = "DELETE FROM " . NOTIFICATION_TABLE . "
					WHERE shakecast_user = $user_id";
				if( !$db->sql_query($sql) )
				{
					message_die(GENERAL_ERROR, 'Could not delete user notification', '', __LINE__, __FILE__, $sql);
				}
	
				$sql = "DELETE FROM " . NOTIFICATION_REQUEST_TABLE . "
					WHERE shakecast_user = $user_id";
				if( !$db->sql_query($sql) )
				{
					message_die(GENERAL_ERROR, 'Could not delete user notification request', '', __LINE__, __FILE__, $sql);
				}
	
				$sql = "DELETE FROM " . NOTIFICATION_REQUEST_TABLE . "
					WHERE shakecast_user = $user_id";
				if( !$db->sql_query($sql) )
				{
					message_die(GENERAL_ERROR, 'Could not delete user notification request', '', __LINE__, __FILE__, $sql);
				}
	
				$sql = "DELETE FROM " . USER_DELIVERY_METHOD_TABLE . "
					WHERE shakecast_user = $user_id";
				if( !$db->sql_query($sql) )
				{
					message_die(GENERAL_ERROR, 'Could not delete user delivery method', '', __LINE__, __FILE__, $sql);
				}
	
				$sql = "DELETE FROM " . SC_USER_TABLE . "
					WHERE shakecast_user = $user_id";
				if( !$db->sql_query($sql) )
				{
					message_die(GENERAL_ERROR, 'Could not delete shakecast user', '', __LINE__, __FILE__, $sql);
				}
	
				$sql = "DELETE FROM " . USER_GROUP_TABLE . "
					WHERE user_id = $user_id";
				if( !$db->sql_query($sql) )
				{
					message_die(GENERAL_ERROR, 'Could not delete user from user_group table', '', __LINE__, __FILE__, $sql);
				}
	
				/*$sql = "DELETE FROM " . GROUPS_TABLE . "
					WHERE group_id = " . $row['group_id'];
				if( !$db->sql_query($sql) )
				{
					message_die(GENERAL_ERROR, 'Could not delete group for this user', '', __LINE__, __FILE__, $sql);
				}
	
				$sql = "DELETE FROM " . AUTH_ACCESS_TABLE . "
					WHERE group_id = " . $row['group_id'];
				if( !$db->sql_query($sql) )
				{
					message_die(GENERAL_ERROR, 'Could not delete group for this user', '', __LINE__, __FILE__, $sql);
				}*/
	
				$sql = "DELETE FROM " . BANLIST_TABLE . "
					WHERE ban_userid = $user_id";
				if ( !$db->sql_query($sql) )
				{
					message_die(GENERAL_ERROR, 'Could not delete user from banlist table', '', __LINE__, __FILE__, $sql);
				}
	
				$sql = "DELETE FROM " . SESSIONS_TABLE . "
					WHERE session_user_id = $user_id";
				if ( !$db->sql_query($sql) )
				{
					message_die(GENERAL_ERROR, 'Could not delete sessions for this user', '', __LINE__, __FILE__, $sql);
				}
				
				$sql = "DELETE FROM " . SESSIONS_KEYS_TABLE . "
					WHERE user_id = $user_id";
				if ( !$db->sql_query($sql) )
				{
					message_die(GENERAL_ERROR, 'Could not delete auto-login keys for this user', '', __LINE__, __FILE__, $sql);
				}
			}

			$message = $lang['user_deleted'] . "<br /><br />" . sprintf($lang['Click_return_useradmin'], "<a href=\"" . append_sid("admin_users.$phpEx") . "\">", "</a>") . "<br /><br />" . sprintf($lang['Click_return_admin_index'], "<a href=\"" . append_sid("index.$phpEx?pane=right") . "\">", "</a>");

			message_die(GENERAL_MESSAGE, $message);
		}
		elseif( $username != '' && !$confirm)
		{
			// Present the confirmation screen to the user
			$template->set_filenames(array(
				'body' => 'admin/confirm_body.tpl')
			);

			$hidden_fields = '<input type="hidden" name="mode" value="'.$mode.'" /><input type="hidden" name="username" value="' . $username . '" />';

			$template->assign_vars(array(
				'MESSAGE_TITLE' => $lang['Confirm'],
				'MESSAGE_TEXT' => $lang['Confirm_user'],

				'L_YES' => $lang['Yes'],
				'L_NO' => $lang['No'],

				'S_CONFIRM_ACTION' => append_sid("admin_users.$phpEx"),
				'S_HIDDEN_FIELDS' => $hidden_fields)
			);
		}
		else
		{
			message_die(GENERAL_MESSAGE, $lang['no_user_selected']);
		}
	}
}
else
{
	//
	// Default user selection box
	//
	$template->set_filenames(array(
		'body' => 'admin/user_select_body.tpl')
	);

	$template->assign_vars(array(
		'L_USER_TITLE' => $lang['User_admin'],
		'L_USER_EXPLAIN' => $lang['User_admin_explain'],
		'L_USER_SELECT' => $lang['Select_a_User'],
		'L_LOOK_UP' => $lang['Look_up_user'],
		'L_FIND_USERNAME' => $lang['Find_username'],

		'U_SEARCH_USER' => append_sid("./../search.$phpEx?mode=searchuser"), 

		'S_USER_ACTION' => append_sid("admin_users.$phpEx"),
		'S_USER_SELECT' => $select_list)
	);

	$user_count = get_db_stat('usercount');
	if ( !$user_count )
	{
		//
		// No group members
		//
		$template->assign_block_vars('switch_no_members', array());
		$template->assign_vars(array(
			'L_NO_MEMBERS' => $lang['No_group_members'])
		);
	}

	if (isset($HTTP_GET_VARS['sort_key']))
	{
		$sort_key = $HTTP_GET_VARS['sort_key'];
		$sort_order = ( isset($HTTP_GET_VARS['sort_order']) ) ? $HTTP_GET_VARS['sort_order'] : 'a';
	}
	else 
	{
		$sort_key = 'username';
		$sort_order = ( isset($HTTP_GET_VARS['sort_order']) ) ? $HTTP_GET_VARS['sort_order'] : 'a';
	}

	$default_sort = array($sort_key, $sort_order);
	$new_sort_order = ($sort_order == 'd') ? 'a' : 'd';
	$img_url = ' <img src="/images/' . $sort_order . '.png" border="0" width="10" height="10">';
	$base_url = append_sid("admin_users.$phpEx?" . POST_GROUPS_URL . "=$group_id");

	$start = ( isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;
	$start = ($start < 0) ? 0 : $start;
	$current_page = ( !$user_count ) ? 1 : ceil( $user_count / $board_config['topics_per_page'] );
	$template->assign_vars(array(
		'PAGINATION' => generate_pagination("admin_users.$phpEx?" . POST_GROUPS_URL . "=$group_id", $user_count, $board_config['topics_per_page'], $start),
		'PAGE_NUMBER' => sprintf($lang['Page_of'], ( floor( $start / $board_config['topics_per_page'] ) + 1 ), $current_page ), 

		'L_GOTO_PAGE' => $lang['Goto_page'])
	);

	$sql = "SELECT username, user_fullname, user_email, user_active, user_occ, user_organization, user_level
			FROM " . USERS_TABLE ."
			WHERE user_id <> " . ANONYMOUS;
	//if ($board_config['topics_per_page']) { $sql .= ' LIMIT '.$board_config['topics_per_page'];}
	//if ($start) { $sql .= " OFFSET $start";}
	if(!$result = $db->sql_query($sql))
	{
		message_die(GENERAL_ERROR, "Could not find damage level settings", $lang['Error'], __LINE__, __FILE__, $sql);
	}
	$rows = $db->sql_fetchrowset($result);

	$rows = arfsort( $rows, array($default_sort));

	$template->assign_vars(array(
		"L_WORDS_TITLE" => $lang['fac_title'],
		"L_WORDS_TEXT" => $lang['fac_explain'],
		"L_USERNAME" => "Username" . (($sort_key == 'username') ? $img_url : ''),
		"L_FULL_NAME" => "Full Name" . (($sort_key == 'user_fullname') ? $img_url : ''),
		"L_EMAIL" => "Email Address" . (($sort_key == 'user_email') ? $img_url : ''),
		"L_USER_STATUS" => "User Status" . (($sort_key == 'user_active') ? $img_url : ''),
		"L_OCCUPATION" => "Job Title" . (($sort_key == 'user_occ') ? $img_url : ''),
		"L_ORGANIZATION" => "Organization" . (($sort_key == 'user_organization') ? $img_url : ''),
		"L_USER_LEVEL" => $lang['User_Level'] . (($sort_key == 'user_level') ? $img_url : ''),
		"L_EDIT" => "Edit",
		"L_DELETE" => "Delete",
		"L_ADD_EVENT" => $lang['Add_new_user'],
		"L_ACTION" => $lang['Action'],

		'U_USERNAME' =>  $base_url."&sort_key=username&sort_order=" . (($sort_key == 'username') ? $new_sort_order : $sort_order),
		'U_FULL_NAME' =>  $base_url."&sort_key=user_fullname&sort_order=" . (($sort_key == 'user_fullname') ? $new_sort_order : $sort_order),
		'U_EMAIL' =>  $base_url."&sort_key=user_email&sort_order=" . (($sort_key == 'user_email') ? $new_sort_order : $sort_order),
		'U_USER_STATUS' =>  $base_url."&sort_key=user_active&sort_order=" . (($sort_key == 'user_active') ? $new_sort_order : $sort_order),
		'U_OCCUPATION' =>  $base_url."&sort_key=user_occ&sort_order=" . (($sort_key == 'user_occ') ? $new_sort_order : $sort_order),
		'U_ORGANIZATION' =>  $base_url."&sort_key=user_organization&sort_order=" . (($sort_key == 'user_organization') ? $new_sort_order : $sort_order),
		'U_USER_LEVEL' =>  $base_url."&sort_key=user_level&sort_order=" . (($sort_key == 'user_level') ? $new_sort_order : $sort_order),

		"S_WORDS_ACTION" => append_sid("admin_users.$phpEx?mode=add"),
		"S_HIDDEN_FIELDS" => '')
	);

	for($i = $start; $i < count($rows) && $i < ($board_config['topics_per_page'] + $start); $i++)
	{
		$row_color = ( !($i % 2) ) ? $theme['td_color1'] : $theme['td_color2'];
		$row_class = ( !($i % 2) ) ? $theme['td_class1'] : $theme['td_class2'];
		$username = $rows[$i]['username'];
		
		$template->assign_block_vars('words', array(
			"ROW_COLOR" => "#" . $row_color,
			"ROW_CLASS" => $row_class,
			"USERNAME" => $rows[$i]['username'],
			"FULL_NAME" => $rows[$i]['user_fullname'],
			"EMAIL" => $rows[$i]['user_email'],
			"USER_STATUS" => $rows[$i]['user_active'] ? "Active" : "Suspended",
			"OCCUPATION" => $rows[$i]['user_occ'],
			"ORGANIZATION" => $rows[$i]['user_organization'],
			"USER_LEVEL" => ($rows[$i]['user_level'] == ADMIN) ? $lang['Auth_Admin'] : $lang['Auth_User'],

			"U_EDIT" => append_sid("admin_users.$phpEx?mode=edit&amp;username=$username"),
			"U_DELETE" => append_sid("admin_users.$phpEx?mode=delete&amp;username=$username"))
		);
	}

}

$template->pparse('body');
include('./page_footer_admin.'.$phpEx);

?>