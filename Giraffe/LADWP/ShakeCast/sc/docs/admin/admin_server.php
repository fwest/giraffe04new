<?php
/*
# $Id: admin_server.php 170 2007-10-31 18:38:34Z klin $

# The PHP interface was originally inspired by PHPBB,
# file/database formats and directory structure are quite similar.

##############################################################################
# 
# Terms and Conditions of Software Use
# ====================================
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Disclaimer of Earthquake Information
# ====================================
# 
# The data and maps provided through this system are preliminary data
# and are subject to revision. They are computer generated and may not
# have received human review or official approval. Inaccuracies in the
# data may be present because of instrument or computer
# malfunctions. Subsequent review may result in significant revisions to
# the data. All efforts have been made to provide accurate information,
# but reliance on, or interpretation of earthquake data from a single
# source is not advised. Data users are cautioned to consider carefully
# the provisional nature of the information before using it for
# decisions that concern personal or public safety or the conduct of
# business that involves substantial monetary or operational
# consequences.
# 
# Disclaimer of Software and its Capabilities
# ===========================================
# 
# This software is provided as an "as is" basis.  Attempts have been
# made to rid the program of software defects and bugs, however the
# U.S. Geological Survey (USGS) have no obligations to provide maintenance, 
# support, updates, enhancements or modifications. In no event shall USGS 
# be liable to any party for direct, indirect, special, incidental or 
# consequential damages, including lost profits, arising out of the use 
# of this software, its documentation, or data obtained though the use 
# of this software, even if USGS or have been advised of the
# possibility of such damage. By downloading, installing or using this
# program, the user acknowledges and understands the purpose and
# limitations of this software.
# 
# Contact Information
# ===================
# 
# Coordination of this effort is under the auspices of the USGS Advanced
# National Seismic System (ANSS) coordinated in Golden, Colorado, which
# functions as the clearing house for development, distribution,
# documentation, and support. For questions, comments, or reports of
# potential bugs regarding this software please contact wald@usgs.gov or
# klin@usgs.gov.  
#
#############################################################################
*/

if( !empty($setmodules) )
{
	$file = basename(__FILE__);
	$module[' General Admin']['ShakeMap Server'] = $file;
	return;
}

define('IN_SC', 1);

//
// Load default header
//
$sc_root_path = "./../";
require($sc_root_path . 'extension.inc');

$cancel = ( isset($HTTP_POST_VARS['cancel']) ) ? true : false;
$no_page_header = $cancel;

require('./pagestart.' . $phpEx);

if ($cancel)
{
	redirect('admin/' . append_sid("admin_server.$phpEx", true));
}

if( isset($HTTP_GET_VARS['mode']) || isset($HTTP_POST_VARS['mode']) )
{
	$mode = (isset($HTTP_GET_VARS['mode'])) ? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode'];
	$mode = htmlspecialchars($mode);
}
else 
{
	//
	// These could be entered via a form button
	//
	if( isset($HTTP_POST_VARS['add_event']) )
	{
		$mode = "add_event";
	}
	else if( isset($HTTP_POST_VARS['save']) )
	{
		$mode = "save";
	}
	else if( isset($HTTP_POST_VARS['save_password']) )
	{
		$mode = "save_password";
	}
	else
	{
		$mode = "";
	}
}

// Restrict mode input to valid options
$mode = ( in_array($mode, array('add', 'edit', 'save', 'delete', 'password', 'save_password')) ) ? $mode : '';

if( $mode != "" )
{
	if( $mode == "edit" || $mode == "add" )
	{
		$server_id = ( isset($HTTP_GET_VARS['id']) ) ? $HTTP_GET_VARS['id'] : '';

		$template->set_filenames(array(
			"body" => "admin/server_edit_body.tpl")
		);


		$facility = array('damage_level' => '', 'name' => '', 'description' => '',
			'severity_rank' => '', 'is_max_severity' => '');
		$s_hidden_fields = '';

		if( $mode == "edit" )
		{
			if( $server_id )
			{
				$sql = "SELECT server_id, dns_address, owner_organization,
							upstream_flag, downstream_flag, poll_flag, query_flag
						FROM server WHERE server_id = ".$server_id."";
				$result = $db->sql_query($sql);
				if( !$result )
				{
					message_die(GENERAL_ERROR, "Couldn't get facility types.", "", __LINE__, __FILE__, $sql );
				}
				$server_settings = $db->sql_fetchrow($result);
				$hidden_fields = '<input type="hidden" name="edit" value="yes" />';
			}
			else
			{
				message_die(GENERAL_MESSAGE, "No server selected for editing ".$damage_level);
			}
		}
		
		$template->assign_vars(array(
			"L_WORDS_TITLE" => $lang['server_title'],
			"L_WORDS_TEXT" => $lang['server_explain'],
			
			"SERVER_ID" => $server_settings['server_id'],
			"DNS_ADDRESS" => $server_settings['dns_address'],
			"OWNER_ORGANIZATION" => $server_settings['owner_organization'],
			"UPSTREAM_FLAG" => ($server_settings['upstream_flag']) ? 'checked' : '',
			"DOWNSTREAM_FLAG" => ($server_settings['downstream_flag']) ? 'checked' : '',
			"POLL_FLAG" => ($server_settings['poll_flag']) ? 'checked' : '',
			"QUERY_FLAG" => ($server_settings['query_flag']) ? 'checked' : '',
			"F_HIDDEN_FIELDS" => $hidden_fields,

			"L_SERVER_EDIT" => "Server Settings",
			"L_SERVER_ID" => "ID*",
			"L_DNS_ADDRESS" => "DNS Address*",
			"L_OWNER_ORGANIZATION" => "Organization",
			"L_LAST_HEARD_FROM" => "Last Heard From",
			"L_SERVER_STATUS" => "Status",
			"L_ERROR_COUNT" => "Error Count",
			"L_UPSTREAM_FLAG" => "Upstream",
			"L_DOWNSTREAM_FLAG" => "Downstream",
			"L_POLL_FLAG" => "Poll",
			"L_QUERY_FLAG" => "Query",
			"L_SELF_FLAG" => "Self",

			"L_SUBMIT" => $lang['Submit'],

			"S_SERVER_ACTION" => append_sid("admin_server.$phpEx"),
			"S_HIDDEN_FIELDS" => $s_hidden_fields)
		);

		$template->pparse("body");

		include('./page_footer_admin.'.$phpEx);
	}
	else if( $mode == "save" )
	{
		$edit = ( isset($HTTP_POST_VARS['edit']) ) ? $HTTP_POST_VARS['edit'] : '';
		$upstream_flag = ($HTTP_POST_VARS['upstream_flag']) ? 1 : 0;
		$downstream_flag = ($HTTP_POST_VARS['downstream_flag']) ? 1 : 0;
		$poll_flag = ($HTTP_POST_VARS['poll_flag']) ? 1 : 0;
		$query_flag = ($HTTP_POST_VARS['query_flag']) ? 1 : 0;
		if( $edit)
		{
			$sql = "UPDATE " . SERVER_TABLE . " 
				SET server_id = ".$HTTP_POST_VARS['server_id'].", 
					dns_address = '" . str_replace("\'", "''", $HTTP_POST_VARS['dns_address']) . "', 
					owner_organization = '" . str_replace("\'", "''", $HTTP_POST_VARS['owner_organization']) . "', 
					upstream_flag = " . $upstream_flag . ", 
					downstream_flag = " . $downstream_flag . ", 
					poll_flag = " . $poll_flag . ", 
					query_flag = " . $query_flag . ", 
					update_username = 'webadmin', 
					update_timestamp = now() 
				WHERE server_id = ".$HTTP_POST_VARS['server_id']."";
			$message = "Server information has been updated";
		}
		else
		{
			$sql = "INSERT INTO " . SERVER_TABLE . " 
				(server_id, dns_address, owner_organization, upstream_flag, downstream_flag,  
				poll_flag, query_flag, update_username, update_timestamp) VALUES (".
					$HTTP_POST_VARS['server_id'].", '".
					str_replace("\'", "''", $HTTP_POST_VARS['dns_address'])."', '". 
					str_replace("\'", "''", $HTTP_POST_VARS['owner_organization'])."', ".
					$upstream_flag.", ".
					$downstream_flag.", ".
					$poll_flag.", ".
					$query_flag.", ".
					"'webadmin', now())"; 
			$message = "Server information has been added";
		}

		if(!$result = $db->sql_query($sql))
		{
			message_die(GENERAL_ERROR, "Could not insert data into words table", $lang['Error'], __LINE__, __FILE__, $sql);
		}
		$message .= "<br /><br />" . sprintf($lang['Click_return_serveradmin'], "<a href=\"" . append_sid("admin_server.$phpEx") . "\">", "</a>") . "<br /><br />" . sprintf($lang['Click_return_admin_index'], "<a href=\"" . append_sid("index.$phpEx?pane=right") . "\">", "</a>");

		message_die(GENERAL_MESSAGE, $message);
	}
	else if( $mode == "password" )
	{
		$server_id = ( isset($HTTP_GET_VARS['id']) ) ? $HTTP_GET_VARS['id'] : '';

		$template->set_filenames(array(
			"body" => "admin/server_password_edit_body.tpl")
		);

		$s_hidden_fields = '';

		if( $server_id )
		{
			$sql = "SELECT dns_address, owner_organization
					FROM server WHERE server_id = ".$server_id."";
			$result = $db->sql_query($sql);
			if( !$result )
			{
				message_die(GENERAL_ERROR, "Couldn't get facility types.", "", __LINE__, __FILE__, $sql );
			}
			$server_settings = $db->sql_fetchrow($result);
			$hidden_fields = '<input type="hidden" name="id" value="'.$server_id.'" /><input type="hidden" name="mode" value="save_password" />';
		}
		else
		{
			message_die(GENERAL_MESSAGE, "No server selected for editing ".$damage_level);
		}
		
		$template->assign_vars(array(
			"L_WORDS_TITLE" => $lang['server_title'],
			"L_WORDS_TEXT" => $lang['server_explain'],
			
			"SERVER_ID" => $server_id,
			"DNS_ADDRESS" => $server_settings['dns_address'],
			"OWNER_ORGANIZATION" => $server_settings['owner_organization'],
			"F_HIDDEN_FIELDS" => $hidden_fields,

			"L_SERVER_EDIT" => "Server Password Settings",
			"L_SERVER" => "Server",
			"L_DNS_ADDRESS" => "DNS Address*",
			"L_OWNER_ORGANIZATION" => "Organization",
			"L_PASSWORD1" => "Password",
			"L_PASSWORD2" => "Re-type Password",

			"L_OUTGOING" => "Update Outgoing Password",
			"L_INCOMING" => "Update Incoming Password",

			"S_SERVER_ACTION" => append_sid("admin_server.$phpEx"),
			"S_HIDDEN_FIELDS" => $s_hidden_fields)
		);

		$template->pparse("body");

		include('./page_footer_admin.'.$phpEx);
	}
	else if( $mode == "save_password" )
	{
		$server_id = ( isset($HTTP_POST_VARS['id']) ) ? $HTTP_POST_VARS['id'] : '';
		$outgoing = ( isset($HTTP_POST_VARS['outgoing']) ) ? $HTTP_POST_VARS['outgoing'] : '';
		$password1 = str_replace("\'", "''", $HTTP_POST_VARS['password1']);
		$password2 = str_replace("\'", "''", $HTTP_POST_VARS['password2']);
		if ($password1 != $password2) {
			message_die(GENERAL_MESSAGE, "Mismatch password inputs");
		}

		if( $outgoing)
		{
			$sql = "UPDATE " . SERVER_TABLE . " 
				SET password = '".base64_encode($password1)."',
					update_username = 'webadmin', 
					update_timestamp = now() 
				WHERE server_id = ".$server_id;
			
			if(!$result = $db->sql_query($sql))
			{
				message_die(GENERAL_ERROR, "Could not insert data into words table", $lang['Error'], __LINE__, __FILE__, $sql);
			}
			
			$message = "Server outgoing password information has been updated";
		}
		else
		{
			$update_password = '/shakecast/sc/bin/update_password.pl';
			$exe_query = "perl $update_password -target ".$server_id." -incoming ".$password1;
			$result = exec($exe_query, $message);
			$message = "Server incoming password information has been updated";
		}

		$message .= "<br /><br />" . sprintf($lang['Click_return_serveradmin'], "<a href=\"" . append_sid("admin_server.$phpEx") . "\">", "</a>") . "<br /><br />" . sprintf($lang['Click_return_admin_index'], "<a href=\"" . append_sid("index.$phpEx?pane=right") . "\">", "</a>");

		message_die(GENERAL_MESSAGE, $message);
	}
	else if( $mode == "delete" )
	{
		if( isset($HTTP_POST_VARS['id']) ||  isset($HTTP_GET_VARS['id']) )
		{
			$event_id = ( isset($HTTP_POST_VARS['id']) ) ? $HTTP_POST_VARS['id'] : $HTTP_GET_VARS['id'];
		}
		else
		{
			$event_id = 0;
		}

		$confirm = isset($HTTP_POST_VARS['confirm']);

		if( $event_id && $confirm )
		{
			$sql = "DELETE FROM " . SERVER_TABLE . " 
				WHERE server_id = '$event_id'";

			if(!$result = $db->sql_query($sql))
			{
				message_die(GENERAL_ERROR, "Could not delete server", $lang['Error']);
			}

			$message = $lang['server_deleted'] . "<br /><br />" . sprintf($lang['Click_return_serveradmin'], "<a href=\"" . append_sid("admin_server.$phpEx") . "\">", "</a>") . "<br /><br />" . sprintf($lang['Click_return_admin_index'], "<a href=\"" . append_sid("index.$phpEx?pane=right") . "\">", "</a>");

			message_die(GENERAL_MESSAGE, $message);
		}
		elseif( $event_id && !$confirm)
		{
			// Present the confirmation screen to the user
			$template->set_filenames(array(
				'body' => 'admin/confirm_body.tpl')
			);

			$hidden_fields = '<input type="hidden" name="mode" value="'.$mode.'" /><input type="hidden" name="id" value="' . $event_id . '" />';

			$template->assign_vars(array(
				'MESSAGE_TITLE' => $lang['Confirm'],
				'MESSAGE_TEXT' => $lang['Confirm_server'],

				'L_YES' => $lang['Yes'],
				'L_NO' => $lang['No'],

				'S_CONFIRM_ACTION' => append_sid("admin_server.$phpEx"),
				'S_HIDDEN_FIELDS' => $hidden_fields)
			);
		}
		else
		{
			message_die(GENERAL_MESSAGE, $lang['No_server_selected']);
		}
	}
}
else
{
	$template->set_filenames(array(
		"body" => "admin/server_list_body.tpl")
	);

	if ( !$facility_count )
	{
		//
		// No group members
		//
		$template->assign_block_vars('switch_no_members', array());
		$template->assign_vars(array(
			'L_NO_MEMBERS' => $lang['No_group_members'])
		);
	}

	$sql = "SELECT server_id, dns_address, last_heard_from, owner_organization, server_status, 
				error_count, upstream_flag, downstream_flag, poll_flag, query_flag, self_flag
			FROM server";
	if(!$result = $db->sql_query($sql))
	{
		message_die(GENERAL_ERROR, "Could not find damage level settings", $lang['Error']);
	}
	$servers = $db->sql_fetchrowset($result);

	$template->assign_vars(array(
		"L_WORDS_TITLE" => $lang['server_title'],
		"L_WORDS_TEXT" => $lang['server_explain'],
		
		"L_SERVER_ID" => "ID",
		"L_DNS_ADDRESS" => "DNS Address",
		"L_OWNER_ORGANIZATION" => "Organization",
		"L_LAST_HEARD_FROM" => "Last Heard From",
		"L_SERVER_STATUS" => "Status",
		"L_ERROR_COUNT" => "Error Count",
		"L_UPSTREAM_FLAG" => "Upstream",
		"L_DOWNSTREAM_FLAG" => "Downstream",
		"L_POLL_FLAG" => "Poll",
		"L_QUERY_FLAG" => "Query",
		"L_SELF_FLAG" => "Self",

		"L_EDIT" => "Edit",
		"L_DELETE" => "Delete",
		"L_PASSWORD" => "Password",
		"L_ADD_EVENT" => "Add new server",
		"L_ACTION" => $lang['Action'],

		"S_WORDS_ACTION" => append_sid("admin_server.$phpEx?mode=add"),
		"S_HIDDEN_FIELDS" => '')
	);

	for($i = 0;$i < count($servers);  $i++)
	{
		$server_id = $servers[$i]['server_id'];
		$dns_address = $servers[$i]['dns_address'];
		$owner_organization = $servers[$i]['owner_organization'];
		$last_heard_from = $servers[$i]['last_heard_from'];
		$server_status = $servers[$i]['server_status'];
		$error_count = $servers[$i]['error_count'];
		$upstream_flag = $servers[$i]['upstream_flag'];
		$downstream_flag = $servers[$i]['downstream_flag'];
		$poll_flag = $servers[$i]['poll_flag'];
		$query_flag = $servers[$i]['query_flag'];
		$self_flag = $servers[$i]['self_flag'];

		$row_color = ( !($i % 2) ) ? $theme['td_color1'] : $theme['td_color2'];
		$row_class = ( !($i % 2) ) ? $theme['td_class1'] : $theme['td_class2'];

		$template->assign_block_vars('words', array(
			"ROW_COLOR" => "#" . $row_color,
			"ROW_CLASS" => $row_class,

			"SERVER_ID" => $server_id,
			"DNS_ADDRESS" => $dns_address,
			"OWNER_ORGANIZATION" => $owner_organization,
			"LAST_HEARD_FROM" => $last_heard_from,
			"SERVER_STATUS" => $server_status,
			"ERROR_COUNT" => $error_count,
			"UPSTREAM_FLAG" => $upstream_flag,
			"DOWNSTREAM_FLAG" => $downstream_flag,
			"POLL_FLAG" => $poll_flag,
			"QUERY_FLAG" => $query_flag,
			"SELF_FLAG" => $self_flag,

			"U_EDIT" => append_sid("admin_server.$phpEx?mode=edit&amp;id=$server_id"),
			"U_DELETE" => append_sid("admin_server.$phpEx?mode=delete&amp;id=$server_id"),
			"U_PASSWORD" => append_sid("admin_server.$phpEx?mode=password&amp;id=$server_id"))
		);
	}
}

$template->pparse("body");

include('./page_footer_admin.'.$phpEx);

?>