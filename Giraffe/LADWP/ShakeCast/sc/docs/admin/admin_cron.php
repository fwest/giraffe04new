<?php
/*
# $Id: admin_cron.php 308 2008-01-23 18:35:05Z klin $

# The PHP interface was originally inspired by PHPBB,
# file/database formats and directory structure are quite similar.

##############################################################################
# 
# Terms and Conditions of Software Use
# ====================================
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Disclaimer of Earthquake Information
# ====================================
# 
# The data and maps provided through this system are preliminary data
# and are subject to revision. They are computer generated and may not
# have received human review or official approval. Inaccuracies in the
# data may be present because of instrument or computer
# malfunctions. Subsequent review may result in significant revisions to
# the data. All efforts have been made to provide accurate information,
# but reliance on, or interpretation of earthquake data from a single
# source is not advised. Data users are cautioned to consider carefully
# the provisional nature of the information before using it for
# decisions that concern personal or public safety or the conduct of
# business that involves substantial monetary or operational
# consequences.
# 
# Disclaimer of Software and its Capabilities
# ===========================================
# 
# This software is provided as an "as is" basis.  Attempts have been
# made to rid the program of software defects and bugs, however the
# U.S. Geological Survey (USGS) have no obligations to provide maintenance, 
# support, updates, enhancements or modifications. In no event shall USGS 
# be liable to any party for direct, indirect, special, incidental or 
# consequential damages, including lost profits, arising out of the use 
# of this software, its documentation, or data obtained though the use 
# of this software, even if USGS or have been advised of the
# possibility of such damage. By downloading, installing or using this
# program, the user acknowledges and understands the purpose and
# limitations of this software.
# 
# Contact Information
# ===================
# 
# Coordination of this effort is under the auspices of the USGS Advanced
# National Seismic System (ANSS) coordinated in Golden, Colorado, which
# functions as the clearing house for development, distribution,
# documentation, and support. For questions, comments, or reports of
# potential bugs regarding this software please contact wald@usgs.gov or
# klin@usgs.gov.  
#
#############################################################################
*/

define('IN_SC', 1);
if( !empty($setmodules) )
{
	$file = basename(__FILE__);
	$module[' General Admin']['Task_Repeater'] = $file;
	return;
}

@set_time_limit(120);

function getCSVValues($string, $separator=",")
{
    $elements = explode($separator, $string);
    for ($i = 0; $i < count($elements); $i++) {
        $nquotes = substr_count($elements[$i], '"');
        if ($nquotes %2 == 1) {
            for ($j = $i+1; $j < count($elements); $j++) {
                if (substr_count($elements[$j], '"') > 0) {
                    // Put the quoted string's pieces back together again
                    array_splice($elements, $i, $j-$i+1,
                        implode($separator, array_slice($elements, $i, $j-$i+1)));
                    break;
                }
            }
        }
        if ($nquotes > 0) {
            // Remove first and last quotes, then merge pairs of quotes
            $qstr =& $elements[$i];
            $qstr = substr_replace($qstr, '', strpos($qstr, '"'), 1);
            $qstr = substr_replace($qstr, '', strrpos($qstr, '"'), 1);
            $qstr = str_replace('""', '"', $qstr);
        }
    }
    return $elements;
}

//
// Load default header
//
$sc_root_path = "./../";
require($sc_root_path . 'extension.inc');
require('./pagestart.' . $phpEx);
include($sc_root_path . 'includes/functions_selects.'.$phpEx);

$result = @exec('schtasks /query /v /fo csv /nh', $output);
foreach ($output as $line)
{
	if (preg_match("/^\s*$/", $line)) {continue;}
	$fields = getCSVValues($line);
	$id = $fields[1];
	$cron[$id] = $id;
}

if( isset($HTTP_GET_VARS['mode']) || isset($HTTP_POST_VARS['mode']) )
{
	$mode = (isset($HTTP_GET_VARS['mode'])) ? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode'];
	$mode = htmlspecialchars($mode);
}
else 
{
	$mode = "";
}

// Restrict mode input to valid options
$utils = array('heartbeat', 'logstats', 'logrotate');
$mode = ( in_array($mode, array('delete', 'add', 'save')) ) ? $mode : '';
$default_lines = 500;
$default_max_lines = 5000;
$default_filter = 'no_filter';

if( $mode == "add" )
{
	$template->set_filenames(array(
		"body" => "admin/cron_edit_body.tpl")
	);

	
    $timestamp=mktime();
    $hour = date('H', $timestamp);
    $minute = date('i', $timestamp);
	$template->assign_vars(array(
		"TASK_TYPE" => task_select(),
		"REPEAT" => repeater_select(),
		"HOUR_SELECT" => clock_select($hour, 'hour'),
		"MINUTE_SELECT" => clock_select($minute, 'minute'),

		"L_TASK_TYPE" => $lang['Taskname'],
		"L_OPT_PARM" => $lang['Opt_parm'],
		"L_OPT_PARM_EXPLAIN" => $lang['Opt_parm_explain'],
		"L_TASK_DATE" => $lang['Next_run_time'],
		"L_REPEAT" => $lang['Repeat'],
		"L_USERNAME" => $lang['Username'],
		"L_PASSWORD" => $lang['Password'],

		"L_WORDS_TITLE" => $lang['Task_Repeater'],
		"L_WORDS_TEXT" => $lang['Task_Repeater_explain'],
		"L_TASK_ADD" => $lang['Add_Task'],
		"L_SUBMIT" => $lang['Submit'],

		"S_FACILITY_ACTION" => append_sid("admin_cron.$phpEx"),
		"S_HIDDEN_FIELDS" => '<input type="hidden" name="mode" id="mode" value="save" />')
	);

	$template->pparse("body");

	include('./page_footer_admin.'.$phpEx);
}
else if( $mode == "save" )
{
	$task = ( isset($HTTP_POST_VARS['task']) ) ? $HTTP_POST_VARS['task'] : '';
	$opt_parm = ( isset($HTTP_POST_VARS['opt_parm']) ) ? $HTTP_POST_VARS['opt_parm'] : '';
	$repeater = ( isset($HTTP_POST_VARS['repeater']) ) ? $HTTP_POST_VARS['repeater'] : '';
	$task_date = ( isset($HTTP_POST_VARS['task_date']) ) ? $HTTP_POST_VARS['task_date'] : '';
	$username = ( isset($HTTP_POST_VARS['username']) ) ? $HTTP_POST_VARS['username'] : '';
	$password = ( isset($HTTP_POST_VARS['password']) ) ? $HTTP_POST_VARS['password'] : '';
	$hour = ( isset($HTTP_POST_VARS['hour']) ) ? intval($HTTP_POST_VARS['hour']) : 0;
	$minute = ( isset($HTTP_POST_VARS['minute']) ) ? intval($HTTP_POST_VARS['minute']) : 0;

	$result = preg_match("/(\d+)\/(\d+)\/(\d+)/", $task_date, $match);
	$task_date = sprintf("%02d/%02d/%04d", $match[1],$match[2],$match[3]);
	$result = preg_match("/(\d+)\/(\d+)\/(\d+)/", $task_date, $match);
	$start_time = sprintf("%02d:%02d:00", $hour, $minute);

	if (! in_array($task, array('heartbeat', 'logstats', 'logrotate', 'shake_fetch', 'tester')) ) 
	{
		message_die(GENERAL_ERROR, $lang['Invalid_Task_Error'], $lang['Error'], __LINE__, __FILE__);
	}
	
	$parm = '';
	$opt_parm = trim($opt_parm);
	if ($task == 'shake_fetch')
	{
		if (preg_match("/-event/i", $opt_parm) && preg_match("/-network/i", $opt_parm))
		{
			$elements = explode(" ", $opt_parm);
			if (count($elements) != 4) 
			{
				message_die(GENERAL_ERROR, $lang['Invalid_Task_Option_Error'], $lang['Error'], __LINE__, __FILE__);
			}
			$parm = $opt_parm;
		} 
		else
		{
			message_die(GENERAL_ERROR, $lang['Invalid_Task_Option_Error'], $lang['Error'], __LINE__, __FILE__);
		}
	}
	else if ($task == 'tester')
	{
		if (preg_match("/-key/i", $opt_parm) && preg_match("/-type/i", $opt_parm))
		{
			$elements = explode(" ", $opt_parm);
			if (count($elements) != 4) 
			{
				message_die(GENERAL_ERROR, $lang['Invalid_Task_Option_Error'], $lang['Error'], __LINE__, __FILE__);
			}
			$parm = $opt_parm;
		} 
		else
		{
			message_die(GENERAL_ERROR, $lang['Invalid_Task_Option_Error'], $lang['Error'], __LINE__, __FILE__);
		}
	}
	
	if ($cron[$task]) 
	{
		message_die(GENERAL_ERROR, $lang['Task_Exist_Error'], $lang['Error'], __LINE__, __FILE__);
	}
	
	$task_path = str_replace("/", "\\", $board_config['RootDir'].'/bin/'.$task.'.pl '.$parm);
	
	$result = @exec('schtasks /create /ru '.$username.' /rp '.$password.' /tn '.$task.' /tr "perl '.$task_path.'" /sc '.$repeater.' /sd '. $task_date. ' /st '.$start_time, $output);

	foreach ($output as $line)
	{
		if (preg_match("/$task/", $line))
		{
			$message = "Task successfully scheduled.<br /><br />" . sprintf($lang['Click_return_taskadmin'], "<a href=\"" . append_sid("admin_cron.$phpEx") . "\">", "</a>") . "<br /><br />" . sprintf($lang['Click_return_admin_index'], "<a href=\"" . append_sid("index.$phpEx?pane=right") . "\">", "</a>");
		
			message_die(GENERAL_MESSAGE, $message);
		}
	}

	message_die(GENERAL_ERROR, $lang['Task_Repeater_Error'], $lang['Error'], __LINE__, __FILE__, 'schtasks /create /ru '.$username.' /rp '.$password.' /tn '.$task.' /tr "perl '.$task_path.'" /sc '.$repeater.' /sd '. $task_date. ' /st '.$start_time);
}
else if( $mode == "delete")
{
	if( isset($HTTP_GET_VARS['id']) || isset($HTTP_POST_VARS['id']) )
	{
		$id = (isset($HTTP_GET_VARS['id'])) ? $HTTP_GET_VARS['id'] : $HTTP_POST_VARS['id'];
		$id = htmlspecialchars($id);
	}
	else 
	{
		$id = "";
	}

	$confirm = isset($HTTP_POST_VARS['confirm']);

	if( $id && $confirm )
	{
		$result = exec('schtasks /delete /tn "'.$id . '" /f', $output);

		if(!$output)
		{
			message_die(GENERAL_ERROR, "Could not delete task", $lang['Error']);
		}

		$message = $lang['Task_deleted'] . "<br /><br />" . sprintf($lang['Click_return_taskadmin'], "<a href=\"" . append_sid("admin_cron.$phpEx") . "\">", "</a>") . "<br /><br />" . sprintf($lang['Click_return_admin_index'], "<a href=\"" . append_sid("index.$phpEx?pane=right") . "\">", "</a>");

		message_die(GENERAL_MESSAGE, $message);
	}
	elseif( $id && !$confirm )
	{
		// Present the confirmation screen to the user
		$template->set_filenames(array(
			'body' => 'admin/confirm_body.tpl')
		);

		$hidden_fields = '<input type="hidden" name="mode" value="'.$mode.'" />';
		$hidden_fields .= '<input type="hidden" name="id" value="' . $id . '" />';
		$message =  $lang['Confirm_task'];
		
		$template->assign_vars(array(
			'MESSAGE_TITLE' => $lang['Confirm'],
			'MESSAGE_TEXT' => $message,

			'L_YES' => $lang['Yes'],
			'L_NO' => $lang['No'],

			'S_CONFIRM_ACTION' => append_sid("admin_cron.$phpEx"),
			'S_HIDDEN_FIELDS' => $hidden_fields)
		);
	}
	else
	{
		message_die(GENERAL_MESSAGE, $lang['Task_Repeater_Error']);
	}
}
else
{
	$template->set_filenames(array(
		"body" => "admin/cron_list_body.tpl")
	);

	$template->assign_vars(array(
		"L_WORDS_TITLE" => $lang['Task_Repeater'],
		"L_WORDS_TEXT" => $lang['Task_Repeater_explain'],
		
		"L_STATUS" => $lang['Status'],
		"L_TASKNAME" => $lang['Taskname'],
		"L_NEXT_RUN_TIME" => $lang['Next_run_time'],
		"L_REPEAT" => $lang['Repeat'],
		"L_DESCRIPTION" => $lang['Description'],
		"L_DELETE" => $lang['Delete'],
		"L_ACTION" => $lang['Action'],
		"L_ADD_TASK" => $lang['Add_Task'],

		"S_WORDS_ACTION" => append_sid("admin_cron.$phpEx"),
		"S_HIDDEN_FIELDS" => '<input type="hidden" name="mode" id="mode" value="" />')
	);

	foreach ($output as $line)
	{
		if (preg_match("/^\s*$/", $line)) {continue;}
		$fields = getCSVValues($line);
		$id = $fields[1];
		
		$row_color = ( !($i % 2) ) ? $theme['td_color1'] : $theme['td_color2'];
		$row_class = ( !($i % 2) ) ? $theme['td_class1'] : $theme['td_class2'];
	
		$template->assign_block_vars("words", array(
			"ROW_COLOR" => "#" . $row_color,
			"ROW_CLASS" => $row_class,

			"L_RUN" => ( in_array($id, $utils) ) ? $lang['Run'] : '',
			"STATUS" => $fields[3],
			"TASKNAME" => $id,
			"NEXT_RUN_TIME" => $fields[2],
			"REPEAT" => $fields[12],
			"DESCRIPTION" => $fields[8],

			"U_RUN" => ( in_array($id, $utils) ) ? append_sid("admin_process.$phpEx?perform=$id") : '',
			"U_DELETE" => append_sid("admin_cron.$phpEx?mode=delete&amp;id=$id"))
		);
	}

}

$template->pparse("body");

include('./page_footer_admin.'.$phpEx);

?>