
function fetch_fragility (oFacility_type) {
	//alert(oFacility_type.options[oFacility_type.selectedIndex].value);
	var oXmlHttp = zXmlHttp.createRequest();
	//cancel any active requests                          
	if (oXmlHttp.readyState != 0) {
		oXmlHttp.abort();
	}                 

	//define the data
	var oData = { 
		requesting: "facility_type_fragility", 
		text: oFacility_type.options[oFacility_type.selectedIndex].value
	};
	
	oXmlHttp.open("post", "/suggestions.php", true);
	oXmlHttp.onreadystatechange = function () {
		if (oXmlHttp.readyState == 4) {
			if (oXmlHttp.status == 200) {
				var aSuggestions = JSON.parse(oXmlHttp.responseText);
				
				//create the layer and assign styles
				var layer = document.createElement("div");
				layer.className = "suggestions";
				layer.style.visibility = "hidden";
				//layer.style.width = oFacility_type.offsetWidth;
				document.body.appendChild(layer);    

				var oDiv = null;
				for (var i=0; i < aSuggestions.length; i++) {
					var fragLowTag = aSuggestions[i]['damage_level'] + '_low_limit';
					var fragHighTag = aSuggestions[i]['damage_level'] + '_high_limit';
					var fragSelectTag = aSuggestions[i]['damage_level'] + '_select';
					var oFragLow = document.getElementById(fragLowTag);
					if (oFragLow) {
						if (aSuggestions[i]['low_limit']) {
							oFragLow.value =  aSuggestions[i]['low_limit'];
						} else {
							oFragLow.value =  '';
						}
					}
					var oFragHigh = document.getElementById(fragHighTag);
					if (oFragHigh) {
						if (aSuggestions[i]['high_limit']) {
							oFragHigh.value = aSuggestions[i]['high_limit'];
						} else {
							oFragHigh.value = '';
						}
					}
					var oFragSelect = document.getElementById(fragSelectTag);
					if (oFragSelect) {
						for (j=0; j < oFragSelect.options.length; j++) {
							if (oFragSelect.options[j].value == aSuggestions[i]['metric']) {
								oFragSelect.options[j].selected = true;
							} else {
								oFragSelect.options[j].selected = false;
							}
						}
					}
				}
				
			} 
		}    
	}
	oXmlHttp.send(JSON.stringify(oData));

};

function fetch_type (oFacility_type) {
	//alert(oFacility_type.options[oFacility_type.selectedIndex].value);
	var oXmlHttp = zXmlHttp.createRequest();
	//cancel any active requests                          
	if (oXmlHttp.readyState != 0) {
		oXmlHttp.abort();
	}                 

	//define the data
	var oData = { 
		requesting: "facility_type", 
		text: oFacility_type.options[oFacility_type.selectedIndex].value
	};
	
	oXmlHttp.open("post", "/suggestions.php", true);
	oXmlHttp.onreadystatechange = function () {
		if (oXmlHttp.readyState == 4) {
			if (oXmlHttp.status == 200) {
				var aSuggestions = JSON.parse(oXmlHttp.responseText);
				
				var facility_type = document.getElementById("facility_type");
				if (facility_type) {
				facility_type.value = aSuggestions[0]['facility_type'];
				}
		
				var name = document.getElementById("name");
				name.value = aSuggestions[0]['name'];
		
				var description = document.getElementById("description");
				description.value = aSuggestions[0]['description'];
		
			} 
		}    
	}
	oXmlHttp.send(JSON.stringify(oData));

};

function fetch_type_id (oFacility_type, url) {
	var type_id = oFacility_type.options[oFacility_type.selectedIndex].value;
	document.location.href = url + '&id=' + type_id;

};

