<?php
/*
# $Id: admin_process.php 295 2008-01-22 17:33:07Z klin $

# The PHP interface was originally inspired by PHPBB,
# file/database formats and directory structure are quite similar.

##############################################################################
# 
# Terms and Conditions of Software Use
# ====================================
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Disclaimer of Earthquake Information
# ====================================
# 
# The data and maps provided through this system are preliminary data
# and are subject to revision. They are computer generated and may not
# have received human review or official approval. Inaccuracies in the
# data may be present because of instrument or computer
# malfunctions. Subsequent review may result in significant revisions to
# the data. All efforts have been made to provide accurate information,
# but reliance on, or interpretation of earthquake data from a single
# source is not advised. Data users are cautioned to consider carefully
# the provisional nature of the information before using it for
# decisions that concern personal or public safety or the conduct of
# business that involves substantial monetary or operational
# consequences.
# 
# Disclaimer of Software and its Capabilities
# ===========================================
# 
# This software is provided as an "as is" basis.  Attempts have been
# made to rid the program of software defects and bugs, however the
# U.S. Geological Survey (USGS) have no obligations to provide maintenance, 
# support, updates, enhancements or modifications. In no event shall USGS 
# be liable to any party for direct, indirect, special, incidental or 
# consequential damages, including lost profits, arising out of the use 
# of this software, its documentation, or data obtained though the use 
# of this software, even if USGS or have been advised of the
# possibility of such damage. By downloading, installing or using this
# program, the user acknowledges and understands the purpose and
# limitations of this software.
# 
# Contact Information
# ===================
# 
# Coordination of this effort is under the auspices of the USGS Advanced
# National Seismic System (ANSS) coordinated in Golden, Colorado, which
# functions as the clearing house for development, distribution,
# documentation, and support. For questions, comments, or reports of
# potential bugs regarding this software please contact wald@usgs.gov or
# klin@usgs.gov.  
#
#############################################################################
*/

define('IN_SC', 1);

if( !empty($setmodules) )
{
	return;
}

//
// Load default header
//
$no_page_header = TRUE;
$sc_root_path = './../';
require($sc_root_path . 'extension.inc');
require('./pagestart.' . $phpEx);

//
// Increase maximum execution time in case of a lot of users, but don't complain about it if it isn't
// allowed.
//
@set_time_limit(1200);

$message = '';
$subject = '';

//
// Do the job ...
//
if( isset($HTTP_GET_VARS['perform']) || isset($HTTP_POST_VARS['perform']) )
{
	$perform = (isset($HTTP_POST_VARS['perform'])) ? $HTTP_POST_VARS['perform'] : $HTTP_GET_VARS['perform'];

	switch($perform)
	{
		case 'stop':
	
			//
			// Get system service statistics
			//
			$result = exec('C:\ShakeCast\admin\stop_sc_services.bat', $output);
	
			if (isset($output))
			{
				// reload page
			echo '<html>\n<body onload="javascript: self.location.href = \'' . append_sid("index.$phpEx?pane=right") . '\';">\nlogrotate\n<br /><br />\n' . $msg_text . "</body>\n</html>";
				//header("Location: " . append_sid("index.$phpEx?pane=right"));
			}

			break;

		case 'start':

			//
			// Get system service statistics
			//
			$result = exec('C:\ShakeCast\admin\start_sc_services.bat', $output);
	
			if (isset($output))
			{
				// reload page
			echo '<html>\n<body onload="javascript: self.location.href = \'' . append_sid("index.$phpEx?pane=right") . '\';">\nlogrotate\n<br /><br />\n' . $msg_text . "</body>\n</html>";
				//header("Location: " . append_sid("index.$phpEx?pane=right"));
			}

			break;
			
		case 'logstats':
	
			//
			// Get system service statistics
			//
			$result = exec('perl C:\ShakeCast\sc\bin\logstats.pl', $output);
	
			if (isset($output))
			{
				// reload page
			echo '<html>\n<body onload="javascript: self.location.href = \'' . append_sid("index.$phpEx?pane=right") . '\';">\nlogrotate\n<br /><br />\n' . $msg_text . "</body>\n</html>";
				//header("Location: " . append_sid("index.$phpEx?pane=right"));
			}

			break;

		case 'logrotate':
	
			//
			// Get system service statistics
			//
			$result = exec('perl C:\ShakeCast\sc\bin\logstats.pl', $output);
	
			if (isset($output))
			{
				// reload page
			echo '<html>\n<body onload="javascript: self.location.href = \'' . append_sid("index.$phpEx?pane=right") . '\';">\nlogrotate\n<br /><br />\n' . $msg_text . "</body>\n</html>";
				//redirect(append_sid("index.$phpEx?pane=right"));
			}

			break;

		case 'heartbeat':
	
			//
			// Get system service statistics
			//
			$result = exec('perl C:\ShakeCast\sc\bin\heartbeat.pl', $output);
	
			if (isset($output))
			{
				// reload page
			echo '<html>\n<body onload="javascript: self.location.href = \'' . append_sid("index.$phpEx?pane=right") . '\';">\nlogrotate\n<br /><br />\n' . $msg_text . "</body>\n</html>";
				//redirect(append_sid("index.$phpEx?pane=right"));
			}

			break;

	}
}

include('./page_footer_admin.'.$phpEx);

?>