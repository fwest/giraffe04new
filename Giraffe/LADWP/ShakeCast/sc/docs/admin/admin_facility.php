<?php
/*
# $Id: admin_facility.php 511 2008-10-20 14:51:19Z klin $

# The PHP interface was originally inspired by PHPBB,
# file/database formats and directory structure are quite similar.

##############################################################################
# 
# Terms and Conditions of Software Use
# ====================================
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Disclaimer of Earthquake Information
# ====================================
# 
# The data and maps provided through this system are preliminary data
# and are subject to revision. They are computer generated and may not
# have received human review or official approval. Inaccuracies in the
# data may be present because of instrument or computer
# malfunctions. Subsequent review may result in significant revisions to
# the data. All efforts have been made to provide accurate information,
# but reliance on, or interpretation of earthquake data from a single
# source is not advised. Data users are cautioned to consider carefully
# the provisional nature of the information before using it for
# decisions that concern personal or public safety or the conduct of
# business that involves substantial monetary or operational
# consequences.
# 
# Disclaimer of Software and its Capabilities
# ===========================================
# 
# This software is provided as an "as is" basis.  Attempts have been
# made to rid the program of software defects and bugs, however the
# U.S. Geological Survey (USGS) have no obligations to provide maintenance, 
# support, updates, enhancements or modifications. In no event shall USGS 
# be liable to any party for direct, indirect, special, incidental or 
# consequential damages, including lost profits, arising out of the use 
# of this software, its documentation, or data obtained though the use 
# of this software, even if USGS or have been advised of the
# possibility of such damage. By downloading, installing or using this
# program, the user acknowledges and understands the purpose and
# limitations of this software.
# 
# Contact Information
# ===================
# 
# Coordination of this effort is under the auspices of the USGS Advanced
# National Seismic System (ANSS) coordinated in Golden, Colorado, which
# functions as the clearing house for development, distribution,
# documentation, and support. For questions, comments, or reports of
# potential bugs regarding this software please contact wald@usgs.gov or
# klin@usgs.gov.  
#
#############################################################################
*/

if( !empty($setmodules) )
{
	$file = basename(__FILE__);
	$module['Facility_Admin'][' Management'] = $file;
	return;
}

define('IN_SC', 1);

//
// Load default header
//
$tester = '/shakecast/sc/bin/tester.pl';
$admin = '/shakecast/sc/bin/admin.pl';
$sc_root_path = "./../";
require($sc_root_path . 'extension.inc');

$cancel = ( isset($HTTP_POST_VARS['cancel']) ) ? true : false;
$no_page_header = $cancel;

$template_file = 'page_facility_header.tpl';
require('./pagestart.' . $phpEx);

if ($cancel)
{
	redirect('admin/' . append_sid("admin_facility.$phpEx", true));
}

if( isset($HTTP_GET_VARS['mode']) || isset($HTTP_POST_VARS['mode']) )
{
	$mode = (isset($HTTP_GET_VARS['mode'])) ? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode'];
	$mode = htmlspecialchars($mode);
}
else 
{
	//
	// These could be entered via a form button
	//
	if( isset($HTTP_POST_VARS['add_event']) )
	{
		$mode = "add";
	}
	else if( isset($HTTP_POST_VARS['save']) )
	{
		$mode = "save";
	}
	else if( isset($HTTP_POST_VARS['delete']) )
	{
		$mode = "delete";
	}
	else
	{
		$mode = "";
	}
}

// Restrict mode input to valid options
$mode = ( in_array($mode, array('add', 'edit', 'save', 'inject_first', 'inject_next', 'delete', 'delete_all', 'add_event')) ) ? $mode : '';

if( $mode != "" )
{
	if( $mode == "edit" || $mode == "add" )
	{
		if( isset($HTTP_GET_VARS['id']) || isset($HTTP_POST_VARS['id']) )
		{
			$fac_id = (isset($HTTP_GET_VARS['id'])) ? $HTTP_GET_VARS['id'] : $HTTP_POST_VARS['id'];
		}
		else
		{
			$fac_id = 0;
		}

		$template->set_filenames(array(
			"body" => "admin/facility_edit_body.tpl")
		);


		$facility = array('facility_id' => '', 'facility_type' => '', 'external_facility_id' => '',
			'facility_name' => '', 'short_name' => '', 'description' => '',
			'lat_min' => '', 'lat_max' => '', 'lon_min' => '', 'lon_max' => '');
		$facility_fragility = array();
		$facility_attribute = array();
		$s_hidden_fields = '<input type="hidden" name="facility[]" value="' . $fac_id . '" />';
//		include($sc_root_path.'/includes/facility_attribute.inc');

		if( $mode == "edit" )
		{
			if( $fac_id )
			{
				$sql = "SELECT facility_id, facility_type, external_facility_id, facility_name, short_name,
							description, lat_min, lat_max, lon_min, lon_max 
						FROM facility WHERE facility_id = $fac_id";
				$result = $db->sql_query($sql);
				if( !$result )
				{
					message_die(GENERAL_ERROR, "Couldn't get facility types.", "", __LINE__, __FILE__, $sql );
				}
				$facility = $db->sql_fetchrow($result);
				
				$sql = "SELECT ff.facility_fragility_id, ff.damage_level, dl.name, ff.low_limit, ff.high_limit, ff.metric 
						FROM facility_fragility ff INNER JOIN damage_level dl on ff.damage_level = dl.damage_level
						WHERE ff.facility_id = $fac_id";
				$result = $db->sql_query($sql);
				if($result)
				{
					while ($row = $db->sql_fetchrow($result)) {
						$facility_fragility[$row['facility_fragility_id']] = array(
							"damage_level"	=>	$row['damage_level'],
							"name"	=>	$row['name'],
							"low_limit"	=>	$row['low_limit'],
							"high_limit"	=>	$row['high_limit'],
							"metric"	=>	$row['metric']);
					}
				}
				
				$sql = "SELECT attribute_name, attribute_value FROM facility_attribute WHERE facility_id = $fac_id";
				$result = $db->sql_query($sql);
				if($result )
				{
					while ($row = $db->sql_fetchrow($result)) {
						$facility_attribute[$row['attribute_name']] = $row['attribute_value'];
					}
				}
				$hidden_fields = '<input type="hidden" name="fac_id" value="'.$fac_id.'" />';
			}
			else
			{
				message_die(GENERAL_MESSAGE, $lang['No_facility_selected']);
			}
		} else {
			$hidden_fields = '<input type="hidden" name="fac_id" value="0" />';
		}
		// Grab the current list of facility types
		$sql = "SELECT facility_type, name FROM facility_type";
		$result = $db->sql_query($sql);
		if( !$result )
		{
			message_die(GENERAL_ERROR, "Couldn't get facility types.", "", __LINE__, __FILE__, $sql );
		}
		$fac_type = $db->sql_fetchrowset($result);
		$fac_type_select = '<select name="facility_type" id="facility_type"  onChange=fetch_fragility(this)><option value="">[Select Type]</option>';
		if( trim($fac_type) )
		{
			for( $i = 0; $i < count($fac_type); $i++ )
			{
				if ($fac_type[$i]['facility_type'] == $facility['facility_type']) {
					$fac_type_select .= '<option value="' . $fac_type[$i]['facility_type'] . '" selected>' . $fac_type[$i]['name'] . '</option>';
				} else {
					$fac_type_select .= '<option value="' . $fac_type[$i]['facility_type'] . '">' . $fac_type[$i]['name'] . '</option>';
				}
			}
		}
		$fac_type_select .= '</select>';
		
		$template->assign_vars(array(
			"FACILITY_TYPE" => $fac_type_select,
			"EXTERNAL_FACILITY_ID" => $facility['external_facility_id'],
			"FACILITY_NAME" => $facility['facility_name'],
			"SHORT_NAME" => $facility['short_name'],
			"DESCRIPTION" => $facility['description'],
			"LAT_MIN" => $facility['lat_min'],
			"LAT_MAX" => $facility['lat_max'],
			"LON_MIN" => $facility['lon_min'],
			"LON_MAX" => $facility['lon_max'],
			"F_HIDDEN_FIELDS" => $hidden_fields,
			"FAC_ID" => $fac_id,

			"L_FACILITY_TYPE" => $lang['facility_type'],
			"L_FACILITY_TYPE_EXPLAIN" => $lang['facility_type_explain'],
			"L_EXTERNAL_FACILITY_ID" => $lang['external_facility_id'],
			"L_EXTERNAL_FACILITY_ID_EXPLAIN" => $lang['external_facility_id_explain'],
			"L_FACILITY_NAME" => $lang['facility_name'],
			"L_FACILITY_NAME_EXPLAIN" => $lang['facility_name_explain'],
			"L_SHORT_NAME" => $lang['short_name'],
			"L_SHORT_NAME_EXPLAIN" => $lang['short_name_explain'],
			"L_FACILITY_DESCRIPTION" => $lang['facility_description'],
			"L_FACILITY_DESCRIPTION_EXPLAIN" => $lang['facility_description_explain'],
			"L_LATITUDE" => $lang['LATITUDE'],
			"L_LATITUDE_EXPLAIN" => $lang['LATLON_EXPLAIN'],
			"L_LONGITUDE" => $lang['LONGITUDE'],
			"L_LONGITUDE_EXPLAIN" => $lang['LATLON_EXPLAIN'],

			"L_DAMAGE_LEVEL" => "Damage Level",
			"L_LOW_LIMIT" => "Low Limit",
			"L_HIGH_LIMIT" => "High Limit",
			"L_METRIC" => "Metric",

			"L_FACILITY_TITLE" => $lang['fac_title'],
			"L_FACILITY_TEXT" => $lang['fac_explain'],
			"L_FACILITY_EDIT" => $lang['facility_edit'],
			"L_FACILITY_ATTRIBUTE" => $lang['facility_attribute'].' (optional)',
			"L_SUBMIT" => $lang['Submit'],
			"L_DELETE" => $lang['Delete'],
			"U_DELETE" => append_sid("admin_facility.$phpEx?mode=delete&amp;id=$fac_id"),
			"U_SEARCH" => append_sid("admin_facility.$phpEx?mode=edit"),
			"U_GM_FORM" => append_sid("gm_form.$phpEx?shape=rect"),

			"S_FACILITY_ACTION" => append_sid("admin_facility.$phpEx"),
			"S_HIDDEN_FIELDS" => $s_hidden_fields)
		);

		// Grab the current list of damage levels
		$sql = "SELECT damage_level, name FROM damage_level order by severity_rank";
		$result = $db->sql_query($sql);
		if( !$result )
		{
			message_die(GENERAL_ERROR, "Couldn't get damage levels.", "", __LINE__, __FILE__, $sql );
		}
		$damage_level = $db->sql_fetchrowset($result);
		
		// Grab the current list of metric
		$sql = "SELECT short_name, name FROM metric";
		$result = $db->sql_query($sql);
		if( !$result )
		{
			message_die(GENERAL_ERROR, "Couldn't get metric.", "", __LINE__, __FILE__, $sql );
		}
		$metric = $db->sql_fetchrowset($result);

		for( $index = 0; $index < count($damage_level); $index++ ) {
			$facility_damage_level = $damage_level[$index]['damage_level'];
			$facility_damage_name = $damage_level[$index]['name'];
			$facility_low_limit = '';
			$facility_high_limit = '';
			$facility_metric = '';
			$facility_fragility_id = '';
			foreach ($facility_fragility as $fac_frag_id => $fac_frag) {
				if ($fac_frag['damage_level'] == $damage_level[$index]['damage_level']) {
					$facility_fragility_id = $fac_frag_id;
					$facility_low_limit = $fac_frag['low_limit'];
					$facility_high_limit = $fac_frag['high_limit'];
					$facility_metric = $fac_frag['metric'];
				}
			}
			if (!$facility_fragility_id) {$facility_fragility_id = $index;}
			$fr_hidden_fields = '<input type="hidden" name="frag_id[]" value="'.$facility_fragility_id.'" />';
			$fr_hidden_fields .= '<input type="hidden" name="FR'.$facility_fragility_id.'[damage_level]" value="'.$facility_damage_level.'" />';
			
			$metric_select = '<select name="FR'.$facility_fragility_id.'[metric]"  id="'.$facility_damage_level.'_select" >';
			for( $i = 0; $i < count($metric); $i++ )
			{
				if (strtoupper($metric[$i]['short_name']) == 'MMI') {
					$metric_name = $metric[$i]['name']; 
				} else {
					$metric_name = $metric[$i]['name'] . " " . $metric_unit[strtoupper($metric[$i]['short_name'])]; 
				}
		
				if($metric[$i]['short_name'] == $facility_metric)
				{
					$metric_select .= '<option value="' . $metric[$i]['short_name'] . '" selected>' . $metric_name . '</option>';
				} else {
					$metric_select .= '<option value="' . $metric[$i]['short_name'] . '">' . $metric_name . '</option>';
				}
			}
			$metric_select .= '</select>';

			$row_color = ( !($i % 2) ) ? $theme['td_color1'] : $theme['td_color2'];
			$row_class = ( !($i % 2) ) ? $theme['td_class1'] : $theme['td_class2'];
	
			$template->assign_block_vars('fragrow', array(
				"ROW_COLOR" => "#" . $row_color,
				"ROW_CLASS" => $row_class,

				"FRAG_ID" => $facility_fragility_id,
				"FR_HIDDEN_FIELDS" => $fr_hidden_fields,
				"DAMAGE_LEVEL" => $facility_damage_name,
				"DAMAGE" => $facility_damage_level,
				"LOW_LIMIT" => $facility_low_limit,
				"HIGH_LIMIT" => $facility_high_limit,
				"METRIC" => $metric_select)
			);
		}
		
		// Grab the current list of facility attributes
		$sql = "SELECT attribute_name 
				FROM ". FACILITY_TYPE_ATTRIBUTE_TABLE ."
				WHERE facility_type = '". $facility['facility_type'] ."'";
		$result = $db->sql_query($sql);
		if( $result )
		{
			while ($row = $db->sql_fetchrow($result)) {
				$attribute_value = ($facility_attribute[$row['attribute_name']]) ? $facility_attribute[$row['attribute_name']] : '';
				$template->assign_block_vars('attrow', array(
					"L_ATTRIB_NAME" => $row['attribute_name'],
					"ATTRIBUTE_VALUE" => $attribute_value)
				);
			}
		}
		$template->pparse("body");

		include('./page_footer_admin.'.$phpEx);
	}
	else if( $mode == "save" )
	{
		$fac_id = ( isset($HTTP_POST_VARS['fac_id']) ) ? intval($HTTP_POST_VARS['fac_id']) : 0;
		/*$word = ( isset($HTTP_POST_VARS['word']) ) ? trim($HTTP_POST_VARS['word']) : "";
		$replacement = ( isset($HTTP_POST_VARS['replacement']) ) ? trim($HTTP_POST_VARS['replacement']) : "";

		if($word == "" || $replacement == "")
		{
			message_die(GENERAL_MESSAGE, $lang['Must_enter_word']);
		}*/
		$sqls = array();
		$lat_max = is_numeric($HTTP_POST_VARS['lat_max']) ? $HTTP_POST_VARS['lat_max'] : $HTTP_POST_VARS['lat_min'];
		$lon_max = is_numeric($HTTP_POST_VARS['lon_max']) ? $HTTP_POST_VARS['lon_max'] : $HTTP_POST_VARS['lon_min'];
		if( $fac_id > 0)
		{
			$sqls[] = "UPDATE " . FACILITY_TABLE . " 
				SET facility_type = '".str_replace("\'", "''", $HTTP_POST_VARS['facility_type'])."', 
					external_facility_id = '" . str_replace("\'", "''", $HTTP_POST_VARS['external_facility_id']) . "', 
					facility_name = '" . str_replace("\'", "''", $HTTP_POST_VARS['facility_name']) . "', 
					short_name = '" . str_replace("\'", "''", $HTTP_POST_VARS['short_name']) . "', 
					description = '" . str_replace("\'", "''", $HTTP_POST_VARS['description']) . "', 
					lat_min = " . $HTTP_POST_VARS['lat_min'] . ", 
					lat_max = " . $lat_max . ", 
					lon_min = " . $HTTP_POST_VARS['lon_min'] . ", 
					lon_max = " . $lon_max . ", 
					update_username = 'webadmin', 
					update_timestamp = now() 
				WHERE facility_id = $fac_id";
			foreach ($HTTP_POST_VARS['FA'] as $attrib_name => $attrib_value) {
				$sql = "SELECT $attrib_name FROM ". FACILITY_ATTRIBUTE_TABLE . "
						WHERE facility_id = $fac_id";
				if($result = $db->sql_query($sql))
				{
					$sqls[] = "UPDATE " . FACILITY_ATTRIBUTE_TABLE . "
						SET attribute_value = '" . str_replace("\'", "''", $attrib_value) . "' 
						WHERE facility_id = $fac_id AND attribute_name = '$attrib_name'";
				} else {
					$sqls[] = "INSERT INTO " . FACILITY_ATTRIBUTE_TABLE . "
						(facility_id, attribute_name, attribute_value) VALUES (
						$fac_id, '" . str_replace("\'", "''", $attrib_name) . "', '" 
						. str_replace("\'", "''", $attrib_value) . "')";
				}
			}
			foreach ($HTTP_POST_VARS['frag_id'] as $frag_id) {
				$fac_frag = $HTTP_POST_VARS['FR'.$frag_id];
				$sql = "SELECT damage_level FROM ". FACILITY_FRAGILITY_TABLE . "
						WHERE facility_id = $fac_id AND facility_fragility_id = $frag_id";
				if($result = $db->sql_query($sql))
				{
					$sqls[] = "UPDATE " . FACILITY_FRAGILITY_TABLE . "
						SET low_limit = " . $fac_frag['low_limit'] . ", 
							high_limit = " . $fac_frag['high_limit'] . ", 
							metric = '" . str_replace("\'", "''", $fac_frag['metric']) . "', 
							update_username = 'webadmin', 
							update_timestamp = now() 
						WHERE facility_id = $fac_id AND facility_fragility_id = $frag_id";
				} else if ($fac_frag['low_limit'] && $fac_frag['high_limit']) {
					$sqls[] = "INSERT INTO " . FACILITY_FRAGILITY_TABLE . "
							(facility_id, damage_level, low_limit, high_limit, metric, update_username, update_timestamp) VALUES (" . 
							$fac_id.", '".
							str_replace("\'", "''", $fac_frag['damage_level'])."', ".
							$fac_frag['low_limit'].", ".
							$fac_frag['high_limit'].", '".
							str_replace("\'", "''", $fac_frag['metric'])."', ".
							"'webadmin', now())"; 
				}
			}
			$message = "Facility information has been updated";
		}
		else
		{
			$sql_fac = "INSERT INTO " . FACILITY_TABLE . " 
				(facility_type, external_facility_id, facility_name, short_name, description,
					lat_min, lat_max, lon_min, lon_max, update_username, update_timestamp) VALUES ('".
					str_replace("\'", "''", $HTTP_POST_VARS['facility_type'])."', '".
					str_replace("\'", "''", $HTTP_POST_VARS['external_facility_id'])."', '". 
					str_replace("\'", "''", $HTTP_POST_VARS['facility_name'])."', '".
					str_replace("\'", "''", $HTTP_POST_VARS['short_name'])."', '".
					str_replace("\'", "''", $HTTP_POST_VARS['description'])."', ".
					$HTTP_POST_VARS['lat_min'].", ".
					$lat_max.", ".
					$HTTP_POST_VARS['lon_min'].", ".
					$lon_max.", ".
					"'webadmin', now())"; 
			if(!$result = $db->sql_query($sql_fac))
			{
				message_die(GENERAL_ERROR, "Could not insert data into facility table", $lang['Error'], __LINE__, __FILE__, $sql_fac);
			}
			$fac_id = $db->sql_nextid();
			foreach ($HTTP_POST_VARS['FA'] as $attrib_name => $attrib_value) {
				if($result = $db->sql_query($sql))
				{
					$sqls[] = "INSERT INTO " . FACILITY_ATTRIBUTE_TABLE . "
						(facility_id, attribute_value, attribute_name) VALUES (".
						$fac_id.", '".
						str_replace("\'", "''", $attrib_value)."', '".
						$attrib_name."')";
				}
			}
			foreach ($HTTP_POST_VARS['frag_id'] as $frag_id) {
				$fac_frag = $HTTP_POST_VARS['FR'.$frag_id];
				$sqls[] = "INSERT INTO " . FACILITY_FRAGILITY_TABLE . "
						(facility_id, damage_level, low_limit, high_limit, metric, update_username, update_timestamp) VALUES (" . 
						$fac_id.", '".
						str_replace("\'", "''", $fac_frag['damage_level'])."', ".
						$fac_frag['low_limit'].", ".
						$fac_frag['high_limit'].", '".
						str_replace("\'", "''", $fac_frag['metric'])."', ".
						"'webadmin', now())"; 
			}
			$message = "Facility information has been added";
		}

		foreach($sqls as $sql) {
			if(!$result = $db->sql_query($sql))
			{
				message_die(GENERAL_ERROR, "Could not insert data into facility table", $lang['Error'], __LINE__, __FILE__, $sql);
			}
		}
		$message .= "<br /><br />" . sprintf($lang['Click_return_facadmin'], "<a href=\"" . append_sid("admin_facility.$phpEx") . "\">", "</a>") . "<br /><br />" . sprintf($lang['Click_return_admin_index'], "<a href=\"" . append_sid("index.$phpEx?pane=right") . "\">", "</a>");

		message_die(GENERAL_MESSAGE, $message);
	}
	else if( $mode == "delete" || $mode == "delete_all")
	{
		$facility_list = ($HTTP_POST_VARS['facility']) ? $HTTP_POST_VARS['facility'] : '';

		$clear_cache = isset($HTTP_POST_VARS['clear_cache']) ? $HTTP_POST_VARS['clear_cache'] : 0;

		$confirm = isset($HTTP_POST_VARS['confirm']);

		if( ($facility_list && $confirm) || ($mode == "delete_all" && $confirm))
		{
			if ($mode == "delete_all")
			{
				$sc_tables = array('facility', 'facility_attribute', 'facility_fragility', 'facility_notification_request',
					'facility_shaking','geometry_facility_profile');
				foreach ($sc_tables as $table) {
					$sql = "TRUNCATE TABLE $table";
					$result = $db->sql_query($sql);
					if(!$result)
					{
						message_die(GENERAL_ERROR, "Could not delete facility", $lang['Error']);
					}
				}
				
				$png_path = $sc_root_path . '/tiles';
				$dir = @opendir($png_path);
				
				while( $file = @readdir($dir) )
				{
					if( preg_match("/^.*?\.png$/", $file) )
					{
						if( file_exists(phpbb_realpath($png_path . '/' . $file)) )
						{
							unlink(phpbb_realpath($png_path . '/' . $file));
						}
					}
				}
				
				@closedir($dir);
			}
			else
			{
				foreach ($facility_list as $event_id) {
					$result = exec('perl '.$admin.' -type delfac -key '.$event_id, $output);
		
					if(!$result)
					{
						message_die(GENERAL_ERROR, "Could not delete facility", $lang['Error']);
					}
				}
			}

			if ($clear_cache)
			{
				$png_path = $sc_root_path . '/tiles';
				$dir = @opendir($png_path);
				
				while( $file = @readdir($dir) )
				{
					if( preg_match("/^.*?\.png$/", $file) )
					{
						if( file_exists(phpbb_realpath($png_path . '/' . $file)) )
						{
							unlink(phpbb_realpath($png_path . '/' . $file));
						}
					}
				}
				
				@closedir($dir);
			}
			
			$message = $lang['facility_deleted'] . "<br /><br />" . sprintf($lang['Click_return_facadmin'], "<a href=\"" . append_sid("admin_facility.$phpEx") . "\">", "</a>") . "<br /><br />" . sprintf($lang['Click_return_admin_index'], "<a href=\"" . append_sid("index.$phpEx?pane=right") . "\">", "</a>");

			message_die(GENERAL_MESSAGE, $message);
		}
		elseif( ($facility_list && !$confirm) || ($mode == "delete_all" && !$confirm))
		{
			// Present the confirmation screen to the user
			$template->set_filenames(array(
				'body' => 'admin/confirm_body.tpl')
			);

			$hidden_fields = '<input type="hidden" name="mode" value="'.$mode.'" /><input type="hidden" name="clear_cache" value="'.$clear_cache.'" />';
			if ($mode == "delete_all")
			{
				$message =  $lang['Confirm_facility_all'];
			}
			else
			{
				foreach ($facility_list as $event_id) {
					$hidden_fields .= '<input type="hidden" name="facility[]" value="' . $event_id . '" />';
				}
				$message =  $lang['Confirm_facility'];
			}
			
			$template->assign_vars(array(
				'MESSAGE_TITLE' => $lang['Confirm'],
				'MESSAGE_TEXT' => $message,

				'L_YES' => $lang['Yes'],
				'L_NO' => $lang['No'],

				'S_CONFIRM_ACTION' => append_sid("admin_facility.$phpEx"),
				'S_HIDDEN_FIELDS' => $hidden_fields)
			);
		}
		else
		{
			message_die(GENERAL_MESSAGE, $lang['No_facility_selected']);
		}
	}
}
else
{
	$template->set_filenames(array(
		"body" => "admin/facility_list_body.tpl")
	);

	$facility_count = get_db_stat('facilitycount');
	if ( !$facility_count )
	{
		//
		// No group members
		//
		$template->assign_block_vars('switch_no_members', array());
		$template->assign_vars(array(
			'L_NO_MEMBERS' => $lang['No_group_members'])
		);
	}

	if (isset($HTTP_GET_VARS['sort_key']))
	{
		$sort_key = $HTTP_GET_VARS['sort_key'];
	}
	else 
	{
		$sort_key = 'facility_id';
	}
	$sort_order = ( isset($HTTP_GET_VARS['sort_order']) ) ? $HTTP_GET_VARS['sort_order'] : 'a';
	$sort_sql = ( $sort_order == 'a' ) ? 'asc' : 'desc';
	
	if( isset($HTTP_POST_VARS['start']) ||  isset($HTTP_GET_VARS['start']) )
	{
		$start = ( isset($HTTP_POST_VARS['start']) ) ? $HTTP_POST_VARS['start'] : $HTTP_GET_VARS['start'];
	}
	$current_page = ( !$facility_count ) ? 1 : ceil( $facility_count / $board_config['topics_per_page'] );
	$template->assign_vars(array(
		'PAGINATION' => generate_pagination("admin_facility.$phpEx?" . POST_GROUPS_URL . "=$group_id&sort_key=$sort_key&sort_order=$sort_order", $facility_count, $board_config['topics_per_page'], $start),
		'PAGE_NUMBER' => sprintf($lang['Page_of'], ( floor( $start / $board_config['topics_per_page'] ) + 1 ), $current_page ), 

		"U_SEARCH" => append_sid("admin_facility.$phpEx?mode=edit"),
		'L_GOTO_PAGE' => $lang['Goto_page'])
	);

	$exe_query = "perl $admin -type findfacs ";
	if ($board_config['topics_per_page']) { $exe_query .= ' -order "' . $sort_key . ' '. $sort_sql . '" -limit '.$board_config['topics_per_page'];}
	if ($start) { $exe_query .= " -offset $start";}
	$result = exec($exe_query, $output);
	$facility_count_display = count($output);
	$field_name = explode("::", $output[0]);

	$new_sort_order = ($sort_order == 'd') ? 'a' : 'd';
	$img_url = ' <img src="/images/' . $sort_order . '.png" border="0" width="10" height="10">';
	$base_url = append_sid("admin_facility.$phpEx?" . POST_GROUPS_URL . "=$group_id");
	
	$template->assign_vars(array(
		"L_WORDS_TITLE" => $lang['fac_title'],
		"L_WORDS_TEXT" => $lang['fac_explain'],
		"L_FAC_ID" => $lang['id'] . (($sort_key == 'facility_id') ? $img_url : ''),
		"L_FAC_TYPE" => $lang['facility_type'] . (($sort_key == 'facility_type') ? $img_url : ''),
		"L_FAC_NAME" => $lang['facility_name'] . (($sort_key == 'facility_name') ? $img_url : ''),
		"L_LATITUDE" => $lang['LATITUDE'] . (($sort_key == 'lat_min') ? $img_url : ''),
		"L_LONGITUDE" => $lang['LONGITUDE'] . (($sort_key == 'lon_min') ? $img_url : ''),
		"L_DESCRIPTION" => $lang['facility_description'] . (($sort_key == 'description') ? $img_url : ''),
		"L_EDIT" => $lang['Edit'],
		"L_DELETE" => $lang['Delete_Select'],
		"L_DELETE_ALL" => $lang['Delete_All'],
		"L_ADD_EVENT" => $lang['Add_new_facility'],
		"L_ACTION" => $lang['Action'],
		"L_SELECT" => $lang['Select'],
		"L_EXPORT" => $lang['Export_Facility'],

		'U_FAC_ID' =>  $base_url."&sort_key=facility_id&sort_order=" . (($sort_key == 'facility_id') ? $new_sort_order : $sort_order),
		'U_FAC_TYPE' => $base_url."&sort_key=facility_type&sort_order=" . (($sort_key == 'facility_type') ? $new_sort_order : $sort_order),
		'U_FAC_NAME' => $base_url."&sort_key=facility_name&sort_order=" . (($sort_key == 'facility_name') ? $new_sort_order : $sort_order),
		'U_LATITUDE' => $base_url."&sort_key=lat_min&sort_order=" . (($sort_key == 'lat_min') ? $new_sort_order : $sort_order),
		'U_LONGITUDE' => $base_url."&sort_key=lon_min&sort_order=" . (($sort_key == 'lon_min') ? $new_sort_order : $sort_order),
		'U_DESCRIPTION' => $base_url."&sort_key=description&sort_order=" . (($sort_key == 'description') ? $new_sort_order : $sort_order),

		"S_WORDS_ACTION" => append_sid("admin_facility.$phpEx"),
		"U_DELETE" => append_sid("admin_facility.$phpEx?mode=delete"),
		"U_ADD" => append_sid("admin_facility.$phpEx?mode=add"),
		"U_EXPORT" => append_sid("admin_db_utilities.$phpEx?perform=export_facility"),
		"S_HIDDEN_FIELDS" => '<input type="hidden" name="mode" id="mode" value="" />')
	);

	for($i = 1;$i < $facility_count_display;  $i++)
	{
		$fields = explode("::", $output[$i]);
		$fac_id = $fields[0];
		$fac_name = $fields[1];
		$fac_type = $fields[3];
		$desc = $fields[4];
		$lat_min = $fields[5];
		$lat_max = $fields[6];
		$lon_min = $fields[7];
		$lon_max = $fields[8];

		$row_color = ( !($i % 2) ) ? $theme['td_color1'] : $theme['td_color2'];
		$row_class = ( !($i % 2) ) ? $theme['td_class1'] : $theme['td_class2'];

		$template->assign_block_vars('words', array(
			"ROW_COLOR" => "#" . $row_color,
			"ROW_CLASS" => $row_class,
			"FAC_ID" => $fac_id,
			"FAC_NAME" => $fac_name,
			"FAC_TYPE" => $fac_type,
			"LAT_MIN" => $lat_min,
			"LAT_MAX" => $lat_max,
			"LON_MIN" => $lon_min,
			"LON_MAX" => $lon_max,
			"DESCRIPTION" => $desc,

			"U_EDIT" => append_sid("admin_facility.$phpEx?mode=edit&amp;id=$fac_id"),
			"U_DELETE" => append_sid("admin_facility.$phpEx?mode=delete&amp;id=$fac_id"))
		);
	}
}

$template->pparse("body");

include('./page_footer_admin.'.$phpEx);

?>