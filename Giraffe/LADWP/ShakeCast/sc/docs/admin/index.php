<?php
/*
# $Id: index.php 479 2008-09-24 18:49:24Z klin $

# The PHP interface was originally inspired by PHPBB,
# file/database formats and directory structure are quite similar.

##############################################################################
# 
# Terms and Conditions of Software Use
# ====================================
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Disclaimer of Earthquake Information
# ====================================
# 
# The data and maps provided through this system are preliminary data
# and are subject to revision. They are computer generated and may not
# have received human review or official approval. Inaccuracies in the
# data may be present because of instrument or computer
# malfunctions. Subsequent review may result in significant revisions to
# the data. All efforts have been made to provide accurate information,
# but reliance on, or interpretation of earthquake data from a single
# source is not advised. Data users are cautioned to consider carefully
# the provisional nature of the information before using it for
# decisions that concern personal or public safety or the conduct of
# business that involves substantial monetary or operational
# consequences.
# 
# Disclaimer of Software and its Capabilities
# ===========================================
# 
# This software is provided as an "as is" basis.  Attempts have been
# made to rid the program of software defects and bugs, however the
# U.S. Geological Survey (USGS) have no obligations to provide maintenance, 
# support, updates, enhancements or modifications. In no event shall USGS 
# be liable to any party for direct, indirect, special, incidental or 
# consequential damages, including lost profits, arising out of the use 
# of this software, its documentation, or data obtained though the use 
# of this software, even if USGS or have been advised of the
# possibility of such damage. By downloading, installing or using this
# program, the user acknowledges and understands the purpose and
# limitations of this software.
# 
# Contact Information
# ===================
# 
# Coordination of this effort is under the auspices of the USGS Advanced
# National Seismic System (ANSS) coordinated in Golden, Colorado, which
# functions as the clearing house for development, distribution,
# documentation, and support. For questions, comments, or reports of
# potential bugs regarding this software please contact wald@usgs.gov or
# klin@usgs.gov.  
#
#############################################################################
*/

define('IN_SC', 1);

//
// Load default header
//
$no_page_header = TRUE;
$sc_root_path = "./../";
require($sc_root_path . 'extension.inc');
require('./pagestart.' . $phpEx);

// ---------------
// Begin functions
//
function inarray($needle, $haystack)
{ 
	for($i = 0; $i < sizeof($haystack); $i++ )
	{ 
		if( $haystack[$i] == $needle )
		{ 
			return true; 
		} 
	} 
	return false; 
}
//
// End functions
// -------------

//
// Generate relevant output
//
if( isset($HTTP_GET_VARS['pane']) && $HTTP_GET_VARS['pane'] == 'left' )
{
	$dir = @opendir(".");

	$setmodules = 1;
	while( $file = @readdir($dir) )
	{
		if( preg_match("/^admin_.*?\." . $phpEx . "$/", $file) )
		{
			include('./' . $file);
		}
	}

	@closedir($dir);

	unset($setmodules);

	include('./page_header_admin.'.$phpEx);

	$template->set_filenames(array(
		"body" => "admin/index_navigate.tpl")
	);

	$template->assign_vars(array(
		"U_SHAKECAST_INDEX" => append_sid("../index.$phpEx"),
		"U_ADMIN_INDEX" => append_sid("index.$phpEx?pane=right"),

		"L_SHAKECAST_INDEX" => $lang['ShakeCast_index'],
		"L_ADMIN_INDEX" => $lang['Admin_Index'], 
		"L_PREVIEW_SHAKECAST" => $lang['Preview_shakeCast'])
	);

	ksort($module);

	while( list($cat, $action_array) = each($module) )
	{
		$cat = ( !empty($lang[$cat]) ) ? $lang[$cat] : preg_replace("/_/", " ", $cat);

		$template->assign_block_vars("catrow", array(
			"ADMIN_CATEGORY" => $cat)
		);

		ksort($action_array);

		$row_count = 0;
		while( list($action, $file)	= each($action_array) )
		{
			$row_color = ( !($row_count%2) ) ? $theme['td_color1'] : $theme['td_color2'];
			$row_class = ( !($row_count%2) ) ? $theme['td_class1'] : $theme['td_class2'];

			$action = ( !empty($lang[$action]) ) ? $lang[$action] : preg_replace("/_/", " ", $action);

			$template->assign_block_vars("catrow.modulerow", array(
				"ROW_COLOR" => "#" . $row_color,
				"ROW_CLASS" => $row_class, 

				"ADMIN_MODULE" => $action,
				"U_ADMIN_MODULE" => append_sid($file))
			);
			$row_count++;
		}
	}

	$template->pparse("body");

	include('./page_footer_admin.'.$phpEx);
}
elseif( isset($HTTP_GET_VARS['pane']) && $HTTP_GET_VARS['pane'] == 'right' )
{

	$template->set_filenames(array(
		"body" => "admin/index_body.tpl")
	);

	$template->assign_vars(array(
		"META" => '<meta http-equiv="refresh" content="30;url=' . append_sid("index.$phpEx?pane=right") . '">',
		"L_WELCOME" => $lang['Welcome_SC'],
		"L_ADMIN_INTRO" => $lang['Admin_intro'],
		"L_FORUM_STATS" => $lang['Forum_stats'],
		"L_SERVER_STATS" => $lang['Server_stats'],
		"L_WHO_IS_ONLINE" => $lang['Who_is_Online'],
		"L_USERNAME" => $lang['Username'],
		"L_LOCATION" => $lang['Location'],
		"L_LAST_UPDATE" => $lang['Last_updated'],
		"L_IP_ADDRESS" => $lang['IP_Address'],
		"L_STATISTIC" => $lang['Statistic'],
		"L_VALUE" => $lang['Value'],
		"L_NUMBER_POSTS" => $lang['Number_posts'],
		"L_POSTS_PER_DAY" => $lang['Posts_per_day'],
		"L_NUMBER_TOPICS" => $lang['Number_topics'],
		"L_TOPICS_PER_DAY" => $lang['Topics_per_day'],
		"L_NUMBER_USERS" => $lang['Number_users'],
		"L_USERS_PER_DAY" => $lang['Users_per_day'],
		"L_BOARD_STARTED" => $lang['Board_started'],
		"L_AVATAR_DIR_SIZE" => $lang['Avatar_dir_size'],
		"L_DB_SIZE" => $lang['Database_size'], 
		"L_FORUM_LOCATION" => $lang['Forum_Location'],
		"L_STARTED" => $lang['Login'],
		"L_DNS_ADDRESS" => "Upstream Server",
		"L_LAST_HEARD_FROM" => "Last Accessed",
		"L_ERROR_COUNT" => "Error Count",
		"L_SERVER_STATUS" => "Server Status",
		"L_BOARD_STARTED" => "ShakeCast System Activities",
		"L_SC_SERVICES" => "System Services",
		"L_NOTIFICATION" => "Latest Processed Event",
		"L_NOTIFIED_USER" => "User Notifications",
		"L_DELIVERY_TIMESTAMP" => "Delivery Timestamp",
		"L_GZIP_COMPRESSION" => $lang['Gzip_compression'])
	);

	include('./page_header_admin.'.$phpEx);

	//
	// Get upstream server statistics
	//
	$sql = "SELECT dns_address, last_heard_from, error_count, server_status
		FROM server  
		WHERE upstream_flag = 1 AND query_flag = 1";
	if( !($result = $db->sql_query($sql)) )
	{
		message_die(GENERAL_ERROR, 'Could not obtain server information', '', __LINE__, __FILE__, $sql);
	}

	if ($row = $db->sql_fetchrow($result)) {
		$dns_address = $row['dns_address'];
		$error_count = $row['error_count'];
		$last_heard_from = $row['last_heard_from'];
		$server_status = $row['server_status'];
	}
	$server_status_color = (strtolower($server_status) == 'alive') ? "green" : "red";
	
	//
	// Get system service statistics
	//
	$result = exec('C:\WINDOWS\SYSTEM32\net.exe start', $output);
	$sc_services = 0;
	foreach ($output as $line) {
		if (preg_match("/shakecast/i", $line)) {$sc_services++;}
	}
	$sc_services_color = ($sc_services == SC_SERVICES) ? "green" : "red";
	
	//
	// Get notification statistics
	//
	$sql = "SELECT shakemap_id, shakemap_version, grid_id
		FROM grid
		ORDER BY grid_id DESC LIMIT 1";
	if( !($result = $db->sql_query($sql)) )
	{
		message_die(GENERAL_ERROR, 'Could not obtain processed ShakeMap information', '', __LINE__, __FILE__, $sql);
	}

	$row = $db->sql_fetchrow($result);
	$evid = $row['shakemap_id'];
	$grid_id = $row['grid_id'];
	$evid_version = $row['shakemap_version'];
	$sql = "SELECT DISTINCT count(shakecast_user) as total
		FROM notification  
		WHERE (event_id = \"$evid\" AND event_version = $evid_version) OR 
			grid_id = $grid_id";
	if( !($result = $db->sql_query($sql)) )
	{
		message_die(GENERAL_ERROR, 'Could not obtain ShakeCast information for latest event', '', __LINE__, __FILE__, $sql);
	}
	$row = $db->sql_fetchrow($result);
	$notification_count = $row['total'];
	$sql = "SELECT queue_timestamp as queue, delivery_timestamp as delivery
		FROM notification  
		WHERE (event_id = \"$evid\" AND event_version = $evid_version) OR 
			grid_id = $grid_id
		Limit 1";
	if( !($result = $db->sql_query($sql)) )
	{
		message_die(GENERAL_ERROR, 'Could not obtain processed information for latest ShakeCast event', '', __LINE__, __FILE__, $sql);
	}
	$row = $db->sql_fetchrow($result);
	$queue_timestamp = ($row['queue']) ? $row['queue'] : "N/A";
	$delivery_timestamp = ($row['delivery']) ? $row['delivery'] : "N/A";
	$sql = "SELECT count(delivery_status) as delivery_count
		FROM notification  
		WHERE ((event_id = \"$evid\" AND event_version = $evid_version) OR 
			grid_id = $grid_id) AND delivery_status = \"COMPLETED\"
		Limit 1";
	if( !($result = $db->sql_query($sql)) )
	{
		message_die(GENERAL_ERROR, 'Could not obtain delivery information for latest ShakeCast event', '', __LINE__, __FILE__, $sql);
	}
	$row = $db->sql_fetchrow($result);
	$delivery_count = ($row['delivery_count']) ? $row['delivery_count'] : "0";
	$delivery_status_color = ($delivery_count == $notification_count) ? "green" : "red";

	$total_posts = get_db_stat('postcount');
	$total_users = get_db_stat('usercount');
	$total_topics = get_db_stat('topiccount');

	$start_date = create_date($board_config['default_dateformat'], $board_config['board_startdate'], $board_config['board_timezone']);

	$boarddays = ( time() - $board_config['board_startdate'] ) / 86400;

	$posts_per_day = sprintf("%.2f", $total_posts / $boarddays);
	$topics_per_day = sprintf("%.2f", $total_topics / $boarddays);
	$users_per_day = sprintf("%.2f", $total_users / $boarddays);

	//
	// DB size ... MySQL only
	//
	// This code is heavily influenced by a similar routine
	// in phpMyAdmin 2.2.0
	//
	if( preg_match("/^mysql/", SQL_LAYER) )
	{
		$sql = "SELECT VERSION() AS mysql_version";
		if($result = $db->sql_query($sql))
		{
			$row = $db->sql_fetchrow($result);
			$version = $row['mysql_version'];

			if( preg_match("/^(3\.23|4\.|5\.)/", $version) )
			{
				$db_name = ( preg_match("/^(3\.23\.[6-9])|(3\.23\.[1-9][1-9])|(4\.)|(5\.)/", $version) ) ? "`$dbname`" : $dbname;

				$sql = "SHOW TABLE STATUS 
					FROM " . $db_name;
				if($result = $db->sql_query($sql))
				{
					$tabledata_ary = $db->sql_fetchrowset($result);

					$dbsize = 0;
					for($i = 0; $i < count($tabledata_ary); $i++)
					{
						if( $tabledata_ary[$i]['Type'] != "MRG_MyISAM" )
						{
							$dbsize += $tabledata_ary[$i]['Data_length'] + $tabledata_ary[$i]['Index_length'];
						}
					}
				} // Else we couldn't get the table status.
			}
			else
			{
				$dbsize = $lang['Not_available'];
			}
		}
		else
		{
			$dbsize = $lang['Not_available'];
		}
	}
	else if( preg_match("/^mssql/", SQL_LAYER) )
	{
		$sql = "SELECT ((SUM(size) * 8.0) * 1024.0) as dbsize 
			FROM sysfiles"; 
		if( $result = $db->sql_query($sql) )
		{
			$dbsize = ( $row = $db->sql_fetchrow($result) ) ? intval($row['dbsize']) : $lang['Not_available'];
		}
		else
		{
			$dbsize = $lang['Not_available'];
		}
	}
	else
	{
		$dbsize = $lang['Not_available'];
	}

	if ( is_integer($dbsize) )
	{
		if( $dbsize >= 1048576 )
		{
			$dbsize = sprintf("%.2f MB", ( $dbsize / 1048576 ));
		}
		else if( $dbsize >= 1024 )
		{
			$dbsize = sprintf("%.2f KB", ( $dbsize / 1024 ));
		}
		else
		{
			$dbsize = sprintf("%.2f Bytes", $dbsize);
		}
	}

	$template->assign_vars(array(
		"DNS_ADDRESS" => $dns_address,
		"ERROR_COUNT" => $error_count,
		"LAST_HEARD_FROM" => $last_heard_from,
		"SERVER_STATUS" => $server_status,
		"SERVER_STATUS_COLOR" => $server_status_color,
		"SC_SERVICES" => $sc_services,
		"L_SC_SERVICES_START" => append_sid("admin_process.$phpEx?perform=start"),
		"L_SC_SERVICES_STOP" => append_sid("admin_process.$phpEx?perform=stop"),
		"L_LOGSTATS" => append_sid("admin_process.$phpEx?perform=logstats"),
		"L_LOGROTATE" => append_sid("admin_process.$phpEx?perform=logrotate"),
		"SC_SERVICES_STATUS_COLOR" => $sc_services_color,
		"NOTIFICATION_COUNT" => $notification_count,
		"EVID" => $evid,
		"EVID_VERSION" => $evid_version,
		"DELIVERY_TIMESTAMP" => $delivery_timestamp,
		"DELIVERY_COUNT" => $delivery_count,
		"DELIVERY_STATUS_COLOR" => $delivery_status_color,
		"NUMBER_OF_POSTS" => $total_posts,
		"NUMBER_OF_TOPICS" => $total_topics,
		"NUMBER_OF_USERS" => $total_users,
		"START_DATE" => $start_date,
		"POSTS_PER_DAY" => $posts_per_day,
		"TOPICS_PER_DAY" => $topics_per_day,
		"USERS_PER_DAY" => $users_per_day,
		"AVATAR_DIR_SIZE" => $avatar_dir_size,
		"DB_SIZE" => $dbsize, 
		"GZIP_COMPRESSION" => ( $board_config['gzip_compress'] ) ? $lang['ON'] : $lang['OFF'])
	);
	//
	// End forum statistics
	//

	// Check for new version
	$current_version = explode('.', '2' . $board_config['version']);
	$minor_revision = (int) $current_version[2];

	$errno = 0;
	$errstr = $version_info = '';

	if (isSet($sc_conf_array['ROOT']['ProxyServer']))
	{
		// Define a context for HTTP.
		$proxy = preg_replace('`http://`i', 'tcp://', $sc_conf_array['ROOT']['ProxyServer']);
		$aContext = array(
		'http' => array(
			'proxy' => $proxy, // This needs to be the server and the port of the NTLM Authentication Proxy Server.
			'request_fulluri' => True,
			),
		);
		$cxContext = stream_context_get_default($aContext);

		// Now all file stream functions can use this context.

		$version_info = file_get_contents('http://earthquake.usgs.gov/resources/software/shakecast/20x.txt', False, $cxContext);
	}
	else
	{
		$version_info = file_get_contents('http://earthquake.usgs.gov/resources/software/shakecast/20x.txt');
	}
	
	if ($version_info)
	{
		$version_info = explode("\n", $version_info);
		$latest_head_revision = (int) $version_info[0];
		$latest_minor_revision = (int) $version_info[2];
		$latest_version = (int) $version_info[0] . '.' . (int) $version_info[1] . '.' . (int) $version_info[2];

		if ($latest_head_revision == 2 && $minor_revision == $latest_minor_revision)
		{
			$version_info = '<p style="color:green">' . $lang['Version_up_to_date'] . '</p>';
		}
		else if ($latest_head_revision == 2)
		{
			$version_info = '<p style="color:red">' . $lang['Version_not_up_to_date'];
			$version_info .= '<br />' . sprintf($lang['Latest_version_info'], $latest_version) . ' ' . sprintf($lang['Current_version_info'], '2' . $board_config['version']) . '</p>';
		}
		else 
		{
			$version_info = '<p style="color:red">' . $lang['No_latest_version_info'] . '</p>';
		}
	}
	else
	{
		if ($errstr)
		{
			$version_info = '<p style="color:red">' . sprintf($lang['Connect_socket_error'], $errstr) . '</p>';
		}
		else
		{
			$version_info = '<p>' . $lang['Socket_functions_disabled'] . '</p>';
		}
	}
	
	$version_info .= '<p>' . $lang['Mailing_list_subscribe_reminder'] . '</p>';
	

	$template->assign_vars(array(
		'VERSION_INFO'	=> $version_info,
		'L_VERSION_INFORMATION'	=> $lang['Version_information'])
	);

	$template->pparse("body");

	include('./page_footer_admin.'.$phpEx);

}
else
{
	//
	// Generate frameset
	//
	$template->set_filenames(array(
		"body" => "admin/index_frameset.tpl")
	);

	$template->assign_vars(array(
		"S_FRAME_NAV" => append_sid("index.$phpEx?pane=left"),
		"S_FRAME_MAIN" => append_sid("index.$phpEx?pane=right"))
	);

	header ("Expires: " . gmdate("D, d M Y H:i:s", time()) . " GMT");
	header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");

	$template->pparse("body");

	$db->sql_close();
	exit;

}

?>