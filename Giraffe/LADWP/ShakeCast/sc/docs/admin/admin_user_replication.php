<?php
/*
# $Id: admin_user_replication.php 521 2008-10-22 14:01:36Z klin $

# The PHP interface was originally inspired by PHPBB,
# file/database formats and directory structure are quite similar.

##############################################################################
# 
# Terms and Conditions of Software Use
# ====================================
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Disclaimer of Earthquake Information
# ====================================
# 
# The data and maps provided through this system are preliminary data
# and are subject to revision. They are computer generated and may not
# have received human review or official approval. Inaccuracies in the
# data may be present because of instrument or computer
# malfunctions. Subsequent review may result in significant revisions to
# the data. All efforts have been made to provide accurate information,
# but reliance on, or interpretation of earthquake data from a single
# source is not advised. Data users are cautioned to consider carefully
# the provisional nature of the information before using it for
# decisions that concern personal or public safety or the conduct of
# business that involves substantial monetary or operational
# consequences.
# 
# Disclaimer of Software and its Capabilities
# ===========================================
# 
# This software is provided as an "as is" basis.  Attempts have been
# made to rid the program of software defects and bugs, however the
# U.S. Geological Survey (USGS) have no obligations to provide maintenance, 
# support, updates, enhancements or modifications. In no event shall USGS 
# be liable to any party for direct, indirect, special, incidental or 
# consequential damages, including lost profits, arising out of the use 
# of this software, its documentation, or data obtained though the use 
# of this software, even if USGS or have been advised of the
# possibility of such damage. By downloading, installing or using this
# program, the user acknowledges and understands the purpose and
# limitations of this software.
# 
# Contact Information
# ===================
# 
# Coordination of this effort is under the auspices of the USGS Advanced
# National Seismic System (ANSS) coordinated in Golden, Colorado, which
# functions as the clearing house for development, distribution,
# documentation, and support. For questions, comments, or reports of
# potential bugs regarding this software please contact wald@usgs.gov or
# klin@usgs.gov.  
#
#############################################################################
*/

define('IN_SC', 1);

if( !empty($setmodules) )
{
	$filename = basename(__FILE__);
	$module['Users']['Replication'] = $filename;

	return;
}

$sc_root_path = './../';
require($sc_root_path . 'extension.inc');
require('./pagestart.' . $phpEx);

$html_entities_match = array('#<#', '#>#');
$html_entities_replace = array('&lt;', '&gt;');

//
// Set mode
//
if( isset( $HTTP_POST_VARS['mode'] ) || isset( $HTTP_GET_VARS['mode'] ) )
{
	$mode = ( isset( $HTTP_POST_VARS['mode']) ) ? $HTTP_POST_VARS['mode'] : $HTTP_GET_VARS['mode'];
	$mode = htmlspecialchars($mode);
}
else
{
	$mode = '';
}

if( isset( $HTTP_POST_VARS['submit'] ) || isset( $HTTP_GET_VARS['submit'] ) )
{
	$mode = 'copy';
}

if ( isset( $HTTP_POST_VARS['list_type'] ) || isset( $HTTP_GET_VARS['list_type'] ) )
{
	$list_type = ( isset( $HTTP_POST_VARS['list_type']) ) ? $HTTP_POST_VARS['list_type'] : $HTTP_GET_VARS['list_type'];
	$list_type = htmlspecialchars($list_type);
}
else 
{
	$list_type = 'user';
}

if( isset( $HTTP_POST_VARS['profile'] ) || isset( $HTTP_GET_VARS['profile'] ) )
{
	$list_type = 'profile';
}
else if( isset( $HTTP_POST_VARS['user'] ) || isset( $HTTP_GET_VARS['user'] ) )
{
	$list_type = 'user';
}

$admin_userdata = get_userdata($userdata['session_user_id']);
if( !$admin_userdata )
{
	message_die(GENERAL_MESSAGE, $lang['No_user_id_specified'] );
}

//
// Begin program
//
// Restrict mode input to valid options
$mode = ( in_array($mode, array('add', 'edit', 'save', 'delete', 'copy')) ) ? $mode : '';

if( $mode == 'copy' )
{
	@set_time_limit(1200);
	$confirm = isset($HTTP_POST_VARS['confirm']);
	if ( isset( $HTTP_POST_VARS['list_type'] ) || isset( $HTTP_GET_VARS['list_type'] ) )
	{
		$list_type = ( isset( $HTTP_POST_VARS['list_type']) ) ? $HTTP_POST_VARS['list_type'] : $HTTP_GET_VARS['list_type'];
		$list_type = htmlspecialchars($list_type);
	}

	if ( isset( $HTTP_POST_VARS['from_list'] ) )
	{
		$from_id = intval( $HTTP_POST_VARS['from_list'] );
	}

	$to_list = array();
	if ( isset( $HTTP_POST_VARS['to_list'] ) )
	{
		$to_list = $HTTP_POST_VARS['to_list'];
	}
	$to_count = count($to_list);

	if( $from_id > 0 && $to_count > 0 && $confirm )
	{
		if ($list_type == 'profile')
		{
			foreach ($to_list as $user_id)
			{
				$sql = "SELECT count(gup.profile_id) as profile_cnt
					FROM " . PROFILE_NOTIFICATION_TABLE . " pnr 
					INNER JOIN " . GEOMETRY_USER_PROFILE_TABLE . " gup 
						ON pnr.profile_id = gup.profile_id
					WHERE gup.shakecast_user = '$user_id'";
				if( !($result = $db->sql_query($sql)) )
				{
					message_die(GENERAL_ERROR, 'Could not obtain user profile information for this user', '', __LINE__, __FILE__, $sql);
				}
		
				$row = $db->sql_fetchrow($result);
				$profile_cnt = $row['profile_cnt'];

				$sql = "SELECT count(notification_request_id) as notification_cnt
					FROM " . NOTIFICATION_REQUEST_TABLE . "   
					WHERE shakecast_user = '$user_id'";
				if( !($result = $db->sql_query($sql)) )
				{
					message_die(GENERAL_ERROR, 'Could not obtain user notification request information for this user', '', __LINE__, __FILE__, $sql);
				}
		
				$row = $db->sql_fetchrow($result);
				$notification_cnt = $row['notification_cnt'];

				if ($profile_cnt != $notification_cnt) 
				{
					$sql = "DELETE FROM " . GEOMETRY_USER_PROFILE_TABLE . "   
						WHERE shakecast_user = '$user_id'";
					if( !($result = $db->sql_query($sql)) )
					{
						message_die(GENERAL_ERROR, 'Could not obtain delete user information from geometry user profile', '', __LINE__, __FILE__, $sql);
					}
				} else {
					$sql = "INSERT INTO ". GEOMETRY_USER_PROFILE_TABLE ." (shakecast_user, profile_id)
							VALUES ($user_id, $from_id)";
					if( !($result = $db->sql_query($sql)) )
					{
						message_die(GENERAL_ERROR, 'Could not insert for this user', '', __LINE__, __FILE__, $sql);
					}
				}
		
				$sql = "SELECT profile_id, damage_level, notification_type, event_type, delivery_method,
						   message_format, limit_value, user_message, notification_priority, auxiliary_script,
						   disabled, product_type, metric, aggregate, aggregation_group
					FROM " . PROFILE_NOTIFICATION_TABLE . "   
					WHERE profile_id = '$from_id'";
				if( !($result = $db->sql_query($sql)) )
				{
					message_die(GENERAL_ERROR, 'Could not obtain group information for this user', '', __LINE__, __FILE__, $sql);
				}
		
				$ids = $db->sql_fetchrowset($result);
				for( $i = 0; $i < count($ids); $i++ )
				{
					$limit_value = ($ids[$i]['limit_value'] != '') ? $ids[$i]['limit_value'] : "NULL";
					$aggregate = ($ids[$i]['aggregate'] != '') ? $ids[$i]['aggregate'] : "NULL";
					$disabled = ($ids[$i]['disabled'] != '') ? $ids[$i]['disabled'] : "NULL";
					$notification_priority = ($ids[$i]['notification_priority'] != '') ? $ids[$i]['notification_priority'] : "NULL";
					$sql = "INSERT INTO ". NOTIFICATION_REQUEST_TABLE . " ( damage_level, notification_type,
								event_type, delivery_method, message_format, limit_value, user_message, notification_priority,
								auxiliary_script, disabled, product_type, metric, aggregate, aggregation_group, shakecast_user,
								update_username, update_timestamp )
							VALUES ('". $ids[$i]['damage_level'] ."', '". $ids[$i]['notification_type'] ."', 
								'". $ids[$i]['event_type'] ."', '". $ids[$i]['delivery_method'] ."', '". $ids[$i]['message_format'] . "', 
								". $limit_value .", '". $ids[$i]['user_message'] ."', ". $notification_priority .", 
								'". $ids[$i]['auxiliary_script'] ."', ". $disabled .", '". $ids[$i]['product_type'] ."', 
								'". $ids[$i]['metric'] ."', ". $aggregate .", '". $ids[$i]['aggregation_group'] ."', 
								$user_id, '".$admin_userdata['username']."', now())";
					if( !($result = $db->sql_query($sql)) )
					{
						message_die(GENERAL_ERROR, 'Could not insert for this user', '', __LINE__, __FILE__, $sql);
					}
		
					$notification_request_id = $db->sql_nextid();
	
					if ($ids[$i]['notification_type'] == 'SHAKING' || $ids[$i]['notification_type'] == 'DAMAGE') {
						$sql = 'INSERT INTO '. FACILITY_NOTIFICATION_REQUEST_TABLE . ' ( notification_request_id, facility_id)
								SELECT '. $notification_request_id .', facility_id
									FROM '. GEOMETRY_FACILITY_PROFILE_TABLE .'
									WHERE profile_id = '. $from_id;
						if( !($result = $db->sql_query($sql)) )
						{
							message_die(GENERAL_ERROR, 'Could not insert for this user', '', __LINE__, __FILE__, $sql);
						}
					}
		
				}
			}
		}
		else if ($list_type == 'user')
		{
			foreach ($to_list as $user_id)
			{
				$sql = "SELECT notification_request_id, damage_level, notification_type, event_type, delivery_method,
						   message_format, limit_value, user_message, notification_priority, auxiliary_script,
						   disabled, product_type, metric, aggregate, aggregation_group
					FROM " . NOTIFICATION_REQUEST_TABLE . "   
					WHERE shakecast_user = '$from_id'";
				if( !($result = $db->sql_query($sql)) )
				{
					message_die(GENERAL_ERROR, 'Could not obtain group information for this user', '', __LINE__, __FILE__, $sql);
				}
		
				$ids = $db->sql_fetchrowset($result);
				for( $i = 0; $i < count($ids); $i++ )
				{
					$from_notification_request_id = $ids[$i]['notification_request_id'];
					$limit_value = ($ids[$i]['limit_value'] != '') ? $ids[$i]['limit_value'] : "NULL";
					$aggregate = ($ids[$i]['aggregate'] != '') ? $ids[$i]['aggregate'] : "NULL";
					$disabled = ($ids[$i]['disabled'] != '') ? $ids[$i]['disabled'] : "NULL";
					$notification_priority = ($ids[$i]['notification_priority'] != '') ? $ids[$i]['notification_priority'] : "NULL";
					$sql = "INSERT INTO ". NOTIFICATION_REQUEST_TABLE . " ( damage_level, notification_type,
								event_type, delivery_method, message_format, limit_value, user_message, notification_priority,
								auxiliary_script, disabled, product_type, metric, aggregate, aggregation_group, shakecast_user,
								update_username, update_timestamp )
							VALUES ('". $ids[$i]['damage_level'] ."', '". $ids[$i]['notification_type'] ."', 
								'". $ids[$i]['event_type'] ."', '". $ids[$i]['delivery_method'] ."', '". $ids[$i]['message_format'] . "', 
								". $limit_value .", '". $ids[$i]['user_message'] ."', ". $notification_priority .", 
								'". $ids[$i]['auxiliary_script'] ."', ". $disabled .", '". $ids[$i]['product_type'] ."', 
								'". $ids[$i]['metric'] ."', ". $aggregate .", '". $ids[$i]['aggregation_group'] ."', 
								$user_id, '".$admin_userdata['username']."', now())";
					if( !($result = $db->sql_query($sql)) )
					{
						message_die(GENERAL_ERROR, 'Could not insert for this user', '', __LINE__, __FILE__, $sql);
					}
		
					$notification_request_id = $db->sql_nextid();
	
					if ($ids[$i]['notification_type'] == 'SHAKING' || $ids[$i]['notification_type'] == 'DAMAGE') {
						$sql = 'INSERT INTO '. FACILITY_NOTIFICATION_REQUEST_TABLE . ' ( notification_request_id, facility_id)
								SELECT '. $notification_request_id .', facility_id
									FROM '. FACILITY_NOTIFICATION_REQUEST_TABLE .'
									WHERE notification_request_id = '. $from_notification_request_id;
						if( !($result = $db->sql_query($sql)) )
						{
							message_die(GENERAL_ERROR, 'Could not insert for this user', '', __LINE__, __FILE__, $sql);
						}
					}
		
				}
				$sql = "DELETE FROM " . GEOMETRY_USER_PROFILE_TABLE . "   
					WHERE shakecast_user = '$user_id'";
				if( !($result = $db->sql_query($sql)) )
				{
					message_die(GENERAL_ERROR, 'Could not obtain delete user information from geometry user profile', '', __LINE__, __FILE__, $sql);
				}
			}
		}

		$message = $lang['user_replicated'] . "<br /><br />" . sprintf($lang['Click_return_userrepadmin'], "<a href=\"" . append_sid("admin_user_replication.$phpEx") . "\">", "</a>") . "<br /><br />" . sprintf($lang['Click_return_admin_index'], "<a href=\"" . append_sid("index.$phpEx?pane=right") . "\">", "</a>");

		message_die(GENERAL_MESSAGE, $message);
	}
	elseif( $from_id > 0 && $to_count > 0 && !$confirm)
	{
		// Present the confirmation screen to the user
		$template->set_filenames(array(
			'body' => 'admin/confirm_body.tpl')
		);

		$hidden_fields = '<input type="hidden" name="mode" value="'.$mode.'" /><input type="hidden" name="username" value="' . $username . '" />';
		$hidden_fields .= '<input type="hidden" name="list_type" value="'.$list_type.'" />';
		$hidden_fields .= '<input type="hidden" name="from_list" value="'.$from_id.'" />';
		foreach ($to_list as $copy_to) 
		{
			if ($from_id == $copy_to && $list_type == 'user') 
			{
				message_die(GENERAL_MESSAGE, $lang['Same_user_error']);
			}
			else if ($copy_to <= 0) 
			{
				message_die(GENERAL_MESSAGE, $lang['non_user_selected']);
			}
			$hidden_fields .= '<input type="hidden" name="to_list[]" value="'.$copy_to.'" />';
		}
		
		$template->assign_vars(array(
			'MESSAGE_TITLE' => $lang['Confirm'],
			'MESSAGE_TEXT' => $lang['Confirm_replicate'],

			'L_YES' => $lang['Yes'],
			'L_NO' => $lang['No'],

			'S_CONFIRM_ACTION' => append_sid("admin_user_replication.$phpEx"),
			'S_HIDDEN_FIELDS' => $hidden_fields)
		);
	}
	else
	{
		message_die(GENERAL_MESSAGE, $lang['no_user_selected']);
	}
}
else
{
	$s_hidden_fields = '<input type="hidden" name="list_type" value="'.$list_type.'" />';
	if( $list_type == 'user' )
	{
		$sql = "SELECT shakecast_user as id, full_name as name
			FROM ". SC_USER_TABLE;
		$from_text = $lang['Select_a_User'];
	} 
	else
	{
		$sql = "SELECT profile_id as id, profile_name as name
			FROM ". GEOMETRY_PROFILE_TABLE;
		$from_text = $lang['Select_a_Profile'];
	}
	
	if ( !($result = $db->sql_query($sql)) )
	{
		message_die(GENERAL_ERROR, "Couldn't obtain ShakeCast user/profile list.", "", __LINE__, __FILE__, $sql);
	}
		
	$rows = array();
	while ( $row = $db->sql_fetchrow($result) )
	{
		$rows[] = $row;
	}
	sort($rows);
	if ( $total_rows = count($rows) )
	{
		$boxstring = '<select name="from_list"><option value="-1">' . $from_text . '</option>';

		for($i = 0; $i < $total_rows; $i++)
		{
			$boxstring .=  '<option value="' . $rows[$i]['id'] . '">' .  $rows[$i]['id'] .': '. $rows[$i]['name'] . '</option>';
		}

		$boxstring .= '</select>';
	}
	else
	{
		$boxstring .= '<select name="from_list" ></select>';
	}

	$boxstring .= '<input type="hidden" name="sid" value="' . $userdata['session_id'] . '" />';
	$boxstring .= $param;
	
	$sql = "SELECT shakecast_user as id, full_name as name
		FROM ". SC_USER_TABLE;
	if ( !($result = $db->sql_query($sql)) )
	{
		message_die(GENERAL_ERROR, "Couldn't obtain ShakeCast user/profile list.", "", __LINE__, __FILE__, $sql);
	}
		
	$rows = array();
	while ( $row = $db->sql_fetchrow($result) )
	{
		$rows[] = $row;
	}
	sort($rows);
	if ( $total_rows = count($rows) )
	{
		$userstring = '<select name="to_list[]" multiple><option value="-1">' . $lang['Select_Users'] . '</option>';

		for($i = 0; $i < $total_rows; $i++)
		{
			$userstring .=  '<option value="' . $rows[$i]['id'] . '">' .  $rows[$i]['id'] .': '. $rows[$i]['name'] . '</option>';
		}

		$userstring .= '</select>';
	}
	else
	{
		$userstring .= '<select name="to_list" ></select>';
	}

	$userstring .= $param;
	
	//
	// Default user selection box
	//
	$template->set_filenames(array(
		'body' => 'admin/user_replication_body.tpl')
	);

	$template->assign_vars(array(
		'L_USER_TITLE' => $lang['User_replication_admin'],
		'L_USER_EXPLAIN' => $lang['User_replication_admin_explain'],
		'L_USER_EXPLAIN' => $lang['User_replication_admin_explain'],
		'L_SOURCE_USER' => $lang['User_source'],
		'L_DESTINATION_USER' => $lang['User_destination'],
		'L_USER_SELECT' => $lang['Select_a_User'],
		'L_LOOK_UP' => $lang['Look_up_user'],
		'L_FIND_USERNAME' => $lang['Find_username'],

		'U_SEARCH_USER' => append_sid("./../search.$phpEx?mode=searchuser"), 

		'S_USER_ACTION' => append_sid("admin_users.$phpEx"),
		'S_USER_SELECT' => $select_list)
	);

	$user_count = get_db_stat('usercount');
	if ( !$user_count )
	{
		//
		// No group members
		//
		$template->assign_block_vars('switch_no_members', array());
		$template->assign_vars(array(
			'L_NO_MEMBERS' => $lang['No_group_members'])
		);
	}

	$template->assign_vars(array(
		"L_USERNAME" => "Username",
		"L_FULL_NAME" => "Full Name",
		"L_EMAIL" => "Email Address",
		"L_USER_STATUS" => "No. Request",
		"L_OCCUPATION" => "No. Profile",
		"L_ORGANIZATION" => "No. Facility",
		"L_USER_LEVEL" => $lang['User_Level'],
		"L_EDIT" => "Edit",
		"L_DELETE" => "Delete",
		"L_ADD_EVENT" => $lang['Add_new_user'],
		"L_ACTION" => $lang['Action'],
		'S_JUMPBOX_SELECT' => $boxstring,
		'S_USERBOX_SELECT' => $userstring,

			"L_USER" => $lang['User_list'],
			"L_PROFILE" => $lang['Profile_list'],
			"U_USER" => 'user',
			"U_PROFILE" => 'profile',
			"STATUS_PROFILE" => ($list_type == 'profile') ? 'DISABLED' : '',
			"STATUS_USER" => ($list_type == 'user') ? 'DISABLED' : '',

			'L_SUBMIT' => $lang['Replicate'],
			'L_RESET' => $lang['Reset'],

		"S_WORDS_ACTION" => append_sid("admin_user_replication.$phpEx"),
		"S_HIDDEN_FIELDS" => $s_hidden_fields)
	);


}

$template->pparse('body');
include('./page_footer_admin.'.$phpEx);

?>