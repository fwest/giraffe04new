<?php
/*
# $Id: admin_user_facility.php 167 2007-10-31 17:26:25Z klin $

# The PHP interface was originally inspired by PHPBB,
# file/database formats and directory structure are quite similar.

##############################################################################
# 
# Terms and Conditions of Software Use
# ====================================
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Disclaimer of Earthquake Information
# ====================================
# 
# The data and maps provided through this system are preliminary data
# and are subject to revision. They are computer generated and may not
# have received human review or official approval. Inaccuracies in the
# data may be present because of instrument or computer
# malfunctions. Subsequent review may result in significant revisions to
# the data. All efforts have been made to provide accurate information,
# but reliance on, or interpretation of earthquake data from a single
# source is not advised. Data users are cautioned to consider carefully
# the provisional nature of the information before using it for
# decisions that concern personal or public safety or the conduct of
# business that involves substantial monetary or operational
# consequences.
# 
# Disclaimer of Software and its Capabilities
# ===========================================
# 
# This software is provided as an "as is" basis.  Attempts have been
# made to rid the program of software defects and bugs, however the
# U.S. Geological Survey (USGS) have no obligations to provide maintenance, 
# support, updates, enhancements or modifications. In no event shall USGS 
# be liable to any party for direct, indirect, special, incidental or 
# consequential damages, including lost profits, arising out of the use 
# of this software, its documentation, or data obtained though the use 
# of this software, even if USGS or have been advised of the
# possibility of such damage. By downloading, installing or using this
# program, the user acknowledges and understands the purpose and
# limitations of this software.
# 
# Contact Information
# ===================
# 
# Coordination of this effort is under the auspices of the USGS Advanced
# National Seismic System (ANSS) coordinated in Golden, Colorado, which
# functions as the clearing house for development, distribution,
# documentation, and support. For questions, comments, or reports of
# potential bugs regarding this software please contact wald@usgs.gov or
# klin@usgs.gov.  
#
#############################################################################
*/

if( !empty($setmodules) )
{
	//$file = basename(__FILE__);
	//$module['Users']['Facility Management'] = $file;
	return;
}

define('IN_SC', 1);

//
// Load default header
//
$manage_profile = '/shakecast/sc/bin/manage_profile.pl';
$sc_root_path = "./../";
require($sc_root_path . 'extension.inc');

$cancel = ( isset($HTTP_POST_VARS['cancel']) ) ? true : false;
$no_page_header = $cancel;
//$template_file = 'page_gm_header.tpl';

require('./pagestart.' . $phpEx);

if ($cancel)
{
	redirect('admin/' . append_sid("admin_user_facility.$phpEx", true));
}

//
// Set mode
//
if( isset( $HTTP_POST_VARS['mode'] ) || isset( $HTTP_GET_VARS['mode'] ) )
{
	$mode = ( isset( $HTTP_POST_VARS['mode']) ) ? $HTTP_POST_VARS['mode'] : $HTTP_GET_VARS['mode'];
	$mode = htmlspecialchars($mode);
}
else
{
	$mode = '';
}

if( isset( $HTTP_POST_VARS['username'] ) || isset( $HTTP_GET_VARS['username'] ) )
{
	$username = ( isset( $HTTP_POST_VARS['username']) ) ? $HTTP_POST_VARS['username'] : $HTTP_GET_VARS['username'];
	$username = htmlspecialchars($username);
}
else
{
	$username = '';
}

if( $username != '' )
{
	$this_userdata = get_userdata($username, true);
	if( !$this_userdata )
	{
		message_die(GENERAL_MESSAGE, $lang['No_user_id_specified'] );
	}

	//
	// Now parse and display it as a template
	//
	$user_id = $this_userdata['user_id'];
	$username = $this_userdata['username'];
	$email = $this_userdata['user_email'];
	$fullname = htmlspecialchars($this_userdata['user_fullname']);
}
	
if( isset( $HTTP_POST_VARS['nrtype'] ) || isset( $HTTP_GET_VARS['nrtype'] ) )
{
	$nrtype = ( isset( $HTTP_POST_VARS['nrtype']) ) ? $HTTP_POST_VARS['nrtype'] : $HTTP_GET_VARS['nrtype'];
	$nrtype = htmlspecialchars($nrtype);
}
else
{
	$nrtype = '';
}

if( isset( $HTTP_POST_VARS['nkey'] ) || isset( $HTTP_GET_VARS['nkey'] ) )
{
	$nkey = ( isset( $HTTP_POST_VARS['nkey']) ) ? $HTTP_POST_VARS['nkey'] : $HTTP_GET_VARS['nkey'];
}
else
{
	$nkey = '';
}

if( isset( $HTTP_POST_VARS['full'] ) || isset( $HTTP_GET_VARS['full'] ) )
{
	$list_type = 'full';
}
else if( isset( $HTTP_POST_VARS['subscribed'] ) || isset( $HTTP_GET_VARS['subscribed'] ) )
{
	$list_type = 'subscribed';
}
else if ( isset( $HTTP_POST_VARS['list_type'] ) || isset( $HTTP_GET_VARS['list_type'] ) )
{
	$list_type = ( isset( $HTTP_POST_VARS['list_type']) ) ? $HTTP_POST_VARS['list_type'] : $HTTP_GET_VARS['list_type'];
	$list_type = htmlspecialchars($list_type);
}
else
{
	$list_type = 'subscribed';
}

//
// Begin program
//
// Restrict mode input to valid options
$mode = ( in_array($mode, array('add', 'edit', 'save', 'delete')) ) ? $mode : '';

if( $mode != "" )
{
	if( $mode == "edit" || $mode == "update" )
	{
		$template->set_filenames(array(
			"body" => "admin/profile_facility_edit_body.tpl")
		);


		$s_hidden_fields = '<input type="hidden" name="mode" value="edit" /><input type="hidden" name="username" value="'.$username.'" /><input type="hidden" name="nkey" value="'.$nkey.'" />';
		if ( isset( $HTTP_POST_VARS['subscribed'] ) )
		{
			$s_hidden_fields .= '<input type="hidden" name="list_type" value="'.$list_type.'" />';
		}
		else if ( isset( $HTTP_POST_VARS['full'] ) )
		{
			$s_hidden_fields .= '<input type="hidden" name="list_type" value="'.$list_type.'" />';
		}

		if( isset( $HTTP_POST_VARS['update'] ) || isset( $HTTP_GET_VARS['update'] ) )
		{
			if( $nkey )
			{
				$facility_total = $HTTP_POST_VARS['facility_total'];
				$facility_list = $HTTP_POST_VARS['facility'];
				foreach ($facility_total as $facility) {
					$sql = "DELETE FROM ". FACILITY_NOTIFICATION_REQUEST_TABLE ."
							WHERE notification_request_id = $nkey AND facility_id = $facility";
					$result = $db->sql_query($sql);
				}
				foreach ($facility_list as $facility) {
					$sql = "INSERT INTO ". FACILITY_NOTIFICATION_REQUEST_TABLE ." (notification_request_id, facility_id)
							VALUES ( $nkey, $facility )";
					$result = $db->sql_query($sql);
					if( !$result )
					{
						message_die(GENERAL_ERROR, "Couldn't insert facility notification request.", "", __LINE__, __FILE__, $sql );
					}
				}
			}
			else
			{
				message_die(GENERAL_MESSAGE, "No profile selected for editing ");
			}
		}
		
		if( (isset($HTTP_POST_VARS['start']) ||  isset($HTTP_GET_VARS['start'])) 
			&& !(isset($HTTP_POST_VARS['full']) || isset($HTTP_POST_VARS['subscribed']))  )
		{
			$start = ( isset($HTTP_POST_VARS['start']) ) ? $HTTP_POST_VARS['start'] : $HTTP_GET_VARS['start'];
		}

		if( $list_type == 'subscribed' )
		{
			$sql = "SELECT count(fnr.facility_id) as total
					FROM ((" . NOTIFICATION_REQUEST_TABLE ." nr 
						INNER JOIN ". FACILITY_NOTIFICATION_REQUEST_TABLE ." fnr on fnr.notification_request_id = nr.notification_request_id)
						INNER JOIN ". FACILITY_TABLE ." f on f.facility_id = fnr.facility_id) 
					WHERE nr.notification_request_id = " . $nkey;
		}
		else 
		{
			$sql = "SELECT count(facility_id) as total
					FROM " . FACILITY_TABLE ." f";
		}
		if(!$result = $db->sql_query($sql))
		{
			message_die(GENERAL_ERROR, "Could not find facility notification requestion settings", $lang['Error'], __LINE__, __FILE__, $sql);
		}
		$row = $db->sql_fetchrow($result);
		$facility_count = $row['total'];

		$current_page = ( !$facility_count ) ? 1 : ceil( $facility_count / $board_config['topics_per_page'] );
		$template->assign_vars(array(
			'PAGINATION' => generate_pagination("admin_user_facility.$phpEx?mode=edit&username=$username&nkey=$nkey&list_type=$list_type", $facility_count, $board_config['topics_per_page'], $start),
			'PAGE_NUMBER' => sprintf($lang['Page_of'], ( floor( $start / $board_config['topics_per_page'] ) + 1 ), $current_page ), 
	
			'L_GOTO_PAGE' => $lang['Goto_page'])
		);

		if( $list_type == 'subscribed' )
		{
			$sql = "SELECT f.facility_id, f.lat_min, f.lon_min, f.facility_name, f.facility_type, fnr.notification_request_id
					FROM ((" . NOTIFICATION_REQUEST_TABLE ." nr 
						INNER JOIN ". FACILITY_NOTIFICATION_REQUEST_TABLE ." fnr on fnr.notification_request_id = nr.notification_request_id)
						INNER JOIN ". FACILITY_TABLE ." f on f.facility_id = fnr.facility_id) 
					WHERE nr.notification_request_id = " . $nkey ."
					ORDER BY facility_id";
		}
		else 
		{
			$sql = "SELECT f.facility_id, f.lat_min, f.lon_min, f.facility_name, f.facility_type
					FROM " . FACILITY_TABLE ." f
					ORDER BY facility_id";
/*			$sql = "SELECT f.facility_id, f.lat_min, f.lon_min, f.facility_name, f.facility_type, fnr.notification_request_id
					FROM (" . FACILITY_TABLE ." f
						 INNER JOIN ". FACILITY_NOTIFICATION_REQUEST_TABLE ." fnr on fnr.facility_id = f.facility_id)
					WHERE fnr.notification_request_id = " . $nkey ." 
					UNION 
					SELECT f.facility_id, f.lat_min, f.lon_min, f.facility_name, f.facility_type, 0
					FROM " . FACILITY_TABLE ." f 
					WHERE f.facility_id NOT IN (
					SELECT f.facility_id
					FROM (" . FACILITY_TABLE ." f
						 INNER JOIN ". FACILITY_NOTIFICATION_REQUEST_TABLE ." fnr on fnr.facility_id = f.facility_id)
					WHERE fnr.notification_request_id = " . $nkey .")
					ORDER BY facility_id";
*/
		}
		if ($board_config['topics_per_page']) { $sql .= ' LIMIT '.$board_config['topics_per_page'];}
		if ($start) { $sql .= " OFFSET $start";}
		if(!$result = $db->sql_query($sql))
		{
			message_die(GENERAL_ERROR, "Could not find facility notification requestion settings", $lang['Error'], __LINE__, __FILE__, $sql);
		}
		$rows = $db->sql_fetchrowset($result);

		for( $i = 0; $i < count($rows); $i++ )
		{
			if( $list_type == 'subscribed' )
			{
				$notification_request_id = $rows[$i]['notification_request_id'];
			}
			else 
			{
				$sql = "SELECT notification_request_id
					FROM " . FACILITY_NOTIFICATION_REQUEST_TABLE ." 
						WHERE notification_request_id = " . $nkey ." 
							AND facility_id = ". $rows[$i]['facility_id'];
				if(!$result = $db->sql_query($sql))
				{
					message_die(GENERAL_ERROR, "Could not find facility notification requestion settings", $lang['Error'], __LINE__, __FILE__, $sql);
				}
				$row = $db->sql_fetchrow($result);
				$notification_request_id = $row['notification_request_id'];
			}
			$u_edit = '<input type=hidden name="facility_total[]" value="'.$rows[$i]['facility_id'].'"><input type=checkbox name="facility[]" value="'.$rows[$i]['facility_id'].'"';
			if ($notification_request_id == $nkey) { $u_edit .= ' checked';}
			$u_edit .= '>';
			$row_color = ( !($i % 2) ) ? $theme['td_color1'] : $theme['td_color2'];
			$row_class = ( !($i % 2) ) ? $theme['td_class1'] : $theme['td_class2'];
	
			$template->assign_block_vars('words', array(
				"ROW_COLOR" => "#" . $row_color,
				"ROW_CLASS" => $row_class,
				"FAC_ID" => $rows[$i]['facility_id'],
				"FAC_NAME" => $rows[$i]['facility_name'],
				"FAC_TYPE" => $rows[$i]['facility_type'],
				"LAT_MIN" => $rows[$i]['lat_min'],
				"LON_MIN" => $rows[$i]['lon_min'],
				//"POLY" => ($fields['poly'] || $update) ? '' : '*',
	
				"CHECKED" => ($rows[$i]['poly'] || $update) ? '' : 'checked',
				//"U_EDIT" => append_sid("admin_profile_facility.$phpEx?mode=edit&amp;id=$fac_id"),
				"U_EDIT" => $u_edit)
			);
		}
		
		$template->assign_vars(array(
				'L_WORDS_TITLE' => $lang['User_notification_admin'],
				'L_WORDS_TEXT' => $lang['User_notification_admin_explain'],
				'L_USER_NOTIFICATION' => $lang['User_facility_notification_explain'],
				'USER_NAME' => $fullname . ' ('. $username . '), ID: '.$nkey ,

			"L_FAC_ID" => "ID",
			"L_FAC_TYPE" => "Type",
			"L_FAC_NAME" => "Facility Name",
			"L_LATITUDE" => "Latitude",
			"L_LONGITUDE" => "Longitude",
			"L_DESCRIPTION" => "Description",
			//"L_EDIT" => "Remove",
			"L_DELETE" => "Delete",
			"L_ADD_NEXT" => $lang['Update_user_facility'],
			"L_ACTION" => $lang['Select'],
			"L_SUB" => $lang['Full_facility'].'Subscribed List',
			"L_FULL" => $lang['Full_facility'].'Full List',

			"CHECKED" => ($list_type == 'subscribed') ? 'checked' : '',
			"U_SUB" => 'subscribed',
			"U_FULL" => 'full',
			"STATUS_FULL" => ($list_type == 'full') ? 'DISABLED' : '',
			"STATUS_SUB" => ($list_type == 'subscribed') ? 'DISABLED' : '',
		
			"S_WORDS_ACTION" => append_sid("admin_user_facility.$phpEx?mode=edit&username=$username&nkey=$nkey&start=$start&list_type=$list_type"),
			"S_ADD_ACTION" => append_sid("admin_user_facility.$phpEx?mode=add&id=$profile_id"),
			"S_HIDDEN_FIELDS" => $s_hidden_fields)
		);

		$template->pparse("body");

		include('./page_footer_admin.'.$phpEx);
	}
	else if( $mode == "add" || $mode == "add_update")
	{
		$profile_id = ( isset($HTTP_GET_VARS['id']) ) ? intval($HTTP_GET_VARS['id']) : '';

		$template->set_filenames(array(
			"body" => "admin/profile_facility_edit_body.tpl")
		);


		$profile = array('profile_id' => '', 'profile_name' => '', 'description' => '', 'geom' => '');
		$s_hidden_fields = '';

		if( $mode == "add_update" )
		{
			if( $profile_id )
			{
				$hidden_fields .= '<input type="hidden" name="profile_id" id="profile_id" value="'.$profile_id.'" />';
				$facility_list = $HTTP_POST_VARS['facility'];
				foreach ($facility_list as $facility) {
					$sql = "INSERT INTO geometry_facility_profile (profile_id, facility_id)
							VALUES ($profile_id, $facility)";
					$result = $db->sql_query($sql);
					if( !$result )
					{
						message_die(GENERAL_ERROR, "Couldn't insert facility into geometry profile facility list.", "", __LINE__, __FILE__, $sql );
					}
				}
				$sql = "UPDATE geometry_profile
						SET updated = 1
						WHERE profile_id = $profile_id";
				$result = $db->sql_query($sql);
				if( !$result )
				{
					message_die(GENERAL_ERROR, "Couldn't update geometry profile.", "", __LINE__, __FILE__, $sql );
				}
			}
			else
			{
				message_die(GENERAL_MESSAGE, "No profile selected for editing ");
			}
		}
		
		$sql = "SELECT count(facility_id) as total FROM facility";
		$result = $db->sql_query($sql);
		if( !$result )
		{
			message_die(GENERAL_ERROR, "Couldn't get facility information.", "", __LINE__, __FILE__, $sql );
		}
		$row = $db->sql_fetchrow($result);
		$facility_count = $row['total'];
		if ( !$facility_count )
		{
			$template->assign_block_vars('switch_no_members', array());
			$template->assign_vars(array(
				'L_NO_MEMBERS' => $lang['No_group_members'])
			);
		}

		$sql = "SELECT f.facility_id, f.lat_min, f.lon_min, f.facility_name, f.facility_type
				FROM geometry_facility_profile gfp INNER JOIN facility f on gfp.facility_id = f.facility_id
				WHERE profile_id = $profile_id";
		if(!$result = $db->sql_query($sql))
		{
			message_die(GENERAL_ERROR, "Could not find damage level settings", $lang['Error']);
		}
		$rows = $db->sql_fetchrowset($result);
		for( $i = 0; $i < count($rows); $i++ )
		{
			if (!$poly_facility[$rows[$i]['facility_id']]) {
				$poly_facility[$rows[$i]['facility_id']] = $rows[$i];
			}
		}

		if( isset($HTTP_POST_VARS['start']) ||  isset($HTTP_GET_VARS['start']) )
		{
			$start = ( isset($HTTP_POST_VARS['start']) ) ? $HTTP_POST_VARS['start'] : $HTTP_GET_VARS['start'];
		}
		$current_page = ( !$facility_count ) ? 1 : ceil( $facility_count / $board_config['topics_per_page'] );
		$template->assign_vars(array(
			'PAGINATION' => generate_pagination("admin_user_facility.$phpEx?mode=add&id=$profile_id", $facility_count, $board_config['topics_per_page'], $start),
			'PAGE_NUMBER' => sprintf($lang['Page_of'], ( floor( $start / $board_config['topics_per_page'] ) + 1 ), $current_page ), 
	
			'L_GOTO_PAGE' => $lang['Goto_page'])
		);

		$sql = "SELECT facility_id, lat_min, lon_min, facility_name, facility_type
				FROM facility";
		if ($board_config['topics_per_page']) {$sql .= " LIMIT ". $board_config['topics_per_page'];}
		if ($start) {$sql .= " OFFSET $start";}
		if(!$result = $db->sql_query($sql))
		{
			message_die(GENERAL_ERROR, "Could not find test level settings", $lang['Error']);
		}
		$rows = $db->sql_fetchrowset($result);
		for( $i = 0; $i < count($rows); $i++ )
		{
			$u_edit = '<input type=checkbox name="facility[]" value="'.$rows[$i]['facility_id'].'">';
			$row_color = ( !($i % 2) ) ? $theme['td_color1'] : $theme['td_color2'];
			$row_class = ( !($i % 2) ) ? $theme['td_class1'] : $theme['td_class2'];
			
			$template->assign_block_vars('words', array(
				"ROW_COLOR" => "#" . $row_color,
				"ROW_CLASS" => $row_class,
				"FAC_ID" => $rows[$i]['facility_id'],
				"FAC_NAME" => $rows[$i]['facility_name'],
				"FAC_TYPE" => $rows[$i]['facility_type'],
				"LAT_MIN" => $rows[$i]['lat_min'],
				"LON_MIN" => $rows[$i]['lon_min'],
				"POLY" => ($poly_facility[$rows[$i]['facility_id']]) ? 'ed' : '',
	
//				"CHECKED" => ($fields['poly'] || $update) ? '' : 'checked',
				//"U_EDIT" => append_sid("admin_profile_facility.$phpEx?mode=edit&amp;id=$fac_id"),
				"U_EDIT" => ($poly_facility[$rows[$i]['facility_id']]) ? '' : $u_edit
				)
			);
		}
		
		$template->assign_vars(array(
			"L_WORDS_TITLE" => $lang['profile_facility_title'],
			"L_WORDS_TEXT" => $lang['profile_facility_explain'],
			"L_FAC_ID" => "ID",
			"L_FAC_TYPE" => "Type",
			"L_FAC_NAME" => "Facility Name",
			"L_LATITUDE" => "Latitude",
			"L_LONGITUDE" => "Longitude",
			"L_DESCRIPTION" => "Description",
			"L_EDIT" => "Add",
			"L_DELETE" => "Delete",
			"L_ADD_NEXT" => $lang['Update_profile_facility'],
			"L_ADD" => $lang['Finish_add_profile_facility'],
			"L_ACTION" => $lang['Action'],
			"U_ADD" => "edit",
			"S_ADD_ACTION" => append_sid("admin_user_facility.$phpEx?mode=edit&id=$profile_id"),
			"S_WORDS_ACTION" => append_sid("admin_user_facility.$phpEx?mode=add_update&id=$profile_id&start=$start"),
			"S_HIDDEN_FIELDS" => '')
		);

		$template->pparse("body");

		include('./page_footer_admin.'.$phpEx);
	}
	else if( $mode == "delete" )
	{
		if( isset($HTTP_POST_VARS['id']) ||  isset($HTTP_GET_VARS['id']) )
		{
			$id = ( isset($HTTP_POST_VARS['id']) ) ? $HTTP_POST_VARS['id'] : $HTTP_GET_VARS['id'];
		}
		else
		{
			$id = 0;
		}

		$confirm = isset($HTTP_POST_VARS['confirm']);

		if( $id && $confirm )
		{
			$sql = "DELETE FROM " . PROFILE_FACILITY_TABLE . " 
				WHERE profile_id = '$id'";

			if(!$result = $db->sql_query($sql))
			{
				message_die(GENERAL_ERROR, "Could not delete profile facility list", $lang['Error']);
			}

			$message = $lang['profile_facility_deleted'] . "<br /><br />" . sprintf($lang['Click_return_pfadmin'], "<a href=\"" . append_sid("admin_user_facility.$phpEx") . "\">", "</a>") . "<br /><br />" . sprintf($lang['Click_return_admin_index'], "<a href=\"" . append_sid("index.$phpEx?pane=right") . "\">", "</a>");

			message_die(GENERAL_MESSAGE, $message);
		}
		elseif( $id && !$confirm)
		{
			// Present the confirmation screen to the user
			$template->set_filenames(array(
				'body' => 'admin/confirm_body.tpl')
			);

			$hidden_fields = '<input type="hidden" name="mode" value="'.$mode.'" /><input type="hidden" name="id" value="' . $id . '" />';

			$template->assign_vars(array(
				'MESSAGE_TITLE' => $lang['Confirm'],
				'MESSAGE_TEXT' => $lang['Confirm_profile_facility_deleted'],

				'L_YES' => $lang['Yes'],
				'L_NO' => $lang['No'],

				'S_CONFIRM_ACTION' => append_sid("admin_user_facility.$phpEx"),
				'S_HIDDEN_FIELDS' => $hidden_fields)
			);
		}
		else
		{
			message_die(GENERAL_MESSAGE, $lang['no_profile_facility_selected']);
		}
	}
}
else
{
	$template->set_filenames(array(
		"body" => "admin/profile_facility_list_body.tpl")
	);

	$sql = "SELECT gp.profile_id, gp.profile_name, gp.description, gp.geom, count(gfp.facility_id) as total
			FROM geometry_profile gp INNER JOIN geometry_facility_profile gfp on gp.profile_id = gfp.profile_id
			GROUP BY gp.profile_id";
	if(!$result = $db->sql_query($sql))
	{
		message_die(GENERAL_ERROR, "Could not find profile facility settings", $lang['Error']);
	}
	$profiles = $db->sql_fetchrowset($result);

	$template->assign_vars(array(
		'L_WORDS_TITLE' => $lang['User_notification_admin'],
		'L_WORDS_TEXT' => $lang['User_notification_admin_explain'],
		'L_USER_NOTIFICATION' => $lang['User_facility_notification_explain'],
		'USER_NAME' => $fullname . ' ('. $username . '), ID: '.$nkey ,

		"L_PROFILE_ID" => "ID",
		"L_PROFILE_NAME" => "Profile Name",
		"L_DESCRIPTION" => "Description",
		"L_TOTAL" => "Facility Count",

		"L_EDIT" => "Edit",
		"L_DELETE" => "Delete",
		"L_ADD_EVENT" => "Add new profile facility list",
		"L_ACTION" => $lang['Action'],

		"S_WORDS_ACTION" => append_sid("admin_user_facility.$phpEx?mode=add"),
		"S_HIDDEN_FIELDS" => '')
	);

	for($i = 0;$i < count($profiles);  $i++)
	{
		$profile_id = $profiles[$i]['profile_id'];
		$profile_name = $profiles[$i]['profile_name'];
		$geom = $profiles[$i]['geom'];
		$description = $profiles[$i]['description'];
		$total = $profiles[$i]['total'];
		$exe_query = "perl $manage_profile -poly ".$geom;
		$output = array();
		$result = exec($exe_query, $output);
		$mismatch = (count($output) - 1 - $total) ? '*' : '';
		
		$row_color = ( !($i % 2) ) ? $theme['td_color1'] : $theme['td_color2'];
		$row_class = ( !($i % 2) ) ? $theme['td_class1'] : $theme['td_class2'];

		$template->assign_block_vars('words', array(
			"ROW_COLOR" => "#" . $row_color,
			"ROW_CLASS" => $row_class,
			"PROFILE_ID" => $profile_id,
			"PROFILE_NAME" => $profile_name,
			"DESCRIPTION" => $description,
			"TOTAL" => $total .$mismatch,

			"U_EDIT" => append_sid("admin_user_facility.$phpEx?mode=edit&amp;id=$profile_id&username=$username"),
			"U_DELETE" => append_sid("admin_user_facility.$phpEx?mode=delete&amp;id=$profile_id&username=$username"))
		);
	}
}

$template->pparse("body");

include('./page_footer_admin.'.$phpEx);

?>