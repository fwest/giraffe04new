<?php
/*
# $Id: search.php 509 2008-10-20 14:42:02Z klin $

# The PHP interface was originally inspired by PHPBB,
# file/database formats and directory structure are quite similar.

##############################################################################
# 
# Terms and Conditions of Software Use
# ====================================
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Disclaimer of Earthquake Information
# ====================================
# 
# The data and maps provided through this system are preliminary data
# and are subject to revision. They are computer generated and may not
# have received human review or official approval. Inaccuracies in the
# data may be present because of instrument or computer
# malfunctions. Subsequent review may result in significant revisions to
# the data. All efforts have been made to provide accurate information,
# but reliance on, or interpretation of earthquake data from a single
# source is not advised. Data users are cautioned to consider carefully
# the provisional nature of the information before using it for
# decisions that concern personal or public safety or the conduct of
# business that involves substantial monetary or operational
# consequences.
# 
# Disclaimer of Software and its Capabilities
# ===========================================
# 
# This software is provided as an "as is" basis.  Attempts have been
# made to rid the program of software defects and bugs, however the
# U.S. Geological Survey (USGS) have no obligations to provide maintenance, 
# support, updates, enhancements or modifications. In no event shall USGS 
# be liable to any party for direct, indirect, special, incidental or 
# consequential damages, including lost profits, arising out of the use 
# of this software, its documentation, or data obtained though the use 
# of this software, even if USGS or have been advised of the
# possibility of such damage. By downloading, installing or using this
# program, the user acknowledges and understands the purpose and
# limitations of this software.
# 
# Contact Information
# ===================
# 
# Coordination of this effort is under the auspices of the USGS Advanced
# National Seismic System (ANSS) coordinated in Golden, Colorado, which
# functions as the clearing house for development, distribution,
# documentation, and support. For questions, comments, or reports of
# potential bugs regarding this software please contact wald@usgs.gov or
# klin@usgs.gov.  
#
#############################################################################
*/

define('IN_SC', true);
$sc_root_path = './';
include($sc_root_path . 'extension.inc');
include($sc_root_path . 'common.'.$phpEx);
include($sc_root_path . 'includes/bbcode.'.$phpEx);
include($sc_root_path . 'includes/functions_search.'.$phpEx);

//
// Start session management
//
$userdata = session_pagestart($user_ip, PAGE_SEARCH);
init_userprefs($userdata);
//
// End session management
//

//
// Start auth check
//
if ( !$userdata['session_logged_in'] )
{
	redirect(append_sid("login.$phpEx?redirect=search.$phpEx", true));
}
//
// End auth check
//

//
// Define initial vars
//
if ( isset($HTTP_POST_VARS['mode']) || isset($HTTP_GET_VARS['mode']) )
{
	$mode = ( isset($HTTP_POST_VARS['mode']) ) ? $HTTP_POST_VARS['mode'] : $HTTP_GET_VARS['mode'];
}
else
{
	$mode = '';
}

if ( isset($HTTP_POST_VARS['search_keywords']) || isset($HTTP_GET_VARS['search_keywords']) )
{
	$search_keywords = ( isset($HTTP_POST_VARS['search_keywords']) ) ? $HTTP_POST_VARS['search_keywords'] : $HTTP_GET_VARS['search_keywords'];
}
else
{
	$search_keywords = '';
}

if ( isset($HTTP_POST_VARS['search_author']) || isset($HTTP_GET_VARS['search_author']))
{
	$search_author = ( isset($HTTP_POST_VARS['search_author']) ) ? $HTTP_POST_VARS['search_author'] : $HTTP_GET_VARS['search_author'];
	$search_author = phpbb_clean_username($search_author);
}
else
{
	$search_author = '';
}

$search_id = ( isset($HTTP_GET_VARS['search_id']) ) ? $HTTP_GET_VARS['search_id'] : '';

if ( isset($HTTP_POST_VARS['search_terms']) )
{
	$search_terms = ( $HTTP_POST_VARS['search_terms'] == 'all' ) ? 1 : 0;
}
else
{
	$search_terms = 0;
}

if ( isset($HTTP_POST_VARS['search_fields']) )
{
	$search_fields = ( $HTTP_POST_VARS['search_fields'] == 'all' ) ? 1 : 0;
}
else
{
	$search_fields = 0;
}

$return_chars = ( isset($HTTP_POST_VARS['return_chars']) ) ? intval($HTTP_POST_VARS['return_chars']) : 200;

$search_cat = ( isset($HTTP_POST_VARS['search_cat']) ) ? intval($HTTP_POST_VARS['search_cat']) : -1;
$search_forum = ( isset($HTTP_POST_VARS['search_forum']) ) ? intval($HTTP_POST_VARS['search_forum']) : -1;

if (isset($HTTP_GET_VARS['sort_key']))
{
	$sort_key = $HTTP_GET_VARS['sort_key'];
	$sort_order = ( isset($HTTP_GET_VARS['sort_order']) ) ? $HTTP_GET_VARS['sort_order'] : 'a';
	$default_sort = array($sort_key, $sort_order);
}
else 
{
	$sort_key = 'facility_id';
	$sort_order = ( isset($HTTP_GET_VARS['sort_order']) ) ? $HTTP_GET_VARS['sort_order'] : 'a';
	$default_sort = array($sort_key, $sort_order);
}

$new_sort_order = ($sort_order == 'd') ? 'a' : 'd';
$img_url = ' <img src="'.$sc_root_path . '/images/' . $sort_order . '.png" border="0" width="10" height="10">';

if ( !empty($HTTP_POST_VARS['search_time']) || !empty($HTTP_GET_VARS['search_time']))
{
	$search_time = time() - ( ( ( !empty($HTTP_POST_VARS['search_time']) ) ? intval($HTTP_POST_VARS['search_time']) : intval($HTTP_GET_VARS['search_time']) ) * 86400 );
	$topic_days = (!empty($HTTP_POST_VARS['search_time'])) ? intval($HTTP_POST_VARS['search_time']) : intval($HTTP_GET_VARS['search_time']);
}
else
{
	$search_time = 0;
	$topic_days = 0;
}

$start = ( isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;
$start = ($start < 0) ? 0 : $start;

$sort_by_types = array($lang['Sort_Time'], $lang['Sort_Post_Subject'], $lang['Sort_Topic_Title'], $lang['Sort_Author'], $lang['Sort_Forum']);

//
// encoding match for workaround
//
$multibyte_charset = 'utf-8, big5, shift_jis, euc-kr, gb2312';

//
// Begin core code
//
if ( $search_keywords != '' || $search_id )
{
	$store_vars = array('search_results', 'total_match_count', 'split_search', 'sort_by', 'sort_dir', 'return_chars');
	$search_results = '';

	//
	// Search ID Limiter, decrease this value if you experience further timeout problems with searching forums
	$limiter = 5000;
	$current_time = time();

	//
	// Cycle through options ...
	//
	if ($search_keywords != '' || $search_author != '' )
	{
		if ( $search_keywords != '' )
		{
			$stopword_array = @file($sc_root_path . 'language/lang_' . $board_config['default_lang'] . '/search_stopwords.txt'); 
			$synonym_array = @file($sc_root_path . 'language/lang_' . $board_config['default_lang'] . '/search_synonyms.txt'); 

			$split_search = array();
			$stripped_keywords = stripslashes($search_keywords);
			$split_search = ( !strstr($multibyte_charset, $lang['ENCODING']) ) ?  split_words(clean_words('search', $stripped_keywords, $stopword_array, $synonym_array), 'search') : split(' ', $search_keywords);	
			unset($stripped_keywords);

			$word_count = 0;
			$current_match_type = 'or';

			$word_match = array();
			$result_list = array();

			$new_paren = FALSE;
			$new_lat = FALSE;
			$new_lon = FALSE;
			for($i = 0; $i < count($split_search); $i++)
			{
				$sql_match = '';
				$paren = str_replace('"', '', trim($split_search[$i]), $count);
				if ($count)
				{
					if ($new_paren) {
						$new_paren = FALSE;
						$split_search[$i] = $paren_phrase .' '.$paren;
						$paren_phrase = '';
						$sql_match = 'paren';
					} else {
						$new_paren = TRUE;
						$paren_phrase = $paren;
						$split_search[$i] = '';
						continue;
					}
				} else if ($new_paren) {
					$paren_phrase = $paren_phrase .' '.$paren;
					$split_search[$i] = '';
					continue;
				}

				$paren = str_ireplace('type:', '', trim($split_search[$i]), $count);
				if ($count)
				{
					$split_search[$i] = $paren;
					$sql_match = 'type';
				}

				$paren = str_ireplace('lat:', '', trim($split_search[$i]), $count);
				if ($count)
				{
					$new_lat = TRUE;
					$lat1 = $paren;
					$split_search[$i] = '';
					continue;
				} else if ($new_lat) {
					$new_lat = FALSE;
					$lat2 = $paren;
					$sql_match = 'lat';
					$split_search[$i] = $sql_match;
				}

				$paren = str_ireplace('lon:', '', trim($split_search[$i]), $count);
				if ($count)
				{
					$new_lon = TRUE;
					$lon1 = $paren;
					$split_search[$i] = '';
					continue;
				} else if ($new_lon) {
					$new_lon = FALSE;
					$lon2 = $paren;
					$sql_match = 'lon';
					$split_search[$i] = $sql_match;
				}

				if ( strlen(str_replace(array('*', '%'), '', trim($split_search[$i]))) < $board_config['search_min_chars'] )
				{
					$split_search[$i] = '';
					continue;
				}

				switch ( $split_search[$i] )
				{
					case 'and':
						$current_match_type = 'and';
						break;

					case 'or':
						$current_match_type = 'or';
						break;

					case 'not':
						$current_match_type = 'not';
						break;

					default:
						if ( !empty($search_terms) )
						{
							$current_match_type = 'and';
						}
						switch ( $sql_match )
						{
							case 'lat':
								$sql_where = "lat_min BETWEEN $lat1 AND $lat2";
								break;
		
							case 'lon':
								$sql_where = "lon_min BETWEEN $lon1 AND $lon2";
								break;
		
							case 'type':
								$match_word =  addslashes('%' . str_replace('*', '', $split_search[$i]) . '%');
								$sql_where = "facility_type LIKE '$match_word'";
								break;
		
							default:
								$match_word =  addslashes('%' . str_replace('*', '', $split_search[$i]) . '%');
								$sql_where = "facility_name LIKE '$match_word'";
						}
						$sql = "SELECT facility_id
							FROM " . FACILITY_TABLE . "
							WHERE $sql_where";
						if ( !($result = $db->sql_query($sql)) )
						{
							message_die(GENERAL_ERROR, 'Could not obtain matched posts list', '', __LINE__, __FILE__, $sql);
						}

						$row = array();
						while( $temp_row = $db->sql_fetchrow($result) )
						{
							$row[$temp_row['facility_id']] = 1;

							if ( !$word_count )
							{
								$result_list[$temp_row['facility_id']] = 1;
							}
							else if ( $current_match_type == 'or' )
							{
								$result_list[$temp_row['facility_id']] = 1;
							}
							else if ( $current_match_type == 'not' )
							{
								$result_list[$temp_row['facility_id']] = 0;
							}
						}

						if ( $current_match_type == 'and' && $word_count )
						{
							@reset($result_list);
							while( list($post_id, $match_count) = @each($result_list) )
							{
								if ( !$row[$post_id] )
								{
									$result_list[$post_id] = 0;
								}
							}
						}

						$word_count++;

						$db->sql_freeresult($result);
				}
			}

			@reset($result_list);

			$search_ids = array();
			while( list($post_id, $matches) = each($result_list) )
			{
				if ( $matches )
				{
					$search_ids[] = $post_id;
				}
			}	
			
			unset($result_list);
			$total_match_count = count($search_ids);
		}

		if (! $total_match_count )
		{
			message_die(GENERAL_MESSAGE, $lang['No_search_match']);
		}

		//
		// Delete old data from the search result table
		//
		$sql = 'DELETE FROM ' . SEARCH_TABLE . '
			WHERE search_time < ' . ($current_time - (int) $board_config['session_length']);
		if ( !$result = $db->sql_query($sql) )
		{
			message_die(GENERAL_ERROR, 'Could not delete old search id sessions', '', __LINE__, __FILE__, $sql);
		}

		//
		// Store new result data
		//
		$search_results = implode(', ', $search_ids);
		$per_page = ( $board_config['topics_per_page'] ) ? $board_config['topics_per_page'] : $board_config['posts_per_page'];

		//
		// Combine both results and search data (apart from original query)
		// so we can serialize it and place it in the DB
		//
		$store_search_data = array();

		//
		// Limit the character length (and with this the results displayed at all following pages) to prevent
		// truncated result arrays. Normally, search results above 12000 are affected.
		// - to include or not to include
		/*
		$max_result_length = 60000;
		if (strlen($search_results) > $max_result_length)
		{
			$search_results = substr($search_results, 0, $max_result_length);
			$search_results = substr($search_results, 0, strrpos($search_results, ','));
			$total_match_count = count(explode(', ', $search_results));
		}
		*/

		for($i = 0; $i < count($store_vars); $i++)
		{
			$store_search_data[$store_vars[$i]] = $$store_vars[$i];
		}

		$result_array = serialize($store_search_data);
		unset($store_search_data);

		mt_srand ((double) microtime() * 1000000);
		$search_id = mt_rand();

		$sql = "UPDATE " . SEARCH_TABLE . " 
			SET search_id = $search_id, search_time = $current_time, search_array = '" . str_replace("\'", "''", $result_array) . "'
			WHERE session_id = '" . $userdata['session_id'] . "'";
		if ( !($result = $db->sql_query($sql)) || !$db->sql_affectedrows() )
		{
			$sql = "INSERT INTO " . SEARCH_TABLE . " (search_id, session_id, search_time, search_array) 
				VALUES($search_id, '" . $userdata['session_id'] . "', $current_time, '" . str_replace("\'", "''", $result_array) . "')";
			if ( !($result = $db->sql_query($sql)) )
			{
				message_die(GENERAL_ERROR, 'Could not insert search results', '', __LINE__, __FILE__, $sql);
			}
		}
	}
	else
	{
		$search_id = intval($search_id);
		if ( $search_id )
		{
			$sql = "SELECT search_array 
				FROM " . SEARCH_TABLE . " 
				WHERE search_id = $search_id  
					AND session_id = '". $userdata['session_id'] . "'";
			if ( !($result = $db->sql_query($sql)) )
			{
				message_die(GENERAL_ERROR, 'Could not obtain search results', '', __LINE__, __FILE__, $sql);
			}

			if ( $row = $db->sql_fetchrow($result) )
			{
				$search_data = unserialize($row['search_array']);
				for($i = 0; $i < count($store_vars); $i++)
				{
					$$store_vars[$i] = $search_data[$store_vars[$i]];
				}
			}
		}
	}

	//
	// Look up data ...
	//
	$base_url = "search.$phpEx?search_id=$search_id";
	if ( $search_results != '' )
	{
		$sql = "SELECT f.external_facility_id, f.facility_id, f.facility_type, f.external_facility_id, f.facility_name, f.short_name, f.description, f.lat_min as lat, f.lon_min as lon, f.update_username, f.update_timestamp, count(fs.facility_id) as facility_count
			FROM (" . FACILITY_SHAKING_TABLE . " fs INNER JOIN " . GRID_TABLE . " g
				ON fs.grid_id = g.grid_id) RIGHT JOIN " . FACILITY_TABLE . " f 
				ON f.facility_id = fs.facility_id 
			WHERE f.facility_id IN ($search_results) 
			GROUP BY f.facility_id";

		if ( !$result = $db->sql_query($sql) )
		{
			message_die(GENERAL_ERROR, 'Could not obtain search results', '', __LINE__, __FILE__, $sql);
		}

		$searchset = array();
		while( $row = $db->sql_fetchrow($result) )
		{
			$searchset[] = $row;
		}
		
		$db->sql_freeresult($result);		
		
		//
		// Define censored word matches
		//
		//$orig_word = array();
		//$replacement_word = array();
		//obtain_word_list($orig_word, $replacement_word);

		//
		// Output header
		//
		$page_title = $lang['Search'];
		include($sc_root_path . 'includes/page_header.'.$phpEx);	

		$template->set_filenames(array(
			'body' => 'search_results.tpl')
		);

		$l_search_matches = ( $total_match_count == 1 ) ? sprintf($lang['Found_search_match'], $total_match_count) : sprintf($lang['Found_search_matches'], $total_match_count);

		$template->assign_vars(array(
			'L_SEARCH_MATCHES' => $l_search_matches, 
			'L_TOPIC' => $lang['Topic'])
		);

		$tracking_topics = ( isset($HTTP_COOKIE_VARS[$board_config['cookie_name'] . '_t']) ) ? unserialize($HTTP_COOKIE_VARS[$board_config['cookie_name'] . '_t']) : array();
		$tracking_forums = ( isset($HTTP_COOKIE_VARS[$board_config['cookie_name'] . '_f']) ) ? unserialize($HTTP_COOKIE_VARS[$board_config['cookie_name'] . '_f']) : array();

		$searchset = arfsort( $searchset, array($default_sort));
		for($i = $start; $i < count($searchset) && $i < ($board_config['topics_per_page'] + $start); $i++)
		{
			$forum_url = append_sid("viewforum.$phpEx?" . POST_FORUM_URL . '=' . $searchset[$i]['forum_id']);
			$topic_url = append_sid("viewtopic.$phpEx?" . POST_TOPIC_URL . '=' . $searchset[$i]['topic_id'] . "&amp;highlight=$highlight_active");
			$post_url = append_sid("viewtopic.$phpEx?" . POST_POST_URL . '=' . $searchset[$i]['post_id'] . "&amp;highlight=$highlight_active") . '#' . $searchset[$i]['post_id'];

			$post_date = create_date($board_config['default_dateformat'], $searchset[$i]['post_time'], $board_config['board_timezone']);

			$message = $searchset[$i]['post_text'];
			$topic_title = $searchset[$i]['topic_title'];

			$forum_id = $searchset[$i]['facility_type'];
			$topic_id = $searchset[$i]['topic_id'];
			$topic_id = $searchset[$i]['facility_type'];

			$message = '';

			//if ( count($orig_word) )
			//{
			//	$topic_title = preg_replace($orig_word, $replacement_word, $searchset[$i]['topic_title']);
			//}

			$topic_type = $searchset[$i]['topic_type'];

			if ($topic_type == POST_ANNOUNCE)
			{
				$topic_type = $lang['Topic_Announcement'] . ' ';
			}
			else if ($topic_type == POST_STICKY)
			{
				$topic_type = $lang['Topic_Sticky'] . ' ';
			}
			else
			{
				$topic_type = '';
			}

			if ( $searchset[$i]['topic_vote'] )
			{
				$topic_type .= $lang['Topic_Poll'] . ' ';
			}

			$views = $searchset[$i]['topic_views'];

			$template->assign_block_vars('searchresults', array( 
				'ID' => $searchset[$i]['facility_id'],
				'EXT_FACILITY_ID' => $searchset[$i]['external_facility_id'],
				'FACILITY_DAMAGE' => $searchset[$i]['facility_count'],
				'DAMAGE_LEVEL' => $searchset[$i]['facility_count'],
				'TYPE' => $searchset[$i]['facility_type'],
				'NAME' => htmlspecialchars($searchset[$i]['facility_name']),
				'LATITUDE' => $searchset[$i]['lat'],
				'LONGITUDE' => $searchset[$i]['lon'],
				'METRIC' => $metric_value,
				'LOCATION' => htmlspecialchars(sprintf("%7.3f", $searchset[$i]['lat']) . '/' . 
					sprintf("%8.3f", $searchset[$i]['lon'])),
				
				'U_NAME' => append_sid("facility.$phpEx?id=" . $searchset[$i]['facility_id'])
				)
			);
		}


		$s_hidden_fields = '<input type="hidden" name="search_id" value="'.$search_id.'" />';
		$template->assign_vars(array(
			'PAGINATION' => generate_pagination($base_url."&sort_key=".$sort_key."&sort_order=".$sort_order , $total_match_count, $board_config['topics_per_page'], $start),
			'PAGE_NUMBER' => sprintf($lang['Page_of'], ( floor( $start / $board_config['topics_per_page'] ) + 1 ), ceil( $total_match_count / $board_config['topics_per_page'] )), 

			'L_METRIC' =>	$metric_tpl,
			'L_ID' => $lang['Fac_Id'] . (($sort_key == 'facility_id') ? $img_url : ''),
			'L_EXT_FACILITY_ID' => $lang['Ext_Fac_Id'] . (($sort_key == 'external_facility_id') ? $img_url : ''),
			'L_TYPE' => $lang['Fac_type'] . (($sort_key == 'facility_type') ? $img_url : ''),
			'L_NAME' => $lang['Fac_name'] . (($sort_key == 'facility_name') ? $img_url : ''),
			'L_DAMAGE_ESTIMATE' => $lang['Damage_estimate'] . (($sort_key == 'facility_count') ? $img_url : ''),
			'L_LATITUDE' => $lang['Lat'] . (($sort_key == 'lat') ? $img_url : ''),
			'L_LONGITUDE' => $lang['Lon'] . (($sort_key == 'lon') ? $img_url : ''),
			'L_GOTO_PAGE' => $lang['Goto_page'] . (($sort_key == 'external_facility_id') ? $img_url : ''),
			
			'U_ID' => $base_url."&sort_key=facility_id&sort_order=" . (($sort_key == 'facility_id') ? $new_sort_order : $sort_order),
			'U_EXT_FACILITY_ID' => $base_url."&sort_key=external_facility_id&sort_order=" . (($sort_key == 'external_facility_id') ? $new_sort_order : $sort_order),
			'U_TYPE' => $base_url."&sort_key=facility_type&sort_order=" . (($sort_key == 'facility_type') ? $new_sort_order : $sort_order),
			'U_NAME' => $base_url."&sort_key=facility_name&sort_order=" . (($sort_key == 'facility_name') ? $new_sort_order : $sort_order),
			'U_DAMAGE_ESTIMATE' => $base_url."&sort_key=facility_count&sort_order=" . (($sort_key == 'facility_count') ? $new_sort_order : $sort_order),
			'U_LATITUDE' => $base_url."&sort_key=lat&sort_order=" . (($sort_key == 'lat') ? $new_sort_order : $sort_order),
			'U_LONGITUDE' => $base_url."&sort_key=lon&sort_order=" . (($sort_key == 'lon') ? $new_sort_order : $sort_order),
			"S_HIDDEN_FIELDS" => $s_hidden_fields)
		);

		$template->pparse('body');

		include($sc_root_path . 'includes/page_tail.'.$phpEx);
	}
	else
	{
		message_die(GENERAL_MESSAGE, $lang['No_search_match']);
	}
}

//
// Search forum
//
$shakecast_category = array('ShakeMap', 'Facility', 'Shaking');

$s_forums = '';
foreach ($shakecast_category as $cat_id)
{
	$s_forums .= '<option value="' . $cat_id . '">' . $cat_id . '</option>';
	if ( empty($list_cat[$cat_id]) )
	{
		$list_cat[$cat_id] = $cat_id;
	}
}

if ( $s_forums != '' )
{
	$s_forums = '<option value="-1">' . $lang['All_available'] . '</option>' . $s_forums;

	//
	// Category to search
	//
	$s_categories = '<option value="-1">' . $lang['All_available'] . '</option>';
	while( list($cat_id, $cat_title) = @each($list_cat))
	{
		$s_categories .= '<option value="' . $cat_id . '">' . $cat_title . '</option>';
	}
}
else
{
	message_die(GENERAL_MESSAGE, $lang['No_searchable_forums']);
}

//
// Number of chars returned
//
$s_characters = '<option value="-1">' . $lang['All_available'] . '</option>';
$s_characters .= '<option value="0">0</option>';
$s_characters .= '<option value="25">25</option>';
$s_characters .= '<option value="50">50</option>';

for($i = 100; $i < 1100 ; $i += 100)
{
	$selected = ( $i == 200 ) ? ' selected="selected"' : '';
	$s_characters .= '<option value="' . $i . '"' . $selected . '>' . $i . '</option>';
}

//
// Sorting
//
$s_sort_by = "";
for($i = 0; $i < count($sort_by_types); $i++)
{
	$s_sort_by .= '<option value="' . $i . '">' . $sort_by_types[$i] . '</option>';
}

//
// Search time
//
$previous_days = array(0, 1, 7, 14, 30, 90, 180, 364);
$previous_days_text = array($lang['All_Posts'], $lang['1_Day'], $lang['7_Days'], $lang['2_Weeks'], $lang['1_Month'], $lang['3_Months'], $lang['6_Months'], $lang['1_Year']);

$s_time = '';
for($i = 0; $i < count($previous_days); $i++)
{
	$selected = ( $topic_days == $previous_days[$i] ) ? ' selected="selected"' : '';
	$s_time .= '<option value="' . $previous_days[$i] . '"' . $selected . '>' . $previous_days_text[$i] . '</option>';
}

//
// Output the basic page
//
$page_title = $lang['Search'];
include($sc_root_path . 'includes/page_header.'.$phpEx);

$template->set_filenames(array(
	'body' => 'search_body.tpl')
);

$template->assign_vars(array(
	'L_SEARCH_QUERY' => $lang['Search_query'], 
	'L_SEARCH_KEYWORDS' => $lang['Search_keywords'], 
	'L_SEARCH_KEYWORDS_EXPLAIN' => $lang['Search_keywords_explain'], 
	
	'S_SEARCH_ACTION' => append_sid("search.$phpEx?mode=results"),
	'S_HIDDEN_FIELDS' => '')
);

$template->pparse('body');

include($sc_root_path . 'includes/page_tail.'.$phpEx);

?>