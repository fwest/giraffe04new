<?php

$w = 500;
$h = 300;

//create the image
$im = imagecreate($w,$h);
if(!$im) {
	//you could return some sort of error image
	//to provide better feedback to the user
	die();
}
$trans = imagecolorallocate($im,0,0,255);
$black = imagecolorallocate($im,0,0,0);
$white = imagecolorallocate($im,255,255,255);
imagefill($im,0,0,$black);
#imagecolortransparent($im, $trans);

//label the number of points for testing
imagestring($im,1,0,0,'  test points in this area:',$trans);
					imageellipse($im, 100, 100, 20, 20, $white );


//echo a GIF
header('content-type:image/png;');
imagepng($im);

?>