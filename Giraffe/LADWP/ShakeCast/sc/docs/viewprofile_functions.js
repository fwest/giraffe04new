var event = gup('event');
//var version = gup('version');
if (!event) {
	event = 'lastevent';
}
var event_getVars = 'event='+event;

var map;
//var centerLatitude = 37.7;
//var centerLongitude = -122.2;
var startZoom = 6;
			var currentType = 'All';
			var allMarkers = {};
			var allTypes = {};

//Create the tile layer object
var detailLayer = new GTileLayer(new GCopyrightCollection(''));

//Method to retrieve the URL of the tile
detailLayer.getTileUrl = function(tile, zoom){
    //pass the x and y position as wel as the zoom
    var tileURL = "server.php?x="+tile.x+"&y="+tile.y+"&zoom="+zoom;
	//GLog.writeUrl(tileURL);
    return tileURL;
};

detailLayer.isPng = function() {
    //The example uses GIF's
    return true;
}

//add your tiles to the normal map projection
detailMapLayers = G_NORMAL_MAP.getTileLayers();
detailMapLayers.push(detailLayer);

//add your tiles to the satellite map projection
detailMapLayers = G_SATELLITE_MAP.getTileLayers();
detailMapLayers.push(detailLayer);

//add your tiles to the satellite map projection
detailMapLayers = G_HYBRID_MAP.getTileLayers();
detailMapLayers.push(detailLayer);



function changeBodyClass(from, to) {
     document.body.className = document.body.className.replace(from, to);
     return false;
}

function init() {
//	handleResize();

    map = new GMap2(document.getElementById("map_pane"));
    map.setCenter(new GLatLng(centerLatitude, centerLongitude), startZoom);
    map.setMapType(G_NORMAL_MAP);
    //map.setMapType(G_HYBRID_MAP);
    map.addControl(new GSmallMapControl());
    map.addControl(new GMapTypeControl());

	//Add shakemap 
	//initializeShakeMap();

	/*if (user_profile) {
			var selectedProfile = new Array();
		for (var i in user_profile) {
			selectedProfile[i] = geom[user_profile[i]];
		}
			drawPolyline(selectedProfile);
	}*/
	
	/*GEvent.addListener(map,'mousemove',function(latlng) {
		var pixelLocation = map.fromLatLngToDivPixel(latlng);
		GLog.write('ll:' + latlng + 'at:' + pixelLocation);
	});*/

	//changeBodyClass('loading', 'standby');
	//GLog.write('data loaded');
}


function drawPolyline(latlonstring) {
	map.clearOverlays();
	for (var i in latlonstring) {
	var latlngs = latlonstring[i].split(",");
    var pointCount = latlngs.length;


	/*var	sidebarid = document.getElementById('sidebar-list');
	while (sidebarid.firstChild) {
		sidebarid.removeChild(sidebarid.firstChild);
	}*/
	var lat_avg = 0;
	var lon_avg = 0;
	var poly_latlngs = [];
	for (var ind=0; ind < latlngs.length; ind = ind+2) {
		poly_latlngs.push(new GLatLng(latlngs[ind], latlngs[ind+1]));
		lat_avg = lat_avg + parseFloat(latlngs[ind]);
		lon_avg = lon_avg + parseFloat(latlngs[ind+1]);
	}
	lat_avg = lat_avg / poly_latlngs.length;
	lon_avg = lon_avg / poly_latlngs.length;
    // Plot polyline, adding the first element to the end, to close the loop.
    //var polyline = new GPolyline(latlngs, 'FF6633', 4, 0.8);
    var polyline = new GPolygon(poly_latlngs, 'FF6633', 4, 0.8, 'FF9966', 0.5);
    map.addOverlay(polyline);
	}
	
	if (lat_avg) {
	map.panTo(new GLatLng(lat_avg, lon_avg));
	}
	
	// set us up to zap the current polyline when we draw the next one.
	removePolyline = function() {
	  map.removeOverlay(polyline);
	}	
}

function initializeShakeMap() {
	//remove the existing points
	//map.clearOverlays();
	//create the boundary for the data
	//log the URL for testing
	//GLog.writeUrl('map_data.php?'+getVars);
	//retrieve the points using Ajax
	var request = GXmlHttp.create();
	request.open('GET', 'shakemap_xml.php?'+event_getVars, true);
	//GLog.writeUrl('shakemap_xml.php?'+event_getVars);
	request.onreadystatechange = function() {
		if (request.readyState == 4) {
			var xml = request.responseXML;
			var shakecast_id = xml.documentElement.getElementsByTagName("shakecast_id").item(0).firstChild.nodeValue;
		if (shakecast_id != '') {
			var LatLonBox = xml.documentElement.getElementsByTagName("LatLonBox").item(0);
			var lat_min = parseFloat(LatLonBox.getAttribute("south"));
			var lat_max = parseFloat(LatLonBox.getAttribute("north"));
			var lon_min = parseFloat(LatLonBox.getAttribute("west"));
			var lon_max = parseFloat(LatLonBox.getAttribute("east"));
			//for(id in shakemap_bounds) {
				var rectBounds = new GLatLngBounds(
					new GLatLng(lat_min, lon_min), 
					new GLatLng(lat_max, lon_max));
				//var img = document.createElement('img');
				var img = '/data/' + shakecast_id + '/ii_overlay.png';
				var sm_rect = new Rectangle(rectBounds, img);
				map.addOverlay(sm_rect);
				map.panTo(new GLatLng((lat_min + lat_max)/2,
						(lon_min + lon_max)/2));
				//createMarker(point);
			var event = xml.documentElement.getElementsByTagName("event").item(0);
			var lat = parseFloat(event.getAttribute("lat"));
			var lon = parseFloat(event.getAttribute("lon"));
			var point = new GLatLng(lat, lon);
			var icon = new GIcon();
			icon.image = "images/epicenter.png";
			icon.shadow = "images/shadow\-epicenter.png";
			icon.iconSize = new GSize(25, 25);
			icon.shadowSize = new GSize(38, 25);
			icon.iconAnchor = new GPoint(12, 12);
			icon.infoWindowAnchor = new GPoint(12, 12);
			//GLog.writeUrl(icon.image);
			var description = "<table border=1><tr bgcolor=#bbbbbb><td colspan=2 align=center><strong>" + 
				event.getAttribute("locstring") + "</strong>" + 
				"<tr><td><table bgcolor=#eeeeee width=100%>" + 
				"<tr><td colspan=2><font size=-1>Event ID: <strong>" + 
				event.getAttribute("id") + "</strong></td></tr>" +
				"<tr><td colspan=2><font size=-1>Magnitude: <strong>" + 
				event.getAttribute("magnitude") + "</strong></td></tr>" +
				"<tr><td><font size=-1>Lat: <strong>" + event.getAttribute("lat") + 
				"<strong></td><td><font size=-1>Lon: <strong>" + event.getAttribute("lon") + "<strong></td></tr>" +
				"<tr><td colspan=2><font size=-1>Time: <strong>" + event.getAttribute("timestamp") + 
				"<strong></td></tr></table></td></tr></table>";
			var marker = new GMarker(point, icon);
			GEvent.addListener(marker, "click", function(){
				marker.openInfoWindowHtml(description);
			});	
			GEvent.addListener(marker, "dblclick", function(){
				window.location = "eq_map.php?" + event_getVars;
			});	
			map.addOverlay(marker);
					//}
		}
		}
	}

	request.send(null);
}


function gup( name ) {
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var tmpURL = window.location.href;
  var results = regex.exec( tmpURL );
  if( results == null )
    return "";
  else
    return results[1];
}

/**
 * Written by Neil Crosby. 
 * http://www.workingwith.me.uk/articles/scripting/standardista_table_sorting
 *
 * This module is based on Stuart Langridge's "sorttable" code.  Specifically, 
 * the determineSortFunction, sortCaseInsensitive, sortDate, sortNumeric, and
 * sortCurrency functions are heavily based on his code.  This module would not
 * have been possible without Stuart's earlier outstanding work.
 *
 * Use this wherever you want, but please keep this comment at the top of this file.
 *
 * Copyright (c) 2006 Neil Crosby
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy 
 * of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
 * SOFTWARE.
 **/
var standardistaTableSorting = {

	that: false,
	isOdd: false,

	sortColumnIndex : -1,
	lastAssignedId : 0,
	newRows: -1,
	lastSortedTable: -1,

	/**
	 * Initialises the Standardista Table Sorting module
	 **/
	init : function() {
		// first, check whether this web browser is capable of running this script
		if (!document.getElementsByTagName) {
			return;
		}
		
		this.that = this;
		
		this.run();
		
	},
	
	/**
	 * Runs over each table in the document, making it sortable if it has a class
	 * assigned named "sortable" and an id assigned.
	 **/
	run : function() {
		var tables = document.getElementsByTagName("table");
		
		for (var i=0; i < tables.length; i++) {
			var thisTable = tables[i];
			
			if (css.elementHasClass(thisTable, 'sortable')) {
				this.makeSortable(thisTable);
			}
		}
	},
	
	/**
	 * Makes the given table sortable.
	 **/
	makeSortable : function(table) {
	
		// first, check if the table has an id.  if it doesn't, give it one
		if (!table.id) {
			table.id = 'sortableTable'+this.lastAssignedId++;
		}
		
		// if this table does not have a thead, we don't want to know about it
		if (!table.tHead || !table.tHead.rows || 0 == table.tHead.rows.length) {
			return;
		}
		
		// we'll assume that the last row of headings in the thead is the row that 
		// wants to become clickable
		var row = table.tHead.rows[table.tHead.rows.length - 1];
		for (var i=0; i < row.cells.length; i++) {
		
			// create a link with an onClick event which will 
			// control the sorting of the table
			var linkEl = createElement('a');
			linkEl.href = '#';
			linkEl.onclick = this.headingClicked;
			linkEl.setAttribute('columnId', i);
			linkEl.title = 'Click to sort';
			// move the current contents of the cell that we're 
			// hyperlinking into the hyperlink
			var innerEls = row.cells[i].childNodes;
			for (var j = 0; j < innerEls.length; j++) {
				linkEl.appendChild(innerEls[j]);
			}
			
			// and finally add the new link back into the cell
			row.cells[i].appendChild(linkEl);

			var spanEl = createElement('span');
			spanEl.className = 'tableSortArrow';
			spanEl.appendChild(document.createTextNode('\u00A0\u00A0'));
			row.cells[i].appendChild(spanEl);

		}
	
		if (css.elementHasClass(table, 'autostripe')) {
			this.isOdd = false;
			var rows = table.tBodies[0].rows;
			// We appendChild rows that already exist to the tbody, so it moves them rather than creating new ones
			for (var i=0;i<rows.length;i++) { 
				this.doStripe(rows[i]);
			}
		}
	},
	
	headingClicked: function(e) {
		
		var that = standardistaTableSorting.that;
		
		// linkEl is the hyperlink that was clicked on which caused
		// this method to be called
		var linkEl = getEventTarget(e);
		
		// directly outside it is a td, tr, thead and table
		var td     = linkEl.parentNode;
		var tr     = td.parentNode;
		var thead  = tr.parentNode;
		var table  = thead.parentNode;
		
		// if the table we're looking at doesn't have any rows
		// (or only has one) then there's no point trying to sort it
		if (!table.tBodies || table.tBodies[0].rows.length <= 1) {
			return false;
		}

		// the column we want is indicated by td.cellIndex
		var column = linkEl.getAttribute('columnId') || td.cellIndex;
		//var column = td.cellIndex;
		
		// find out what the current sort order of this column is
		var arrows = css.getElementsByClass(td, 'tableSortArrow', 'span');
		var previousSortOrder = '';
		if (arrows.length > 0) {
			previousSortOrder = arrows[0].getAttribute('sortOrder');
		}
		
		// work out how we want to sort this column using the data in the first cell
		// but just getting the first cell is no good if it contains no data
		// so if the first cell just contains white space then we need to track
		// down until we find a cell which does contain some actual data
		var itm = ''
		var rowNum = 0;
		while ('' == itm && rowNum < table.tBodies[0].rows.length) {
			itm = that.getInnerText(table.tBodies[0].rows[rowNum].cells[column]);
			rowNum++;
		}
		var sortfn = that.determineSortFunction(itm);

		// if the last column that was sorted was this one, then all we need to 
		// do is reverse the sorting on this column
		if (table.id == that.lastSortedTable && column == that.sortColumnIndex) {
			newRows = that.newRows;
			newRows.reverse();
		// otherwise, we have to do the full sort
		} else {
			that.sortColumnIndex = column;
			var newRows = new Array();

			for (var j = 0; j < table.tBodies[0].rows.length; j++) { 
				newRows[j] = table.tBodies[0].rows[j]; 
			}

			newRows.sort(sortfn);
		}

		that.moveRows(table, newRows);
		that.newRows = newRows;
		that.lastSortedTable = table.id;
		
		// now, give the user some feedback about which way the column is sorted
		
		// first, get rid of any arrows in any heading cells
		var arrows = css.getElementsByClass(tr, 'tableSortArrow', 'span');
		for (var j = 0; j < arrows.length; j++) {
			var arrowParent = arrows[j].parentNode;
			arrowParent.removeChild(arrows[j]);

			if (arrowParent != td) {
				spanEl = createElement('span');
				spanEl.className = 'tableSortArrow';
				spanEl.appendChild(document.createTextNode('\u00A0\u00A0'));
				arrowParent.appendChild(spanEl);
			}
		}
		
		// now, add back in some feedback 
		var spanEl = createElement('span');
		spanEl.className = 'tableSortArrow';
		if (null == previousSortOrder || '' == previousSortOrder || 'DESC' == previousSortOrder) {
			spanEl.appendChild(document.createTextNode(' \u2191'));
			spanEl.setAttribute('sortOrder', 'ASC');
		} else {
			spanEl.appendChild(document.createTextNode(' \u2193'));
			spanEl.setAttribute('sortOrder', 'DESC');
		}
		
		td.appendChild(spanEl);
		
		return false;
	},

	getInnerText : function(el) {
		
		if ('string' == typeof el || 'undefined' == typeof el) {
			return el;
		}
		
		if (el.innerText) {
			return el.innerText;  // Not needed but it is faster
		}

		var str = el.getAttribute('standardistaTableSortingInnerText');
		if (null != str && '' != str) {
			return str;
		}
		str = '';

		var cs = el.childNodes;
		var l = cs.length;
		for (var i = 0; i < l; i++) {
			// 'if' is considerably quicker than a 'switch' statement, 
			// in Internet Explorer which translates up to a good time 
			// reduction since this is a very often called recursive function
			if (1 == cs[i].nodeType) { // ELEMENT NODE
				str += this.getInnerText(cs[i]);
				break;
			} else if (3 == cs[i].nodeType) { //TEXT_NODE
				str += cs[i].nodeValue;
				break;
			}
		}
		
		// set the innertext for this element directly on the element
		// so that it can be retrieved early next time the innertext
		// is requested
		el.setAttribute('standardistaTableSortingInnerText', str);
		
		return str;
	},

	determineSortFunction : function(itm) {
		
		var sortfn = this.sortCaseInsensitive;
		
		if (itm.match(/^\d\d[\/-]\d\d[\/-]\d\d\d\d$/)) {
			sortfn = this.sortDate;
		}
		if (itm.match(/^\d\d[\/-]\d\d[\/-]\d\d$/)) {
			sortfn = this.sortDate;
		}
		if (itm.match(/^[�$]/)) {
			sortfn = this.sortCurrency;
		}
		if (itm.match(/^\d?\.?\d+$/)) {
			sortfn = this.sortNumeric;
		}
		if (itm.match(/^[+-]?\d*\.?\d+([eE]-?\d+)?$/)) {
			sortfn = this.sortNumeric;
		}
    		if (itm.match(/^([01]?\d\d?|2[0-4]\d|25[0-5])\.([01]?\d\d?|2[0-4]\d|25[0-5])\.([01]?\d\d?|2[0-4]\d|25[0-5])\.([01]?\d\d?|2[0-4]\d|25[0-5])$/)) {
        		sortfn = this.sortIP;
   		}

		return sortfn;
	},
	
	sortCaseInsensitive : function(a, b) {
		var that = standardistaTableSorting.that;
		
		var aa = that.getInnerText(a.cells[that.sortColumnIndex]).toLowerCase();
		var bb = that.getInnerText(b.cells[that.sortColumnIndex]).toLowerCase();
		if (aa==bb) {
			return 0;
		} else if (aa<bb) {
			return -1;
		} else {
			return 1;
		}
	},
	
	sortDate : function(a,b) {
		var that = standardistaTableSorting.that;

		// y2k notes: two digit years less than 50 are treated as 20XX, greater than 50 are treated as 19XX
		var aa = that.getInnerText(a.cells[that.sortColumnIndex]);
		var bb = that.getInnerText(b.cells[that.sortColumnIndex]);
		
		var dt1, dt2, yr = -1;
		
		if (aa.length == 10) {
			dt1 = aa.substr(6,4)+aa.substr(3,2)+aa.substr(0,2);
		} else {
			yr = aa.substr(6,2);
			if (parseInt(yr) < 50) { 
				yr = '20'+yr; 
			} else { 
				yr = '19'+yr; 
			}
			dt1 = yr+aa.substr(3,2)+aa.substr(0,2);
		}
		
		if (bb.length == 10) {
			dt2 = bb.substr(6,4)+bb.substr(3,2)+bb.substr(0,2);
		} else {
			yr = bb.substr(6,2);
			if (parseInt(yr) < 50) { 
				yr = '20'+yr; 
			} else { 
				yr = '19'+yr; 
			}
			dt2 = yr+bb.substr(3,2)+bb.substr(0,2);
		}
		
		if (dt1==dt2) {
			return 0;
		} else if (dt1<dt2) {
			return -1;
		}
		return 1;
	},

	sortCurrency : function(a,b) { 
		var that = standardistaTableSorting.that;

		var aa = that.getInnerText(a.cells[that.sortColumnIndex]).replace(/[^0-9.]/g,'');
		var bb = that.getInnerText(b.cells[that.sortColumnIndex]).replace(/[^0-9.]/g,'');
		return parseFloat(aa) - parseFloat(bb);
	},

	sortNumeric : function(a,b) { 
		var that = standardistaTableSorting.that;

		var aa = parseFloat(that.getInnerText(a.cells[that.sortColumnIndex]));
		if (isNaN(aa)) { 
			aa = 0;
		}
		var bb = parseFloat(that.getInnerText(b.cells[that.sortColumnIndex])); 
		if (isNaN(bb)) { 
			bb = 0;
		}
		return aa-bb;
	},

	makeStandardIPAddress : function(val) {
		var vals = val.split('.');

		for (x in vals) {
			val = vals[x];

			while (3 > val.length) {
				val = '0'+val;
			}
			vals[x] = val;
		}

		val = vals.join('.');

		return val;
	},

	sortIP : function(a,b) { 
		var that = standardistaTableSorting.that;

		var aa = that.makeStandardIPAddress(that.getInnerText(a.cells[that.sortColumnIndex]).toLowerCase());
		var bb = that.makeStandardIPAddress(that.getInnerText(b.cells[that.sortColumnIndex]).toLowerCase());
		if (aa==bb) {
			return 0;
		} else if (aa<bb) {
			return -1;
		} else {
			return 1;
		}
	},

	moveRows : function(table, newRows) {
		this.isOdd = false;

		// We appendChild rows that already exist to the tbody, so it moves them rather than creating new ones
		for (var i=0;i<newRows.length;i++) { 
			var rowItem = newRows[i];

			this.doStripe(rowItem);

			table.tBodies[0].appendChild(rowItem); 
		}
	},
	
	doStripe : function(rowItem) {
		if (this.isOdd) {
			css.addClassToElement(rowItem, 'odd');
		} else {
			css.removeClassFromElement(rowItem, 'odd');
		}
		
		this.isOdd = !this.isOdd;
	}

}

function standardistaTableSortingInit() {
	standardistaTableSorting.init();
}

/**
 * This array is used to remember mark status of rows in browse mode
 */
var marked_row = new Array;

/**
 * enables highlight and marking of rows in data tables
 *
 */
function PMA_markRowsInit() {
    // for every table row ...
	var rows = document.getElementsByTagName('tr');
	for ( var i = 0; i < rows.length; i++ ) {
	    // ... with the class 'odd' or 'even' ...
		if ( 'row0' != rows[i].className && 'row1' != rows[i].className ) {
		    continue;
		}
	    // ... add event listeners ...
        // ... to highlight the row on mouseover ...
	    if ( navigator.appName == 'Microsoft Internet Explorer' ) {
	        // but only for IE, other browsers are handled by :hover in css
			rows[i].onmouseover = function() {
			    this.className += ' hover';
			}
			rows[i].onmouseout = function() {
			    this.className = this.className.replace( ' hover', '' );
			}
	    }
		if ('row0' == rows[i].className ) {
		    unique_id = rows[i].id;
			selectedProfile[unique_id] = geom[unique_id];
			drawPolyline(selectedProfile);
		}
        // ... and to mark the row on click ...
		rows[i].onmousedown = function() {
		    var unique_id;
            if ( this.id.length > 0 ) {
                unique_id = this.id;
            } else {
		        return;
		    }
			//alert(unique_id);
			var tds = this.getElementsByTagName('td');
			var row_id = document.getElementById(unique_id);
			var ckbox_id = document.getElementById('ckbox_'+unique_id);
			if (selectedProfile[unique_id]) {
				row_id.className = "row1";
				ckbox_id.checked = false;
			} else {
				row_id.className = "row0";
				ckbox_id.checked = true;
			}
			var id;
			for ( var i = 0; i < tds.length; i++ ) {
				// ... with the class 'odd' or 'even' ...
				if ( 'id' == tds[i].className) {
					id = tds[i].childNodes[0].nodeValue;
					if (selectedProfile[id]) {
						delete selectedProfile[id];
					} else {
						selectedProfile[id] = geom[id];
					}
				}
			}
			drawPolyline(selectedProfile);
		}

	}
}

function copyCheckboxesRange(the_name)
{
	var ckbox_id = document.getElementById(the_name);

    if (typeof(ckbox_id) != 'undefined') {
        if (ckbox_id.checked == true) {
                ckbox_id.checked = false;
            }else {
                ckbox_id.checked = true;
            }
       
    }
}

