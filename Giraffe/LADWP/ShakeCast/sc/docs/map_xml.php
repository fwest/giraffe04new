<?php
/*
# $Id: map_xml.php 502 2008-10-09 15:32:14Z klin $

# The PHP interface was originally inspired by PHPBB,
# file/database formats and directory structure are quite similar.

##############################################################################
# 
# Terms and Conditions of Software Use
# ====================================
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Disclaimer of Earthquake Information
# ====================================
# 
# The data and maps provided through this system are preliminary data
# and are subject to revision. They are computer generated and may not
# have received human review or official approval. Inaccuracies in the
# data may be present because of instrument or computer
# malfunctions. Subsequent review may result in significant revisions to
# the data. All efforts have been made to provide accurate information,
# but reliance on, or interpretation of earthquake data from a single
# source is not advised. Data users are cautioned to consider carefully
# the provisional nature of the information before using it for
# decisions that concern personal or public safety or the conduct of
# business that involves substantial monetary or operational
# consequences.
# 
# Disclaimer of Software and its Capabilities
# ===========================================
# 
# This software is provided as an "as is" basis.  Attempts have been
# made to rid the program of software defects and bugs, however the
# U.S. Geological Survey (USGS) have no obligations to provide maintenance, 
# support, updates, enhancements or modifications. In no event shall USGS 
# be liable to any party for direct, indirect, special, incidental or 
# consequential damages, including lost profits, arising out of the use 
# of this software, its documentation, or data obtained though the use 
# of this software, even if USGS or have been advised of the
# possibility of such damage. By downloading, installing or using this
# program, the user acknowledges and understands the purpose and
# limitations of this software.
# 
# Contact Information
# ===================
# 
# Coordination of this effort is under the auspices of the USGS Advanced
# National Seismic System (ANSS) coordinated in Golden, Colorado, which
# functions as the clearing house for development, distribution,
# documentation, and support. For questions, comments, or reports of
# potential bugs regarding this software please contact wald@usgs.gov or
# klin@usgs.gov.  
#
#############################################################################
*/

define('IN_SC', true);
$sc_root_path = './';
//include($sc_root_path . 'includes/begin_caching.php');
include($sc_root_path . 'extension.inc');
include($sc_root_path . 'common.'.$phpEx);

//retrieve the variables from the GET vars
list($nelat,$nelng) = explode(',',$_GET['ne']);
list($swlat,$swlng) = explode(',',$_GET['sw']);

//clean the data
$nelng=(float)$nelng;
$swlng=(float)$swlng;
$nelat=(float)$nelat;
$swlat=(float)$swlat;

//require('db_credentials.php');
//$conn = mysql_connect("localhost", $db_name, $db_pass);
//mysql_select_db("sc", $conn);

//retrieve the variables from the GET vars
if (isset($_GET['event'])) {
	$evid = $_GET['event'];
}
if (isset($_GET['version'])) {
	$version = $_GET['version'];
}

if ($evid == 'lastevent') {
	//$result = mysql_query(
	$sql = "SELECT shakemap_id as evid, shakemap_version as version
		FROM grid 
		ORDER BY grid_id DESC LIMIT 1";
	if ( !($result = $db->sql_query($sql)) )
	{
		message_die(GENERAL_ERROR, 'Could not query ShakeMap event information', '', __LINE__, __FILE__, $sql);
	}
	if ($row = $db->sql_fetchrow($result))
	{
		$evid = $row['evid'];
		$version = $row['version'];
	}
	$db->sql_freeresult($result);
}
	
$damage_array = array();
if(isset($evid)) {
	if(!isset($version)) {
		$sql = "SELECT shakemap_version
			FROM 
				shakemap
			WHERE
				(shakemap_id = '$evid')
			ORDER BY
				shakemap_version DESC LIMIT 1";
		
		if ( !($result = $db->sql_query($sql)) )
		{
			message_die(GENERAL_ERROR, 'Could not query ShakeMap event information', '', __LINE__, __FILE__, $sql);
		}
		if ($row = $db->sql_fetchrow($result))
		{
			$version = $row['shakemap_version'];
		}
		$db->sql_freeresult($result);
	}

	//retrieve all points in the southwest/northeast boundary
	$sql = "SELECT g.grid_id, sm.metric, sm.value_column_number
		FROM 
			(grid g INNER JOIN shakemap_metric sm on
				g.shakemap_id = sm.shakemap_id AND g.shakemap_version = sm.shakemap_version)
		WHERE
			(g.shakemap_id = '$evid' AND g.shakemap_version = $version)
			AND sm.value_column_number IS NOT NULL";
	
	if ( !($result = $db->sql_query($sql)) )
	{
		message_die(GENERAL_ERROR, 'Could not query ShakeMap metric information', '', __LINE__, __FILE__, $sql);
	}
	$metric_query = '';
	$metric_xml = "<IntensityMeasure>\n";
	while ($row = $db->sql_fetchrow($result)) {
		$metric = $row['metric'];
		$grid_id = $row['grid_id'];
		$value_column_number = $row['value_column_number'];

		$metric_query = ",\"$metric\", fs.value_".$value_column_number." $metric_query";
		$metric_xml = $metric_xml." <metric unit=\\\"$metric\\\" value=\\\"\$value_$value_column_number\\\" />";
		$temp_xml = "<key unit=\\\"$metric\\\" value=\\\"\$value_$value_column_number\\\" />";

		$damage_sql = "select ff.damage_level, dl.name, ff.facility_id
		  from grid g
			   straight_join shakemap s
			   straight_join event e
			   straight_join facility_shaking sh
			   straight_join facility_fragility ff
			   inner join damage_level dl on ff.damage_level = dl.damage_level
		 where ff.metric = '".$metric."'
		   and s.shakemap_id = '".$evid."'
		   and s.shakemap_version = ".$version."
		   and g.grid_id = ".$grid_id."
		   and s.event_id = e.event_id and s.event_version = e.event_version
		   and g.grid_id = sh.grid_id
		   and (s.shakemap_id = g.shakemap_id and
				s.shakemap_version = g.shakemap_version)
		   and sh.facility_id = ff.facility_id
		   and sh.value_".$value_column_number." between ff.low_limit and ff.high_limit";

		if ( ($damage_result = $db->sql_query($damage_sql)) )
		{
			while ( $damage_row = $db->sql_fetchrow($damage_result) )
			{
				$damage_array[$damage_row['facility_id']] = $damage_row['damage_level'];
			}
		}
		$db->sql_freeresult($damage_result);

	}
	$metric_xml = $metric_xml."\n</IntensityMeasure>";
	$db->sql_freeresult($result);
}

if (isset($evid)) {
	$sql = "SELECT count(f.facility_id) as total_count
		FROM 
			(facility f INNER JOIN facility_shaking fs on
				f.facility_id = fs.facility_id) 
		WHERE
			(f.lon_min > $swlng AND f.lon_min < $nelng)
			AND (f.lat_min <= $nelat AND f.lat_min >= $swlat)
			AND (fs.grid_id = $grid_id)";

	if ( !($result = $db->sql_query($sql)) )
	{
		message_die(GENERAL_ERROR, 'Could not query facility count information', '', __LINE__, __FILE__, $sql);
	}

	if ( $row = $db->sql_fetchrow($result) ) {
		$total_count = $row['total_count'];
	}
	$db->sql_freeresult($result);

	if($nelng > $swlng) {
		//retrieve all points in the southwest/northeast boundary
		$sql = "SELECT f.facility_id,f.lat_min, f.lon_min, f.facility_name, f.facility_type
				$metric_query
			FROM 
				(facility f INNER JOIN facility_shaking fs on
					f.facility_id = fs.facility_id) 
			WHERE
				(f.lon_min > $swlng AND f.lon_min < $nelng)
				AND (f.lat_min <= $nelat AND f.lat_min >= $swlat)
				AND (fs.grid_id = $grid_id)
			ORDER BY
				fs.value_3 DESC
			LIMIT 100";
	
	} else {
		//retrieve all points in the southwest/northeast boundary
		//split over the meridian
		$sql = "SELECT f.facility_id,f.lat_min, f.lon_min, f.facility_name, f.facility_type,
				$metric_query
			FROM 
				(facility f INNER JOIN facility_shaking fs on
					f.facility_id = fs.facility_id) 
			WHERE
				(f.lon_min >= $swlng OR f.lon_min <= $nelng)
				AND (f.lat_min <= $nelat AND f.lat_min >= $swlat)
				AND (fs.grid_id == $grid_id)
			ORDER BY
				fs.value_3 DESC
			LIMIT 100";
	}
} else {
	$sql = "SELECT count(facility_id) as total_count
		FROM 
			facility 
		WHERE
			(lon_min > $swlng AND lon_min < $nelng)
			AND (lat_min <= $nelat AND lat_min >= $swlat)";

	if ( !($result = $db->sql_query($sql)) )
	{
		message_die(GENERAL_ERROR, 'Could not query facility count information', '', __LINE__, __FILE__, $sql);
	}

	if ( $row = $db->sql_fetchrow($result) ) {
		$total_count = $row['total_count'];
	}
	$db->sql_freeresult($result);

	$sql = "SELECT facility_id,lat_min, lon_min, facility_name, facility_type
		FROM 
			facility
		WHERE
			(lon_min > $swlng AND lon_min < $nelng)
			AND (lat_min <= $nelat AND lat_min >= $swlat)
		ORDER BY
			facility_name
		LIMIT 100";
}

$list = array();

if ( !($result = $db->sql_query($sql)) )
{
	message_die(GENERAL_ERROR, 'Could not query facility information', '', __LINE__, __FILE__, $sql);
}
while($row = $db->sql_fetchrow($result))
{
	$facility_id = $row['facility_id'];
	$lon_min = $row['lon_min'];
	$lat_min = $row['lat_min'];
	$facility_name = $row['facility_name'];
	$facility_type = $row['facility_type'];
	$value_1 = $row['value_1'];
	$value_2 = $row['value_2'];
	$value_3 = $row['value_3'];
	$value_4 = $row['value_4'];
	$value_5 = $row['value_5'];
	$value_6 = $row['value_6'];
	$value_7 = $row['value_7'];
	$value_8 = $row['value_8'];
	$value_9 = $row['value_9'];
	$value_10 = $row['value_10'];
	$icon = '';
	$file = "images/$facility_type.png";
	if(file_exists($file)) {
		$icon = 'true';
	}
	if (isset($evid)) {
		eval("\$xml = \"$metric_xml\";");
		$list[] = "
		<marker
			fac_id=\"$facility_id\" name=\"".htmlspecialchars($facility_name)."\" type=\"$facility_type\"
			icon = \"$icon\" latitude=\"$lat_min\" longitude=\"$lon_min\" >
			<damage value=\"".$damage_array[$facility_id]."\"/>
			$xml
		</marker>";
	} else {
		$list[] = "
		<marker 
			fac_id=\"$facility_id\" name=\"".htmlspecialchars($facility_name)."\" type=\"$facility_type\"
			icon = \"$icon\" latitude=\"$lat_min\" longitude=\"$lon_min\" >
		</marker>";
	}
}
//echo back the JavaScript object nicely formatted
header('content-type:text/xml;');
echo "<markers>";
echo join("\n\t",$list)."\n";
echo "<count>{$total_count}</count>\n";
echo "</markers>";
