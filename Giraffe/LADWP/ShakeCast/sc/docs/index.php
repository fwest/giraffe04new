<?php
/*
# $Id: index.php 498 2008-10-08 19:07:21Z klin $

# The PHP interface was originally inspired by PHPBB,
# file/database formats and directory structure are quite similar.

##############################################################################
# 
# Terms and Conditions of Software Use
# ====================================
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Disclaimer of Earthquake Information
# ====================================
# 
# The data and maps provided through this system are preliminary data
# and are subject to revision. They are computer generated and may not
# have received human review or official approval. Inaccuracies in the
# data may be present because of instrument or computer
# malfunctions. Subsequent review may result in significant revisions to
# the data. All efforts have been made to provide accurate information,
# but reliance on, or interpretation of earthquake data from a single
# source is not advised. Data users are cautioned to consider carefully
# the provisional nature of the information before using it for
# decisions that concern personal or public safety or the conduct of
# business that involves substantial monetary or operational
# consequences.
# 
# Disclaimer of Software and its Capabilities
# ===========================================
# 
# This software is provided as an "as is" basis.  Attempts have been
# made to rid the program of software defects and bugs, however the
# U.S. Geological Survey (USGS) have no obligations to provide maintenance, 
# support, updates, enhancements or modifications. In no event shall USGS 
# be liable to any party for direct, indirect, special, incidental or 
# consequential damages, including lost profits, arising out of the use 
# of this software, its documentation, or data obtained though the use 
# of this software, even if USGS or have been advised of the
# possibility of such damage. By downloading, installing or using this
# program, the user acknowledges and understands the purpose and
# limitations of this software.
# 
# Contact Information
# ===================
# 
# Coordination of this effort is under the auspices of the USGS Advanced
# National Seismic System (ANSS) coordinated in Golden, Colorado, which
# functions as the clearing house for development, distribution,
# documentation, and support. For questions, comments, or reports of
# potential bugs regarding this software please contact wald@usgs.gov or
# klin@usgs.gov.  
#
#############################################################################
*/

define('IN_SC', true);
$sc_root_path = './';
//$page_cache_time = 60;
//include($sc_root_path . 'includes/begin_caching.php');
include($sc_root_path . 'extension.inc');
include($sc_root_path . 'common.'.$phpEx);

//
// Start initial var setup
//
if ( isset($HTTP_GET_VARS[EVENT_TOPIC_URL]) || isset($HTTP_POST_VARS[EVENT_TOPIC_URL]) )
{
	$event_id = ( isset($HTTP_GET_VARS[EVENT_TOPIC_URL]) ) ? $HTTP_GET_VARS[EVENT_TOPIC_URL] : $HTTP_POST_VARS[EVENT_TOPIC_URL];
}
else
{
	$event_id = '';
}

if ( isset($HTTP_GET_VARS[EVENT_TOPIC_VERSION]) || isset($HTTP_POST_VARS[EVENT_TOPIC_VERSION]) )
{
	$event_version = ( isset($HTTP_GET_VARS[EVENT_TOPIC_VERSION]) ) ? intval($HTTP_GET_VARS[EVENT_TOPIC_VERSION]) : intval($HTTP_POST_VARS[EVENT_TOPIC_VERSION]);
}
else
{
	$event_version = '';
}
//
// End initial var setup
//

//
// Start session management
//
$userdata = session_pagestart($user_ip, PAGE_INDEX);
init_userprefs($userdata);
//
// End session management
//

$tracking_forums = ( isset($HTTP_COOKIE_VARS[$board_config['cookie_name'] . '_f']) ) ? unserialize($HTTP_COOKIE_VARS[$board_config['cookie_name'] . "_f"]) : array();

//
// Start page proper
//
//
// Find information of latest event
//
if ($event_id) {
	$sql = "SELECT g.shakemap_id, g.shakemap_version, g.grid_id,
				e.event_location_description, e.event_timestamp, e.magnitude, e.lat, e.lon, e.event_type
		FROM 
			(grid g  INNER JOIN event e on
			g.shakemap_id = e.event_id AND g.shakemap_version = e.event_version)
		WHERE
			e.event_id = '" . $event_id . "'";
	if ($event_version) {
		$sql .= "AND e.event_version = '" . $event_version . "'
			ORDER BY
			shakemap_version DESC LIMIT 1";
	} else {
		$sql .= "ORDER BY
			shakemap_version DESC LIMIT 1";
	}
} else {
	$sql = "SELECT g.shakemap_id, g.shakemap_version, g.grid_id,
				e.event_location_description, e.event_timestamp, e.magnitude, e.lat, e.lon, e.event_type
		FROM 
			(grid g INNER JOIN event e on
			g.shakemap_id = e.event_id AND g.shakemap_version = e.event_version)
		ORDER BY
			g.grid_id DESC LIMIT 1";
}

if ( !($result = $db->sql_query($sql)) )
{
	message_die(GENERAL_ERROR, 'Could not query ShakeMap information', '', __LINE__, __FILE__, $sql);
}
$event = $db->sql_fetchrow($result);
$db->sql_freeresult($result);

$ii_overlay_dir = $sc_conf_array['ROOT']['DataRoot']."/".$event['shakemap_id']."-".$event['shakemap_version'];
$ii_overlay_jpg = $ii_overlay_dir."/ii_overlay.jpg";
$ii_overlay_png = $ii_overlay_dir."/ii_overlay.png";
if(!file_exists($ii_overlay_png) && file_exists($ii_overlay_jpg) && function_exists('imagecreate')) {
	$image = ImageCreateFromJPEG($ii_overlay_jpg);
	ImagePNG($image, $ii_overlay_png);
}

if ($event_id) {
	$view_event_url = append_sid("event.$phpEx?" . SC_LIST_URL . "=2&" . 
		EVENT_TOPIC_URL . "=" . $event['shakemap_id'] ."&" . EVENT_TOPIC_VERSION . "=" .
		$event['shakemap_version']);
} else {
	$view_event_url = append_sid("eq_list.$phpEx?" . POST_FORUM_URL . "=1");
}

$sql = "SELECT sm.metric, m.name, sm.value_column_number, sm.max_value, sm.min_value
	FROM 
		shakemap_metric sm INNER JOIN metric m on
		sm.metric = m.short_name
	WHERE
		sm.shakemap_id ='".$event['shakemap_id']."' 
		and sm.shakemap_version =".$event['shakemap_version']."
		AND sm.value_column_number IS NOT NULL";
		
if ( !($result = $db->sql_query($sql)) )
{
	message_die(GENERAL_ERROR, 'Could not query shaking metric information', '', __LINE__, __FILE__, $sql);
}
$metric = $db->sql_fetchrowset($result);
$db->sql_freeresult($result);

$sql = "SELECT count(metric) as total_count
	FROM 
		shakemap_metric
	WHERE
		shakemap_id ='".$event['shakemap_id']."' 
		and shakemap_version =".$event['shakemap_version'];
		
if ( !($result = $db->sql_query($sql)) )
{
	message_die(GENERAL_ERROR, 'Could not query shaking metric information', '', __LINE__, __FILE__, $sql);
}
$param_count = 0;
if ($row = $db->sql_fetchrow($result))
{
	$param_count = $row['total_count'];
}
$db->sql_freeresult($result);

$damage_level = array();
$total_count = 0;
for ($ind = 0; $ind < count($metric); $ind++) {
	$sql = "select ff.damage_level, dl.name,
		   count(ff.facility_id) as facility_count
	  from grid g
		   straight_join shakemap s
		   straight_join event e
		   straight_join facility_shaking sh
		   straight_join facility_fragility ff
		   inner join damage_level dl on ff.damage_level = dl.damage_level
	 where ff.metric = '".$metric[$ind]['metric']."'
	   and s.shakemap_id = '".$event['shakemap_id']."'
	   and s.shakemap_version = ".$event['shakemap_version']."
	   and g.grid_id = ".$event['grid_id']."
	   and s.event_id = e.event_id and s.event_version = e.event_version
	   and g.grid_id = sh.grid_id
	   and (s.shakemap_id = g.shakemap_id and
			s.shakemap_version = g.shakemap_version)
	   and sh.facility_id = ff.facility_id
	   and sh.value_".$metric[$ind]['value_column_number']." between ff.low_limit and ff.high_limit
	 GROUP BY ff.damage_level
	 ORDER BY dl.severity_rank";

	if ( !($result = $db->sql_query($sql)) )
	{
		message_die(GENERAL_ERROR, 'Could not query facility damage assessment information', '', __LINE__, __FILE__, $sql);
	}
	
	while ($row = $db->sql_fetchrow($result))
	{
		$damage_level[$row['damage_level']] += $row['facility_count'];
		$total_count += $row['facility_count'];
		$param_count++;
	}
	//$damage_level[$metric[$ind]['metric']] = $db->sql_fetchrowset($result);
	$db->sql_freeresult($result);
}

//
// Start output of page
//
define('SHOW_ONLINE', true);
$page_title = $lang['Index'];
include($sc_root_path . 'includes/index_header.'.$phpEx);

$template->set_filenames(array(
	'body' => 'index_body.tpl')
);

$sql = 
	"SELECT post_subject, post_text
	FROM 
		" . POSTS_TEXT_TABLE . "
	WHERE
		shakemap_id = \"".$event['shakemap_id']."\"";
if ( ($result = $db->sql_query($sql)) )
{
	if($comment_row = $db->sql_fetchrow($result))
	{
		$template->assign_block_vars('comment_event', array(
			'L_COMMENT' => $comment_row['post_subject'] . " : ",
			'L_COMMENT_TEXT' => str_replace("\n", "<br>", $comment_row['post_text'])));
	}
}

if ( $event['event_type'] == 'TEST' || $event['event_type'] == 'SCENARIO' )
{
	$template->assign_block_vars('test_event', array());
}

$forum_name = 'M '. $event['magnitude'] . ' - ' . $event['event_location_description'];
$forum_desc = '<br />ID: <strong>' . $event['shakemap_id'] . 
'</strong> Version: <strong>' .	$event['shakemap_version'] . '</strong><br />';
$forum_desc .= 'Origin Time: '. $event['event_timestamp'] .'<br />';
$forum_desc .= 'Location: '. $event['lon'] . ', ' . $event['lat'];

$damage_level_array = array();
$sql = "select damage_level
  from damage_level
 ORDER BY severity_rank";
if ( !($result = $db->sql_query($sql)) )
{
	message_die(GENERAL_ERROR, 'Could not damage level information', '', __LINE__, __FILE__, $sql);
}
while ($row = $db->sql_fetchrow($result)) 
{
	array_push($damage_level_array, $row['damage_level']);
}
$db->sql_freeresult($result);

$facility_summary = '<ul id="scnav">';
foreach ($damage_level_array as $damage_level_key) {
	if (isset($damage_level[$damage_level_key]))
	{
		$damage_summary = $damage_level[$damage_level_key];
		$facility_summary .= '<li style="width:'.intval($damage_summary/$total_count*300).'px; background-color:'.$damage_level_key.'; text-align:center;">';
			$facility_summary .= '<strong>'.$damage_summary.'</strong>';
		$facility_summary .= '</li>';
	}
}
$facility_summary .= '</ul>';

make_jumpbox('index.'.$phpEx);

$template->assign_vars(array(
	'FORUM_NAME' => $forum_name,
	'FORUM_DESC' => $forum_desc,
	'FORUM_COUNT' => 'Number of facilities evaluated: <strong>'.(($total_count) ? $total_count : 0).'</strong>',
	'MAPPING_STYLE' => (($param_count > 7) ? 'style="height:350px;"' : ''),

	'L_MODERATOR' => $l_moderators, 
	'L_FORUM_FOLDER_ALT' => $folder_alt, 

	'EQ' => $facility_summary, 

	'U_VIEWFORUM' => $view_event_url)
);

for ($ind = 0; $ind < count($metric); $ind++) {
	if (strtoupper($metric[$ind]['metric']) == 'MMI') {
		$metric[$ind]['min_value'] = $mmi_numeric[intval($metric[$ind]['min_value'])];
		$metric[$ind]['max_value'] = $mmi_numeric[intval($metric[$ind]['max_value'])];
	}
	$template->assign_block_vars('metrics', array(	
	'METRIC_NAME' => $metric[$ind]['name'],
	'METRIC_UNIT' => $metric_unit[strtoupper($metric[$ind]['metric'])],
	'MIN_VALUE' => $metric[$ind]['min_value'],
	'MAX_VALUE' => $metric[$ind]['max_value'])
	);
}

//
// Generate the page
//
$template->pparse('body');

include($sc_root_path . 'includes/page_tail.'.$phpEx);
?>