<?php
/*
# $Id: event.php 509 2008-10-20 14:42:02Z klin $

# The PHP interface was originally inspired by PHPBB,
# file/database formats and directory structure are quite similar.

##############################################################################
# 
# Terms and Conditions of Software Use
# ====================================
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Disclaimer of Earthquake Information
# ====================================
# 
# The data and maps provided through this system are preliminary data
# and are subject to revision. They are computer generated and may not
# have received human review or official approval. Inaccuracies in the
# data may be present because of instrument or computer
# malfunctions. Subsequent review may result in significant revisions to
# the data. All efforts have been made to provide accurate information,
# but reliance on, or interpretation of earthquake data from a single
# source is not advised. Data users are cautioned to consider carefully
# the provisional nature of the information before using it for
# decisions that concern personal or public safety or the conduct of
# business that involves substantial monetary or operational
# consequences.
# 
# Disclaimer of Software and its Capabilities
# ===========================================
# 
# This software is provided as an "as is" basis.  Attempts have been
# made to rid the program of software defects and bugs, however the
# U.S. Geological Survey (USGS) have no obligations to provide maintenance, 
# support, updates, enhancements or modifications. In no event shall USGS 
# be liable to any party for direct, indirect, special, incidental or 
# consequential damages, including lost profits, arising out of the use 
# of this software, its documentation, or data obtained though the use 
# of this software, even if USGS or have been advised of the
# possibility of such damage. By downloading, installing or using this
# program, the user acknowledges and understands the purpose and
# limitations of this software.
# 
# Contact Information
# ===================
# 
# Coordination of this effort is under the auspices of the USGS Advanced
# National Seismic System (ANSS) coordinated in Golden, Colorado, which
# functions as the clearing house for development, distribution,
# documentation, and support. For questions, comments, or reports of
# potential bugs regarding this software please contact wald@usgs.gov or
# klin@usgs.gov.  
#
#############################################################################
*/

define('IN_SC', true);
$sc_root_path = './';
//$page_cache_time = 60;
//include($sc_root_path . 'includes/begin_caching.php');
include($sc_root_path . 'extension.inc');
include($sc_root_path . 'common.'.$phpEx);

//
// Start session management
//
$userdata = session_pagestart($user_ip, $sc_list_id);
init_userprefs($userdata);
//
// End session management
//

//
// Start auth check
//
if ( !$userdata['session_logged_in'] )
{
	redirect(append_sid("login.$phpEx?redirect=event.$phpEx", true));
}
//
// End auth check
//

//
// Start initial var setup
//
if ( isset($HTTP_GET_VARS[SC_LIST_URL]) || isset($HTTP_POST_VARS[SC_LIST_URL]) )
{
	$sc_list_id = ( isset($HTTP_GET_VARS[SC_LIST_URL]) ) ? intval($HTTP_GET_VARS[SC_LIST_URL]) : intval($HTTP_POST_VARS[SC_LIST_URL]);
}
else
{
	$sc_list_id = '';
}

if ( isset($HTTP_GET_VARS[EVENT_TOPIC_URL]) || isset($HTTP_POST_VARS[EVENT_TOPIC_URL]) )
{
	$event_id = ( isset($HTTP_GET_VARS[EVENT_TOPIC_URL]) ) ? $HTTP_GET_VARS[EVENT_TOPIC_URL] : $HTTP_POST_VARS[EVENT_TOPIC_URL];
}
else
{
	$event_id = '';
}

if ( isset($HTTP_GET_VARS[EVENT_TOPIC_VERSION]) || isset($HTTP_POST_VARS[EVENT_TOPIC_VERSION]) )
{
	$event_version = ( isset($HTTP_GET_VARS[EVENT_TOPIC_VERSION]) ) ? intval($HTTP_GET_VARS[EVENT_TOPIC_VERSION]) : intval($HTTP_POST_VARS[EVENT_TOPIC_VERSION]);
}
else
{
	$event_version = '';
}


if (isset($HTTP_GET_VARS['sort_key']))
{
	$sort_key = $HTTP_GET_VARS['sort_key'];
	$sort_order = ( isset($HTTP_GET_VARS['sort_order']) ) ? $HTTP_GET_VARS['sort_order'] : 'd';
	$default_sort = array($sort_key, $sort_order);
	if ($sort_key != 'damage') 
	{
		$default_sort = array_merge( $default_sort , array('damage', 'd'));
	}
}
else 
{
	$sort_key = 'damage';
	$sort_order = ( isset($HTTP_GET_VARS['sort_order']) ) ? $HTTP_GET_VARS['sort_order'] : 'd';
	$default_sort = array($sort_key, $sort_order);
}

$new_sort_order = ($sort_order == 'd') ? 'a' : 'd';
$img_url = ' <img src="'.$sc_root_path . '/images/' . $sort_order . '.png" border="0" width="10" height="10">';
$start = ( isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;
$start = ($start < 0) ? 0 : $start;
$base_url = append_sid("event.$phpEx?f=2&" . EVENT_TOPIC_URL . "=$event_id&" . EVENT_TOPIC_VERSION . "=$event_version");
if (!$sc_list_id || !$event_id || !$event_version)
{
	message_die(GENERAL_MESSAGE, 'Event_not_exist');
}

$ii_overlay_dir = $sc_conf_array['ROOT']['DataRoot']."/".$event_id."-".$event_version;
$ii_overlay_jpg = $ii_overlay_dir."/ii_overlay.jpg";
$ii_overlay_png = $ii_overlay_dir."/ii_overlay.png";
if(!file_exists($ii_overlay_png) && file_exists($ii_overlay_jpg) && function_exists('imagecreate')) {
	$image = ImageCreateFromJPEG($ii_overlay_jpg);
	ImagePNG($image, $ii_overlay_png);
}

//
// Go ahead and pull all data for this event
//
$sql = 
	"SELECT event_location_description, magnitude, lat, lon, event_type
	FROM 
		event
	WHERE
		event_id = \"$event_id\" AND event_version = \"$event_version\"";
if ( ($result = $db->sql_query($sql)) )
{
	$event_row = $db->sql_fetchrow($result);
}

if ( $event_row['event_type'] == 'TEST' || $event_row['event_type'] == 'SCENARIO' )
{
	$template->assign_block_vars('test_event', array());
}

$sql = 
	"SELECT post_subject, post_text
	FROM 
		" . POSTS_TEXT_TABLE . "
	WHERE
		shakemap_id = \"$event_id\"";
if ( ($result = $db->sql_query($sql)) )
{
	if($comment_row = $db->sql_fetchrow($result))
	{
		$template->assign_block_vars('comment_event', array(
			'L_COMMENT' => $comment_row['post_subject'] . " : ",
			'L_COMMENT_TEXT' => str_replace("\n", "<br>", $comment_row['post_text'])));
	}
}

$sql = 
	"SELECT g.grid_id, sm.metric, sm.value_column_number
	FROM 
		(grid g INNER JOIN shakemap_metric sm on
			g.shakemap_id = sm.shakemap_id AND g.shakemap_version = sm.shakemap_version)
	WHERE
		g.shakemap_id = \"$event_id\" AND g.shakemap_version = \"$event_version\"
		AND sm.value_column_number IS NOT NULL";
if ( !($result = $db->sql_query($sql)) )
{
   message_die(GENERAL_ERROR, 'Could not obtain ShakeMap event information', '', __LINE__, __FILE__, $sql);
}
$metric_query = '';
$metric_tpl = '';
$metric_item = array();
$facility_damage = array();
$damage_level = array();
$severity_rank = array();
while( $row = $db->sql_fetchrow($result) )
{
	$grid_id = $row['grid_id'];
	$metric_query = ", " . 'value_'.$row['value_column_number'] . " as ". strtoupper($row['metric']) . " $metric_query";
	array_push($metric_item, strtoupper($row['metric']));
	if (strtoupper($row['metric']) == 'MMI') {
		if ($sort_key == strtoupper($row['metric']))
		{
			$metric_tpl = "$metric_tpl<th><a href=\"" . 
			append_sid("event.$phpEx?f=2&" . EVENT_TOPIC_URL . "=$event_id&" . EVENT_TOPIC_VERSION . "=$event_version&sort_key=".strtoupper($row['metric'])."&sort_order=" . $new_sort_order)
			. "\">" . strtoupper($row['metric']) . $img_url. "</a></th>";
		}
		else
		{
			$metric_tpl = "$metric_tpl<th><a href=\"" . 
			append_sid("event.$phpEx?f=2&" . EVENT_TOPIC_URL . "=$event_id&" . EVENT_TOPIC_VERSION . "=$event_version&sort_key=".strtoupper($row['metric'])."&sort_order=" . $sort_order)
			. "\">" . strtoupper($row['metric']) . "</a></th>";
		}
	} else {
		if ($sort_key == strtoupper($row['metric']))
		{
			$metric_tpl = "$metric_tpl<th><a href=\"" . 
			append_sid("event.$phpEx?f=2&" . EVENT_TOPIC_URL . "=$event_id&" . EVENT_TOPIC_VERSION . "=$event_version&sort_key=".strtoupper($row['metric'])."&sort_order=" . $new_sort_order)
			. "\">" . strtoupper($row['metric']) . " " .$metric_unit[strtoupper($row['metric'])] . $img_url. "</a></th>";
		}
		else
		{
			$metric_tpl = "$metric_tpl<th><a href=\"" . 
			append_sid("event.$phpEx?f=2&" . EVENT_TOPIC_URL . "=$event_id&" . EVENT_TOPIC_VERSION . "=$event_version&sort_key=".strtoupper($row['metric'])."&sort_order=" . $sort_order)
			. "\">" . strtoupper($row['metric']) . " " .$metric_unit[strtoupper($row['metric'])] . "</a></th>";
		}
	}
	
	$sql = "select ff.damage_level, dl.name, ff.facility_id, dl.severity_rank, dl.is_max_severity,
		sh.value_".$row['value_column_number']." fac_shaking, ff.low_limit, ff.high_limit
	  from grid g
		   straight_join shakemap s
		   straight_join event e
		   straight_join facility_shaking sh
		   straight_join facility_fragility ff
		   inner join damage_level dl on ff.damage_level = dl.damage_level
	 where ff.metric = '".$row['metric']."'
	   and s.shakemap_id = '".$event_id."'
	   and s.shakemap_version = ".$event_version."
	   and g.grid_id = ".$row['grid_id']."
	   and s.event_id = e.event_id and s.event_version = e.event_version
	   and g.grid_id = sh.grid_id
	   and (s.shakemap_id = g.shakemap_id and
			s.shakemap_version = g.shakemap_version)
	   and sh.facility_id = ff.facility_id
	   and sh.value_".$row['value_column_number']." between ff.low_limit and ff.high_limit";
	if (($facility_result = $db->sql_query($sql)) )
	{
		$damage_estimate = $db->sql_fetchrowset($facility_result);
		if (count($damage_estimate) > 0)
		{
			$metric_sort = array(strtoupper($row['metric']), 'd');
		}
		$db->sql_freeresult($facility_result);
		for ($ind = 0; $ind < count($damage_estimate); $ind++) {
			$facility_damage['f'.$damage_estimate[$ind]['facility_id']] = $damage_estimate[$ind]['damage_level'];
			$damage_level[$damage_estimate[$ind]['damage_level']] = $damage_estimate[$ind]['name'];
			$severity_rank['f'.$damage_estimate[$ind]['facility_id']] = $damage_estimate[$ind]['severity_rank'];
			if ($damage_estimate[$ind]['is_max_severity']) 
			{
				$exceedance['f'.$damage_estimate[$ind]['facility_id']] = ($damage_estimate[$ind]['fac_shaking'] / $damage_estimate[$ind]['low_limit']);
			}
			else
			{
				$exceedance['f'.$damage_estimate[$ind]['facility_id']] = ($damage_estimate[$ind]['fac_shaking'] - $damage_estimate[$ind]['low_limit']) / ($damage_estimate[$ind]['high_limit'] - $damage_estimate[$ind]['low_limit']);
			}
		}
	}
}
$db->sql_freeresult($result);
	
$count_sql = "SELECT count(f.facility_id) as total
	FROM ((((" . SHAKEMAP_TABLE ." s INNER JOIN ". EVENT_TABLE . " e on
		s.event_id = e.event_id AND s.event_version = e.event_version) 
		INNER JOIN grid g on	g.shakemap_id = s.shakemap_id AND g.shakemap_version = s.shakemap_version) 
		INNER JOIN facility_shaking fs on fs.grid_id = g.grid_id) 
		INNER JOIN facility f on fs.facility_id = f.facility_id)
	WHERE 
		g.grid_id = \"$grid_id\"";
$result = $db->sql_query($count_sql);
if( !$result )
{
	message_die(GENERAL_ERROR, "Couldn't get ShakeMap list information.", "", __LINE__, __FILE__, $sql );
}
$row = $db->sql_fetchrow($result);
$total_facilities = $row['total'];
$db->sql_freeresult($result);


$sql = "SELECT f.facility_id,f.external_facility_id,f.lat_min, f.lon_min, f.facility_name, f.facility_type, 
			s.shakemap_id, s.shakemap_version, s.receive_timestamp, s.shakemap_region, 
			e.event_type, e.event_location_description, e.magnitude, e.lat, e.lon
			$metric_query
	FROM ((((" . SHAKEMAP_TABLE ." s INNER JOIN ". EVENT_TABLE . " e on
		s.event_id = e.event_id AND s.event_version = e.event_version) 
		INNER JOIN grid g on	g.shakemap_id = s.shakemap_id AND g.shakemap_version = s.shakemap_version) 
		INNER JOIN facility_shaking fs on fs.grid_id = g.grid_id) 
		INNER JOIN facility f on fs.facility_id = f.facility_id)
	WHERE 
		g.grid_id = \"$grid_id\"";
if ( !($result = $db->sql_query($sql)) )
{
	message_die(GENERAL_ERROR, "Could not obtain ShakeCast facility information.", '', __LINE__, __FILE__, $sql);
}


$postrow = array();
$total_posts = 0;
if ($row = $db->sql_fetchrow($result))
{
	do
	{
		$fac_damage = ($severity_rank['f'.$row['facility_id']]) ? $severity_rank['f'.$row['facility_id']] : 0;
		$postrow[] = array_merge($row, array('damage' => $fac_damage, 'exceedance' => $exceedance['f'.$row['facility_id']]));
	}
	while ($row = $db->sql_fetchrow($result));
	$db->sql_freeresult($result);

	$total_posts = count($postrow);
}
$order_arr =
  array(
    $default_sort, array('damage', 'd'), array('exceedance', 'd'), array('facility_name', 'a')
  );

$postrow = arfsort( $postrow, $order_arr);
//
// Post, reply and other URL generation for
// templating vars
//
$view_gmap_url = append_sid("eq_map.$phpEx?" . EVENT_TOPIC_URL . "=$event_id&"
	 . EVENT_TOPIC_VERSION . "=$event_version");
$view_list_url = append_sid("eq_list.$phpEx?" . SC_LIST_URL . "=$sc_list_id");
switch ($sc_list_id) {
	case 1:
		$list_name = 'Latest Processed Earthquake';
		break;
	case 2:
		$list_name = 'Earthquake Archive';
		break;
	case 3:
		$list_name = 'Earthquake Scenario';
		break;
	default:
		$list_name = '';
}

//
// Load templates
//
$template->set_filenames(array(
	'body' => 'event_body.tpl')
);

//
// Output page header
//
$page_title = 'Event Summary';
$active_class = 'C_EARTHQUAKE';
$header_tpl = 'event_header.tpl';
include($sc_root_path . 'includes/page_header.'.$phpEx);

//
// If we've got a hightlight set pass it on to pagination,
// I get annoyed when I lose my highlight after the first page.
//
$pagination = generate_pagination("event.$phpEx?" . SC_LIST_URL . "=$sc_list_id&amp;" . 
	EVENT_TOPIC_URL . "=$event_id&amp;" . EVENT_TOPIC_VERSION ."=$event_version" .
	"&sort_key=$sort_key&sort_order=$sort_order", 
	$total_facilities, $board_config['topics_per_page'], $start);

//
// Send vars to template
//
$gmap = '<div id="map" style="width: 250px; height: 300px"></div>';
$template->assign_vars(array(
    'LIST_NAME' => $list_name,
    'TOPIC_TITLE' => "ShakeCast Facility Summary",
	'PAGINATION' => $pagination,
	'PAGE_NUMBER' => sprintf($lang['Page_of'], ( floor( $start / $board_config['topics_per_page'] ) + 1 ), ceil( $total_facilities / $board_config['topics_per_page'] )), 

	'L_METRIC' =>	$metric_tpl,
	'L_EVENT' => "M ". $event_row['magnitude']. " - ". $event_row['event_location_description'] . 
		" (ID: ". $event_id." - ".$event_version.")",
	'L_FAC_ID' =>  $lang['Fac_Id'] . (($sort_key == 'external_facility_id') ? $img_url : ''),
	'L_TYPE' => $lang['Fac_type'] . (($sort_key == 'facility_type') ? $img_url : ''),
	'L_NAME' => $lang['Fac_description'] . (($sort_key == 'facility_name') ? $img_url : ''),
	'L_DAMAGE_ESTIMATE' => $lang['Damage_estimate'] . (($sort_key == 'damage') ? $img_url : ''),
	'L_EXCEEDANCE' => $lang['Exceedance_ratio'] . (($sort_key == 'exceedance') ? $img_url : ''),
	'L_LATITUDE' => $lang['Lat'] . (($sort_key == 'lat_min') ? $img_url : ''),
	'L_LONGITUDE' => $lang['Lon'] . (($sort_key == 'lon_min') ? $img_url : ''),
	'L_GMAP' => $lang['GMap_view'],

	'U_FAC_ID' =>  $base_url."&sort_key=external_facility_id&sort_order=" . (($sort_key == 'external_facility_id') ? $new_sort_order : $sort_order),
	'U_TYPE' => $base_url."&sort_key=facility_type&sort_order=" . (($sort_key == 'facility_type') ? $new_sort_order : $sort_order),
	'U_NAME' => $base_url."&sort_key=facility_name&sort_order=" . (($sort_key == 'facility_name') ? $new_sort_order : $sort_order),
	'U_DAMAGE_ESTIMATE' => $base_url."&sort_key=damage&sort_order=" . (($sort_key == 'damage') ? $new_sort_order : $sort_order),
	'U_LATITUDE' => $base_url."&sort_key=lat_min&sort_order=" . (($sort_key == 'lat_min') ? $new_sort_order : $sort_order),
	'U_LONGITUDE' => $base_url."&sort_key=lon_min&sort_order=" . (($sort_key == 'lon_min') ? $new_sort_order : $sort_order),
	'U_GMAP' => $view_gmap_url,
	'U_VIEW_LIST' => $view_list_url)
);

//
// Okay, let's do the loop, yeah come on baby let's do the loop
// and it goes like this ...
//
for($i = $start; $i < $total_posts && $i < ($board_config['topics_per_page'] + $start); $i++)
{
	$row_color = ( !($i % 2) ) ? $theme['td_color1'] : $theme['td_color2'];
	$row_class = ( !($i % 2) ) ? $theme['td_class1'] : $theme['td_class2'];
	$metric_value = '';
	foreach ($metric_item as $m_value) {
		if (strtoupper($m_value) == 'MMI') {
			$metric_value = "$metric_value<td>".$mmi_numeric[intval($postrow[$i][$m_value])]."</td>";
		} else {
			$metric_value = "$metric_value<td>".$postrow[$i][$m_value]."</td>";
		}
	}

	$template->assign_block_vars('postrow', array(
		'ROW_COLOR' => ($i & 1),
		'ROW_CLASS' => $row_class,

		'ID' => $postrow[$i]['external_facility_id'],
		'FACILITY_ID' => $postrow[$i]['facility_id'],
		'FACILITY_DAMAGE' => $facility_damage['f'.$postrow[$i]['facility_id']],
		'DAMAGE_LEVEL' => ($damage_level[$facility_damage['f'.$postrow[$i]['facility_id']]]) ? $damage_level[$facility_damage['f'.$postrow[$i]['facility_id']]] : 'Not Evaluated',
		'EXCEEDANCE' => $postrow[$i]['exceedance'],
		'TYPE' => $postrow[$i]['facility_type'],
		'NAME' => htmlspecialchars($postrow[$i]['facility_name']),
		'LATITUDE' => $postrow[$i]['lat_min'],
		'LONGITUDE' => $postrow[$i]['lon_min'],
		'METRIC' => $metric_value,
		'LOCATION' => htmlspecialchars(sprintf("%7.3f", $postrow[$i]['lat_min']) . '/' . 
			sprintf("%8.3f", $postrow[$i]['lon_min'])))
	);
}

$template->pparse('body');

include($sc_root_path . 'includes/page_tail.'.$phpEx);

?>