var event_id = gup('event');
var version = gup('version');
var event_getVars;
if (event_id) {
	event_getVars = 'event=' + event_id
	if (version) {
		event_getVars = event_getVars + '&version=' + version;
	}
}

var browser=navigator.appName;
if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)){ //test for MSIE x.x;
 var version=new Number(RegExp.$1) // capture x.x portion and store as a number
}

var map;
//var centerLatitude = 37.7;
//var centerLongitude = -122.2;
var startZoom = 10;
var maxZoom = 12;
var sm_visible = 1;
var sm_rect;
var noMarkerLimit = 100;
			var currentType = 'All';
			var allMarkers = {};
			var allTypes = {};
			
//Create the tile layer object
var detailLayer = new GTileLayer(new GCopyrightCollection(''));

//Method to retrieve the URL of the tile
detailLayer.getTileUrl = function(tile, zoom){
    //pass the x and y position as wel as the zoom
    var tileURL = "server.php?x="+tile.x+"&y="+tile.y+"&zoom="+zoom;
	//GLog.writeUrl(tileURL);
    return tileURL;
};

detailLayer.isPng = function() {
    //The example uses GIF's
    return true;
}

//add your tiles to the normal map projection
detailMapLayers = G_NORMAL_MAP.getTileLayers();
detailMapLayers.push(detailLayer);

//add your tiles to the satellite map projection
detailMapLayers = G_SATELLITE_MAP.getTileLayers();
detailMapLayers.push(detailLayer);

//add your tiles to the satellite map projection
detailMapLayers = G_HYBRID_MAP.getTileLayers();
detailMapLayers.push(detailLayer);


var deselectCurrent = function() {};

function initializePoint(pointData) {
	var point = new GLatLng(pointData.latitude, pointData.longitude);
	if (pointData.icon == 'true') { 
	var icon = new GIcon();
	icon.image = "images/" + pointData.type + ".png";
	icon.shadow = "images/shadow\-" + pointData.type + ".png";
	icon.iconSize = new GSize(25, 25);
	icon.shadowSize = new GSize(38, 25);
	icon.iconAnchor = new GPoint(12, 12);
	icon.infoWindowAnchor = new GPoint(12, 12);
	//GLog.writeUrl(icon.image);
	var marker = new GMarker(point, icon);
	} else {
	var marker = new GMarker(point);
	}
	var listItem = document.createElement('li');
	var listItemLink = listItem.appendChild(document.createElement('a'));
	var visible = false;
	var description = "<table border=1 cellspacing=0 cellpadding=0><tr><td>" + 
		"<table><tr bgcolor=#cccccc><td colspan=2><strong> " + 
		pointData.name + "</strong></td></tr>" + 
		"<tr><td><font size=-1>Lat: " + pointData.latitude + "</td><td><font size=-1>Lon: " + 
		pointData.longitude + "</td></tr></table></td></tr>";
	if (pointData.im) {
		description = description + "<tr><td colspan=2><table width=100%>"; 
		for (var metric in pointData.im) {
		description = description + "<tr bgcolor=#eeeeee><td>" + metric + ": </td><td><strong>" + 
		pointData.im[metric] + "</strong></td></tr>";
		}
		description = description + "</table></td></tr>"; 
	}
	description = description + "</table>"; 
	listItemLink.href = "#"+pointData.fac_id;
	listItemLink.innerHTML = '<strong>' + pointData.name + ' </strong><span>' 
		+ pointData.type  + '</span>';
	
	var focusPoint = function() {
		deselectCurrent();
		listItem.className = 'current';
		deselectCurrent = function() { listItem.className = ''; }
		//marker.openInfoWindowHtml(pointData.name);
		marker.openInfoWindowHtml(description);
		map.panTo(point);
		return false;
	}

	GEvent.addListener(marker, 'click', focusPoint);	
	listItemLink.onclick = focusPoint;

	pointData.show = function() {
		if (!visible) {
			map.addOverlay(marker);
			visible = true;
		}
	}
	pointData.hide = function() {
		if (visible) {
			map.removeOverlay(marker);
			visible = false;
		}
	}
	pointData.visible = function() {
		return visible;
	}

	pointData.point = function() {
		return point;
	}

	//pointData.show();
}

function refreshMarkers() {
		var map_bound = map.getBounds();
		for(id in allMarkers) {
			if ((allMarkers[id].type == currentType || 'All' == currentType) &&
				map_bound.contains(allMarkers[id].point()))
				allMarkers[id].show();
			else
				allMarkers[id].hide();	
		}
}

function refreshTabs(newTypes) {
	for(var type in allTypes) {
		if (!newTypes[type] && 'All' != type) {
			allTypes[type].hide();
		} else {
			allTypes[type].show();
		}
	}
}

function windowHeight() {
	// Standard browsers (Mozilla, Safari, etc.)
	if (self.innerHeight)
		return self.innerHeight;
	// IE 6
	if (document.documentElement && document.documentElement.clientHeight)
		return document.documentElement.clientHeight;
	// IE 5
	if (document.body)
		return document.body.clientHeight;
	// Just in case.
	return 0;
}

function changeBodyClass(from, to) {
     document.body.className = document.body.className.replace(from, to);
     return false;
}

function init() {

    map = new GMap2(document.getElementById("map_pane"));
    map.addControl(new GLargeMapControl());
    map.addControl(new GMapTypeControl());
    map.setCenter(new GLatLng(centerLatitude, centerLongitude), startZoom);
    //map.setMapType(G_NORMAL_MAP);
    map.setMapType(G_HYBRID_MAP);
	//map.enableContinuousZoom();

	//Add shakemap 
	initializeShakeMap();

	//updateMarkers();
	
	GEvent.addListener(map,'moveend',function() {
		updateMarkers();
	});

	/*GEvent.addListener(map,'mousemove',function(latlng) {
		var pixelLocation = map.fromLatLngToDivPixel(latlng);
		GLog.write('ll:' + latlng + 'at:' + pixelLocation);
	});*/

	changeBodyClass('loading', 'standby');
	//GLog.write('data loaded');
}

function updateMarkers() {
	
	//remove the existing points
	//map.clearOverlays();
	//create the boundary for the data
	var bounds = map.getBounds();
	var southWest = bounds.getSouthWest();
	var northEast = bounds.getNorthEast();
	var zoom = map.getZoom();
	var getVars = 'ne=' + northEast.toUrlValue()
		+ '&sw=' + southWest.toUrlValue()+ '&zoom=' + zoom
	if (event_getVars) {
		getVars = getVars + '&' + event_getVars;
	}
	//log the URL for testing
	if ( zoom > maxZoom ) {map.removeOverlay(sm_rect); sm_visible = 0;}
	
	if (zoom >= 10 ) {
	if (sm_visible == 0 && zoom <= maxZoom ) {map.addOverlay(sm_rect); sm_visible = 1;}
	//GLog.writeUrl('map_xml.php?'+getVars);
	//retrieve the points using Ajax
	var request = GXmlHttp.create();
	request.open('GET', 'map_xml.php?'+getVars, true);
	request.onreadystatechange = function() {
		if (request.readyState == 4) {
			var xml = request.responseXML;
			//var count = xml.documentElement.getElementsByTagName("count").item(0).firstChild.nodeValue;
			//GLog.write(count+":"+zoom);
			var facTypes = { 'All':{} };
			var facilities = xml.documentElement.getElementsByTagName("marker");
			//GLog.write(facilities.length);
			for(var id = 0; id < facilities.length; id++) {
				var key = 'f' + facilities[id].getAttribute("fac_id");
				if (!allMarkers[key]) {
					var marker = {};
					marker.fac_id = facilities[id].getAttribute("fac_id");
					marker.name = facilities[id].getAttribute("name");
					marker.type = facilities[id].getAttribute("type");
					marker.icon = facilities[id].getAttribute("icon");
					marker.latitude = parseFloat(facilities[id].getAttribute("latitude"));
					marker.longitude = parseFloat(facilities[id].getAttribute("longitude"));
					if (event_id) {
						var im = facilities[id].getElementsByTagName("metric");
						var metric = {};
						for (var m_id = 0; m_id < im.length; m_id++) {
							metric[im[m_id].getAttribute("unit")] =
								parseFloat(im[m_id].getAttribute("value"));
						}
						marker.im = metric;
					}
					initializePoint(marker);
					allMarkers[key] = marker;
				}
				facTypes[facilities[id].getAttribute("type")] = true;
			}

			refreshMarkers();
			var marker_cnt = 0;
			for (var cnt in allMarkers) {
				if (!bounds.contains(new GLatLng(allMarkers[cnt].latitude, allMarkers[cnt].longitude))) {
					allMarkers[cnt].hide();
					delete allMarkers[cnt];
				} else {
				marker_cnt++;
				}
			}
			//GLog.write(marker_cnt);
		}
		}
	request.send(null);
	}

}

function initializeShakeMap() {
	//remove the existing points
	//map.clearOverlays();
	//create the boundary for the data
	if (!event_id) {
		return "";
	}
	//log the URL for testing
	//GLog.writeUrl('map_data.php?'+getVars);
	//retrieve the points using Ajax
	var request = GXmlHttp.create();
	request.open('GET', 'shakemap_xml.php?'+event_getVars, true);
	//GLog.writeUrl('shakemap_xml.php?'+event_getVars);
	request.onreadystatechange = function() {
		if (request.readyState == 4) {
			var xml = request.responseXML;
			var shakecast_id = xml.documentElement.getElementsByTagName("shakecast_id").item(0).firstChild.nodeValue;
		if (shakecast_id != '') {
			var LatLonBox = xml.documentElement.getElementsByTagName("LatLonBox").item(0);
			var lat_min = parseFloat(LatLonBox.getAttribute("south"));
			var lat_max = parseFloat(LatLonBox.getAttribute("north"));
			var lon_min = parseFloat(LatLonBox.getAttribute("west"));
			var lon_max = parseFloat(LatLonBox.getAttribute("east"));
			//for(id in shakemap_bounds) {
				var rectBounds = new GLatLngBounds(
					new GLatLng(lat_min, lon_min), 
					new GLatLng(lat_max, lon_max));
				//var img = document.createElement('img');
				var img = '/data/' + shakecast_id + '/ii_overlay.png';
				sm_rect = new Rectangle(rectBounds, img);
				map.addOverlay(sm_rect);
				map.panTo(new GLatLng((lat_min + lat_max)/2,
						(lon_min + lon_max)/2));
				//createMarker(point);
			var event_id = xml.documentElement.getElementsByTagName("event").item(0);
			var lat = parseFloat(event_id.getAttribute("lat"));
			var lon = parseFloat(event_id.getAttribute("lon"));
			var point = new GLatLng(lat, lon);
			var icon = new GIcon();
			icon.image = "images/epicenter.png";
			icon.shadow = "images/shadow\-epicenter.png";
			icon.iconSize = new GSize(25, 25);
			icon.shadowSize = new GSize(38, 25);
			icon.iconAnchor = new GPoint(12, 12);
			icon.infoWindowAnchor = new GPoint(12, 12);
			//GLog.writeUrl(icon.image);
			var description = "<table border=1><tr bgcolor=#bbbbbb><td colspan=2 align=center><strong>" + 
				event_id.getAttribute("locstring") + "</strong>" + 
				"<tr><td><table bgcolor=#eeeeee width=100%>" + 
				"<tr><td colspan=2><font size=-1>Event ID: <strong>" + 
				event_id.getAttribute("id") + "</strong></td></tr>" +
				"<tr><td colspan=2><font size=-1>Magnitude: <strong>" + 
				event_id.getAttribute("magnitude") + "</strong></td></tr>" +
				"<tr><td><font size=-1>Lat: <strong>" + event_id.getAttribute("lat") + 
				"<strong></td><td><font size=-1>Lon: <strong>" + event_id.getAttribute("lon") + "<strong></td></tr>" +
				"<tr><td colspan=2><font size=-1>Time: <strong>" + event_id.getAttribute("timestamp") + 
				"<strong></td></tr></table></td></tr></table>";
			var marker = new GMarker(point, icon);
			GEvent.addListener(marker, "click", function(){
				marker.openInfoWindowHtml(description);
			});	
			//GEvent.addListener(marker, "dblclick", function(){
			//	window.location = "googlemaps.php?event=lastevent";
			//});	
			map.addOverlay(marker);
					//}
		}
		}
	}

	request.send(null);
}


function gup( name ) {
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var tmpURL = window.location.href;
  var results = regex.exec( tmpURL );
  if( results == null )
    return "";
  else
    return results[1];
}

/**
 * Written by Neil Crosby. 
 * http://www.workingwith.me.uk/articles/scripting/standardista_table_sorting
 *
 * This module is based on Stuart Langridge's "sorttable" code.  Specifically, 
 * the determineSortFunction, sortCaseInsensitive, sortDate, sortNumeric, and
 * sortCurrency functions are heavily based on his code.  This module would not
 * have been possible without Stuart's earlier outstanding work.
 *
 * Use this wherever you want, but please keep this comment at the top of this file.
 *
 * Copyright (c) 2006 Neil Crosby
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy 
 * of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
 * SOFTWARE.
 **/
var standardistaTableSorting = {

	that: false,
	isOdd: false,

	sortColumnIndex : -1,
	lastAssignedId : 0,
	newRows: -1,
	lastSortedTable: -1,

	/**
	 * Initialises the Standardista Table Sorting module
	 **/
	init : function() {
		// first, check whether this web browser is capable of running this script
		if (!document.getElementsByTagName) {
			return;
		}
		
		this.that = this;
		
		this.run();
		
	},
	
	/**
	 * Runs over each table in the document, making it sortable if it has a class
	 * assigned named "sortable" and an id assigned.
	 **/
	run : function() {
		var tables = document.getElementsByTagName("table");
		
		for (var i=0; i < tables.length; i++) {
			var thisTable = tables[i];
			
			if (css.elementHasClass(thisTable, 'sortable')) {
				this.makeSortable(thisTable);
			}
		}
	},
	
	/**
	 * Makes the given table sortable.
	 **/
	makeSortable : function(table) {
	
		// first, check if the table has an id.  if it doesn't, give it one
		if (!table.id) {
			table.id = 'sortableTable'+this.lastAssignedId++;
		}
		
		// if this table does not have a thead, we don't want to know about it
		if (!table.tHead || !table.tHead.rows || 0 == table.tHead.rows.length) {
			return;
		}
		
		// we'll assume that the last row of headings in the thead is the row that 
		// wants to become clickable
		var row = table.tHead.rows[table.tHead.rows.length - 1];
		for (var i=0; i < row.cells.length; i++) {
		
			// create a link with an onClick event which will 
			// control the sorting of the table
			var linkEl = createElement('a');
			linkEl.href = '#';
			linkEl.onclick = this.headingClicked;
			linkEl.setAttribute('columnId', i);
			linkEl.title = 'Click to sort';
			// move the current contents of the cell that we're 
			// hyperlinking into the hyperlink
			var innerEls = row.cells[i].childNodes;
			for (var j = 0; j < innerEls.length; j++) {
				linkEl.appendChild(innerEls[j]);
			}
			
			// and finally add the new link back into the cell
			row.cells[i].appendChild(linkEl);

			var spanEl = createElement('span');
			spanEl.className = 'tableSortArrow';
			spanEl.appendChild(document.createTextNode('\u00A0\u00A0'));
			row.cells[i].appendChild(spanEl);

		}
	
		if (css.elementHasClass(table, 'autostripe')) {
			this.isOdd = false;
			var rows = table.tBodies[0].rows;
			// We appendChild rows that already exist to the tbody, so it moves them rather than creating new ones
			for (var i=0;i<rows.length;i++) { 
				this.doStripe(rows[i]);
			}
		}
	},
	
	headingClicked: function(e) {
		
		var that = standardistaTableSorting.that;
		
		// linkEl is the hyperlink that was clicked on which caused
		// this method to be called
		var linkEl = getEventTarget(e);
		
		// directly outside it is a td, tr, thead and table
		var td     = linkEl.parentNode;
		var tr     = td.parentNode;
		var thead  = tr.parentNode;
		var table  = thead.parentNode;
		
		// if the table we're looking at doesn't have any rows
		// (or only has one) then there's no point trying to sort it
		if (!table.tBodies || table.tBodies[0].rows.length <= 1) {
			return false;
		}

		// the column we want is indicated by td.cellIndex
		var column = linkEl.getAttribute('columnId') || td.cellIndex;
		//var column = td.cellIndex;
		
		// find out what the current sort order of this column is
		var arrows = css.getElementsByClass(td, 'tableSortArrow', 'span');
		var previousSortOrder = '';
		if (arrows.length > 0) {
			previousSortOrder = arrows[0].getAttribute('sortOrder');
		}
		
		// work out how we want to sort this column using the data in the first cell
		// but just getting the first cell is no good if it contains no data
		// so if the first cell just contains white space then we need to track
		// down until we find a cell which does contain some actual data
		var itm = ''
		var rowNum = 0;
		while ('' == itm && rowNum < table.tBodies[0].rows.length) {
			itm = that.getInnerText(table.tBodies[0].rows[rowNum].cells[column]);
			rowNum++;
		}
		var sortfn = that.determineSortFunction(itm);

		// if the last column that was sorted was this one, then all we need to 
		// do is reverse the sorting on this column
		if (table.id == that.lastSortedTable && column == that.sortColumnIndex) {
			newRows = that.newRows;
			newRows.reverse();
		// otherwise, we have to do the full sort
		} else {
			that.sortColumnIndex = column;
			var newRows = new Array();

			for (var j = 0; j < table.tBodies[0].rows.length; j++) { 
				newRows[j] = table.tBodies[0].rows[j]; 
			}

			newRows.sort(sortfn);
		}

		that.moveRows(table, newRows);
		that.newRows = newRows;
		that.lastSortedTable = table.id;
		
		// now, give the user some feedback about which way the column is sorted
		
		// first, get rid of any arrows in any heading cells
		var arrows = css.getElementsByClass(tr, 'tableSortArrow', 'span');
		for (var j = 0; j < arrows.length; j++) {
			var arrowParent = arrows[j].parentNode;
			arrowParent.removeChild(arrows[j]);

			if (arrowParent != td) {
				spanEl = createElement('span');
				spanEl.className = 'tableSortArrow';
				spanEl.appendChild(document.createTextNode('\u00A0\u00A0'));
				arrowParent.appendChild(spanEl);
			}
		}
		
		// now, add back in some feedback 
		var spanEl = createElement('span');
		spanEl.className = 'tableSortArrow';
		if (null == previousSortOrder || '' == previousSortOrder || 'DESC' == previousSortOrder) {
			spanEl.appendChild(document.createTextNode(' \u2191'));
			spanEl.setAttribute('sortOrder', 'ASC');
		} else {
			spanEl.appendChild(document.createTextNode(' \u2193'));
			spanEl.setAttribute('sortOrder', 'DESC');
		}
		
		td.appendChild(spanEl);
		
		return false;
	},

	getInnerText : function(el) {
		
		if ('string' == typeof el || 'undefined' == typeof el) {
			return el;
		}
		
		if (el.innerText) {
			return el.innerText;  // Not needed but it is faster
		}

		var str = el.getAttribute('standardistaTableSortingInnerText');
		if (null != str && '' != str) {
			return str;
		}
		str = '';

		var cs = el.childNodes;
		var l = cs.length;
		for (var i = 0; i < l; i++) {
			// 'if' is considerably quicker than a 'switch' statement, 
			// in Internet Explorer which translates up to a good time 
			// reduction since this is a very often called recursive function
			if (1 == cs[i].nodeType) { // ELEMENT NODE
				str += this.getInnerText(cs[i]);
				break;
			} else if (3 == cs[i].nodeType) { //TEXT_NODE
				str += cs[i].nodeValue;
				break;
			}
		}
		
		// set the innertext for this element directly on the element
		// so that it can be retrieved early next time the innertext
		// is requested
		el.setAttribute('standardistaTableSortingInnerText', str);
		
		return str;
	},

	determineSortFunction : function(itm) {
		
		var sortfn = this.sortCaseInsensitive;
		
		if (itm.match(/^\d\d[\/-]\d\d[\/-]\d\d\d\d$/)) {
			sortfn = this.sortDate;
		}
		if (itm.match(/^\d\d[\/-]\d\d[\/-]\d\d$/)) {
			sortfn = this.sortDate;
		}
		if (itm.match(/^[�$]/)) {
			sortfn = this.sortCurrency;
		}
		if (itm.match(/^\d?\.?\d+$/)) {
			sortfn = this.sortNumeric;
		}
		if (itm.match(/^[+-]?\d*\.?\d+([eE]-?\d+)?$/)) {
			sortfn = this.sortNumeric;
		}
    		if (itm.match(/^([01]?\d\d?|2[0-4]\d|25[0-5])\.([01]?\d\d?|2[0-4]\d|25[0-5])\.([01]?\d\d?|2[0-4]\d|25[0-5])\.([01]?\d\d?|2[0-4]\d|25[0-5])$/)) {
        		sortfn = this.sortIP;
   		}

		return sortfn;
	},
	
	sortCaseInsensitive : function(a, b) {
		var that = standardistaTableSorting.that;
		
		var aa = that.getInnerText(a.cells[that.sortColumnIndex]).toLowerCase();
		var bb = that.getInnerText(b.cells[that.sortColumnIndex]).toLowerCase();
		if (aa==bb) {
			return 0;
		} else if (aa<bb) {
			return -1;
		} else {
			return 1;
		}
	},
	
	sortDate : function(a,b) {
		var that = standardistaTableSorting.that;

		// y2k notes: two digit years less than 50 are treated as 20XX, greater than 50 are treated as 19XX
		var aa = that.getInnerText(a.cells[that.sortColumnIndex]);
		var bb = that.getInnerText(b.cells[that.sortColumnIndex]);
		
		var dt1, dt2, yr = -1;
		
		if (aa.length == 10) {
			dt1 = aa.substr(6,4)+aa.substr(3,2)+aa.substr(0,2);
		} else {
			yr = aa.substr(6,2);
			if (parseInt(yr) < 50) { 
				yr = '20'+yr; 
			} else { 
				yr = '19'+yr; 
			}
			dt1 = yr+aa.substr(3,2)+aa.substr(0,2);
		}
		
		if (bb.length == 10) {
			dt2 = bb.substr(6,4)+bb.substr(3,2)+bb.substr(0,2);
		} else {
			yr = bb.substr(6,2);
			if (parseInt(yr) < 50) { 
				yr = '20'+yr; 
			} else { 
				yr = '19'+yr; 
			}
			dt2 = yr+bb.substr(3,2)+bb.substr(0,2);
		}
		
		if (dt1==dt2) {
			return 0;
		} else if (dt1<dt2) {
			return -1;
		}
		return 1;
	},

	sortCurrency : function(a,b) { 
		var that = standardistaTableSorting.that;

		var aa = that.getInnerText(a.cells[that.sortColumnIndex]).replace(/[^0-9.]/g,'');
		var bb = that.getInnerText(b.cells[that.sortColumnIndex]).replace(/[^0-9.]/g,'');
		return parseFloat(aa) - parseFloat(bb);
	},

	sortNumeric : function(a,b) { 
		var that = standardistaTableSorting.that;

		var aa = parseFloat(that.getInnerText(a.cells[that.sortColumnIndex]));
		if (isNaN(aa)) { 
			aa = 0;
		}
		var bb = parseFloat(that.getInnerText(b.cells[that.sortColumnIndex])); 
		if (isNaN(bb)) { 
			bb = 0;
		}
		return aa-bb;
	},

	makeStandardIPAddress : function(val) {
		var vals = val.split('.');

		for (x in vals) {
			val = vals[x];

			while (3 > val.length) {
				val = '0'+val;
			}
			vals[x] = val;
		}

		val = vals.join('.');

		return val;
	},

	sortIP : function(a,b) { 
		var that = standardistaTableSorting.that;

		var aa = that.makeStandardIPAddress(that.getInnerText(a.cells[that.sortColumnIndex]).toLowerCase());
		var bb = that.makeStandardIPAddress(that.getInnerText(b.cells[that.sortColumnIndex]).toLowerCase());
		if (aa==bb) {
			return 0;
		} else if (aa<bb) {
			return -1;
		} else {
			return 1;
		}
	},

	moveRows : function(table, newRows) {
		this.isOdd = false;

		// We appendChild rows that already exist to the tbody, so it moves them rather than creating new ones
		for (var i=0;i<newRows.length;i++) { 
			var rowItem = newRows[i];

			this.doStripe(rowItem);

			table.tBodies[0].appendChild(rowItem); 
		}
	},
	
	doStripe : function(rowItem) {
		if (this.isOdd) {
			css.addClassToElement(rowItem, 'odd');
		} else {
			css.removeClassFromElement(rowItem, 'odd');
		}
		
		this.isOdd = !this.isOdd;
	}

}

function standardistaTableSortingInit() {
	standardistaTableSorting.init();
}

/**
 * This array is used to remember mark status of rows in browse mode
 */
var marked_row = new Array;

/**
 * enables highlight and marking of rows in data tables
 *
 */
function PMA_markRowsInit() {
    // for every table row ...
	var rows = document.getElementsByTagName('tr');
	for ( var i = 0; i < rows.length; i++ ) {
	    // ... with the class 'odd' or 'even' ...
		//if ( 'row0' != rows[i].className && 'row1' != rows[i].className ) {
		//    continue;
		//}
	    // ... add event listeners ...
        // ... to highlight the row on mouseover ...
	    if ( navigator.appName == 'Microsoft Internet Explorer' ) {
	        // but only for IE, other browsers are handled by :hover in css
			rows[i].onmouseover = function() {
			    this.className += ' hover';
			}
			rows[i].onmouseout = function() {
			    this.className = this.className.replace( ' hover', '' );
			}
	    }
        // ... and to mark the row on click ...
		rows[i].onmousedown = function() {
		    var unique_id;
            if ( this.id.length > 0 ) {
                unique_id = this.id;
            } else {
		        return;
		    }
			//alert(unique_id);
			var map_id = document.getElementById("map_pane");
			map_id.style.zIndex = 150;
			map_id.style.display = "block";
			var mapping_id = document.getElementById("map_wrap");
			mapping_id.style.zIndex = 140;
			mapping_id.style.display = "block";
			if ((browser=="Microsoft Internet Explorer")	&& (version<=6))
			{
				if ( document.documentElement.scrollTop ) {
					mapping_id.style.top =  document.documentElement.scrollTop + 'px';
				} else {
					mapping_id.style.top =  document.body.scrollTop  + 'px';
				}
				mapping_id.style.position = "absolute";
			} else {
				mapping_id.style.position = "fixed";
			}
			var tds = this.getElementsByTagName('td');
			var lat;
			var lon;
			for ( var i = 0; i < tds.length; i++ ) {
				// ... with the class 'odd' or 'even' ...
				if ( 'lat' == tds[i].className) {
					lat = tds[i].childNodes[0].nodeValue;
				} else if ( 'lon' == tds[i].className ) {
					lon = tds[i].childNodes[0].nodeValue;
				}
			}
			map.panTo(new GLatLng(lat, lon));
            /*if ( typeof(marked_row[unique_id]) == 'undefined' || !marked_row[unique_id] ) {
                marked_row[unique_id] = true;
            } else {
                marked_row[unique_id] = false;
            }

            if ( marked_row[unique_id] ) {
			    this.className += ' marked';
            } else {
			    this.className = this.className.replace(' marked', '');
            }*/
		}

	}
}

function hide_gm() {
			var map_id = document.getElementById("map_pane");
			map_id.style.zIndex = -1;
			map_id.style.display = "none";
			var mapping_id = document.getElementById("map_wrap");
			mapping_id.style.zIndex = -2;
			mapping_id.style.display = "none";
}


/**
*
*  Crossbrowser Drag Handler
*  http://www.webtoolkit.info/
*
**/

var DragHandler = {


	// private property.
	_oElem : null,


	// public method. Attach drag handler to an element.
	attach : function(oElem) {
		oElem.onmousedown = DragHandler._dragBegin;

		// callbacks
		oElem.dragBegin = new Function();
		oElem.drag = new Function();
		oElem.dragEnd = new Function();

		return oElem;
	},


	// private method. Begin drag process.
	_dragBegin : function(e) {
		var oElem = DragHandler._oElem = this;

		if (isNaN(parseInt(oElem.style.left))) { oElem.style.left = '0px'; }
		if (isNaN(parseInt(oElem.style.top))) { oElem.style.top = '0px'; }

		var x = parseInt(oElem.style.left);
		var y = parseInt(oElem.style.top);

		e = e ? e : window.event;
		oElem.mouseX = e.clientX;
		oElem.mouseY = e.clientY;

		oElem.dragBegin(oElem, x, y);

		document.onmousemove = DragHandler._drag;
		document.onmouseup = DragHandler._dragEnd;
		return false;
	},


	// private method. Drag (move) element.
	_drag : function(e) {
		var oElem = DragHandler._oElem;

		var x = parseInt(oElem.style.left);
		var y = parseInt(oElem.style.top);

		e = e ? e : window.event;
		oElem.style.left = x + (e.clientX - oElem.mouseX) + 'px';
		oElem.style.top = y + (e.clientY - oElem.mouseY) + 'px';

		oElem.mouseX = e.clientX;
		oElem.mouseY = e.clientY;

		oElem.drag(oElem, x, y);

		return false;
	},


	// private method. Stop drag process.
	_dragEnd : function() {
		var oElem = DragHandler._oElem;

		var x = parseInt(oElem.style.left);
		var y = parseInt(oElem.style.top);

		oElem.dragEnd(oElem, x, y);

		document.onmousemove = null;
		document.onmouseup = null;
		DragHandler._oElem = null;
	}

}
