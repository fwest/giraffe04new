/* $Id: map_functions.js 430 2008-08-14 16:52:31Z klin $ */

var event = gup('event');
var version = gup('version');
var event_getVars;
if (event) {
	event_getVars = 'event=' + event
	if (version) {
		event_getVars = event_getVars + '&version=' + version;
	}
}

var browser=navigator.appName;
if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)){ //test for MSIE x.x;
 var version=new Number(RegExp.$1) // capture x.x portion and store as a number
}

var map;
//var centerLatitude = 37.7;
//var centerLongitude = -122.2;
var startZoom = 8;
var maxZoom = 12;
var sm_visible = 1;
var sm_rect;
var noMarkerLimit = 100;
var markerOn = false;
			var currentType = 'All';
			var allMarkers = {};
			var allTypes = {};
			
//Create the tile layer object
var detailLayer = new GTileLayer(new GCopyrightCollection(''));

//Method to retrieve the URL of the tile
detailLayer.getTileUrl = function(tile, zoom){
    //pass the x and y position as wel as the zoom
    var tileURL = "server.php?x="+tile.x+"&y="+tile.y+"&zoom="+zoom;
	//GLog.writeUrl(tileURL);
    return tileURL;
};

detailLayer.isPng = function() {
    //The example uses GIF's
    return true;
}

//add your tiles to the normal map projection
detailMapLayers = G_NORMAL_MAP.getTileLayers();
detailMapLayers.push(detailLayer);

//add your tiles to the satellite map projection
detailMapLayers = G_SATELLITE_MAP.getTileLayers();
detailMapLayers.push(detailLayer);

//add your tiles to the satellite map projection
detailMapLayers = G_HYBRID_MAP.getTileLayers();
detailMapLayers.push(detailLayer);


var deselectCurrent = function() {};

function initializePoint(pointData) {
	var point = new GLatLng(pointData.latitude, pointData.longitude);
	if (pointData.icon == 'true') { 
	var icon = new GIcon();
	if (pointData.im) {
		icon.image = "images/" + pointData.type + pointData.damage + ".png";
	} else {
		icon.image = "images/" + pointData.type + ".png";
	}
	icon.shadow = "images/shadow\-" + pointData.type + ".png";
	icon.iconSize = new GSize(25, 25);
	icon.shadowSize = new GSize(38, 25);
	icon.iconAnchor = new GPoint(12, 12);
	icon.infoWindowAnchor = new GPoint(12, 12);
	//GLog.writeUrl(icon.image);
	var marker = new GMarker(point, icon);
	} else {
	var marker = new GMarker(point);
	}
	var listItem = document.createElement('li');
	var listItemLink = listItem.appendChild(document.createElement('a'));
	var visible = false;
	/*var description = "<div  id=\"info\"><span class=\"emphmed\">"+ pointData.name +"</span><br/><br/>" + 
		"<span class=\"genmed\">Location: <strong>" + pointData.latitude + "</strong>, " + 
		"<strong>" + pointData.longitude + "</strong></span></div>";
	if (pointData.im) {
		description = description + "<div  id=\"info\">"; 
		for (var metric in pointData.im) {
		description = description + "<span class=\"genmed\">" + metric + ": <strong>" + 
		pointData.im[metric] + "</strong></span><br/><br/>";
		}
		description = description + "</div>"; 
	}
	listItemLink.href = "#"+pointData.fac_id;
	listItemLink.innerHTML = '<strong>' + pointData.name + ' </strong><span>' 
		+ pointData.type  + '</span>';
	*/
		var description = "<table border=1 cellspacing=0 cellpadding=0><tr><td>" + 
		"<table><tr bgcolor=#cccccc><td colspan=2><strong> " + 
		pointData.name + "</strong></td></tr>" + 
		"<tr><td><font size=-1>Lat: " + pointData.latitude + "</td><td><font size=-1>Lon: " + 
		pointData.longitude + "</td></tr></table></td></tr>";
	if (pointData.im) {
		description = description + "<tr><td colspan=2><table width=100%>"; 
		for (var metric in pointData.im) {
		description = description + "<tr bgcolor=#eeeeee><td>" + metric + ": </td><td><strong>" + 
		pointData.im[metric] + "</strong></td></tr>";
		}
		description = description + "</table></td></tr>"; 
	}
	description = description + "</table>"; 
	listItemLink.href = "#"+pointData.fac_id;
	listItemLink.innerHTML = '<strong>' + pointData.name + ' </strong><span>' 
		+ pointData.type  + '</span>';
	

	
	if (pointData.im) {
		listItem.className = pointData.damage;
	}
	
	var focusPoint = function() {
		deselectCurrent();
		listItem.className = 'current';
		deselectCurrent = function() { 
			if (pointData.im) {
				listItem.className = pointData.damage;
			} else {
				listItem.className = ''; 
			}
		}
		//marker.openInfoWindowHtml(pointData.name);
		marker.openInfoWindowHtml(description);
		map.panTo(point);
		return false;
	}

	GEvent.addListener(marker, 'click', focusPoint);	
	listItemLink.onclick = focusPoint;

	pointData.show = function() {
		if (!visible) {
			document.getElementById('sidebar-list').appendChild(listItem);
			map.addOverlay(marker);
			visible = true;
		}
	}
	pointData.hide = function() {
		if (visible) {
			document.getElementById('sidebar-list').removeChild(listItem);
			map.removeOverlay(marker);
			visible = false;
		}
	}
	pointData.visible = function() {
		return visible;
	}

	pointData.point = function() {
		return point;
	}

	//pointData.show();
}

function initializeItem(pointData) {
	var point = new GLatLng(pointData.latitude, pointData.longitude);
	var listItem = document.createElement('li');
	var listItemLink = listItem.appendChild(document.createElement('a'));
	var visible = false;
	/*var description = "<div  id=\"info\"><span class=\"emphmed\">"+ pointData.name +"</span><br/><br/>" + 
		"<span class=\"genmed\">Location: <strong>" + pointData.latitude + "</strong>, " + 
		"<strong>" + pointData.longitude + "</strong></span></div>";
	if (pointData.im) {
		description = description + "<div  id=\"info\">"; 
		for (var metric in pointData.im) {
		description = description + "<span class=\"genmed\">" + metric + ": <strong>" + 
		pointData.im[metric] + "</strong></span><br/><br/>";
		}
		description = description + "</div>"; 
	}
	
	listItemLink.href = "#"+pointData.fac_id;
	listItemLink.innerHTML = listItemLink.innerHTML + '<strong>' + pointData.name + ' </strong>';
	listItemLink.innerHTML = listItemLink.innerHTML + '<span>' 	+ pointData.type  + '</span>';
	*/
	var description = "<table border=1 cellspacing=0 cellpadding=0><tr><td>" + 
		"<table><tr bgcolor=#cccccc><td colspan=2><strong> " + 
		pointData.name + "</strong></td></tr>" + 
		"<tr><td><font size=-1>Lat: " + pointData.latitude + "</td><td><font size=-1>Lon: " + 
		pointData.longitude + "</td></tr></table></td></tr>";
	if (pointData.im) {
		description = description + "<tr><td colspan=2><table width=100%>"; 
		for (var metric in pointData.im) {
		description = description + "<tr bgcolor=#eeeeee><td>" + metric + ": </td><td><strong>" + 
		pointData.im[metric] + "</strong></td></tr>";
		}
		description = description + "</table></td></tr>"; 
	}
	description = description + "</table>"; 
	listItemLink.href = "#"+pointData.fac_id;
	listItemLink.innerHTML = '<strong>' + pointData.name + ' </strong><span>' 
		+ pointData.type  + '</span>';
	
	if (pointData.im) {
		listItem.className = pointData.damage;
	}
	
	var focusPoint = function() {
		deselectCurrent();
		listItem.className = 'current';
		deselectCurrent = function() { 
			if (pointData.im) {
				listItem.className = pointData.damage;
			} else {
				listItem.className = ''; 
			}
		}
		//marker.openInfoWindowHtml(pointData.name);
		//marker.openInfoWindowHtml(description);
		map.panTo(point);
		return false;
	}

	//GEvent.addListener(marker, 'click', focusPoint);	
	listItemLink.onclick = focusPoint;

	pointData.show = function() {
		if (!visible) {
			document.getElementById('sidebar-list').appendChild(listItem);
			//map.addOverlay(marker);
			visible = true;
		}
	}
	pointData.hide = function() {
		if (visible) {
			document.getElementById('sidebar-list').removeChild(listItem);
			//map.removeOverlay(marker);
			visible = false;
		}
	}
	pointData.visible = function() {
		return visible;
	}

	pointData.point = function() {
		return point;
	}

	//pointData.show();
}

function initializeSortTab(tab_item) {
	var listItem = document.createElement('li');
	var listItemLink = listItem.appendChild(document.createElement('a'));
	var visible = false;
	
	listItemLink.href = "#";
	var image_url = "images/" + tab_item.type + ".png";
	listItemLink.innerHTML = '<img src=\"' + image_url + '\" border=0 valign=\"middle\">' + tab_item.type;
	listItemLink.onclick = function() {
		changeBodyClass('standby', 'loading');
		currentType = tab_item.type;
		refreshMarkers();
		changeBodyClass('loading', 'standby');
		return false;
	}

	tab_item.show = function() {
		if (!visible) {
			document.getElementById('filters').appendChild(listItem);
			visible = true;
		}
	}
	tab_item.hide = function() {
		if (visible) {
			document.getElementById('filters').removeChild(listItem);
			visible = false;
		}
	}
	
	document.getElementById('filters').appendChild(listItem);
	visible = true;

}


function refreshMarkers() {
		var map_bound = map.getBounds();
		for(id in allMarkers) {
			if ((allMarkers[id].type == currentType || 'All' == currentType) &&
				map_bound.contains(allMarkers[id].point()))
				allMarkers[id].show();
			else
				allMarkers[id].hide();	
		}
}

function refreshTabs(newTypes) {
	for(var type in allTypes) {
		if (!newTypes[type] && 'All' != type) {
			allTypes[type].hide();
		} else {
			allTypes[type].show();
		}
	}
}

function windowHeight() {
	// Standard browsers (Mozilla, Safari, etc.)
	if (self.innerHeight)
		return self.innerHeight;
	// IE 6
	if (document.documentElement && document.documentElement.clientHeight)
		return document.documentElement.clientHeight;
	// IE 5
	if (document.body)
		return document.body.clientHeight;
	// Just in case.
	return 0;
}

function handleResize() {
	var height = windowHeight() - document.getElementById('toolbar').offsetHeight - 100;
	document.getElementById('map_pane').style.height = height+60 + 'px';
	document.getElementById('map').style.height = height + 'px';
	document.getElementById('sidebar').style.height = height + 'px';
}

function changeBodyClass(from, to) {
     document.body.className = document.body.className.replace(from, to);
     return false;
}

function init() {
	//document.getElementById('button-sidebar-hide').onclick = function() { return changeBodyClass('sidebar-right', 'nosidebar'); };
	//document.getElementById('button-sidebar-show').onclick = function() { return changeBodyClass('nosidebar', 'sidebar-right'); };
	handleResize();

    map = new GMap2(document.getElementById("map"));
    map.addControl(new GLargeMapControl());
    map.addControl(new GMapTypeControl());
    map.setCenter(new GLatLng(centerLatitude, centerLongitude), startZoom);
    //map.setMapType(G_NORMAL_MAP);
    map.setMapType(G_HYBRID_MAP);
	//map.enableContinuousZoom();

	//Add shakemap 
	initializeShakeMap();


	//updateMarkers();
	/*GEvent.addListener(map,'zoomend',function() {
		changeBodyClass('standby', 'loading');
		var zoomed = map.getZoom();
		if (zoomed < 10) {
			if (markerOn == true) {
				markerOn = false;
				for (var cnt in allMarkers) {
					allMarkers[cnt].hide();
					delete allMarkers[cnt];
				}
				map.closeInfoWindow();
			}
			//map.clearOverlays();
			//for(id in allMarkers) {
			//	allMarkers[id].hide();	
			//}
			//var filters_id = document.getElementById('toolbar');
			//var filters_id = document.getElementById('filters');
			//filters_id.style.display = "none";
			//filters_id.style.display = "block";
			//map.closeInfoWindow();
			updatelistItem();
		} else {
			if (markerOn == false) {
				markerOn = true;
				for (var cnt in allMarkers) {
					allMarkers[cnt].hide();
					delete allMarkers[cnt];
				}
			}
			
			var bounds = map.getBounds();
			var southWest = bounds.getSouthWest();
			var northEast = bounds.getNorthEast();
			var getVars = 'ne=' + northEast.toUrlValue()
				+ '&sw=' + southWest.toUrlValue()+ '&zoom=' + zoomed
			if (event_getVars) {
				getVars = getVars + '&' + event_getVars;
			}
			//log the URL for testing
			//GLog.writeUrl('map_xml.php?'+getVars);
			//retrieve the points using Ajax
			var request_count = GXmlHttp.create();
			request_count.open('GET', 'map_xml.php?'+getVars, true);
			request_count.onreadystatechange = function() {
				if (request_count.readyState == 4) {
					var xml = request_count.responseXML;
					//var count = xml.documentElement.getElementsByTagName("count").item(0).firstChild.nodeValue;
					//GLog.write(count+":"+zoom);
					var total_count = parseInt(xml.documentElement.getElementsByTagName("count").item(0).firstChild.nodeValue);
					if (total_count > noMarkerLimit) {
						if (markerOn == true) {
							markerOn = false;
							for (var cnt in allMarkers) {
								allMarkers[cnt].hide();
								delete allMarkers[cnt];
							}
							map.closeInfoWindow();
						}
						updatelistItem();
						alert("Facility Markers Not Displayed Due to Large Number!");
					} else {
						if (markerOn == false) {
							markerOn = true;
							for (var cnt in allMarkers) {
								allMarkers[cnt].hide();
								delete allMarkers[cnt];
							}
						}
						updateMarkers();
					}
				}
			}
			request_count.send(null);
			//var filters_id = document.getElementById('toolbar');
			//var filters_id = document.getElementById('filters');
			//filters_id.style.display = "block";
		}
		changeBodyClass('loading', 'standby');
	});
	*/
	
	GEvent.addListener(map,'moveend',function() {
		changeBodyClass('standby', 'loading');
		var zoomed = map.getZoom();
		if ((browser=="Microsoft Internet Explorer"))
		{
			if ( zoomed > maxZoom ) {
				map.removeOverlay(sm_rect); sm_visible = 0;
			} else if (zoomed <= maxZoom ) {
				map.removeOverlay(sm_rect); sm_rect.redraw(); map.addOverlay(sm_rect); sm_visible = 1;
			}
		}
		if (zoomed < 10) {
			if (markerOn == true) {
				markerOn = false;
				for (var cnt in allMarkers) {
					allMarkers[cnt].hide();
					delete allMarkers[cnt];
				}
				map.closeInfoWindow();
			}
			//map.clearOverlays();
			//for(id in allMarkers) {
			//	allMarkers[id].hide();	
			//}
			//var filters_id = document.getElementById('toolbar');
			//var filters_id = document.getElementById('filters');
			//filters_id.style.display = "none";
			//filters_id.style.display = "block";
			//map.closeInfoWindow();
			updatelistItem();
		} else {
			var bounds = map.getBounds();
			var southWest = bounds.getSouthWest();
			var northEast = bounds.getNorthEast();
			var getVars = 'ne=' + northEast.toUrlValue()
				+ '&sw=' + southWest.toUrlValue()+ '&zoom=' + zoomed
			if (event_getVars) {
				getVars = getVars + '&' + event_getVars;
			}
			//log the URL for testing
			//GLog.writeUrl('map_xml.php?'+getVars);
			//retrieve the points using Ajax
			var request_count = GXmlHttp.create();
			request_count.open('GET', 'map_xml.php?'+getVars, true);
			request_count.onreadystatechange = function() {
				if (request_count.readyState == 4) {
					var xml = request_count.responseXML;
					//var count = xml.documentElement.getElementsByTagName("count").item(0).firstChild.nodeValue;
					//GLog.write(count+":"+zoom);
					var total_count = parseInt(xml.documentElement.getElementsByTagName("count").item(0).firstChild.nodeValue);
					if (total_count > noMarkerLimit) {
						if (markerOn == true) {
							markerOn = false;
							for (var cnt in allMarkers) {
								allMarkers[cnt].hide();
								delete allMarkers[cnt];
							}
							map.closeInfoWindow();
						}
						updatelistItem();
						//alert("Facility Markers Not Displayed Due to Large Number!");
					} else {
						if (markerOn == false) {
							markerOn = true;
							for (var cnt in allMarkers) {
								allMarkers[cnt].hide();
								delete allMarkers[cnt];
							}
						}
						updateMarkers();
					}
				}
			}
			request_count.send(null);
			//var filters_id = document.getElementById('toolbar');
			//var filters_id = document.getElementById('filters');
			//filters_id.style.display = "block";
		}
		changeBodyClass('loading', 'standby');
	});

	/*GEvent.addListener(map,'mousemove',function(latlng) {
		var pixelLocation = map.fromLatLngToDivPixel(latlng);
		GLog.write('ll:' + latlng + 'at:' + pixelLocation);
	});*/

	changeBodyClass('loading', 'standby');
	//GLog.write('data loaded');
}

function updatelistItem() {
	
	//remove the existing points
	//map.clearOverlays();
	//create the boundary for the data
	var bounds = map.getBounds();
	var southWest = bounds.getSouthWest();
	var northEast = bounds.getNorthEast();
	var zoom = map.getZoom();
	var getVars = 'ne=' + northEast.toUrlValue()
		+ '&sw=' + southWest.toUrlValue()+ '&zoom=' + zoom
	if (event_getVars) {
		getVars = getVars + '&' + event_getVars;
	}
	//log the URL for testing
	//GLog.writeUrl('map_xml.php?'+getVars);
	//retrieve the points using Ajax
	var request = GXmlHttp.create();
	request.open('GET', 'map_xml.php?'+getVars, true);
	request.onreadystatechange = function() {
		if (request.readyState == 4) {
			var xml = request.responseXML;
			//var count = xml.documentElement.getElementsByTagName("count").item(0).firstChild.nodeValue;
			//GLog.write(count+":"+zoom);
			var facTypes = { 'All':{} };
			var facilities = xml.documentElement.getElementsByTagName("marker");
			//GLog.write(facilities.length);
			for(var id = 0; id < facilities.length; id++) {
				var key = 'f' + facilities[id].getAttribute("fac_id");
				if (!allMarkers[key]) {
					var marker = {};
					marker.fac_id = facilities[id].getAttribute("fac_id");
					marker.name = facilities[id].getAttribute("name");
					marker.type = facilities[id].getAttribute("type");
					marker.icon = facilities[id].getAttribute("icon");
					marker.latitude = parseFloat(facilities[id].getAttribute("latitude"));
					marker.longitude = parseFloat(facilities[id].getAttribute("longitude"));
					if (event) {
						var im = facilities[id].getElementsByTagName("metric");
						var metric = {};
						for (var m_id = 0; m_id < im.length; m_id++) {
							metric[im[m_id].getAttribute("unit")] =
								parseFloat(im[m_id].getAttribute("value"));
						}
						marker.im = metric;
						var damage = facilities[id].getElementsByTagName("damage");
						marker.damage = damage[0].getAttribute("value");
					}
					initializeItem(marker);
					allMarkers[key] = marker;
				}
				facTypes[facilities[id].getAttribute("type")] = true;
			}

			for(var type in facTypes) {
				if (!allTypes[type]) {
					var tab_item = {};
					tab_item['type'] = type;
					initializeSortTab(tab_item);
					allTypes[type] = tab_item;
				}
			}
			
			refreshMarkers();
			refreshTabs(facTypes);
			var marker_cnt = 0;
			for (var cnt in allMarkers) {
				if (!bounds.contains(new GLatLng(allMarkers[cnt].latitude, allMarkers[cnt].longitude))) {
					allMarkers[cnt].hide();
					delete allMarkers[cnt];
				} else {
				marker_cnt++;
				}
			}
			//GLog.write(marker_cnt);
		}
	}
	request.send(null);

}

function updateMarkers() {
	
	//remove the existing points
	//map.clearOverlays();
	//create the boundary for the data
	var bounds = map.getBounds();
	var southWest = bounds.getSouthWest();
	var northEast = bounds.getNorthEast();
	var zoom = map.getZoom();
	var getVars = 'ne=' + northEast.toUrlValue()
		+ '&sw=' + southWest.toUrlValue()+ '&zoom=' + zoom
	if (event_getVars) {
		getVars = getVars + '&' + event_getVars;
	}
	//log the URL for testing
	
	if (zoom >= 10 ) {
	//GLog.writeUrl('map_xml.php?'+getVars);
	//retrieve the points using Ajax
	var request = GXmlHttp.create();
	request.open('GET', 'map_xml.php?'+getVars, true);
	request.onreadystatechange = function() {
		if (request.readyState == 4) {
			var xml = request.responseXML;
			//var count = xml.documentElement.getElementsByTagName("count").item(0).firstChild.nodeValue;
			//GLog.write(count+":"+zoom);
			var facTypes = { 'All':{} };
			var facilities = xml.documentElement.getElementsByTagName("marker");
			//GLog.write(facilities.length);
			for(var id = 0; id < facilities.length; id++) {
				var key = 'f' + facilities[id].getAttribute("fac_id");
				if (!allMarkers[key]) {
					var marker = {};
					marker.fac_id = facilities[id].getAttribute("fac_id");
					marker.name = facilities[id].getAttribute("name");
					marker.type = facilities[id].getAttribute("type");
					marker.icon = facilities[id].getAttribute("icon");
					marker.latitude = parseFloat(facilities[id].getAttribute("latitude"));
					marker.longitude = parseFloat(facilities[id].getAttribute("longitude"));
					if (event) {
						var im = facilities[id].getElementsByTagName("metric");
						var metric = {};
						for (var m_id = 0; m_id < im.length; m_id++) {
							metric[im[m_id].getAttribute("unit")] =
								parseFloat(im[m_id].getAttribute("value"));
						}
						marker.im = metric;
						var damage = facilities[id].getElementsByTagName("damage");
						marker.damage = damage[0].getAttribute("value");
					}
					initializePoint(marker);
					allMarkers[key] = marker;
				}
				facTypes[facilities[id].getAttribute("type")] = true;
			}

			for(var type in facTypes) {
				if (!allTypes[type]) {
					var tab_item = {};
					tab_item['type'] = type;
					initializeSortTab(tab_item);
					allTypes[type] = tab_item;
				}
			}
			
			refreshMarkers();
			refreshTabs(facTypes);
			var marker_cnt = 0;
			for (var cnt in allMarkers) {
				if (!bounds.contains(new GLatLng(allMarkers[cnt].latitude, allMarkers[cnt].longitude))) {
					allMarkers[cnt].hide();
					delete allMarkers[cnt];
				} else {
					marker_cnt++;
				}
			}
			//GLog.write(marker_cnt);
		}
		}
	request.send(null);
	}

}

function initializeShakeMap() {
	//remove the existing points
	//map.clearOverlays();
	//create the boundary for the data
	if (!event) {
		return "";
	}
	//log the URL for testing
	//GLog.writeUrl('map_data.php?'+getVars);
	//retrieve the points using Ajax
	var request = GXmlHttp.create();
	request.open('GET', 'shakemap_xml.php?'+event_getVars, true);
	//GLog.writeUrl('shakemap_xml.php?'+event_getVars);
	request.onreadystatechange = function() {
		if (request.readyState == 4) {
			var xml = request.responseXML;
			var shakecast_id = xml.documentElement.getElementsByTagName("shakecast_id").item(0).firstChild.nodeValue;
		if (shakecast_id != '') {
			var LatLonBox = xml.documentElement.getElementsByTagName("LatLonBox").item(0);
			var lat_min = parseFloat(LatLonBox.getAttribute("south"));
			var lat_max = parseFloat(LatLonBox.getAttribute("north"));
			var lon_min = parseFloat(LatLonBox.getAttribute("west"));
			var lon_max = parseFloat(LatLonBox.getAttribute("east"));
			//for(id in shakemap_bounds) {
				var rectBounds = new GLatLngBounds(
					new GLatLng(lat_min, lon_min), 
					new GLatLng(lat_max, lon_max));
				//var img = document.createElement('img');
				var img = '/data/' + shakecast_id + '/ii_overlay.png';
				sm_rect = new Rectangle(rectBounds, img);
				map.addOverlay(sm_rect);
				map.panTo(new GLatLng((lat_min + lat_max)/2,
						(lon_min + lon_max)/2));
				//createMarker(point);
			var event = xml.documentElement.getElementsByTagName("event").item(0);
			var lat = parseFloat(event.getAttribute("lat"));
			var lon = parseFloat(event.getAttribute("lon"));
			var point = new GLatLng(lat, lon);
			var icon = new GIcon();
			icon.image = "images/epicenter.png";
			icon.shadow = "images/shadow\-epicenter.png";
			icon.iconSize = new GSize(25, 25);
			icon.shadowSize = new GSize(38, 25);
			icon.iconAnchor = new GPoint(12, 12);
			icon.infoWindowAnchor = new GPoint(12, 12);
			//GLog.writeUrl(icon.image);
			var description = "<div  id=\"info\"><span class=\"emphmed\">"+event.getAttribute("locstring")+"</span></div><br/><br/>" + 
				"<div  id=\"info\"><span class=\"genmed\">Event ID: <strong>" + event.getAttribute("id") + "</strong></span><br/><br/>" + 
				"<span class=\"genmed\">Magnitude: <strong>" + event.getAttribute("magnitude") + "</strong></span><br/><br/>" + 
				"<span class=\"genmed\">Location (Lat, Lon): <strong>" + event.getAttribute("lat") + "</strong>, " + 
				"<strong>" + event.getAttribute("lon") + "</strong></span><br/><br/>" + 
				"<span class=\"genmed\">Time: <strong>" + event.getAttribute("timestamp") + "</strong></span></div>";
			var marker = new GMarker(point, icon);
			GEvent.addListener(marker, "click", function(){
				marker.openInfoWindowHtml(description);
			});	
			//GEvent.addListener(marker, "dblclick", function(){
			//	window.location = "googlemaps.php?event=lastevent";
			//});	
			map.addOverlay(marker);
					//}
		}
		}
	}

	request.send(null);
}


function gup( name ) {
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var tmpURL = window.location.href;
  var results = regex.exec( tmpURL );
  if( results == null )
    return "";
  else
    return results[1];
}

function handleWheel(delta) {
	if (delta < 0)
		map.zoomOut();
	else 
		map.zoomIn();
}

function wheel(event){
	var delta = 0;
	if (!event) event = window.event;
	if (event.wheelDelta) {
		delta = event.wheelDelta/120; 
		if (window.opera) delta = -delta;
	} else if (event.detail) {
		delta = -event.detail/3;
	}
	if (delta)
		handleWheel(delta);
        if (event.preventDefault)
                event.preventDefault();
        event.returnValue = false;
}

/* Initialization code. */
if (document.attachEvent)
	document.attachEvent("onmousewheel", wheel);
else
	window.addEventListener('DOMMouseScroll', wheel, false);

window.onmousewheel = document.onmousewheel = wheel;
//window.onresize = handleResize;
window.onload = init;