<?php
/*
# $Id: shakemap_xml.php 138 2007-08-23 21:49:39Z klin $

# The PHP interface was originally inspired by PHPBB,
# file/database formats and directory structure are quite similar.

##############################################################################
# 
# Terms and Conditions of Software Use
# ====================================
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Disclaimer of Earthquake Information
# ====================================
# 
# The data and maps provided through this system are preliminary data
# and are subject to revision. They are computer generated and may not
# have received human review or official approval. Inaccuracies in the
# data may be present because of instrument or computer
# malfunctions. Subsequent review may result in significant revisions to
# the data. All efforts have been made to provide accurate information,
# but reliance on, or interpretation of earthquake data from a single
# source is not advised. Data users are cautioned to consider carefully
# the provisional nature of the information before using it for
# decisions that concern personal or public safety or the conduct of
# business that involves substantial monetary or operational
# consequences.
# 
# Disclaimer of Software and its Capabilities
# ===========================================
# 
# This software is provided as an "as is" basis.  Attempts have been
# made to rid the program of software defects and bugs, however the
# U.S. Geological Survey (USGS) have no obligations to provide maintenance, 
# support, updates, enhancements or modifications. In no event shall USGS 
# be liable to any party for direct, indirect, special, incidental or 
# consequential damages, including lost profits, arising out of the use 
# of this software, its documentation, or data obtained though the use 
# of this software, even if USGS or have been advised of the
# possibility of such damage. By downloading, installing or using this
# program, the user acknowledges and understands the purpose and
# limitations of this software.
# 
# Contact Information
# ===================
# 
# Coordination of this effort is under the auspices of the USGS Advanced
# National Seismic System (ANSS) coordinated in Golden, Colorado, which
# functions as the clearing house for development, distribution,
# documentation, and support. For questions, comments, or reports of
# potential bugs regarding this software please contact wald@usgs.gov or
# klin@usgs.gov.  
#
#############################################################################
*/

define('IN_SC', true);
$sc_root_path = './';
//include($sc_root_path . 'includes/begin_caching.php');
include($sc_root_path . 'extension.inc');
include($sc_root_path . 'common.'.$phpEx);


//retrieve the variables from the GET vars
if (isset($_GET['event'])) {
	$evid = $_GET['event'];
}
if (isset($_GET['version'])) {
	$version = $_GET['version'];
}

//return unless isset($evid);


if ($evid == 'lastevent') {
	//$result = mysql_query(
	$sql = "SELECT shakemap_id as evid, shakemap_version as version
		FROM grid 
		ORDER BY grid_id DESC LIMIT 1";
	if ( !($result = $db->sql_query($sql)) )
	{
		message_die(GENERAL_ERROR, 'Could not query ShakeMap event information', '', __LINE__, __FILE__, $sql);
	}
	if ($row = $db->sql_fetchrow($result))
	{
		$evid = $row['evid'];
		$version = $row['version'];
	}
	$db->sql_freeresult($result);
}
	
if(isset($version)) {
	//retrieve all points in the southwest/northeast boundary
	//$result = mysql_query(
	$sql = "SELECT s.lat_min, s.lat_max, s.lon_min, s.lon_max, s.shakemap_version,
		e.event_location_description, e.event_timestamp, e.magnitude, e.lat, e.lon
	FROM 
		(shakemap s INNER JOIN event e on
			s.shakemap_id = e.event_id)
	WHERE
		(s.shakemap_id = '$evid' AND s.shakemap_version = $version)
	ORDER BY
		s.shakemap_version DESC LIMIT 1";

} elseif (isset($evid)) {
	//retrieve all points in the southwest/northeast boundary
	//split over the meridian
	$sql = "SELECT s.lat_min, s.lat_max, s.lon_min, s.lon_max, s.shakemap_version,
		e.event_location_description, e.event_timestamp, e.magnitude, e.lat, e.lon
	FROM 
		(shakemap s INNER JOIN event e on
			s.shakemap_id = e.event_id)
	WHERE
		(s.shakemap_id = '$evid')
	ORDER BY
		s.shakemap_version DESC LIMIT 1";
}

$list = array();
$i=0;

if ( !($result = $db->sql_query($sql)) )
{
	message_die(GENERAL_ERROR, 'Could not query ShakeMap information', '', __LINE__, __FILE__, $sql);
}
while ($row = $db->sql_fetchrow($result))
{
	$i++;
	$list[] = "
	<shakecast_id>".$evid."-".$row['shakemap_version']."</shakecast_id>
	<LatLonBox 
		north=\"".$row['lat_max']."\" south=\"".$row['lat_min']."\"
		east=\"".$row['lon_max']."\" west=\"".$row['lon_min']."\"
	/>
	<event 
		id=\"$evid\" locstring=\"".htmlspecialchars($row['event_location_description'])."\" 
		timestamp=\"".$row['event_timestamp']."\" lat=\"".$row['lat']."\" lon=\"".$row['lon']."\" magnitude=\"".$row['magnitude']."\"
	/>";
}
//echo back the JavaScript object nicely formatted
header('content-type:text/xml;');
echo '<shakecast>';
echo join("\n\t",$list)."\n";
echo '</shakecast>';
