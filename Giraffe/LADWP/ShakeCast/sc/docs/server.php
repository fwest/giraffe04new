<?php
/*
# $Id: server.php 138 2007-08-23 21:49:39Z klin $

# The PHP interface was originally inspired by PHPBB,
# file/database formats and directory structure are quite similar.

##############################################################################
# 
# Terms and Conditions of Software Use
# ====================================
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Disclaimer of Earthquake Information
# ====================================
# 
# The data and maps provided through this system are preliminary data
# and are subject to revision. They are computer generated and may not
# have received human review or official approval. Inaccuracies in the
# data may be present because of instrument or computer
# malfunctions. Subsequent review may result in significant revisions to
# the data. All efforts have been made to provide accurate information,
# but reliance on, or interpretation of earthquake data from a single
# source is not advised. Data users are cautioned to consider carefully
# the provisional nature of the information before using it for
# decisions that concern personal or public safety or the conduct of
# business that involves substantial monetary or operational
# consequences.
# 
# Disclaimer of Software and its Capabilities
# ===========================================
# 
# This software is provided as an "as is" basis.  Attempts have been
# made to rid the program of software defects and bugs, however the
# U.S. Geological Survey (USGS) have no obligations to provide maintenance, 
# support, updates, enhancements or modifications. In no event shall USGS 
# be liable to any party for direct, indirect, special, incidental or 
# consequential damages, including lost profits, arising out of the use 
# of this software, its documentation, or data obtained though the use 
# of this software, even if USGS or have been advised of the
# possibility of such damage. By downloading, installing or using this
# program, the user acknowledges and understands the purpose and
# limitations of this software.
# 
# Contact Information
# ===================
# 
# Coordination of this effort is under the auspices of the USGS Advanced
# National Seismic System (ANSS) coordinated in Golden, Colorado, which
# functions as the clearing house for development, distribution,
# documentation, and support. For questions, comments, or reports of
# potential bugs regarding this software please contact wald@usgs.gov or
# klin@usgs.gov.  
#
#############################################################################
*/

define('IN_SC', true);
$sc_root_path = './';
//include($sc_root_path . 'includes/begin_caching.php');
include($sc_root_path . 'extension.inc');
include($sc_root_path . 'common.'.$phpEx);

//include the helper calculations
require($sc_root_path . 'GoogleMapUtility.'.$phpEx);

//this script may require additional memory and time
set_time_limit(0);
ini_set('memory_limit',8388608*10);

//create an array of the size for each marker at each zoom level
$markerSizes = array(1,1,1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,10,10,11,11,12,12);

//get the lat/lng bounds of this tile from the utility function
//return abounds object with width,height,x,y
$rect = GoogleMapUtility::getTileRect(
	(int)$_GET['x'],
	(int)$_GET['y'],
	(int)$_GET['zoom']
);

//create aunique file name for this tile
//if ($_GET['zoom'] >= 10 && $_GET['count'] < 100) {
//if ($_GET['zoom'] >= 10 ) {
//	$file = 'tiles/null.png';
//} else {
	$file = 'tiles/c'.md5(
		serialize($markerSizes).
		serialize($rect).'|'.
		$_GET['x'].'|'.
		$_GET['y'].'|'.
		$_GET['zoom']).'.png';
//}

header('content-type:image/png;');
//check if the file already exists
if(!file_exists($file)) {

	//create anew image
	$im = imagecreate(GoogleMapUtility::TILE_SIZE,GoogleMapUtility::TILE_SIZE);
	$trans = imagecolorallocate($im,0,0,255);
	imagefill($im,0,0,$trans);
	imagecolortransparent($im, $trans);
	$black = imagecolorallocate($im,0,0,0);
	$white = imagecolorallocate($im,255,255,255);

	//set up some colors for the markers.
	//each marker will have acolor based on the height of the tower
	$darkRed = imagecolorallocate($im,150,0,0);
	$red = imagecolorallocate($im,250,0,0);
	$darkGreen = imagecolorallocate($im,0,150,0);
	$green = imagecolorallocate($im,0,250,0);
	$darkBlue = imagecolorallocate($im,0,0,150);
	$blue = imagecolorallocate($im,0,0,250);
	$orange = imagecolorallocate($im,250,150,0);

	//init some vars
	$extend = 0;
	$z = (int)$_GET['zoom'];
	$swlat=$rect->y + $extend;
	$swlng=$rect->x+ $extend;
	$nelat=$swlat+$rect->height + $extend;
	$nelng=$swlng+$rect->width + $extend;

	//using joins...
	$sql = "SELECT
			lat_min ,lon_min ,facility_name
		FROM
			facility
		WHERE
			(lon_min > $swlng AND lon_min < $nelng)
		AND (lat_min <= $nelat AND lat_min >= $swlat)
			ORDER BY
		lat_min";
		
	if ( !($result = $db->sql_query($sql)) )
	{
		message_die(GENERAL_ERROR, 'Could not query facility information', '', __LINE__, __FILE__, $sql);
	}
	$row = $db->sql_fetchrowset($result);
	$db->sql_freeresult($result);

	//get the number of points in this tile
	$count = count($row);
	$filled=array();
	if($count>0) {
		for($i = 0;$i < count($row);  $i++)
		{
			//get the x,y coordinate of the marker in the tile
			$point = GoogleMapUtility::getPixelOffsetInTile($row[$i]['lat_min'],$row[$i]['lon_min'],$z);

			//check if the marker was already drawn there
			$filled["{$point->x},{$point->y}"] = (isset($filled["{$point->x},{$point->y}"])) ?
				$filled["{$point->x},{$point->y}"] : 0;
				
			if($filled["{$point->x},{$point->y}"]<2) {

				$c = $blue;
				//imagefilledrectangle($im, $point->x, $point->y, $point->x+$size, $point->y+$size, $c );

				//if there is aready apoint there, make it orange
				if($filled["{$point->x},{$point->y}"]==1) $c=$orange;

				//get the size
				$size = $markerSizes[$z];
				$size = 10;

				//draw the marker
				if($z<2) imagesetpixel($im, $point->x, $point->y, $c );
				elseif($z<12) {
					imagefilledellipse($im, $point->x, $point->y, $size, $size, $c );
					imageellipse($im, $point->x, $point->y, $size, $size, $white );
				} else {
					imageellipse($im, $point->x, $point->y, $size-1, $size-1, $c );
					imageellipse($im, $point->x, $point->y, $size-2, $size-2, $c );
					imageellipse($im, $point->x, $point->y, $size+1, $size+1, $black );
					imageellipse($im, $point->x, $point->y, $size, $size, $white );
				}

				//record that we drew the marker
				$filled["{$point->x},{$point->y}"]++;
			}
		}
	}
	//write some info about the tile to the image for testing
	/*imagestring($im,1,-1,0, "$count points in tile ({$_GET['x']},{$_GET['y']}) @ zoom $z ",$white);
	imagestring($im,1,0,1, "$count points in tile ({$_GET['x']},{$_GET['y']}) @ zoom $z ",$white);
	imagestring($im,1,0,-1, "$count points in tile ({$_GET['x']},{$_GET['y']}) @ zoom $z ",$white);
	imagestring($im,1,1,0,	"$count points in tile ({$_GET['x']},{$_GET['y']}) @ zoom $z ",$white);
	imagestring($im,1,0,0,	"$count points in tile ({$_GET['x']},{$_GET['y']}) @ zoom $z ",$black);
	imagestring($im,1,0,9, date('r'),$black);*/

	//output the new image to the file system and then send it to the browser
	imagepng($im, $file);
	echo file_get_contents($file);
	//imagepng($im);
	//echo $im;

} else {
	//output the existing image to the browser
	echo file_get_contents($file);
}
?>