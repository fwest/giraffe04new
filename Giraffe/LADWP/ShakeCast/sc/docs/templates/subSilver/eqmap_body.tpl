<div id="nav">
	<span class="gensmall"><a href="{U_EQ_TABLE}" class="nav">{L_EQ_TABLE}</a></span>
</div>


<div id="eqmap_wrapper">

  <table align="center" id="eq_list" width="100%">
	<tr class="row2"> 
	  <th>{L_TITLE}</th>
	</tr>
	<tr class="row0"> 
		<td id="map_pane" valign="top">
			<div id="toolbar">
				<ul id="filters" class="nav"><span class="emphmed">Facility Type: </span></ul>
			</div>
			<div id="content">
				<div id="map-wrapper">
					<div id="map"></div>
				</div>
				<div id="sidebar">
					<ul id="sidebar-list">
					</ul>
				</div>
			</div>
			<div id="alert">
				<p>Now Loading...</p>
			</div>
		</td>
	</tr>
  </table>
</div>


