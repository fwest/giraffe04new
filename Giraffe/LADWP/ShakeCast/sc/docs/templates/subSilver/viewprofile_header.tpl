<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset={S_CONTENT_ENCODING}">
<meta http-equiv="Content-Style-Type" content="text/css">
{META}
{NAV_LINKS}
<title>{SITENAME} :: {PAGE_TITLE}</title>
<!-- link rel="stylesheet" href="templates/subSilver/{T_HEAD_STYLESHEET}" type="text/css" -->
<link rel="stylesheet" href="templates/subSilver/sc.css" type="text/css">
    <style type="text/css">
    v\:* {
      behavior:url(#default#VML);
    }
	
	.sortable table, 
	.sortable td
	{
		border               : 1px solid #999;
		border-collapse      : collapse;
	  font                 : small/0.9 "Tahoma", "Bitstream Vera Sans", Verdana, Helvetica, sans-serif;
	}
	.sortable th
	{
	  font                 : small/1.5 "Tahoma", "Bitstream Vera Sans", Verdana, Helvetica, sans-serif;
		color: #FFA34F; font-size: 13px; font-weight : bold;
		background-color: #006699; height: 25px;
		background-image: url(images/cellpic3.gif);
	}
	.sortable table
	{
		border                :none;
		border                :2px solid #999;
		position : relative;
		height: 100%
	}
	.sortable thead th,
	.sortable tbody th
	{
	  color                 : #eee;  
		padding               : 5px 10px;
	  border-left           : 1px solid #999;
	  border-right           : 1px solid #999;
	}
	.sortable tbody th
	{
	  background-color            : #DEE3E7;
	  border-top            : 1px solid #999;
	  text-align            : left;
	  font-weight           : normal;
	  font-size: 13px; font-weight : bold;
	}
	.sortable tbody tr td
	{
		padding               : 5px 5px;
	  color                 : #666;
	}
	.sortable tr.row0	{ background-color: #DD6900; }
	.sortable tr.row1	{ background-color: #EEEEEE; }
	.sortable tr.row2 th	{ background-color: #0066CC; }
	.sortable tr.row2 a	{ color : #FFF; }
    </style>

    <script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key={GM_API_KEY}" type="text/javascript"></script>
	<script type='text/javascript' src='common.js'></script>
	<script type='text/javascript' src='css.js'></script>
	<script src="default_sc.js" type="text/javascript"></script>
	<script src="viewprofile_functions.js" type="text/javascript"></script>
	<script src="shakemap.js" type="text/javascript"></script>
	<script type='text/javascript'>
		var geom = new Object();
		var selectedProfile = new Array();
	</script>
</head>
<body bgcolor="{T_BODY_BGCOLOR}" text="{T_BODY_TEXT}" link="{T_BODY_LINK}" vlink="{T_BODY_VLINK}" onload="standardistaTableSortingInit();init();PMA_markRowsInit();" onunload="GUnload()">
<div id="container">
<div id="header">
    <h1>
        <a href="{U_INDEX}"><img src="templates/subSilver/images/header.jpg" alt="{L_INDEX}"></a>

    </h1>
    <ul id="globalnav">
        <li><a href="{U_INDEX}" class="{C_HOME}">Home</a>
			<ul>
        <li><a href="{U_SEARCH}" class="{C_SEARCH}">{L_SEARCH}</a></li>
		<li><a href="{U_FAQ}" class="{C_FAQ}">{L_FAQ}</a></li>
			</ul>
		</li>
        <li><a href="{U_EARTHQUAKE}" class="{C_EARTHQUAKE}">{L_EARTHQUAKE}</a>
			<ul>
				<li><a href="{U_EQ_LATEST}">{L_EQ_LATEST}</a></li>
				<li><a href="{U_EQ_LIST}">{L_EQ_LIST}</a></li>
				<li><a href="{U_EQ_TEST}">{L_EQ_TEST}</a></li>
			</ul>
		</li>
        <li><a href="{U_SEARCH}" class="{C_SEARCH}">{L_SEARCH}</a></li>
		<li><a href="{U_FAQ}" class="{C_FAQ}">{L_FAQ}</a></li>
        <li><a href="{U_PROFILE}" class="{C_PROFILE}">{L_PROFILE}</a>
			<ul>
				<li><a href="{U_USER_GENERAL}">{L_USER_GENERAL}</a></li>
				<li><a href="{U_EMAIL_LIST}">{L_EMAIL_LIST}</a></li>
				<li><a href="{U_NOTIFICATION}">{L_NOTIFICATION}</a></li>
			</ul>
		</li>
		<!-- BEGIN switch_user_logged_out -->
		<li><a href="{U_REGISTER}" class="{C_REGISTER}">{L_REGISTER}</a></li>
		<!-- END switch_user_logged_out -->
		<!-- BEGIN switch_user_logged_in -->
		<li><a href="{U_ADMIN}" class="{C_ADMIN}">{L_ADMIN}</a></li>
		<!-- END switch_user_logged_in -->
        <li><a href="{U_LOGIN_LOGOUT}" class="{C_LOGIN_LOGOUT}">{L_LOGIN_LOGOUT}</a></li>
    </ul>
</div>
<div id="content">
