<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html dir="{S_CONTENT_DIRECTION}">
<head>
<meta http-equiv="Content-Type" content="text/html; charset={S_CONTENT_ENCODING}">
<meta http-equiv="Content-Style-Type" content="text/css">
{META}
{NAV_LINKS}
<title>{SITENAME} :: {PAGE_TITLE}</title>
<!-- link rel="stylesheet" href="templates/subSilver/{T_HEAD_STYLESHEET}" type="text/css" -->
<link rel="stylesheet" href="templates/subSilver/sc.css" type="text/css">
    <script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key={GM_API_KEY}" type="text/javascript"></script>
	<script src="shakemap.js" type="text/javascript"></script>
	<script src="default_sc.js" type="text/javascript"></script>
	<script src="index_map_functions.js" type="text/javascript"></script>
</head>
<body bgcolor="{T_BODY_BGCOLOR}" text="{T_BODY_TEXT}" link="{T_BODY_LINK}" vlink="{T_BODY_VLINK}" onunload="GUnload()">
<div id="container">
<div id="header">
    <h1>
        <a href="{U_INDEX}"><img src="templates/subSilver/images/header.jpg" alt="{L_INDEX}"></a>

    </h1>
    <ul id="globalnav">
        <li><a href="{U_INDEX}" class="here">Home</a>
			<ul>
				<li><a href="#"></a></li>
			</ul>
		</li>
        <li><a href="{U_EARTHQUAKE}">{L_EARTHQUAKE}</a></li>
        <li><a href="{U_SEARCH}">{L_SEARCH}</a></li>
		<li><a href="{U_FAQ}">{L_FAQ}</a></li>
        <li><a href="{U_PROFILE}">{L_PROFILE}</a></li>
		<!-- BEGIN switch_user_logged_out -->
		<li><a href="{U_REGISTER}">{L_REGISTER}</a></li>
		<!-- END switch_user_logged_out -->
		<!-- BEGIN switch_user_logged_in -->
		<li><a href="{U_ADMIN}">{L_ADMIN}</a></li>
		<!-- END switch_user_logged_in -->
        <li><a href="{U_LOGIN_LOGOUT}">{L_LOGIN_LOGOUT}</a></li>
    </ul>
</div>
<div id="content">
