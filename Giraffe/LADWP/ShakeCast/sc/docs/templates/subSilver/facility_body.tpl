
<div id="facility_wraper">
	<div id="facility">
	<table align="center" id="eq_list" style="width:100%" border="1">
	<tr class="row2">
		<th>{L_FACILITY_ID}</th>
		<td class="row2" id="facility_id">{FACILITY_ID}</td>
	</tr>
	<tr class="row2">
		<th>{L_FACILITY_NAME}</th>
		<td class="row2">{FACILITY_NAME}</td>
	</tr>
	<tr class="row2">
		<th>{L_SHORT_NAME}</th>
		<td class="row2">{SHORT_NAME}</td>
	</tr>
	<tr class="row2">
		<th>{L_FACILITY_TYPE}</th>
		<td class="row2">{FACILITY_TYPE}</td>
	</tr>
	<tr class="row2">
		<th>{L_DESCRIPTION}</th>
		<td class="row2">{DESCRIPTION}</td>
	</tr>
	<tr class="row2">
		<th>{L_LAT}</th>
		<td class="row2">{LAT_MIN} <-> {LAT_MAX}</td>
	</tr>
	<tr class="row2">
		<th>{L_LON}</th>
		<td class="row2">{LON_MIN} <-> {LON_MAX}</td>
	</tr>
<!-- BEGIN attrow -->
	<tr class="row2">
		<th>{attrow.L_ATTRIB_NAME}</td>
		<td class="row2">{attrow.ATTRIBUTE_VALUE}</td>
	</tr>
<!-- END attrow -->
</table>

	</div>
	
	<div id="facility-right">
<table align="center" id="eq_list" style="width:100%" border="1">
	<tr class="row2">
		<th>{L_DAMAGE_LEVEL}</th>
		<th>{L_LOW_LIMIT}</th>
		<th>{L_HIGH_LIMIT}</th>
		<th>{L_FRAGILITY_METRIC}</th>
	</tr>
<!-- BEGIN fragrow -->
	<tr>
		<td class="row2">{fragrow.DAMAGE_LEVEL}</td>
		<td class="row2">{fragrow.LOW_LIMIT}</td>
		<td class="row2">{fragrow.HIGH_LIMIT}</td>
		<td class="row2">{fragrow.METRIC}</td>
	</tr>
<!-- END fragrow -->
</table>
	</div>
</div>
<br />
<div id="eq_list_wrapper">
	<div id="eqtable" >
		<table align="center" id="eq_list">
			<thead>
				<tr class="row2">
					<th><a href="{U_TYPE}">{L_TYPE}</a></th>
					<th><a href="{U_NAME}">{L_NAME}</a></th>
					<th><a href="{U_LOCATION}">{L_LOCATION}</a></th>
					<th><a href="{U_DAMAGE_ESTIMATE}">{L_DAMAGE_ESTIMATE}</a></th>
					{L_METRIC}
				</tr>
			</thead>
			<tbody>
			<!-- BEGIN postrow -->
				<tr class="row1" id="{postrow.FACILITY_ID}">
					<td class="lat">{postrow.TYPE}</td>
					<td><a href="{postrow.U_NAME}" class="topictitle">{postrow.NAME}</a></td>
					<td class="lat">{postrow.DESCRIPTION}</td>
					<td class="{postrow.FACILITY_DAMAGE}">{postrow.DAMAGE_LEVEL}</td>
					{postrow.METRIC}
				</tr>
			<!-- END postrow -->
			</tbody>
		</table>
	
  <table width="100%" cellspacing="2" border="0" align="center" cellpadding="2">
	<tr> 
	  <td align="right" valign="middle" nowrap="nowrap"><span class="gensmall">{S_TIMEZONE}</span><br /><span class="nav">{PAGINATION}</span> 
		</td>
	</tr>
	<tr>
	  <td align="left"><span class="nav">{PAGE_NUMBER}</span></td>
	</tr>
  </table>
	</div>
</div>


