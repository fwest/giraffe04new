
<h1>{L_WORDS_TITLE}</h1>

<P>{L_WORDS_TEXT}</p>

<form method="post" action="{S_WORDS_ACTION}"><table cellspacing="1" cellpadding="4" border="0" align="center" class="forumline">
	<tr>
		<th class="thCornerL">{L_DAMAGE_LEVEL}</th>
		<th class="thTop">{L_NAME}</th>
		<th class="thTop">{L_DESCRIPTION}</th>
		<th class="thTop">{L_SEVERITY_RANK}</th>
		<th class="thTop">{L_IS_MAX_SEVERITY}</th>
		<th colspan="2" class="thCornerR">{L_ACTION}</th>
	</tr>
	<!-- BEGIN words -->
	<tr>
		<td class="{words.ROW_CLASS}" align="center">{words.DAMAGE_LEVEL}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.NAME}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.DESCRIPTION}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.SEVERITY_RANK}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.IS_MAX_SEVERITY}</td>
		<td class="{words.ROW_CLASS}"><a href="{words.U_EDIT}">{L_EDIT}</a></td>
		<td class="{words.ROW_CLASS}"><a href="{words.U_DELETE}">{L_DELETE}</a></td>
	</tr>
	<!-- END words -->
	<tr>
		<td colspan="8" align="center" class="catBottom">{S_HIDDEN_FIELDS}<input type="submit" name="add_event" value="{L_ADD_EVENT}" class="mainoption" /></td>
	</tr>
</table></form>
