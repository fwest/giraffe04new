
<h1>{L_WORDS_TITLE}</h1>

<P>{L_WORDS_TEXT}</p>

<form method="post" action="{S_SERVER_ACTION}">
<table cellspacing="1" cellpadding="4" border="0" align="center" class="forumline">
	<tr>
		<th colspan="2" class="thHead">{L_SERVER_EDIT}{F_HIDDEN_FIELDS}</th>
	</tr>
	<tr>
		<td class="row1">{L_SERVER_ID}</td>
		<td class="row2"><input class="post" type="text" name="server_id" value="{SERVER_ID}" size="50" /></td>
	</tr>
	<tr>
		<td class="row1">{L_DNS_ADDRESS}</td>
		<td class="row2"><input class="post" type="text" name="dns_address" value="{DNS_ADDRESS}" size="50" /></td>
	</tr>
	<tr>
		<td class="row1">{L_OWNER_ORGANIZATION}</td>
		<td class="row2"><input class="post" type="text" name="owner_organization" value="{OWNER_ORGANIZATION}" size="50" /></td>
	</tr>
	<tr>
		<td class="row1">{L_UPSTREAM_FLAG}</td>
		<td class="row2"><input class="post" type="checkbox" name="upstream_flag" value="yes" {UPSTREAM_FLAG}/> Yes</td>
	</tr>
	<tr>
		<td class="row1">{L_DOWNSTREAM_FLAG}</td>
		<td class="row2"><input class="post" type="checkbox" name="downstream_flag" value="yes" {DOWNSTREAM_FLAG}/> Yes</td>
	</tr>
	<tr>
		<td class="row1">{L_POLL_FLAG}</td>
		<td class="row2"><input class="post" type="checkbox" name="poll_flag" value="yes" {POLL_FLAG}/> Yes</td>
	</tr>
	<tr>
		<td class="row1">{L_QUERY_FLAG}</td>
		<td class="row2"><input class="post" type="checkbox" name="query_flag" value="yes" {QUERY_FLAG}/> Yes</td>
	</tr>
	<tr>
		<td class="catBottom" colspan="2" align="center">{S_HIDDEN_FIELDS}<input type="submit" name="save" value="{L_SUBMIT}" class="mainoption" /></td>
	</tr>
</table>
</form>

