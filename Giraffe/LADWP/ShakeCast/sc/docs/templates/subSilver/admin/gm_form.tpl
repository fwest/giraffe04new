<html>
<head>
<title>ShakeCast Facility Picker</title>
    <script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key={GM_API_KEY}" type="text/javascript"></script>
	<script src="/default_sc.js" type="text/javascript"></script>
	<script src="profile_gm.js" type="text/javascript"></script>
	<script language="JavaScript" type="text/javascript">
		var geocoder = null;
		function initProfile() {
			//var mode = gup('mode');
			if (document.getElementById("map")) {
				init_gm();
				geocoder = new GClientGeocoder();
			}
		}
			
		function setMaster()
		{
			if (latlngs.length > 0) {
				var latitude = parseFloat(latlngs[0].lat());
				var longitude = parseFloat(latlngs[0].lng());
				opener.document.facility_edit.lat_min.value = latitude.toFixed(4);
				opener.document.facility_edit.lon_min.value = longitude.toFixed(4);
				if (latlngs.length > 1) {
					latitude = parseFloat(latlngs[1].lat());
					longitude = parseFloat(latlngs[1].lng());
				}
				opener.document.facility_edit.lat_max.value = latitude.toFixed(4);
				opener.document.facility_edit.lon_max.value = longitude.toFixed(4);
			}
		   self.close();
		}

		function showAddress(address) {
			geocoder.getLocations(address, addAddressToMap);
		}
		
		function addAddressToMap(response) {
		  if (!response || response.Status.code != 200) {
			alert("\"" + address + "\" not found");
		  } else {
			place = response.Placemark[0];
			point = new GLatLng(place.Point.coordinates[1],
								place.Point.coordinates[0]);
								
			if (latlngs.length < 2) {
				latlngs.push(point);
			} else {
				latlngs[1] = point;
			}
			
			map.setCenter(point, 13);
			initializePoint(latlngs.length - 1);
			
			if (latlngs.length > 1) {			
				redrawPolyline();
			}
		  }
		}
	</script>
</head>
<body bgcolor="#426498" onload="initProfile()" onUnLoad="setMaster()">
<div align="left">
<form name="popForm" onSubmit="setMaster(); return false">
<input type="text" size="60" name="address" value="1711 Illinois Street, Golden, CO 80401" />
<input type="button" value="Locate!" onClick="showAddress(address.value);">
<input type="submit" value="DONE"><br>
<div id="map" style="width: 540px; height: 360px"></div>
<div id="sidebar-list"></div>
</form>

</div>
</body>
</html>