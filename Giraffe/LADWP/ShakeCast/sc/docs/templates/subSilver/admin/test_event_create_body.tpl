
<h1>{L_WORDS_TITLE}</h1>

<P>{L_WORDS_TEXT}</p>

<form method="post" action="{S_WORDS_ACTION}"><table cellspacing="1" cellpadding="4" border="0" align="center" class="forumline">
	<tr>
		<th class="thCornerL"><a href="{U_EVENT_ID}" class="event">{L_EVENT_ID}</a></th>
		<th class="thCornerL"><a href="{U_EVENT_TIMESTAMP}" class="event">{L_EVENT_TIMESTAMP}</a></th>
		<th class="thTop"><a href="{U_MAGNITUDE}" class="event">{L_MAGNITUDE}</a></th>
		<th class="thTop"><a href="{U_LATITUDE}" class="event">{L_LATITUDE}</a></th>
		<th class="thTop"><a href="{U_LONGITUDE}" class="event">{L_LONGITUDE}</a></th>
		<th class="thTop"><a href="{U_DESCRIPTION}" class="event">{L_DESCRIPTION}</a></th>
		<th class="thCornerR">{L_ACTION}</th>
	</tr>
	<!-- BEGIN words -->
	<tr>
		<td class="{words.ROW_CLASS}" align="center">{words.EVENT_ID}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.EVENT_TIMESTAMP}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.MAGNITUDE}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.LATITUDE}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.LONGITUDE}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.DESCRIPTION}</td>
		<td class="{words.ROW_CLASS}" align="center"><a href="{words.U_EVENT}">{L_ADD_EVENT}</a></td>
	</tr>
	<!-- END words -->
</table></form>

<table width="100%" cellspacing="2" border="0" align="center" cellpadding="2">
  <tr> 
	<td align="left" valign="top"><span class="nav">{PAGE_NUMBER}</span></td>
	<td align="right" valign="top" nowrap="nowrap"><span class="nav">{PAGINATION}</span><br /><span class="gensmall">{S_TIMEZONE}</span></td>
  </tr>
</table>
