
<h1>{L_WORDS_TITLE}</h1>

<P>{L_WORDS_TEXT}</p>

<form method="post" action="{S_WORDS_ACTION}"><table cellspacing="1" cellpadding="4" border="0" align="center" class="forumline">
	<tr>
		<th class="thCornerL">{L_LOG_FILE}</th>
		<th>{L_FILE_SIZE}</th>
		<th>{L_DESCRIPTION}</th>
		<th class="thCornerR">{L_ACTION}</th>
	</tr>
	<!-- BEGIN words -->
	<tr>
		<td class="{words.ROW_CLASS}" align="center" valign="top">{words.LOG_FILE}</td>
		<td class="{words.ROW_CLASS}" align="center" valign="top">{words.FILE_SIZE}</td>
		<td class="{words.ROW_CLASS}" align="left">{words.DESCRIPTION}</td>
		<td class="{words.ROW_CLASS}" align="center"><a href="{words.U_VIEW}">{L_VIEW}</a></td>
	</tr>
	<!-- END words -->
	<tr>
		<td colspan="4" align="center" class="catBottom">{S_HIDDEN_FIELDS}</td>
	</tr>
</table></form>

<table width="100%" cellspacing="2" border="0" align="center" cellpadding="2">
  <tr> 
	<td align="left" valign="top"><span class="nav">{PAGE_NUMBER}</span></td>
	<td align="right" valign="top" nowrap="nowrap"><span class="nav">{PAGINATION}</span><br /><span class="gensmall">{S_TIMEZONE}</span></td>
  </tr>
</table>
