
<h1>{L_USER_TITLE}</h1>

<p>{L_USER_EXPLAIN}</p>

{ERROR_BOX}

<form action="{S_PROFILE_ACTION}" {S_FORM_ENCTYPE} method="post"><table width="80%" cellspacing="1" cellpadding="4" border="0" align="center" class="forumline">
	<tr> 
	  <th class="thHead" colspan="2">{L_REGISTRATION_INFO}{PROFILE_NAME}</th>
	</tr>
	<tr> 
	  <td class="row2" colspan="2"><span class="gensmall">{L_ITEMS_REQUIRED}</span></td>
	</tr>
	<tr> 
	  <td class="row1" width="38%"><span class="gen">{L_TYPE}: *</span></td>
	  <td class="row2"> 
		<input class="post" type="hidden" name="TXT_NOTIFICATION_TYPE" value="{TYPE}" />{TYPE_TXT}
	  </td>
	</tr>
	<tr> 
	  <td class="row1"><span class="gen">{L_EVENT_TYPE}: </span></td>
	  <td class="row2"> {EVENT_TYPE}
	  </td>
	</tr>
	<tr> 
	  <td class="row1"><span class="gen">{L_DELIVERY}: *</span></td>
	  <td class="row2"> {DELIVERY}
	  </td>
	</tr>
	<tr> 
	  <td class="row1"><span class="gen">{L_TEMPLATE}: </span></td>
	  <td class="row2"> {TEMPLATE}
	  </td>
	</tr>
	<!-- BEGIN limit_value -->
	<tr> 
	  <td class="row1"><span class="gen">{L_LIMIT_VALUE}: </span></td>
	  <td class="row2"> 
		<input class="post" type="text" name="TXT_LIMIT_VALUE" size="35" maxlength="32" value="{limit_value.LIMIT_VALUE}" />
	  </td>
	</tr>
	<!-- END limit_value -->
	<!-- BEGIN product -->
	<tr> 
	  <td class="row1"><span class="gen">{L_PRODUCT}: </span></td>
	  <td class="row2"> {product.PRODUCT} 
	  </td>
	</tr>
	<!-- END product -->
	<!-- BEGIN metric -->
	<tr> 
	  <td class="row1"><span class="gen">{L_METRIC}: </span></td>
	  <td class="row2"> {metric.METRIC} 
	  </td>
	</tr>
	<!-- END metric -->
	<!-- BEGIN damage_level -->
	<tr> 
	  <td class="row1"><span class="gen">{L_DAMAGE_LEVEL}</span></td>
	  <td class="row2"> {damage_level.DAMAGE_LEVEL}
	  </td>
	</tr>
	<!-- END damage_level -->
	<tr> 
	  <td class="row1"><span class="gen">{L_DISABLE}</span></td>
	  <td class="row2"> 
		<input class="post" type="checkbox" name="TXT_DISABLED" value=1 {DISABLE} />
	  </td>
	</tr>
	<tr> 
	  <td class="row1"><span class="gen">{L_AGGREGATE}</span></td>
	  <td class="row2"> 
		<input class="post" type="checkbox" name="TXT_AGGREGATE" value=1 {AGGREGATE} />
	  </td>
	</tr>
	<tr> 
	  <td class="row1"><span class="gen">{L_AGGREGATION_GROUP}</span></td>
	  <td class="row2"> 
		<input class="post" type="text" name="TXT_AGGREGATION_GROUP" size="35" maxlength="150" value="{AGGREGATION_GROUP}" />
	  </td>
	</tr>
	  <td class="catBottom" colspan="2" align="center">{S_HIDDEN_FIELDS} 
		<input type="submit" name="submit" value="{L_SUBMIT}" class="mainoption" />
		&nbsp;&nbsp; 
		<input type="reset" value="{L_RESET}" class="liteoption" />
	  </td>
	</tr>
</table></form>

