
<h1>{L_WORDS_TITLE}</h1>

<P>{L_WORDS_TEXT}</p>

<form method="post" name="profile_form" action="{S_FACILITY_TYPE_ACTION}"
	onsubmit="document.profile_form.geom.value = gm_profile();">
<table cellspacing="1" cellpadding="4" border="0" align="center" class="forumline">
	<tr>
		<th colspan="3" class="thHead">{L_PROFILE_POLYGON_EDIT}{F_HIDDEN_FIELDS}</th>
	</tr>
	<tr>
		<td class="row1">{L_PROFILE_NAME}</td>
		<td class="row2">{PROFILE_NAME}</td>
		<td class="row2"></td>
	</tr>
	<tr>
		<td class="row1">{L_DESCRIPTION}</td>
		<td class="row2">{DESCRIPTION}</td>
		<td class="row2"></td>
	</tr>
	<tr>
		<td class="row1">{L_POLY_TYPE}</td>
		<td class="row2">{POLY_TYPE}</td>
		<td class="row2"></td>
	</tr>
	<tr>
		{METRIC}
		<td class="row1" colspan="2">{GMAP}</td>
		<td class="row2" valign="top">
<!--
			{SHOW_SELECT}
-->
			{SHOW_SELECT}
			<div id="sidebar-list">
			</div>
		</td>
	</tr>
	<tr>
		<td class="catBottom" colspan="3" align="center">{S_HIDDEN_FIELDS}<input type="submit" id="save" name="save" value="{L_SUBMIT}" class="mainoption"/></td>
	</tr>
</table>
</form>
