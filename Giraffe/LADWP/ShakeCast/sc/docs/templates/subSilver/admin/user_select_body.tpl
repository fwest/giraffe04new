
<h1>{L_USER_TITLE}</h1>

<p>{L_USER_EXPLAIN}</p>

<form method="post" name="post" action="{S_USER_ACTION}"><table cellspacing="1" cellpadding="4" border="0" align="center" class="forumline">
	<tr>
		<th class="thHead" align="center">{L_USER_SELECT}</th>
	</tr>
	<tr>
		<td class="row1" align="center"><input type="text" class="post" name="username" maxlength="50" size="20" /> <input type="hidden" name="mode" value="edit" />{S_HIDDEN_FIELDS}<input type="submit" name="submituser" value="{L_LOOK_UP}" class="mainoption" /></td>
	</tr>
</table></form>

<form method="post" action="{S_WORDS_ACTION}"><table cellspacing="1" cellpadding="4" border="0" align="center" class="forumline">
	<tr>
		<th class="thCornerL"><a href="{U_USERNAME}" class="event">{L_USERNAME}</a></th>
		<th class="thTop"><a href="{U_FULL_NAME}" class="event">{L_FULL_NAME}</a></th>
		<th class="thTop"><a href="{U_EMAIL}" class="event">{L_EMAIL}</a></th>
		<th class="thTop"><a href="{U_OCCUPATION}" class="event">{L_OCCUPATION}</a></th>
		<th class="thTop"><a href="{U_ORGANIZATION}" class="event">{L_ORGANIZATION}</a></th>
		<th class="thTop"><a href="{U_USER_LEVEL}" class="event">{L_USER_LEVEL}</a></th>
		<th class="thTop"><a href="{U_USER_STATUS}" class="event">{L_USER_STATUS}</a></th>
		<th colspan="2" class="thCornerR">{L_ACTION}</th>
	</tr>
	<!-- BEGIN words -->
	<tr>
		<td class="{words.ROW_CLASS}" align="center">{words.USERNAME}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.FULL_NAME}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.EMAIL}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.OCCUPATION}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.ORGANIZATION}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.USER_LEVEL}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.USER_STATUS}</td>
		<td class="{words.ROW_CLASS}"><a href="{words.U_EDIT}">{L_EDIT}</a></td>
		<td class="{words.ROW_CLASS}"><a href="{words.U_DELETE}">{L_DELETE}</a></td>
	</tr>
	<!-- END words -->
	<tr>
		<td colspan="9" align="center" class="catBottom">{S_HIDDEN_FIELDS}<input type="submit" name="add_event" value="{L_ADD_EVENT}" class="mainoption" /></td>
	</tr>
</table></form>

<table width="100%" cellspacing="2" border="0" align="center" cellpadding="2">
  <tr> 
	<td align="left" valign="top"><span class="nav">{PAGE_NUMBER}</span></td>
	<td align="right" valign="top" nowrap="nowrap"><span class="nav">{PAGINATION}</span><br /><span class="gensmall">{S_TIMEZONE}</span></td>
  </tr>
</table>
