
<h1>{L_WORDS_TITLE}</h1>

<P>{L_WORDS_TEXT}</p>

<table cellspacing="1" cellpadding="4" border="0" align="center" class="forumline">
	<tr>
		<th class="thCornerL">{L_EVENT_ID}</th>
		<th class="thCornerL">{L_EVENT_TIMESTAMP}</th>
		<th class="thTop">{L_MAGNITUDE}</th>
		<th class="thTop">{L_LATITUDE}</th>
		<th class="thTop">{L_LONGITUDE}</th>
		<th class="thTop">{L_DESCRIPTION}</th>
		<th colspan = "3" class="thCornerR">{L_ACTION}</th>
	</tr>
	<!-- BEGIN words -->
	<tr>
		<td class="{words.ROW_CLASS}" align="center"><a href="{words.U_EVENT}">{words.EVENT_ID}</a></td>
		<td class="{words.ROW_CLASS}" align="center">{words.EVENT_TIMESTAMP}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.MAGNITUDE}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.LATITUDE}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.LONGITUDE}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.DESCRIPTION}</td>
		<td class="{words.ROW_CLASS}" align="center"><a href="{words.U_NOTIFY}">{L_NOTIFY}</a></td>
		<td class="{words.ROW_CLASS}" align="center"><a href="{words.U_RETRACT}">{L_RETRACT}</a></td>
		<td class="{words.ROW_CLASS}" align="center"><a href="{words.U_DELETE}">{L_DELETE}</a></td>
	</tr>
	<!-- END words -->
</table>