
<h1>{L_USER_TITLE}</h1>

<p>{L_USER_EXPLAIN}</p>

{ERROR_BOX}

<form action="{S_PROFILE_ACTION}" {S_FORM_ENCTYPE} method="post"><table width="98%" cellspacing="1" cellpadding="4" border="0" align="center" class="forumline">
	<tr> 
	  <th class="thHead" colspan="2">{L_REGISTRATION_INFO}</th>
	</tr>
	<tr> 
	  <td class="row2" colspan="2"><span class="gensmall">{L_ITEMS_REQUIRED}</span></td>
	</tr>
	<tr> 
	  <td class="row1" width="38%"><span class="gen">{L_USERNAME}: *</span></td>
	  <td class="row2"> 
		<input class="post" type="text" name="username" size="35" maxlength="40" value="{USERNAME}" />
	  </td>
	</tr>
	<tr> 
	  <td class="row1"><span class="gen">{L_EMAIL_ADDRESS}: *</span></td>
	  <td class="row2"> 
		<input class="post" type="text" name="email" size="35" maxlength="255" value="{EMAIL}" />
	  </td>
	</tr>
	<tr> 
	  <td class="row1"><span class="gen">{L_NEW_PASSWORD}: *</span><br />
		<span class="gensmall">{L_PASSWORD_IF_CHANGED}</span></td>
	  <td class="row2"> 
		<input class="post" type="password" name="password" size="35" maxlength="32" value="" />
	  </td>
	</tr>
	<tr> 
	  <td class="row1"><span class="gen">{L_CONFIRM_PASSWORD}: * </span><br />
		<span class="gensmall">{L_PASSWORD_CONFIRM_IF_CHANGED}</span></td>
	  <td class="row2"> 
		<input class="post" type="password" name="password_confirm" size="35" maxlength="32" value="" />
	  </td>
	</tr>
	<tr> 
	  <td class="catsides" colspan="2">&nbsp;</td>
	</tr>
	<tr> 
	  <th class="thSides" colspan="2">{L_PROFILE_INFO}</th>
	</tr>
	<tr> 
	  <td class="row1"><span class="gen">{L_FULLNAME}</span></td>
	  <td class="row2"> 
		<input class="post" type="text" name="fullname" size="35" maxlength="255" value="{FULLNAME}" />
	  </td>
	</tr>
	<tr> 
	  <td class="row1"><span class="gen">{L_OCCUPATION}</span></td>
	  <td class="row2"> 
		<input class="post" type="text" name="occupation" size="35" maxlength="100" value="{OCCUPATION}" />
	  </td>
	</tr>
	<tr> 
	  <td class="row1"><span class="gen">{L_ORGANIZATION}</span></td>
	  <td class="row2"> 
		<input class="post" type="text" name="organization" size="35" maxlength="150" value="{ORGANIZATION}" />
	  </td>
	</tr>
	<tr> 
	  <td class="row1"><span class="gen">{L_LOCATION}</span></td>
	  <td class="row2"> 
		<input class="post" type="text" name="location" size="35" maxlength="100" value="{LOCATION}" />
	  </td>
	</tr>

	<tr> 
	  <td class="catsides" colspan="2">&nbsp;</td>
	</tr>
	<tr> 
	  <th class="thSides" colspan="2">{L_DELIVERY_METHOD_INFO}</th>
	</tr>
	<tr> 
	  <td class="row1"><span class="gen">{L_EMAIL_HTML}</span></td>
	  <td class="row2"> 
		<input class="post" type="text" name="email_html" size="35" maxlength="100" value="{EMAIL_HTML}" />
	  </td>
	</tr>
	<tr> 
	  <td class="row1"><span class="gen">{L_EMAIL_TEXT}</span></td>
	  <td class="row2"> 
		<input class="post" type="text" name="email_text" size="35" maxlength="150" value="{EMAIL_TEXT}" />
	  </td>
	</tr>
	<tr> 
	  <td class="row1"><span class="gen">{L_PAGER}</span></td>
	  <td class="row2"> 
		<input class="post" type="text" name="pager" size="35" maxlength="100" value="{PAGER}" />
	  </td>
	</tr>

	<tr> 
	  <td class="catSides" colspan="2">&nbsp;</td>
	</tr>
	<tr>
	  <th class="thSides" colspan="2">{L_SPECIAL}</th>
	</tr>
	<tr>
	  <td class="row1" colspan="2"><span class="gensmall">{L_SPECIAL_EXPLAIN}</span></td>
	</tr>
	<tr> 
	  <td class="row1"><span class="gen">{L_USER_ACTIVE}</span></td>
	  <td class="row2"> 
		<input type="radio" name="user_status" value="1" {USER_ACTIVE_YES} />
		<span class="gen">{L_YES}</span>&nbsp;&nbsp; 
		<input type="radio" name="user_status" value="0" {USER_ACTIVE_NO} />
		<span class="gen">{L_NO}</span></td>
	</tr>
<!--
	<tr>
		<td class="row1"><span class="gen">{L_SELECT_RANK}</span></td>
		<td class="row2"><select name="user_rank">{RANK_SELECT_BOX}</select></td>
	</tr>
-->
	<tr>
		<td class="row1"><span class="gen">{L_SELECT_USER_LEVEL}</span></td>
		<td class="row2"><select name="user_level">{USER_LEVEL_BOX}</select></td>
	</tr>
	<tr> 
	  <td class="row1"><span class="gen">{L_DELETE_USER}?</span></td>
	  <td class="row2"> 
		<input type="checkbox" name="deleteuser">
		{L_DELETE_USER_EXPLAIN}</td>
	</tr>
	<tr> 
	  <td class="catBottom" colspan="2" align="center">{S_HIDDEN_FIELDS} 
		<input type="submit" name="submit" value="{L_SUBMIT}" class="mainoption" />
		&nbsp;&nbsp; 
		<input type="reset" value="{L_RESET}" class="liteoption" />
	  </td>
	</tr>
</table></form>
