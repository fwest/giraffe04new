
<h1>{L_WORDS_TITLE}</h1>

<P>{L_WORDS_TEXT}</p>

<form method="post" name="profile_form" action="{S_FACILITY_TYPE_ACTION}">
<table cellspacing="1" cellpadding="4" border="0" align="center" class="forumline">
	<tr> 
	  <th class="thHead" colspan="15">{L_REGISTRATION_INFO}{PROFILE_NAME}</th>
	</tr>
	<tr> 
	  <td class="row2" colspan="15"><span class="gensmall">{L_ITEMS_REQUIRED}</span></td>
	</tr>
	<tr>
		<th class="thCornerL">{L_ID}</th>
		<th class="thTop">{L_TYPE}</th>
		<th class="thTop">{L_EVENT_TYPE}</th>
		<th class="thTop">{L_DELIVERY}</th>
		<th class="thTop">{L_TEMPLATE}</th>
		<th class="thTop">{L_LIMIT_VALUE}</th>
		<th class="thTop">{L_DAMAGE_LEVEL}</th>
		<th class="thTop">{L_PRODUCT}</th>
		<th class="thTop">{L_METRIC}</th>
		<th class="thTop">{L_DISABLE}</th>
		<th class="thTop">{L_AGGREGATE}</th>
		<th class="thTop">{L_AGGREGATION_GROUP}</th>
		<th class="thTop">{L_FACILITY}</th>
		<th colspan="3" class="thCornerR">{L_ACTION}</th>
	</tr>
	<!-- BEGIN words -->
	<tr>
		<td class="{words.ROW_CLASS}" align="center">{words.ID}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.Type}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.Event_Type}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.Delivery}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.Template}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.Limit_Value}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.Damage_Level}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.Product}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.Metric}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.Disable}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.Aggregate}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.Aggregation_Group}</td>
		<td class="{words.ROW_CLASS}" align="center"><a href="{words.U_FAC_EDIT}">{words.Facility_Count}</a></td>
		<td class="{words.ROW_CLASS}"><a href="{words.U_EDIT}">{L_EDIT}</a></td>
		<td class="{words.ROW_CLASS}"><a href="{words.U_DELETE}">{L_DELETE}</a></td>
	</tr>
	<!-- END words -->
	<tr>
		<td colspan="15" align="center" class="catBottom">{S_HIDDEN_FIELDS}</td>
	</tr>
</table>
</form>

<table width="100%" cellspacing="2" border="0" align="center" cellpadding="2">
  <tr> 
	<td align="left" valign="top"><span class="nav">{JUMPBOX}</td>
	<td align="right" valign="top" nowrap="nowrap"><span class="nav">{PAGINATION}</span><br /><span class="gensmall">{S_TIMEZONE}</span></td>
  </tr>
</table>
