
<script language="javascript" type="text/javascript" src="/includes/datetimepicker.js"></script>
<h1>{L_WORDS_TITLE}</h1>

<p>{L_WORDS_TEXT}</p>

<form method="post" action="{S_FACILITY_ACTION}">
<table cellspacing="1" cellpadding="4" border="0" align="center" class="forumline" width="100%">
	<tr>
		<th colspan="2" class="thHead">{L_TASK_ADD}{F_HIDDEN_FIELDS}</th>
	</tr>
	<tr>
		<td class="row1">{L_TASK_TYPE}</td>
		<td class="row2">{TASK_TYPE}</td>
	</tr>
	<tr>
		<td class="row1">{L_OPT_PARM}<br /><span class="gensmall">{L_OPT_PARM_EXPLAIN}</span></td>
		<td class="row2"><input type="Text" id="opt_parm" name="opt_parm" maxlength="50" size="50"></td>
	</tr>
	<tr>
		<td class="row1">{L_TASK_DATE}</td>
		<td class="row2">	  		
			<input type="Text" id="task_date" name="task_date" maxlength="25" size="25">
			<a href="javascript:NewCal('task_date','mmddyyyy',false,24)"><img src="/images/cal.gif" width="16" height="16" border="0" alt="Pick a date"></a>
			<br>{HOUR_SELECT} : {MINUTE_SELECT}
		</td>
	</tr>
	<tr>
		<td class="row1">{L_REPEAT}</td>
		<td class="row2">{REPEAT}</td>
	</tr>
	<tr>
		<td class="row1">{L_USERNAME}</td>
		<td class="row2"><input type="Text" id="username" name="username" maxlength="25" size="25"></td>
	</tr>
	<tr>
		<td class="row1">{L_PASSWORD}</td>
		<td class="row2"><input type="password" id="password" name="password" maxlength="25" size="25"></td>
	</tr>
	<tr>
		<td class="catBottom" colspan="2" align="center">{S_HIDDEN_FIELDS}<input type="submit" name="save" value="{L_SUBMIT}" class="mainoption" /></td>
	</tr>
</table>

</form>
