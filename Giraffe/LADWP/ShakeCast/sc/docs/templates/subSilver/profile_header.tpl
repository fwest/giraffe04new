<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html dir="{S_CONTENT_DIRECTION}">
<head>
<meta http-equiv="Content-Type" content="text/html; charset={S_CONTENT_ENCODING}">
<meta http-equiv="Content-Style-Type" content="text/css">
{META}
{NAV_LINKS}
<title>{SITENAME} :: {PAGE_TITLE}</title>
<!-- link rel="stylesheet" href="templates/subSilver/{T_HEAD_STYLESHEET}" type="text/css" -->
<link rel="stylesheet" href="templates/subSilver/sc.css" type="text/css">

</head>
<body bgcolor="{T_BODY_BGCOLOR}" text="{T_BODY_TEXT}" link="{T_BODY_LINK}" vlink="{T_BODY_VLINK}">
<div id="container">
<div id="header">
    <h1>
        <a href="{U_INDEX}"><img src="templates/subSilver/images/header.jpg" alt="{L_INDEX}"></a>

    </h1>
    <ul id="globalnav">
        <li><a href="{U_INDEX}" class="{C_HOME}">Home</a>
			<ul>
        <li><a href="{U_SEARCH}" class="{C_SEARCH}">{L_SEARCH}</a></li>
		<li><a href="{U_FAQ}" class="{C_FAQ}">{L_FAQ}</a></li>
			</ul>
		</li>
        <li><a href="{U_EARTHQUAKE}" class="{C_EARTHQUAKE}">{L_EARTHQUAKE}</a>
			<ul>
				<li><a href="{U_EQ_LATEST}">{L_EQ_LATEST}</a></li>
				<li><a href="{U_EQ_LIST}">{L_EQ_LIST}</a></li>
				<li><a href="{U_EQ_TEST}">{L_EQ_TEST}</a></li>
			</ul>
		</li>
        <li><a href="{U_SEARCH}" class="{C_SEARCH}">{L_SEARCH}</a></li>
		<li><a href="{U_FAQ}" class="{C_FAQ}">{L_FAQ}</a></li>
        <li><a href="{U_PROFILE}" class="{C_PROFILE}">{L_PROFILE}</a>
			<ul>
				<li><a href="{U_USER_GENERAL}">{L_USER_GENERAL}</a></li>
				<li><a href="{U_EMAIL_LIST}">{L_EMAIL_LIST}</a></li>
				<li><a href="{U_NOTIFICATION}">{L_NOTIFICATION}</a></li>
			</ul>
		</li>
		<!-- BEGIN switch_user_logged_out -->
		<li><a href="{U_REGISTER}" class="{C_REGISTER}">{L_REGISTER}</a></li>
		<!-- END switch_user_logged_out -->
		<!-- BEGIN switch_user_logged_in -->
		<li><a href="{U_ADMIN}" class="{C_ADMIN}">{L_ADMIN}</a></li>
		<!-- END switch_user_logged_in -->
        <li><a href="{U_LOGIN_LOGOUT}" class="{C_LOGIN_LOGOUT}">{L_LOGIN_LOGOUT}</a></li>
    </ul>
</div>
<div id="content">
