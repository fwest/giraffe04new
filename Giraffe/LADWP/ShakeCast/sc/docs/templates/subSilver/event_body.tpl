<!-- BEGIN test_event -->
<center><span class="emphlarge">Scenario Earthquake</span></center><br /> 
<!-- END test_event -->

<div id="nav">
	<!-- BEGIN comment_event -->
	<div class="emph, nav" ><span style="color:red">{comment_event.L_COMMENT}</span><span>{comment_event.L_COMMENT_TEXT}</span></div><br /> 
	<!-- END comment_event -->
	<span class="gensmall"><a href="{U_GMAP}" class="nav">{L_GMAP}</a></span>
</div>


<div id="eq_list_wrapper">

	<div id="eqtable" >
	<table>
		<tr>
			<th id="eqtableTH">{L_EVENT}</th>
		</tr>
		<tr> 
			<td align="left" valign="top">
				<table align="center" id="eq_list">
				<thead>
				<tr class="row2">
				<th><a href="{U_FAC_ID}">{L_FAC_ID}</a></th>
				<th><a href="{U_TYPE}">{L_TYPE}</a></th>
				<th><a href="{U_NAME}">{L_NAME}</a></th>
				<th><a href="{U_DAMAGE_ESTIMATE}">{L_DAMAGE_ESTIMATE}</a></th>
				<th><a href="{U_LATITUDE}">{L_LATITUDE}</a></th>
				<th><a href="{U_LONGITUDE}">{L_LONGITUDE}</a></th>
				{L_METRIC}
				</tr>
				</thead>
				<tbody>
				<!-- BEGIN postrow -->
				<tr class="{postrow.ROW_CLASS}" id="{postrow.FACILITY_ID}">
				<td>{postrow.ID}</td>
				<td>{postrow.TYPE}</td>
				<td>{postrow.NAME}</td>
				<td class="{postrow.FACILITY_DAMAGE}">{postrow.DAMAGE_LEVEL}</td>
				<td class="lat">{postrow.LATITUDE}</td>
				<td class="lon">{postrow.LONGITUDE}</td>
				{postrow.METRIC}
				</tr>
				<!-- END postrow -->
				</tbody>
				</table>
			</td>
		</tr>
	</table>
	
	<table width="100%" cellspacing="2" border="0" align="center" cellpadding="2">
		<tr> 
		  <td align="right" valign="middle" nowrap="nowrap"><span class="gensmall">{S_TIMEZONE}</span><br /><span class="nav">{PAGINATION}</span> 
			</td>
		</tr>
		<tr>
		  <td align="left"><span class="nav">{PAGE_NUMBER}</span></td>
		</tr>
	</table>
	  
	</div>
</div>

