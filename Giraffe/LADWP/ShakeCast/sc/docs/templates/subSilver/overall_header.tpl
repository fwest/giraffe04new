<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html dir="{S_CONTENT_DIRECTION}">
<head>
<meta http-equiv="Content-Type" content="text/html; charset={S_CONTENT_ENCODING}">
<meta http-equiv="Content-Style-Type" content="text/css">
{META}
{NAV_LINKS}
<title>{SITENAME} :: {PAGE_TITLE}</title>
<!-- link rel="stylesheet" href="templates/subSilver/{T_HEAD_STYLESHEET}" type="text/css" -->
<link rel="stylesheet" href="templates/subSilver/sc.css" type="text/css">
<!-- BEGIN switch_enable_pm_popup -->
<script language="Javascript" type="text/javascript">
<!--
	if ( {PRIVATE_MESSAGE_NEW_FLAG} )
	{
		window.open('{U_PRIVATEMSGS_POPUP}', '_phpbbprivmsg', 'HEIGHT=225,resizable=yes,WIDTH=400');;
	}
//-->
</script>
<!-- END switch_enable_pm_popup -->
</head>
<body bgcolor="{T_BODY_BGCOLOR}" text="{T_BODY_TEXT}" link="{T_BODY_LINK}" vlink="{T_BODY_VLINK}">
<div id="container">
<div id="header">
    <h1>
        <a href="{U_INDEX}"><img src="templates/subSilver/images/header.jpg" alt="{L_INDEX}"></a>

    </h1>
    <ul id="globalnav">
        <li><a href="{U_INDEX}" class="{C_HOME}">Home</a>
			<ul>
				<li><a href="#"></a></li>
			</ul>
		</li>
        <li><a href="{U_EARTHQUAKE}" class="{C_EARTHQUAKE}">{L_EARTHQUAKE}</a></li>
        <li><a href="{U_SEARCH}" class="{C_SEARCH}">{L_SEARCH}</a></li>
		<li><a href="{U_FAQ}" class="{C_FAQ}">{L_FAQ}</a></li>
        <li><a href="{U_PROFILE}" class="{C_PROFILE}">{L_PROFILE}</a></li>
		<!-- BEGIN switch_user_logged_out -->
		<li><a href="{U_REGISTER}" class="{C_REGISTER}">{L_REGISTER}</a></li>
		<!-- END switch_user_logged_out -->
		<!-- BEGIN switch_user_logged_in -->
		<li><a href="{U_ADMIN}" class="{C_ADMIN}">{L_ADMIN}</a></li>
		<!-- END switch_user_logged_in -->
        <li><a href="{U_LOGIN_LOGOUT}" class="{C_LOGIN_LOGOUT}">{L_LOGIN_LOGOUT}</a></li>
    </ul>
</div>
<div id="content">
