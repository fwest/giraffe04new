<!-- BEGIN test_event -->
<center><span class="emphlarge">Scenario Earthquake</span></center><br /> 
<!-- END test_event -->

<!-- BEGIN comment_event -->
<div id="nav">
	<div class="emph, nav" ><span style="color:red">{comment_event.L_COMMENT}</span><span>{comment_event.L_COMMENT_TEXT}</span></div><br /> 
</div>
<!-- END comment_event -->

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
	<td align="right">{JUMPBOX}</td>
  </tr>
</table>
<div id="mapping" {MAPPING_STYLE}>
	<div id="map" {MAPPING_STYLE}></div>
	
	<div id="map-right">
		<div id="info">
		<h1>ShakeCast Summary</h1>
		{EQ}
		<br />
		<br />
		<span class="genmed">{FORUM_COUNT}</span> <br />
		<!-- BEGIN metrics -->
		<span class="genmed">{metrics.METRIC_NAME} {metrics.METRIC_UNIT} </span>: <span class="emphmed">{metrics.MIN_VALUE} - {metrics.MAX_VALUE}</span><br /> 
		<!-- END metrics -->
		</div>
		<br />
		<div id="info">
		<span class="eqinfolink"> <a href="{U_VIEWFORUM}" class="eqinfolink">{FORUM_NAME}</a></span> 
		<br /><span class="genmed">{FORUM_DESC}<br /></span>
		</div>
	</div>
</div>

<br />

<!-- BEGIN switch_user_logged_out -->
<form method="post" action="{S_LOGIN_ACTION}">
  <table width="100%" cellpadding="3" cellspacing="1" border="0" class="forumline">
	<tr> 
	  <td class="catHead" height="28"><a name="login"></a><span class="cattitle">{L_LOGIN_LOGOUT}</span></td>
	</tr>
	<tr> 
	  <td class="row1" align="center" valign="middle" height="28"><span class="gensmall">{L_USERNAME}: 
		<input class="post" type="text" name="username" size="10" />
		&nbsp;&nbsp;&nbsp;{L_PASSWORD}: 
		<input class="post" type="password" name="password" size="10" maxlength="32" />
		<!-- BEGIN switch_allow_autologin -->
		&nbsp;&nbsp; &nbsp;&nbsp;{L_AUTO_LOGIN} 
		<input class="text" type="checkbox" name="autologin" />
		<!-- END switch_allow_autologin -->
		&nbsp;&nbsp;&nbsp; 
		<input type="submit" class="mainoption" name="login" value="{L_LOGIN}" />
		</span> </td>
	</tr>
  </table>
</form>
<!-- END switch_user_logged_out -->

