<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html dir="{S_CONTENT_DIRECTION}">
<head>
<meta http-equiv="Content-Type" content="text/html; charset={S_CONTENT_ENCODING}">
<meta http-equiv="Content-Style-Type" content="text/css">
{META}
{NAV_LINKS}
<title>{SITENAME} :: {PAGE_TITLE}</title>
<!-- link rel="stylesheet" href="templates/Caltrans/{T_HEAD_STYLESHEET}" type="text/css" -->
<link rel="stylesheet" href="templates/subSilver/sc.css" type="text/css">
	<script type='text/javascript' src='common.js'></script>
	<script type='text/javascript' src='css.js'></script>
	<script src="default_sc.js" type="text/javascript"></script>
	<script src="facility_functions.js" type="text/javascript"></script>
	<style type="text/css">
	#map_wrap { 
		position: fixed; 
		top: 0px; 
		left:0px; 
		z-index:-2; 
		background: #fff;
		height: 330px;
		width: 610px;
		padding: 5px;
		border: 1px dashed #666;
		//background: url(/templates/subSilver/images/map_wrap_top.gif) no-repeat 0 0 100%;
}
		#map_pane { 
		position: relative; top: 10px; left:5px; z-index:-1; border-width: 2px; border-color:#eee;
			width: 600px; 
		height: 300px;
		/*background-image: url(images/1551_PGA.png);*/
	}
	
	</style>
	<script type='text/javascript'>
	function fixedposition() {
			var map_id = document.getElementById("map_pane");
			map_id.style.zIndex = -1;
			map_id.style.display = "none";
			var mapping_id = document.getElementById("map_wrap");
			mapping_id.style.zIndex = -2;
			mapping_id.style.display = "none";
			var dragable1 = DragHandler.attach(document.getElementById('map_wrap'));
}
</script>
</head>
<body bgcolor="{T_BODY_BGCOLOR}" text="{T_BODY_TEXT}" link="{T_BODY_LINK}" vlink="{T_BODY_VLINK}" onload="standardistaTableSortingInit();init();PMA_markRowsInit({FACILITY_ID});fixedposition();" onunload="GUnload()">
<script type="text/javascript" src="./js/niftycube.js"></script>
<script type="text/javascript">
	NiftyLoad=function(){
	Nifty("div#content");
	Nifty("div#eq_list_wrapper");
	Nifty("table#eq_list");
	}
</script>

<div id="map_wrap">
	<a href="#" onclick="hide_gm()" style="float:right;padding: 5px;">Close</a>
	<div id="map_pane"><img id="plot_pane" src="/images/1551_MMI.png"></div>
</div>

<div id="container">
	<div id="header">
    <h1>
        <a href="{U_INDEX}"><img src="templates/subSilver/images/header.jpg" alt="{L_INDEX}"></a>

    </h1>
		<ul id="globalnav">
		<li><a href="{U_INDEX}" class="{C_HOME}">Home</a></li>
		<li><a href="{U_EARTHQUAKE}" class="{C_EARTHQUAKE}">{L_EARTHQUAKE}</a>
		</li>
		<li><a href="{U_SEARCH}" class="{C_SEARCH}">{L_SEARCH}</a>
			<ul><li><a href="#"></a></li>
			</ul>
		</li>
		<li><a href="{U_FAQ}">{L_FAQ}</a></li>
		<li><a href="{U_PROFILE}">{L_PROFILE}</a></li>
		<!-- BEGIN switch_user_logged_out -->
		<li><a href="{U_REGISTER}">{L_REGISTER}</a></li>
		<!-- END switch_user_logged_out -->
		<!-- BEGIN switch_user_logged_in -->
		<li><a href="{U_ADMIN}">{L_ADMIN}</a></li>
		<!-- END switch_user_logged_in -->
		<li><a href="{U_LOGIN_LOGOUT}">{L_LOGIN_LOGOUT}</a></li>
		</ul>
	</div>
	
	<div id="content">
