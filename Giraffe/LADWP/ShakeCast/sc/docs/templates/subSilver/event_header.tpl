<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html dir="{S_CONTENT_DIRECTION}">
<head>
<meta http-equiv="Content-Type" content="text/html; charset={S_CONTENT_ENCODING}">
<meta http-equiv="Content-Style-Type" content="text/css">
{META}
{NAV_LINKS}
<title>{SITENAME} :: {PAGE_TITLE}</title>
<!-- link rel="stylesheet" href="templates/subSilver/{T_HEAD_STYLESHEET}" type="text/css" -->
<link rel="stylesheet" href="templates/subSilver/sc.css" type="text/css">
    <script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key={GM_API_KEY}" type="text/javascript"></script>
	<script type='text/javascript' src='common.js'></script>
	<script type='text/javascript' src='css.js'></script>
	<script src="default_sc.js" type="text/javascript"></script>
	<script src="event_functions.js" type="text/javascript"></script>
	<script src="shakemap.js" type="text/javascript"></script>
	<style type="text/css">
	#map_wrap { 
		position: fixed; 
		top: 0px; 
		left:0px; 
		z-index:-2; 
		background: #fff;
		height: 330px;
		width: 410px;
		padding: 5px;
		border: 1px dashed #666;
		//background: url(/templates/subSilver/images/map_wrap_top.gif) no-repeat 0 0 100%;
}
	#map_pane { 
	position: relative; top: 10px; left:5px; z-index:-1; border-width: 2px; border-color:#eee;
		width: 400px; 
	height: 300px;
}
	</style>
	<script type='text/javascript'>
	function fixedposition() {
			var map_id = document.getElementById("map_pane");
			map_id.style.zIndex = -1;
			map_id.style.display = "none";
			var mapping_id = document.getElementById("map_wrap");
			mapping_id.style.zIndex = -2;
			mapping_id.style.display = "none";
			var dragable1 = DragHandler.attach(document.getElementById('map_wrap'));
}
</script>
</head>
<body bgcolor="{T_BODY_BGCOLOR}" text="{T_BODY_TEXT}" link="{T_BODY_LINK}" vlink="{T_BODY_VLINK}" onload="init();PMA_markRowsInit();fixedposition();" onunload="GUnload()">

<div id="map_wrap">
	<a href="#" onclick="hide_gm()" style="float:right;padding: 5px;">Close</a>
	<a href="{U_GMAP}">Map View</a><br />
	<div id="map_pane"></div>
</div>

<div id="container">
<div id="header">
    <h1>
        <a href="{U_INDEX}"><img src="templates/subSilver/images/header.jpg" alt="{L_INDEX}"></a>

    </h1>
    <ul id="globalnav">
        <li><a href="{U_INDEX}" class="{C_HOME}">Home</a>
			<ul>
        <li><a href="{U_SEARCH}" class="{C_SEARCH}">{L_SEARCH}</a></li>
		<li><a href="{U_FAQ}" class="{C_FAQ}">{L_FAQ}</a></li>
			</ul>
		</li>
        <li><a href="{U_EARTHQUAKE}" class="{C_EARTHQUAKE}">{L_EARTHQUAKE}</a>
			<ul>
				<li><a href="{U_EQ_LATEST}">{L_EQ_LATEST}</a></li>
				<li><a href="{U_EQ_LIST}">{L_EQ_LIST}</a></li>
				<li><a href="{U_EQ_TEST}">{L_EQ_TEST}</a></li>
			</ul>
		</li>
        <li><a href="{U_SEARCH}">{L_SEARCH}</a></li>
		<li><a href="{U_FAQ}">{L_FAQ}</a></li>
        <li><a href="{U_PROFILE}">{L_PROFILE}</a></li>
		<!-- BEGIN switch_user_logged_out -->
		<li><a href="{U_REGISTER}">{L_REGISTER}</a></li>
		<!-- END switch_user_logged_out -->
		<!-- BEGIN switch_user_logged_in -->
		<li><a href="{U_ADMIN}">{L_ADMIN}</a></li>
		<!-- END switch_user_logged_in -->
        <li><a href="{U_LOGIN_LOGOUT}">{L_LOGIN_LOGOUT}</a></li>
    </ul>
</div>
<div id="content">
