		<div id="nav">
			<span class="gensmall"><a href="{U_INDEX}" class="nav">{L_INDEX}</a> -> <a class="nav" href="{U_VIEW_FORUM}">{FORUM_NAME}</a></span>
		</div>

		<form method="post" action="{S_NOTIFY_ACTION}">
		<div id="mapping">
			<div id="map_pane" style="height:450px; width:50%; border: 1px solid #999; float: left;"></div>

			<div id="map-right" style="margin-left: 15px; width: 46%; top:10px; float: left;">
				<h1>{L_PROFILE_MESSAGE}</h1>
				<div id="info">
					<table class="sortable">
					<thead>
					<tr>
					<th field="Id" dataType="Number">ID</th>
					<th field="Name" dataType="String">{L_NAME}</th>
					<th field="Description" dataType="String">{L_DESCRIPTION}</th>
					<th field="Subscribe" dataType="String">{L_SUBSCRIBE}</th>
					</tr>
					</thead>
					<tbody>
					<!-- BEGIN postrow -->
					<tr class="{postrow.CLASS_ID}" id="{postrow.ID}">
					<td class="id">{postrow.ID}</td>
					<td>{postrow.NAME}</td>
					<td>{postrow.DESCRIPTION}</td>
					<td><input type="checkbox" id="ckbox_{postrow.ID}" name="user_profile[]" onclick="copyCheckboxesRange('ckbox_{postrow.ID}');" value="{postrow.ID}" {postrow.SUBSCRIBE}/></td>
					{postrow.METRIC}
					</tr>
					<!-- END postrow -->
					</tbody>
					</table>
				</div>
				<br clear="all" />{S_HIDDEN_FIELDS}<input type="image" src="templates/Caltrans/images/updateNoteProfiles.gif" width="192" height="22" border="0" name="save" value="{L_SUBMIT}" />
			</div>
			<br clear="all" />
			<div class="clearfix"></div>
			
		</div>
		</form>

