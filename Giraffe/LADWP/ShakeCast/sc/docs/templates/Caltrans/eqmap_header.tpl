<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html dir="{S_CONTENT_DIRECTION}">
<head>
<meta http-equiv="Content-Type" content="text/html; charset={S_CONTENT_ENCODING}">
<meta http-equiv="Content-Style-Type" content="text/css">
{META}
{NAV_LINKS}
<title>{SITENAME} :: {PAGE_TITLE}</title>
<!-- link rel="stylesheet" href="templates/Caltrans/{T_HEAD_STYLESHEET}" type="text/css" -->
<link rel="stylesheet" href="templates/Caltrans/sc.css" type="text/css">

    <script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key={GM_API_KEY}&amp;f=q&amp;q=http://earthquake.usgs.gov/eqcenter/shakemap/global/shake/2007ciaw/download/2007ciaw.kml" type="text/javascript"></script>
	<script src="shakemap.js" type="text/javascript"></script>
	<script src="default_sc.js" type="text/javascript"></script>
	<script src="map_functions.js" type="text/javascript"></script>
	<link href="eqmap.css" rel="stylesheet" type="text/css" />
</head>
<body bgcolor="{T_BODY_BGCOLOR}" text="{T_BODY_TEXT}" link="{T_BODY_LINK}" vlink="{T_BODY_VLINK}">
<div id="container">
<div id="header">
    <ul id="globalnav">
	<li><a href="{U_INDEX}" class="{C_HOME}">Home</a></li>
	<li class="here"><a href="{U_EARTHQUAKE}" class="{C_EARTHQUAKE}">{L_EARTHQUAKE}</a>
		<ul>
		<li><a href="{U_EQ_LATEST}">{L_EQ_LATEST}</a></li>
		<li><a href="{U_EQ_LIST}">{L_EQ_LIST}</a></li>
		<li><a href="{U_EQ_TEST}">{L_EQ_TEST}</a></li>
		</ul>
	</li>
	<li><a href="{U_SEARCH}">{L_SEARCH}</a></li>
	<li><a href="{U_FAQ}">{L_FAQ}</a></li>
	<li><a href="{U_PROFILE}">{L_PROFILE}</a></li>
	<!-- BEGIN switch_user_logged_out -->
	<li><a href="{U_REGISTER}">{L_REGISTER}</a></li>
	<!-- END switch_user_logged_out -->
	<!-- BEGIN switch_user_logged_in -->
	<li><a href="{U_ADMIN}">{L_ADMIN}</a></li>
	<!-- END switch_user_logged_in -->
	<li><a href="{U_LOGIN_LOGOUT}">{L_LOGIN_LOGOUT}</a></li>
    </ul>
</div>
<div id="content">
