<script type="text/javascript"><!--

var formblock;
var forminputs;

function prepare() {
formblock= document.getElementById('form_id');
forminputs = formblock.getElementsByTagName('input');
}

function select_all(name, value) {
for (i = 0; i < forminputs.length; i++) {
// regex here to check name attribute
var regex = new RegExp(name, "i");
if (regex.test(forminputs[i].getAttribute('name'))) {
if (value == true) {
forminputs[i].checked = true;
} else {
forminputs[i].checked = false;
}
}
}
}

if (window.addEventListener) {
window.addEventListener("load", prepare, false);
} else if (window.attachEvent) {
window.attachEvent("onload", prepare)
} else if (document.getElementById) {
window.onload = prepare;
}

//--></script>

<h1>{L_WORDS_TITLE}</h1>

<P>{L_WORDS_TEXT}</p>

<form id="form_id" name="myform" method="post" action="{S_WORDS_ACTION}"><table cellspacing="1" cellpadding="4" border="0" align="center" class="forumline">
	<tr> 
	  <th class="thHead" colspan="6">{L_USER_NOTIFICATION} {USER_NAME}</th>
	</tr>
	<tr> 
	  <td class="row2" colspan="6" align="center"><span class="gensmall">{L_ITEMS_REQUIRED}</span>
	  <input type="submit" name="{U_SUB}" value="{L_SUB}" class="mainoption" {STATUS_SUB} />
	  <input type="submit" name="{U_FULL}" value="{L_FULL}" class="mainoption" {STATUS_FULL} />
	  </td>
	</tr>
	<tr>
		<th class="thCornerL">{L_FAC_ID}</th>
		<th class="thTop">{L_FAC_TYPE}</th>
		<th class="thTop">{L_FAC_NAME}</th>
		<th class="thTop">{L_LATITUDE}</th>
		<th class="thTop">{L_LONGITUDE}</th>
		<th class="thCornerR">{L_ACTION}<br>
		<input type=checkbox name="checkall" value="1" {CHECKED} onClick="select_all('facility', this.checked);">
		</th>
	</tr>
	<!-- BEGIN words -->
	<tr>
		<td class="{words.ROW_CLASS}" align="center">{words.FAC_ID}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.FAC_TYPE}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.FAC_NAME}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.LAT_MIN}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.LON_MIN}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.U_EDIT}{L_EDIT}{words.POLY}</a></td>
	</tr>
	<!-- END words -->
	<tr>
		<td colspan="6" align="center" class="catBottom">{S_HIDDEN_FIELDS}<input type="submit" name="update" value="{L_ADD_NEXT}" class="mainoption" /></td>
	</tr>
</table></form>

<table width="100%" cellspacing="2" border="0" align="center" cellpadding="2">
  <tr> 
	<td align="left" valign="top"><span class="nav">{PAGE_NUMBER}</span></td>
	<td align="right" valign="top" nowrap="nowrap"><span class="nav">{PAGINATION}</span><br /><span class="gensmall">{S_TIMEZONE}</span></td>
  </tr>
</table>
