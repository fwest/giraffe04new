
<h1>{L_WORDS_TITLE}</h1>

<P>{L_WORDS_TEXT}</p>

<form method="post" action="{S_WORDS_ACTION}"><table cellspacing="1" cellpadding="4" border="0" align="center" class="forumline">
	<tr>
		<th class="thCornerL">{L_SERVER_ID}</th>
		<th class="thTop">{L_DNS_ADDRESS}</th>
		<th class="thTop">{L_OWNER_ORGANIZATION}</th>
		<th class="thTop">{L_LAST_HEARD_FROM}</th>
		<th class="thTop">{L_SERVER_STATUS}</th>
		<th class="thTop">{L_ERROR_COUNT}</th>
		<th class="thTop">{L_UPSTREAM_FLAG}</th>
		<th class="thTop">{L_DOWNSTREAM_FLAG}</th>
		<th class="thTop">{L_POLL_FLAG}</th>
		<th class="thTop">{L_QUERY_FLAG}</th>
		<th class="thTop">{L_SELF_FLAG}</th>
		<th colspan="3" class="thCornerR">{L_ACTION}</th>
	</tr>
	<!-- BEGIN words -->
	<tr>
		<td class="{words.ROW_CLASS}" align="center">{words.SERVER_ID}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.DNS_ADDRESS}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.OWNER_ORGANIZATION}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.LAST_HEARD_FROM}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.SERVER_STATUS}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.ERROR_COUNT}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.UPSTREAM_FLAG}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.DOWNSTREAM_FLAG}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.POLL_FLAG}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.QUERY_FLAG}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.SELF_FLAG}</td>
		<td class="{words.ROW_CLASS}"><a href="{words.U_EDIT}">{L_EDIT}</a></td>
		<td class="{words.ROW_CLASS}"><a href="{words.U_DELETE}">{L_DELETE}</a></td>
		<td class="{words.ROW_CLASS}"><a href="{words.U_PASSWORD}">{L_PASSWORD}</a></td>
	</tr>
	<!-- END words -->
	<tr>
		<td colspan="14" align="center" class="catBottom">{S_HIDDEN_FIELDS}<input type="submit" name="add_event" value="{L_ADD_EVENT}" class="mainoption" /></td>
	</tr>
</table></form>
