
<h1>{L_WORDS_TITLE}</h1>

<P>{L_WORDS_TEXT}</p>

<form method="post" action="{U_SEARCH}">
        <script type="text/javascript" src="/includes/json.js"></script>
        <script type="text/javascript" src="/includes/zxml.js"></script>
        <script type="text/javascript" src="facility_type.js"></script>

	<table width="90%" border="0" align="center">
	  <tr> 
		<td align="right" valign="top" nowrap="nowrap">
		<input type="hidden" name="facility" id="facility" size="40" autocomplete="off" />
<!--
			<input type="submit" value="Lookup Facility" />
-->
			</td>
	  </tr>
	</table>
</form>

<form method="post" action="{S_FACILITY_TYPE_ACTION}">
<table cellspacing="1" cellpadding="4" border="0" align="center" class="forumline">
	<tr>
		<th colspan="2" class="thHead">{L_FACILITY_TYPE_EDIT}{F_HIDDEN_FIELDS}</th>
	</tr>
	<tr>
		<td class="row1">{L_FACILITY_TYPE}</td>
		<td class="row2">{FACILITY_TYPE}</td>
	</tr>
	<tr>
		<td class="row1">{L_NAME}</td>
		<td class="row2"><input class="post" type="text" name="name" id="name" value="{NAME}" size="30"/></td>
	</tr>
	<tr>
		<td class="row1">{L_DESCRIPTION}</td>
		<td class="row2"><input class="post" type="text" name="description" id="description" value="{DESCRIPTION}" size="30"/></td>
	</tr>
	<tr>
		<td class="catBottom" colspan="2" align="center">{S_HIDDEN_FIELDS}<input type="submit" name="save" value="{L_SUBMIT}" class="mainoption" /></td>
	</tr>
</table>

<br />

<table cellspacing="1" cellpadding="4" border="0" align="center" class="forumline">
	<tr class="thHead">
		<th>{L_DAMAGE_LEVEL}</th>
		<th>{L_LOW_LIMIT}</th>
		<th>{L_HIGH_LIMIT}</th>
		<th>{L_METRIC}</th>
	</tr>
<!-- BEGIN fragrow -->
	<tr>
		<td class="{fragrow.ROW_CLASS}">{fragrow.DAMAGE_LEVEL}{fragrow.FR_HIDDEN_FIELDS}</td>
		<td class="{fragrow.ROW_CLASS}"><input class="post" type="text" name="FR{fragrow.FRAG_ID}[low_limit]"  id="{fragrow.DAMAGE}_low_limit" value="{fragrow.LOW_LIMIT}" />*</td>
		<td class="{fragrow.ROW_CLASS}"><input class="post" type="text" name="FR{fragrow.FRAG_ID}[high_limit]" id="{fragrow.DAMAGE}_high_limit" value="{fragrow.HIGH_LIMIT}" />*</td>
		<td class="{fragrow.ROW_CLASS}">{fragrow.METRIC}*</td>
	</tr>
<!-- END fragrow -->
</table>
</form>
