
<h1>{L_SYSTEM_SERVICE_TITLE}</h1>

<p>{L_SERVICE_EXPLAIN}</p>

<form action="{S_CONFIG_ACTION}" method="post"><table width="99%" cellpadding="4" cellspacing="1" border="0" align="center" class="forumline">
	<tr>
	  <th class="thHead" colspan="2">{L_GENERAL_SETTINGS}</th>
	</tr>
	<tr>
		<td class="row2" colspan="2"><span class="gensmall">{L_GENERAL_SETTINGS_EXPLAIN}</span></td>
	</tr>
	<tr>
		<td class="row1">{L_ROOTDIR}</td>
		<td class="row2"><input class="post" type="text" maxlength="255" size="40" name="RootDir" value="{RootDir}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_DATAROOT}</td>
		<td class="row2"><input class="post" type="text" maxlength="255" size="40" name="DataRoot" value="{DataRoot}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_TEMPLATEDIR}</td>
		<td class="row2"><input class="post" type="text" maxlength="255" size="40" name="TemplateDir" value="{TemplateDir}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_LOGDIR}<br /><span class="gensmall">{L_SITE_NAME_EXPLAIN}</span></td>
		<td class="row2"><input class="post" type="text" size="40" maxlength="255" name="LogDir" value="{LogDir}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_LOGFILE}<br /><span class="gensmall">{L_GM_API_KEY_EXPLAIN}</span></td>
		<td class="row2"><input class="post" type="text" size="40" maxlength="255" name="LogFile" value="{LogFile}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_THRESHOLD}<br /><span class="gensmall">{L_THRESHOLD_EXPLAIN}</span></td>
		<td class="row2"><input class="post" type="text" size="20" maxlength="100" name="Threshold" value="{Threshold}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_LOGLEVEL}</td>
		<td class="row2">{LogLevel}</td>
	</tr>
	<tr>
		<td class="row1">{L_USERID}<br /><span class="gensmall">{L_VISUAL_CONFIRM_EXPLAIN}</span></td>
		<td class="row2"><input class="post" type="text" size="25" maxlength="100" name="UserID" value="{UserID}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_GROUPID}<br /><span class="gensmall">{L_ALLOW_AUTOLOGIN_EXPLAIN}</span></td>
		<td class="row2"><input class="post" type="text" size="25" maxlength="100" name="GroupID" value="{GroupID}" /></td>
	</tr>
	<tr>
	  <th class="thHead" colspan="2">{L_LOCAL_DESTINATION}</th>
	</tr>
	<tr>
		<td class="row2" colspan="2"><span class="gensmall">{L_LOCAL_DESTINATION_EXPLAIN}</span></td>
	</tr>
	<tr>
		<td class="row1">{L_LOCALSERVERID} <br /><span class="gensmall">{L_AUTOLOGIN_TIME_EXPLAIN}</span></td>
		<td class="row2"><input class="post" type="text" size="25" maxlength="100" name="LocalServerId" value="{LocalServerId}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_Destination_HOSTNAME}<br /><span class="gensmall">{L_BOARD_EMAIL_FORM_EXPLAIN}</span></td>
		<td class="row2"><input class="post" type="text" size="25" maxlength="100" name="Destination_Hostname" value="{Destination_Hostname}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_DESTINATION_PASSWORD} <br /><span class="gensmall">{L_FLOOD_INTERVAL_EXPLAIN}</span></td>
		<td class="row2"><input class="post" type="text" size="25" maxlength="100" name="Destination_Password" value="{Destination_Password}" /></td>
	</tr>
	<tr>
	  <th class="thHead" colspan="2">{L_ACCESS_ADMIN}</th>
	</tr>
	<tr>
		<td class="row2" colspan="2"><span class="gensmall">{L_ACCESS_ADMIN_EXPLAIN}</span></td>
	</tr>
	<tr>
		<td class="row1">{L_ADMIN_HTPASSWORDPATH} <br /><span class="gensmall">{L_SEARCH_FLOOD_INTERVAL_EXPLAIN}</span></td>
		<td class="row2"><input class="post" type="text" size="40" maxlength="255" name="Admin_HtPasswordPath" value="{Admin_HtPasswordPath}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_ADMIN_SERVERPWDFILE}<br /><span class="gensmall">{L_MAX_LOGIN_ATTEMPTS_EXPLAIN}</span></td>
		<td class="row2"><input class="post" type="text" size="40" maxlength="255" name="Admin_ServerPwdFile" value="{Admin_ServerPwdFile}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_ADMIN_USERPWDFILE}<br /><span class="gensmall">{L_LOGIN_RESET_TIME_EXPLAIN}</span></td>
		<td class="row2"><input class="post" type="text" size="40" maxlength="255" name="Admin_UserPwdFile" value="{Admin_UserPwdFile}" /></td>
	</tr>
	<tr>
	  <th class="thHead" colspan="2">{L_DISPATCHER}</th>
	</tr>
	<tr>
		<td class="row2" colspan="2"><span class="gensmall">{L_DISPATCHER_EXPLAIN}</span></td>
	</tr>
	<tr>
		<td class="row1">{L_DISPATCHER_AUTOSTART}</td>
		<td class="row2"><input type="radio" name="Dispatcher_AUTOSTART" value="1" {Dispatcher_AUTOSTART} /> {L_YES}&nbsp;&nbsp;<input type="radio" name="Dispatcher_AUTOSTART" value="" {Dispatcher_AUTOSTART_NO} /> {L_NO}</td>
	</tr>
	<tr>
		<td class="row1">{L_DISPATCHER_LOG}</td>
		<td class="row2"><input class="post" type="text" name="Dispatcher_LOG" size="40" maxlength="255" value="{Dispatcher_LOG}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_DISPATCHER_LOGGING}</td>
		<td class="row2">{Dispatcher_LOGGING}</td>
	</tr>
	<tr>
		<td class="row1">{L_DISPATCHER_MAXWORKERS}</td>
		<td class="row2"><input class="post" type="text" name="Dispatcher_MaxWorkers" size="3" maxlength="4" value="{Dispatcher_MaxWorkers}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_DISPATCHER_MINWORKERS}<br /><span class="gensmall">{L_OVERRIDE_STYLE_EXPLAIN}</span></td>
		<td class="row2"><input class="post" type="text" name="Dispatcher_MinWorkers" size="3" maxlength="4" value="{Dispatcher_MinWorkers}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_DISPATCHER_POLL}</td>
		<td class="row2"><input class="post" type="text" name="Dispatcher_POLL" size="3" maxlength="4" value="{Dispatcher_POLL}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_DISPATCHER_PORT}<br /><span class="gensmall">{L_DATE_FORMAT_EXPLAIN}</span></td>
		<td class="row2"><input class="post" type="text" size="25" maxlength="100" name="Dispatcher_PORT" value="{Dispatcher_PORT}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_DISPATCHER_PROMPT}</td>
		<td class="row2"><input class="post" type="text" size="25" maxlength="100" name="Dispatcher_PROMPT" value="{Dispatcher_PROMPT}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_DISPATCHER_REQUESTPORT}</td>
		<td class="row2"><input class="post" type="text" size="25" maxlength="100" name="Dispatcher_RequestPort" value="{Dispatcher_RequestPort}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_DISPATCHER_SERVICE_NAME}</td>
		<td class="row2"><input class="post" type="text" size="25" maxlength="100" name="Dispatcher_SERVICE_NAME" value="{Dispatcher_SERVICE_NAME}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_DISPATCHER_SERVICE_TITLE}</td>
		<td class="row2"><input class="post" type="text" size="25" maxlength="100" name="Dispatcher_SERVICE_TITLE" value="{Dispatcher_SERVICE_TITLE}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_DISPATCHER_SPOLL}</td>
		<td class="row2"><input class="post" type="text" name="Dispatcher_SPOLL" size="3" maxlength="4" value="{Dispatcher_SPOLL}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_DISPATCHER_WORKERPORT}<br /><span class="gensmall">{L_COOKIE_SECURE_EXPLAIN}</span></td>
		<td class="row2"><input class="post" type="text" size="25" maxlength="100" name="Dispatcher_WorkerPort" value="{Dispatcher_WorkerPort}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_DISPATCHER_WORKERTIMEOUT}</td>
		<td class="row2"><input class="post" type="text" size="25" maxlength="100" name="Dispatcher_WorkerTimeout" value="{Dispatcher_WorkerTimeout}" /></td>
	</tr>
	<tr>
	  <th class="thHead" colspan="2">{L_POLLER}</th>
	</tr>
	<tr>
		<td class="row2" colspan="2"><span class="gensmall">{L_POLLER_EXPLAIN}</span></td>
	</tr>
	<tr>
		<td class="row1">{L_POLLER_AUTOSTART}</td>
		<td class="row2"><input type="radio" name="Poller_AUTOSTART" value="1" {Poller_AUTOSTART} /> {L_YES}&nbsp;&nbsp;<input type="radio" name="Poller_AUTOSTART" value="" {Poller_AUTOSTART_NO} /> {L_NO}</td>
	</tr>
	<tr>
		<td class="row1">{L_POLLER_LOG}</td>
		<td class="row2"><input class="post" type="text" name="Poller_LOG" size="40" maxlength="255" value="{Poller_LOG}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_POLLER_LOGGING}</td>
		<td class="row2">{Poller_LOGGING}</td>
	</tr>
	<tr>
		<td class="row1">{L_POLLER_MSGLEVEL}</td>
		<td class="row2">{Poller_MSGLEVEL}</td>
	</tr>
	<tr>
		<td class="row1">{L_POLLER_POLL}<br /><span class="gensmall">{L_OVERRIDE_STYLE_EXPLAIN}</span></td>
		<td class="row2"><input class="post" type="text" size="5" maxlength="5" name="Poller_POLL" value="{Poller_POLL}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_POLLER_PORT}</td>
		<td class="row2"><input class="post" type="text" size="25" maxlength="100" name="Poller_PORT" value="{Poller_PORT}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_POLLER_PROMPT}<br /><span class="gensmall">{L_DATE_FORMAT_EXPLAIN}</span></td>
		<td class="row2"><input class="post" type="text" size="25" maxlength="100" name="Poller_PROMPT" value="{Poller_PROMPT}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_POLLER_SERVICE_NAME}</td>
		<td class="row2"><input class="post" type="text" size="25" maxlength="100" name="Poller_SERVICE_NAME" value="{Poller_SERVICE_NAME}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_POLLER_SERVICE_TITLE}</td>
		<td class="row2"><input class="post" type="text" size="25" maxlength="100" name="Poller_SERVICE_TITLE" value="{Poller_SERVICE_TITLE}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_POLLER_SPOLL}</td>
		<td class="row2"><input class="post" type="text" size="5" maxlength="5" name="Poller_SPOLL" value="{Poller_SPOLL}" /></td>
	</tr>
	<tr>
	  <th class="thHead" colspan="2">{L_NOTIFYQUEUE}</th>
	</tr>
	<tr>
		<td class="row2" colspan="2"><span class="gensmall">{L_NOTIFYQUEUE_EXPLAIN}</span></td>
	</tr>
	<tr>
		<td class="row1">{L_NOTIFYQUEUE_LOGLEVEL}</td>
		<td class="row2">{NotifyQueue_LogLevel}</td>
	</tr>
	<tr>
		<td class="row1">{L_NOTIFYQUEUE_SCANPERIOD}</td>
		<td class="row2"><input class="post" type="text" name="NotifyQueue_ScanPeriod" size="5" maxlength="5" value="{NotifyQueue_ScanPeriod}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_NOTIFYQUEUE_SERVICETITLE}</td>
		<td class="row2"><input class="post" type="text" name="NotifyQueue_ServiceTitle" size="30" maxlength="255" value="{NotifyQueue_ServiceTitle}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_NOTIFYQUEUE_SPOLL}<br /><span class="gensmall">{L_OVERRIDE_STYLE_EXPLAIN}</span></td>
		<td class="row2"><input class="post" type="text" name="NotifyQueue_Spoll" size="5" maxlength="5" value="{NotifyQueue_Spoll}" /></td>
	</tr>
	<tr>
	  <th class="thHead" colspan="2">{L_NOTIFY}</th>
	</tr>
	<tr>
		<td class="row2" colspan="2"><span class="gensmall">{L_NOTIFY_EXPLAIN}</span></td>
	</tr>
	<tr>
		<td class="row1">{L_NOTIFY_LOGLEVEL}</td>
		<td class="row2">{Notify_LogLevel}</td>
	</tr>
	<tr>
		<td class="row1">{L_NOTIFY_SCANPERIOD}</td>
		<td class="row2"><input class="post" type="text" name="Notify_ScanPeriod" size="5" maxlength="5" value="{Notify_ScanPeriod}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_NOTIFY_SERVICETITLE}</td>
		<td class="row2"><input class="post" type="text" name="Notify_ServiceTitle" size="30" maxlength="255" value="{Notify_ServiceTitle}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_NOTIFY_SPOLL}<br /><span class="gensmall">{L_OVERRIDE_STYLE_EXPLAIN}</span></td>
		<td class="row2"><input class="post" type="text" name="Notify_Spoll" size="5" maxlength="5" value="{Notify_Spoll}" /></td>
	</tr>
	<tr>
	  <th class="thHead" colspan="2">{L_RSS}</th>
	</tr>
	<tr>
		<td class="row2" colspan="2"><span class="gensmall">{L_RSS_EXPLAIN}</span></td>
	</tr>
	<tr>
		<td class="row1">{L_RSS_AUTOSTART}</td>
		<td class="row2"><input type="radio" name="rss_AUTOSTART" value="1" {rss_AUTOSTART} /> {L_YES}&nbsp;&nbsp;<input type="radio" name="rss_AUTOSTART" value="" {rss_AUTOSTART_NO} /> {L_NO}</td>
	</tr>
	<tr>
		<td class="row1">{L_RSS_LOG}</td>
		<td class="row2"><input class="post" type="text" name="rss_LOG" size="40" maxlength="255" value="{rss_LOG}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_RSS_LOGGING}<br /><span class="gensmall">{L_OVERRIDE_STYLE_EXPLAIN}</span></td>
		<td class="row2">{rss_LOGGING}</td>
	</tr>
	<tr>
		<td class="row1">{L_RSS_MSGLEVEL}</td>
		<td class="row2">{rss_MSGLEVEL}</td>
	</tr>
	<tr>
		<td class="row1">{L_RSS_POLL}</td>
		<td class="row2"><input class="post" type="text" maxlength="5" size="5" name="rss_POLL" value="{rss_POLL}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_RSS_PORT}</td>
		<td class="row2"><input class="post" type="text" maxlength="100" size="25" name="rss_PORT" value="{rss_PORT}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_RSS_PROMPT}</td>
		<td class="row2"><input class="post" type="text" maxlength="100" size="25" name="rss_PROMPT" value="{rss_PROMPT}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_RSS_SERVICE_NAME}</td>
		<td class="row2"><input class="post" type="text" size="30" maxlength="255" name="rss_SERVICE_NAME" value="{rss_SERVICE_NAME}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_RSS_SERVICE_TITLE}<br /><span class="gensmall">{L_ALLOWED_TAGS_EXPLAIN}</span></td>
		<td class="row2"><input class="post" type="text" size="30" maxlength="255" name="rss_SERVICE_TITLE" value="{rss_SERVICE_TITLE}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_RSS_SPOLL}</td>
		<td class="row2"><input class="post" type="text" name="rss_SPOLL" size="5" maxlength="5" value="{rss_SPOLL}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_RSS_REGION}</td>
		<td class="row2"><input class="post" type="text" name="rss_REGION" size="40" maxlength="255" value="{rss_REGION}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_RSS_TIME_WINDOW}</td>
		<td class="row2"><input class="post" type="text" name="rss_TIME_WINDOW" size="5" maxlength="5" value="{rss_TIME_WINDOW}" /></td>
	</tr>
	<tr>
	  <th class="thHead" colspan="2">{L_NOTIFICATION}</th>
	</tr>
	<tr>
		<td class="row2" colspan="2"><span class="gensmall">{L_NOTIFICATION_EXPLAIN}</span></td>
	</tr>
	<tr>
		<td class="row1">{L_NOTIFICATION_FROM} <br /><span class="gensmall">{L_SMILIES_PATH_EXPLAIN}</span></td>
		<td class="row2"><input class="post" type="text" size="40" maxlength="255" name="Notification_From" value="{Notification_From}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_NOTIFICATION_ENVELOPEFROM}</td>
		<td class="row2"><input class="post" type="text" size="40" maxlength="255" name="Notification_EnvelopeFrom" value="{Notification_EnvelopeFrom}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_NOTIFICATION_SMTPSERVER}<br /><span class="gensmall">{L_MAX_SIG_LENGTH_EXPLAIN}</span></td>
		<td class="row2"><input class="post" type="text" size="30" maxlength="100" name="Notification_SmtpServer" value="{Notification_SmtpServer}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_NOTIFICATION_DEFAULTEMAILTEMPLATE}</td>
		<td class="row2"><input class="post" type="text" size="30" maxlength="100" name="Notification_DefaultEmailTemplate" value="{Notification_DefaultEmailTemplate}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_NOTIFICATION_DEFAULTSCRIPTTEMPLATE}</td>
		<td class="row2"><input class="post" type="text" size="30" maxlength="100" name="Notification_DefaultScriptTemplate" value="{Notification_DefaultScriptTemplate}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_NOTIFICATION_USERNAME} <br /><span class="gensmall">{L_ALLOW_REMOTE_EXPLAIN}</span></td>
		<td class="row2"><input class="post" type="text" size="25" maxlength="100" name="Notification_Username" value="{Notification_Username}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_NOTIFICATION_PASSWORD}</td>
		<td class="row2"><input class="post" type="text" size="25" maxlength="100" name="Notification_Password" value="{Notification_Password}" /></td>
	</tr>
	<tr>
	  <th class="thHead" colspan="2">{L_LOGROTATE}</th>
	</tr>
	<tr>
		<td class="row2" colspan="2"><span class="gensmall">{L_LOGROTATE_EXPLAIN}</span></td>
	</tr>
	<tr>
		<td class="row1">{L_LOGROTATE_LOGSTATDIR} <br /><span class="gensmall">{L_SMILIES_PATH_EXPLAIN}</span></td>
		<td class="row2"><input class="post" type="text" size="40" maxlength="255" name="Logrotate_LOGSTATDIR" value="{Logrotate_LOGSTATDIR}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_LOGROTATE_LOGFILE}</td>
		<td class="row2"><input class="post" type="text" size="40" maxlength="255" name="Logrotate_logfile" value="{Logrotate_logfile}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_LOGROTATE_ROTATE-TIME}<br /><span class="gensmall">{L_MAX_SIG_LENGTH_EXPLAIN}</span></td>
		<td class="row2"><input class="post" type="text" size="20" maxlength="100" name="Logrotate_rotate-time" value="{Logrotate_rotate-time}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_LOGROTATE_MAX_SIZE}</td>
		<td class="row2"><input class="post" type="text" size="20" maxlength="100" name="Logrotate_max_size" value="{Logrotate_max_size}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_LOGROTATE_KEEP-FILES}</td>
		<td class="row2"><input class="post" type="text" size="20" maxlength="100" name="Logrotate_keep-files" value="{Logrotate_keep-files}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_LOGROTATE_COMPRESS} <br /><span class="gensmall">{L_ALLOW_REMOTE_EXPLAIN}</span></td>
		<td class="row2"><input type="radio" name="Logrotate_compress" value="Yes" {Logrotate_compress} /> {L_YES}&nbsp;&nbsp;<input type="radio" name="Logrotate_compress" value="" {Logrotate_compress_NO} /> {L_NO}</td>
	</tr>
	<tr>
		<td class="row1">{L_LOGROTATE_STATUS-FILE}</td>
		<td class="row2"><input class="post" type="text" size="40" maxlength="255" name="Logrotate_status-file" value="{Logrotate_status-file}" /></td>
	</tr>
	<tr>
		<td class="catBottom" colspan="2" align="center">{S_HIDDEN_FIELDS}<input type="submit" name="submit" value="{L_SUBMIT}" class="mainoption" />&nbsp;&nbsp;<input type="reset" value="{L_RESET}" class="liteoption" />
		</td>
	</tr>
</table></form>

<br clear="all" />
