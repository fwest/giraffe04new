
<h1>{L_WORDS_TITLE}</h1>

<P>{L_WORDS_TEXT}</p>

<form method="post" action="{S_WORDS_ACTION}"><table cellspacing="1" cellpadding="4" border="0" align="center" class="forumline">
	<tr>
		<th class="thCornerL">{L_FACILITY_TYPE}</th>
		<th class="thTop">{L_ATTRIBUTE_NAME}</th>
		<th class="thTop">{L_DESCRIPTION}</th>
		<th colspan="2" class="thCornerR">{L_ACTION}</th>
	</tr>
	<!-- BEGIN words -->
	<tr>
		<td class="{words.ROW_CLASS}" align="center">{words.FACILITY_TYPE}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.ATTRIBUTE_NAME}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.DESCRIPTION}</td>
		<td class="{words.ROW_CLASS}"><a href="{words.U_EDIT}">{L_EDIT}</a></td>
		<td class="{words.ROW_CLASS}"><a href="{words.U_DELETE}">{L_DELETE}</a></td>
	</tr>
	<!-- END words -->
	<tr>
		<td colspan="8" align="center" class="catBottom">{S_HIDDEN_FIELDS}<input type="submit" name="add_event" value="{L_ADD_EVENT}" class="mainoption" /></td>
	</tr>
</table></form>

<table width="100%" cellspacing="2" border="0" align="center" cellpadding="2">
  <tr> 
	<td align="left" valign="top"><span class="nav">{PAGE_NUMBER}</span></td>
	<td align="right" valign="top" nowrap="nowrap"><span class="nav">{PAGINATION}</span><br /><span class="gensmall">{S_TIMEZONE}</span></td>
  </tr>
</table>
