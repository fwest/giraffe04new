
<h1>{L_TEMPLATE_TITLE}</h1>

<p>{L_TEMPLATE_EXPLAIN}</p>

<form method="post" action="{S_TEMPLATE_ACTION}"><table class="forumline" cellspacing="1" cellpadding="4" border="0" align="center">
	<tr>
		<th class="thHead" colspan="2">{L_TEMPLATE_CONFIG}</th>
	</tr>
	<tr>
		<td class="row1">{L_NAME}*</td>
		<td class="row2"><input class="post" type="text" name="name" value="{NAME}"  size="30"/></td>
	</tr>
	<tr>
		<td class="row1">{L_FILE_NAME}*</td>
		<td class="row2"><input class="post" type="text" name="file_name" value="{FILE_NAME}"  size="30"/></td>
	</tr>
	<tr>
		<td class="row1">{L_DESCRIPTION}</td>
		<td class="row2"><input class="post" type="text" name="description" value="{DESCRIPTION}" size="60"/></td>
	</tr>
	<!-- BEGIN words -->
	<tr>
		<td class="row1">{words.L_NOTIFICATION_TYPE}</td>
		<td class="row2">{words.NOTIFICATION_TYPE}</td>
	</tr>
	<tr>
		<td class="row1">{words.L_DELIVERY_METHOD}</td>
		<td class="row2">{words.DELIVERY_METHOD}</td>
	</tr>
	<!-- END words -->
	<tr>
		<td class="row1">{L_HEADER_CODE}</td>
		<td class="row2"><textarea name="file_header" rows="10" cols="60" wrap="virtual" style="width:450px;height:200px" tabindex="3" class="post">{FILE_HEADER}</textarea></td>
	</tr>
	<tr>
		<td class="row1">{L_BODY_CODE}</td>
		<td class="row2"><span class="gen"> <textarea name="file_body" rows="10" cols="60" wrap="virtual" style="width:450px;height:100px" tabindex="3" class="post">{FILE_BODY}</textarea></span></td>
	</tr>
	<tr>
		<td class="row1">{L_FOOTER_CODE}</td>
		<td class="row2"><textarea name="file_footer" rows="10" cols="60" wrap="virtual" style="width:450px;height:100px" tabindex="3" class="post">{FILE_FOOTER}</textarea></td>
	</tr>
	<tr>
		<td class="catBottom" colspan="2" align="center">{S_HIDDEN_FIELDS}<input class="mainoption" type="submit" value="{L_SUBMIT}" /></td>
	</tr>
</table></form>
