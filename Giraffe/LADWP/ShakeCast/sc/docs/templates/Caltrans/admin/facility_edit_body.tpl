
<script language="JavaScript" type="text/javascript">

var Fwin = null;
Fwin_Wid = 560;
Fwin_Hgt = 400;
Fwin_Left = (screen) ? screen.width/2 - Fwin_Wid/2 : 560;
Fwin_Top =  (screen) ? screen.availHeight/2 - Fwin_Hgt/2 : 400;
function openFormWin(url) {
Fwin = open(url,'Fwin','width='+Fwin_Wid+',height='+Fwin_Hgt+',left='+
Fwin_Left+',top='+Fwin_Top+',status=0');
setTimeout('Fwin.focus()',100);
}

</script>

<h1>{L_FACILITY_TITLE}</h1>

<p>{L_FACILITY_TEXT}</p>

<form method="post" action="{U_SEARCH}">
	<table width="90%" border="0" align="center">
	  <tr> 
		<td align="right" valign="top" nowrap="nowrap">
		<input type="text" name="facility" id="facility" size="40" autocomplete="off" />
		<input type="hidden" name="id" id="id" value="" />
			<input type="submit" value="Lookup Facility" />
			</td>
	  </tr>
	</table>
</form>

<form method="post" action="{S_FACILITY_ACTION}" name="facility_edit">
<table border="0" align="center" class="forumline">
<tr><td>
	<tr><td>
	<table cellspacing="1" cellpadding="4" border="0" align="center" class="forumline" width="100%">
	<tr>
		<th colspan="2" class="thHead">{L_FACILITY_EDIT}{F_HIDDEN_FIELDS}</th>
	</tr>
	<tr>
		<td class="row1">{L_EXTERNAL_FACILITY_ID}<br /><span class="gensmall">{L_EXTERNAL_FACILITY_ID_EXPLAIN}</span></td>
		<td class="row2"><input class="post" type="text" name="external_facility_id" value="{EXTERNAL_FACILITY_ID}" size="32" maxlength="32"/>*</td>
	</tr>
	<tr>
		<td class="row1">{L_FACILITY_NAME}<br /><span class="gensmall">{L_FACILITY_NAME_EXPLAIN}</span></td>
		<td class="row2"><input class="post" type="text" name="facility_name" value="{FACILITY_NAME}" size="32" maxlength="128" />*</td>
	</tr>
	<tr>
		<td class="row1">{L_SHORT_NAME}<br /><span class="gensmall">{L_SHORT_NAME_EXPLAIN}</span></td>
		<td class="row2"><input class="post" type="text" name="short_name" value="{SHORT_NAME}"  size="10" maxlength="10"/></td>
	</tr>
	<tr>
		<td class="row1">{L_FACILITY_TYPE}</td>
		<td class="row2">{FACILITY_TYPE}*</td>
	</tr>
	<tr>
		<td class="row1">{L_FACILITY_DESCRIPTION}<br /><span class="gensmall">{L_FACILITY_DESCRIPTION_EXPLAIN}</span></td>
		<td class="row2"><textarea class="post" type="textarea" name="description" value="" cols="50" rows="3" >{DESCRIPTION}</textarea></td>
	</tr>
	<tr>
		<td class="row1">{L_LATITUDE} <span class="gensmall">{L_LATITUDE_EXPLAIN}</span> <a href="#" onClick="openFormWin('{U_GM_FORM}');return false;">Show Map Picker</a>
</td>
		<td class="row2"><input class="post" type="text" name="lat_min" value="{LAT_MIN}" />* <-> <input class="post" type="text" name="lat_max" value="{LAT_MAX}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_LONGITUDE} <span class="gensmall">{L_LONGITUDE_EXPLAIN}</span></td>
		<td class="row2"><input class="post" type="text" name="lon_min" value="{LON_MIN}" />* <-> <input class="post" type="text" name="lon_max" value="{LON_MAX}" /></td>
	</tr>
</table>
</td></tr>
<tr><td>
<table cellspacing="1" cellpadding="4" border="0" align="center" class="forumline">
	<tr class="thHead">
		<th>{L_DAMAGE_LEVEL}</th>
		<th>{L_LOW_LIMIT}</th>
		<th>{L_HIGH_LIMIT}</th>
		<th>{L_METRIC}</th>
	</tr>
<!-- BEGIN fragrow -->
	<tr>
		<td class="{fragrow.ROW_CLASS}">{fragrow.DAMAGE_LEVEL}{fragrow.FR_HIDDEN_FIELDS}</td>
		<td class="{fragrow.ROW_CLASS}"><input class="post" type="text" name="FR{fragrow.FRAG_ID}[low_limit]"  id="{fragrow.DAMAGE}_low_limit" value="{fragrow.LOW_LIMIT}" />*</td>
		<td class="{fragrow.ROW_CLASS}"><input class="post" type="text" name="FR{fragrow.FRAG_ID}[high_limit]" id="{fragrow.DAMAGE}_high_limit" value="{fragrow.HIGH_LIMIT}" />*</td>
		<td class="{fragrow.ROW_CLASS}">{fragrow.METRIC}*</td>
	</tr>
<!-- END fragrow -->
</table>
</td></tr>
	<tr>
		<td class="catBottom" colspan="2" align="center">{S_HIDDEN_FIELDS}<input type="submit" name="save" value="{L_SUBMIT}" class="mainoption" /><input type="submit" name="delete" value="{L_DELETE}" class="mainoption" /></td>
	</tr>
</table>

<br />

<table cellspacing="1" cellpadding="4" border="0" align="center" class="forumline">
<tr>
	<th colspan="2" class="thHead">{L_FACILITY_ATTRIBUTE}</th>
</tr>
<tr>
	<td class="row2" colspan="2"></td>
</tr>
<!-- BEGIN attrow -->
	<tr>
		<td class="row1">{attrow.L_ATTRIB_NAME}</td>
		<td class="row2"><input class="post" type="text" name="FA[{attrow.L_ATTRIB_NAME}]" value="{attrow.ATTRIBUTE_VALUE}" size="50"/></td>
	</tr>
<!-- END attrow -->
</table>
</form>

<script language="JavaScript" type="text/javascript">
//You should create the validator only after the definition of the HTML form
  var frmvalidator  = new Validator("facility_edit");
  frmvalidator.EnableMsgsTogether();
  frmvalidator.addValidation("external_facility_id","req","Please enter External Facility ID");
  frmvalidator.addValidation("external_facility_id","maxlen=20",	"Max length for External Facility ID is 32");
  
  frmvalidator.addValidation("facility_name","req","Please enter Facility Name");
  frmvalidator.addValidation("facility_name","maxlen=128","Max length is 128");
  
  frmvalidator.addValidation("lat_min","req","Please enter Facility Latitude");
  frmvalidator.addValidation("lon_min","req","Please enter Facility Longitude");
  frmvalidator.addValidation("lat_min","gt=-90");
  frmvalidator.addValidation("lon_min","gt=-180");
  frmvalidator.addValidation("lat_max","lt=90");
  frmvalidator.addValidation("lon_max","lt=180");
  
  frmvalidator.addValidation("facility_type","dontselect=0","Please select one Facility Type");

</script>
