<script type="text/javascript"><!--

function setMode(type){
	document.getElementById('mode').value = type;
}

//--></script>
<h1>{L_WORDS_TITLE}</h1>

<P>{L_WORDS_TEXT}</p>

<form method="post" id="form_id" name="myform" action="{S_WORDS_ACTION}"><table cellspacing="1" cellpadding="4" border="0" align="center" class="forumline">
	<tr>
		<th class="thCornerL">{L_STATUS}</th>
		<th>{L_TASKNAME}</th>
		<th>{L_NEXT_RUN_TIME}</th>
		<th>{L_REPEAT}</th>
		<th>{L_DESCRIPTION}</th>
		<th class="thCornerR">{L_ACTION}</th>
	</tr>
	<!-- BEGIN words -->
	<tr>
		<td class="{words.ROW_CLASS}" align="center" valign="top">{words.STATUS}</td>
		<td class="{words.ROW_CLASS}" align="center" valign="top">{words.TASKNAME}</td>
		<td class="{words.ROW_CLASS}" align="center" valign="top">{words.NEXT_RUN_TIME}</td>
		<td class="{words.ROW_CLASS}" align="center" valign="top">{words.REPEAT}</td>
		<td class="{words.ROW_CLASS}" align="left">{words.DESCRIPTION}</td>
		<td class="{words.ROW_CLASS}" align="center"><a href="{words.U_RUN}">{words.L_RUN}</a> <a href="{words.U_DELETE}">{L_DELETE}</a></td>
	</tr>
	<!-- END words -->
	<tr>
		<td colspan="6" align="center" class="catBottom">{S_HIDDEN_FIELDS}
		<input type="submit" value="{L_ADD_TASK}" onClick=setMode('add') class="mainoption" /></td>
	</tr>
</table></form>

