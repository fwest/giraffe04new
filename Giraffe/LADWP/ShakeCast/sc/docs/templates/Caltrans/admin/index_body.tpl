
<h1>{L_WELCOME}</h1>

<p>{L_ADMIN_INTRO}</p>

<h1>{L_SERVER_STATS}</h1>
<!--
<h1>{L_FORUM_STATS}</h1>
-->

<table width="100%" cellpadding="4" cellspacing="1" border="0" class="forumline">
<!--
  <tr> 
	<th width="25%" nowrap="nowrap" height="25" class="thCornerL">{L_STATISTIC}</th>
	<th width="25%" height="25" class="thTop">{L_VALUE}</th>
	<th width="25%" nowrap="nowrap" height="25" class="thTop">{L_STATISTIC}</th>
	<th width="25%" height="25" class="thCornerR">{L_VALUE}</th>
  </tr>
-->
  <tr> 
	<td width="25%" class="row1" nowrap="nowrap" align="right">{L_DNS_ADDRESS}:</td>
	<td class="row2" colspan=3><b>{DNS_ADDRESS}</b></td>
  </tr>
  <tr> 
	<td class="row1" nowrap="nowrap" align="right">{L_LAST_HEARD_FROM}:</td>
	<td class="row2"><b>{LAST_HEARD_FROM}</b></td>
  </tr>
  <tr> 
	<td class="row1" nowrap="nowrap" align="right">{L_ERROR_COUNT}:</td>
	<td class="row2"><b>{ERROR_COUNT}</b></td>
  </tr>
  <tr> 
	<td class="row1" nowrap="nowrap" align="right">{L_SERVER_STATUS}:</td>
	<td bgcolor={SERVER_STATUS_COLOR}><b>{SERVER_STATUS}</b></td>
  </tr>
</table>

<br />

<table width="100%" cellpadding="4" cellspacing="1" border="0" class="forumline">
  <tr> 
	<td class="row1" nowrap="nowrap" valign="top" align="right">{L_BOARD_STARTED}:
		<br>[<a href="{L_LOGSTATS}">Update Chart</a>]<br>[<a href="{L_LOGROTATE}">Rotate Logs</a>]</td>
	<td class="row2"><img src="/images/sc_daily_bar.png"></td>
  </tr>
  <tr> 
	<td width="25%" class="row1" nowrap="nowrap" align="right">{L_SC_SERVICES}:
		<br>[<a href="{L_SC_SERVICES_START}">Start</a> | <a href="{L_SC_SERVICES_STOP}">Stop</a>]</td>
	<td bgcolor={SC_SERVICES_STATUS_COLOR}><b>{SC_SERVICES} ACTIVE</b></td>
  </tr>
  <tr> 
	<td width="25%" class="row1" nowrap="nowrap" align="right">{L_DB_SIZE}:</td>
	<td class="row2"><b>{DB_SIZE}</b></td>
  </tr>
</table>

<br />

<table width="100%" cellpadding="4" cellspacing="1" border="0" class="forumline">
  <tr> 
	<td width="25%" class="row1" nowrap="nowrap" align="right">{L_NOTIFICATION}:</td>
	<td class="row2"><b>{EVID} (Version {EVID_VERSION})</b></td>
  </tr>
  <tr> 
	<td width="25%" class="row1" nowrap="nowrap" align="right">{L_DELIVERY_TIMESTAMP}:</td>
	<td class="row2"}><b>{DELIVERY_TIMESTAMP}</b></td>
  </tr>
  <tr> 
	<td width="25%" class="row1" nowrap="nowrap" align="right">{L_NOTIFIED_USER}:</td>
	<td bgcolor={DELIVERY_STATUS_COLOR}><b>{DELIVERY_COUNT}/{NOTIFICATION_COUNT}</b></td>
  </tr>
</table>

<br />

<h1>{L_VERSION_INFORMATION}</h1>

{VERSION_INFO}

<br />
