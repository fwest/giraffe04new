<script type="text/javascript"><!--

var formblock;
var forminputs;

function prepare() {
formblock= document.getElementById('form_id');
forminputs = formblock.getElementsByTagName('input');
}

function select_all(name, value) {
for (i = 0; i < forminputs.length; i++) {
// regex here to check name attribute
var regex = new RegExp(name, "i");
if (regex.test(forminputs[i].getAttribute('name'))) {
if (value == true) {
forminputs[i].checked = true;
} else {
forminputs[i].checked = false;
}
}
}
}

if (window.addEventListener) {
window.addEventListener("load", prepare, false);
} else if (window.attachEvent) {
window.attachEvent("onload", prepare)
} else if (document.getElementById) {
window.onload = prepare;
}

function setMode(type){
	document.getElementById('mode').value = type;
}

//--></script>

<h1>{L_WORDS_TITLE}</h1>

<P>{L_WORDS_TEXT}</p>

<form method="post" action="{U_SEARCH}">
	<table width="90%" border="0" align="center">
	  <tr> 
		<td align="right" valign="top" nowrap="nowrap">
		<input type="text" name="facility" id="facility" size="40" autocomplete="off" />
		<input type="hidden" name="id" id="id" value="" />
			<input type="submit" value="Lookup Facility" />
			</td>
	  </tr>
	</table>
</form>

<form id="form_id" name="myform" method="post" action="{S_WORDS_ACTION}"><table cellspacing="1" cellpadding="4" border="0" align="center" class="forumline">
	<tr class="row2">
		<th class="thCornerL"><a href="{U_FAC_ID}" class="event">{L_FAC_ID}</a></th>
		<th class="thTop"><a href="{U_FAC_TYPE}" class="event">{L_FAC_TYPE}</a></th>
		<th class="thTop"><a href="{U_FAC_NAME}" class="event">{L_FAC_NAME}</a></th>
		<th class="thTop"><a href="{U_DESCRIPTION}" class="event">{L_DESCRIPTION}</a></th>
		<th class="thTop"><a href="{U_LATITUDE}" class="event">{L_LATITUDE}</a></th>
		<th class="thTop"><a href="{U_LONGITUDE}" class="event">{L_LONGITUDE}</a></th>
		<th class="thTop">{L_ACTION}</th>
		<th class="thCornerR">{L_SELECT}<br>
		<input type=checkbox name="checkall" value="1" {CHECKED} onClick="select_all('facility', this.checked);"></th>
	</tr>
	<!-- BEGIN words -->
	<tr>
		<td class="{words.ROW_CLASS}" align="center">{words.FAC_ID}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.FAC_TYPE}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.FAC_NAME}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.DESCRIPTION}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.LAT_MIN} <-> {words.LAT_MAX}</td>
		<td class="{words.ROW_CLASS}" align="center">{words.LON_MIN} <-> {words.LON_MAX}</td>
		<td class="{words.ROW_CLASS}" align="center"><a href="{words.U_EDIT}">{L_EDIT}</a></td>
		<td class="{words.ROW_CLASS}" align="center"><input type=checkbox name="facility[]" value="{words.FAC_ID}"></td>
	</tr>
	<!-- END words -->
	<tr>
		<td colspan="6" align="center" class="catBottom">{S_HIDDEN_FIELDS}
			<input type="button" value="{L_EXPORT}" onClick="window.location.href='{U_EXPORT}';" class="mainoption" />
			<input type="submit" value="{L_ADD_EVENT}" onClick=setMode('add') class="mainoption" /></td>
		<td colspan="2" align="center" class="catBottom"><input type=checkbox name="clear_cache" value="1">Clear Cache<br><input type="submit" value="{L_DELETE}" onClick=setMode('delete') class="mainoption" >
			<br><input type="submit" value="{L_DELETE_ALL}" onClick=setMode('delete_all') class="mainoption" ></td>
	</tr>
</table></form>

<table width="100%" cellspacing="2" border="0" align="center" cellpadding="2">
  <tr> 
	<td align="left" valign="top"><span class="nav">{PAGE_NUMBER}</span></td>
	<td align="right" valign="top" nowrap="nowrap"><span class="nav">{PAGINATION}</span><br /><span class="gensmall">{S_TIMEZONE}</span></td>
  </tr>
</table>
