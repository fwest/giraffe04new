
<h1>{L_WORDS_TITLE}</h1>

<P>{L_WORDS_TEXT}</p>

<form method="post" action="{S_DAMAGE_ACTION}">
<table cellspacing="1" cellpadding="4" border="0" align="center" class="forumline">
	<tr>
		<th colspan="2" class="thHead">{L_DAMAGE_EDIT}{F_HIDDEN_FIELDS}</th>
	</tr>
	<tr>
		<td class="row1">{L_DAMAGE_LEVEL}</td>
		<td class="row2">{DAMAGE_LEVEL}</td>
	</tr>
	<tr>
		<td class="row1">{L_NAME}</td>
		<td class="row2"><input class="post" type="text" name="name" value="{NAME}"  size="50"/></td>
	</tr>
	<tr>
		<td class="row1">{L_DESCRIPTION}</td>
		<td class="row2"><textarea class="post" type="textarea" name="description" value="" cols="50" rows="3" >{DESCRIPTION}</textarea></td>
	</tr>
	<tr>
		<td class="row1">{L_SEVERITY_RANK}</td>
		<td class="row2"><input class="post" type="text" name="severity_rank" value="{SEVERITY_RANK}" /></td>
	</tr>
	<tr>
		<td class="row1">{L_IS_MAX_SEVERITY}</td>
		<td class="row2"><input class="post" type="checkbox" name="is_max_severity" value="yes" {IS_MAX_SEVERITY}/> Yes</td>
	</tr>
	<tr>
		<td class="catBottom" colspan="2" align="center">{S_HIDDEN_FIELDS}<input type="submit" name="save" value="{L_SUBMIT}" class="mainoption" /></td>
	</tr>
</table>
</form>
