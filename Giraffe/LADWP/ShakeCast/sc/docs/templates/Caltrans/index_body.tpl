		<!-- BEGIN test_event -->
		<center><span class="emphlarge">Scenario Earthquake</span></center><br /> 
		<!-- END test_event -->

<!-- BEGIN comment_event -->
<div id="nav">
	<div class="emph, nav" ><span style="color:red">{comment_event.L_COMMENT}</span><span style="color:white">{comment_event.L_COMMENT_TEXT}</span></div><br /> 
</div>
<!-- END comment_event -->

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
	<td align="right">{JUMPBOX}</td>
  </tr>
</table>

		<!-- START: mapping div -->
		<div id="mapping" class="clearfix">
			
			<div id="map"></div>
	
			<div id="map-right">
				<div id="info">
					<h2>ShakeCast Summary</h2>
					{EQ}
					<span class="genmed">{FORUM_COUNT}</span> 
					<!-- BEGIN metrics -->
					<span class="genmed">{metrics.METRIC_NAME} {metrics.METRIC_UNIT}: <span class="emphmed">{metrics.MIN_VALUE} - {metrics.MAX_VALUE}</span></span>
					<!-- END metrics -->
					<br />
					<span class="eqinfolink"> <a href="{U_VIEWFORUM}" class="eqinfolink">{FORUM_NAME}</a></span> 
					<br />
					{FORUM_DESC}
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</div><!-- mapping div -->
	</div><!-- /content div -->
	
	<a name="login"></a>
	<div id="loginForm">
		<!-- BEGIN switch_user_logged_out -->
		<form method="post" action="{S_LOGIN_ACTION}">
		<table cellpadding="3" cellspacing="1" border="0" width="775">
		<tr>
		<td class="cattitle">{L_LOGIN_LOGOUT}</td>
		<td class="gensmall">{L_USERNAME}: <input class="post" type="text" name="username" /></td>
		<td class="gensmall">{L_PASSWORD}: <input class="post" type="password" name="password" maxlength="32" /></td>
		<td class="gensmall"><span class="genVsmall">{L_AUTO_LOGIN}</span> <input class="text" type="checkbox" name="autologin" /></td>
		<td class="gensmall"><input type="image" src="templates/Caltrans/images/login.gif" width="57" height="22" name="login" value="{L_LOGIN}" border="0" class="login" /></td>
		</tr>
		</table>
		</form>
		<!-- END switch_user_logged_out -->
	</div>

