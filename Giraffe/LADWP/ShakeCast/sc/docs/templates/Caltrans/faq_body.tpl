		<div id="nav">
		<span class="gensmall"><a href="{L_FAQ}" class="nav">FAQ</a></span>
		</div>


		<div id="eq_list_wrapper">
			<div id="eqtable" >
				<table cellspacing="1" cellpadding="3" border="0" id="eq_list">
				<tr>
				<td class="row1">
				<!-- BEGIN faq_block_link -->
				<span class="gen"><b>{faq_block_link.BLOCK_TITLE}</b></span><br />
				<!-- BEGIN faq_row_link -->
				<span class="gen"><a href="{faq_block_link.faq_row_link.U_FAQ_LINK}" class="postlink">{faq_block_link.faq_row_link.FAQ_LINK}</a></span><br />
				<!-- END faq_row_link -->
				<br />
				<!-- END faq_block_link -->
				</td>
				</tr>
				</table>

				<br clear="all" />

				<!-- BEGIN faq_block -->
				<table id="forumline" width="100%" cellspacing="1" cellpadding="3" border="0" align="center">
				<tr> 
				<td class="catHead">{faq_block.BLOCK_TITLE}</td>
				</tr>
				<!-- BEGIN faq_row -->  
				<tr> 
				<td class="{faq_block.faq_row.ROW_CLASS} postbody">
					<a name="{faq_block.faq_row.U_FAQ_ID}"></a>
					<strong>{faq_block.faq_row.FAQ_QUESTION}</strong>
					<br />{faq_block.faq_row.FAQ_ANSWER}
					<br /><a href="#top">{L_BACK_TO_TOP}</a>
				</td>
				</tr>
				<!-- END faq_row -->
				</table>

				<br clear="all" />
				<!-- END faq_block -->

			</div>
		</div>