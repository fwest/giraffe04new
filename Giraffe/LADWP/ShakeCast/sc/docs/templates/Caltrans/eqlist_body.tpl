
		<div id="eq_list_wrapper">
			<script type='text/javascript' src='common.js'></script>
			<script type='text/javascript' src='css.js'></script>
<!--		//<script type='text/javascript' src='standardista-table-sorting.js'></script>  -->

			<table align="center" class='sortable' id="eq_list">
			<thead>
			<tr nowrap="nowrap"> 
			  <th><a href="{U_FACILITY_COUNT}">{L_FACILITY_COUNT}</a></th>
			  <th><a href="{U_MAGNITUDE}">{L_MAGNITUDE}</a></th>
			  <th><a href="{U_EARTHQUAKE}">{L_EARTHQUAKE}</a></th>
			  <th><a href="{U_LOCATION}">{L_LOCATION}</a></th>
			  <th><a href="{U_EVENT_ID}">{L_EVENT_ID}</a></th>
			  <th><a href="{U_TIME}">{L_TIME}</a></th>
			</tr>
			</thead>
			<tbody>
			<!-- BEGIN topicrow -->
			<tr class="row{topicrow.ROW_COLOR}"> 
			<td align="center"><strong>{topicrow.FACILITY_COUNT}</strong></td>
			<td align="center"><strong>{topicrow.MAG}</strong></td>
			<td><a href="{topicrow.U_VIEW_EVENT}" class="topictitle">{topicrow.DESCRIPTION}</a></td>
			<td>{topicrow.LAT}, {topicrow.LON}</td>
			<td>{topicrow.REGION} {topicrow.SHAKEMAP_ID}</td>
			<td>{topicrow.LAST_UPDATE}</td>
			</tr>
			<!-- END topicrow -->
			</tbody>
			</table>

			<table width="100%" cellspacing="2" border="0" align="center" cellpadding="2">
			<tr> 
			<td align="right" valign="middle" nowrap="nowrap"><span class="gensmall">{S_TIMEZONE}</span><br /><span class="nav">{PAGINATION}</span> 
			</td>
			</tr>
			<tr>
			<td align="left"><span class="nav">{PAGE_NUMBER}</span></td>
			</tr>
			</table>
		</div>
		<div class="clearfix"></div>