
{ERROR_BOX}

<div id="nav">
	<span class="gensmall"><a href="{U_PROFILE}" class="nav">Profile</a></span>
</div>


<div id="eq_list_wrapper">
	<div id="eqtable" >

<form action="{S_PROFILE_ACTION}" {S_FORM_ENCTYPE} method="post">

<table border="0" cellpadding="3" cellspacing="1" width="100%" class="forumline"   id="eq_list">
	<tr class="row2"> 
		<th class="thHead" colspan="2" height="25" valign="middle">{L_USER_INFO}</th>
	</tr>
	<tr> 
		<td class="row2" colspan="2"><span class="gensmall">{L_ITEMS_REQUIRED}</span></td>
	</tr>
	<tr> 
		<td class="row1" width="38%"><span class="gen">{L_USERNAME}: </span></td>
		<td class="row2"><input type="hidden" name="username" value="{USERNAME}" /><span class="gen"><b>{USERNAME}</b></span></td>
	</tr>
	<tr> 
		<td class="row1"><span class="gen">{L_EMAIL_ADDRESS}: *</span></td>
		<td class="row2"><input type="hidden" name="email" value="{EMAIL}" /><span class="gen">{EMAIL}</span></td>
	</tr>
	<tr> 
		<td class="row2" colspan="2"><span class="gensmall"></span></td>
	</tr>
	<tr class="row2"> 
	  <th class="thSides" colspan="2" height="25" valign="middle">{L_DELIVERY_METHOD_INFO}</th>
	</tr>
	<tr> 
	  <td class="row1"><span class="gen">{L_EMAIL_HTML}:</span></td>
	  <td class="row2"> 
		<input type="text" class="post" style="width: 200px"  name="EMAIL_HTML" size="25" maxlength="255" value="{EMAIL_HTML}" /> {LOCK_EMAIL_HTML}
	  </td>
	</tr>
	<tr> 
	  <td class="row1"><span class="gen">{L_EMAIL_TEXT}:</span></td>
	  <td class="row2"> 
		<input type="text" class="post" style="width: 200px"  name="EMAIL_TEXT" size="25" maxlength="100" value="{EMAIL_TEXT}" /> {LOCK_EMAIL_TEXT}
	  </td>
	</tr>
	<tr> 
	  <td class="row1"><span class="gen">{L_PAGER}:</span></td>
	  <td class="row2"> 
		<input type="text" class="post" style="width: 200px"  name="PAGER" size="25" maxlength="100" value="{PAGER}" /> {LOCK_PAGER}
	  </td>
	</tr>
	<tr>
		<td class="catBottom" colspan="2" align="center" height="28">{S_HIDDEN_FIELDS}<input type="image" src="templates/Caltrans/images/submit.gif" width="63" height="22" border="0" name="submit" value="{L_SUBMIT}" />&nbsp;&nbsp;<input type="image" src="templates/Caltrans/images/reset.gif" width="59" height="22" border="0" value="{L_RESET}" name="reset" /></td>
	</tr>
	<tr> 
		<td class="row2" colspan="2"><img src="/images/address_lock.gif" alt="Locked address"> <span class="gensmall">Deactivaed delivery address. Validation is required to start receiving notifications.</span></td>
	</tr>
</table>

</form>
	</div>
</div>
