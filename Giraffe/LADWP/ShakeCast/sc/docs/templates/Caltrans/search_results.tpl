 

<div id="nav">
	<span class="gensmall"><a href="{U_INDEX}" class="nav">{L_INDEX}</a></span>
</div>


<div id="eq_list_wrapper">

		<script type='text/javascript' src='common.js'></script>
		<script type='text/javascript' src='css.js'></script>

	<div id="eqtable" >
	<table>
		<tr>
			<th id="eqtableTH">{L_SEARCH_MATCHES}</th>
		</tr>
		<tr> 
			<td align="left" valign="top">
				<table align="center" id="eq_list">
					<thead>
						<tr class="row2">
							<th><a href="{U_ID}">{L_ID}</a></th>
							<th><a href="{U_EXT_FACILITY_ID}">{L_EXT_FACILITY_ID}</a></th>
							<th><a href="{U_TYPE}">{L_TYPE}</a></th>
							<th><a href="{U_NAME}">{L_NAME}</a></th>
							<th><a href="{U_DAMAGE_ESTIMATE}">{L_DAMAGE_ESTIMATE}</a></th>
							<th><a href="{U_LATITUDE}">{L_LATITUDE}</a></th>
							<th><a href="{U_LONGITUDE}">{L_LONGITUDE}</a></th>
							{L_METRIC}
						</tr>
					</thead>
					<tbody>
					<!-- BEGIN searchresults -->
						<tr class="row1" id="{searchresults.FACILITY_ID}">
							<td>{searchresults.ID}</td>
							<td>{searchresults.EXT_FACILITY_ID}</td>
							<td>{searchresults.TYPE}</td>
							<td><a href="{searchresults.U_NAME}" class="topictitle">{searchresults.NAME}</a></td>
							<td class="{searchresults.FACILITY_DAMAGE}">{searchresults.DAMAGE_LEVEL}</td>
							<td class="lat">{searchresults.LATITUDE}</td>
							<td class="lon">{searchresults.LONGITUDE}</td>
							{searchresults.METRIC}
						</tr>
					<!-- END searchresults -->
					</tbody>
				</table>
			</td>
		</tr>
	</table>
	
	<table width="100%" cellspacing="2" border="0" align="center" cellpadding="2">
		<tr> 
		  <td align="right" valign="middle" nowrap="nowrap"><span class="gensmall">{S_TIMEZONE}</span><br /><span class="nav">{PAGINATION}</span> 
			</td>
		</tr>
		<tr>
		  <td align="left"><span class="nav">{PAGE_NUMBER}</span></td>
		</tr>
	</table>
	  
	</div>
</div>
