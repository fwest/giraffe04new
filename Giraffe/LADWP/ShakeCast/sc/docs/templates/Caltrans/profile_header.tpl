<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html dir="{S_CONTENT_DIRECTION}">
<head>
<meta http-equiv="Content-Type" content="text/html; charset={S_CONTENT_ENCODING}">
<meta http-equiv="Content-Style-Type" content="text/css">
{META}
{NAV_LINKS}
<title>{SITENAME} :: {PAGE_TITLE}</title>
<!-- link rel="stylesheet" href="templates/Caltrans/{T_HEAD_STYLESHEET}" type="text/css" -->
<link rel="stylesheet" href="templates/Caltrans/sc.css" type="text/css">

<script type="text/javascript" src="./js/niftycube.js"></script>
<script type="text/javascript">
	NiftyLoad=function(){
	Nifty("div#content");
	Nifty("div#eq_list_wrapper");
	}
</script>

</head>
<body bgcolor="{T_BODY_BGCOLOR}" text="{T_BODY_TEXT}" link="{T_BODY_LINK}" vlink="{T_BODY_VLINK}">
<div id="container">
	<div id="header">
		<ul id="globalnav">
		<li><a href="{U_INDEX}" class="{C_HOME}">Home</a></li>
		<li><a href="{U_EARTHQUAKE}" class="{C_EARTHQUAKE}">{L_EARTHQUAKE}</a></li>
		<li><a href="{U_SEARCH}" class="{C_SEARCH}">{L_SEARCH}</a></li>
		<li><a href="{U_FAQ}" class="{C_FAQ}">{L_FAQ}</a></li>
		<li class="here"><a href="{U_PROFILE}">{L_PROFILE}</a>
			<ul>
			<li><a href="{U_USER_GENERAL}">{L_USER_GENERAL}</a></li>
			<li><a href="{U_EMAIL_LIST}">{L_EMAIL_LIST}</a></li>
			<li><a href="{U_NOTIFICATION}">{L_NOTIFICATION}</a></li>
			</ul>
		</li>
		<!-- BEGIN switch_user_logged_out -->
		<li><a href="{U_REGISTER}" class="{C_REGISTER}">{L_REGISTER}</a></li>
		<!-- END switch_user_logged_out -->
		<!-- BEGIN switch_user_logged_in -->
		<li><a href="{U_ADMIN}" class="{C_ADMIN}">{L_ADMIN}</a></li>
		<!-- END switch_user_logged_in -->
		<li><a href="{U_LOGIN_LOGOUT}" class="{C_LOGIN_LOGOUT}">{L_LOGIN_LOGOUT}</a></li>
		</ul>
	</div>
	
	<div id="content">
