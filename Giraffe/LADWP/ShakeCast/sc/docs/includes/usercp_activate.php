<?php
/*
# $Id: usercp_activate.php 132 2007-08-17 21:56:49Z klin $

# The PHP interface was originally inspired by PHPBB,
# file/database formats and directory structure are quite similar.

##############################################################################
# 
# Terms and Conditions of Software Use
# ====================================
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Disclaimer of Earthquake Information
# ====================================
# 
# The data and maps provided through this system are preliminary data
# and are subject to revision. They are computer generated and may not
# have received human review or official approval. Inaccuracies in the
# data may be present because of instrument or computer
# malfunctions. Subsequent review may result in significant revisions to
# the data. All efforts have been made to provide accurate information,
# but reliance on, or interpretation of earthquake data from a single
# source is not advised. Data users are cautioned to consider carefully
# the provisional nature of the information before using it for
# decisions that concern personal or public safety or the conduct of
# business that involves substantial monetary or operational
# consequences.
# 
# Disclaimer of Software and its Capabilities
# ===========================================
# 
# This software is provided as an "as is" basis.  Attempts have been
# made to rid the program of software defects and bugs, however the
# U.S. Geological Survey (USGS) have no obligations to provide maintenance, 
# support, updates, enhancements or modifications. In no event shall USGS 
# be liable to any party for direct, indirect, special, incidental or 
# consequential damages, including lost profits, arising out of the use 
# of this software, its documentation, or data obtained though the use 
# of this software, even if USGS or have been advised of the
# possibility of such damage. By downloading, installing or using this
# program, the user acknowledges and understands the purpose and
# limitations of this software.
# 
# Contact Information
# ===================
# 
# Coordination of this effort is under the auspices of the USGS Advanced
# National Seismic System (ANSS) coordinated in Golden, Colorado, which
# functions as the clearing house for development, distribution,
# documentation, and support. For questions, comments, or reports of
# potential bugs regarding this software please contact wald@usgs.gov or
# klin@usgs.gov.  
#
#############################################################################
*/

if ( !defined('IN_SC') )
{
	die('Hacking attempt');
	exit;
}

if ( isset($HTTP_GET_VARS['delivery_method']) )
{
	$sql = "SELECT shakecast_user, delivery_method, delivery_address, actkey 
		FROM " . USER_DELIVERY_METHOD_TABLE . "
		WHERE shakecast_user = " . intval($HTTP_GET_VARS[POST_USERS_URL]) . " AND delivery_method = '" . $HTTP_GET_VARS['delivery_method'] . "'";
	if ( !($result = $db->sql_query($sql)) )
	{
		message_die(GENERAL_ERROR, 'Could not obtain user delivery method information', '', __LINE__, __FILE__, $sql);
	}
	if ( !($row = $db->sql_fetchrow($result)) )
	{
		message_die(GENERAL_MESSAGE, $lang['No_such_user']);
	}

	if ((trim($row['actkey']) == trim($HTTP_GET_VARS['act_key'])) && (trim($row['actkey']) != ''))
	{
		if (!$userdata['session_logged_in'])
		{
			redirect(append_sid('login.' . $phpEx . '?redirect=profile.' . $phpEx . '&mode=activate&' . POST_USERS_URL . '=' . $row['shakecast_user'] . '&delivery_method=' . trim($HTTP_GET_VARS['delivery_method']) . '&act_key=' . trim($HTTP_GET_VARS['act_key'])));
		}

		$sql = "UPDATE " . USER_DELIVERY_METHOD_TABLE . "
			SET actkey = ''  
			WHERE shakecast_user = " . $row['shakecast_user'] . " AND delivery_method = '" . $HTTP_GET_VARS['delivery_method'] . "'"; 
		if ( !($result = $db->sql_query($sql)) )
		{
			message_die(GENERAL_ERROR, 'Could not update users table', '', __LINE__, __FILE__, $sql_update);
		} 
		else {
			$sql = "INSERT INTO " . VALIDATED_TABLE . "
				(validated_userid, validated_email) VALUES (" . $row['shakecast_user'] . ", '" . $row['delivery_address'] . "')";
			if ( !($result = $db->sql_query($sql)) )
			{
				message_die(GENERAL_ERROR, 'Could not update validated table', '', __LINE__, __FILE__, $sql_update);
			} 
		}

		$template->assign_vars(array(
			'META' => '<meta http-equiv="refresh" content="10;url=' . append_sid("index.$phpEx") . '">')
		);

		message_die(GENERAL_MESSAGE, $lang['Delivery_method_updated']);
	}
	message_die(GENERAL_ERROR, 'Could not validate user delivery method information');
}
else 
{
	$sql = "SELECT user_active, user_id, username, user_email, user_newpasswd, user_lang, user_actkey 
		FROM " . USERS_TABLE . "
		WHERE user_id = " . intval($HTTP_GET_VARS[POST_USERS_URL]);
	if ( !($result = $db->sql_query($sql)) )
	{
		message_die(GENERAL_ERROR, 'Could not obtain user information', '', __LINE__, __FILE__, $sql);
	}
	
	if ( !($row = $db->sql_fetchrow($result)) )
	{
		message_die(GENERAL_MESSAGE, $lang['No_such_user']);
	}

	$user_id = $row['user_id'];
	$user_email = $row['user_email'];
	if ( $row['user_active'] && trim($row['user_actkey']) == '' )
	{
		$template->assign_vars(array(
			'META' => '<meta http-equiv="refresh" content="10;url=' . append_sid("index.$phpEx") . '">')
		);

		message_die(GENERAL_MESSAGE, $lang['Already_activated']);
	}
	else if ((trim($row['user_actkey']) == trim($HTTP_GET_VARS['act_key'])) && (trim($row['user_actkey']) != ''))
	{
		if (intval($board_config['require_activation']) == USER_ACTIVATION_ADMIN && $row['user_newpasswd'] == '')
		{
			if (!$userdata['session_logged_in'])
			{
				redirect(append_sid('login.' . $phpEx . '?redirect=profile.' . $phpEx . '&mode=activate&' . POST_USERS_URL . '=' . $row['user_id'] . '&act_key=' . trim($HTTP_GET_VARS['act_key'])));
			}
			else if ($userdata['user_level'] != ADMIN)
			{
				message_die(GENERAL_MESSAGE, $lang['Not_Authorised']);
			}
		}

		$sql_update_pass = ( $row['user_newpasswd'] != '' ) ? ", user_password = '" . str_replace("\'", "''", $row['user_newpasswd']) . "', user_newpasswd = ''" : '';

		$sql = "UPDATE " . USERS_TABLE . "
			SET user_active = 1, user_actkey = ''" . $sql_update_pass . " 
			WHERE user_id = " . $row['user_id']; 
		if ( !($result = $db->sql_query($sql)) )
		{
			message_die(GENERAL_ERROR, 'Could not update users table', '', __LINE__, __FILE__, $sql_update);
		}

		$sql = "INSERT INTO " . SC_USER_TABLE . "	(shakecast_user, email_address, user_type)
			VALUES (" . $row['user_id'] . ", '" .str_replace("\'", "''", $row['user_email']) . "', 'USER')";
		if ( !($result = $db->sql_query($sql)) )
		{
			message_die(GENERAL_ERROR, 'Could not insert data into shakecast users table', '', __LINE__, __FILE__, $sql);
		}

		$sql = "INSERT INTO " . VALIDATED_TABLE . "
			(validated_userid, validated_email) VALUES (" . $user_id . ", '" . $user_email . "')";
		if (!($result = $db->sql_query($sql)))
		{
			message_die(GENERAL_ERROR, "Couldn't insert data into validation table.", "", __LINE__, __FILE__, $sql);
		}

		$delivery_methods = array('EMAIL_HTML', 'EMAIL_TEXT', 'PAGER');
		foreach ($delivery_methods as $delivery_method) {
			$sql = "INSERT INTO " . USER_DELIVERY_METHOD_TABLE . "	(shakecast_user, delivery_method, delivery_address)
				VALUES (" . $user_id . ", '" .str_replace("\'", "''", $delivery_method) . "', '" .str_replace("\'", "''", $user_email) . "')";
			if (!($result = $db->sql_query($sql)))
			{
				message_die(GENERAL_ERROR, "Couldn't insert data into user delivery method table.", "", __LINE__, __FILE__, $sql);
			}
		}

		if ( intval($board_config['require_activation']) == USER_ACTIVATION_ADMIN && $sql_update_pass == '' )
		{
			include($phpbb_root_path . 'includes/emailer.'.$phpEx);
			$emailer = new emailer($board_config['smtp_delivery']);

			$emailer->from($board_config['board_email']);
			$emailer->replyto($board_config['board_email']);

			$emailer->use_template('admin_welcome_activated', $row['user_lang']);
			$emailer->email_address($row['user_email']);
			$emailer->set_subject($lang['Account_activated_subject']);

			$emailer->assign_vars(array(
				'SITENAME' => $board_config['sitename'], 
				'USERNAME' => $row['username'],
				'PASSWORD' => $password_confirm,
				'EMAIL_SIG' => (!empty($board_config['board_email_sig'])) ? str_replace('<br />', "\n", "-- \n" . $board_config['board_email_sig']) : '')
			);
			$emailer->send();
			$emailer->reset();

			$template->assign_vars(array(
				'META' => '<meta http-equiv="refresh" content="10;url=' . append_sid("index.$phpEx") . '">')
			);

			message_die(GENERAL_MESSAGE, $lang['Account_active_admin']);
		}
		else
		{
			$template->assign_vars(array(
				'META' => '<meta http-equiv="refresh" content="10;url=' . append_sid("index.$phpEx") . '">')
			);

			$message = ( $sql_update_pass == '' ) ? $lang['Account_active'] : $lang['Password_activated']; 
			message_die(GENERAL_MESSAGE, $message);
		}
	}
	else
	{
		message_die(GENERAL_MESSAGE, $lang['Wrong_activation']);
	}
}

?>