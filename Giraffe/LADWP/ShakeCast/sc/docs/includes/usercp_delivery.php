<?php
/*
# $Id: usercp_delivery.php 434 2008-08-14 18:45:18Z klin $

# The PHP interface was originally inspired by PHPBB,
# file/database formats and directory structure are quite similar.

##############################################################################
# 
# Terms and Conditions of Software Use
# ====================================
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Disclaimer of Earthquake Information
# ====================================
# 
# The data and maps provided through this system are preliminary data
# and are subject to revision. They are computer generated and may not
# have received human review or official approval. Inaccuracies in the
# data may be present because of instrument or computer
# malfunctions. Subsequent review may result in significant revisions to
# the data. All efforts have been made to provide accurate information,
# but reliance on, or interpretation of earthquake data from a single
# source is not advised. Data users are cautioned to consider carefully
# the provisional nature of the information before using it for
# decisions that concern personal or public safety or the conduct of
# business that involves substantial monetary or operational
# consequences.
# 
# Disclaimer of Software and its Capabilities
# ===========================================
# 
# This software is provided as an "as is" basis.  Attempts have been
# made to rid the program of software defects and bugs, however the
# U.S. Geological Survey (USGS) have no obligations to provide maintenance, 
# support, updates, enhancements or modifications. In no event shall USGS 
# be liable to any party for direct, indirect, special, incidental or 
# consequential damages, including lost profits, arising out of the use 
# of this software, its documentation, or data obtained though the use 
# of this software, even if USGS or have been advised of the
# possibility of such damage. By downloading, installing or using this
# program, the user acknowledges and understands the purpose and
# limitations of this software.
# 
# Contact Information
# ===================
# 
# Coordination of this effort is under the auspices of the USGS Advanced
# National Seismic System (ANSS) coordinated in Golden, Colorado, which
# functions as the clearing house for development, distribution,
# documentation, and support. For questions, comments, or reports of
# potential bugs regarding this software please contact wald@usgs.gov or
# klin@usgs.gov.  
#
#############################################################################
*/

if ( !defined('IN_SC') )
{
	die("Hacking attempt");
	exit;
}

$unhtml_specialchars_match = array('#&gt;#', '#&lt;#', '#&quot;#', '#&amp;#');
$unhtml_specialchars_replace = array('>', '<', '"', '&');

$error = FALSE;
$error_msg = '';
$page_title = $lang['Edit_profile'];
$user_id = $userdata['user_id'];
$delivery_method = array('email' => '', 'EMAIL_HTML' => '', 'EMAIL_TEXT' => '', 'PAGER' => '');
$current_delivery_method = array();
$validate_delivery_method = array();

$sql = "SELECT delivery_method, delivery_address, actkey 
	FROM " . USER_DELIVERY_METHOD_TABLE . "
	WHERE shakecast_user = $user_id";
if ( !($result = $db->sql_query($sql)) )
{
	message_die(GENERAL_ERROR, 'Could not obtain user delivery method information', '', __LINE__, __FILE__, $sql);
}

while ( $row = $db->sql_fetchrow($result) )
{
	$current_delivery_method[$row['delivery_method']] = $row['delivery_address'];
	$validate_delivery_method[$row['delivery_method']] = ($row['actkey']) ? TRUE : FALSE;
}
$db->sql_freeresult($result);
$current_delivery_method['email'] = ($HTTP_POST_VARS['current_email']) ? trim(htmlspecialchars($HTTP_POST_VARS['current_email'])) : '';

//
// Check and initialize some variables if needed
//
if ( isset($HTTP_POST_VARS['submit']) || isset($HTTP_POST_VARS['submit_x']) )
{
	include($sc_root_path . 'includes/functions_validate.'.$phpEx);

	foreach ( $delivery_method as $key => $param )
	{
		if ( !empty($HTTP_POST_VARS[$key]) )
		{
			$delivery_method[$key] = trim(htmlspecialchars($HTTP_POST_VARS[$key]));
		}
	}

	$username = ( !empty($HTTP_POST_VARS['username']) ) ? phpbb_clean_username($HTTP_POST_VARS['username']) : '';

	// session id check
	
	if ($sid == '' || $sid != $userdata['session_id'])
	{
		$error = true;
		$error_msg .= ( ( isset($error_msg) ) ? '<br />' : '' ) . $lang['Session_invalid'];
	}
	if ( $user_id != $userdata['user_id'] )
	{
		$error = TRUE;
		$error_msg .= ( ( isset($error_msg) ) ? '<br />' : '' ) . $lang['Wrong_Profile'];
	}
	
	
	if ( !$error )
	{
		//
		// The users account has been deactivated, send them an email with a new activation key
		//
		include($sc_root_path . 'includes/emailer.'.$phpEx);
		$emailer = new emailer($board_config['smtp_delivery']);

		foreach ( $delivery_method as $delivery_method_key => $delivery_address )
		{
			$validation = validate_delivery_address($userdata['user_id'], $delivery_address);
			if ( $validation['error'] && !empty($delivery_address) && $board_config['require_activation'] != USER_ACTIVATION_NONE )
			{
				$user_active = 0;
	
				$user_actkey = gen_rand_string(true);
				$key_len = 54 - ( strlen($server_url) );
				$key_len = ( $key_len > 6 ) ? $key_len : 6;
				$user_actkey = substr($user_actkey, 0, $key_len);
			}
			else
			{
				$user_active = 1;
				$user_actkey = '';
			}

			$sql = "SELECT  delivery_address 
				FROM " . USER_DELIVERY_METHOD_TABLE . " 
				WHERE shakecast_user = $user_id AND delivery_method = '" . $delivery_method_key . "'";
			if ( !($result = $db->sql_query($sql)) )
			{
				message_die(GENERAL_ERROR, 'Could not update user delivery method table', '', __LINE__, __FILE__, $sql);
			}
			
			if ($row = $db->sql_fetchrow($result))
			{
				$sql = "UPDATE " . USER_DELIVERY_METHOD_TABLE . "
					SET delivery_address = '" . str_replace("\'", "''", $delivery_address) ."', actkey = '" . str_replace("\'", "''", $user_actkey) . "'
					WHERE shakecast_user = $user_id AND delivery_method = '" . $delivery_method_key . "'";
			} 
			else 
			{
				$sql = "INSERT INTO " . USER_DELIVERY_METHOD_TABLE . "
					(delivery_address, actkey, shakecast_user, delivery_method) 
					VALUES ('" . str_replace("\'", "''", $delivery_address) ."', '" . str_replace("\'", "''", $user_actkey) . "', 
					 " . $user_id . ", '" . $delivery_method_key . "')";
			}
			if ( !($result = $db->sql_query($sql)) )
			{
				message_die(GENERAL_ERROR, 'Could not update user delivery method table', '', __LINE__, __FILE__, $sql);
			}

			if ( !$user_active )
			{
				$emailer->from($board_config['board_email']);
				$emailer->replyto($board_config['board_email']);

				$emailer->use_template('user_validate', stripslashes($user_lang));
				$emailer->email_address($delivery_address);
				$emailer->set_subject($lang['Validate']);

				$emailer->assign_vars(array(
					'SITENAME' => $board_config['sitename'],
					'USERNAME' => preg_replace($unhtml_specialchars_match, $unhtml_specialchars_replace, substr(str_replace("\'", "'", $username), 0, 25)),
					'EMAIL_SIG' => (!empty($board_config['board_email_sig'])) ? str_replace('<br />', "\n", "-- \n" . $board_config['board_email_sig']) : '',

					'U_ACTIVATE' => $server_url . '?mode=activate&' . POST_USERS_URL . '=' . $user_id . '&delivery_method=' . $delivery_method_key . '&act_key=' . $user_actkey)
				);
				$emailer->send();
				$emailer->reset();

				$message = $lang['Delivery_method_updated_inactive'] . '<br /><br />' . sprintf($lang['Click_return_index'],  '<a href="' . append_sid("index.$phpEx") . '">', '</a>');
			}
			else
			{
				$message = $lang['Delivery_method_updated'] . '<br /><br />' . sprintf($lang['Click_return_index'],  '<a href="' . append_sid("index.$phpEx") . '">', '</a>');
			}
		}

		$template->assign_vars(array(
			"META" => '<meta http-equiv="refresh" content="5;url=' . append_sid("index.$phpEx") . '">')
		);

		message_die(GENERAL_MESSAGE, $message);
	}
} // End of submit


if ( $error )
{
	//
	// If an error occured we need to stripslashes on returned data
	//
	$username = stripslashes($username);
	$email = stripslashes($email);
	$cur_password = '';
	$new_password = '';
	$password_confirm = '';

	$website = stripslashes($website);
	$location = stripslashes($location);
	$occupation = stripslashes($occupation);
	$interests = stripslashes($interests);

	$user_lang = stripslashes($user_lang);
	$user_dateformat = stripslashes($user_dateformat);

}
else if ( $mode == 'editemail' )
{
	$user_id = $userdata['user_id'];
	$username = $userdata['username'];
	$email = $userdata['user_email'];
	$cur_password = '';
	$new_password = '';
	$password_confirm = '';

	$website = $userdata['user_website'];
	$location = $userdata['user_from'];
	$occupation = $userdata['user_occ'];
	$interests = $userdata['user_interests'];

	$user_style = $userdata['user_style'];
	$user_lang = $userdata['user_lang'];
	$user_timezone = $userdata['user_timezone'];
	$user_dateformat = $userdata['user_dateformat'];

	$email_html = ($delivery_method['EMAIL_HTML']) ? $delivery_method['EMAIL_HTML']: $current_delivery_method['EMAIL_HTML'];
	$email_text = ($delivery_method['EMAIL_TEXT']) ? $delivery_method['EMAIL_TEXT'] : $current_delivery_method['EMAIL_TEXT'];
	$pager = ($delivery_method['PAGER']) ? $delivery_method['PAGER'] : $current_delivery_method['PAGER'];
	$lock_email_html = ($validate_delivery_method['EMAIL_HTML']) ? '<img src="/images/address_lock.gif" alt="Locked address">' : '';
	$lock_email_text = ($validate_delivery_method['EMAIL_TEXT']) ? '<img src="/images/address_lock.gif" alt="Locked address">' : '';
	$lock_pager = ($validate_delivery_method['PAGER']) ? '<img src="/images/address_lock.gif" alt="Locked address">' : '';
}

//
// Default pages
//
$header_tpl = 'profile_header.tpl';
include($sc_root_path . 'includes/page_header.'.$phpEx);

if ( $mode == 'editprofile' )
{
	if ( $user_id != $userdata['user_id'] )
	{
		$error = TRUE;
		$error_msg = $lang['Wrong_Profile'];
	}
}

include($sc_root_path . 'includes/functions_selects.'.$phpEx);

$s_hidden_fields = '<input type="hidden" name="mode" value="' . $mode . '" />';
$s_hidden_fields .= '<input type="hidden" name="sid" value="' . $userdata['session_id'] . '" />';
if( $mode == 'editemail' )
{
	$s_hidden_fields .= '<input type="hidden" name="user_id" value="' . $userdata['user_id'] . '" />';
	//
	// Send the users current email address. If they change it, and account activation is turned on
	// the user account will be disabled and the user will have to reactivate their account.
	//
	$s_hidden_fields .= '<input type="hidden" name="current_email" value="' . $userdata['user_email'] . '" />';
}

if ( $error )
{
	$template->set_filenames(array(
		'reg_header' => 'error_body.tpl')
	);
	$template->assign_vars(array(
		'ERROR_MESSAGE' => $error_msg)
	);
	$template->assign_var_from_handle('ERROR_BOX', 'reg_header');
}

$template->set_filenames(array(
	'body' => 'profile_email.tpl')
);

if ( $mode == 'editprofile' )
{
	$template->assign_block_vars('switch_edit_profile', array());
}

if ( $board_config['allow_namechange'] )
{
	$template->assign_block_vars('switch_namechange_allowed', array());
}
else
{
	$template->assign_block_vars('switch_namechange_disallowed', array());
}


//
// Let's do an overall check for settings/versions which would prevent
// us from doing file uploads....
//
$ini_val = ( phpversion() >= '4.0.0' ) ? 'ini_get' : 'get_cfg_var';
$form_enctype = ( @$ini_val('file_uploads') == '0' || strtolower(@$ini_val('file_uploads') == 'off') || phpversion() == '4.0.4pl1' || !$board_config['allow_avatar_upload'] || ( phpversion() < '4.0.3' && @$ini_val('open_basedir') != '' ) ) ? '' : 'enctype="multipart/form-data"';

$template->assign_vars(array(
	'USERNAME' => isset($username) ? $username : '',
	'CUR_PASSWORD' => isset($cur_password) ? $cur_password : '',
	'NEW_PASSWORD' => isset($new_password) ? $new_password : '',
	'PASSWORD_CONFIRM' => isset($password_confirm) ? $password_confirm : '',
	'EMAIL' => isset($email) ? $email : '',
	'EMAIL_HTML' => $email_html,
	'EMAIL_TEXT' => $email_text,
	'PAGER' => $pager,
	'LOCK_EMAIL_HTML' => $lock_email_html,
	'LOCK_EMAIL_TEXT' => $lock_email_text,
	'LOCK_PAGER' => $lock_pager,

	'L_SUBMIT' => $lang['Submit'],
	'L_RESET' => $lang['Reset'],
	'L_EMAIL_HTML' => $lang['EMAIL_HTML'],
	'L_EMAIL_TEXT' => $lang['EMAIL_TEXT'],
	'L_PAGER' => $lang['PAGER'],
	'L_YES' => $lang['Yes'],
	'L_NO' => $lang['No'],
	'L_USER_INFO' => "ShakeCast User",
	'L_DELIVERY_METHOD_INFO' => "Delivery Methods",
	'L_EMAIL_ADDRESS' => $lang['Email_address'],

	'S_HIDDEN_FIELDS' => $s_hidden_fields,
	'S_FORM_ENCTYPE' => $form_enctype,
	'S_PROFILE_ACTION' => append_sid("profile.$phpEx"))
);


$template->pparse('body');

include($sc_root_path . 'includes/page_tail.'.$phpEx);

?>