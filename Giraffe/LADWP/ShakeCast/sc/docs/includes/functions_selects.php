<?php
/*
# $Id: functions_selects.php 450 2008-08-15 19:09:49Z klin $
#   Origin            : phpBB function_selects.php

# The PHP interface was originally inspired by PHPBB,
# file/database formats and directory structure are quite similar.

##############################################################################
# 
# Terms and Conditions of Software Use
# ====================================
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Disclaimer of Earthquake Information
# ====================================
# 
# The data and maps provided through this system are preliminary data
# and are subject to revision. They are computer generated and may not
# have received human review or official approval. Inaccuracies in the
# data may be present because of instrument or computer
# malfunctions. Subsequent review may result in significant revisions to
# the data. All efforts have been made to provide accurate information,
# but reliance on, or interpretation of earthquake data from a single
# source is not advised. Data users are cautioned to consider carefully
# the provisional nature of the information before using it for
# decisions that concern personal or public safety or the conduct of
# business that involves substantial monetary or operational
# consequences.
# 
# Disclaimer of Software and its Capabilities
# ===========================================
# 
# This software is provided as an "as is" basis.  Attempts have been
# made to rid the program of software defects and bugs, however the
# U.S. Geological Survey (USGS) have no obligations to provide maintenance, 
# support, updates, enhancements or modifications. In no event shall USGS 
# be liable to any party for direct, indirect, special, incidental or 
# consequential damages, including lost profits, arising out of the use 
# of this software, its documentation, or data obtained though the use 
# of this software, even if USGS or have been advised of the
# possibility of such damage. By downloading, installing or using this
# program, the user acknowledges and understands the purpose and
# limitations of this software.
# 
# Contact Information
# ===================
# 
# Coordination of this effort is under the auspices of the USGS Advanced
# National Seismic System (ANSS) coordinated in Golden, Colorado, which
# functions as the clearing house for development, distribution,
# documentation, and support. For questions, comments, or reports of
# potential bugs regarding this software please contact wald@usgs.gov or
# klin@usgs.gov.  
#
#############################################################################
*/


//
// Pick a language, any language ...
//
function language_select($default, $select_name = "language", $dirname="language")
{
	global $phpEx, $sc_root_path;

	$dir = opendir($sc_root_path . $dirname);

	$lang = array();
	while ( $file = readdir($dir) )
	{
		if (preg_match('#^lang_#i', $file) && !is_file(@phpbb_realpath($sc_root_path . $dirname . '/' . $file)) && !is_link(@phpbb_realpath($sc_root_path . $dirname . '/' . $file)))
		{
			$filename = trim(str_replace("lang_", "", $file));
			$displayname = preg_replace("/^(.*?)_(.*)$/", "\\1 [ \\2 ]", $filename);
			$displayname = preg_replace("/\[(.*?)_(.*)\]/", "[ \\1 - \\2 ]", $displayname);
			$lang[$displayname] = $filename;
		}
	}

	closedir($dir);

	@asort($lang);
	@reset($lang);

	$lang_select = '<select name="' . $select_name . '">';
	while ( list($displayname, $filename) = @each($lang) )
	{
		$selected = ( strtolower($default) == strtolower($filename) ) ? ' selected="selected"' : '';
		$lang_select .= '<option value="' . $filename . '"' . $selected . '>' . ucwords($displayname) . '</option>';
	}
	$lang_select .= '</select>';

	return $lang_select;
}

//
// Pick a template/theme combo, 
//
function style_select($default_style, $select_name = "style", $dirname = "templates")
{
	global $db;

	$sql = "SELECT themes_id, style_name
		FROM " . THEMES_TABLE . "
		ORDER BY template_name, themes_id";
	if ( !($result = $db->sql_query($sql)) )
	{
		message_die(GENERAL_ERROR, "Couldn't query themes table", "", __LINE__, __FILE__, $sql);
	}

	$style_select = '<select name="' . $select_name . '">';
	while ( $row = $db->sql_fetchrow($result) )
	{
		$selected = ( $row['themes_id'] == $default_style ) ? ' selected="selected"' : '';

		$style_select .= '<option value="' . $row['themes_id'] . '"' . $selected . '>' . $row['style_name'] . '</option>';
	}
	$style_select .= "</select>";

	return $style_select;
}

//
// Pick a timezone
//
function tz_select($default, $select_name = 'timezone')
{
	global $sys_timezone, $lang;

	if ( !isset($default) )
	{
		$default == $sys_timezone;
	}
	$tz_select = '<select name="' . $select_name . '">';

	while( list($offset, $zone) = @each($lang['reg_tz']) )
	{
		$selected = ( $offset == $default ) ? ' selected="selected"' : '';
		$tz_select .= '<option value="' . $offset . '"' . $selected . '>' . $zone . '</option>';
	}
	$tz_select .= '</select>';

	return $tz_select;
}

//
// Pick a Log Level
//
function loglevel_select($default, $select_name = 'loglevel')
{
	global $sys_loglevel, $lang;

	if ( !isset($default) )
	{
		$default = 1;
	}
	$loglevel_select = '<select name="' . $select_name . '">';

	while( list($loglevel, $description) = @each($lang['loglevel']) )
	{
		$selected = ( $loglevel == $default ) ? ' selected="selected"' : '';
		$loglevel_select .= '<option value="' . $loglevel . '"' . $selected . '>' . $description . '</option>';
	}
	$loglevel_select .= '</select>';
	
	reset($lang['loglevel']);
	return $loglevel_select;
}

//
// Pick a Log Filter
//
function logfilter_select($default, $select_name = 'logfilter')
{
	global $sys_loglevel, $lang;

	if ( !isset($default) )
	{
		$default = 'no_filter';
	}
	$logfilter_select = '<select name="' . $select_name . '">';

	while( list($logfilter, $description) = @each($lang['logfilter']) )
	{
		$selected = ( $logfilter == $default ) ? ' selected="selected"' : '';
		$logfilter_select .= '<option value="' . $logfilter . '"' . $selected . '>' . $description . '</option>';
	}
	$logfilter_select .= '</select>';
	
	reset($lang['logfilter']);
	return $logfilter_select;
}

//
// Pick a Log Lines
//
function lines_select($default, $select_name = 'linesselect')
{
	global $sys_loglevel, $lang;

	if ( !isset($default) )
	{
		$default = 500;
	}
	$lines_select = '<select name="' . $select_name . '">';

	while( list($linesselect, $description) = @each($lang['linesselect']) )
	{
		$selected = ( $linesselect == $default ) ? ' selected="selected"' : '';
		$lines_select .= '<option value="' . $linesselect . '"' . $selected . '>' . $description . '</option>';
	}
	$lines_select .= '</select>';
	
	reset($lang['linesselect']);
	return $lines_select;
}

//
// Pick a Command Line option
//
function cmd_option_select($default, $select_name = 'cmdoption')
{
	global $sys_loglevel, $lang;

	if ( !isset($default) )
	{
		$default = 'default';
	}
	$cmd_option_select = '<select name="cmdoption">';

	while( list($cmdoption, $description) = @each($lang[$select_name]) )
	{
		$selected = ( $linesselect == $default ) ? ' selected="selected"' : '';
		$cmd_option_select .= '<option value="' . $cmdoption . '"' . $selected . '>' . $description . '</option>';
	}
	$cmd_option_select .= '</select>';
	
	reset($lang[$select_name]);
	return $cmd_option_select;
}

//
// Pick a Command Line option
//
function task_select($default, $select_name = 'task')
{
	global $sys_loglevel, $lang;

	if ( !isset($default) )
	{
		$default = 'default';
	}
	$cmd_option_select = '<select name="task">';

	while( list($cmdoption, $description) = @each($lang[$select_name]) )
	{
		$selected = ( $linesselect == $default ) ? ' selected="selected"' : '';
		$cmd_option_select .= '<option value="' . $cmdoption . '"' . $selected . '>' . $description . '</option>';
	}
	$cmd_option_select .= '</select>';
	
	reset($lang[$select_name]);
	return $cmd_option_select;
}

//
// Pick a Command Line option
//
function repeater_select($default, $select_name = 'repeater')
{
	global $sys_loglevel, $lang;

	if ( !isset($default) )
	{
		$default = 'onceonly';
	}
	$repeater_select = '<select name="repeater">';

	while( list($cmdoption, $description) = @each($lang[$select_name]) )
	{
		$selected = ( $linesselect == $default ) ? ' selected="selected"' : '';
		$repeater_select .= '<option value="' . $cmdoption . '"' . $selected . '>' . $description . '</option>';
	}
	$repeater_select .= '</select>';
	
	reset($lang[$select_name]);
	return $repeater_select;
}

//
// Pick a Command Line option
//
function clock_select($default, $select_name = 'hour')
{
	global $sys_loglevel, $lang;

	if ( !isset($default) )
	{
		$default = 0;
	}
	
	$count = ($select_name == 'hour') ? 23 : 59 ;
	$clock_select = '<select name="'.$select_name.'">';

    for ($i = 0; $i <= $count; $i++) {
        $clock_select .= '<option value="' . $i . '" ';
        if ($default == $i) {
            $clock_select .= 'selected="SELECTED"';
        }
        $clock_select .= '>' . $i . '</option>';
    }
	$clock_select .= '</select>';
	
	return $clock_select;
}

?>