<?php
/*
# $Id: eq_map.php 195 2007-11-05 15:43:28Z klin $

# The PHP interface was originally inspired by PHPBB,
# file/database formats and directory structure are quite similar.

##############################################################################
# 
# Terms and Conditions of Software Use
# ====================================
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Disclaimer of Earthquake Information
# ====================================
# 
# The data and maps provided through this system are preliminary data
# and are subject to revision. They are computer generated and may not
# have received human review or official approval. Inaccuracies in the
# data may be present because of instrument or computer
# malfunctions. Subsequent review may result in significant revisions to
# the data. All efforts have been made to provide accurate information,
# but reliance on, or interpretation of earthquake data from a single
# source is not advised. Data users are cautioned to consider carefully
# the provisional nature of the information before using it for
# decisions that concern personal or public safety or the conduct of
# business that involves substantial monetary or operational
# consequences.
# 
# Disclaimer of Software and its Capabilities
# ===========================================
# 
# This software is provided as an "as is" basis.  Attempts have been
# made to rid the program of software defects and bugs, however the
# U.S. Geological Survey (USGS) have no obligations to provide maintenance, 
# support, updates, enhancements or modifications. In no event shall USGS 
# be liable to any party for direct, indirect, special, incidental or 
# consequential damages, including lost profits, arising out of the use 
# of this software, its documentation, or data obtained though the use 
# of this software, even if USGS or have been advised of the
# possibility of such damage. By downloading, installing or using this
# program, the user acknowledges and understands the purpose and
# limitations of this software.
# 
# Contact Information
# ===================
# 
# Coordination of this effort is under the auspices of the USGS Advanced
# National Seismic System (ANSS) coordinated in Golden, Colorado, which
# functions as the clearing house for development, distribution,
# documentation, and support. For questions, comments, or reports of
# potential bugs regarding this software please contact wald@usgs.gov or
# klin@usgs.gov.  
#
#############################################################################
*/

define('IN_SC', true);
$sc_root_path = './';
include($sc_root_path . 'extension.inc');
include($sc_root_path . 'common.'.$phpEx);

//
// Start session management
//
$userdata = session_pagestart($user_ip, PAGE_FAQ);
init_userprefs($userdata);
//
// End session management
//
//
// Start session management
//
$userdata = session_pagestart($user_ip, $sc_list_id);
init_userprefs($userdata);
//
// End session management
//



if ( isset($HTTP_GET_VARS[EVENT_TOPIC_URL]) || isset($HTTP_POST_VARS[EVENT_TOPIC_URL]) )
{
	$event_id = ( isset($HTTP_GET_VARS[EVENT_TOPIC_URL]) ) ? $HTTP_GET_VARS[EVENT_TOPIC_URL] : $HTTP_POST_VARS[EVENT_TOPIC_URL];
}
else
{
	$event_id = '';
}

if ( isset($HTTP_GET_VARS[EVENT_TOPIC_VERSION]) || isset($HTTP_POST_VARS[EVENT_TOPIC_VERSION]) )
{
	$event_version = ( isset($HTTP_GET_VARS[EVENT_TOPIC_VERSION]) ) ? intval($HTTP_GET_VARS[EVENT_TOPIC_VERSION]) : intval($HTTP_POST_VARS[EVENT_TOPIC_VERSION]);
}
else
{
	$event_version = '';
}

//
// End session management
//

$lang_file = 'lang_main';
$l_title = 'Google Maps';

include($sc_root_path . 'language/lang_' . $board_config['default_lang'] . '/' . $lang_file . '.' . $phpEx);
if (isset($_GET['event'])) {
	$l_title = $l_title . ' for ShakeMap ' . $_GET['event'];
} else {
	$l_title = $l_title . ' for ShakeCast Facilities ';
}

//
// Lets build a page ...
//
$header_tpl = 'eqmap_header.tpl';
$page_title = $l_title;
$active_class = 'C_EARTHQUAKE';
include($sc_root_path . 'includes/page_header.'.$phpEx);

$template->set_filenames(array(
	'body' => 'eqmap_body.tpl')
);

$view_table_url = append_sid("event.$phpEx?f=2&" . EVENT_TOPIC_URL . "=$event_id&"
	 . EVENT_TOPIC_VERSION . "=$event_version");
$template->assign_vars(array(
	'L_TITLE' => $l_title, 
	'L_EQ_TABLE' => $lang['EQ_Table_view'],

	'U_EQ_TABLE' => $view_table_url
	)
);

$template->pparse('body');

include($sc_root_path . 'includes/page_tail.'.$phpEx);

?>