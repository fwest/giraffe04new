<?php
/*
# $Id: eq_list.php 509 2008-10-20 14:42:02Z klin $

# The PHP interface was originally inspired by PHPBB,
# file/database formats and directory structure are quite similar.

##############################################################################
# 
# Terms and Conditions of Software Use
# ====================================
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Disclaimer of Earthquake Information
# ====================================
# 
# The data and maps provided through this system are preliminary data
# and are subject to revision. They are computer generated and may not
# have received human review or official approval. Inaccuracies in the
# data may be present because of instrument or computer
# malfunctions. Subsequent review may result in significant revisions to
# the data. All efforts have been made to provide accurate information,
# but reliance on, or interpretation of earthquake data from a single
# source is not advised. Data users are cautioned to consider carefully
# the provisional nature of the information before using it for
# decisions that concern personal or public safety or the conduct of
# business that involves substantial monetary or operational
# consequences.
# 
# Disclaimer of Software and its Capabilities
# ===========================================
# 
# This software is provided as an "as is" basis.  Attempts have been
# made to rid the program of software defects and bugs, however the
# U.S. Geological Survey (USGS) have no obligations to provide maintenance, 
# support, updates, enhancements or modifications. In no event shall USGS 
# be liable to any party for direct, indirect, special, incidental or 
# consequential damages, including lost profits, arising out of the use 
# of this software, its documentation, or data obtained though the use 
# of this software, even if USGS or have been advised of the
# possibility of such damage. By downloading, installing or using this
# program, the user acknowledges and understands the purpose and
# limitations of this software.
# 
# Contact Information
# ===================
# 
# Coordination of this effort is under the auspices of the USGS Advanced
# National Seismic System (ANSS) coordinated in Golden, Colorado, which
# functions as the clearing house for development, distribution,
# documentation, and support. For questions, comments, or reports of
# potential bugs regarding this software please contact wald@usgs.gov or
# klin@usgs.gov.  
#
#############################################################################
*/

define('IN_SC', true);
$sc_root_path = './';
//include($sc_root_path . 'includes/begin_caching.php');
include($sc_root_path . 'extension.inc');
include($sc_root_path . 'common.'.$phpEx);

//
// Start initial var setup
//
if ( isset($HTTP_GET_VARS[SC_LIST_URL]) || isset($HTTP_POST_VARS[SC_LIST_URL]) )
{
	$sc_list_id = ( isset($HTTP_GET_VARS[SC_LIST_URL]) ) ? intval($HTTP_GET_VARS[SC_LIST_URL]) : intval($HTTP_POST_VARS[SC_LIST_URL]);
}
else
{
	$sc_list_id = '';
}

$start = ( isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;
$start = ($start < 0) ? 0 : $start;

//
// End initial var setup
//

//
// Start session management
//
$userdata = session_pagestart($user_ip, $sc_list_id);
init_userprefs($userdata);
//
// End session management
//

//
// Start auth check
//
if ( !$userdata['session_logged_in'] )
{
	$redirect = SC_LIST_URL . "=$sc_list_id" . ( ( isset($start) ) ? "&start=$start" : '' );
	redirect(append_sid("login.$phpEx?redirect=eq_list.$phpEx&$redirect", true));
}
//
// End of auth check
//

$tracking_forums = ( isset($HTTP_COOKIE_VARS[$board_config['cookie_name'] . '_f']) ) ? unserialize($HTTP_COOKIE_VARS[$board_config['cookie_name'] . '_f']) : '';

//
// Do the forum Prune
//
/*
if ( $is_auth['auth_mod'] && $board_config['prune_enable'] )
{
	if ( $forum_row['prune_next'] < time() && $forum_row['prune_enable'] )
	{
		include($sc_root_path . 'includes/prune.'.$phpEx);
		require($sc_root_path . 'includes/functions_admin.'.$phpEx);
		auto_prune($sc_list_id);
	}
}
*/
//
// End of forum prune
//
if (isset($HTTP_GET_VARS['sort_key']))
{
	$sort_key = $HTTP_GET_VARS['sort_key'];
	$sort_order = ( isset($HTTP_GET_VARS['sort_order']) ) ? $HTTP_GET_VARS['sort_order'] : 'd';
}
else 
{
	$sort_key = 'receive_timestamp';
	$sort_order = ( isset($HTTP_GET_VARS['sort_order']) ) ? $HTTP_GET_VARS['sort_order'] : 'd';
}

$default_sort = array($sort_key, $sort_order);
$new_sort_order = ($sort_order == 'd') ? 'a' : 'd';
$img_url = ' <img src="'.$sc_root_path . '/images/' . $sort_order . '.png" border="0" width="10" height="10">';
$base_url = append_sid("eq_list.$phpEx?" . SC_LIST_URL ."=$sc_list_id");

//
// Grab all the basic data (all topics except announcements)
// for this forum
//
$total_topics = 0;
$sm_sql = "SELECT s.shakemap_id, s.shakemap_version, s.receive_timestamp, s.shakemap_region, 
			e.event_type, e.event_location_description, e.magnitude, e.lat, e.lon
	FROM " . SHAKEMAP_TABLE ." s INNER JOIN ". EVENT_TABLE . " e on
		s.event_id = e.event_id AND s.event_version = e.event_version";
$count_sql = "SELECT count(s.shakemap_id) as total
	FROM " . SHAKEMAP_TABLE ." s INNER JOIN ". EVENT_TABLE . " e on
		s.event_id = e.event_id AND s.event_version = e.event_version";
switch ($sc_list_id) {
	case 1:
	$sql = "SELECT shakemap_id, shakemap_version, grid_id
		FROM grid
		ORDER BY grid_id DESC LIMIT 1";
		if ( !($result = $db->sql_query($sql)) )
		{
		   message_die(GENERAL_ERROR, 'Could not obtain ShakeMap information', '', __LINE__, __FILE__, $sql);
		}
		while( $row = $db->sql_fetchrow($result) )
		{
			$shakemap_id = $row['shakemap_id'];
			$shakemap_version = $row['shakemap_version'];
		}
		$db->sql_freeresult($result);
			
		$sql = $sm_sql . "
			WHERE 
				s.shakemap_id = \"$shakemap_id\"
			ORDER BY s.receive_timestamp DESC, s.shakemap_version DESC ";
		$count_sql .= "
			WHERE 
				s.shakemap_id = \"$shakemap_id\"
			ORDER BY s.receive_timestamp DESC, s.shakemap_version DESC ";
		break;
	case 2:
		$sql = $sm_sql . "
			WHERE
				s.superceded_timestamp IS  NULL  
				AND NOT (e.event_type = 'TEST' OR e.event_type = 'SCENARIO')
			ORDER BY s.receive_timestamp DESC, s.shakemap_version DESC";
		$count_sql .= "
			WHERE
				s.superceded_timestamp IS  NULL  
				AND NOT (e.event_type = 'TEST' OR e.event_type = 'SCENARIO')
			ORDER BY s.receive_timestamp DESC, s.shakemap_version DESC";
		break;
	case 3:
		$sql = $sm_sql . "
			WHERE
				s.superceded_timestamp IS  NULL  
				AND (e.event_type = 'TEST' OR e.event_type = 'SCENARIO')
			ORDER BY s.receive_timestamp DESC, s.shakemap_version DESC ";
		$count_sql .= "
			WHERE
				s.superceded_timestamp IS  NULL  
				AND (e.event_type = 'TEST' OR e.event_type = 'SCENARIO')
			ORDER BY s.receive_timestamp DESC, s.shakemap_version DESC ";
		break;
	default:
		$sql = $sm_sql . "
			WHERE
				s.superceded_timestamp IS  NULL 
			ORDER BY s.receive_timestamp DESC, s.shakemap_version DESC";
		$count_sql .= "
			WHERE
				s.superceded_timestamp IS  NULL 
			ORDER BY s.receive_timestamp DESC, s.shakemap_version DESC";
}

$result = $db->sql_query($count_sql);
if( !$result )
{
	message_die(GENERAL_ERROR, "Couldn't get ShakeMap list information.", "", __LINE__, __FILE__, $sql );
}
$row = $db->sql_fetchrow($result);
$total_topics = $row['total'];
$db->sql_freeresult($result);

//if ($board_config['topics_per_page']) {$sql .= " LIMIT ". $board_config['topics_per_page'];}
//if ($start) {$sql .= " OFFSET $start";}
if ( !($result = $db->sql_query($sql)) )
{
   message_die(GENERAL_ERROR, 'Could not obtain ShakeMap information', '', __LINE__, __FILE__, $sql);
}

while( $row = $db->sql_fetchrow($result) )
{
	$sql = "SELECT count(fs.facility_id) as facility_count
		FROM facility_shaking fs INNER JOIN grid g ON
			fs.grid_id = g.grid_id
		WHERE
			g.shakemap_id = '".$row['shakemap_id']."' AND 
			g.shakemap_version = ".$row['shakemap_version'];
	if ( ($count_result = $db->sql_query($sql)) )
	{
	   $count_row = $db->sql_fetchrow($count_result);
	   $row['facility_count'] = $count_row['facility_count'];
	}

	$topic_rowset[] = $row;
	$db->sql_freeresult($count_result);
}

$db->sql_freeresult($result);

switch ($sort_key) {
	case 'location':
		$order_arr = array(
			$default_sort, array('lat', $sort_order), array('lon', 'd'));
		break;
	case 'earthquake':
		$order_arr = array(
			$default_sort, array('shakemap_region', $sort_order), array('shakemap_id', 'd'));
		break;
	default:
		$order_arr = array($default_sort);
}

$topic_rowset = arfsort( $topic_rowset, $order_arr);

if ($sc_list_id == 1 && $total_topics == 1) {
	header ("Location: event.php?f=$sc_list_id&event=$shakemap_id&version=$shakemap_version"); 
	exit;
}

//
// Dump out the page header and load viewforum template
//
define('SHOW_ONLINE', true);
$page_title = 'Earthquake List';
$header_tpl = 'eqlist_header.tpl';
$active_class = 'C_EARTHQUAKE';
include($sc_root_path . 'includes/page_header.'.$phpEx);

$template->set_filenames(array(
	'body' => 'eqlist_body.tpl')
);

$template->assign_vars(array(
	'L_EARTHQUAKE' => 'Earthquake' . (($sort_key == 'event_location_description') ? $img_url : ''),
	'L_REGION' => 'Network',
	'L_EVENT_ID' => $lang['Event_ID'] . (($sort_key == 'earthquake') ? $img_url : ''),
	'L_TIME' => $lang['Time'] . (($sort_key == 'receive_timestamp') ? $img_url : ''),
	'L_LOCATION' => $lang['Location'] . (($sort_key == 'location') ? $img_url : ''),
	'L_MAGNITUDE' => $lang['Magnitude'] . (($sort_key == 'magnitude') ? $img_url : ''),
	'L_FACILITY_COUNT' => "No. Facility Evaluated" . (($sort_key == 'facility_count') ? $img_url : ''),

	'U_FACILITY_COUNT' =>  $base_url."&sort_key=facility_count&sort_order=" . (($sort_key == 'facility_count') ? $new_sort_order : $sort_order),
	'U_MAGNITUDE' => $base_url."&sort_key=magnitude&sort_order=" . (($sort_key == 'magnitude') ? $new_sort_order : $sort_order),
	'U_TIME' => $base_url."&sort_key=receive_timestamp&sort_order=" . (($sort_key == 'receive_timestamp') ? $new_sort_order : $sort_order),
	'U_LOCATION' => $base_url."&sort_key=location&sort_order=" . (($sort_key == 'location') ? $new_sort_order : $sort_order),
	'U_EARTHQUAKE' => $base_url."&sort_key=event_location_description&sort_order=" . (($sort_key == 'event_location_description') ? $new_sort_order : $sort_order),
	'U_EVENT_ID' => $base_url."&sort_key=earthquake&sort_order=" . (($sort_key == 'earthquake') ? $new_sort_order : $sort_order),

	'U_VIEW_FORUM' => append_sid("eq_list.$phpEx?" . SC_LIST_URL ."=$sc_list_id"))

);
//
// End header
//

//
// Okay, lets dump out the page ...
//
if( $total_topics )
{
	for($i = $start; $i < count($topic_rowset) && $i < ($board_config['topics_per_page'] + $start); $i++)
	{
		$shakemap_id = $topic_rowset[$i]['shakemap_id'];
		$shakemap_version = $topic_rowset[$i]['shakemap_version'];

		$description = $topic_rowset[$i]['event_location_description'];
		$description = $description . " (Version " . $shakemap_version . ")";
		
		$region = strtoupper($topic_rowset[$i]['shakemap_region']);

		$post_time = strtotime($topic_rowset[$i]['receive_timestamp']);
		$last_update = create_date($board_config['default_dateformat'], $post_time, $board_config['board_timezone']);

		$topic_magnitude = $topic_rowset[$i]['magnitude'];
		
		$view_event_url = append_sid("event.$phpEx?" . SC_LIST_URL . "=$sc_list_id&" . 
			EVENT_TOPIC_URL . "=$shakemap_id&" . EVENT_TOPIC_VERSION . "=$shakemap_version");

		$lat = $topic_rowset[$i]['lat'];
		$lon = $topic_rowset[$i]['lon'];
		
		
		$template->assign_block_vars('topicrow', array(
			//'ROW_COLOR' => $row_color,
			'ROW_COLOR' => ($i & 1),
			'REGION' => $region,
			'NEWEST_POST_IMG' => $newest_post_img, 
			'DESCRIPTION' => $description,
			'MAG' => $topic_magnitude,
			'LAT' => $lat, 
			'LON' => $lon, 
			'FACILITY_COUNT' => $topic_rowset[$i]['facility_count'],
			'SHAKEMAP_ID' => $shakemap_id,
			'LAST_UPDATE' => $last_update, 

			'U_VIEW_EVENT' => $view_event_url)
		);
	}

	$template->assign_vars(array(
		'PAGINATION' => generate_pagination("eq_list.$phpEx?f=$sc_list_id&sort_key=$sort_key&sort_order=$sort_order", $total_topics, $board_config['topics_per_page'], $start),
		'PAGE_NUMBER' => sprintf($lang['Page_of'], ( floor( $start / $board_config['topics_per_page'] ) + 1 ), ceil( $total_topics / $board_config['topics_per_page'] )), 

		'L_GOTO_PAGE' => $lang['Goto_page'])
	);
}
else
{
	//
	// No ShakeMaps
	//
	$no_topics_msg = $lang['No_ShakeMaps'];
	$template->assign_vars(array(
		'L_NO_TOPICS' => $no_topics_msg)
	);

	$template->assign_block_vars('switch_no_topics', array() );

}

//
// Parse the page and print
//
$template->pparse('body');

//
// Page footer
//
include($sc_root_path . 'includes/page_tail.'.$phpEx);

?>