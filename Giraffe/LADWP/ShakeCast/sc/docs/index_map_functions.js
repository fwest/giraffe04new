var event = gup('event');
//var version = gup('version');
if (!event) {
	event = 'lastevent';
}
var event_getVars = 'event='+event;

var map;
//var centerLatitude = 37.7;
//var centerLongitude = -122.2;
var startZoom = 6;
			var currentType = 'All';
			var allMarkers = {};
			var allTypes = {};

//Create the tile layer object
var detailLayer = new GTileLayer(new GCopyrightCollection(''));

//Method to retrieve the URL of the tile
detailLayer.getTileUrl = function(tile, zoom){
    //pass the x and y position as wel as the zoom
    var tileURL = "server.php?x="+tile.x+"&y="+tile.y+"&zoom="+zoom;
	//GLog.writeUrl(tileURL);
    return tileURL;
};

detailLayer.isPng = function() {
    //The example uses GIF's
    return true;
}

//add your tiles to the normal map projection
detailMapLayers = G_NORMAL_MAP.getTileLayers();
detailMapLayers.push(detailLayer);

//add your tiles to the satellite map projection
detailMapLayers = G_SATELLITE_MAP.getTileLayers();
detailMapLayers.push(detailLayer);

//add your tiles to the satellite map projection
detailMapLayers = G_HYBRID_MAP.getTileLayers();
detailMapLayers.push(detailLayer);



function changeBodyClass(from, to) {
     document.body.className = document.body.className.replace(from, to);
     return false;
}

function init() {
//	handleResize();

    map = new GMap2(document.getElementById("map"));
    map.setCenter(new GLatLng(centerLatitude, centerLongitude), startZoom);
    //map.setMapType(G_NORMAL_MAP);
    map.setMapType(G_HYBRID_MAP);
	if (event != 'lastevent') {
    map.addControl(new GSmallMapControl());
    map.addControl(new GMapTypeControl());
	}

	//Add shakemap 
	initializeShakeMap();

	/*if (user_profile) {
			var selectedProfile = new Array();
		for (var i in user_profile) {
			selectedProfile[i] = geom[user_profile[i]];
		}
			drawPolyline(selectedProfile);
	}*/
	
	/*GEvent.addListener(map,'mousemove',function(latlng) {
		var pixelLocation = map.fromLatLngToDivPixel(latlng);
		GLog.write('ll:' + latlng + 'at:' + pixelLocation);
	});*/

	changeBodyClass('loading', 'standby');
	//GLog.write('data loaded');
}


function drawPolyline(latlonstring) {
	map.clearOverlays();
	for (var i in latlonstring) {
	var latlngs = latlonstring[i].split(",");
    var pointCount = latlngs.length;
    var id;
	
    map.addControl(new GSmallMapControl());
    map.addControl(new GMapTypeControl());
	/*var	sidebarid = document.getElementById('sidebar-list');
	while (sidebarid.firstChild) {
		sidebarid.removeChild(sidebarid.firstChild);
	}*/
	var lat_avg = 0;
	var lon_avg = 0;
	var poly_latlngs = [];
	for (var ind=0; ind < latlngs.length; ind = ind+2) {
		poly_latlngs.push(new GLatLng(latlngs[ind], latlngs[ind+1]));
		lat_avg = lat_avg + parseFloat(latlngs[ind]);
		lon_avg = lon_avg + parseFloat(latlngs[ind+1]);
	}
	lat_avg = lat_avg / poly_latlngs.length;
	lon_avg = lon_avg / poly_latlngs.length;
    // Plot polyline, adding the first element to the end, to close the loop.
    //var polyline = new GPolyline(latlngs, 'FF6633', 4, 0.8);
    var polyline = new GPolygon(poly_latlngs, 'FF6633', 4, 0.8, 'FF9966', 0.5);
    map.addOverlay(polyline);
	}
		
	map.panTo(new GLatLng(lat_avg, lon_avg));
	
	// set us up to zap the current polyline when we draw the next one.
	removePolyline = function() {
	  map.removeOverlay(polyline);
	}	
}

function initializeShakeMap() {
	//remove the existing points
	//map.clearOverlays();
	//create the boundary for the data
	//log the URL for testing
	//GLog.writeUrl('map_data.php?'+getVars);
	//retrieve the points using Ajax
	var request = GXmlHttp.create();
	request.open('GET', 'shakemap_xml.php?'+event_getVars, true);
	//GLog.writeUrl('shakemap_xml.php?'+event_getVars);
	request.onreadystatechange = function() {
		if (request.readyState == 4) {
			var xml = request.responseXML;
			var shakecast_id = xml.documentElement.getElementsByTagName("shakecast_id").item(0).firstChild.nodeValue;
		if (shakecast_id != '') {
			var LatLonBox = xml.documentElement.getElementsByTagName("LatLonBox").item(0);
			var lat_min = parseFloat(LatLonBox.getAttribute("south"));
			var lat_max = parseFloat(LatLonBox.getAttribute("north"));
			var lon_min = parseFloat(LatLonBox.getAttribute("west"));
			var lon_max = parseFloat(LatLonBox.getAttribute("east"));
			//for(id in shakemap_bounds) {
				var rectBounds = new GLatLngBounds(
					new GLatLng(lat_min, lon_min), 
					new GLatLng(lat_max, lon_max));
				//var img = document.createElement('img');
				var img = '/data/' + shakecast_id + '/ii_overlay.png';
				var sm_rect = new Rectangle(rectBounds, img);
				map.addOverlay(sm_rect);
				map.panTo(new GLatLng((lat_min + lat_max)/2,
						(lon_min + lon_max)/2));
				//createMarker(point);
			var event = xml.documentElement.getElementsByTagName("event").item(0);
			var lat = parseFloat(event.getAttribute("lat"));
			var lon = parseFloat(event.getAttribute("lon"));
			var point = new GLatLng(lat, lon);
			var icon = new GIcon();
			icon.image = "images/epicenter.png";
			icon.shadow = "images/shadow\-epicenter.png";
			icon.iconSize = new GSize(25, 25);
			icon.shadowSize = new GSize(38, 25);
			icon.iconAnchor = new GPoint(12, 12);
			icon.infoWindowAnchor = new GPoint(12, 12);
			//GLog.writeUrl(icon.image);
			var description = "<table border=1><tr bgcolor=#bbbbbb><td colspan=2 align=center><strong>" + 
				event.getAttribute("locstring") + "</strong>" + 
				"<tr><td><table bgcolor=#eeeeee width=100%>" + 
				"<tr><td colspan=2><font size=-1>Event ID: <strong>" + 
				event.getAttribute("id") + "</strong></td></tr>" +
				"<tr><td colspan=2><font size=-1>Magnitude: <strong>" + 
				event.getAttribute("magnitude") + "</strong></td></tr>" +
				"<tr><td><font size=-1>Lat: <strong>" + event.getAttribute("lat") + 
				"<strong></td><td><font size=-1>Lon: <strong>" + event.getAttribute("lon") + "<strong></td></tr>" +
				"<tr><td colspan=2><font size=-1>Time: <strong>" + event.getAttribute("timestamp") + 
				"<strong></td></tr></table></td></tr></table>";
			var marker = new GMarker(point, icon);
			GEvent.addListener(marker, "click", function(){
				marker.openInfoWindowHtml(description);
			});	
			GEvent.addListener(marker, "dblclick", function(){
				window.location = "eq_map.php?" + event_getVars;
			});	
			map.addOverlay(marker);
					//}
		}
		}
	}

	request.send(null);
}


function gup( name ) {
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var tmpURL = window.location.href;
  var results = regex.exec( tmpURL );
  if( results == null )
    return "";
  else
    return results[1];
}

window.onload = init;