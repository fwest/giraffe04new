<?php
/*
# $Id: suggestions.php 138 2007-08-23 21:49:39Z klin $

# The PHP interface was originally inspired by PHPBB,
# file/database formats and directory structure are quite similar.

##############################################################################
# 
# Terms and Conditions of Software Use
# ====================================
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Disclaimer of Earthquake Information
# ====================================
# 
# The data and maps provided through this system are preliminary data
# and are subject to revision. They are computer generated and may not
# have received human review or official approval. Inaccuracies in the
# data may be present because of instrument or computer
# malfunctions. Subsequent review may result in significant revisions to
# the data. All efforts have been made to provide accurate information,
# but reliance on, or interpretation of earthquake data from a single
# source is not advised. Data users are cautioned to consider carefully
# the provisional nature of the information before using it for
# decisions that concern personal or public safety or the conduct of
# business that involves substantial monetary or operational
# consequences.
# 
# Disclaimer of Software and its Capabilities
# ===========================================
# 
# This software is provided as an "as is" basis.  Attempts have been
# made to rid the program of software defects and bugs, however the
# U.S. Geological Survey (USGS) have no obligations to provide maintenance, 
# support, updates, enhancements or modifications. In no event shall USGS 
# be liable to any party for direct, indirect, special, incidental or 
# consequential damages, including lost profits, arising out of the use 
# of this software, its documentation, or data obtained though the use 
# of this software, even if USGS or have been advised of the
# possibility of such damage. By downloading, installing or using this
# program, the user acknowledges and understands the purpose and
# limitations of this software.
# 
# Contact Information
# ===================
# 
# Coordination of this effort is under the auspices of the USGS Advanced
# National Seismic System (ANSS) coordinated in Golden, Colorado, which
# functions as the clearing house for development, distribution,
# documentation, and support. For questions, comments, or reports of
# potential bugs regarding this software please contact wald@usgs.gov or
# klin@usgs.gov.  
#
#############################################################################
*/

define('IN_SC', true);
$sc_root_path = './';
//include($sc_root_path . 'includes/begin_caching.php');
include($sc_root_path . 'extension.inc');
include($sc_root_path . 'common.'.$phpEx);

    //plain text header
    header("Content-Type: text/plain; charset=UTF-8");
    
    //get the data that was posted
	$fp = fopen("php://input", 'r');

	$oData = json_decode(stream_get_contents($fp));
    $aSuggestions = array();
	
    //make sure there's text
    if (strlen($oData->text) > 0) {

		switch($oData->requesting)
		{
			case 'facility':
			//create the SQL query string
			$sQuery = "Select facility_id, facility_name from ".$oData->requesting." where facility_name like '".
					  $oData->text."%' limit 0,".$oData->limit;
			break;

			case 'facility_type_fragility':
			//create the SQL query string
			$sQuery = "Select facility_type, damage_level, low_limit, high_limit, metric from ".$oData->requesting." where facility_type = '".
					  $oData->text."' ";
			break;

			case 'facility_type':
			//create the SQL query string
			$sQuery = "Select facility_type, name, description from ".$oData->requesting." where facility_type = '".
					  $oData->text."' ";
			break;

		}

              
        //make the database connection
        if($result = $db->sql_query($sQuery)) {
            while ($row = $db->sql_fetchrow($result)) {            
                $aSuggestions[] = $row;
            }
		}
    
		if ( count($aSuggestions) == 0 && $oData->requesting == 'facility_type_fragility' ) {
			$sQuery = "Select damage_level from damage_level";
			if($result = $db->sql_query($sQuery)) {
				while ($row = $db->sql_fetchrow($result)) {            
					$aSuggestions[] = $row;
				}
			}
		}
       //mysql_free_result($oResult);
		$db->sql_freeresult($result);
        
    }
    
    echo(json_encode($aSuggestions));
?>