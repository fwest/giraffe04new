-- MySQL dump 10.11
--
-- Host: localhost    Database: sc
-- ------------------------------------------------------
-- Server version	5.0.41-community-nt

-- 
-- Dumping data for table `aggregation_counted`
-- 

INSERT INTO `aggregation_counted` (`name`) VALUES 
('EVENT_ID');

-- 
-- Dumping data for table `aggregation_grouped`
-- 

INSERT INTO `aggregation_grouped` (`name`) VALUES 
('DAMAGE_LEVEL'),
('METRIC/GRID_VALUE');

-- 
-- Dumping data for table `aggregation_measured`
-- 

INSERT INTO `aggregation_measured` (`name`) VALUES 
('GRID_VALUE'),
('MAGNITUDE');

-- 
-- Dumping data for table `damage_level`
-- 

INSERT INTO `damage_level` (`DAMAGE_LEVEL`, `NAME`, `DESCRIPTION`, `SEVERITY_RANK`, `IS_MAX_SEVERITY`, `UPDATE_USERNAME`, `UPDATE_TIMESTAMP`) VALUES 
('GREEN', 'Damage Unlikely or Slightly', 'Damage is not likely to the facility.', 100, 0, 'kwl', '2007-07-25 00:00:00'),
('YELLOW', 'Moderate Damage Possible', 'This facility has possibly suffered damage.', 200, 0, 'kwl', '2007-07-25 00:00:00'),
('ORANGE', 'Extensive Damage Possible', 'This facility has possibly suffered extensive damage.', 300, 0, 'kwl', '2007-07-25 00:00:00'),
('RED', 'Complete Damage Possible', 'This facility has probably suffered damage.', 400, 1, 'kwl', '2007-07-25 00:00:00');

-- 
-- Dumping data for table `delivery_method`
-- 

INSERT INTO `delivery_method` (`DELIVERY_METHOD`, `NAME`, `DESCRIPTION`, `SCRIPT_NAME`, `UPDATE_USERNAME`, `UPDATE_TIMESTAMP`) VALUES 
('PAGER', 'Pager', 'Text message or SMS message on cell phone', NULL, 'pan', '2003-01-01 00:00:00'),
('EMAIL_TEXT', 'Text Email', 'Text message in electronic mail.', NULL, 'pan', '2003-01-01 00:00:00'),
('EMAIL_HTML', 'HTML Email', 'HTML message in electronic mail.', NULL, 'pan', '2003-01-01 00:00:00'),
('VOICE', 'Voice', 'Generated voice message', NULL, 'pan', '2003-01-01 00:00:00'),
('SCRIPT', 'Script', 'Execute a script', NULL, 'shc', '2003-10-22 00:00:00');

-- 
-- Dumping data for table `delivery_status`
-- 

INSERT INTO `delivery_status` (`DELIVERY_STATUS`, `NAME`, `DESCRIPTION`, `UPDATE_USERNAME`, `UPDATE_TIMESTAMP`) VALUES 
('IN_PROCESS', 'In Process', 'Delivery is in process or underway', 'pan', '2003-01-01 00:00:00'),
('ACTIVE', 'Active', 'Delivery is currently underway', 'pan', '2003-01-01 00:00:00'),
('PENDING', 'Pending', 'Delivery has been attempted, pending confirmation', 'pan', '2003-01-01 00:00:00'),
('COMPLETED', 'Completed', 'Delivery has been successfully completed', 'pan', '2003-01-01 00:00:00'),
('FAILED', 'Failure', 'Delivery was not completed', 'pan', '2003-01-01 00:00:00'),
('ERRORS', 'Errors Encountered', 'The last delivery attempt encountered errors', 'pan', '2003-01-01 00:00:00'),
('REJECTED', 'Rejected', 'The delivery was rejected at the other end', 'pan', '2003-01-01 00:00:00');

-- 
-- Dumping data for table `event`
-- 

INSERT INTO `event` (`EVENT_ID`, `EVENT_VERSION`, `EVENT_STATUS`, `EVENT_TYPE`, `EVENT_NAME`, `EVENT_LOCATION_DESCRIPTION`, `EVENT_TIMESTAMP`, `EXTERNAL_EVENT_ID`, `RECEIVE_TIMESTAMP`, `MAGNITUDE`, `LAT`, `LON`, `SEQ`, `INITIAL_VERSION`, `SUPERCEDED_TIMESTAMP`) VALUES 
('Northridge_scte', 1, 'NORMAL', 'TEST', 'Northridge', 'Northridge', '1994-01-17 12:30:55', 'ciNorthridge_scte', '2007-06-26 15:43:17', 6.7, 34.213, -118.5357, 1, 1, NULL);

-- 
-- Dumping data for table `event_status`
-- 

INSERT INTO `event_status` (`EVENT_STATUS`, `NAME`, `DESCRIPTION`, `UPDATE_USERNAME`, `UPDATE_TIMESTAMP`) VALUES 
('NORMAL', 'Normal', 'Typical', 'pan', '2003-01-01 00:00:00'),
('CANCELLED', 'Cancelled', 'The event was cancelled or retracted', 'pan', '2003-01-01 00:00:00'),
('INCOMPLETE', 'Incomplete', 'The event has been created but the event data is incomplete', 'pan', '2003-01-01 00:00:00'),
('RELEASED', 'Released', 'Released for use', 'dwb', '2003-09-17 00:00:00');

-- 
-- Dumping data for table `event_type`
-- 

INSERT INTO `event_type` (`EVENT_TYPE`, `NAME`, `DESCRIPTION`, `UPDATE_USERNAME`, `UPDATE_TIMESTAMP`) VALUES 
('ACTUAL', 'Actual', 'An actual earthquake event', 'pan', '2003-01-01 00:00:00'),
('TEST', 'Local Test', 'A test event', 'pan', '2003-01-01 00:00:00'),
('SCENARIO', 'Scenario', 'A ShakeCast Scenario event', 'pan', '2003-01-01 00:00:00'),
('HEARTBEAT', 'Heartbeat', 'Network heartbeat', 'dwb', '2004-08-25 00:00:00');

-- 
-- Dumping data for table `facility_type`
-- 

INSERT INTO `facility_type` VALUES ('C1HH', 'C1H High Code', 'C1H High Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('C1HM', 'C1H Moderate Code', 'C1H Moderate Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('C1HL', 'C1H Low Code', 'C1H Low Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('C1HP', 'C1H Pre Code', 'C1H Pre Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('C1MH', 'C1M High Code', 'C1M High Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('C1MM', 'C1M Moderate Code', 'C1M Moderate Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('C1ML', 'C1M Low Code', 'C1M Low Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('C1MP', 'C1M Pre Code', 'C1M Pre Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('C1LH', 'C1L High Code', 'C1L High Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('C1LM', 'C1L Moderate Code', 'C1L Moderate Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('C1LL', 'C1L Low Code', 'C1L Low Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('C1LP', 'C1L Pre Code', 'C1L Pre Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('C2HH', 'C2H High Code', 'C2H High Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('C2HM', 'C2H Moderate Code', 'C2H Moderate Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('C2HL', 'C2H Low Code', 'C2H Low Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('C2HP', 'C2H Pre Code', 'C2H Pre Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('C2MH', 'C2M High Code', 'C2M High Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('C2MM', 'C2M Moderate Code', 'C2M Moderate Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('C2ML', 'C2M Low Code', 'C2M Low Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('C2MP', 'C2M Pre Code', 'C2M Pre Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('C2LH', 'C2L High Code', 'C2L High Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('C2LM', 'C2L Moderate Code', 'C2L Moderate Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('C2LL', 'C2L Low Code', 'C2L Low Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('C2LP', 'C2L Pre Code', 'C2L Pre Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('C3HL', 'C3H Low Code', 'C3H Low Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('C3HP', 'C3H Pre Code', 'C3H Pre Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('C3LL', 'C3L Low Code', 'C3L Low Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('C3LP', 'C3L Pre Code', 'C3L Pre Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('C3ML', 'C3M Low Code', 'C3M Low Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('C3MP', 'C3M Pre Code', 'C3M Pre Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('MHH', 'MH High Code', 'MH High Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('MHM', 'MH Moderate Code', 'MH Moderate Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('MHL', 'MH Low Code', 'MH Low Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('MHP', 'MH Pre Code', 'MH Pre Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('PC1H', 'PC1 High Code', 'PC1 High Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('PC1M', 'PC1 Moderate Code', 'PC1 Moderate Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('PC1L', 'PC1 Low Code', 'PC1 Low Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('PC1P', 'PC1 Pre Code', 'PC1 Pre Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('PC2HH', 'PC2H High Code', 'PC2H High Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('PC2HM', 'PC2H Moderate Code', 'PC2H Moderate Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('PC2HL', 'PC2H Low Code', 'PC2H Low Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('PC2HP', 'PC2H Pre Code', 'PC2H Pre Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('PC2MH', 'PC2M High Code', 'PC2M High Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('PC2MM', 'PC2M Moderate Code', 'PC2M Moderate Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('PC2ML', 'PC2M Low Code', 'PC2M Low Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('PC2MP', 'PC2M Pre Code', 'PC2M Pre Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('PC2LH', 'PC2L High Code', 'PC2L High Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('PC2LM', 'PC2L Moderate Code', 'PC2L Moderate Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('PC2LL', 'PC2L Low Code', 'PC2L Low Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('PC2LP', 'PC2L Pre Code', 'PC2L Pre Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('RM1MH', 'RM1M High Code', 'RM1M High Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('RM1MM', 'RM1M Moderate Code', 'RM1M Moderate Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('RM1ML', 'RM1M Low Code', 'RM1M Low Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('RM1MP', 'RM1M Pre Code', 'RM1M Pre Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('RM1LH', 'RM1L High Code', 'RM1L High Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('RM1LM', 'RM1L Moderate Code', 'RM1L Moderate Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('RM1LL', 'RM1L Low Code', 'RM1L Low Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('RM1LP', 'RM1L Pre Code', 'RM1L Pre Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('RM2HH', 'RM2H High Code', 'RM2H High Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('RM2HM', 'RM2H Moderate Code', 'RM2H Moderate Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('RM2HL', 'RM2H Low Code', 'RM2H Low Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('RM2HP', 'RM2H Pre Code', 'RM2H Pre Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('RM2MH', 'RM2M High Code', 'RM2M High Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('RM2MM', 'RM2M Moderate Code', 'RM2M Moderate Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('RM2ML', 'RM2M Low Code', 'RM2M Low Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('RM2MP', 'RM2M Pre Code', 'RM2M Pre Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('RM2LH', 'RM2L High Code', 'RM2L High Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('RM2LM', 'RM2L Moderate Code', 'RM2L Moderate Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('RM2LL', 'RM2L Low Code', 'RM2L Low Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('RM2LP', 'RM2L Pre Code', 'RM2L Pre Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S1HH', 'S1H High Code', 'S1H High Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S1HM', 'S1H Moderate Code', 'S1H Moderate Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S1HL', 'S1H Low Code', 'S1H Low Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S1HP', 'S1H Pre Code', 'S1H Pre Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S1MH', 'S1M High Code', 'S1M High Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S1MM', 'S1M Moderate Code', 'S1M Moderate Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S1ML', 'S1M Low Code', 'S1M Low Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S1MP', 'S1M Pre Code', 'S1M Pre Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S1LH', 'S1L High Code', 'S1L High Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S1LM', 'S1L Moderate Code', 'S1L Moderate Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S1LL', 'S1L Low Code', 'S1L Low Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S1LP', 'S1L Pre Code', 'S1L Pre Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S2HH', 'S2H High Code', 'S2H High Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S2HM', 'S2H Moderate Code', 'S2H Moderate Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S2HL', 'S2H Low Code', 'S2H Low Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S2HP', 'S2H Pre Code', 'S2H Pre Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S2MH', 'S2M High Code', 'S2M High Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S2MM', 'S2M Moderate Code', 'S2M Moderate Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S2ML', 'S2M Low Code', 'S2M Low Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S2MP', 'S2M Pre Code', 'S2M Pre Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S2LH', 'S2L High Code', 'S2L High Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S2LM', 'S2L Moderate Code', 'S2L Moderate Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S2LL', 'S2L Low Code', 'S2L Low Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S2LP', 'S2L Pre Code', 'S2L Pre Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S3H', 'S3 High Code', 'S3 High Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S3M', 'S3 Moderate Code', 'S3 Moderate Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S3L', 'S3 Low Code', 'S3 Low Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S3P', 'S3 Pre Code', 'S3 Pre Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S4HH', 'S4H High Code', 'S4H High Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S4HM', 'S4H Moderate Code', 'S4H Moderate Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S4HL', 'S4H Low Code', 'S4H Low Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S4HP', 'S4H Pre Code', 'S4H Pre Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S4MH', 'S4M High Code', 'S4M High Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S4MM', 'S4M Moderate Code', 'S4M Moderate Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S4ML', 'S4M Low Code', 'S4M Low Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S4MP', 'S4M Pre Code', 'S4M Pre Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S4LH', 'S4L High Code', 'S4L High Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S4LM', 'S4L Moderate Code', 'S4L Moderate Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S4LL', 'S4L Low Code', 'S4L Low Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S4LP', 'S4L Pre Code', 'S4L Pre Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S5HL', 'S5H Low Code', 'S5H Low Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S5HP', 'S5H Pre Code', 'S5H Pre Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S5LL', 'S5L Low Code', 'S5L Low Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S5LP', 'S5L Pre Code', 'S5L Pre Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S5ML', 'S5M Low Code', 'S5M Low Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('S5MP', 'S5M Pre Code', 'S5M Pre Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('URMLL', 'URML Low Code', 'URML Low Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('URMLP', 'URML Pre Code', 'URML Pre Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('URMML', 'URMM Low Code', 'URMM Low Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('URMMP', 'URMM Pre Code', 'URMM Pre Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('W1H', 'W1 High Code', 'W1 High Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('W1M', 'W1 Moderate Code', 'W1 Moderate Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('W1L', 'W1 Low Code', 'W1 Low Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('W1P', 'W1 Pre Code', 'W1 Pre Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('W2H', 'W2 High Code', 'W2 High Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('W2M', 'W2 Moderate Code', 'W2 Moderate Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('W2L', 'W2 Low Code', 'W2 Low Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('W2P', 'W2 Pre Code', 'W2 Pre Code', 'kwl', '2007-07-14 12:18:10');
INSERT INTO `facility_type` VALUES ('BRIDGE', 'Bridge', 'Bridge or overpass', 'pan', '2003-01-01 00:00:00');
INSERT INTO `facility_type` VALUES ('CAMPUS', 'Multi-building campus', 'A campus of multiple buildings and multiple types', 'pan', '2003-01-01 00:00:00');
INSERT INTO `facility_type` VALUES ('CAPITAL', 'Captial', 'Country and State Captial', 'kwl', '2006-10-22 15:40:54');
INSERT INTO `facility_type` VALUES ('CITY', 'City', 'A city point', 'dwb', '2004-04-23 12:56:11');
INSERT INTO `facility_type` VALUES ('CORA', 'Dam - Constant radius arch', 'Constant radius arch', 'dwb', '2004-08-26 12:07:29');
INSERT INTO `facility_type` VALUES ('COUNTY', 'County', 'A county', 'shc', NULL);
INSERT INTO `facility_type` VALUES ('CRIB', 'Dam - Crib', 'Crib', 'dwb', '2004-08-26 12:07:29');
INSERT INTO `facility_type` VALUES ('DAM', 'Dam', 'Dam', 'pan', '2003-01-01 00:00:00');
INSERT INTO `facility_type` VALUES ('DISTRICT', 'District', 'An administrative district', 'shc', '2003-09-16 00:00:00');
INSERT INTO `facility_type` VALUES ('ENGINEERED', 'Engineered Building', 'Engineered Building', 'pan', '2003-01-01 00:00:00');
INSERT INTO `facility_type` VALUES ('ERRK', 'Dam - Earth & rock', 'Earth & rock', 'dwb', '2004-08-26 12:07:29');
INSERT INTO `facility_type` VALUES ('ERTH', 'Dam - Earth', 'Earth', 'dwb', '2004-08-26 12:07:29');
INSERT INTO `facility_type` VALUES ('FLBT', 'Dam - Flashboard & buttress', 'Flashboard & buttress', 'dwb', '2004-08-26 12:07:29');
INSERT INTO `facility_type` VALUES ('GRAV', 'Dam - Gravity', 'Gravity', 'dwb', '2004-08-26 12:07:29');
INSERT INTO `facility_type` VALUES ('HYDF', 'Dam - Hydraulic fill', 'Hydraulic fill', 'dwb', '2004-08-26 12:07:29');
INSERT INTO `facility_type` VALUES ('INDUSTRIAL', 'Industrial', 'An industrial or manufacturing facility', 'pan', '2003-01-01 00:00:00');
INSERT INTO `facility_type` VALUES ('MULA', 'Dam - Multiple arch', 'Multiple arch', 'dwb', '2004-08-26 12:07:29');
INSERT INTO `facility_type` VALUES ('MULTIFAM', 'Multi-family housing unit', 'Multi-family housing unit, such as apartment, condo, hotel, etc.', 'pan', '2003-01-01 00:00:00');
INSERT INTO `facility_type` VALUES ('NUCLEAR', 'Nuclear - Facility', 'Power Plant', 'kwl', '2006-07-21 08:17:02');
INSERT INTO `facility_type` VALUES ('RECT', 'Dam - Reinforced concrete', 'Reinforced concrete', 'dwb', '2004-08-26 12:07:29');
INSERT INTO `facility_type` VALUES ('ROAD', 'Basic Roadway', 'Basic roadway, not bridge, overpass, underpass, or tunnel', 'pan', '2003-01-01 00:00:00');
INSERT INTO `facility_type` VALUES ('ROCK', 'Dam - Rockfill', 'Rockfill', 'dwb', '2004-08-26 12:07:29');
INSERT INTO `facility_type` VALUES ('SINGLEFAM', 'Single Family Home', 'Single family home', 'pan', '2003-01-01 00:00:00');
INSERT INTO `facility_type` VALUES ('SLBT', 'Dam - Slab & buttress', 'Slab & buttress', 'dwb', '2004-08-26 12:07:29');
INSERT INTO `facility_type` VALUES ('STRUCTURE', 'Structure', 'A general structure', 'pan', '2003-01-01 00:00:00');
INSERT INTO `facility_type` VALUES ('TANK', 'Tank or Concrete Reservoir', 'Tank or reservoir for water, gas, or product', 'pan', '2003-01-01 00:00:00');
INSERT INTO `facility_type` VALUES ('TUNNEL', 'Tunnel', 'Tunnel', 'pan', '2003-01-01 00:00:00');
INSERT INTO `facility_type` VALUES ('UNKNOWN', 'Unknown Facility Type', 'The type of the facility is unknown', 'pan', '2003-01-01 00:00:00');
INSERT INTO `facility_type` VALUES ('VARA', 'Dam - Variable radius arch', 'Variable radius arch', 'dwb', '2004-08-26 12:10:24');
INSERT INTO `facility_type` VALUES ('VA_HOSP', 'VA Hospital', NULL, 'kwl', '2007-02-21 09:35:12');

-- 
-- Dumping data for table `grid`
-- 

INSERT INTO `grid` (`SHAKEMAP_ID`, `SHAKEMAP_VERSION`, `LAT_MIN`, `LAT_MAX`, `LON_MIN`, `LON_MAX`, `ORIGIN_LAT`, `ORIGIN_LON`, `LATITUDE_CELL_COUNT`, `LONGITUDE_CELL_COUNT`, `GRID_ID`) VALUES 
('Northridge_scte', 1, 33.3797, 35.0463, -119.786, -117.286, 34.21, -118.54, 101, 151, 1);

-- 
-- Dumping data for table `metric`
-- 

INSERT INTO `metric` (`SHORT_NAME`, `METRIC_ID`, `NAME`, `DESCRIPTION`, `UPDATE_USERNAME`, `UPDATE_TIMESTAMP`) VALUES 
('PGA', '1', 'Peak Ground Acceleration', NULL, 'pan', '2003-01-01 00:00:00'),
('PGV', '2', 'Peak Ground Velocity', NULL, 'dwb', '2003-05-06 16:45:00'),
('MMI', '3', 'Instrumental Intensity', NULL, 'dwb', '2003-05-06 16:47:00'),
('PSA03', '4', 'Peak Spectral Acc. at 0.3 sec', NULL, 'pan', '2003-01-01 00:00:00'),
('PSA10', '5', 'Peak Spectral Acc. at 1.0 sec', NULL, 'pan', '2003-01-01 00:00:00'),
('PSA30', '6', 'Peak Spectral Acc. at 3.0 sec', NULL, 'pan', '2003-01-01 00:00:00'),
('SDPGA', '7', 'PGA Uncertainty in Std Deviation', NULL, 'kwl', '2008-10-01 15:00:00'),
('SVEL', '8', 'Estimated Vs30 in m/s', NULL, 'kwl', '2008-10-01 15:00:00');

-- 
-- Dumping data for table `notification_class`
-- 

INSERT INTO `notification_class` (`NOTIFICATION_CLASS`, `NAME`, `DESCRIPTION`, `UPDATE_USERNAME`, `UPDATE_TIMESTAMP`) VALUES 
('EVENT', 'Event', 'New event notification', 'pan', '2003-01-01 00:00:00'),
('SYSTEM', 'System', 'System notification', 'pan', '2003-01-01 00:00:00'),
('FACILITY', 'Facility', 'Facility shaking notification', 'pan', '2003-01-01 00:00:00'),
('PRODUCT', 'Product', 'New product notification', 'pan', '2003-01-01 00:00:00'),
('GRID', 'Grid', 'Grid-related notification', NULL, NULL);

-- 
-- Dumping data for table `notification_request_status`
-- 

INSERT INTO `notification_request_status` (`PARMNAME`, `PARMVALUE`) VALUES 
('LAST_EVENT_SEQ', '1'),
('LAST_PRODUCT_SEQ', '1'),
('LAST_GRID_SEQ', '1'),
('LAST_STATION_SEQ', '1'),
('LAST_SYSTEM_SEQ', '1');

-- 
-- Dumping data for table `notification_type`
-- 

INSERT INTO `notification_type` (`NOTIFICATION_TYPE`, `NOTIFICATION_CLASS`, `NAME`, `DESCRIPTION`, `NOTIFICATION_ATTEMPTS`, `UPDATE_USERNAME`, `UPDATE_TIMESTAMP`) VALUES 
('CAN_EVENT', 'EVENT', 'Cancel Event', 'The event was cancelled (no longer exists)', 5, 'pan', '2003-01-01 00:00:00'),
('NEW_EVENT', 'EVENT', 'New Event', 'A new event has been processeed', 10, 'pan', '2003-01-01 00:00:00'),
('UPD_EVENT', 'EVENT', 'Updated event', 'Updated data has been received for an existing event', 5, 'pan', '2003-01-01 00:00:00'),
('SHAKING', 'GRID', 'Facility Shaken', 'Facility was shaken', 10, 'pan', '2003-01-01 00:00:00'),
('NEW_PROD', 'PRODUCT', 'New Product', 'A new product has arrived', 5, NULL, NULL),
('DAMAGE', 'GRID', 'Facility Damage', 'Facility Damage Possibility', 10, NULL, NULL),
('SYSTEM', 'SYSTEM', 'System Message', 'Abnormal system process result', 10, 'kwl', '2008-05-03 12:28:00');

-- 
-- Dumping data for table `phpbb_config`
-- 

INSERT INTO `phpbb_config` (`config_name`, `config_value`) VALUES 
('config_id', '1'),
('board_disable', '0'),
('sitename', 'ShakeCast 2.0'),
('site_desc', 'ShakeCast GUI framework based on phpBB'),
('cookie_name', 'sc2mysql'),
('cookie_path', '/'),
('cookie_domain', ''),
('cookie_secure', '0'),
('session_length', '3600'),
('allow_html', '1'),
('allow_html_tags', 'b,i,u,pre'),
('allow_bbcode', '0'),
('allow_smilies', '0'),
('allow_sig', '0'),
('allow_namechange', '0'),
('allow_theme_create', '0'),
('allow_avatar_local', '0'),
('allow_avatar_remote', '0'),
('allow_avatar_upload', '0'),
('enable_confirm', '1'),
('allow_autologin', '1'),
('max_autologin_time', '0'),
('override_user_style', '1'),
('posts_per_page', '15'),
('topics_per_page', '50'),
('hot_threshold', '25'),
('max_poll_options', '10'),
('max_sig_chars', '255'),
('max_inbox_privmsgs', '50'),
('max_sentbox_privmsgs', '25'),
('max_savebox_privmsgs', '50'),
('board_email_sig', 'Thanks, The Management'),
('board_email', 'admin@localhost'),
('smtp_delivery', '1'),
('smtp_host', ''),
('smtp_username', ''),
('smtp_password', ''),
('sendmail_fix', '0'),
('require_activation', '2'),
('flood_interval', '15'),
('search_flood_interval', '15'),
('search_min_chars', '3'),
('max_login_attempts', '5'),
('login_reset_time', '30'),
('board_email_form', '0'),
('avatar_filesize', '6144'),
('avatar_max_width', '80'),
('avatar_max_height', '80'),
('avatar_path', 'images/avatars'),
('avatar_gallery_path', 'images/avatars/gallery'),
('smilies_path', 'images/smiles'),
('default_style', '1'),
('default_dateformat', 'D M d, Y g:i a'),
('board_timezone', '1'),
('prune_enable', '1'),
('privmsg_disable', '0'),
('gzip_compress', '0'),
('coppa_fax', ''),
('coppa_mail', ''),
('record_online_users', '3'),
('record_online_date', '1174183554'),
('server_name', 'localhost'),
('server_port', '80'),
('script_path', '/'),
('version', '.0.520'),
('rand_seed', '5e081d69ced8c38c2627ed0e41fd37cd'),
('board_startdate', '1168467455'),
('default_lang', 'english'),
('gm_api_key', 'ABQIAAAAFr1SZqAxLssG5UzEVXknDBTj573nqvj6o6MHA_eeQuLMzbm6sxTrZwK6OWNeNUIdh-v522rVLcDM3A'),
('UserID', 'www'),
('GroupID', 'www'),
('RootDir', 'c:/shakecast/sc'),
('DataRoot', 'c:/shakecast/sc/data'),
('LogDir', 'c:/shakecast/sc/logs'),
('LogFile', 'sc.log'),
('LogLevel', '2'),
('LocalServerId', '1'),
('Threshold', ''),
('TemplateDir', 'c:/shakecast/sc/templates'),
('Logrotate_LOGSTATDIR', 'c:/shakecast/sc/docs/images'),
('Logrotate_logfile', 'c:/shakecast/sc/logs/sc.log c:/shakecast/sc/logs/sc_access.log c:/shakecast/sc/logs/sc_error.log'),
('Logrotate_rotate-time', '1 week'),
('Logrotate_max_size', '100 M'),
('Logrotate_keep-files', '5'),
('Logrotate_compress', 'Yes'),
('Logrotate_status-file', 'c:/shakecast/sc/logs/logrotate.status'),
('Poller_AUTOSTART', '1'),
('Poller_LOG', 'c:/shakecast/sc/logs/sc.log'),
('Poller_LOGGING', '2'),
('Poller_MSGLEVEL', '2'),
('Poller_POLL', '120'),
('Poller_PORT', '53457'),
('Poller_PROMPT', 'polld>'),
('Poller_SERVICE_NAME', 'polld'),
('Poller_SERVICE_TITLE', 'ShakeCast Polling Daemon'),
('Poller_SPOLL', '10'),
('Notification_From', 'shakecast@sample.gov'),
('Notification_EnvelopeFrom', 'shakecast@sample.com'),
('Notification_SmtpServer', 'smtp.sample.com'),
('Notification_DefaultEmailTemplate', 'default.txt'),
('Notification_DefaultScriptTemplate', 'default.pl'),
('Destination_Hostname', 'localhost'),
('Destination_Password', 'scadmin'),
('NotifyQueue_LogLevel', '2'),
('NotifyQueue_ServiceTitle', 'ShakeCast Notification Generator'),
('NotifyQueue_Spoll', '10'),
('NotifyQueue_ScanPeriod', '60'),
('Notify_LogLevel', '2'),
('Notify_ServiceTitle', 'ShakeCast Notification Distributor'),
('Notify_Spoll', '10'),
('Notify_ScanPeriod', '60'),
('rss_AUTOSTART', '1'),
('rss_LOG', 'c:/shakecast/sc/logs/sc.log'),
('rss_LOGGING', '1'),
('rss_MSGLEVEL', '2'),
('rss_POLL', '60'),
('rss_PORT', '53458'),
('rss_PROMPT', 'rssd>'),
('rss_SERVICE_NAME', 'rssd'),
('rss_SERVICE_TITLE', 'ShakeCast RSS Daemon'),
('rss_SPOLL', '10'),
('rss_REGION', 'SC CI NC NN'),
('Admin_HtPasswordPath', 'C:/Program Files/Apache Group/Apache2/bin/htpasswd'),
('Admin_ServerPwdFile', 'c:/shakecast/sc/userdbs/sc-servers'),
('Admin_UserPwdFile', 'c:/shakecast/sc/userdbs/sc-users'),
('Dispatcher_MinWorkers', '2'),
('Dispatcher_MaxWorkers', '20'),
('Dispatcher_WorkerPort', '58163'),
('Dispatcher_RequestPort', '58164'),
('Dispatcher_WorkerTimeout', '3600'),
('Dispatcher_AUTOSTART', '1'),
('Dispatcher_LOG', 'c:/shakecast/sc/logs/sc.log'),
('Dispatcher_LOGGING', '1'),
('Dispatcher_PORT', '53456'),
('Dispatcher_PROMPT', 'dispd>'),
('Dispatcher_POLL', '1'),
('Dispatcher_SPOLL', '1'),
('Dispatcher_SERVICE_NAME', 'dispd'),
('Dispatcher_SERVICE_TITLE', 'ShakeCast Dispatcher'),
('rss_TIME_WINDOW', '7'),
('Notification_Username', ''),
('Notification_Password', '');

-- 
-- Dumping data for table `phpbb_groups`
-- 

INSERT INTO `phpbb_groups` (`group_id`, `group_type`, `group_name`, `group_description`, `group_moderator`, `group_single_user`) VALUES 
(1, 1, 'Anonymous', 'Personal User', 0, 1),
(2, 1, 'Admin', 'Personal User', 0, 1);

-- 
-- Dumping data for table `phpbb_themes`
-- 

INSERT INTO `phpbb_themes` (`themes_id`, `template_name`, `style_name`, `head_stylesheet`, `body_background`, `body_bgcolor`, `body_text`, `body_link`, `body_vlink`, `body_alink`, `body_hlink`, `tr_color1`, `tr_color2`, `tr_color3`, `tr_class1`, `tr_class2`, `tr_class3`, `th_color1`, `th_color2`, `th_color3`, `th_class1`, `th_class2`, `th_class3`, `td_color1`, `td_color2`, `td_color3`, `td_class1`, `td_class2`, `td_class3`, `fontface1`, `fontface2`, `fontface3`, `fontsize1`, `fontsize2`, `fontsize3`, `fontcolor1`, `fontcolor2`, `fontcolor3`, `span_class1`, `span_class2`, `span_class3`, `img_size_poll`, `img_size_privmsg`) VALUES 
(1, 'subSilver', 'subSilver', 'subSilver.css', '', 'E5E5E5', '000000', '006699', '5493B4', '', 'DD6900', 'EFEFEF', 'DEE3E7', 'D1D7DC', '', '', '', '98AAB1', '006699', 'FFFFFF', 'cellpic1.gif', 'cellpic3.gif', 'cellpic2.jpg', 'FAFAFA', 'FFFFFF', '', 'row1', 'row2', '', 'Verdana, Arial, Helvetica, sans-serif', 'Trebuchet MS', 'Courier, ''Courier New'', sans-serif', 10, 11, 12, '444444', '006600', 'FFA34F', '', '', '', NULL, NULL),
(2,'Caltrans','Caltrans','Caltrans.css','','E5E5E5','000000','006699','5493B4','','DD6900','EFEFEF','DEE3E7','D1D7DC','','','','98AAB1','006699','FFFFFF','cellpic1.gif','cellpic3.gif','cellpic2.jpg','FAFAFA','FFFFFF','','row1','row2','','Verdana, Arial, Helvetica, sans-serif','Trebuchet MS','Courier, \'Courier New\', sans-serif',10,11,12,'444444','006600','FFA34F','','','',0,0);

-- 
-- Dumping data for table `phpbb_themes_name`
-- 

INSERT INTO `phpbb_themes_name` (`themes_id`, `tr_color1_name`, `tr_color2_name`, `tr_color3_name`, `tr_class1_name`, `tr_class2_name`, `tr_class3_name`, `th_color1_name`, `th_color2_name`, `th_color3_name`, `th_class1_name`, `th_class2_name`, `th_class3_name`, `td_color1_name`, `td_color2_name`, `td_color3_name`, `td_class1_name`, `td_class2_name`, `td_class3_name`, `fontface1_name`, `fontface2_name`, `fontface3_name`, `fontsize1_name`, `fontsize2_name`, `fontsize3_name`, `fontcolor1_name`, `fontcolor2_name`, `fontcolor3_name`, `span_class1_name`, `span_class2_name`, `span_class3_name`) VALUES 
(1, 'The lightest row colour', 'The medium row color', 'The darkest row colour', '', '', '', 'Border round the whole page', 'Outer table border', 'Inner table border', 'Silver gradient picture', 'Blue gradient picture', 'Fade-out gradient on index', 'Background for quote boxes', 'All white areas', '', 'Background for topic posts', '2nd background for topic posts', '', 'Main fonts', 'Additional topic title font', 'Form fonts', 'Smallest font size', 'Medium font size', 'Normal font size (post body etc)', 'Quote & copyright text', 'Code text colour', 'Main table header text colour', '', '', '');

-- 
-- Dumping data for table `phpbb_users`
-- 

INSERT INTO `phpbb_users` (`user_id`, `user_active`, `username`, `user_password`, `user_session_time`, `user_session_page`, `user_lastvisit`, `user_regdate`, `user_level`, `user_posts`, `user_timezone`, `user_style`, `user_lang`, `user_dateformat`, `user_new_privmsg`, `user_unread_privmsg`, `user_last_privmsg`, `user_login_tries`, `user_last_login_try`, `user_emailtime`, `user_viewemail`, `user_attachsig`, `user_allowhtml`, `user_allowbbcode`, `user_allowsmile`, `user_allowavatar`, `user_allow_pm`, `user_allow_viewonline`, `user_notify`, `user_notify_pm`, `user_popup_pm`, `user_rank`, `user_avatar`, `user_avatar_type`, `user_email`, `user_icq`, `user_fullname`, `user_from`, `user_sig`, `user_sig_bbcode_uid`, `user_aim`, `user_yim`, `user_msnm`, `user_occ`, `user_organization`, `user_actkey`, `user_newpasswd`) VALUES 
(-1, 0, 'Anonymous', '', 0, 0, 0, 1168467455, 0, 0, '0.00', NULL, '', '', 0, 0, 0, 0, 0, NULL, 0, 0, 1, 1, 1, 1, 0, 1, 0, 1, 0, NULL, '', 0, '', '', '', '', '', NULL, '', '', '', '', '', '', ''),
(1, 1, 'scadmin', 'c33cd727cf4697f71cd45ef32328f730', 1182867178, 0, 1182865374, 1168467455, 1, 3, '0.00', 1, 'english', 'D M d, Y g:i a', 0, 0, 1174229798, 0, 0, NULL, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '');

-- 
-- Dumping data for table `phpbb_user_group`
-- 

INSERT INTO `phpbb_user_group` (`group_id`, `user_id`, `user_pending`) VALUES 
(1, -1, 0);

-- 
-- Dumping data for table `product`
-- 

INSERT INTO `product` (`SHAKEMAP_ID`, `SHAKEMAP_VERSION`, `PRODUCT_TYPE`, `PRODUCT_ID`, `PRODUCT_STATUS`, `GENERATING_SERVER`, `MAX_VALUE`, `MIN_VALUE`, `GENERATION_TIMESTAMP`, `RECEIVE_TIMESTAMP`, `UPDATE_TIMESTAMP`, `LAT_MIN`, `LAT_MAX`, `LON_MIN`, `LON_MAX`, `PRODUCT_FILE_EXISTS`, `SUPERCEDED_TIMESTAMP`) VALUES 
('Northridge_scte', 1, 'GRID', 1, 'REVIEWED', 1, NULL, NULL, '2004-06-29 15:36:05', '2007-06-26 15:43:17', '2007-06-26 15:43:18', 33.3797, 35.0463, -119.786, -117.286, '1', NULL);

-- 
-- Dumping data for table `product_format`
-- 

INSERT INTO `product_format` (`PRODUCT_FORMAT`, `NAME`, `DESCRIPTION`, `UPDATE_USERNAME`, `UPDATE_TIMESTAMP`) VALUES 
(1, 'Grid', 'An XYZ Grid file with header row', 'pan', '2003-01-01 00:00:00'),
(2, 'Grid XML', 'A grid file in XML format', 'pan', '2003-01-01 00:00:00'),
(3, 'ShapeFile', 'A GIS Shape File set', 'pan', '2003-01-01 00:00:00'),
(4, 'JPG', 'A JPEG image', 'pan', '2003-01-01 00:00:00'),
(5, 'PNG', 'A Portable Network Graphics image', 'pan', '2003-01-01 00:00:00'),
(6, 'PostScript', 'A PostScript image', 'pan', '2003-01-01 00:00:00'),
(7, 'HAZUS', 'HAZUS files', 'pan', '2003-01-01 00:00:00'),
(8, 'ShakeMail', 'A ShakeMail-format ASCII picture', 'pan', '2003-01-01 00:00:00'),
(9, 'Media Guide', 'A text guide for use by the media', 'pan', '2003-01-01 00:00:00'),
(10, 'TV JPG', 'A JPEG file suitable for use on television', 'pan', '2003-01-01 00:00:00'),
(11, 'TV Bare JPG', 'A JPEG file without decoration, suitable for use on television', 'pan', '2003-01-01 00:00:00'),
(12, 'TV PS', 'A PostScript file suitable for use on television', 'pan', '2003-01-01 00:00:00'),
(13, 'TV Bare PS', 'A PostScript file without decoration suitable for use on television', 'pan', '2003-01-01 00:00:00');

-- 
-- Dumping data for table `product_status`
-- 

INSERT INTO `product_status` (`PRODUCT_STATUS`, `NAME`, `DESCRIPTION`, `UPDATE_USERNAME`, `UPDATE_TIMESTAMP`) VALUES 
('RELEASED', 'Released', 'Released for use', 'pan', '2003-01-01 00:00:00'),
('REVIEWED', 'Reviewed', 'Released for use and reviewed by a human', 'pan', '2003-01-01 00:00:00'),
('CANCELLED', 'Cancelled', 'Cancelled or retracted - not to be used', 'pan', '2003-01-01 00:00:00');

-- 
-- Dumping data for table `product_type`
-- 

INSERT INTO `product_type` (`PRODUCT_TYPE`, `NAME`, `DESCRIPTION`, `METRIC`, `FILENAME`, `UPDATE_USERNAME`, `UPDATE_TIMESTAMP`) VALUES 
('CONT_PGA', 'PGA contour data', NULL, 'PGA', 'cont_pga.dat', 'dwb', '2003-07-24 10:46:00'),
('CONT_PGV', 'PGV contour data', NULL, 'PGV', 'cont_pgv.dat', 'dwb', '2003-08-01 18:00:00'),
('CONT_PSA03', 'PSA03 contour data', NULL, 'PSA03', 'cont_psa03.dat', 'dwb', '2003-07-24 10:46:00'),
('CONT_PSA10', 'PSA10 contour data', NULL, 'PSA10', 'cont_psa10.dat', 'dwb', '2003-07-24 10:46:00'),
('CONT_PSA30', 'PSA30 contour data', NULL, 'PSA30', 'cont_psa30.dat', 'dwb', '2003-07-24 10:46:00'),
('EVT_TXT', 'event description', NULL, NULL, 'event.txt', 'dwb', '2003-07-24 10:46:00'),
('GRID', 'Grid', NULL, NULL, 'grid.xyz.zip', NULL, NULL),
('HAZUS', 'HAZUS archive', NULL, NULL, 'hazus.zip', NULL, NULL),
('INTEN_JPG', 'Instrumental Intensity JPEG', NULL, 'MMI', 'intensity.jpg', 'dwb', '2003-08-01 18:00:00'),
('INTEN_PPS', 'Instrumental Intensity PS poster', NULL, 'MMI', 'inten_pstr.ps.zip', 'dwb', '2003-08-01 18:00:00'),
('INTEN_PS', 'Instrumental Intensity PS', NULL, 'MMI', 'intensity.ps.zip', 'dwb', '2003-08-01 18:00:00'),
('PGA_JPG', 'PGA JPEG', NULL, 'PGA', 'pga.jpg', NULL, NULL),
('PGA_PPS', 'PGA poster PS', NULL, 'PGA', 'pga_poster.ps.zip', 'dwb', '2003-08-01 18:00:00'),
('PGA_PS', 'PGA PS', NULL, 'PGA', 'pga.ps.zip', NULL, NULL),
('PGV_JPG', 'PGV JPEG', NULL, 'PGV', 'pgv.jpg', NULL, NULL),
('PGV_PPS', 'PGV poster PS', NULL, 'PGV', 'pga_poster.ps.zip', 'dwb', '2003-08-01 18:00:00'),
('PGV_PS', 'PGV PS', NULL, 'PGV', 'pgv.ps.zip', NULL, NULL),
('PSA03_JPG', 'PSA 0.3 sec JPEG', NULL, 'PSA03', 'psa03.jpg', NULL, NULL),
('PSA03_PS', 'PSA 0.3 sec PS', NULL, 'PSA03', 'psa03.ps.zip', NULL, NULL),
('PSA10_JPG', 'PSA 1.0 sec JPEG', NULL, 'PSA10', 'psa10.jpg', NULL, NULL),
('PSA10_PS', 'PSA 1.0 sec PS', NULL, 'PSA10', 'psa10.ps.zip', NULL, NULL),
('PSA30_JPG', 'PSA 3.0 sec JPEG', NULL, 'PSA30', 'psa30.jpg', NULL, NULL),
('PSA30_PS', 'PSA 3.0 sec PS', NULL, 'PSA30', 'psa30.ps.zip', NULL, NULL),
('SHAPE', 'Shape files', NULL, NULL, 'shape.zip', NULL, NULL),
('STN_TXT', 'Stationlist Text', NULL, NULL, 'stationlist.txt', NULL, NULL),
('STN_XML', 'Stationlist XML', NULL, NULL, 'stationlist.xml', NULL, NULL),
('TV_JPG', 'Media intensity JPEG', NULL, 'MMI', 'tvmap.jpg', 'dwb', '2003-07-24 10:46:00'),
('TV_PS', 'Media intensity PS', NULL, 'MMI', 'tvmap.ps.zip', 'dwb', '2003-07-24 10:46:00'),
('TV_TXT', 'Media event summary', NULL, NULL, 'tvguide.txt', 'dwb', '2003-07-24 10:46:00'),
('TVBARE_JPG', 'Media intensity (bare) JPEG', NULL, 'MMI', 'tvmap_bare.jpg', 'dwb', '2003-07-25 12:49:00'),
('TVBARE_PS', 'Media intensity (bare) PS', NULL, 'MMI', 'tvmap_bare.ps.zip', 'dwb', '2003-07-25 12:49:00'),
('GRID_XML', 'GRID_XML', NULL, NULL, 'grid.xml', 'kwl', '2006-11-30 15:32:10'),
('KML', 'KML', NULL, NULL, '%EVENT_ID%.kml', 'kwl', '2006-12-01 15:28:29'),
('IIOVER_PNG', 'IIOVER_PNG', NULL, NULL, 'ii_overlay.png', 'kwl', '2006-12-01 15:28:29'),
('IIOVER_JPG', 'IIOVER_JPG', NULL, NULL, 'ii_overlay.jpg', 'kwl', '2007-10-12 13:47:41');

-- 
-- Dumping data for table `server`
-- 

INSERT INTO `server` (`SERVER_ID`, `DNS_ADDRESS`, `IP_ADDRESS`, `OWNER_ORGANIZATION`, `LAST_HEARD_FROM`, `ERROR_COUNT`, `SYSTEM_GENERATION`, `SOFTWARE_VERSION`, `BIRTH_TIMESTAMP`, `DEATH_TIMESTAMP`, `PKI_KEY`, `SERVER_STATUS`, `LAT`, `LON`, `LAST_EVENT_TIMESTAMP`, `NUMBER_OF_USERS`, `ACCESS_COUNT`, `EVENT_COUNT`, `PRODUCT_COUNT`, `UPDATE_USERNAME`, `UPDATE_TIMESTAMP`, `PASSWORD`, `UPSTREAM_FLAG`, `DOWNSTREAM_FLAG`, `POLL_FLAG`, `QUERY_FLAG`, `SELF_FLAG`, `EVENT_HWM`, `SHAKEMAP_HWM`, `PRODUCT_HWM`) VALUES 
(1, 'localhost', NULL, NULL, '2007-06-25 15:46:20', 0, 0, NULL, NULL, NULL, NULL, 'ALIVE', NULL, NULL, NULL, 0, 0, 0, 0, 'scadmin', '2006-12-16 10:25:22', NULL, 1, NULL, NULL, NULL, NULL, 0, 0, 0),
(1000, 'localhost', NULL, NULL, '2006-12-08 16:21:58', 0, 0, NULL, NULL, NULL, NULL, 'ALIVE', NULL, NULL, NULL, 0, 0, 0, 0, 'scadmin', '2006-12-16 10:25:22', NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 0),
(1302, 'earthquake.usgs.gov', NULL, NULL, '2007-06-25 22:44:31', 0, 0, NULL, NULL, NULL, NULL, 'ALIVE', NULL, NULL, NULL, 0, 0, 0, 0, 'scadmin', '2006-11-27 10:24:08', NULL, 1, 0, 0, 1, 0, 0, 0, 0);

-- 
-- Dumping data for table `server_status`
-- 

INSERT INTO `server_status` (`SERVER_STATUS`, `NAME`, `DESCRIPTION`, `UPDATE_USERNAME`, `UPDATE_TIMESTAMP`) VALUES 
('ALIVE', 'Alive', 'Server is alive and functioning', 'pan', '2003-01-01 00:00:00'),
('UNKNOWN', 'Unknown', 'Server status is unknown', 'pan', '2003-01-01 00:00:00'),
('REMOVED', 'Removed', 'Server is known to no longer exist', 'pan', '2003-01-01 00:00:00');

-- 
-- Dumping data for table `shakecast_user`
-- 

INSERT INTO `shakecast_user` (`SHAKECAST_USER`, `USER_TYPE`, `USERNAME`, `PASSWORD`, `UPDATE_USERNAME`, `UPDATE_TIMESTAMP`) VALUES 
(1, 'ADMIN', 'scadmin', 'scadmin', 'cmdadmin', now());

-- 
-- Dumping data for table `shakemap`
-- 

INSERT INTO `shakemap` (`SHAKEMAP_ID`, `SHAKEMAP_VERSION`, `SHAKEMAP_STATUS`, `EVENT_ID`, `EVENT_VERSION`, `GENERATING_SERVER`, `SHAKEMAP_REGION`, `GENERATION_TIMESTAMP`, `RECEIVE_TIMESTAMP`, `LAT_MIN`, `LAT_MAX`, `LON_MIN`, `LON_MAX`, `BEGIN_TIMESTAMP`, `END_TIMESTAMP`, `SEQ`, `SUPERCEDED_TIMESTAMP`) VALUES 
('Northridge_scte', 1, 'REVIEWED', 'Northridge_scte', 1, 1, 'ci', '2004-06-29 17:46:58', '2007-06-26 15:43:17', 33.3797, 35.0463, -119.786, -117.286, '2004-06-29 17:46:58', '2004-06-29 17:46:58', 1, NULL);

-- 
-- Dumping data for table `shakemap_metric`
-- 

INSERT INTO `shakemap_metric` (`SHAKEMAP_ID`, `SHAKEMAP_VERSION`, `METRIC`, `VALUE_COLUMN_NUMBER`, `MAX_VALUE`, `MIN_VALUE`) VALUES 
('Northridge_scte', 1, 'MMI', 3, 10, 4.59),
('Northridge_scte', 1, 'PGA', 1, 91.8607, 4.4018),
('Northridge_scte', 1, 'PGV', 2, 183.6959, 2.1756),
('Northridge_scte', 1, 'PSA03', 4, 265.9653, 7.7936),
('Northridge_scte', 1, 'PSA10', 5, 198.7476, 2.1827),
('Northridge_scte', 1, 'PSA30', 6, 36.4198, 0.4298);

-- 
-- Dumping data for table `shakemap_status`
-- 

INSERT INTO `shakemap_status` (`SHAKEMAP_STATUS`, `NAME`, `DESCRIPTION`, `UPDATE_USERNAME`, `UPDATE_TIMESTAMP`) VALUES 
('NORMAL', 'Normal', 'Typical', 'pan', '2003-01-01 00:00:00'),
('CANCELLED', 'Cancelled', 'The event was cancelled or retracted', 'pan', '2003-01-01 00:00:00'),
('INCOMPLETE', 'Incomplete', 'The event has been created by the event data is incomplete', 'pan', '2003-01-01 00:00:00'),
('RELEASED', 'Released', 'Released for use', 'dwb', '2003-09-17 00:00:00'),
('REVIEWED', 'Reviewed', 'Released for use and reviewed by a human', 'dwb', '2003-09-17 00:00:00');

-- 
-- Dumping data for table `user_type`
-- 

INSERT INTO `user_type` (`USER_TYPE`, `NAME`, `DESCRIPTION`, `UPDATE_USERNAME`, `UPDATE_TIMESTAMP`) VALUES 
('ADMIN', 'Administrator', 'ShakeCast System Administrators', 'pan', '2003-01-01 00:00:00'),
('USER', 'User', 'ShakeCast User', 'pan', '2003-01-01 00:00:00'),
('SYSTEM', 'System', 'ShakeCast System software', 'pan', '2003-01-01 00:00:00');

-- 
-- Dumping data for table `yes_no`
-- 

INSERT INTO `yes_no` (`ID`, `NAME`) VALUES 
(0, 'NO'),
(1, 'YES');

-- 
-- Dumping data for table `facility_type_fragility`
-- 
INSERT INTO `facility_type_fragility` (`FACILITY_TYPE_FRAGILITY_ID`, `FACILITY_TYPE`, `DAMAGE_LEVEL`, `LOW_LIMIT`, `HIGH_LIMIT`, `UPDATE_USERNAME`, `UPDATE_TIMESTAMP`, `METRIC`) VALUES 
(1, 'W1H', 'GREEN', 0, 63, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(2, 'W1H', 'YELLOW', 63, 147, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(3, 'W1H', 'ORANGE', 147, 231, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(4, 'W1H', 'RED', 231, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(5, 'W2H', 'GREEN', 0, 64, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(6, 'W2H', 'YELLOW', 64, 132, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(7, 'W2H', 'ORANGE', 132, 239, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(8, 'W2H', 'RED', 239, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(9, 'S1LH', 'GREEN', 0, 36, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(10, 'S1LH', 'YELLOW', 36, 74, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(11, 'S1LH', 'ORANGE', 74, 171, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(12, 'S1LH', 'RED', 171, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(13, 'S1MH', 'GREEN', 0, 30, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(14, 'S1MH', 'YELLOW', 30, 71, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(15, 'S1MH', 'ORANGE', 71, 164, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(16, 'S1MH', 'RED', 164, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(17, 'S1HH', 'GREEN', 0, 24, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(18, 'S1HH', 'YELLOW', 24, 60, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(19, 'S1HH', 'ORANGE', 60, 151, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(20, 'S1HH', 'RED', 151, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(21, 'S2LH', 'GREEN', 0, 47, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(22, 'S2LH', 'YELLOW', 47, 87, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(23, 'S2LH', 'ORANGE', 87, 168, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(24, 'S2LH', 'RED', 168, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(25, 'S2MH', 'GREEN', 0, 31, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(26, 'S2MH', 'YELLOW', 31, 84, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(27, 'S2MH', 'ORANGE', 84, 186, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(28, 'S2MH', 'RED', 186, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(29, 'S2HH', 'GREEN', 0, 25, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(30, 'S2HH', 'YELLOW', 25, 75, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(31, 'S2HH', 'ORANGE', 75, 184, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(32, 'S2HH', 'RED', 184, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(33, 'S3H', 'GREEN', 0, 30, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(34, 'S3H', 'YELLOW', 30, 62, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(35, 'S3H', 'ORANGE', 62, 115, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(36, 'S3H', 'RED', 115, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(37, 'S4LH', 'GREEN', 0, 45, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(38, 'S4LH', 'YELLOW', 45, 82, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(39, 'S4LH', 'ORANGE', 82, 153, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(40, 'S4LH', 'RED', 153, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(41, 'S4MH', 'GREEN', 0, 32, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(42, 'S4MH', 'YELLOW', 32, 84, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(43, 'S4MH', 'ORANGE', 84, 179, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(44, 'S4MH', 'RED', 179, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(45, 'S4HH', 'GREEN', 0, 29, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(46, 'S4HH', 'YELLOW', 29, 79, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(47, 'S4HH', 'ORANGE', 79, 187, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(48, 'S4HH', 'RED', 187, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(61, 'C1LH', 'GREEN', 0, 40, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(62, 'C1LH', 'YELLOW', 40, 80, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(63, 'C1LH', 'ORANGE', 80, 158, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(64, 'C1LH', 'RED', 158, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(65, 'C1MH', 'GREEN', 0, 31, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(66, 'C1MH', 'YELLOW', 31, 84, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(67, 'C1MH', 'ORANGE', 84, 185, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(68, 'C1MH', 'RED', 185, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(69, 'C1HH', 'GREEN', 0, 25, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(70, 'C1HH', 'YELLOW', 25, 71, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(71, 'C1HH', 'ORANGE', 71, 155, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(72, 'C1HH', 'RED', 155, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(73, 'C2LH', 'GREEN', 0, 52, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(74, 'C2LH', 'YELLOW', 52, 103, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(75, 'C2LH', 'ORANGE', 103, 178, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(76, 'C2LH', 'RED', 178, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(77, 'C2MH', 'GREEN', 0, 41, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(78, 'C2MH', 'YELLOW', 41, 100, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(79, 'C2MH', 'ORANGE', 100, 224, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(80, 'C2MH', 'RED', 224, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(81, 'C2HH', 'GREEN', 0, 33, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(82, 'C2HH', 'YELLOW', 33, 94, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(83, 'C2HH', 'ORANGE', 94, 215, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(84, 'C2HH', 'RED', 215, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(97, 'PC1H', 'GREEN', 0, 40, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(98, 'PC1H', 'YELLOW', 40, 83, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(99, 'PC1H', 'ORANGE', 83, 144, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(100, 'PC1H', 'RED', 144, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(101, 'PC2LH', 'GREEN', 0, 41, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(102, 'PC2LH', 'YELLOW', 41, 79, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(103, 'PC2LH', 'ORANGE', 79, 141, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(104, 'PC2LH', 'RED', 141, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(105, 'PC2MH', 'GREEN', 0, 33, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(106, 'PC2MH', 'YELLOW', 33, 77, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(107, 'PC2MH', 'ORANGE', 77, 174, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(108, 'PC2MH', 'RED', 174, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(109, 'PC2HH', 'GREEN', 0, 26, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(110, 'PC2HH', 'YELLOW', 26, 72, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(111, 'PC2HH', 'ORANGE', 72, 171, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(112, 'PC2HH', 'RED', 171, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(113, 'RM1LH', 'GREEN', 0, 53, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(114, 'RM1LH', 'YELLOW', 53, 107, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(115, 'RM1LH', 'ORANGE', 107, 181, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(116, 'RM1LH', 'RED', 181, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(117, 'RM1MH', 'GREEN', 0, 43, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(118, 'RM1MH', 'YELLOW', 43, 93, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(119, 'RM1MH', 'ORANGE', 93, 218, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(120, 'RM1MH', 'RED', 218, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(121, 'RM2LH', 'GREEN', 0, 48, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(122, 'RM2LH', 'YELLOW', 48, 100, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(123, 'RM2LH', 'ORANGE', 100, 171, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(124, 'RM2LH', 'RED', 171, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(125, 'RM2MH', 'GREEN', 0, 38, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(126, 'RM2MH', 'YELLOW', 38, 86, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(127, 'RM2MH', 'ORANGE', 86, 210, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(128, 'RM2MH', 'RED', 210, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(129, 'RM2HH', 'GREEN', 0, 28, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(130, 'RM2HH', 'YELLOW', 28, 77, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(131, 'RM2HH', 'ORANGE', 77, 205, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(132, 'RM2HH', 'RED', 205, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(141, 'MHH', 'GREEN', 0, 21, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(142, 'MHH', 'YELLOW', 21, 36, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(143, 'MHH', 'ORANGE', 36, 69, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(144, 'MHH', 'RED', 69, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(145, 'W1M', 'GREEN', 0, 49, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(146, 'W1M', 'YELLOW', 49, 105, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(147, 'W1M', 'ORANGE', 105, 154, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(148, 'W1M', 'RED', 154, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(149, 'W2M', 'GREEN', 0, 40, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(150, 'W2M', 'YELLOW', 40, 74, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(151, 'W2M', 'ORANGE', 74, 130, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(152, 'W2M', 'RED', 130, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(153, 'S1LM', 'GREEN', 0, 25, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(154, 'S1LM', 'YELLOW', 25, 48, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(155, 'S1LM', 'ORANGE', 48, 92, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(156, 'S1LM', 'RED', 92, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(157, 'S1MM', 'GREEN', 0, 24, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(158, 'S1MM', 'YELLOW', 24, 51, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(159, 'S1MM', 'ORANGE', 51, 94, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(160, 'S1MM', 'RED', 94, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(161, 'S1HM', 'GREEN', 0, 21, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(162, 'S1HM', 'YELLOW', 21, 45, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(163, 'S1HM', 'ORANGE', 45, 90, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(164, 'S1HM', 'RED', 90, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(165, 'S2LM', 'GREEN', 0, 30, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(166, 'S2LM', 'YELLOW', 30, 53, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(167, 'S2LM', 'ORANGE', 53, 97, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(168, 'S2LM', 'RED', 97, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(169, 'S2MM', 'GREEN', 0, 25, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(170, 'S2MM', 'YELLOW', 25, 61, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(171, 'S2MM', 'ORANGE', 61, 112, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(172, 'S2MM', 'RED', 112, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(173, 'S2HM', 'GREEN', 0, 22, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(174, 'S2HM', 'YELLOW', 22, 56, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(175, 'S2HM', 'ORANGE', 56, 117, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(176, 'S2HM', 'RED', 117, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(177, 'S3M', 'GREEN', 0, 22, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(178, 'S3M', 'YELLOW', 22, 38, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(179, 'S3M', 'ORANGE', 38, 69, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(180, 'S3M', 'RED', 69, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(181, 'S4LM', 'GREEN', 0, 30, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(182, 'S4LM', 'YELLOW', 30, 47, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(183, 'S4LM', 'ORANGE', 47, 90, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(184, 'S4LM', 'RED', 90, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(185, 'S4MM', 'GREEN', 0, 25, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(186, 'S4MM', 'YELLOW', 25, 59, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(187, 'S4MM', 'ORANGE', 59, 106, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(188, 'S4MM', 'RED', 106, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(189, 'S4HM', 'GREEN', 0, 24, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(190, 'S4HM', 'YELLOW', 24, 59, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(191, 'S4HM', 'ORANGE', 59, 112, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(192, 'S4HM', 'RED', 112, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(205, 'C1LM', 'GREEN', 0, 26, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(206, 'C1LM', 'YELLOW', 26, 47, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(207, 'C1LM', 'ORANGE', 47, 89, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(208, 'C1LM', 'RED', 89, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(209, 'C1MM', 'GREEN', 0, 24, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(210, 'C1MM', 'YELLOW', 24, 56, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(211, 'C1MM', 'ORANGE', 56, 102, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(212, 'C1MM', 'RED', 102, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(213, 'C1HM', 'GREEN', 0, 21, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(214, 'C1HM', 'YELLOW', 21, 47, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(215, 'C1HM', 'ORANGE', 47, 85, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(216, 'C1HM', 'RED', 85, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(217, 'C2LM', 'GREEN', 0, 34, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(218, 'C2LM', 'YELLOW', 34, 56, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(219, 'C2LM', 'ORANGE', 56, 100, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(220, 'C2LM', 'RED', 100, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(221, 'C2MM', 'GREEN', 0, 30, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(222, 'C2MM', 'YELLOW', 30, 63, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(223, 'C2MM', 'ORANGE', 63, 117, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(224, 'C2MM', 'RED', 117, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(225, 'C2HM', 'GREEN', 0, 26, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(226, 'C2HM', 'YELLOW', 26, 66, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(227, 'C2HM', 'ORANGE', 66, 123, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(228, 'C2HM', 'RED', 123, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(241, 'PC1M', 'GREEN', 0, 28, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(242, 'PC1M', 'YELLOW', 28, 51, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(243, 'PC1M', 'ORANGE', 51, 82, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(244, 'PC1M', 'RED', 82, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(245, 'PC2LM', 'GREEN', 0, 29, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(246, 'PC2LM', 'YELLOW', 29, 46, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(247, 'PC2LM', 'ORANGE', 46, 85, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(248, 'PC2LM', 'RED', 85, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(249, 'PC2MM', 'GREEN', 0, 24, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(250, 'PC2MM', 'YELLOW', 24, 52, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(251, 'PC2MM', 'ORANGE', 52, 99, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(252, 'PC2MM', 'RED', 99, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(253, 'PC2HM', 'GREEN', 0, 22, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(254, 'PC2HM', 'YELLOW', 22, 53, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(255, 'PC2HM', 'ORANGE', 53, 103, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(256, 'PC2HM', 'RED', 103, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(257, 'RM1LM', 'GREEN', 0, 34, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(258, 'RM1LM', 'YELLOW', 34, 57, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(259, 'RM1LM', 'ORANGE', 57, 98, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(260, 'RM1LM', 'RED', 98, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(261, 'RM1MM', 'GREEN', 0, 30, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(262, 'RM1MM', 'YELLOW', 30, 59, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(263, 'RM1MM', 'ORANGE', 59, 118, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(264, 'RM1MM', 'RED', 118, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(265, 'RM2LM', 'GREEN', 0, 32, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(266, 'RM2LM', 'YELLOW', 32, 54, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(267, 'RM2LM', 'ORANGE', 54, 93, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(268, 'RM2LM', 'RED', 93, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(269, 'RM2MM', 'GREEN', 0, 26, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(270, 'RM2MM', 'YELLOW', 26, 55, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(271, 'RM2MM', 'ORANGE', 55, 114, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(272, 'RM2MM', 'RED', 114, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(273, 'RM2HM', 'GREEN', 0, 23, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(274, 'RM2HM', 'YELLOW', 23, 55, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(275, 'RM2HM', 'ORANGE', 55, 116, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(276, 'RM2HM', 'RED', 116, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(285, 'MHM', 'GREEN', 0, 21, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(286, 'MHM', 'YELLOW', 21, 36, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(287, 'MHM', 'ORANGE', 36, 69, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(288, 'MHM', 'RED', 69, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(289, 'W1L', 'GREEN', 0, 39, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(290, 'W1L', 'YELLOW', 39, 70, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(291, 'W1L', 'ORANGE', 70, 109, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(292, 'W1L', 'RED', 109, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(293, 'W2L', 'GREEN', 0, 26, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(294, 'W2L', 'YELLOW', 26, 55, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(295, 'W2L', 'ORANGE', 55, 86, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(296, 'W2L', 'RED', 86, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(297, 'S1LL', 'GREEN', 0, 20, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(298, 'S1LL', 'YELLOW', 20, 34, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(299, 'S1LL', 'ORANGE', 34, 55, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(300, 'S1LL', 'RED', 55, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(301, 'S1ML', 'GREEN', 0, 21, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(302, 'S1ML', 'YELLOW', 21, 33, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(303, 'S1ML', 'ORANGE', 33, 56, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(304, 'S1ML', 'RED', 56, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(305, 'S1HL', 'GREEN', 0, 17, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(306, 'S1HL', 'YELLOW', 17, 32, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(307, 'S1HL', 'ORANGE', 32, 55, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(308, 'S1HL', 'RED', 55, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(309, 'S2LL', 'GREEN', 0, 20, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(310, 'S2LL', 'YELLOW', 20, 34, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(311, 'S2LL', 'ORANGE', 34, 57, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(312, 'S2LL', 'RED', 57, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(313, 'S2ML', 'GREEN', 0, 21, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(314, 'S2ML', 'YELLOW', 21, 40, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(315, 'S2ML', 'ORANGE', 40, 67, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(316, 'S2ML', 'RED', 67, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(317, 'S2HL', 'GREEN', 0, 20, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(318, 'S2HL', 'YELLOW', 20, 41, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(319, 'S2HL', 'ORANGE', 41, 72, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(320, 'S2HL', 'RED', 72, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(321, 'S3L', 'GREEN', 0, 15, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(322, 'S3L', 'YELLOW', 15, 23, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(323, 'S3L', 'ORANGE', 23, 44, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(324, 'S3L', 'RED', 44, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(325, 'S4LL', 'GREEN', 0, 18, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(326, 'S4LL', 'YELLOW', 18, 30, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(327, 'S4LL', 'ORANGE', 30, 53, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(328, 'S4LL', 'RED', 53, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(329, 'S4ML', 'GREEN', 0, 20, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(330, 'S4ML', 'YELLOW', 20, 36, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(331, 'S4ML', 'ORANGE', 36, 62, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(332, 'S4ML', 'RED', 62, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(333, 'S4HL', 'GREEN', 0, 20, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(334, 'S4HL', 'YELLOW', 20, 38, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(335, 'S4HL', 'ORANGE', 38, 68, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(336, 'S4HL', 'RED', 68, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(337, 'S5LL', 'GREEN', 0, 20, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(338, 'S5LL', 'YELLOW', 20, 32, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(339, 'S5LL', 'ORANGE', 32, 52, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(340, 'S5LL', 'RED', 52, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(341, 'S5ML', 'GREEN', 0, 21, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(342, 'S5ML', 'YELLOW', 21, 39, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(343, 'S5ML', 'ORANGE', 39, 61, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(344, 'S5ML', 'RED', 61, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(345, 'S5HL', 'GREEN', 0, 21, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(346, 'S5HL', 'YELLOW', 21, 40, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(347, 'S5HL', 'ORANGE', 40, 67, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(348, 'S5HL', 'RED', 67, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(349, 'C1LL', 'GREEN', 0, 17, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(350, 'C1LL', 'YELLOW', 17, 31, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(351, 'C1LL', 'ORANGE', 31, 52, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(352, 'C1LL', 'RED', 52, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(353, 'C1ML', 'GREEN', 0, 20, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(354, 'C1ML', 'YELLOW', 20, 37, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(355, 'C1ML', 'ORANGE', 37, 62, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(356, 'C1ML', 'RED', 62, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(357, 'C1HL', 'GREEN', 0, 17, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(358, 'C1HL', 'YELLOW', 17, 31, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(359, 'C1HL', 'ORANGE', 31, 51, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(360, 'C1HL', 'RED', 51, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(361, 'C2LL', 'GREEN', 0, 22, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(362, 'C2LL', 'YELLOW', 22, 34, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(363, 'C2LL', 'ORANGE', 34, 60, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(364, 'C2LL', 'RED', 60, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(365, 'C2ML', 'GREEN', 0, 22, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(366, 'C2ML', 'YELLOW', 22, 44, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(367, 'C2ML', 'ORANGE', 44, 72, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(368, 'C2ML', 'RED', 72, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(369, 'C2HL', 'GREEN', 0, 22, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(370, 'C2HL', 'YELLOW', 22, 44, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(371, 'C2HL', 'ORANGE', 44, 75, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(372, 'C2HL', 'RED', 75, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(373, 'C3LL', 'GREEN', 0, 20, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(374, 'C3LL', 'YELLOW', 20, 30, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(375, 'C3LL', 'ORANGE', 30, 51, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(376, 'C3LL', 'RED', 51, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(377, 'C3ML', 'GREEN', 0, 20, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(378, 'C3ML', 'YELLOW', 20, 37, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(379, 'C3ML', 'ORANGE', 37, 59, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(380, 'C3ML', 'RED', 59, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(381, 'C3HL', 'GREEN', 0, 18, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(382, 'C3HL', 'YELLOW', 18, 38, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(383, 'C3HL', 'ORANGE', 38, 61, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(384, 'C3HL', 'RED', 61, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(385, 'PC1L', 'GREEN', 0, 20, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(386, 'PC1L', 'YELLOW', 20, 29, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(387, 'PC1L', 'ORANGE', 29, 52, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(388, 'PC1L', 'RED', 52, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(389, 'PC2LL', 'GREEN', 0, 17, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(390, 'PC2LL', 'YELLOW', 17, 28, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(391, 'PC2LL', 'ORANGE', 28, 51, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(392, 'PC2LL', 'RED', 51, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(393, 'PC2ML', 'GREEN', 0, 18, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(394, 'PC2ML', 'YELLOW', 18, 36, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(395, 'PC2ML', 'ORANGE', 36, 60, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(396, 'PC2ML', 'RED', 60, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(397, 'PC2HL', 'GREEN', 0, 18, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(398, 'PC2HL', 'YELLOW', 18, 36, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(399, 'PC2HL', 'ORANGE', 36, 63, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(400, 'PC2HL', 'RED', 63, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(401, 'RM1LL', 'GREEN', 0, 23, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(402, 'RM1LL', 'YELLOW', 23, 33, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(403, 'RM1LL', 'ORANGE', 33, 62, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(404, 'RM1LL', 'RED', 62, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(405, 'RM1ML', 'GREEN', 0, 22, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(406, 'RM1ML', 'YELLOW', 22, 40, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(407, 'RM1ML', 'ORANGE', 40, 72, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(408, 'RM1ML', 'RED', 72, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(409, 'RM2LL', 'GREEN', 0, 21, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(410, 'RM2LL', 'YELLOW', 21, 32, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(411, 'RM2LL', 'ORANGE', 32, 59, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(412, 'RM2LL', 'RED', 59, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(413, 'RM2ML', 'GREEN', 0, 20, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(414, 'RM2ML', 'YELLOW', 20, 39, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(415, 'RM2ML', 'ORANGE', 39, 69, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(416, 'RM2ML', 'RED', 69, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(417, 'RM2HL', 'GREEN', 0, 20, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(418, 'RM2HL', 'YELLOW', 20, 40, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(419, 'RM2HL', 'ORANGE', 40, 71, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(420, 'RM2HL', 'RED', 71, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(421, 'URMLL', 'GREEN', 0, 23, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(422, 'URMLL', 'YELLOW', 23, 37, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(423, 'URMLL', 'ORANGE', 37, 53, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(424, 'URMLL', 'RED', 53, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(425, 'URMML', 'GREEN', 0, 18, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(426, 'URMML', 'YELLOW', 18, 31, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(427, 'URMML', 'ORANGE', 31, 53, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(428, 'URMML', 'RED', 53, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(429, 'MHL', 'GREEN', 0, 21, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(430, 'MHL', 'YELLOW', 21, 36, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(431, 'MHL', 'ORANGE', 36, 69, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(432, 'MHL', 'RED', 69, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(433, 'W1P', 'GREEN', 0, 33, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(434, 'W1P', 'YELLOW', 33, 59, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(435, 'W1P', 'ORANGE', 59, 89, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(436, 'W1P', 'RED', 89, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(437, 'W2P', 'GREEN', 0, 22, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(438, 'W2P', 'YELLOW', 22, 43, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(439, 'W2P', 'ORANGE', 43, 69, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(440, 'W2P', 'RED', 69, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(441, 'S1LP', 'GREEN', 0, 15, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(442, 'S1LP', 'YELLOW', 15, 25, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(443, 'S1LP', 'ORANGE', 25, 44, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(444, 'S1LP', 'RED', 44, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(445, 'S1MP', 'GREEN', 0, 16, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(446, 'S1MP', 'YELLOW', 16, 26, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(447, 'S1MP', 'ORANGE', 26, 45, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(448, 'S1MP', 'RED', 45, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(449, 'S1HP', 'GREEN', 0, 14, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(450, 'S1HP', 'YELLOW', 14, 25, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(451, 'S1HP', 'ORANGE', 25, 44, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(452, 'S1HP', 'RED', 44, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(453, 'S2LP', 'GREEN', 0, 16, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(454, 'S2LP', 'YELLOW', 16, 26, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(455, 'S2LP', 'ORANGE', 26, 45, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(456, 'S2LP', 'RED', 45, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(457, 'S2MP', 'GREEN', 0, 16, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(458, 'S2MP', 'YELLOW', 16, 32, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(459, 'S2MP', 'ORANGE', 32, 54, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(460, 'S2MP', 'RED', 54, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(461, 'S2HP', 'GREEN', 0, 15, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(462, 'S2HP', 'YELLOW', 15, 33, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(463, 'S2HP', 'ORANGE', 33, 57, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(464, 'S2HP', 'RED', 57, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(465, 'S3P', 'GREEN', 0, 11, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(466, 'S3P', 'YELLOW', 11, 18, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(467, 'S3P', 'ORANGE', 18, 34, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(468, 'S3P', 'RED', 34, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(469, 'S4LP', 'GREEN', 0, 15, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(470, 'S4LP', 'YELLOW', 15, 23, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(471, 'S4LP', 'ORANGE', 23, 41, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(472, 'S4LP', 'RED', 41, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(473, 'S4MP', 'GREEN', 0, 15, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(474, 'S4MP', 'YELLOW', 15, 29, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(475, 'S4MP', 'ORANGE', 29, 49, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(476, 'S4MP', 'RED', 49, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(477, 'S4HP', 'GREEN', 0, 16, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(478, 'S4HP', 'YELLOW', 16, 31, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(479, 'S4HP', 'ORANGE', 31, 54, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(480, 'S4HP', 'RED', 54, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(481, 'S5LP', 'GREEN', 0, 16, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(482, 'S5LP', 'YELLOW', 16, 25, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(483, 'S5LP', 'ORANGE', 25, 43, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(484, 'S5LP', 'RED', 43, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(485, 'S5MP', 'GREEN', 0, 16, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(486, 'S5MP', 'YELLOW', 16, 32, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(487, 'S5MP', 'ORANGE', 32, 49, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(488, 'S5MP', 'RED', 49, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(489, 'S5HP', 'GREEN', 0, 16, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(490, 'S5HP', 'YELLOW', 16, 33, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(491, 'S5HP', 'ORANGE', 33, 53, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(492, 'S5HP', 'RED', 53, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(493, 'C1LP', 'GREEN', 0, 14, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(494, 'C1LP', 'YELLOW', 14, 24, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(495, 'C1LP', 'ORANGE', 24, 41, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(496, 'C1LP', 'RED', 41, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(497, 'C1MP', 'GREEN', 0, 15, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(498, 'C1MP', 'YELLOW', 15, 30, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(499, 'C1MP', 'ORANGE', 30, 49, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(500, 'C1MP', 'RED', 49, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(501, 'C1HP', 'GREEN', 0, 14, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(502, 'C1HP', 'YELLOW', 14, 24, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(503, 'C1HP', 'ORANGE', 24, 40, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(504, 'C1HP', 'RED', 40, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(505, 'C2LP', 'GREEN', 0, 17, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(506, 'C2LP', 'YELLOW', 17, 28, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(507, 'C2LP', 'ORANGE', 28, 48, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(508, 'C2LP', 'RED', 48, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(509, 'C2MP', 'GREEN', 0, 17, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(510, 'C2MP', 'YELLOW', 17, 34, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(511, 'C2MP', 'ORANGE', 34, 57, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(512, 'C2MP', 'RED', 57, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(513, 'C2HP', 'GREEN', 0, 17, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(514, 'C2HP', 'YELLOW', 17, 36, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(515, 'C2HP', 'ORANGE', 36, 60, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(516, 'C2HP', 'RED', 60, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(517, 'C3LP', 'GREEN', 0, 16, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(518, 'C3LP', 'YELLOW', 16, 24, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(519, 'C3LP', 'ORANGE', 24, 40, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(520, 'C3LP', 'RED', 40, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(521, 'C3MP', 'GREEN', 0, 16, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(522, 'C3MP', 'YELLOW', 16, 29, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(523, 'C3MP', 'ORANGE', 29, 47, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(524, 'C3MP', 'RED', 47, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(525, 'C3HP', 'GREEN', 0, 15, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(526, 'C3HP', 'YELLOW', 15, 31, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(527, 'C3HP', 'ORANGE', 31, 49, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(528, 'C3HP', 'RED', 49, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(529, 'PC1P', 'GREEN', 0, 16, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(530, 'PC1P', 'YELLOW', 16, 24, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(531, 'PC1P', 'ORANGE', 24, 40, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(532, 'PC1P', 'RED', 40, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(533, 'PC2LP', 'GREEN', 0, 15, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(534, 'PC2LP', 'YELLOW', 15, 22, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(535, 'PC2LP', 'ORANGE', 22, 40, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(536, 'PC2LP', 'RED', 40, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(537, 'PC2MP', 'GREEN', 0, 15, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(538, 'PC2MP', 'YELLOW', 15, 28, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(539, 'PC2MP', 'ORANGE', 28, 48, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(540, 'PC2MP', 'RED', 48, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(541, 'PC2HP', 'GREEN', 0, 15, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(542, 'PC2HP', 'YELLOW', 15, 29, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(543, 'PC2HP', 'ORANGE', 29, 49, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(544, 'PC2HP', 'RED', 49, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(545, 'RM1LP', 'GREEN', 0, 18, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(546, 'RM1LP', 'YELLOW', 18, 28, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(547, 'RM1LP', 'ORANGE', 28, 49, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(548, 'RM1LP', 'RED', 49, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(549, 'RM1MP', 'GREEN', 0, 17, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(550, 'RM1MP', 'YELLOW', 17, 32, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(551, 'RM1MP', 'ORANGE', 32, 57, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(552, 'RM1MP', 'RED', 57, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(553, 'RM2LP', 'GREEN', 0, 17, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(554, 'RM2LP', 'YELLOW', 17, 25, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(555, 'RM2LP', 'ORANGE', 25, 47, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(556, 'RM2LP', 'RED', 47, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(557, 'RM2MP', 'GREEN', 0, 16, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(558, 'RM2MP', 'YELLOW', 16, 30, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(559, 'RM2MP', 'ORANGE', 30, 54, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(560, 'RM2MP', 'RED', 54, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(561, 'RM2HP', 'GREEN', 0, 15, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(562, 'RM2HP', 'YELLOW', 15, 31, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(563, 'RM2HP', 'ORANGE', 31, 57, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(564, 'RM2HP', 'RED', 57, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(565, 'URMLP', 'GREEN', 0, 20, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(566, 'URMLP', 'YELLOW', 20, 30, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(567, 'URMLP', 'ORANGE', 30, 43, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(568, 'URMLP', 'RED', 43, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(569, 'URMMP', 'GREEN', 0, 15, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(570, 'URMMP', 'YELLOW', 15, 24, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(571, 'URMMP', 'ORANGE', 24, 44, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(572, 'URMMP', 'RED', 44, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(573, 'MHP', 'GREEN', 0, 13, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(574, 'MHP', 'YELLOW', 13, 21, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(575, 'MHP', 'ORANGE', 21, 39, 'kwl', '2007-09-25 13:48:36', 'PGA'),
(576, 'MHP', 'RED', 39, 99999, 'kwl', '2007-09-25 13:48:36', 'PGA');

-- 
-- Dumping data for table `log_message_type`
-- 

INSERT INTO `log_message_type` (`LOG_MESSAGE_TYPE`, `NAME`, `DESCRIPTION`, `UPDATE_USERNAME`, `UPDATE_TIMESTAMP`) VALUES 
('ERROR', 'Error Message', NULL, 'kwl', '2008-05-01 15:15:00'),
('WARN', 'Warning Message', NULL, 'kwl', '2008-05-01 15:15:00');

