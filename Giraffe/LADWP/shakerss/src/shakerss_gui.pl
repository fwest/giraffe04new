#!/usr/bin/perl

# $Id: shakerss_gui.pl 427 2008-08-14 16:36:38Z klin $

##############################################################################
##############################################################################
# 
# Terms and Conditions of Software Use
# ====================================
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Disclaimer of Earthquake Information
# ====================================
# 
# The data and maps provided through this system are preliminary data
# and are subject to revision. They are computer generated and may not
# have received human review or official approval. Inaccuracies in the
# data may be present because of instrument or computer
# malfunctions. Subsequent review may result in significant revisions to
# the data. All efforts have been made to provide accurate information,
# but reliance on, or interpretation of earthquake data from a single
# source is not advised. Data users are cautioned to consider carefully
# the provisional nature of the information before using it for
# decisions that concern personal or public safety or the conduct of
# business that involves substantial monetary or operational
# consequences.
# 
# Disclaimer of Software and its Capabilities
# ===========================================
# 
# This software is provided as an "as is" basis.  Attempts have been
# made to rid the program of software defects and bugs, however the
# U.S. Geological Survey (USGS) have no obligations to provide maintenance, 
# support, updates, enhancements or modifications. In no event shall USGS 
# be liable to any party for direct, indirect, special, incidental or 
# consequential damages, including lost profits, arising out of the use 
# of this software, its documentation, or data obtained though the use 
# of this software, even if USGS or have been advised of the
# possibility of such damage. By downloading, installing or using this
# program, the user acknowledges and understands the purpose and
# limitations of this software.
# 
# Contact Information
# ===================
# 
# Coordination of this effort is under the auspices of the USGS Advanced
# National Seismic System (ANSS) coordinated in Golden, Colorado, which
# functions as the clearing house for development, distribution,
# documentation, and support. For questions, comments, or reports of
# potential bugs regarding this software please contact wald@usgs.gov or
# klin@usgs.gov.  
#
#############################################################################
	

use strict;

use FindBin;
use lib $FindBin::Bin.'/lib';
use LoadConfig;
use Facility;
use LWP::Simple;
#require "SimpleGet.pl";
use File::Basename;
use File::Path;
use IO::File;
use Getopt::Long;
use Carp;
use Time::Local;
#use Time::HiRes;
use CGI qw/:cgi/;

my $os = $^O;
if ($os =~ /MSWin32/i) {
	require Windows;
	import Windows;
	BEGIN { Win32::SetChildShowWindow(0) if defined &Win32::SetChildShowWindow };

} elsif ($os =~ /darwin/i) {
	require MacOSX;
	import MacOSX;
} else {
	require Linux;
	import Linux;
}

############################################################################
# Prototypes for the logging routines
############################################################################
sub logmsg;
sub logver;
sub logerr;
sub logscr;
sub mydate;

#######################################################################
# Stuff to handle command line options and program documentation:
#######################################################################

#######################################################################
# User config 
#######################################################################
	
my $logfile;			# Where we dump all of our messages
my $log;			# Filehandle of the log file
my $rss_home = $FindBin::Bin;
my $cfile      = "shakerss.conf";
my $config_dirs = [ $rss_home ];
my @postcmds;				# list of commands from the 'postcmd' config statement
my $download_dir;
my $rss_url;
my @data_files;
my @events;
my @regions;
my $all_regions;
my $time_window = 0;
my $event_update = 0;
my $mag_cutoff = 0;
my $refresh_interval;
my $server_port = 8080;

#
# Parse the config file
#
configure($cfile);

#######################################################################
# End of command line option stuff
#######################################################################

#######################################################################
# Run the program
#######################################################################

&gui_init($server_port);
my $last_update;
my $daemon = start_webserver();
open_url($server_port);

while (1) {
      # Listen for a gui event
      &gui_listen;

      # If now is later than the user's refresh interval, then 
      # refresh the rss feed.
      if ((time - $last_update) > $refresh_interval) {
         $last_update = time; 
		 my $result = open_rss();
      }
	  
      # Pass on important messages to gui log window and tray icon 
	if (-e "$rss_home/shakerss.status") {
		open(RSS, "< $rss_home/shakerss.status");
		my @result = <RSS>;
		close(RSS);
		unlink "$rss_home/shakerss.status";
		gui_note(join '', @result);
	}
	

      my $connection = $daemon->accept;

      # no connection? move on.
      next unless defined $connection;

      # if we're this far, we've got a connection.
      # get the browser's request from our connection.
      my $request = $connection->get_request; next unless defined $request;

      # if this is an invalid URL (something funky with ..'s, or
      # other characters we're not really fond of), then we send
      # a cheapo message saying that we don't like them. this 
      # should stop stuff like directory traversals, etc.
      # note, we ->print and not ->send_error because HTTP::Daemon
      # doesn't create a valid HTML document, and that's dumb.
      if ($request->url->path !~ /^[\/A-Za-z0-9\-_\.]+$/ || $request->url->path =~ /\.\./) { 
         $connection->print("<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">\n" .
                            "<html><head><title>Forbidden</title></head><body>\n" .
                            "<h1>Forbidden</h1> The server understood the request, " .
                            "but is refusing to fulfill it. Please don't try again.\n" .
                            "<hr><address>ShakeCast ". 
                            "Server at 127.0.0.1 Port $server_port" .
                            "</address></body></html>");
         next; # return to looping.
      }

      # if there's a query string, remove the path information for
      # CGI.pm and then feed it (or the POST in $request->content).
      my $form_parameters; # this is the final holder of form variables.
      if ( $request->uri =~ /\?/ ) {
         $form_parameters = $request->uri;
         $form_parameters =~ s/[^\?]+\?(.*)/$1/;
      } else { $form_parameters = $request->content; }
      $CGI::Q = new CGI($form_parameters);
		if (param('add_facility')) {
			gui_note(param('add_facility'));
			process_facility($download_dir, param('add_facility'));
			gui_note('-'x80);
		} elsif (param('action')) {
			gui_note('Action ('.param('action').') for event '.param('event').' at '.localtime());
			open_rss(param('event'), param('action'));
			gui_note('-'x80);
		} elsif (param('download_sm')) {
			gui_note('Action (Download ShakeMap) from URL '.param('download_sm').' at '.localtime());
			open_rss(param('download_sm'));
			gui_note('-'x80);
		}
      # set the location of our requested filename. if this is a
      # directory listings ("/"), then rewrite to become "/index.html".
      my $requested_file = $request->url->path; $requested_file =~ s/^\///;
      my $filename = "$download_dir/$requested_file";
      if ($filename =~ /[\/\\:]$/) { $filename .= "index.html"; }

      # now, we start serving the files. if this is an image and
      # it exists, then we binmode it for Windows, and send it out.
      if ( ( $filename =~ /(jpg|gif|png)$/i ) and -e $filename ) { 
         open(IMG, $filename) or logmsg("Oof! ShakeCast could not open $filename.");

         # print out the http headers.
         my $type = "image/$1";
         $connection->send_basic_header();
         $connection->print("Content-type: $type\015\012");
         $connection->print("\015\012"); # no more headers.

         # and now the image.
         binmode $connection; binmode IMG;
         $connection->print($_) while <IMG>; close(IMG);
      }

      # if the filename exists, pass it over.
      elsif (-e $filename) {
         open(IMG, $filename) or logmsg("Oof! ShakeCast could not open $filename.");

         # print out the http headers.
         $connection->send_basic_header();
		 if ($filename =~ /xml$/i) {
			 $connection->print("Content-Type: text/xml\015\012");
		 } else {
			 $connection->print("Content-Type: text/html\015\012");
		 }
         $connection->print("\015\012"); # no more headers.

         # fill it in, and then send it out. fun, fun.
         # parse_template is located in AmphetaDesk::Templates.
         $connection->print($_) while <IMG>; close(IMG);
      }

      # no clue, so write out an "apache rulezzzzz" error page.
      # note, we ->print and not ->send_error because HTTP::Daemon
      # doesn't create a valid HTML document, nor could we get
      # it to listen to our customized error message. we also don't
      # ->send_basic_header(404, "Not Found") for a similar reason:
      # we want to customize our error message, and any $msg we
      # throw becomes part of the response code, which is bad.
      else { # yeah. i love apache. more than you. or my burning ears.
         $connection->send_basic_header(404, "Not Found");
         $connection->print("Content-type: text/html\015\012");
         $connection->print("\015\012"); # no more headers.
         $connection->print("<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">\n" .
                            "<html><head><title>Not Found</title></head><body>\n" .
                            "<h1>Not Found</h1> The requested URL $requested_file was not found." .
                            "<hr><address>ShakeCast " . 
                            "Server at 127.0.0.1 Port $server_port" .
                            "</address></body></html>\n");
      }

      # all done with this request.
      $connection->close;

}

sub start_webserver {

	use HTTP::Daemon;
	my $daemon; 
	# attempt to create the daemon on this port. we pass the
	# arguments as a hash so that we can determine if we should 
	# bind to localhost, or allow outside connections. happy, quasi?
	my %daemon_args = ( LocalPort => $server_port, ReuseAddr => 1, Timeout => .1 );
	$daemon_args{LocalAddr} = "127.0.0.1" ;
	$daemon = HTTP::Daemon->new( %daemon_args );

	# if we grabbed the socket, exit the loop, else make a note
	# and try the next one. if we did grab a port successfully,
	# set our template value so we know what port we're using.
	if ($daemon) { logmsg("Successfully set up port $server_port for the internal webserver."); }
	else { logmsg("There was a problem starting the internal webserver on port $server_port: $!."); }

   # if we couldn't grab any of the sockets, then we need to exit.
   unless ($daemon) { logerr("Unfortunately, ShakeCast couldn't grab " .
                            "port $server_port for the local webserver. This isn't " .
                            "going to fix itself.", 1); exit; }

   # turn on autoflush. as of
   # IO::Socket 1.18, this is 
   # the default, but we leave
   # it on, just in case.
   $daemon->autoflush;
   return $daemon;

}


############################################################################
# Logs a message to the logfile with time/date stamp
############################################################################
sub logmsg { 

  print $log "$$: @_ on ", mydate(), "\n";
  print "@_ on ", mydate(), "\n";
  gui_note("@_ on ".mydate());
  gui_note('-'x80); 
  return;
}

############################################################################
# Logs a message to the logfile without time/date stamp
############################################################################
sub logver { 

  print $log "$$: @_\n";
  return;
}

############################################################################
# Logs a message with time/date stamp to the logfile, then quits
############################################################################
sub logerr { 

  logmsg shift; 
  exit 1; 
}

############################################################################
# Logs a message with to the screen
############################################################################
sub logscr { 

  print STDOUT "$0 $$: @_ on ", mydate(), "\n";
  return;
}

sub mydate {

  my ($sec, $min, $hr, $day, $mon, $yr) = localtime();
  return sprintf('%02d/%02d/%4d %02d:%02d:%02d', 
		  $mon + 1, $day, $yr + 1900, $hr, $min, $sec);
}


########################################################################
# sub configure()
# Read the config file and set the appropriate values
########################################################################
sub configure {

  my $cfg  = new LoadConfig($cfile, $config_dirs, \&logscr)
                or logerr "Couldn't open config file $cfile";

  $cfg->parse( {'logfile'		=> \&logfile,
				'download_dir'	=> \&download_dir,
				'rss_url'		=> \&rss_url,
				'server_port'		=> \&server_port,
                'refresh_interval'		=> \&refresh_interval,
	       } );

  return 0;
}

#######################################################################
# Configuration subroutines
#######################################################################

sub logfile {

  $logfile = shift;
  #----------------------------------------------------------------------
  # Replace the <RSS_HOME> string with the install dir
  #----------------------------------------------------------------------
  $logfile =~ s/<RSS_HOME>/$rss_home/;
  #----------------------------------------------------------------------
  # logfile is guaranteed to be something, but can we open it?
  #----------------------------------------------------------------------
  if (!open(LOGOUT, ">> $logfile")) {
    logscr "can't open $logfile; trying /tmp/shakerss.log";
    $logfile = '/tmp/shakerss.log';
    if (!open(LOGOUT, ">> $logfile")) {
      logscr "can't open $logfile; will message to STDOUT";
      $logfile = 'STDOUT';
      $log = \*STDOUT;
      return "Couldn't open a logfile";
    }
    LOGOUT->autoflush(1);
    logscr "ok: logging to $logfile";
    $log = \*LOGOUT;
    return "Logging to $logfile";
  }
  LOGOUT->autoflush(1);
  $log = \*LOGOUT;
  return undef;
}

sub rss_url {
  
  my $url = shift;

  defined $url 
	or return "rss_url: bad value: $url";

  $rss_url = $url;
  return undef;
}

sub refresh_interval {
  
  my $window = shift;

  $refresh_interval = $window;
  return undef;
}

sub server_port {
  
  my $port = shift;

  $server_port = $port;
  return undef;
}

sub download_dir {
  
  my $download = shift;
    
  defined $download 
	or return "download: argument required";

  $download =~ s/<RSS_HOME>/$rss_home/;

  #validate directory
  if (not -e "$download") {
    mkpath("$download", 0, 0755) or logerr "Couldn't create download dir";
  }

  $download_dir = $download;
  return undef;
}

