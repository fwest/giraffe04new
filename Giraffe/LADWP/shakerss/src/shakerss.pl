#!/usr/bin/perl

# $Id: shakerss.pl 427 2008-08-14 16:36:38Z klin $

##############################################################################
##############################################################################
# 
# Terms and Conditions of Software Use
# ====================================
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Disclaimer of Earthquake Information
# ====================================
# 
# The data and maps provided through this system are preliminary data
# and are subject to revision. They are computer generated and may not
# have received human review or official approval. Inaccuracies in the
# data may be present because of instrument or computer
# malfunctions. Subsequent review may result in significant revisions to
# the data. All efforts have been made to provide accurate information,
# but reliance on, or interpretation of earthquake data from a single
# source is not advised. Data users are cautioned to consider carefully
# the provisional nature of the information before using it for
# decisions that concern personal or public safety or the conduct of
# business that involves substantial monetary or operational
# consequences.
# 
# Disclaimer of Software and its Capabilities
# ===========================================
# 
# This software is provided as an "as is" basis.  Attempts have been
# made to rid the program of software defects and bugs, however the
# U.S. Geological Survey (USGS) have no obligations to provide maintenance, 
# support, updates, enhancements or modifications. In no event shall USGS 
# be liable to any party for direct, indirect, special, incidental or 
# consequential damages, including lost profits, arising out of the use 
# of this software, its documentation, or data obtained though the use 
# of this software, even if USGS or have been advised of the
# possibility of such damage. By downloading, installing or using this
# program, the user acknowledges and understands the purpose and
# limitations of this software.
# 
# Contact Information
# ===================
# 
# Coordination of this effort is under the auspices of the USGS Advanced
# National Seismic System (ANSS) coordinated in Golden, Colorado, which
# functions as the clearing house for development, distribution,
# documentation, and support. For questions, comments, or reports of
# potential bugs regarding this software please contact wald@usgs.gov or
# klin@usgs.gov.  
#
#############################################################################


use strict;
use FindBin;
use lib $FindBin::Bin.'/lib';
use LoadConfig;
use LWP::UserAgent;
use HTTP::Request;
use File::Basename;
use File::Path;
use IO::File;
use Getopt::Long;
use Carp;
use Time::Local;
use RSS;

############################################################################
# Prototypes for the logging routines
############################################################################

sub logmsg;
sub logver;
sub logerr;
sub logscr;
sub mydate;

#######################################################################
# Stuff to handle command line options and program documentation:
#######################################################################

my $desc = "Check the RSS feed at http://earthquake.usgs.gov/eqcenter/shakemap/shakerss.php "
	 . "for new events and execuates command for retrieved data files. ";

my $flgs = [{ FLAG => 'event',
              ARG  => 'event_id',
              DESC => 'Specifies the id of the event to process'},
            { FLAG => 'config_check',
			  DESC => 'Check the configuration file, then quit.'},
            { FLAG => 'process',
			  DESC => 'Reprocess the event.'},
            { FLAG => 'add',
			  DESC => 'Add selected event into local archive.'},
            { FLAG => 'remove',
			  DESC => 'Remove selected event from local archive.'},
            { FLAG => 'download',
              ARG  => 'download_id',
			  DESC => 'Download selected event from URL to local archive.'},
            { FLAG => 'silent',
              DESC => 'Generate no standard output messages.'},
            { FLAG => 'help',
              DESC => 'Print program documentation and quit.'},
            { FLAG => 'verbose',
              DESC => 'Print detailed processing information.'}
           ];

my $options = setOptions($flgs) or die "Error in setOptions";

if (defined $options->{'help'}) {
  printDoc($desc);
  exit 0;
}

my $evid    = $options->{'event'} if defined $options->{'event'};
my $config_quit  = defined $options->{'config_check'} ? 1 : 0;
my $verbose = defined $options->{'verbose'} ? 1 : 0;
my $process    = defined $options->{'process'} ? 1 : 0;
my $silent    = defined $options->{'silent'} ? 1 : 0;
my $add_remove;
if (defined $options->{'add'}) {
	$add_remove = 1;
} elsif (defined $options->{'remove'}) {
	$add_remove = -1;
}
my $download_url    = $options->{'download'} if defined $options->{'download'};

logscr "Unknown argument(s): @ARGV" if (@ARGV);


#######################################################################
# User config 
#######################################################################
	
my $logfile;			# Where we dump all of our messages
my $log;				# Filehandle of the log file
my $rss_home = $FindBin::Bin;
my $cfile      = "shakerss.conf";
my $config_dirs = [ $rss_home ];
my @postcmds;			# list of commands from the 'postcmd' config statement
my $download_dir;
my $rss_url;
my $proxy;
my @data_files;
my @events;
my @regions;
my $all_regions;
my $time_window = 0;
my $event_update = 0;
my $exec_postcmd  = 0;
my $mag_cutoff = 0;

#
# Parse the config file
#
configure($cfile);
if ($config_quit) {
	logscr "Config succeeded, quitting.";
    exit 0;
}
@events = () unless (defined $options->{'event'});
my $ua = new LWP::UserAgent;
$ua->agent("USGS ShakeCast");
$ua->proxy("http", "$proxy") if ($proxy);

#######################################################################
# End of command line option stuff
#######################################################################

#######################################################################
# Run the program
#######################################################################

if ($process && $evid) {
	# Reprocess ShakeMap of the specified event, usually applies to 
	# changes of user facilities
	sc_xml($download_dir."/$evid", $evid);
} elsif ($add_remove && $evid) {
	# Add/remove ShakeMap from the archive 
	manip_archive($add_remove);
} elsif ($download_url) {
	# Import ShakeMap via URL into archive, this applies to 
	# all ShakeMaps (actual, sencario, and atlas) from all regions
	download_sm($download_url);
} else {
	# Check for new ShakeMaps from the USGS RSS feed
	main();
}
exit 0;

#######################################################################
# Subroutines
#######################################################################
#
# Add/remove ShakeMap from the archive 
#
sub manip_archive {

	my ($add_remove) = @_;
	my (@lines, @saved_items, @items, $added_item);

	#get archive rss
	if (-e "$download_dir/archive.xml") {
		open(FH, "<$download_dir/archive.xml");
		@lines = <FH>;
		close(FH);
		
		@items = rss_to_items(join '', @lines);
		
		foreach my $item (@items) {
			# skipped the event(s) if already exists in the archive
			# the default action for remove
			next if (item_grid($item) =~ /\/$evid\//);
			push @saved_items, $item
		}
	}
	
	# if add to archive, the item should exists in current rss
	if ($add_remove > 0) {
		open(FH, "<$download_dir/rss.xml");
		@lines = <FH>;
		close(FH);

		@items = rss_to_items(join '', @lines);

		foreach my $item (@items) {
			if (item_grid($item) =~ /\/$evid\//) {
				push @saved_items, "$item";
				last;
			}
		}
	}
	
	# put back the archive.xml
	my $update_time = localtime();
	open(SHAKE, ">$download_dir/archive.xml");
	print SHAKE <<ITEM;
<?xml version="1.0"?>
<rss version="2.0" xmlns:geo="http://www.w3.org/2003/01/geo/wgs84_pos#" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:eq="http://earthquake.usgs.gov/rss/1.0/" xmlns:georss="http://www.georss.org/georss/">
<channel><title>USGS Earthquake ShakeMaps</title><description>List of ShakeMaps for events in the last 30 days</description><link>http://earthquake.usgs.gov/</link><dc:publisher>U.S. Geological Survey</dc:publisher><pubDate>$update_time</pubDate>
ITEM

	if (@saved_items) {
		foreach my $item (@saved_items) {
			print SHAKE "<item>",$item,"</item>\n";
		}
	}
	
	print SHAKE "</channel></rss>\n";
	close(SHAKE);
}


#
# Add/remove ShakeMap from the archive 
#
sub main {

my %evt_list;
my $min_shake_time = time() - $time_window * 86400;
#set last time RSS read
	if( -e "$rss_home/shakerss.dat" )
	{
		open(SHAKE, "<$rss_home/shakerss.dat");
		while (my $line = <SHAKE>) {
			chomp($line);
			my ($evt, $pubDate) = split /:/, $line;
			next unless ($pubDate > $min_shake_time);
			$evt_list{$evt} = $pubDate;
		}
		close(SHAKE);
	}

#get current rss
	logmsg "Retrieving Shake RSS Data";

	#my $rss_data = get($rss_url);
	my $resp = $ua->get($rss_url);
	logmsg "Error: ", $resp->status_line unless $resp->is_success;
	my $rss_data = $resp->content;
	open(FH, ">$download_dir/rss.xml");
	print FH $rss_data;
	close(FH);
	logscr "\nReturn: ",substr($rss_data,0, 80),"...\n" if ($verbose);
	my @items = rss_to_items($rss_data);
	logmsg "\t$#items RSS items in feed";

#track new min_shake_time and number of new events
	my $num_new = 0;
	my @saved_items;

#execute command for each new item
	foreach my $item (@items) {
	  my $region_code = &region_code($item);
	  next unless ((grep /$region_code/i, @regions) || $all_regions);
	  if (defined $evid) {
	    if (item_grid($item) =~ /\/$evid\//) {
		@events = ($item);
		last;
		}
	  } else {
	    my $eq_time = item_eq_time($item);
	    my $time = item_time($item);
	    my $mag = item_mag($item);
	    my $grid = item_grid($item);
	    #get event id
	    $grid =~ /shake\/(.+)\/download/;
	    my $evt = $1;
	    
		push @saved_items, $item if (defined $evt_list{$evt});
	    next if (defined $evt_list{$evt} && $event_update<1);
	    if($time > $evt_list{$evt} && $eq_time > $min_shake_time && $mag > $mag_cutoff) {
		  ++$num_new;
		  push @events, $item;
		  $evt_list{$evt} = $time;
		  push @saved_items, $item;
	    }
	  }
	}
	if (!$evid) {
		open(SHAKE, ">$download_dir/item.xml");
		print SHAKE <<ITEM;
<?xml version="1.0"?>
<rss version="2.0" xmlns:geo="http://www.w3.org/2003/01/geo/wgs84_pos#" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:eq="http://earthquake.usgs.gov/rss/1.0/" xmlns:georss="http://www.georss.org/georss/">
<channel><title>USGS Earthquake ShakeMaps</title><description>List of ShakeMaps for events in the last 30 days</description><link>http://earthquake.usgs.gov/</link><dc:publisher>U.S. Geological Survey</dc:publisher><pubDate>$min_shake_time</pubDate>
ITEM
		foreach my $item (@saved_items) {
			print SHAKE "<item>",$item,"</item>\n";
		}
		print SHAKE "</channel></rss>\n";
		close(SHAKE);
	}

	#store time of last shakemap in shake.dat
	open(SHAKE, ">$rss_home/shakerss.dat");
	foreach my $key (keys %evt_list) {
		print SHAKE $key,':',$evt_list{$key},"\n";
	}
	close(SHAKE);

	if (scalar @events) {
		open(RSS, ">$rss_home/shakerss.status");
		print RSS '<xml>', map {  q{<item>}.$_ .q{</item>} } (@events);
		print RSS '</xml>';
		close(RSS);
	}
	
	my $recent_event;
	foreach(@events) {
	  #get grid url
	  my $grid = item_grid($_);
	  my $link = item_link($_);
	   

	  #get event id
	  $grid =~ /shake\/(.+)\/download/;
	  $evid = $1;
      my $dest = $download_dir."/$evid";
	  $recent_event = $evid unless (defined $recent_event);

	  #get network id
	  $grid =~ /shakemap\/(.+)\/shake/;
	  my $network = $1;
	  my $failed_dn = 'false';
	  foreach my $data_file (@data_files) {
	    #download grid file
		my $grid_url = "$grid/$data_file";
		$grid_url =~ s/<EVENT>/$evid/g;
		#my $grid_contents = get($grid_url);
		my $req = HTTP::Request->new('GET', $grid_url);
		my $response = $ua->request($req);
		#print "Response: ",$response->is_success,"\n";
		my $grid_contents = $response->content;

	    if (! $response->is_success) {
		  $data_file .= ".zip";
		  #$grid_contents = get($grid_url);
		  my $req = HTTP::Request->new('GET', $grid_url);
		  $response = $ua->request($req);
		  #print "Response: ",$response->is_success,"\n";
		  $grid_contents = $response->content;
		  #$grid_contents = $ua->request($req)->content;		}
		}

	    if ($response->is_success) {
	      logmsg "Download: Event $grid_url File";
	    } else {
		  $data_file =~ s/.zip$//;
	      logmsg "Download Failed: Event $grid_url File";
		  $failed_dn = 'true';
	      next;
	    };
		
	    #validate directory
	    if (not -e "$dest") {
	      my $result = mkpath("$dest", 0, 0755);
		  if (!$result) {
		  logmsg "Couldn't create download dir $dest";
		  $failed_dn = 'true';
		  }
	    }

	    #save grid file
		my $saved_file = "$dest/$data_file";
		$saved_file =~ s/<EVENT>/$evid/g;
	    open(GRD, ">$saved_file")  or logmsg "Couldn't save file: $saved_file";
	    binmode GRD;
	    print GRD $grid_contents;
	    close(GRD);
		# Stupid Windows fix for .zip type masking
		if (-B "$dest/$data_file" && "$dest/$data_file" =~ /.ps$|.txt$|.xyz$/) {
		  rename("$dest/$data_file", "$dest/$data_file.zip");
		}
	  }

	  #postcmd runs
	  if (@postcmds && @events && ($failed_dn eq 'false' || $exec_postcmd) ) {
        foreach ( @postcmds ) {
		  my $cmd = $_;
		  $cmd =~ s/<EVENT>/$evid/g;
		  $cmd =~ s/<SHAKE_WEB>/$link/g;
		  #print "Running command: $cmd\n";
		  my $rc = system($cmd);
	      if ($rc == 0) {
		    logmsg "Normal command run: '$cmd'";
          } else {
			logmsg "Error command run: $rc, '$cmd'";
		  }
        }
      }
	
		sc_xml($dest, $evid);
	}
	
	if (scalar @events && $recent_event) {
		open(EVT, ">$download_dir/recent_event.txt");
		print EVT $recent_event;
		close(EVT);
	}

return @events;
}

sub download_sm {

	my ($grid) = @_;
	my $link = $grid;
	
	$grid =~ s/(index.php|intensity.html)$//i;
	$grid .= 'download/';
	
	#get event id
	my ($evid) = $grid =~ /shake\/(.+)\/download/;
	
	return 0 unless ($evid);
	
	my $dest = $download_dir."/$evid";

	#get network id
	my ($network) = $grid =~ /shakemap\/(.+)\/shake/;
	my $failed_dn = 'false';
	foreach my $data_file (@data_files) {
		#download grid file
		my $grid_url = "$grid/$data_file";
		$grid_url =~ s/<EVENT>/$evid/g;
		#my $grid_contents = get($grid_url);
		my $req = HTTP::Request->new('GET', $grid_url);
		my $response = $ua->request($req);
		#print "Response: ",$response->is_success,"\n";
		my $grid_contents = $response->content;

		if (! $response->is_success) {
			$data_file .= ".zip";
			#$grid_contents = get($grid_url);
			my $req = HTTP::Request->new('GET', $grid_url);
			$response = $ua->request($req);
			#print "Response: ",$response->is_success,"\n";
			$grid_contents = $response->content;
			#$grid_contents = $ua->request($req)->content;		}
		}

		if ($response->is_success) {
			logmsg "Download: Event $grid_url File";
		} else {
			$data_file =~ s/.zip$//;
			logmsg "Download Failed: Event $grid_url File";
			$failed_dn = 'true';
			next;
		};

		#validate directory
		if (not -e "$dest") {
			my $result = mkpath("$dest", 0, 0755);
			if (!$result) {
				logmsg "Couldn't create download dir $dest";
				$failed_dn = 'true';
			}
		}

		#save grid file
		my $saved_file = "$dest/$data_file";
		$saved_file =~ s/<EVENT>/$evid/g;
		open(GRD, ">$saved_file")  or logmsg "Couldn't save file: $saved_file";
			binmode GRD;
			print GRD $grid_contents;
		close(GRD);
		# Stupid Windows fix for .zip type masking
		if (-B "$dest/$data_file" && "$dest/$data_file" =~ /.ps$|.txt$|.xyz$/) {
			rename("$dest/$data_file", "$dest/$data_file.zip");
		}
	}

	#postcmd runs
	if (@postcmds && ($failed_dn eq 'false' || $exec_postcmd) ) {
		foreach ( @postcmds ) {
			my $cmd = $_;
			$cmd =~ s/<EVENT>/$evid/g;
			$cmd =~ s/<SHAKE_WEB>/$link/g;
			#print "Running command: $cmd\n";
			my $rc = system($cmd);
			if ($rc == 0) {
				logmsg "Normal command run: '$cmd'";
			} else {
				logmsg "Error command run: $rc, '$cmd'";
			}
		}
	}

	my ($event_spec, $shakemap_spec) = sc_xml($dest, $evid);
	#<item><title>3.5 - 13.9 mi N of Trona, CA</title><description><![CDATA[<img src="http://earthquake.usgs.gov/eqcenter/shakemap/thumbs/shakemap_sc_10665629.jpg" width="100" align="left" hspace="10"/><p>Date: Fri, 14 May 2010 11:35:16 UTC<br/>Lat/Lon: 35.9653/-117.332<br/>Depth: 2.2</p>]]></description><link>http://earthquake.usgs.gov/eqcenter/shakemap/sc/shake/10665629/</link><pubDate>Fri, 14 May 2010 12:41:45 +0000</pubDate><geo:lat>35.9653</geo:lat><geo:long>-117.332</geo:long><dc:subject>3</dc:subject><eq:seconds>1273836916</eq:seconds><eq:depth>2.2</eq:depth><eq:region>sc</eq:region></item>
	my $event_item = '<title>'.$event_spec->{'magnitude'}.' - '.
		$event_spec->{'event_description'}.'</title><link>'.$link.'</link>';
	
	my (@items, @saved_items);
	push @saved_items, $event_item;
	if (-e "$download_dir/archive.xml") {
		open(SHAKE, "< $download_dir/archive.xml");
		my @lines = <SHAKE>;
		close(SHAKE);
		@items = rss_to_items(join '', @lines);
		foreach my $item (@items) {
			push @saved_items, $item unless ($item =~ /$link/);
		}
	}
	
	open(SHAKE, ">$download_dir/archive.xml");
	print SHAKE <<ITEM;
<?xml version="1.0"?>
<rss version="2.0" xmlns:geo="http://www.w3.org/2003/01/geo/wgs84_pos#" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:eq="http://earthquake.usgs.gov/rss/1.0/" xmlns:georss="http://www.georss.org/georss/">
<channel><title>USGS Earthquake ShakeMaps</title><description>List of ShakeMaps for events in the last 30 days</description><link>http://earthquake.usgs.gov/</link><dc:publisher>U.S. Geological Survey</dc:publisher>
ITEM
	foreach my $item (@saved_items) {
		print SHAKE "<item>",$item,"</item>\n";
	}
	print SHAKE "</channel></rss>\n";
	close(SHAKE);

	open(RSS, ">$rss_home/shakerss.status");
	print RSS '<xml><item><title>anual download from '.$link.'</title></item></xml>';
	close(RSS);

	open(EVT, ">$download_dir/recent_event.txt");
	print EVT $evid;
	close(EVT);

	return $evid;
}

#split rss feed into array of items, item data is untampered, but item tags do not match (cant use with XML::Simple)
sub rss_to_items
{
	my $rss = shift;

	my ($start, @items) = split '<item>', $rss;

	foreach(@items)
	{
		s/<\/item>.+//s;
	}

	return(@items);
}

#parses creation time of an RSS item
sub item_mag
{
	my $item = shift;
	
	$item =~ /<title>([\d\.]+)\s*\-.+<\/title>/;

	return($1);
}

sub item_eq_time
{
	my $item = shift;
	
	$item =~ /<eq:seconds>(\d+)<\/eq:seconds>/;

	return($1);
}

sub item_time
{
	my $item = shift;
	my %month = ( "JAN" => 0,
					"FEB" => 1,
					"MAR" => 2,
					"APR" => 3,
					"MAY" => 4,
					"JUN" => 5,
					"JUL" => 6,
					"AUG" => 7,
					"SEP" => 8,
					"OCT" => 9,
					"NOV" => 10,
					"DEC" => 11 );
					
	#<pubDate>Tue, 21 Nov 2006 21:06:23 +0000</pubDate>
	my ($day, $mon, $year, $hour, $min, $sec) = $item =~
	  /<pubDate>.+\s(\d+)\s(\w+)\s(\d+)\s(\d+):(\d+):(\d+).+<\/pubDate>/;
	
	#print "($day, ",$month{uc($mon)},", $year, $hour, $min, $sec)\n";
	return (timegm($sec, $min, $hour, $day, $month{uc($mon)}, $year-1900));
}

sub region_code
{
	my $item = shift;
	
	$item =~ /<eq:region>(\w+)<\/eq:region>/;

	return($1);
}

#parses link and appends grid file to path
sub item_grid
{
	my $item = shift;

	my ($url) = $item =~ /<link>(.+)<\/link>/;
#	$url =~ s/intensity.html$/download\//i;
	$url =~ s/(index.php|intensity.html)$//i;
	
#	return($1 . "/grid.xyz");
	$url .= 'download/';
	return($url);
}

#parses link and appends grid file to path
sub item_link
{
	my $item = shift;

	my ($url) = $item =~ /<link>(.+)<\/link>/;

	return($url);
}

my $fref;
my ($bn, $flag, $type, $flag_desc, $pdoc);
sub setOptions {
  $fref     = shift;
  my $dbug  = shift;
  my @names = ();

  foreach my $ff ( @$fref ) {
    (defined $ff->{FLAG} and $ff->{FLAG} !~ /^$/) or next;
    my $str = $ff->{FLAG};
    #----------------------------------------------------------------------
    # Is there an argument?
    #----------------------------------------------------------------------
    if ((defined $ff->{ARG}  and $ff->{ARG}  !~ /^$/)
     or (defined $ff->{TYPE} and $ff->{TYPE} !~ /^$/)
     or (defined $ff->{REQ}  and $ff->{REQ}  !~ /^$/)) {
      #----------------------------------------------------------------------
      # Yes, there's an argument of some kind; is it 
      # manditory or optional?
      #----------------------------------------------------------------------
      $str .= (defined $ff->{REQ} and $ff->{REQ} =~ /y/) ? '=' : ':';
      #----------------------------------------------------------------------
      # What is the expected type of the argument; default to 's'
      #----------------------------------------------------------------------
      my $type = (defined $ff->{TYPE} and $ff->{TYPE} !~ /^$/) 
               ? $ff->{TYPE} : 's';
      $str .= $type;
      #----------------------------------------------------------------------
      # If the type of argument is '!', then set $str directly
      #----------------------------------------------------------------------
      if ($type eq '!') {
	$str = $ff->{FLAG} . $type;
      }
      #----------------------------------------------------------------------
      # If ARG is undefined or empty, fix it up for the documentation
      #----------------------------------------------------------------------
      if (!defined $ff->{ARG} or $ff->{ARG} =~ /^$/) {
	$ff->{ARG} = $type =~ /s/ ? 'string'
		   : $type =~ /i/ ? 'integer'
		   : $type =~ /f/ ? 'float'
		   : $type =~ /!/ ? ''
		   : '???';
	if (defined $ff->{REQ}  and $ff->{REQ}  !~ /y/) {
	  $ff->{ARG} = '[' . $ff->{ARG} . ']';
	}
      }
    }
    print "OPTION LINE: $str\n" if (defined $dbug and $dbug != 0);
    push @names, $str;
  }

  my $options = {};

  if (@names) {
    GetOptions($options, @names) or die "Error in GetOptions";
  }

  if (defined $dbug and $dbug != 0) {
    foreach my $key ( keys %$options ) {
      print "option: $key value: $options->{$key}\n";
    }
  }
  return $options;
}

sub printDoc {

  $pdoc = shift;
  $bn   = basename($0);

  $~ = "PROGINFO";
  write;

  if (@$fref) {
    $~ = "OPTINFO";
  } else {
    $~ = "NOOPTINFO";
  }
  write;

  $~ = "FLAGINFO";
  foreach my $ff ( @$fref ) {
    (defined $ff->{FLAG} and $ff->{FLAG} !~ /^$/) or next;
    $flag      = $ff->{FLAG};
    $type      = defined $ff->{ARG}  ? $ff->{ARG}  : '';
    $flag_desc = defined $ff->{DESC} ? $ff->{DESC} : '';
    write;
  }
  $~ = "ENDINFO";
  write;
  0;
}

#######################################################################
# Self-documentation formats; we use the '#' character as the first 
# character (which is a royal pain to do) so that the documentation 
# can be included in a shell file
#######################################################################

format PROGINFO =
@<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
'################################################################################'
@ Program     : @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
'#',	 $bn
^ Description : ^<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
rhsh($pdoc),	      $pdoc
^ ~~            ^<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
rhsh($pdoc),	      $pdoc
@ Options     :
'#'
.

format OPTINFO =
@     Flag       Arg       Description
'#'
.

format NOOPTINFO =
@     NONE
'#'
.

format FLAGINFO =
@     ---------- --------- -----------------------------------------------------
'#'
^    -@<<<<<<<<< @<<<<<<<< ^<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
rhsh($flag_desc), $flag, $type, $flag_desc
^ ~~                       ^<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
rhsh($flag_desc),	   $flag_desc
.

format ENDINFO =
@<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
'################################################################################'
.

sub rhsh {

  my $more = shift;
  return '#' if ($more ne '');
  return '';
}


############################################################################
# Logs a message to the logfile with time/date stamp
############################################################################
sub logmsg { 

  print $log "$$: @_ on ", mydate(), "\n";
  print "@_ on ", mydate(), "\n" unless ($silent);
  return;
}

############################################################################
# Logs a message to the logfile without time/date stamp
############################################################################
sub logver { 

  print $log "$$: @_\n";
  return;
}

############################################################################
# Logs a message with time/date stamp to the logfile, then quits
############################################################################
sub logerr { 

  logmsg shift; 
  exit 1; 
}

############################################################################
# Logs a message with to the screen
############################################################################
sub logscr { 

  print STDOUT "$0 $$: @_ on ", mydate(), "\n"  unless ($silent);
  return;
}

sub mydate {

  my ($sec, $min, $hr, $day, $mon, $yr) = localtime();
  return sprintf('%02d/%02d/%4d %02d:%02d:%02d', 
		  $mon + 1, $day, $yr + 1900, $hr, $min, $sec);
}


########################################################################
# sub configure()
# Read the config file and set the appropriate values
########################################################################
sub configure {

  my $cfg  = new LoadConfig($cfile, $config_dirs, \&logscr)
                or logerr "Couldn't open config file $cfile";

  $cfg->parse( {'logfile'		=> \&logfile,
				'download_dir'	=> \&download_dir,
				'rss_url'		=> \&rss_url,
				'proxy'			=> \&proxy,
				'region'		=> \&region,
				'data_file'		=> \&data_file,
                'postcmd'		=> \&postcmd,
                'time_window'		=> \&time_window,
                'event_update'		=> \&event_update,
                'exec_postcmd'		=> \&exec_postcmd,
                'mag_cutoff'		=> \&mag_cutoff,
	       } );

  return 0;
}

#######################################################################
# Configuration subroutines
#######################################################################

sub logfile {

  $logfile = shift;
  #----------------------------------------------------------------------
  # Replace the <RSS_HOME> string with the install dir
  #----------------------------------------------------------------------
  $logfile =~ s/<RSS_HOME>/$rss_home/;
  #----------------------------------------------------------------------
  # logfile is guaranteed to be something, but can we open it?
  #----------------------------------------------------------------------
  if (!open(LOGOUT, ">> $logfile")) {
    logscr "can't open $logfile; trying /tmp/shakerss.log";
    $logfile = '/tmp/shakerss.log';
    if (!open(LOGOUT, ">> $logfile")) {
      logscr "can't open $logfile; will message to STDOUT";
      $logfile = 'STDOUT';
      $log = \*STDOUT;
      return "Couldn't open a logfile";
    }
    LOGOUT->autoflush(1);
    logscr "ok: logging to $logfile";
    $log = \*LOGOUT;
    return "Logging to $logfile";
  }
  LOGOUT->autoflush(1);
  $log = \*LOGOUT;
  return undef;
}

sub rss_url {
  
  my $url = shift;

  defined $url 
	or return "rss_url: bad value: $url";

  $rss_url = $url;
  return undef;
}

sub proxy {
  
  my $url = shift;

  defined $url 
	or return "proxy: bad value: $url";

  $ENV{"http_proxy"} = $url;
  $proxy = $url;

  return undef;
}

sub time_window {
  
  my $window = shift;

  $time_window = $window;
  return undef;
}

sub event_update {
  
  my $update = shift;

  $event_update = 1 if ($update =~ /true/i);
  return undef;
}

sub exec_postcmd  {
  
  my $update = shift;

  $exec_postcmd  = 1 if ($update =~ /true/i);
  return undef;
}

sub mag_cutoff {
  
  my $mag = shift;

  $mag_cutoff = $mag;
  return undef;
}

sub region {
  
  my $netid = shift;

  defined $netid 
	or return "region: bad value: $netid";

  push @regions, split(' ', $netid);
  $all_regions = 1 if (grep /all/i, @regions);
#  print join('::', @regions),", $all_regions\n";
  return undef;
}

sub download_dir {
  
  my $download = shift;
    
  defined $download 
	or return "download: argument required";

  $download =~ s/<RSS_HOME>/$rss_home/;

  #validate directory
  if (not -e "$download") {
    mkpath("$download", 0, 0755) or logerr "Couldn't create download dir";
  }

  $download_dir = $download;
  return undef;
}

sub data_file {

  my $data    = shift;

  defined $data 
	or return "data_file: argument required";
  
  if (defined $evid) {
	$data =~ s/<EVENT>/$evid/g;
  }
  push @data_files, $data;

  return undef;
}

sub postcmd {

  my $command = shift;

  defined $command 
	or return "postcmd: argument required";

  $command =~ s/<RSS_HOME>/$rss_home/;
  if (defined $evid) {
	$command =~ s/<EVENT>/$evid/g;
  }

  push @postcmds, $command;
  return undef;
}


