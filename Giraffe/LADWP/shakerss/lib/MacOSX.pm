package MacOSX;
###############################################################################
# This package handles all the GUI routines for the Windows operating system. #
# It requires various modules to be compiled into the runtime wrapper script  #
# - these modules are assumed to be there. If not, oopsy, we got problems.    #
#                                                                             #
# LIST OF ROUTINES BELOW:                                                     #
#   gui_init - start the gui and display the window.                          #
#   gui_listen - listen for window events for our gui.                        #
#   gui_note - send a note to our gui window.                                 #
#   open_url - open a url in the system's default browser.                    #
#   _things - various routines that control the gui and should be private.    #
###############################################################################

use strict; $|++;
use Fcntl;

use FindBin;

require Exporter;
use vars qw( @ISA @EXPORT );
@ISA = qw( Exporter );
@EXPORT = qw( gui_init gui_listen gui_note open_url open_rss exit_program );

# these variables are global for
# the Windows gui - they just keep
# track of various things that are important.
my ($hwnd, $icon, $icon_b, $hwnd_class, $window, $menu_bar, $server_port);
my ($img, $systray_icon, $systray_menu, $font, $logbox);

# remember the process id.
my (%cmds, $fork_id_path);
my $libpath = $FindBin::Bin.'/lib';


###############################################################################
# gui_init - start the gui and display the window.                            #
###############################################################################
# USAGE:                                                                      #
#   gui_init;                                                                 #
#                                                                             #
# NOTES:                                                                      #
#   This routine loads specific libraries specific to the OS and attempts to  #
#   do everything necessary to start up a GUI window and start listening for  #
#   events. Further down in this file, you should see supplementary GUI       #
#   routines (starting with _) that are part of the GUI happenings this       #
#   routine inits.                                                            #
#                                                                             #
#   Most of the code within this routine is thanks to David Berube.           #
#   NOTE: All of this code was written for Win32::GUI v0.5xx, which means     #
#   that deprecated v0.6xx things like -style and -exstyle still exist. The   #
#   reason is twofold: one, we still need to use v0.5xx under Windows 95,     #
#   and two, we're too lazy to find out the right equivalents under v0.6xx.   #
#                                                                             #
# RETURNS:                                                                    #
#   1; this routine always returns happily.                                   #
###############################################################################

sub gui_init {


	open STDIN, '/dev/null'   or die "Can't read /dev/null: $!";
	open STDOUT, '>/dev/null' or die "Can't write to /dev/null: $!";
	open STDERR, '>/dev/null' or die "Can't write to /dev/null: $!";
	defined(my $pid = fork)   or die "Can't fork: $!";
	exit if $pid;

return 1;

}

###############################################################################
# gui_listen - listen for window events for our gui.                          #
###############################################################################
# USAGE:                                                                      #
#   gui_listen;                                                               #
#                                                                             #
# NOTES:                                                                      #
#   This routine checks the event queue and sees if there is anything that    #
#   needs to be done. It's called from the main loop of our program.          #
#                                                                             #
# RETURNS:                                                                    #
#   1; this routine always returns happily.                                   #
###############################################################################

sub gui_listen {

   my $lin;

   # set to non-blocking in gui_init().
   if (defined ($lin=<STDIN>)) {

      # parse out the command prefix, keyword, and args
      chomp $lin; my ($prefix, $cmd, @args) = split(/ /, $lin);

      # only accept input with prefix 'CMD'. if the subroutine
      # for the CMD is found, we rush off to execution.
      if ((uc $prefix) eq 'CMD') {
         $cmds{uc $cmd}->()
            if (defined $cmds{uc $cmd});
      }
   }


   return 1;

}

###############################################################################
# gui_note - send a note to our gui window.                                   #
###############################################################################
# USAGE:                                                                      #
#   gui_note("This is a gui window line. Yup.");                              #
#                                                                             #
# NOTES:                                                                      #
#   Much like note(), only we send our message to our os specific gui window. #
#                                                                             #
# RETURNS:                                                                    #
#   1; this routine always returns happily.                                   #
###############################################################################

sub gui_note {

   # eventually, this will do something a bit more meaningful.
   my ($message) = @_; print STDOUT $message . "\n"; return 1;

   return 1;

}

###############################################################################
# open_url - open a url in the system's default browser.                      #
###############################################################################
# USAGE:                                                                      #
#    open_url( );                                                             #
#                                                                             #
# OS SPECIFIC NOTES:                                                          #
#    This routine checks in the registry for the user's default browser.      #
#    If we haven't been told to use anything else, we use the registry        #
#    settings, else we try to pass the $url as a shell parameter to the       #
#    user's preferred choice.                                                 #
#                                                                             #
# RETURNS:                                                                    #
#    1; we instruct the user to open their browser if we can't.               #
###############################################################################

sub open_url {

  my ($port) = @_;
  $server_port = $port if ($port);
   # construct our url.
   my $url = "http://127.0.0.1:$server_port/index.html";

   # we spit out our suggestion just to catch all instances.
   gui_note("GUI_CMD OPEN_URL $url", 1) if $ENV{DYLD_LIBRARY_PATH};
   gui_note("If your browser doesn't load, go to <$url>.", 1);

		system( "open $url"); 


   return 1;

}

sub open_rss {

	my ($event, $action) = @_;
	
	my ($exitcode, $cmd_param, $result);

	if ($event && $action =~ /process|add|remove/i) {
		$cmd_param = " -event $event -".$action;
	} elsif ($event) {
		$cmd_param = " -download ".$event;
	} else {
		$cmd_param = " &";
	}

	#unless ( $fork_id_path = fork ) { 

	if (-e "$libpath/../shakerss.status") {
		open(RSS, "< $libpath/../shakerss.status");
		$result = <RSS>;
		close(RSS);
		unlink "$libpath/../shakerss.status";
	}

		system( "$libpath/../shakerss $cmd_param"); 
	#}

   return $result;

}

###############################################################################
# _things - various routines that control the gui and should be private.      #
###############################################################################
# USAGE:                                                                      #
#    These are internally called by the GUI and shouldn't be publically       #
#    used. So stop poking around here. Sheesh. Flippin' nosy people. Sigh.    #
###############################################################################

sub _gui_cmd_refresh {

  my ($server_port) = @_;
   # helloooooooo, nurse!
   gui_note("Refreshing the channel list...",                                                   1);
   gui_note("--------------------------------------------------------------------------------", 1);

   # do the dirty deed of download and open.
   my $url = "http://127.0.0.1:$server_port/index.html";
   gui_note("GUI_CMD OPEN_URL $url", 1) if $ENV{DYLD_LIBRARY_PATH};

   return 1;

}

# our exit code - called from various methods in the Windows
# GUI (systray quit, window termination, file->exit, etc.)
sub exit_program {

   # remove the icon.
   #Windows::_Remove_Systray_Icon;

   # ciao.
   exit;

}


1;
