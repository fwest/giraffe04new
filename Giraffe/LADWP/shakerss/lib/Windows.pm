package Windows;
###############################################################################
# This package handles all the GUI routines for the Windows operating system. #
# It requires various modules to be compiled into the runtime wrapper script  #
# - these modules are assumed to be there. If not, oopsy, we got problems.    #
#                                                                             #
# LIST OF ROUTINES BELOW:                                                     #
#   gui_init - start the gui and display the window.                          #
#   gui_listen - listen for window events for our gui.                        #
#   gui_note - send a note to our gui window.                                 #
#   open_url - open a url in the system's default browser.                    #
#   _things - various routines that control the gui and should be private.    #
###############################################################################

use strict; $|++;
use Win32::API;
use Win32::GUI;
use Win32::TieRegistry;
use Win32::Process;

use FindBin;

require Exporter;
use vars qw( @ISA @EXPORT );
@ISA = qw( Exporter );
@EXPORT = qw( gui_init gui_listen gui_note open_url open_rss exit_program );

# these variables are global for
# the Windows gui - they just keep
# track of various things that are important.
my ($hwnd, $icon, $icon_b, $hwnd_class, $window, $menu_bar, $server_port);
my ($img, $systray_icon, $systray_menu, $font, $logbox);

# remember the process id.
my ($Processrss, $ProcessObj);
my $libpath = $FindBin::Bin.'/lib';


###############################################################################
# gui_init - start the gui and display the window.                            #
###############################################################################
# USAGE:                                                                      #
#   gui_init;                                                                 #
#                                                                             #
# NOTES:                                                                      #
#   This routine loads specific libraries specific to the OS and attempts to  #
#   do everything necessary to start up a GUI window and start listening for  #
#   events. Further down in this file, you should see supplementary GUI       #
#   routines (starting with _) that are part of the GUI happenings this       #
#   routine inits.                                                            #
#                                                                             #
#   Most of the code within this routine is thanks to David Berube.           #
#   NOTE: All of this code was written for Win32::GUI v0.5xx, which means     #
#   that deprecated v0.6xx things like -style and -exstyle still exist. The   #
#   reason is twofold: one, we still need to use v0.5xx under Windows 95,     #
#   and two, we're too lazy to find out the right equivalents under v0.6xx.   #
#                                                                             #
# RETURNS:                                                                    #
#   1; this routine always returns happily.                                   #
###############################################################################

sub gui_init {

	my ($port) = @_;
	$server_port = $port if ($port);
   # hwnd is a handle to a window - basically, window's
   # way of keeping track of it's program windows...
   $hwnd = Win32::GUI::GetPerlWindow();

   # comment this to see error messages in a dos window
   # otherwise, this will hide the blasted thing...
   #Win32::GUI::Hide($hwnd);

   # get the width and height of the user's system.
   my $screen_width  = Win32::GUI::GetSystemMetrics(0);
   my $screen_height = Win32::GUI::GetSystemMetrics(1);

   # create the icon handler.
   $icon = new Win32::GUI::Icon( "$libpath/shakecast_icon_w.ico" );
   $icon_b = new Win32::GUI::Icon( "$libpath/shakecast_icon.ico" );

   # create a window class for our window.
   $hwnd_class = new Win32::GUI::Class( -name => "ShakeCast Class",
                                        -icon => $icon_b
                                      );

   # set up our menu bar. these point to various
   # routines defined later on in this file, as 
   # well as set up key commands for the items.
   $menu_bar = new Win32::GUI::Menu( "&File"                 => "Windows::File",
                                     " > E&xit"              => "Windows::_FileExit",
                                     "&Edit"                 => "Windows::Edit",
                                     " > &Copy Ctrl+C"       => "Windows::_EditCopy",
                                     " > Select &All Ctrl+A" => "Windows::_EditSelectAll",
                                     "&Tools"                => "Windows::Tools",
                                     " > &Open Browser"      => "Windows::_OpenBrowser",
                                     " > &Refresh RSS"      => "Windows::_RefreshRss",
                                     " > &Edit Config"      => "Windows::_OpenConf",
                                   );

   # creates the main window. notice that we use a generic
   # "Name" parameter, which is used for events, which must
   # have the format Name_EventName.
   $window = new Win32::GUI::Window( -name    => 'Windows::_Window', 
                                     -text    => 'ShakeCast RSS Reader',
                                     -left    => ($screen_width - 600)/2,
                                     -top     => ($screen_height - 400)/2,
                                     -width   => 480, -height => 400,
                                     -maxsize => [480, 400],
                                     -minsize => [480, 400],
                                     -menu    => $menu_bar,
                                     -class   => $hwnd_class,
                                     -icon    => $icon_b,
                                     -maximizebox => 0
                                   );

   # create the systray icon.
   $systray_icon = $window->AddNotifyIcon( -name => "Windows::_Systray",
                                           -icon => $icon_b,
                                           -balloon  => 1,
                                           -balloon_title  => 'ShakeCast Lite Started',
                                           -balloon_tip  => 'Personal Web Server Port: '.$server_port,
                                           -balloon_timeout  => 5000,
                                           -tip  => 'ShakeCast Lite'
                                         );

   # create the popup menu.
   $systray_menu = new Win32::GUI::Menu( "SystrayMenu Functions" => "SystrayMenu",
                                         "> Open Browser"        => "Windows::_OpenBrowser",
                                         "> Refresh RSS"        => "Windows::_RefreshRss",
                                         "> Edit Config"        => "Windows::_OpenConf",
                                         "> Exit"                => "Windows::_SystrayExit"
                                       );

   # actually display the image...
   $img = new Win32::GUI::Bitmap( "$libpath/shakecast_logo.bmp" );

   # create our pretty logo.
   $window->AddLabel( -text    => "",
                      -name    => "Bitmap",
                      -left    => 0,
                      -top     => 0,
                      -width   => 480,
                      -height  => 68,
                      -visible => 1,
					  -bitmap  => $img,
                    );

   # set the font of our log box below. we grab the Win32::GUI version so
   # that we can work around a bug fixed in the newest version of Win32::GUI,
   # whilst still providing support for the older (used by our Win95 runtime).
   my $version = $Win32::GUI::VERSION; my $font_size;
   if ($version eq "0.0.558") { $font_size = 12; } else { $font_size = 7; }
   $font = Win32::GUI::Font->new( -name => "Verdana", -size => $font_size );  

   # create the log box which is gonna hold all our info.
   $logbox = $window->AddRichEdit( -name    => "Windows::_RichEdit",
                                   -font    => $font,
                                   -top     => 70,
                                   -left    => 0,
                                   -width   => 505,
                                   -height  => 240,
                                   -tabstop => 1,
									-hscroll   => 1,
									-vscroll   => 1,
									-autohscroll => 1,
									-autovscroll => 1,
                                 );

   # and make it a little smaller than our window size.
   $logbox->Resize( $window->ScaleWidth, $window->ScaleHeight - 70 );

   # finally, show all
   # the junk we just did.
   #$window->Show();

   return 1;

}

###############################################################################
# gui_listen - listen for window events for our gui.                          #
###############################################################################
# USAGE:                                                                      #
#   gui_listen;                                                               #
#                                                                             #
# NOTES:                                                                      #
#   This routine checks the event queue and sees if there is anything that    #
#   needs to be done. It's called from the main loop of our program.          #
#                                                                             #
# RETURNS:                                                                    #
#   1; this routine always returns happily.                                   #
###############################################################################

sub gui_listen {

   # anyone there?
   Win32::GUI::PeekMessage(0,0,0);
   Win32::GUI::DoEvents();

   return 1;

}

###############################################################################
# gui_note - send a note to our gui window.                                   #
###############################################################################
# USAGE:                                                                      #
#   gui_note("This is a gui window line. Yup.");                              #
#                                                                             #
# NOTES:                                                                      #
#   Much like note(), only we send our message to our os specific gui window. #
#                                                                             #
# RETURNS:                                                                    #
#   1; this routine always returns happily.                                   #
###############################################################################

sub gui_note {

   my ($message) = @_;
   if ($message =~ /^<xml>/) {
		use XML::Simple;
		my $xml = XMLin($message)->{item};
		my $balloon_tip;
		if (ref($xml) eq "HASH") {
			$balloon_tip = 'M'.$xml->{title}." \n".localtime();
			$message = $balloon_tip;
		} elsif (ref($xml) eq "ARRAY") {
			$balloon_tip = 'M'.$xml->[0]->{title}." \n".localtime();
			$balloon_tip .= "\n\n... ". (scalar @$xml - 1). " more ShakeMap.";
			$message = join "\n", map {  $_->{'title'}  } (@$xml);
			$message .= "\n".localtime();
		} 
		
	  $systray_icon->Change( -icon => $icon,
						   -balloon  => 1,
						   -balloon_title  => 'ShakeCast Lite New ShakeMap',
						   -balloon_tip  => $balloon_tip,
		);
	}

   # select our last line.
   $logbox->Select(999999,999999);
   $logbox->ReplaceSel("$message\n", 1);
   select(undef, undef, undef, 0.25);
 
   # autoscroll the log box.
   $logbox->SendMessage (0x115, 1, 0) while $message =~ /\n|$/g;

   # listen for good measure.
   Win32::GUI::PeekMessage(0,0,0);
   Win32::GUI::DoEvents();

   return 1;

}

###############################################################################
# open_url - open a url in the system's default browser.                      #
###############################################################################
# USAGE:                                                                      #
#    open_url( );                                                             #
#                                                                             #
# OS SPECIFIC NOTES:                                                          #
#    This routine checks in the registry for the user's default browser.      #
#    If we haven't been told to use anything else, we use the registry        #
#    settings, else we try to pass the $url as a shell parameter to the       #
#    user's preferred choice.                                                 #
#                                                                             #
# RETURNS:                                                                    #
#    1; we instruct the user to open their browser if we can't.               #
###############################################################################

sub open_url {

  my ($port) = @_;
  $server_port = $port if ($port);
   # construct our url.
   my $url = "http://127.0.0.1:$server_port/index.html";
   my $browser = $Registry->{"Classes\\http\\shell\\open\\command"}->{'\\'};

   # if a browser_path hasn't been set, try
   # to open the default browser using the native API.
   # we do an API call instead of a 'start $url' sort
   # of thing, because the API call seems far more stable -
   # we've experienced slowness with 'start $url'
   # and random crashes on some machines. 
   #if ( get_setting("user_browser_path") eq "default" ) {
      my $ShellExecute = new Win32::API("shell32", "ShellExecuteA", ['N','P', 'P', 'P', 'P', 'I'], 'N');
      $ShellExecute->Call(0, "open", $url, 0, 0, 1);
   #}
   


   return 1;

}

sub open_conf {

   my $systemroot = $ENV{SystemRoot};
   #unless ( $fork_id_path = fork ) { 
	Win32::Process::Create($ProcessObj,
                                "$systemroot\\system32\\notepad.exe",
                                "notepad $libpath/../shakerss.conf",
                                0,
                                NORMAL_PRIORITY_CLASS,
                                ".");
   #}

   return 1;

}

sub open_rss {

	my ($event, $action) = @_;
	
	my ($exitcode, $cmd_param, $result);
	$Processrss->GetExitCode($exitcode) if ( $Processrss );
	if ($event && $action =~ /process|add|remove/i) {
		$cmd_param = " -event $event -".$action;
	} elsif ($event) {
		$cmd_param = " -download ".$event;
	}
	
	unless ($exitcode ) {
		$Processrss->Kill($exitcode) if ($Processrss);
		Win32::Process::Create($Processrss,
									"$libpath/../shakerss.exe",
									"shakerss.exe $cmd_param",
									0,
									CREATE_NO_WINDOW,
									".");
	}

   return $result;

}

###############################################################################
# _things - various routines that control the gui and should be private.      #
###############################################################################
# USAGE:                                                                      #
#    These are internally called by the GUI and shouldn't be publically       #
#    used. So stop poking around here. Sheesh. Flippin' nosy people. Sigh.    #
###############################################################################

# various menu commands.
sub Windows::_EditCopy_Click      { $logbox->SendMessage(0x301,0,0); }
sub Windows::_EditSelectAll_Click { $logbox->Select(0,999999); }
sub Windows::_FileExit_Click      { &exit_program; }
sub Windows::_OpenConf_Click { open_conf(); }
sub Windows::_OpenBrowser_Click { open_url(); }
sub Windows::_RefreshRss_Click { 
	open_rss(); 
	gui_note("ShakeMap RSS Feed refreshed at ".localtime()); 
	gui_note('-'x80); 
}
sub Windows::_Remove_Systray_Icon { $systray_icon->Remove(  ); }
sub Windows::_Systray_MouseEvent { my $event = shift; if ($event == 515) { $window->Enable; $window->Show; } }

# If someone right clicks the systray icon, show a menu.
sub Windows::_Systray_RightClick { 

   # get the x and y coords of the mouse to display the menu at.
   my($x, $y) = Win32::GUI::GetCursorPos();

   # make the popup menu visible at the cursor
   $window->TrackPopupMenu($systray_menu->{SystrayMenu}, $x, $y);
}

# If someone right clicks the systray icon, show a menu.
sub Windows::_Systray_Click { 

  $systray_icon->Change( -icon => $icon_b );
  
  if (Win32::GUI::IsVisible($window)) {
    Windows::_Window_Minimize();
  } else {
    $window->Enable; $window->Show; 
    Win32::GUI::SetForegroundWindow($window);
  }
}

# quit the program. we only do it this way so we can track what was clicked.
sub Windows::_SystrayExit_Click { &exit_program; }

# hide the window if it's been minimized. the only
# way to get it back would be from the systray icon.
# we need to figure out what to do when people click
# the "X' on the window. minimize? or close the app?
sub Windows::_Window_Minimize  { $window->Disable; $window->Hide; return -1; }
sub Windows::_Window_Terminate { $window->Disable; $window->Hide; return -1; }

# our exit code - called from various methods in the Windows
# GUI (systray quit, window termination, file->exit, etc.)
sub exit_program {

   # remove the icon.
   Windows::_Remove_Systray_Icon;

   # ciao.
   exit;

}

1;
