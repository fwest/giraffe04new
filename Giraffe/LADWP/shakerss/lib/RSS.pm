package RSS;

# $Id: RSS.pm 486 2008-10-03 16:57:51Z klin $

##############################################################################
# 
# Terms and Conditions of Software Use
# ====================================
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Disclaimer of Earthquake Information
# ====================================
# 
# The data and maps provided through this system are preliminary data
# and are subject to revision. They are computer generated and may not
# have received human review or official approval. Inaccuracies in the
# data may be present because of instrument or computer
# malfunctions. Subsequent review may result in significant revisions to
# the data. All efforts have been made to provide accurate information,
# but reliance on, or interpretation of earthquake data from a single
# source is not advised. Data users are cautioned to consider carefully
# the provisional nature of the information before using it for
# decisions that concern personal or public safety or the conduct of
# business that involves substantial monetary or operational
# consequences.
# 
# Disclaimer of Software and its Capabilities
# ===========================================
# 
# This software is provided as an "as is" basis.  Attempts have been
# made to rid the program of software defects and bugs, however the
# U.S. Geological Survey (USGS) have no obligations to provide maintenance, 
# support, updates, enhancements or modifications. In no event shall USGS 
# be liable to any party for direct, indirect, special, incidental or 
# consequential damages, including lost profits, arising out of the use 
# of this software, its documentation, or data obtained though the use 
# of this software, even if USGS or have been advised of the
# possibility of such damage. By downloading, installing or using this
# program, the user acknowledges and understands the purpose and
# limitations of this software.
# 
# Contact Information
# ===================
# 
# Coordination of this effort is under the auspices of the USGS Advanced
# National Seismic System (ANSS) coordinated in Golden, Colorado, which
# functions as the clearing house for development, distribution,
# documentation, and support. For questions, comments, or reports of
# potential bugs regarding this software please contact wald@usgs.gov or
# klin@usgs.gov.  
#
#############################################################################

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../lib";

use Cwd;
use File::Path;
use File::Basename qw(basename);
use IO::File;
use XML::Writer;
use XML::Simple;
use XML::Parser;
use POSIX qw(strftime);
use Getopt::Long;
use LWP::Simple;
use Time::Local;
use Data::Dumper;

use vars qw( @ISA @EXPORT );
@ISA = qw( Exporter );
@EXPORT = qw( sc_xml );

############################################################################
# Prototypes for the logging routines
############################################################################
sub logmsg;
sub logver;
sub logerr;
sub logscr;
sub mydate;
sub grid_keys;

#######################################################################
# Global variables
#######################################################################

my $perl = 'PERL.EXE';

#----------------------------------------------------------------------
# Name of the configuration files
#----------------------------------------------------------------------

my $shake_home = $FindBin::Bin;

#######################################################################
# End global variables
#######################################################################

#######################################################################
# End of command line option stuff
#######################################################################

#######################################################################
# User config 
#######################################################################
	
my $logfile;			# Where we dump all of our messages
my $log;			# Filehandle of the log file
my $rss_home = $FindBin::Bin;
my $config_dirs = [ $rss_home ];
my $download_dir;
my $rss_url;
my @data_files;
my @events;
my @regions;
my $all_regions;
my $tag = "";
my %grid_spec;
my @grid_metric;
my %shakemap_spec;
my %event_spec;

#######################################

sub BEGIN {
    no strict 'refs';
    for my $method (qw(
			event_id event_version external_event_id
			event_status event_type
			event_name event_location_description
			magnitude lat lon
			event_timestamp receive_timestamp
			initial_version superceded_timestamp
			seq
			)) {
	*$method = sub {
	    my $self = shift;
	    @_ ? ($self->{$method} = shift, return $self)
	       : return $self->{$method};
	}
    }
}

sub from_xml {
    my ($class, $xml_source) = @_;
    undef $SC::errstr;
    my $xml = SC->xml_in($xml_source);
    return undef unless defined $xml;
    unless (exists $xml->{'event'}) {
	$SC::errstr = 'XML error: event element not found';
	return undef;
    }
    return $class->new(%{ $xml->{'event'} });
}


sub new {
    my ($class, $xml) = @_;
    my $self = bless {} => $class;
    Carp::carp "Odd (wrong?) number of parameters in new()" if $^W && (@_ & 1); 
	$self->{'feed'} = $xml;

    return $self;
}


sub _min {
    (defined $_[0] && (!defined $_[1] || $_[0] <= $_[1])) ? $_[0] : $_[1];
}

sub _max {
    (defined $_[0] && (!defined $_[1] || $_[0] >= $_[1])) ? $_[0] : $_[1];
}

sub ts_to_time {
    my ($time_str) = @_;
	
	use Time::Local;
	my %months = ('jan' => 0, 'feb' =>1, 'mar' => 2, 'apr' => 3, 'may' => 4, 'jun' => 5,
		'jul' => 6, 'aug' => 7, 'sep' => 8, 'oct' => 9, 'nov' => 10, 'dec' => 11);
	my ($mday, $mon, $yr, $hr, $min, $sec);
	my $timegm;
	
	if ($time_str =~ /[a-zA-Z]+/) {
		# <pubDate>Tue, 04 Mar 2008 20:57:43 +0000</pubDate>
		($mday, $mon, $yr, $hr, $min, $sec) = $time_str 
			=~ /(\d+)\s+(\w+)\s+(\d+)\s+(\d+)\:(\d+)\:(\d+)\s+/;
		$timegm = timegm($sec, $min, $hr, $mday, $months{lc($mon)}, $yr-1900);
	} else {
		#2008-10-04 20:57:43
		($yr, $mon, $mday, $hr, $min, $sec) = $time_str 
			=~ /(\d+)\-(\d+)\-(\d+)\s+(\d+)\:(\d+)\:(\d+)/;
	   
		$timegm = timegm($sec, $min, $hr, $mday, $mon-1, $yr-1900);
	}
    
	return ($timegm);
}

sub time_to_ts {
    my $ts = (@_ ? shift : time);
	if ($ts =~ /[\:\-]/) {
		$ts =~ s/[a-zA-Z]/ /g;
		$ts =~ s/\s+$//g;
		$ts = ts_to_time($ts);
	}
	
    my ($sec, $min, $hr, $mday, $mon, $yr);
	($sec, $min, $hr, $mday, $mon, $yr) = localtime $ts;
    sprintf ("%04d-%02d-%02d %02d:%02d:%02d",
	     $yr+1900, $mon+1, $mday,
	     $hr, $min, $sec);
}

sub sc_xml {

my ($dest, $evid) = @_;
  my ($sv, $dbh, $sth, $ofile, $fh, $ev_status, $sm_status, $etype);
  my (%grdinfo, $file, $base, $lines, $val);
  my ($owd, $writer, $key, $product);
  my ($command, $result);
  
  my $facility;
  my $facility_file = "$dest/../facility.xml";
  $facility = XMLin($facility_file) if (-e $facility_file);
	$facility_file = "$dest/facility.xml";
	unlink $facility_file if (-e $facility_file);
  
	$file = "$dest/grid.xml";

	my $parser = new XML::Parser;
	$parser->setHandlers(      Start => \&startElement,
											 End => \&endElement,
											 Char => \&characterData,
											 Default => \&default);
	$parser->parsefile($file);

	my $lon_spacing = $grid_spec{'nominal_lon_spacing'};
	my $lat_spacing = $grid_spec{'nominal_lat_spacing'};
	my $lon_cell_count = $grid_spec{'nlon'};
	my $lat_cell_count = $grid_spec{'nlat'};
	my $lat_min = $grid_spec{'lat_min'};
	my $lat_max = $grid_spec{'lat_max'};
	my $lon_min = $grid_spec{'lon_min'};
	my $lon_max = $grid_spec{'lon_max'};
    my $cols_per_degree = sprintf "%d", 1/$lon_spacing + 0.5;
    my $rows_per_degree = sprintf "%d", 1/$lat_spacing + 0.5;
	$lat_min -= 0.5 * $lat_spacing;
	$lat_max += 0.5 * $lat_spacing;
	$lon_min -= 0.5 * $lon_spacing;
	$lon_max += 0.5 * $lon_spacing;

	
	my (@max, @min, @cells);
	open (FH, "< $dest/grid.xml") || die "couldn't parse grid data $!";
	my $line;
	do {
		$line = <FH>;
	} until ($line =~ /^<grid_data>/);
	
	my $cell_count = 0;
	while ($line = <FH>) {
		$line =~ s/\n|\t//g;
        my ($lon, $lat, @gv) = split ' ', $line;
		for (my $i = 0; $i < scalar @gv; $i++) {
			$min[$i] = _min($min[$i], $gv[$i]);
			$max[$i] = _max($max[$i], $gv[$i]);
		}
		$cells[$cell_count++] = \@gv;
	}

	$event_spec{'event_timestamp'} = time_to_ts($event_spec{'event_timestamp'});
	$shakemap_spec{'process_timestamp'} = time_to_ts($shakemap_spec{'process_timestamp'});

	if ($shakemap_spec{'map_status'} =~ /RELEASED|REVIEWED/i) {
	$ev_status = 'NORMAL';
	} else {
	$ev_status = $shakemap_spec{'map_status'};
	}
  #-----------------------------------------------------------------------
  # Generate event.xml:
  #
  # Open the event file
  #----------------------------------------------------------------------
  $ofile = "$dest/event.xml";
  $fh = new IO::File "> $ofile" or logscr "Couldn't open $ofile";

  #----------------------------------------------------------------------
  # Creae the XML::Writer object; the "NEWLINES" implementation is a
  # disaster so we don't use it, and hence have to add our own all
  # over the place
  #----------------------------------------------------------------------
  $writer = new XML::Writer(OUTPUT => \*$fh, NEWLINES => 0);

  $writer->emptyTag("event",
		    "event_id"          => $shakemap_spec{'event_id'},
		    "event_version"     => $shakemap_spec{'shakemap_version'},
		    "event_status"      => $ev_status,
		    "event_type"        => $shakemap_spec{'shakemap_event_type'},
		    "event_name"        => $shakemap_spec{'event_name'},
		    "event_location_description" => $event_spec{'event_description'},
		    "event_timestamp"   => "$event_spec{'event_timestamp'}",
		    "user_timezone"   => strftime("%Z", localtime()),
		    "external_event_id" => "$shakemap_spec{'event_id'}",
		    "magnitude"         => $event_spec{'magnitude'},
		    "lat"               => $event_spec{'lat'},
		    "lon"               => $event_spec{'lon'});
  $writer->end();
  $fh->close;

  #-----------------------------------------------------------------------
  # Generate shakemap.xml:
  #
  # Open the shakemap file
  #----------------------------------------------------------------------
  $ofile = "$dest/shakemap.xml";
  $fh = new IO::File "> $ofile" or logscr "Couldn't open $ofile";

  #----------------------------------------------------------------------
  # Creae the XML::Writer object;
  #----------------------------------------------------------------------
	$writer = new XML::Writer(OUTPUT => \*$fh, NEWLINES => 0);
	$writer->startTag("shakemap", 
		"shakemap_id"          => $shakemap_spec{'shakemap_id'}, 
		"shakemap_version"     => $shakemap_spec{'shakemap_version'}, 
		"event_id"             => $shakemap_spec{'event_id'}, 
		"event_version"        => $shakemap_spec{'shakemap_version'}, 
		"shakemap_status"      => $shakemap_spec{'map_status'},
		"generating_server"    => 1,
		"shakemap_region"      => $shakemap_spec{'shakemap_originator'},
		"generation_timestamp" => $shakemap_spec{'process_timestamp'},
		"begin_timestamp"      => $shakemap_spec{'process_timestamp'},
		"end_timestamp"        => $shakemap_spec{'process_timestamp'},
		"lat_min"              => $lat_min,
		"lat_max"              => $lat_max,
		"lon_min"              => $lon_min,
		"lon_max"              => $lon_max);
	$writer->characters("\n");
	for (my $i = 0; $i < scalar @min; $i++) {
		$writer->emptyTag("metric",
				  "metric_name" => $grid_metric[$i+2],
				  "min_value"   => $min[$i],
				  "max_value"   => $max[$i]);
		$writer->characters("\n");
	}
	$writer->endTag("shakemap");
	$writer->end();
	$fh->close;

	if ($facility) {
		my @shaking;
		my $xml = {};
		for (my $index =0; $index < scalar @{$facility->{'FACILITY'}}; $index++) {
			my $poi = $facility->{'FACILITY'}->[$index];
			next unless ($poi->{'LON'} && $poi->{'LAT'});
			my %poi_damage;
			if ($facility->{'FACILITY'}->[$index]->{'DAMAGE'}) {
				my $states = $facility->{'FACILITY'}->[$index]->{'DAMAGE'};
				foreach my $state (keys %{$states}) {
					$poi_damage{$states->{$state}->{'METRIC'}} = 1;
				}
			}
			my $lon = $poi->{'LON'};
			my $lat = $poi->{'LAT'};
			next unless ($lon < $lon_max && $lon > $lon_min 
				&& $lat < $lat_max && $lat > $lat_min);
			
			my $cell_no = int(($lat_max- $lat) * $rows_per_degree) *  $lon_cell_count
				+ int(($lon - $lon_min) * $cols_per_degree + 0.5);

			if ($cell_no >= 0 && $cell_no <= $cell_count) {
				my @summary = @{$cells[$cell_no]};

				for (my $i = 0; $i < scalar @min; $i++) {
					$poi->{'SHAKING'}->{$grid_metric[$i+2]}
						= $summary[$i];
					next unless ($poi_damage{$grid_metric[$i+2]});
					my $states = $facility->{'FACILITY'}->[$index]->{'DAMAGE'};
					foreach my $state (keys %{$states}) {
						next unless ($states->{$state}->{'METRIC'} eq $grid_metric[$i+2]);
						$states->{$state}->{'STATE'} = 1 if
							($summary[$i] >= $states->{$state}->{'LOW_LIMIT'} &&
							$summary[$i] <= $states->{$state}->{'HIGH_LIMIT'});
					}
				}
				push @shaking, $poi;
			}
		}
		

		if (scalar @shaking) {
			$facility_file = "$dest/facility.xml";
			$xml->{'FACILITY'} = \@shaking;
			$xml = XMLout( $xml, 'OutputFile' => $facility_file);	
		}
	}

return (\%event_spec, \%shakemap_spec);
}

sub startElement {

      my( $parseinst, $element, %attrs ) = @_;
        SWITCH: {
                if ($element eq "shakemap_grid") {
                        #$count++;
                        $tag = "shakemap_grid";
                        #print "shakemap_grid $count:\n";
						foreach my $key (keys %attrs) {
							$shakemap_spec{$key} = $attrs{$key};
							#print $key,": ", $shakemap_spec{$key}, "\n";
						}
						last SWITCH;
                }
                if ($element eq "event") {
                        #$count++;
                        $tag = "event";
                        #print "event $count:\n";
						foreach my $key (keys %attrs) {
							$event_spec{$key} = $attrs{$key};
							#print $key,": ", $event_spec{$key}, "\n";
						}
						last SWITCH;
                }
                if ($element eq "grid_specification") {
                        #$count++;
                        $tag = "grid_specification";
                        #print "grid_specification $count:\n";
						foreach my $key (keys %attrs) {
							$grid_spec{$key} = $attrs{$key};
							#print $key,": ", $grid_spec{$key}, "\n";
						}
						last SWITCH;
                }
                if ($element eq "grid_field") {
                        #print "grid_field: $count:\n";
                        $tag = "grid_field";
						$grid_metric[$attrs{'index'}-1] = $attrs{'name'};
						#print $attrs{'index'},": ", $attrs{'name'}, "\n";
						last SWITCH;
                }
                if ($element eq "grid_data") {
                        #print "grid_data: ";
                        $tag = "grid_data";
                        last SWITCH;
                }
        }

 }

sub endElement {

      my( $parseinst, $element ) = @_;

 }

sub characterData {

      my( $parseinst, $data ) = @_;

 }

sub default {

      my( $parseinst, $data ) = @_;
        # do nothing, but stay quiet

 }
 
sub grid_keys {
  my $xmlref     = shift;
  my $dbug  = shift;
  my @grid_keys = ();

  #my @fields = keys %{$xmlref->{'grid_field'}}, "\n";
  foreach my $ff ( keys %{$xmlref->{'grid_field'}} ) {
    #(defined $ff->{'index'}) or next;
	#print keys  %{$xmlref->{'grid_field'}->{$ff}}, "\n";
    $xmlref->{'grid_field'}->{$ff}->{'value'} = [];
    push @grid_keys, $ff;
  }

  my $grid_data = $xmlref->{'grid_data'};
  my (@grid_data) = $grid_data =~ /([+-]?\d+\.?\d*)/g;
  my $ind =0;
  while (my @line_data = splice(@grid_data, 0, scalar @grid_keys)) {
    foreach my $key (@grid_keys) { 
      push @{$xmlref->{'grid_field'}->{$key}->{'value'}}, 
		$line_data[$xmlref->{'grid_field'}->{$key}->{'index'} - 1];
	}
  }

  foreach my $key (@grid_keys) { 
    my @sort_a = sort  @{$xmlref->{'grid_field'}->{$key}->{'value'}};
    print  $sort_a[0], ",", $sort_a[$#sort_a], "\n";
    $xmlref->{'grid_field'}->{$key}->{'min'} = $sort_a[0];
	$xmlref->{'grid_field'}->{$key}->{'max'} = $sort_a[$#sort_a];
  }

  return \@grid_keys;
}

sub mydate {

  my ($sec, $min, $hr, $day, $mon, $yr) = localtime();
  return sprintf('%02d/%02d/%4d %02d:%02d:%02d', 
		  $mon + 1, $day, $yr + 1900, $hr, $min, $sec);
}


1;


