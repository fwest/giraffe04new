package Facility;

# $Id: Facility.pm 519 2008-10-22 13:58:44Z klin $

##############################################################################
# 
# Terms and Conditions of Software Use
# ====================================
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Disclaimer of Earthquake Information
# ====================================
# 
# The data and maps provided through this system are preliminary data
# and are subject to revision. They are computer generated and may not
# have received human review or official approval. Inaccuracies in the
# data may be present because of instrument or computer
# malfunctions. Subsequent review may result in significant revisions to
# the data. All efforts have been made to provide accurate information,
# but reliance on, or interpretation of earthquake data from a single
# source is not advised. Data users are cautioned to consider carefully
# the provisional nature of the information before using it for
# decisions that concern personal or public safety or the conduct of
# business that involves substantial monetary or operational
# consequences.
# 
# Disclaimer of Software and its Capabilities
# ===========================================
# 
# This software is provided as an "as is" basis.  Attempts have been
# made to rid the program of software defects and bugs, however the
# U.S. Geological Survey (USGS) have no obligations to provide maintenance, 
# support, updates, enhancements or modifications. In no event shall USGS 
# be liable to any party for direct, indirect, special, incidental or 
# consequential damages, including lost profits, arising out of the use 
# of this software, its documentation, or data obtained though the use 
# of this software, even if USGS or have been advised of the
# possibility of such damage. By downloading, installing or using this
# program, the user acknowledges and understands the purpose and
# limitations of this software.
# 
# Contact Information
# ===================
# 
# Coordination of this effort is under the auspices of the USGS Advanced
# National Seismic System (ANSS) coordinated in Golden, Colorado, which
# functions as the clearing house for development, distribution,
# documentation, and support. For questions, comments, or reports of
# potential bugs regarding this software please contact wald@usgs.gov or
# klin@usgs.gov.  
#
#############################################################################

use strict;
use warnings;

use Data::Dumper;
use Getopt::Long;
use Text::CSV_XS;
use XML::Simple;

use FindBin;
use lib "$FindBin::Bin/lib";

require Exporter;
use vars qw( @ISA @EXPORT );
@ISA = qw( Exporter );
@EXPORT = qw( process_facility );

sub epr;
sub vpr;
sub vvpr;

my %options = (
    'insert'    => 0,
    'replace'   => 0,
    'skip'      => 0,
    'update'    => 0,
    'delete'    => 0,	
    'verbose'   => 0,
    'help'      => 0,
    'quote'     => '"',
    'separator' => ',',
    'limit=n'   => 100,
);

my $csv;
my $fh;

my %columns;        # field_name -> position
my %metrics;        # metric_name -> [ green_ix, yellow_ix, red_ix ]
my %attrs;          # attribute name -> position

# specify required columns, 1=always required, 2=not required for update
my %required = (
    'FACILITY_NAME'             => 2,
    'LAT'                       => 1,
    'LON'                       => 1,
);

# translate damage level to array index
my %damage_levels = (
    'GREEN'     => 0,
    'YELLOW'    => 1,
    'ORANGE'       => 2,
    'RED'       => 3
);
# map array index to damage level
my @damage_levels = (
    'GREEN',
    'YELLOW',
    'ORANGE',
    'RED'
);


my $sub_ins_upd;

my $mode;
use constant M_INSERT  => 1;
use constant M_REPLACE => 2;
use constant M_UPDATE  => 3;
use constant M_SKIP    => 4;
use constant M_DELETE    => 5;

$mode = M_REPLACE;      # default mode
$mode = M_INSERT   if $options{'insert'};
$mode = M_UPDATE   if $options{'update'};
$mode = M_SKIP     if $options{'skip'};
$mode = M_DELETE     if $options{'delete'};	
$options{'limit'} = $options{'limit'} ? $options{'limit'} : 200;

$csv = Text::CSV_XS->new({
        'quote_char'  => $options{'quote'},
        'escape_char' => $options{'quote'},
        'sep_char'    => $options{'separator'}
 });

    
	
sub process_facility {

	my ($download_dir, $content) = @_;

	my $file = "$download_dir/facility.csv";
	open (FH, "> $file");
	print FH $content;
	close(FH);
	
    $fh = new IO::File;
    unless ($fh->open($file, 'r')) {
        epr "cannot open $file\: $!";
        next;
    }
    vpr "Processing $file";
    my $xml = process();
    $fh->close;

	return 0 unless ($xml);
	
	$file = "$download_dir/facility.xml";
	$xml = XMLout( $xml, 'OutputFile' => $file);	

}
sub process {
    unless (process_header()) {
        print "file had errors, skipping";
        return;
    }
    my $err_cnt = 0;
    my $nrec = 0;
    my $nins = 0;
    my $nupd = 0;
    my $nrepl = 0;
    my $nskip = 0;
	my ($ext_id, $type);

	my $xml = {};
	my @facilities;
    while (!$fh->eof) {

        my $colp = $csv->getline($fh);
		
		my %facility =  &$sub_ins_upd($colp);
		next unless (defined $facility{'FACILITY_NAME'});
        # TODO error handling
        if ($options{'limit'} && $nrec++ >= $options{'limit'}) {
            print "Limit reached, skipping";
			if (scalar @facilities) {
				$xml->{'FACILITY'} = \@facilities;
				return $xml;
			} else {
				return;
			}
        }
        
        if (%metrics) {
            while (my ($metric, $valp) = each %metrics) {
				my $str = "$metric, ". $valp->[0] . ", ";
				my $level;
				my $lo;
				foreach my $ix (0 .. 3) {
					next unless $valp->[$ix];	# col not present in input file
					my $val = $colp->[$valp->[$ix]];
					next unless defined $val;	# treat blank like missing
					if (defined $level) {
						$str .= "$ix , $level, $lo, $val";
						$facility{'DAMAGE'}->{$level}->{'METRIC'} = $metric;
						$facility{'DAMAGE'}->{$level}->{'LOW_LIMIT'} = $lo;
						$facility{'DAMAGE'}->{$level}->{'HIGH_LIMIT'} = $val;
					}
					$lo = $val;
					$level = $damage_levels[$ix]
				}
				if (defined $level) {
					$str .= ", $level, $lo, 99999";
					$facility{'DAMAGE'}->{$level}->{'METRIC'} = $metric;
					$facility{'DAMAGE'}->{$level}->{'LOW_LIMIT'} = $lo;
					$facility{'DAMAGE'}->{$level}->{'HIGH_LIMIT'} = 99999;
				}
            }
		}
		push @facilities, \%facility;
    }
	
	if (scalar @facilities) {
		$xml->{'FACILITY'} = \@facilities;
		return $xml;
	} 
	
	return 0;
}

# Read and process header, building data structures
# Returns 1 if header is ok, 0 for problems
sub process_header {
    my $err_cnt = 0;
    undef %columns;
    undef %metrics;
    undef %attrs;

    my $header = $fh->getline;
    return 1 unless $header;      # empty file not an error
    
    # parse header line
    vvpr $header;
    unless ($csv->parse($header)) {
        print "CSV header parse error on field '", $csv->error_input, "'";
        return 0;
    }

    my $ix = 0;         # field index

    # uppercase and trim header field names
    my @fields = map { uc $_ } $csv->fields;

    foreach (@fields) {
        s/^\s+//;
        s/\s+$//;
    }
    # Field name is one of:
    #   METRIC:<metric-name>:<metric-level>
    #   GROUP:<group-name>
    #   <facility-column-name>
    foreach my $field (@fields) {
        if ($field =~ /^METRIC\s*:\s*(\w+)\s*:\s*(\w+)/) {
            vvpr "$ix\: METRIC: $1 LEVEL: $2";
            my $lx = $damage_levels{$2};
            if (defined $lx) {
                # TODO check for unknown metric names
                $metrics{$1}->[$lx] = $ix;
            } else {
                epr "metric level '$2' must be one of RED, YELLOW, or GREEN";
                $err_cnt++;
            }
        } elsif ($field =~ /^ATTR(?:IBUTE)?\s*:\s*(.*)/) {
            vvpr "$ix\: ATTR: $1";
            $attrs{$1} = $ix;
        } else {
            vvpr "$ix\: COLUMN: $field";
            # TODO check for unknown columns (either here or later on)
            $columns{$field} = $ix;
        }
        $ix++;
    }

    # check for required fields
    while (my ($req, $req_type) = each %required) {
        # relax required fields for update (only PK is mandatory)
        next if $req_type == 2 and $mode == M_UPDATE;
        unless (defined $columns{$req}) {
            epr "required field $req is missing";
            $err_cnt++;
        }
    }

    return 0 if $err_cnt;

    # dynamically create a sub that takes the input array of fields and
    # returns a new list with just those fields that go into the facility
    # insert/update statement, in the proper order
    my @keys = sort keys %columns;
    my $sub = "sub { (" .
        join(',', (map {  q{'}.$_ .q{' => }. q{$_[0]->[} . $columns{$_} . q{]} } (@keys))) .
        ') }';
    
    $sub_ins_upd = eval $sub;

    return 1;

}


sub vpr {
    if ($options{'verbose'} >= 1) {
        print @_, "\n";
    }
}

sub vvpr {
    if ($options{'verbose'} >= 2) {
        print @_, "\n";
    }
}

sub epr {
    print STDERR @_, "\n";
}

1;