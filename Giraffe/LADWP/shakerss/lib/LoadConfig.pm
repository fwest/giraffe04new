package LoadConfig;
#######################################################################
# LoadConfig Package
#######################################################################


BEGIN {
#       @(#)LoadConfig.pm	1.7     07/15/03     TriNet

use strict;
use Carp;

#######################################################################
# LoadConfig provides an interface to reading configuration files with
# the structure:
#
#	statement : value
#
# where the first ':' splits the two strings (which may contain
# whitespace.
#
# The following functions are provided:
#
# new("filename", <ref to array of directories> [, $errfunc])
# 
#	Initializes a new LoadConfig object. Returns 'undef' on error.
#	Arguments:
#
#	"filename" is the name of the config file; the module will look
#                  for this file in the directories specified by the list
#                  and in the order that they appear, using the first
#                  instance of the file that it finds
#
#	"$errfunc" is a reference to an error reporting function which 
#	           takes a string argument ('carp' is used by default)  
#	           The error function will be passed a string with no \n 
#		   on the end, and is expected to add one if appropriate 
#		   (which it usually is).
#
# LoadConfig::parse( $func_hash_ref [, $opt_arg] )
#
#	Generic config file parser.  The argument "$func_hash_ref" is a 
#	reference to a hash of (statement => $func_ref) pairs, where 
#	'statement' is the name of the statement in the config file, and 
#	'$func_ref' is a reference to the function to call when that 
#	statement is encountered.  The 'value' part of the configuration 
#	line will be passed to the user function as its first argument; if
#	'$opt_arg' is supplied, it will be passed as the second argument,
#	otherwise 'undef' will be passed as the second argument.  These 
#	user-defined functions should return "undef" on success, and an 
#	arbitrarily complex error message on failure.  The parser will not 
#	quit on error (unless the error function supplied to "LoadConfig::new" 
#	exits when called), but will try to finish parsing the file and 
#	return the number of errors encountered.
#
#	The parser allows multi-line "value" segments that are continued 
#	by ending the line with a "&&".  Example:
#
#		count : one two     &&
#			three four
#
#	Is equivalent to:
#
#		count : one two three four
#
#	If anything other than a carriage return (except white space)
#	follows the "&&", the "&&" will be considered part of a single-
#	line "value" statement and will be passed to the statement 
#	handler as part of the argument.
#
# Custom handling of config files is achieved through the use of the
# following access methods:
#
# LoadConfig::nextsv()
#
#	Returns a two-element list consisting of the statement and value
#	of the next non-comment, non-empty line in the config file.  Returns
#	(undef, undef) on error.  The strings returned have their leading
#	and trailing whitespace stripped.
#
# LoadConfig::nextsv_raw()
#
#	Returns a two-element list consisting of the statement and value
#	of the next non-comment, non-empty line in the config file.  Returns
#	(undef, undef) on error.
#
# LoadConfig::nextline()
#
#	Returns the next non-comment, non-empty line in the config file.  
#	Returns undef on error.  The strings returned have their leading
#	and trailing whitespace stripped.
#
# LoadConfig::nextline_raw()
#
#	Returns the next non-comment, non-empty line in the config file.  
#	Returns undef on error.
#
# LoadConfig::linenum()
#
#	Returns the line number (in the config file) of the current line.
#	Note that this includes comments and blank lines.
#
# LoadConfig::more()
#
#	Returns true (1) if there are more lines in the config, otherwise
#	returns false (0)
#######################################################################

#######################################################################
# The 'self' object contains the following elements:
#
# FILE    => Config file
# PFUNCS  => Reference to a hash of user-defined functions to call, 
#            keyed by statement name (if the parse() function is used)
# EFUNC   => Reference to the error function
# CFGARR  => Array of configuration lines
# LINEARR => Line numbers to corresponding lines in CFGARR
# INDEX   => Current index
#######################################################################

sub new {

  my $self  = {};
  my $class = shift;
  my $file  = shift;
  my $aref  = shift;
  $self->{EFUNC} = shift || \&carp;

  if (not defined $file) {
    &{$self->{EFUNC}}("ERROR in LoadConfig::new: must specify config file");
    return undef;
  }
  if (!defined $aref) {
    &{$self->{EFUNC}}("ERROR in LoadConfig::new: no search path given");
    return undef;
  }
  #-----------------------------------------------------------------------
  # Look for the config file in the specified places
  #-----------------------------------------------------------------------
  foreach my $dir ( @$aref ) {
    my $path = "$dir/$file";
    if (-e "$path") {
      $self->{FILE} = $path;
      last;
    }
  }
  if (not defined $self->{FILE}) {
    &{$self->{EFUNC}}("LoadConfig::new ERROR: can't find config file "
		    . "'$file' in\n" 
		    . join(",\n", @$aref));
    return undef;
  }

  unless (open(CONFIG, $self->{FILE})) {
    &{$self->{EFUNC}}("LoadConfig::new ERROR: can't open config file "
		    . "$self->{FILE}");
    return undef;
  }
  $self->{CFGARR}  = [];
  $self->{LINEARR} = [];
  $self->{INDEX}   = -1;

  #----------------------------------------------------------------------
  # Read the config file; we used to do this:
  #
  #  my @cfgarr = grep {!/^#/ and !/^\n/} <CONFIG>;
  #
  # But we wanted to count lines, so now we use the less clever loop
  # below
  #----------------------------------------------------------------------
  while (<CONFIG>) {
    next if (/^#/ or /^\n/);
    push @{$self->{CFGARR}}, $_;
    push @{$self->{LINEARR}}, $.;
  }
  close CONFIG;
  chomp @{$self->{CFGARR}};

  return bless $self, $class;
}

###########################################################################
# Generic parsing function.  See documentation above.
###########################################################################
sub parse {

  my $self      = shift;
  my $erf       = $self->{EFUNC};
  my $configerr = 0;

  if (!defined ($self->{PFUNCS} = shift)) {
    &$erf("ERROR in LoadConfig::parse: must specify function hash");
    return undef;
  }
  my $opt_arg   = shift || undef;	# kind of redundant, but explicit

  while ($self->more) {
    my ($statement, $value) = $self->nextsv;
    my $lineno = $self->linenum;
    #----------------------------------------------------------------------
    # Check for a valid line in the config file -- we should really quit
    # if this test fails, forcing a properly written config file, but
    # we don't want to kill a functioning program just because of a 
    # reconfig, so we do the best we can and report errors at the end.
    #----------------------------------------------------------------------
    if (!defined $statement || !defined $value) {
      #&$erf("unusable line (line $lineno) in config file:\n\t"
	#  . "line must be of the 'statement : value' form");
      $configerr++;
      next;
    }

    #----------------------------------------------------------------------
    # Special handling for multi-line statements
    #----------------------------------------------------------------------
    while ($value =~ /&&$/) {
      $value =~ s/\s&&$//;
      if (!$self->more) {
        &$erf("end of file reached on continued line");
	$configerr++;
	last;
      }
      #--------------------------------------------------------------------
      # Basically, we just glue what follows onto the line
      #--------------------------------------------------------------------
      $value .= " " . $self->nextline;
    }

    #----------------------------------------------------------------------
    # Get the handler function that corresponds to the statement
    #----------------------------------------------------------------------
    my $fref = $self->{PFUNCS}{$statement} || $self->{PFUNCS}{'DEFAULT'};
    if (!defined $fref) {
      #&$erf("non-applicable statement '$statement' on line $lineno of config file\n");
      $configerr++;
      next;
    }

    #----------------------------------------------------------------------
    # Call the appropriate handler
    #----------------------------------------------------------------------
    if (defined (my $errmsg = &$fref($value, $opt_arg))) {
      #-----------------------------------------------------------------------
      # Strip any trailing \n from the errmsg (because the error function
      # is presumed to add one); strip multiple \n's and stick a tab in
      # front of each line
      #-----------------------------------------------------------------------
      $errmsg =~ s/\n+$//;
      $errmsg =~ s/\n+/\n/g;
      $errmsg =~ s/\n/\n\t/g;
      &$erf("CONFIG ERROR: '$statement' statement at line $lineno in "
	  . "$self->{FILE}:\n"
	  . "\t$errmsg");
      $configerr++; 
    }
  }
  return $configerr;
}

#######################################################################
# Returns the statement/value pair of the next line in the config file
# -- no whitespace stripping
#######################################################################
sub nextsv_raw {

  my $self = shift;

  if (!$self->more) {
    &{$self->{EFUNC}}("LoadConfig::nextsv_raw ERROR: no more lines");
    return (undef, undef);
  }
  return split(":", $self->{CFGARR}[ ++$self->{INDEX} ], 2);
}

#######################################################################
# Returns the statement/value pair of the next line in the config file
# with whitespace stripped
#######################################################################
sub nextsv {

  my $self    = shift;
  my ($s, $v) = $self->nextsv_raw();

  return (undef, undef) if (!defined $s and !defined $v);

  if (!defined $s or !defined $v) {
    #&{$self->{EFUNC}}("LoadConfig::nextsv skipped non-relevent line in input");
    return (undef, undef);
  }

  #----------------------------------------------------------------------
  # Strip whitespace before returning
  #----------------------------------------------------------------------
  chomp $s;
  chomp $v;
  return ( $s =~ /^\s*(.+?)\s*$/, $v =~ /^\s*(.+?)\s*$/ );
}

#######################################################################
# Returns the next line in the config file -- no whitespace stripping 
#######################################################################
sub nextline_raw {

  my $self = shift;

  if (!$self->more) {
    &{$self->{EFUNC}}("LoadConfig::nextline_raw ERROR: no more lines");
    return undef;
  }
  return $self->{CFGARR}[ ++$self->{INDEX} ];
}

#######################################################################
# Returns the next line in the config file
#######################################################################
sub nextline {

  my $self = shift;
  my $l    = $self->nextline_raw();

  return undef if !defined $l;

  chomp $l;
  return ($l =~ /^\s*(.+?)\s*$/)[0];
}

#######################################################################
# Returns the line number (in the config file) of the current line
#######################################################################
sub linenum {

  my $self = shift;

  return $self->{LINEARR}[$self->{INDEX}];
}

#######################################################################
# Returns true (1) if there are more lines in the config, otherwise
# returns false (0)
#######################################################################
sub more {

  my $self = shift;

  return 1 if ($self->{INDEX} + 1 < @{$self->{CFGARR}});
  return 0;
}
}

1;