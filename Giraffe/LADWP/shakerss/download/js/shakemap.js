// Add Map overlay
// A Rectangle is a simple overlay that outlines a lat/lng bounds on the
// map. It has a border of the given weight and color and can optionally
// have a semi-transparent background color.
function Rectangle(bounds, image, opt_weight, opt_color, opt_opacity) {
  this.bounds_ = bounds;
  this.weight_ = opt_weight || 0;
  this.color_ = opt_color || "#000";
  this.image_ = image || "";
  this.opacity_ = opt_opacity || 50;
}

Rectangle.prototype = new GOverlay();

// Creates the DIV representing this rectangle.
Rectangle.prototype.initialize = function(map) {
  // Create the DIV representing our rectangle
  var div = document.createElement("div");
  div.style.border = this.weight_ + "px solid " + this.color_;
  div.style.position = "absolute";
  
  //image for overlay
  if(this.image_ != "")
  {
		  //Setup image
		  var image = document.createElement("img");
		  image.src = this.image_ ;
		  image.style.width = "100%";
		  image.style.height = "100%";
		  
		  //Transparency for IE
		  if (navigator.appName.indexOf("Microsoft")!=-1&&parseInt(navigator.appVersion)>=4)
		  //{image.style.filter =  "alpha(opacity="+this.opacity_+")";}	  
		  {image.style.filter =  "progid:DXImageTransform.Microsoft.Alpha(opacity="+this.opacity_+")";}	  
		  
		  //Mozilla transparency
		  if (navigator.appName.indexOf("Netscape")!=-1&&parseInt(navigator.appVersion)>=5)
		  {image.style.MozOpacity= this.opacity_ / 100;}
		  
		  image.style.opacity= this.opacity_ / 100;
		  //Add to div	
		  div.appendChild(image);
  }	
  // Our rectangle is flat against the map, so we add our selves to the
  // MAP_PANE pane, which is at the same z-index as the map itself (i.e.,
  // below the marker shadows)
  map.getPane(G_MAP_MAP_PANE).appendChild(div);

  this.map_ = map;
  this.div_ = div;
}

// Remove the main DIV from the map pane
Rectangle.prototype.remove = function() {
  this.div_.parentNode.removeChild(this.div_);
}

// Copy our data to a new Rectangle
Rectangle.prototype.copy = function() {
  return new Rectangle(this.bounds_,this.image_,this.weight_, this.color_, this.opacity_);
}

// Redraw the rectangle based on the current projection and zoom level
Rectangle.prototype.redraw = function(force) {
  // We only need to redraw if the coordinate system has changed
  if (!force) return;

  // Calculate the DIV coordinates of two opposite corners of our bounds to
  // get the size and position of our rectangle
  var c1 = this.map_.fromLatLngToDivPixel(this.bounds_.getSouthWest());
  var c2 = this.map_.fromLatLngToDivPixel(this.bounds_.getNorthEast());

  // Now position our DIV based on the DIV coordinates of our bounds
  this.div_.style.width = Math.abs(c2.x - c1.x) + "px";
  this.div_.style.height = Math.abs(c2.y - c1.y) + "px";
  this.div_.style.left = (Math.min(c2.x, c1.x) - this.weight_) + "px";
  this.div_.style.top = (Math.min(c2.y, c1.y) - this.weight_) + "px";
}

