
 ' Delete directories older than 30 days
basedir = "c:\giraffe\ladwp\"
 
' ############################################################################
'
' Search for any directories more than 30 days old.  If found, delete the
' directory to conserve space
'
Const intDaysOld = 3
Dim objFSO : Set objFSO = CreateObject("Scripting.FileSystemObject")
Dim objFolder : Set objFolder = objFSO.GetFolder(basedir&"shakerss\download")
Dim objSubFolder
For Each objSubFolder In objFolder.SubFolders
     If objSubFolder.DateLastModified < DateValue(Now() - intDaysOld) Then
       '  WScript.Echo objSubFolder.DateLastModified
         objSubFolder.Delete True
    End If
Next 

' ###############################################################################
'  INITIAL SETUP 
'  Set up intial files, including connection to the application and GIRAFFE document
'
thedir = WScript.Arguments(0) 
Set objFSO = CreateObject("Scripting.FileSystemObject")
'Set objFile = objFSO.OpenTextFile(basedir&"shakerss\download\recent_event.txt", 1)
'thedir = objFile.ReadLine
 
Set ManApp = CreateObject("Manifold.Application")
Set theDoc = ManApp.NewDocument(basedir&"shakerss\download\manifold_giraffe.map",false)

' Set up the initial list of component sets within the application and create a
' new blank query

Set theCompSet = theDoc.ComponentSet
Set theQry = theDoc.NewQuery("Query")

 
'#####################################################################################
'
' We are going to have to read the XML file downloaded by ShakeCast Lite, so this
' prepares the files.
'
Set xmlDoc = CreateObject("Msxml2.DOMDocument")
Set http=createObject("Microsoft.xmlhttp")
theDoc.NewComments("comments")
Set comments = theCompSet("comments")

' Create and run a query to CREATE DRAWING, then assign the
' latitude / longitude coordinate system to the drawing

theQry.Text = "create drawing gridpts (pgv double)"
theQry.Run
theCompSet("gridpts").CoordinateSystem = ManApp.NewCoordinateSystem("Latitude / Longitude")

' Load the XML document into an array

xmlDoc.Load(basedir&"shakerss\download\"&thedir&"\grid.xml")
ElemList = xmlDoc.getElementsByTagName("grid_data").item(0).text 
GridArray = Split(ElemList,vblf)


' For each line in the array, get the X,Y, and Z values and
' insert them into the drawing we created

for each gridline in GridArray
	gridrecord = Split(gridline," ")
	comments.addtext gridrecord(0)&" "&gridrecord(1)&" "&gridrecord(4)&vblf
	theQry.Text = "INSERT INTO gridpts ([geom (i)], pgv) VALUES (NewpointLatLon("&gridrecord(0)&","&gridrecord(1)&"),"&gridrecord(4)&")" 
	theQry.Run
Next

Set ShpExp = theDoc.NewExport("SHP")
ShpExp.Export theCompSet("gridpts"), basedir&"shakerss\download\"&thedir&"\xyz.shp", promptNone


'#####################################################################################

'  Import the PGV points as a new drawing, change the name, and project it to 
'  the California coordinate system which is the same as PGA surf

Set NewImp = theDoc.NewImport("shp")
NewImp.ConvertPolicy = convertall



NewImp.Import basedir&"shakerss\download\"&thedir&"\xyz.shp",promptnone 
Set theDrw = theCompSet.Item(theCompSet.Count - 2)

theDrw.ProjectTo(theCompSet.Item("pgasurf").CoordinateSystem)

'##############################################################################
'##############################################################################
' RASTER INTERPOLATION

' Get the pixels from the raster surface, and then interpolate the PGV values
Set theSurfPS = theCompSet.Item("pgasurf").PixelSet
theSurfPS.TransformWith("Gravity([xyz Drawing],PGV,-1)")

'##############################################################################



'##############################################################################
' CALCULATE MEAN PRESSURE AT DEMAND NODES

theQry.text = "			SELECT epa_junctions.*,meanpressure,z1"
theQry.text = theQry.text & "				INTO DemandNode"
theQry.text = theQry.text & "					FROM epa_junctions, "
theQry.text = theQry.text & "					(SELECT avg(node_pressure) as MeanPressure, zone as z1 "
theQry.text = theQry.text & "					FROM (SELECT epa_junctions.*,node_pressure.* "
theQry.text = theQry.text & "							  FROM [epa_junctions],[Node_Pressure] "
theQry.text = theQry.text & "				              WHERE epa_junctions.[id2] = node_pressure.id "
theQry.text = theQry.text & "							  ) "
theQry.text = theQry.text & "							  GROUP BY zone)  "
theQry.text = theQry.text & "				WHERE [epa_junctions].[ZONE] = z1 AND "
theQry.text = theQry.text & "				epa_junctions.[DEMAND1] > 0 "
theQry.Run

' ###############################################################################

' ###############################################################################
' CREATE DISTRIBUTIOND database
'  This subroutine creates the DistributionD file by interpolating the PGV values
'  for the individual demand nodes.  
'
theQry.text = 				" SELECT ID, Ave_PRESSURE, PGV_Soils, [Soil_Category_NAD83 Drawing].[VSCAT], 0 G_RR "
theQry.text = theQry.text & " INTO DistributionD "	   
theQry.text = theQry.text & " FROM "
theQry.text = theQry.text & "	(SELECT [demandnode].[id2] as ID, [demandnode].[meanpressure] as Ave_PRESSURE, "
theQry.text = theQry.text & "			 		[Soil_Category_NAD83 Drawing].[VSCAT], "
theQry.text = theQry.text & "			 Height(pgasurf,[demandnode].[Geom (i)]) as PGV_Soils "
theQry.text = theQry.text & "	 FROM demandnode, [Soil_Category_NAD83 Drawing] "
theQry.text = theQry.text & "	WHERE touches(demandnode.[geom (i)],[Soil_Category_NAD83 Drawing].id)) "
theQry.Run

' ###############################################################################
' ###############################################################################
' CORRECT PGV_SOILS IN DISTRIBUTIOND
'  This subroutine corrects the interpolated PGV values of the demand nodes
'  using the appropriate soil correction factors.  
'
theQry.text = 		" UPDATE DistributionD "
theQry.text = theQry.text & " SET PGV_Soils = "
theQry.text = theQry.text & " CASE "
theQry.text = theQry.text & " 	WHEN VSCAT = ""A"" THEN pgv_soils * .8 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""B"" THEN pgv_soils * 1 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""C"" AND pgv_soils < 14 THEN pgv_soils * 1.7 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""C"" AND pgv_soils BETWEEN 14 AND 23.67 THEN pgv_soils * 1.6 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""C"" AND pgv_soils BETWEEN 23.67 AND 33.13 THEN pgv_soils * 1.5 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""C"" AND pgv_soils BETWEEN 33.13 AND 42.5 THEN pgv_soils * 1.4 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""C"" AND pgv_soils > 42.5 THEN pgv_soils * 1.3 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""D"" AND pgv_soils < 14 THEN pgv_soils * 2.4 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""D"" AND pgv_soils BETWEEN 14 AND 23.67 THEN pgv_soils * 2 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""D"" AND pgv_soils BETWEEN 23.67 AND 33.13 THEN pgv_soils * 1.8 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""D"" AND pgv_soils BETWEEN 33.13 AND 42.5 THEN pgv_soils * 1.6 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""D"" AND pgv_soils > 42.5 THEN pgv_soils * 1.6 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""E"" AND pgv_soils < 14 THEN pgv_soils * 3.5 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""E"" AND pgv_soils BETWEEN 14 AND 23.67 THEN pgv_soils * 3.2 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""E"" AND pgv_soils BETWEEN 23.67 AND 33.13 THEN pgv_soils * 2.8 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""E"" AND pgv_soils BETWEEN 33.13 AND 42.5 THEN pgv_soils * 2.4 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""E"" AND pgv_soils > 42.5 THEN pgv_soils * 2.4 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""AB"" THEN pgv_soils * .9 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""BC"" THEN pgv_soils * 1 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""CD"" AND pgv_soils < 14 THEN pgv_soils * 2.05 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""CD"" AND pgv_soils BETWEEN 14 AND 23.67 THEN pgv_soils * 1.8 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""CD"" AND pgv_soils BETWEEN 23.67 AND 33.13 THEN pgv_soils * 1.65 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""CD"" AND pgv_soils BETWEEN 33.13 AND 42.5 THEN pgv_soils * 1.5 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""CD"" AND pgv_soils > 42.5 THEN pgv_soils * 1.4 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""DE"" AND pgv_soils < 14 THEN pgv_soils * 2.95 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""DE"" AND pgv_soils BETWEEN 14 AND 23.67 THEN pgv_soils * 2.6 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""DE"" AND pgv_soils BETWEEN 23.67 AND 33.13 THEN pgv_soils * 2.3 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""DE"" AND pgv_soils BETWEEN 33.13 AND 42.5 THEN pgv_soils * 2 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""DE"" AND pgv_soils > 42.5 THEN pgv_soils * 1.95 "
theQry.text = theQry.text & " 	END "
theQry.Run

' ###############################################################################

' ###############################################################################
' CALCULATE G_RR IN DISTRIBUTIOND
'  This subroutine calculates the repair rate of the demand nodes using the PGV vs. RR
'  regression formula for material type 'CI'.
'
theQry.text = 	" UPDATE DistributionD "
theQry.text = theQry.text & " 	SET G_RR = "
theQry.text = theQry.text & " 		Exp(1.21 * LOG(PGV_Soils) - 6.81) "
theQry.Run

' ###############################################################################

' ###############################################################################
' PREPARE DISTRIBUTIOND FOR EXPORT
'  This subroutine prepares the DistributionD file for export as a text file to be 
'  inputted in GIRAFFE.
'
theQry.text = 	" SELECT [DistributionD].[ID], [DistributionD].[G_RR], DistributionD.[Ave_PRESSURE] "
theQry.text = theQry.text & " 	INTO NodePressureInput "
theQry.text = theQry.text & " 	FROM DistributionD "
theQry.Run

' #################################################################################################

' #################################################################################################
' CREATE SM_PGV_Pipes database
'  This subroutine computes the PGV for pipes using Kriging.  It interpolates the PGV and
'  associates it with the pipe segment
'
theQry.text = 				"SELECT [Soil_Category_NAD83 Drawing].[VSCAT],[pgv_pipes].id,pgv_pipes.pgv, "
theQry.text = theQry.text & "	   pipelength,pgv_pipes.material,pgv_pipes.pipeid, 0 RR"
theQry.text = theQry.text & " INTO SM_PGV_Pipes "
theQry.text = theQry.text & " FROM "
theQry.text = theQry.text & "	(SELECT ([epa_pipes].[id]), [Epa_pipes].[Length (I)] as pipelength,[Epa_pipes].[MATERIAL], "
theQry.text = theQry.text & "			[Epa_pipes].[ID 2] as pipeid,[Geom (I)], "
theQry.text = theQry.text & "			Height(pgasurf,[epa_pipes].[Geom (I)]) as PGV "
theQry.text = theQry.text & "	FROM epa_pipes) as pgv_pipes, [Soil_Category_NAD83 Drawing] "
theQry.text = theQry.text & "	WHERE touches(pgv_pipes.id,[Soil_Category_NAD83 Drawing].id) "
theQry.Run

' ###############################################################################

' ###############################################################################
' CORRECT PGV IN SM_PGV_Pipes
'   This subroutine corrects the interpolated PGV values of the pipes
'   using the appropriate soil correction factors.  
'
theQry.text = 	" UPDATE [SM_PGV_Pipes] "
theQry.text = theQry.text & " SET PGV = "
theQry.text = theQry.text & " CASE "
theQry.text = theQry.text & " 	WHEN VSCAT = ""A"" THEN pgv*.8 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""B"" THEN pgv*1 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""C"" AND pgv < 14 THEN pgv * 1.7 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""C"" AND pgv BETWEEN 14 AND 23.67 THEN pgv * 1.6 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""C"" AND pgv BETWEEN 23.67 AND 33.13 THEN pgv * 1.5 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""C"" AND pgv BETWEEN 33.13 AND 42.5 THEN pgv * 1.4 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""C"" AND pgv > 42.5 THEN pgv * 1.3 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""D"" AND pgv < 14 THEN pgv * 2.4 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""D"" AND pgv BETWEEN 14 AND 23.67 THEN pgv * 2 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""D"" AND pgv BETWEEN 23.67 AND 33.13 THEN pgv * 1.8 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""D"" AND pgv BETWEEN 33.13 AND 42.5 THEN pgv * 1.6 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""D"" AND pgv > 42.5 THEN pgv * 1.6 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""E"" AND pgv < 14 THEN pgv * 3.5 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""E"" AND pgv BETWEEN 14 AND 23.67 THEN pgv * 3.2 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""E"" AND pgv BETWEEN 23.67 AND 33.13 THEN pgv * 2.8 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""E"" AND pgv BETWEEN 33.13 AND 42.5 THEN pgv * 2.4 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""E"" AND pgv > 42.5 THEN pgv * 2.4 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""AB"" THEN pgv * .9 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""BC"" THEN pgv * 1 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""CD"" AND pgv < 14 THEN pgv * 2.05 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""CD"" AND pgv BETWEEN 14 AND 23.67 THEN pgv * 1.8 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""CD"" AND pgv BETWEEN 23.67 AND 33.13 THEN pgv * 1.65 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""CD"" AND pgv BETWEEN 33.13 AND 42.5 THEN pgv * 1.5 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""CD"" AND pgv > 42.5 THEN pgv * 1.4 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""DE"" AND pgv < 14 THEN pgv * 2.95 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""DE"" AND pgv BETWEEN 14 AND 23.67 THEN pgv * 2.6 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""DE"" AND pgv BETWEEN 23.67 AND 33.13 THEN pgv * 2.3 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""DE"" AND pgv BETWEEN 33.13 AND 42.5 THEN pgv * 2 "
theQry.text = theQry.text & " 	WHEN VSCAT = ""DE"" AND pgv > 42.5 THEN pgv * 1.95 "
theQry.text = theQry.text & " 	END "
theQry.Run

' ###############################################################################

' ###############################################################################
' UPDATE MATERIAL IN SM_PGV_PIPES
'  This subroutine updates the material type of each pipe segment according to the 5
'  material types used by LADWP.
'
theQry.text = 		" UPDATE SM_PGV_Pipes "
theQry.text = theQry.text & " SET MATERIAL = "
theQry.text = theQry.text & " CASE [SM_PGV_Pipes].[MATERIAL] "
theQry.text = theQry.text & " 	WHEN ""COP"" then ""CON"" " 
theQry.text = theQry.text & " 	WHEN """" THEN ""N/A"" "
theQry.text = theQry.text & " 	WHEN ""AC"" then ""N/A"" " 
theQry.text = theQry.text & " 	WHEN ""RIV"" THEN ""RS"" "
theQry.text = theQry.text & " 	WHEN ""B&S WS"" THEN ""STL"" "
theQry.text = theQry.text & " 	WHEN ""MANN"" THEN ""STL"" "
theQry.text = theQry.text & " 	WHEN ""MATH"" THEN ""STL"" "
theQry.text = theQry.text & " 	WHEN ""Molox Bell & Ball Joint"" then ""STL"" "
theQry.text = theQry.text & " 	WHEN ""ST"" THEN ""STL"" "
theQry.text = theQry.text & " 	WHEN ""STD"" THEN ""STL"" "
theQry.text = theQry.text & " 	WHEN ""steel"" then ""STL"" "
theQry.text = theQry.text & " 	WHEN ""Steel"" then ""STL"" "
theQry.text = theQry.text & " 	WHEN ""STL GALV"" THEN ""STL"" "
theQry.text = theQry.text & " 	WHEN ""VICT"" THEN ""STL"" "
theQry.text = theQry.text & " 	WHEN ""WCJ"" THEN ""STL"" "
theQry.text = theQry.text & " 	WHEN ""WRG"" THEN ""STL"" "
theQry.text = theQry.text & " 	WHEN ""WS"" THEN ""STL"" "
theQry.text = theQry.text & " 	WHEN ""WSJ"" THEN ""STL"" "
theQry.text = theQry.text & " 	WHEN ""WWJ"" THEN ""STL"" "
theQry.text = theQry.text & " ELSE [MATERIAL] "
theQry.text = theQry.text & " END "
theQry.Run

' ###############################################################################

' ###############################################################################
' CALCULATE RR IN SM_PGV_PIPES
'  This subroutine calculates the repair rate for each pipe using the appropriate
'  RR vs. PGV regression formulas.
'
theQry.text = 		"UPDATE SM_PGV_Pipes"
theQry.text = theQry.text & " SET RR = "
theQry.text = theQry.text & " CASE [SM_PGV_Pipes].[material]"
theQry.text = theQry.text & "	WHEN ""CI"" "
theQry.text = theQry.text & "		THEN exp(1.21 * log(PGV) - 6.81)"
theQry.text = theQry.text & " 	WHEN ""DI""  "
theQry.text = theQry.text & "		THEN exp(1.84 * log(PGV) - 9.4)"
theQry.text = theQry.text & "	WHEN ""CON"" "
theQry.text = theQry.text & "		THEN exp(2.59 * log(PGV) - 12.11) "
theQry.text = theQry.text & "	WHEN ""RS"" "
theQry.text = theQry.text & "		THEN exp(1.41 * log(PGV) - 8.19) "
theQry.text = theQry.text & "	WHEN ""STL"" "
theQry.text = theQry.text & "		THEN exp(2.59 * log(PGV) - 14.16) "
theQry.text = theQry.text & "	WHEN ""N/A"" "
theQry.text = theQry.text & "		THEN ((Exp(1.21 * LOG(PGV) - 6.81)) + (Exp(1.84 * Log(PGV) - 9.4)) + "
theQry.text = theQry.text & "			(Exp(2.59 * Log(PGV) - 12.11)) + (Exp(1.41 * Log(PGV) - 8.19)) + "
theQry.text = theQry.text & "			(Exp(2.59 * Log(PGV) - 14.16)))/5 "
theQry.text = theQry.text & "	END "
theQry.Run

' ###############################################################################

' ###############################################################################
' PREPARE SM_PGV_PIPES FOR EXPORT
'  This subroutine prepares the SM_PGV_Pipes file for export as a text file to be 
'  inputted in GIRAFFE.
'
theQry.text = 			" SELECT PipeID, [Length], RR, Material "
theQry.text = theQry.text & " 	INTO RRInput "
theQry.text = theQry.text & " 	FROM "
theQry.text = theQry.text & " 		(SELECT [SM_PGV_Pipes].[pipeid] as PipeID,First([MATERIAL]) "
theQry.text = theQry.text & " 			AS Material,Avg([RR]) as RR, Sum([pipelength])/1000 as [Length] "
theQry.text = theQry.text & " 		From [SM_PGV_Pipes] "
theQry.text = theQry.text & " 		Group by [pipeid]) "
theQry.Run

' #########################################################################################################

' #######################################################################################################
' EXPORT THE DATA AS CSV FILES FOR USE IN GIRAFFE
Set thePipeComp = theCompSet.Item("RRInput")
Set DistributionD = theCompSet.Item("NodePressureInput")
Set XLExp = theDoc.NewExport("csv")
XLExp.Export thePipeComp,basedir&"shakerss\download\"&thedir&"\RRInput.inp",PromptNone 
XLExp.Export DistributionD,basedir&"shakerss\download\"&thedir&"\NodePressureInput.inp",PromptNone 

' Write out the XML file...
theXML = "<?xml version=""1.0""?> "&vbcrlf&_
"<SimulationParameters xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" ProjectName=""AutoSimMCFixed"""&vbcrlf&_
"OutputPath=""c:\giraffe\AUTORUNOUTPUT"""&vbcrlf&_
"SystemFile=""c:\giraffe\ladwp\shakerss\download\giraffe2007.inp"""&vbcrlf&_
"MinimumPressureToEliminate=""-5"""&vbcrlf&_
"NumberOfHours=""24"""&vbcrlf&_
"NumberOfStepsPerHour=""24"""&vbcrlf&_
"RandomSeed=""10"""&vbcrlf&_
"NumberOfSimulations=""1"""&vbcrlf&_
"Category=""MonteFixed"""&vbcrlf&_
"PipeRepairRateFile="""&basedir&"shakerss\download\"&thedir&"\rrinput.inp"""&vbcrlf&_
"CalibrateNodalDemand=""false"""&vbcrlf&_
"RegressionEquation=""Percent90"""&vbcrlf&_
"MeanPressureFile=""c:\temp\GIRAFFEDATA\testData\MCFixed\pressure.inp"" />"

Set XLExp = theDoc.NewExport("txt")
Set theComments = theCompSet.Item("Comments")

' Export out the XML file
theComments.Text = theXML
XLExp.Export theComments,basedir&"shakerss\download\"&thedir&"\autorun.xml",PromptNone

theComments.Text = "cd \giraffe "&vbcrlf&"giraffe.exe "&basedir&"shakerss\download\"&thedir&"\autorun.xml"
XLExp.Export theComments,basedir&"shakerss\download\"&thedir&"\autorun.bat",PromptNone

theDoc.SaveAs basedir&"shakerss\download\"&thedir&"\giraffe"&thedir&".map"
Set oShell = WScript.CreateObject ("WScript.Shell")
oshell.run basedir&"shakerss\download\"&thedir&"\autorun.bat"