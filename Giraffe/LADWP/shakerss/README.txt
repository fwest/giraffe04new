README.TXT for shakerss


* Typical usage for Windows users

1. Unzip the file into a directory, e.g., "c:\shakerss."  
2. Edit the file "shakerss.conf" to specify regions, ShakeMap products, and post-commands.

ShakeCast Lite with User Interface
----------------------------------
3. Double-click to run the executable "shakerss_gui.exe."  The ShakeCast Lite icon will
   appear in the Toolbar.
4. Click on the ShakeCast icon to access the main window and menu of the program.  
5. The Web-based User inferface can be launched directly from the program menu or from a web browser.

ShakeCast Lite Command Line Interface
-------------------------------------
3. Double-click to run the executable "shakerss.exe."  A DOS console window should appear with download status.  The window will be closed when the download is complete.
4. To run this script frequently, add it to "Start->Settings->Control Panel->Schedule Tasks->Add Scheduled Task."  
5. For detailed command line options, type "shakerss.exe -help."

Note: The source scripts in Perl are available in the "src" directory.  To run the Perl scripts, make sure that ActivePerl is installed, which can be downloaded at http://aspn.activestate.com/ASPN/Downloads/ActivePerl/



* Typical usage for Mac/UNIX users

1. Uncompress the tar-ball file into a directory, e.g., "~user/shakerss."
	unzip shakerss.zip 
2. Edit the file "shakerss.conf" to specify regions, ShakeMap products, and post-commands.

ShakeCast Lite with User Interface
----------------------------------
3. Double-click to run the executable "shakerss_gui."  The ShakeCast Lite icon will
   appear in the Toolbar.
4. Click on the ShakeCast icon to access the main window and menu of the program.  
5. The Web-based User inferface can be launched directly from the program menu or from a web browser.

ShakeCast Lite Command Line Interface
-------------------------------------
3. Run the script "shakerss" to download recent ShakeMaps manually.
4. To run this script as a cron job, try the following example

	Crontab, running RSS reader every 5 minutes:
*/5 * * * * /home/user/shakerss/shakerss  &

5. For detailed command line options, type "shakerss -help."
