﻿namespace Giraffe
{
    partial class SimulationDialogMonteCarloFlexible
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SuspendLayout();
            // 
            // textBoxPipeRepairRateFile
            // 
            // 
            // SimulationDialogMonteCarloFlexible
            // 
            this.ClientSize = new System.Drawing.Size(750, 445);
            this.HelpButton = true;
            this.Name = "SimulationDialogMonteCarloFlexible";
            this.Text = "Monte Carlo - Flexible";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
    }
}
