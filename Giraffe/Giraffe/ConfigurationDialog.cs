﻿using System;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.Windows.Forms;
using Giraffe;
using GiraffeUI;

namespace Giraffe
{
    public partial class ConfigurationDialog : Form
    {
        #region Properties

        /// <summary>
        /// Configuration parameters - called "Parameter Defines" in classic GIRAFFE
        /// </summary>
        private readonly ParameterDefinesAlt _parameters = new ParameterDefinesAlt();

        /// <summary>
        /// Flag array that indicates that a control has failed validation on a tab page
        /// </summary>
        private readonly bool[] _errorOnTab = new bool[] {false, false, false, false};

        /// <summary>
        /// Array of fragment URI's for linking to context-sensitive help in the help file
        /// </summary>
        private readonly string[] _helpOnTab = new string[] {"CONFIGFLEX", "CONFIGNODAL", "CONFIGPIPELEAK", "CONFIGPIPEDAMAGE"};

        /// <summary>
        /// Common file filter for GIRAFFE configuration files (i.e. Parameter Defines)
        /// </summary>
        private const string GiraffeConfigFileFilter = "GIRAFFE Configuration files (*.txt)|*.txt";

        /// <summary>
        /// ErrorProvider used to indicate which controls have failed validation
        /// </summary>
        private readonly ErrorProvider _errorProvider;

        /// <summary>
        /// Configuration setting where the configuration file path is stored
        /// </summary>
        private const string ParameterDefinesFileConfigSetting = "ParameterDefinesFile";

        protected Configuration Configuration;

        /// <summary>
        /// Gets/sets the path of the configuration file
        /// </summary>
        private string ParameterDefinesFile
        {
            get
            {
                return Configuration.AppSettings.Settings[ParameterDefinesFileConfigSetting].Value;
            }
            set
            {
                Configuration.AppSettings.Settings[ParameterDefinesFileConfigSetting].Value = value;
            }
        }

        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        public ConfigurationDialog()
        {
            _errorProvider = new ErrorProvider();
            InitializeComponent();
        }

        # region Control Event Handlers

        /// <summary>
        /// Handles the Load button click event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonLoad_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.Filter = GiraffeConfigFileFilter;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    // Fill in the text box and load the specified file
                    textBoxConfigFile.Text = openFileDialog.FileName;
                    LoadParameterDefinesFile(openFileDialog.FileName);
                    // Persist the current configuration file path
                    ParameterDefinesFile = textBoxConfigFile.Text;
                }
            }
        }

        /// <summary>
        /// Handles the Save button click event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSave_Click(object sender, EventArgs e)
        {
            // Clear all tab page error flags
            for (int index = 0; index < _errorOnTab.Length; index++)
            {
                _errorOnTab[index] = false;
            }

            // Perform validation of all child controls
            if (ValidateChildren())
            {
                // If validation was successful, prompt the user to save
                using (SaveFileDialog saveFileDialog = new SaveFileDialog())
                {
                    saveFileDialog.FileName = textBoxConfigFile.Text;
                    saveFileDialog.Filter = GiraffeConfigFileFilter;
                    saveFileDialog.RestoreDirectory = true;

                    if (saveFileDialog.ShowDialog() == DialogResult.OK)
                    {
                        // Update the text box and save the parameters
                        textBoxConfigFile.Text = saveFileDialog.FileName;
                        SaveParameterDefinesFile(saveFileDialog.FileName);
                        // Persist the current configuration file path
                        ParameterDefinesFile = textBoxConfigFile.Text;
                    }
                }
            }
            else
            {
                // Validation failed - invalidate the control to redraw the 
                // tabs to show where errors occurred
                tabControlConfiguration.Invalidate();
            }
        }

        /// <summary>
        /// Handles the Close button click event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Handles the Leave event for the CI Break textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxBreakBreakProCI_Leave(object sender, EventArgs e)
        {
            // Keep the CI Leak textbox in synch (Break + Leak = 1.0)
            RefreshLeakRatio(sender as ValidatingTextBox, textBoxCILeakRatio);
        }

        /// <summary>
        /// Handles the Leave event for the DI Break textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxBreakProDI_Leave(object sender, EventArgs e)
        {
            // Keep the DI Leak textbox in synch (Break + Leak = 1.0)
            RefreshLeakRatio(sender as ValidatingTextBox, textBoxDILeakRatio);
        }

        /// <summary>
        /// Handles the Leave event for the RS Break textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxBreakProRS_Leave(object sender, EventArgs e)
        {
            // Keep the RS Leak textbox in synch (Break + Leak = 1.0)
            RefreshLeakRatio(sender as ValidatingTextBox, textBoxRSLeakRatio);
        }

        /// <summary>
        /// Handles the Leave event for the CON Break textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxBreakProCON_Leave(object sender, EventArgs e)
        {
            // Keep the CON Leak textbox in synch (Break + Leak = 1.0)
            RefreshLeakRatio(sender as ValidatingTextBox, textBoxCONLeakRatio);
        }

        /// <summary>
        /// Handles the PassedValidation event for all ValidationTextBox controls.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox_PassedValidation(object sender, ValidationEventArgs e)
        {
            // When the textbox validation passes, clear error text
            SetValidationError(sender, "");
        }

        #endregion

        #region Form Event Handlers

        /// <summary>
        /// Handles the Help button click event for the form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConfigurationDialog_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            // Pass the currently selected tab to get context-sensitive help
            ShowHelp(tabControlConfiguration.SelectedIndex);
        }

        /// <summary>
        /// Handles the Form Load event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConfigurationDialog_Load(object sender, EventArgs e)
        {
            // Cache the app.config
            Configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            
            // If there is a persisted configuration file, load it
            if (!string.IsNullOrEmpty(ParameterDefinesFile))
            {
                textBoxConfigFile.Text = ParameterDefinesFile;
                LoadParameterDefinesFile(ParameterDefinesFile);
            }

            AssignValidationProperties();
            AssignToolTips();
        }

        /// <summary>
        /// Handles the Form Closing event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConfigurationDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Write to app.config
            Configuration.Save(ConfigurationSaveMode.Minimal);
        }

        #endregion

        #region Tab Control Management

        /// <summary>
        /// Handles the DrawItem event for the tab control.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Because the DrawMode is OwnerDrawFixed, the tabs must be drawn manually.</remarks>
        private void tabControlConfiguration_DrawItem(object sender, DrawItemEventArgs e)
        {
            // Default fore/back colors
            Color controlText = SystemColors.ControlText;
            Color backColor = SystemColors.Control;

            // If there is a validation error on this tab, color it RED
            if (_errorOnTab[e.Index])
            {
                backColor = Color.Red;
            }

            DrawTab(tabControlConfiguration, e, backColor, controlText, tabControlConfiguration.TabPages[e.Index].Text);
        }

        // Tab page indexes
        private const int OptionTabIndex = 0;
        private const int NodalDemandTabIndex = 1;
        private const int PipeLeakageTabIndex = 2;
        private const int PipeDamageTabIndex = 3;

        /// <summary>
        /// Handles the FailedValidation event for ValidatingTextBoxes on this tab page only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tabPageOptions_FailedValidation(object sender, ValidationEventArgs e)
        {
            // Set the error flag for this tab
            _errorOnTab[OptionTabIndex] = true;
            // Set the error message for the eventing control (i.e. sender)
            SetValidationError(sender, e.Message);
        }

        /// <summary>
        /// Handles the FailedValidation event for ValidatingTextBoxes on this tab page only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tabPageNodalDemand_FailedValidation(object sender, ValidationEventArgs e)
        {
            // Set the error flag for this tab
            _errorOnTab[NodalDemandTabIndex] = true;
            // Set the error message for the eventing control (i.e. sender)
            SetValidationError(sender, e.Message);
        }

        /// <summary>
        /// Handles the FailedValidation event for ValidatingTextBoxes on this tab page only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tabPagePipeLeakage_FailedValidation(object sender, ValidationEventArgs e)
        {
            // Set the error flag for this tab
            _errorOnTab[PipeLeakageTabIndex] = true;
            // Set the error message for the eventing control (i.e. sender)
            SetValidationError(sender, e.Message);
        }

        /// <summary>
        /// Handles the FailedValidation event for ValidatingTextBoxes on this tab page only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tabPagePipeDamage_FailedValidation(object sender, ValidationEventArgs e)
        {
            // Set the error flag for this tab
            _errorOnTab[PipeDamageTabIndex] = true;
            // Set the error message for the eventing control (i.e. sender)
            SetValidationError(sender, e.Message);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Show the help file.
        /// </summary>
        /// <param name="tabIndex"></param>
        private void ShowHelp(int tabIndex)
        {
            // Passing the fragement URI will locate the context-sensitive help for the specified tab page
            HelpDialog helpDlg = new HelpDialog(_helpOnTab[tabIndex]);
            helpDlg.Show();
        }

        /// <summary>
        /// Refreshes the leak ratio based on the passed corresponsing break ratio.
        /// It is assumed that the sum of the break and leak ratios is 1.0.
        /// This method handles the control display of the read-only textboxes only; the actual values are updated by the data objects.
        /// </summary>
        /// <param name="tbBreak"></param>
        /// <param name="tbLeak"></param>
        private void RefreshLeakRatio(ValidatingTextBox tbBreak, ValidatingTextBox tbLeak)
        {
            tbLeak.Text = ParameterDefinesAlt.CalculateLeakProbability(tbBreak.Text);
        }

        /// <summary>
        /// Draw a tab page header.
        /// </summary>
        /// <param name="tabControl"></param>
        /// <param name="e"></param>
        /// <param name="backColor"></param>
        /// <param name="foreColor"></param>
        /// <param name="caption"></param>
        private void DrawTab(TabControl tabControl, DrawItemEventArgs e, Color backColor, Color foreColor, string caption)
        {
            // Get the rectangle bounding the tab header
            Rectangle bounds = e.Bounds;
            // Create fore/back brushes
            Brush brush1 = (Brush)new SolidBrush(backColor);
            Brush brush2 = (Brush)new SolidBrush(foreColor);
            // Prepare to load the caption
            StringFormat format = new StringFormat();

            try
            {
                // Fill the rectangle with the back color
                e.Graphics.FillRectangle(brush1, bounds);
                // Align and write the caption in the fore color
                format.Alignment = StringAlignment.Center;
                format.LineAlignment = StringAlignment.Center;
                format.FormatFlags = StringFormatFlags.NoWrap;
                e.Graphics.DrawString(caption, e.Font, brush2, (RectangleF)bounds, format);
            }
            finally
            {
                brush1.Dispose();
                brush2.Dispose();
                format.Dispose();
            }
        }

        /// <summary>
        /// Sets the validation error message on the ErrorProvider for the specified ValidatingTextBox control.
        /// </summary>
        /// <param name="control">The ValidatingTextBox that has failed validation.</param>
        /// <param name="message">Validation error message to display.</param>
        private void SetValidationError(object control, string message)
        {
            ValidatingTextBox validatingTextBox = control as ValidatingTextBox;

            if (validatingTextBox != null)
            {
                _errorProvider.SetError(validatingTextBox, message);
            }
        }

        /// <summary>
        /// Assigns static tooltip text to each of the specified controls.
        /// </summary>
        protected virtual void AssignToolTips()
        {
            SetToolTip(textBoxBreakProCI, TooltipText.BreakProCI);
            SetToolTip(textBoxBreakProDI, TooltipText.BreakProDI);
            SetToolTip(textBoxBreakProRS, TooltipText.BreakProRS);
            SetToolTip(textBoxBreakProSTL, TooltipText.BreakProSTL);
            SetToolTip(textBoxBreakProCON, TooltipText.BreakProCON);
            SetToolTip(textBoxSTLLeakRatio, TooltipText.STLLeakRatio);
            SetToolTip(textBoxType1tD, TooltipText.Type1tD);
            SetToolTip(textBoxType1kD, TooltipText.Type1kD);
            SetToolTip(textBoxType2aD, TooltipText.Type2aD);
            SetToolTip(textBoxType3kD, TooltipText.Type3kD);
            SetToolTip(textBoxType3aD, TooltipText.Type3aD);
            SetToolTip(textBoxType4kD, TooltipText.Type4kD);
            SetToolTip(textBoxType5kD, TooltipText.Type5kD);
            SetToolTip(textBoxType5wD, TooltipText.Type5wD);
            SetToolTip(textBoxCIType1D, TooltipText.CIType1D);
            SetToolTip(textBoxCIType2D, TooltipText.CIType2D);
            SetToolTip(textBoxCIType3D, TooltipText.CIType3D);
            SetToolTip(textBoxCIType4D, TooltipText.CIType4D);
            SetToolTip(textBoxCIType5D, TooltipText.CIType5D);
            SetToolTip(textBoxRSType1D, TooltipText.RSType1D);
            SetToolTip(textBoxRSType2D, TooltipText.RSType2D);
            SetToolTip(textBoxRSType3D, TooltipText.RSType3D);
            SetToolTip(textBoxRSType4D, TooltipText.RSType4D);
            SetToolTip(textBoxRSType5D, TooltipText.RSType5D);
            SetToolTip(textBoxOtherType1D, TooltipText.OtherType1D);
            SetToolTip(textBoxOtherType2D, TooltipText.OtherType2D);
            SetToolTip(textBoxOtherType3D, TooltipText.OtherType3D);
            SetToolTip(textBoxOtherType4D, TooltipText.OtherType4D);
            SetToolTip(textBoxOtherType5D, TooltipText.OtherType5D);
            SetToolTip(textBoxDIType1D, TooltipText.DIType1D);
            SetToolTip(textBoxDIType2D, TooltipText.DIType2D);
            SetToolTip(textBoxDIType3D, TooltipText.DIType3D);
            SetToolTip(textBoxDIType4D, TooltipText.DIType4D);
            SetToolTip(textBoxDIType5D, TooltipText.DIType5D);
            SetToolTip(textBoxSTLType1D, TooltipText.STLType1D);
            SetToolTip(textBoxSTLType2D, TooltipText.STLType2D);
            SetToolTip(textBoxSTLType3D, TooltipText.STLType3D);
            SetToolTip(textBoxSTLType4D, TooltipText.STLType4D);
            SetToolTip(textBoxSTLType5D, TooltipText.STLType5D);
            SetToolTip(textBoxMiiMP, TooltipText.MiiMP);
            SetToolTip(textBoxMisMP, TooltipText.MisMP);
            SetToolTip(textBoxMsiMP, TooltipText.MsiMP);
            SetToolTip(textBoxMssMP, TooltipText.MssMP);
            SetToolTip(textBoxMiiSD, TooltipText.MiiSD);
            SetToolTip(textBoxMisSD, TooltipText.MisSD);
            SetToolTip(textBoxMsiSD, TooltipText.MsiSD);
            SetToolTip(textBoxMssSD, TooltipText.MssSD);
            SetToolTip(textBoxUiiMP, TooltipText.UiiMP);
            SetToolTip(textBoxUisMP, TooltipText.UisMP);
            SetToolTip(textBoxUsiMP, TooltipText.UsiMP);
            SetToolTip(textBoxUssMP, TooltipText.UssMP);
            SetToolTip(textBoxmRRCap, TooltipText.mRRCap);
        }

        /// <summary>
        /// Sets tooltip text on the specified control.
        /// There is a special case for handling ValidationTextBoxes.
        /// </summary>
        /// <param name="control"></param>
        /// <param name="text"></param>
        protected void SetToolTip(System.Windows.Forms.Control control, string text)
        {
            if (control != null)
            {
                // Set the tooltip text for the specified control
                toolTip1.SetToolTip(control, text);

                if (control is ValidatingTextBox)
                {
                    // The ValidatingTextBox control is composed of a fully-Docked TextBox control
                    // that fills the base UserControl.  Although the tooltip text is assigned to the 
                    // control, it is the component TextBox control that gets the MouseHover event.
                    // SetToolTip is an extension provider so shadowing the property won't work.  A custom
                    // property is set instead, and the ValidatingTextBox will handle the internal assignment
                    // on first hover (see ValidatingTextBox MouseHover event handler).
                    (control as ValidatingTextBox).ToolTipProvider = toolTip1;
                }
            }
        }

        /// <summary>
        /// Assigns static validation regular expressions and error messages to the specified controls.
        /// </summary>
        private void AssignValidationProperties()
        {
            textBoxMaxNSimu.ValidatingRegex = ValidatingExpressions.ValidInteger1to100Regex;
            textBoxMaxNSimu.ValidatingRegexMessage = ValidatingExpressions.InvalidInteger1to100Message;
            textBoxCOVFlexible.ValidatingRegex = ValidatingExpressions.ValidConvergenceRegex;
            textBoxCOVFlexible.ValidatingRegexMessage = ValidatingExpressions.InvalidConvergenceMessage;
            textBoxMeanFlexible.ValidatingRegex = ValidatingExpressions.ValidConvergenceRegex;
            textBoxMeanFlexible.ValidatingRegexMessage = ValidatingExpressions.InvalidConvergenceMessage;
            textBoxBreakProCI.ValidatingRegex = ValidatingExpressions.ValidProbabilityRegex;
            textBoxBreakProCI.ValidatingRegexMessage = ValidatingExpressions.InvalidProbabilityMessage;
            textBoxBreakProDI.ValidatingRegex = ValidatingExpressions.ValidProbabilityRegex;
            textBoxBreakProDI.ValidatingRegexMessage = ValidatingExpressions.InvalidProbabilityMessage;
            textBoxBreakProRS.ValidatingRegex = ValidatingExpressions.ValidProbabilityRegex;
            textBoxBreakProRS.ValidatingRegexMessage = ValidatingExpressions.InvalidProbabilityMessage;
            textBoxBreakProSTL.ValidatingRegex = ValidatingExpressions.ValidProbabilityRegex;
            textBoxBreakProSTL.ValidatingRegexMessage = ValidatingExpressions.InvalidProbabilityMessage;
            textBoxBreakProCON.ValidatingRegex = ValidatingExpressions.ValidProbabilityRegex;
            textBoxBreakProCON.ValidatingRegexMessage = ValidatingExpressions.InvalidProbabilityMessage;
            textBoxSTLLeakRatio.ValidatingRegex = ValidatingExpressions.ValidProbabilityRegex;
            textBoxSTLLeakRatio.ValidatingRegexMessage = ValidatingExpressions.InvalidProbabilityMessage;
            textBoxCILeakRatio.ValidatingRegex = ValidatingExpressions.ValidProbabilityRegex;
            textBoxCILeakRatio.ValidatingRegexMessage = ValidatingExpressions.InvalidProbabilityMessage;
            textBoxDILeakRatio.ValidatingRegex = ValidatingExpressions.ValidProbabilityRegex;
            textBoxDILeakRatio.ValidatingRegexMessage = ValidatingExpressions.InvalidProbabilityMessage;
            textBoxRSLeakRatio.ValidatingRegex = ValidatingExpressions.ValidProbabilityRegex;
            textBoxRSLeakRatio.ValidatingRegexMessage = ValidatingExpressions.InvalidProbabilityMessage;
            textBoxCONLeakRatio.ValidatingRegex = ValidatingExpressions.ValidProbabilityRegex;
            textBoxCONLeakRatio.ValidatingRegexMessage = ValidatingExpressions.InvalidProbabilityMessage;
            textBoxType1tD.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxType1tD.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxType1kD.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxType1kD.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxType2aD.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxType2aD.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxType3kD.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxType3kD.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxType3aD.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxType3aD.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxType4kD.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxType4kD.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxType5kD.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxType5kD.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxType5wD.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxType5wD.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxCIType1D.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxCIType1D.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxCIType2D.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxCIType2D.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxCIType3D.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxCIType3D.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxCIType4D.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxCIType4D.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxCIType5D.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxCIType5D.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxRSType1D.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxRSType1D.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxRSType2D.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxRSType2D.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxRSType3D.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxRSType3D.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxRSType4D.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxRSType4D.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxRSType5D.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxRSType5D.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxOtherType1D.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxOtherType1D.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxOtherType2D.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxOtherType2D.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxOtherType3D.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxOtherType3D.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxOtherType4D.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxOtherType4D.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxOtherType5D.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxOtherType5D.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxDIType1D.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxDIType1D.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxDIType2D.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxDIType2D.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxDIType3D.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxDIType3D.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxDIType4D.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxDIType4D.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxDIType5D.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxDIType5D.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxSTLType1D.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxSTLType1D.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxSTLType2D.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxSTLType2D.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxSTLType3D.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxSTLType3D.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxSTLType4D.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxSTLType4D.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxSTLType5D.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxSTLType5D.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxMiiMP.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxMiiMP.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxMisMP.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxMisMP.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxMsiMP.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxMsiMP.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxMssMP.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxMssMP.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxMiiSD.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxMiiSD.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxMisSD.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxMisSD.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxMsiSD.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxMsiSD.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxMssSD.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxMssSD.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxUiiMP.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxUiiMP.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxUisMP.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxUisMP.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxUsiMP.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxUsiMP.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxUssMP.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxUssMP.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
            textBoxmRRCap.ValidatingRegex = ValidatingExpressions.ValidFloatRegex;
            textBoxmRRCap.ValidatingRegexMessage = ValidatingExpressions.InvalidFloatMessage;
        }

        /// <summary>
        /// Loads the specified configuration file.
        /// </summary>
        /// <param name="filename"></param>
        private void LoadParameterDefinesFile(string filename)
        {
            _parameters.LoadConfiguration(filename);
            textBoxMaxNSimu.Text = _parameters.MaxNSimu;
            textBoxMeanFlexible.Text = _parameters.MeanFlexible;
            textBoxCOVFlexible.Text = _parameters.COVFlexible;
            textBoxBreakProCI.Text = _parameters.BreakProCI;
            textBoxBreakProDI.Text = _parameters.BreakProDI;
            textBoxBreakProRS.Text = _parameters.BreakProRS;
            textBoxBreakProSTL.Text = _parameters.BreakProSTL;
            textBoxBreakProCON.Text = _parameters.BreakProCON;
            textBoxSTLLeakRatio.Text = _parameters.STLLeakRatio;
            textBoxCILeakRatio.Text = _parameters.CILeakRatio;
            textBoxDILeakRatio.Text = _parameters.DILeakRatio;
            textBoxRSLeakRatio.Text = _parameters.RSLeakRatio;
            textBoxCONLeakRatio.Text = _parameters.CONLeakRatio;
            textBoxType1tD.Text = _parameters.Type1tD;
            textBoxType1kD.Text = _parameters.Type1kD;
            textBoxType2aD.Text = _parameters.Type2aD;
            textBoxType3kD.Text = _parameters.Type3kD;
            textBoxType3aD.Text = _parameters.Type3aD;
            textBoxType4kD.Text = _parameters.Type4kD;
            textBoxType5kD.Text = _parameters.Type5kD;
            textBoxType5wD.Text = _parameters.Type5wD;
            textBoxCIType1D.Text = _parameters.CIType1D;
            textBoxCIType2D.Text = _parameters.CIType2D;
            textBoxCIType3D.Text = _parameters.CIType3D;
            textBoxCIType4D.Text = _parameters.CIType4D;
            textBoxCIType5D.Text = _parameters.CIType5D;
            textBoxRSType1D.Text = _parameters.RSType1D;
            textBoxRSType2D.Text = _parameters.RSType2D;
            textBoxRSType3D.Text = _parameters.RSType3D;
            textBoxRSType4D.Text = _parameters.RSType4D;
            textBoxRSType5D.Text = _parameters.RSType5D;
            textBoxOtherType1D.Text = _parameters.OtherType1D;
            textBoxOtherType2D.Text = _parameters.OtherType2D;
            textBoxOtherType3D.Text = _parameters.OtherType3D;
            textBoxOtherType4D.Text = _parameters.OtherType4D;
            textBoxOtherType5D.Text = _parameters.OtherType5D;
            textBoxDIType1D.Text = _parameters.DIType1D;
            textBoxDIType2D.Text = _parameters.DIType2D;
            textBoxDIType3D.Text = _parameters.DIType3D;
            textBoxDIType4D.Text = _parameters.DIType4D;
            textBoxDIType5D.Text = _parameters.DIType5D;
            textBoxSTLType1D.Text = _parameters.STLType1D;
            textBoxSTLType2D.Text = _parameters.STLType2D;
            textBoxSTLType3D.Text = _parameters.STLType3D;
            textBoxSTLType4D.Text = _parameters.STLType4D;
            textBoxSTLType5D.Text = _parameters.STLType5D;
            textBoxMiiMP.Text = _parameters.MiiMP;
            textBoxMisMP.Text = _parameters.MisMP;
            textBoxMsiMP.Text = _parameters.MsiMP;
            textBoxMssMP.Text = _parameters.MssMP;
            textBoxMiiSD.Text = _parameters.MiiSD;
            textBoxMisSD.Text = _parameters.MisSD;
            textBoxMsiSD.Text = _parameters.MsiSD;
            textBoxMssSD.Text = _parameters.MssSD;
            textBoxUiiMP.Text = _parameters.UiiMP;
            textBoxUisMP.Text = _parameters.UisMP;
            textBoxUsiMP.Text = _parameters.UsiMP;
            textBoxUssMP.Text = _parameters.UssMP;
            textBoxmRRCap.Text = _parameters.mRRCap;
        }

        /// <summary>
        /// Saves the specified configuration file.
        /// </summary>
        /// <param name="filename"></param>
        private void SaveParameterDefinesFile(string filename)
        {
            _parameters.MaxNSimu = textBoxMaxNSimu.Text;
            _parameters.MeanFlexible = textBoxMeanFlexible.Text;
            _parameters.COVFlexible = textBoxCOVFlexible.Text;
            _parameters.BreakProCI = textBoxBreakProCI.Text;
            _parameters.BreakProDI = textBoxBreakProDI.Text;
            _parameters.BreakProRS = textBoxBreakProRS.Text;
            _parameters.BreakProSTL = textBoxBreakProSTL.Text;
            _parameters.BreakProCON = textBoxBreakProCON.Text;
            _parameters.STLLeakRatio = textBoxSTLLeakRatio.Text;
            _parameters.Type1tD = textBoxType1tD.Text;
            _parameters.Type1kD = textBoxType1kD.Text;
            _parameters.Type2aD = textBoxType2aD.Text;
            _parameters.Type3kD = textBoxType3kD.Text;
            _parameters.Type3aD = textBoxType3aD.Text;
            _parameters.Type4kD = textBoxType4kD.Text;
            _parameters.Type5kD = textBoxType5kD.Text;
            _parameters.Type5wD = textBoxType5wD.Text;
            _parameters.CIType1D = textBoxCIType1D.Text;
            _parameters.CIType2D = textBoxCIType2D.Text;
            _parameters.CIType3D = textBoxCIType3D.Text;
            _parameters.CIType4D = textBoxCIType4D.Text;
            _parameters.CIType5D = textBoxCIType5D.Text;
            _parameters.RSType1D = textBoxRSType1D.Text;
            _parameters.RSType2D = textBoxRSType2D.Text;
            _parameters.RSType3D = textBoxRSType3D.Text;
            _parameters.RSType4D = textBoxRSType4D.Text;
            _parameters.RSType5D = textBoxRSType5D.Text;
            _parameters.OtherType1D = textBoxOtherType1D.Text;
            _parameters.OtherType2D = textBoxOtherType2D.Text;
            _parameters.OtherType3D = textBoxOtherType3D.Text;
            _parameters.OtherType4D = textBoxOtherType4D.Text;
            _parameters.OtherType5D = textBoxOtherType5D.Text;
            _parameters.DIType1D = textBoxDIType1D.Text;
            _parameters.DIType2D = textBoxDIType2D.Text;
            _parameters.DIType3D = textBoxDIType3D.Text;
            _parameters.DIType4D = textBoxDIType4D.Text;
            _parameters.DIType5D = textBoxDIType5D.Text;
            _parameters.STLType1D = textBoxSTLType1D.Text;
            _parameters.STLType2D = textBoxSTLType2D.Text;
            _parameters.STLType3D = textBoxSTLType3D.Text;
            _parameters.STLType4D = textBoxSTLType4D.Text;
            _parameters.STLType5D = textBoxSTLType5D.Text;
            _parameters.MiiMP = textBoxMiiMP.Text;
            _parameters.MisMP = textBoxMisMP.Text;
            _parameters.MsiMP = textBoxMsiMP.Text;
            _parameters.MssMP = textBoxMssMP.Text;
            _parameters.MiiSD = textBoxMiiSD.Text;
            _parameters.MisSD = textBoxMisSD.Text;
            _parameters.MsiSD = textBoxMsiSD.Text;
            _parameters.MssSD = textBoxMssSD.Text;
            _parameters.UiiMP = textBoxUiiMP.Text;
            _parameters.UisMP = textBoxUisMP.Text;
            _parameters.UsiMP = textBoxUsiMP.Text;
            _parameters.UssMP = textBoxUssMP.Text;
            _parameters.mRRCap = textBoxmRRCap.Text;
            _parameters.SaveConfiguration(filename);
        }

        #endregion

    }
}
