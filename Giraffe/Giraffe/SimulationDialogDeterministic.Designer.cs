﻿using GiraffeUI;

namespace Giraffe
{
    partial class SimulationDialogDeterministic
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxPipeDamageFile = new GiraffeUI.ValidatingTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.buttonBrowsePipeDamage = new System.Windows.Forms.Button();
            this.buttonEditPipeDamage = new System.Windows.Forms.Button();
            this.buttonGeneratePipeDamage = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this._errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxPipeDamageFile
            // 
            this.textBoxPipeDamageFile.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxPipeDamageFile.ErrorProvider = null;
            this.textBoxPipeDamageFile.Location = new System.Drawing.Point(136, 264);
            this.textBoxPipeDamageFile.Name = "textBoxPipeDamageFile";
            this.textBoxPipeDamageFile.ReadOnly = true;
            this.textBoxPipeDamageFile.Required = true;
            this.textBoxPipeDamageFile.Size = new System.Drawing.Size(436, 20);
            this.textBoxPipeDamageFile.TabIndex = 29;
            this.textBoxPipeDamageFile.TabStop = false;
            this.textBoxPipeDamageFile.ToolTipProvider = null;
            this.textBoxPipeDamageFile.ValidatingRegex = null;
            this.textBoxPipeDamageFile.ValidatingRegexMessage = null;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(40, 269);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 13);
            this.label5.TabIndex = 30;
            this.label5.Text = "Pipe Damage File";
            // 
            // buttonBrowsePipeDamage
            // 
            this.buttonBrowsePipeDamage.Location = new System.Drawing.Point(578, 264);
            this.buttonBrowsePipeDamage.Name = "buttonBrowsePipeDamage";
            this.buttonBrowsePipeDamage.Size = new System.Drawing.Size(75, 23);
            this.buttonBrowsePipeDamage.TabIndex = 9;
            this.buttonBrowsePipeDamage.Text = "Browse...";
            this.buttonBrowsePipeDamage.UseVisualStyleBackColor = true;
            this.buttonBrowsePipeDamage.Click += new System.EventHandler(this.buttonBrowsePipeDamage_Click);
            // 
            // buttonEditPipeDamage
            // 
            this.buttonEditPipeDamage.Location = new System.Drawing.Point(659, 264);
            this.buttonEditPipeDamage.Name = "buttonEditPipeDamage";
            this.buttonEditPipeDamage.Size = new System.Drawing.Size(75, 23);
            this.buttonEditPipeDamage.TabIndex = 10;
            this.buttonEditPipeDamage.Text = "Edit...";
            this.buttonEditPipeDamage.UseVisualStyleBackColor = true;
            this.buttonEditPipeDamage.Click += new System.EventHandler(this.buttonEditPipeDamage_Click);
            // 
            // buttonGeneratePipeDamage
            // 
            this.buttonGeneratePipeDamage.Location = new System.Drawing.Point(578, 219);
            this.buttonGeneratePipeDamage.Name = "buttonGeneratePipeDamage";
            this.buttonGeneratePipeDamage.Size = new System.Drawing.Size(156, 39);
            this.buttonGeneratePipeDamage.TabIndex = 8;
            this.buttonGeneratePipeDamage.Text = "Generate Pipe Damage using Manifold GIS";
            this.buttonGeneratePipeDamage.UseVisualStyleBackColor = true;
            this.buttonGeneratePipeDamage.Click += new System.EventHandler(this.buttonGeneratePipeDamage_Click);
            // 
            // SimulationDialogDeterministic
            // 
            this.ClientSize = new System.Drawing.Size(750, 445);
            this.Controls.Add(this.buttonGeneratePipeDamage);
            this.Controls.Add(this.buttonEditPipeDamage);
            this.Controls.Add(this.buttonBrowsePipeDamage);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBoxPipeDamageFile);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.HelpButton = true;
            this.Name = "SimulationDialogDeterministic";
            this.Text = "Deterministic Simulation";
            this.Controls.SetChildIndex(this.checkBoxStartStop, 0);
            this.Controls.SetChildIndex(this.checkBoxTimestamp, 0);
            this.Controls.SetChildIndex(this.textBoxSimFolder, 0);
            this.Controls.SetChildIndex(this.textBoxOutputFolder, 0);
            this.Controls.SetChildIndex(this.textBoxSysDefFile, 0);
            this.Controls.SetChildIndex(this.textBoxMinPressure, 0);
            this.Controls.SetChildIndex(this.textBoxSimTime, 0);
            this.Controls.SetChildIndex(this.textBoxSimStep, 0);
            this.Controls.SetChildIndex(this.textBoxPipeDamageFile, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.buttonBrowsePipeDamage, 0);
            this.Controls.SetChildIndex(this.buttonEditPipeDamage, 0);
            this.Controls.SetChildIndex(this.buttonGeneratePipeDamage, 0);
            ((System.ComponentModel.ISupportInitialize)(this._errorProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ValidatingTextBox textBoxPipeDamageFile;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button buttonBrowsePipeDamage;
        private System.Windows.Forms.Button buttonEditPipeDamage;
        private System.Windows.Forms.Button buttonGeneratePipeDamage;
    }
}
