﻿using System;
using System.Windows.Forms;
using System.Configuration;
using Giraffe;

namespace Giraffe
{
    /// <summary>
    /// Monte Carlo - Fixed Simulation dialog
    /// </summary>
    public partial class SimulationDialogMonteCarloFixed : SimulationDialogMonteCarlo
    {
        #region Properties

        /// <summary>
        /// Configuration path to the default project folder
        /// </summary>
        protected override string ProjectFolderConfigSetting
        {
            get { return "MCFixedProjectFolder"; }
        }

        /// <summary>
        /// Configuration path to the default timestamp flag
        /// </summary>
        protected override string TimestampConfigSetting
        {
            get { return "MCFixedTimestamp"; }
        }

        /// <summary>
        /// Configuration path to the default output folder
        /// </summary>
        protected override string OutputFolderConfigSetting
        {
            get { return "MCFixedOutputFolder"; }
        }

        /// <summary>
        /// Configuration path to the system definition file
        /// </summary>
        protected override string SysDefFileConfigSetting
        {
            get { return "MCFixedSysDefFile"; }
        }

        /// <summary>
        /// Configuration path to the default minimum pressure to eliminate
        /// </summary>
        protected override string MinPressureConfigSetting
        {
            get { return "MCFixedMinPressure"; }
        }

        /// <summary>
        /// Configuration path to the default simulation time period
        /// </summary>
        protected override string SimTimeConfigSetting
        {
            get { return "MCFixedSimTime"; }
        }

        /// <summary>
        /// Configuration path to the default simulation step period
        /// </summary>
        protected override string SimStepConfigSetting
        {
            get { return "MCFixedSimStep"; }
        }

        /// <summary>
        /// Derived classes should override the app.config key for persisting the random seed
        /// </summary>
        protected override string RandomSeedConfigSetting
        {
            get { return "MCFixedRandomSeed"; }
        }

        /// <summary>
        /// Derived classes should override the app.config key for persisting the pipe repair rate file
        /// </summary>
        protected override string PipeRepairConfigSetting
        {
            get { return "MCFixedPipeRepair"; }
        }

        /// <summary>
        /// Derived classes should override the app.config key for persisting the nodal demand calibration flag
        /// </summary>
        protected override string NodalDemandConfigSetting
        {
            get { return "MCFixedNodalDemand"; }
        }

        /// <summary>
        /// Derived classes should override the app.config key for persisting the regression equation
        /// </summary>
        protected override string RegressionConfigSetting
        {
            get { return "MCFixedRegression"; }
        }

        /// <summary>
        /// Derived classes should override the app.config key for persisting the mean pressure file
        /// </summary>
        protected override string MeanPressureConfigSetting
        {
            get { return "MCFixedMeanPressure"; }
        }

        /// <summary>
        /// Derived classes should override the app.config key for persisting the number of simulations
        /// </summary>
        private const string NumSimsConfigSetting = "MCFixedNumSimulations";
        /// <summary>
        /// Gets/sets the default number of simulations
        /// </summary>
        protected virtual string NumSimulations
        {
            get
            {
                return Configuration.AppSettings.Settings[NumSimsConfigSetting].Value;
            }
            set
            {
                Configuration.AppSettings.Settings[NumSimsConfigSetting].Value = value;
            }
        }

        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        public SimulationDialogMonteCarloFixed()
        {
            InitializeComponent();
        }

        #region Methods

        /// <summary>
        /// Disables all controls during a simulation run.
        /// </summary>
        protected override void DisableAllControls()
        {
            base.DisableAllControls();
            textBoxNumSimulations.Enabled = false;
        }

        /// <summary>
        /// Enables all controls after a simulation run.
        /// </summary>
        protected override void EnableAllControls()
        {
            textBoxNumSimulations.Enabled = true;
            base.EnableAllControls();
        }

        /// <summary>
        /// Loads the persisted default input values
        /// </summary>
        protected override void LoadPersistedInputValues()
        {
            base.LoadPersistedInputValues();
            textBoxNumSimulations.Text = NumSimulations;
        }

        /// <summary>
        /// Persists the current input values for re-use
        /// </summary>
        protected override void PersistInputValues()
        {
            NumSimulations = textBoxNumSimulations.Text;
            base.PersistInputValues();
        }

        /// <summary>
        /// Override this method to call the simulation method
        /// </summary>
        protected override void StartSimulation()
        {
            base.StartSimulation();

            // Load parameter defines
            ParameterDefines parameterDefines = new ParameterDefines();
            parameterDefines.LoadConfiguration(ParameterDefinesFile);

            // Assign simulation parameters
            SimulationParameters simulationParameters = new SimulationParameters();
            simulationParameters.Category = SimulationType.MonteFixed;
            // Get UI control settings
            simulationParameters.ProjectName = textBoxSimFolder.Text;
            simulationParameters.Timestamp = checkBoxTimestamp.Checked;
            simulationParameters.OutputPath = textBoxOutputFolder.Text;
            simulationParameters.SystemFile = textBoxSysDefFile.Text;
            simulationParameters.MinimumPressureToEliminate = Convert.ToInt16(textBoxMinPressure.Text);
            simulationParameters.NumberOfHours = Convert.ToInt16(textBoxSimTime.Text);
            simulationParameters.NumberOfStepsPerHour = Convert.ToInt16(textBoxSimStep.Text);
            simulationParameters.NumberOfSimulations = Convert.ToInt16(textBoxNumSimulations.Text);
            simulationParameters.MeanPressureFile = textBoxMeanPressureFile.Text;
            simulationParameters.PipeRepairRateFile = textBoxPipeRepairRateFile.Text;
            simulationParameters.CalibrateNodalDemand = checkBoxNodalDemandCalibration.Checked;
            simulationParameters.RegressionEquation = (RegressionType) comboBoxRegressionEquation.SelectedIndex;
            simulationParameters.RandomSeed = Convert.ToInt16(textBoxRandomSeed.Text);
            // Get OTHER settings
            simulationParameters.Debug = false;
            simulationParameters.Wsort = true;
            // Default the rest - don't care about these (use defaults in data class?)
            simulationParameters.PipeDamageFile = string.Empty;

            SimWorker.RunWorkerAsync(new GiraffeSimulation(parameterDefines, simulationParameters));
        }

        /// <summary>
        /// Show context-sensitive help in the help file.
        /// </summary>
        protected override void ShowHelp()
        {
            base.ShowHelp();

            HelpDialog dlg = new HelpDialog("MCFIXSIM");
            dlg.Show();
        }

        /// <summary>
        /// Assigns static tooltip text to the specified controls.
        /// </summary>
        protected override void AssignToolTips()
        {
            base.AssignToolTips();
            SetToolTip(textBoxNumSimulations, TooltipText.NumberOfSimulations);
        }

        /// <summary>
        /// Validates other controls with unique needs.
        /// </summary>
        /// <returns>False if any control fails validation; otherwise, true is returned.</returns>
        protected override bool ValidateOtherControls()
        {
            bool valid = base.ValidateOtherControls();
            int n;

            // Validate number of simulations
            if (!(int.TryParse(textBoxNumSimulations.Text, out n)) || (n <= 0))
            {
               _errorProvider.SetError(textBoxNumSimulations, "Please enter a positive number of simulations.");
                valid = false;
            }
            else
            {
                _errorProvider.SetError(textBoxNumSimulations, "");
            }

            return valid;
        }

        #endregion
    }
}