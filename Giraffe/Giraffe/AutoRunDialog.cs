﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Giraffe
{
    /// <summary>
    /// This form appears when the application is invoked via the command line.
    /// </summary>
    public partial class AutoRunDialog : Form
    {
        private SimulationParameters simulationParameters;
        private ParameterDefines parameterDefines;
        public BackgroundWorker AutoSimWorker;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="configuration"></param>
        public AutoRunDialog(SimulationParameters parameters, ParameterDefines configuration)
        {
            simulationParameters = parameters;
            parameterDefines = configuration;

            // Create a BackgroundWorker that reports progress and allows cancellation
            AutoSimWorker = new BackgroundWorker
            {
                WorkerReportsProgress = true,
                WorkerSupportsCancellation = true
            };

            AutoSimWorker.DoWork += AutoSimWorker_DoWork;
            AutoSimWorker.ProgressChanged += AutoSimWorker_ProgressChanged;
            AutoSimWorker.RunWorkerCompleted += AutoSimWorker_RunWorkerCompleted;

            InitializeComponent();
        }

        /// <summary>
        /// Converts the SimulationParameters into a string for display to the user.
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns>Array of strings representing the list of specified SimulationParameters.</returns>
        private string[] GetSimulationParametersSummary(SimulationParameters parameters)
        {
            Hashtable defaults = new Hashtable();
            // Create a "default" object
            SimulationParameters defaultParameters = new SimulationParameters();
            // Iterate through the default object and store it's settings
            foreach (var property in defaultParameters.GetType().GetFields())
            {
                defaults.Add(property.Name, property.GetValue(defaultParameters));
            }

            List<string> summary = new List<string>();

            // Iterate through the passed parameters and build strings for each
            foreach (var property in parameters.GetType().GetFields())
            {
                StringBuilder sb = new StringBuilder();
                object propValue = property.GetValue(parameters);
                sb.AppendFormat("{0} = \"{1}\"", property.Name, propValue);

                // If the passed parameter matches the default value, append DEFAULT
                if (defaults[property.Name].ToString() == propValue.ToString())
                {
                    sb.Append(" (DEFAULT)");
                }

                summary.Add(sb.ToString());
            }

            return summary.ToArray();
        }

        /// <summary>
        /// Fires when the BackgroundWorker thread has completed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AutoSimWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            string message = "Successful Simulation!";

            // NOTE: Cancelled is checked first.  In some situations, a cancelled worker
            // will throw if the Result member is accessed.
            if (e.Cancelled)
            {
                // If the thread was cancelled, notify the user
                message = "Cancelled!";
            }
            else if (e.Error != null)
            {
                // If an error was returned, display error text
                message = string.Format("Simulation FAILED [Error: {0}]", e.Error.Message);
            }
            else if (e.Result != null)
            {
                Exception ex = (Exception)e.Result;
                // If an error was returned, display error text
                message = string.Format("Simulation FAILED [Error: {0}]", ex.Message);
            }

            textBoxStatus.Text = message;

            Shutdown();
        }

        /// <summary>
        /// Handles the BackgroundWorker ProgressChanged event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AutoSimWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            // If user status has been passed, update the status text
            if (e.UserState != null)
            {
                textBoxStatus.Text = ((string)e.UserState);
            }

            // If progress percentage has been passed, update the progress bar
            if (e.ProgressPercentage >= 0)
            {
                progressBar1.Value = e.ProgressPercentage;
            }
        }

        /// <summary>
        /// Handles the BackgroundWorker's DoWork event; occurs when RunWorkerAsync is called.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AutoSimWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            // Get the passwed worker
            BackgroundWorker worker = sender as BackgroundWorker;
            // Execute the simulation on the worker thread
            ((GiraffeSimulation)e.Argument).ExecuteSimulation(worker, e);
        }

        /// <summary>
        /// Displays a brief countdown for the user before closing the dialog.
        /// </summary>
        private void Shutdown()
        {
            // Countdown for 10 seconds
            for (int i = 10; i > 0; i--)
            {
                labelCountdown.Text = string.Format("SHUTTING DOWN IN {0} SECONDS!", i);
                Application.DoEvents();
                Thread.Sleep(700);
                labelCountdown.Text = "";
                Application.DoEvents();
                Thread.Sleep(300);
            }

            Close();
        }

        /// <summary>
        /// Handles the form's Load event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AutoRunDialog_Load(object sender, EventArgs e)
        {
            labelCountdown.Text = "";

            // SHOW THE PROPERTIES ON THE DIALOG
            foreach (var s in GetSimulationParametersSummary(simulationParameters))
            {
                listBox1.Items.Add(s);
            }

            GiraffeSimulation autoSim = new GiraffeSimulation(parameterDefines, simulationParameters);
            AutoSimWorker.RunWorkerAsync(autoSim);
        }

        /// <summary>
        /// Handles the Cancel button's click event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            AutoSimWorker.CancelAsync();
        }

        /// <summary>
        /// Handles the form's Closing event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AutoRunDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Delay the close if the SimWorker is busy
            if (AutoSimWorker.IsBusy)
            {
                // Cancel the worker
                AutoSimWorker.CancelAsync();
                // Abort this closing
                e.Cancel = true;
            }
        }
    }
}