﻿using System;
using System.Windows.Forms;
using System.Configuration;
using Giraffe;

namespace Giraffe
{
    /// <summary>
    /// Monte Carlo Flexible Simulation dialog
    /// </summary>
    public partial class SimulationDialogMonteCarloFlexible : SimulationDialogMonteCarlo
    {
        #region Properties

        /// <summary>
        /// Configuration path to the default project folder
        /// </summary>
        protected override string ProjectFolderConfigSetting
        {
            get { return "MCFlexibleProjectFolder"; }
        }

        /// <summary>
        /// Configuration path to the default timestamp flag
        /// </summary>
        protected override string TimestampConfigSetting
        {
            get { return "MCFlexibleTimestamp"; }
        }

        /// <summary>
        /// Configuration path to the default output folder
        /// </summary>
        protected override string OutputFolderConfigSetting
        {
            get { return "MCFlexibleOutputFolder"; }
        }

        /// <summary>
        /// Configuration path to the system definition file
        /// </summary>
        protected override string SysDefFileConfigSetting
        {
            get { return "MCFlexibleSysDefFile"; }
        }

        /// <summary>
        /// Configuration path to the default minimum pressure to eliminate
        /// </summary>
        protected override string MinPressureConfigSetting
        {
            get { return "MCFlexibleMinPressure"; }
        }

        /// <summary>
        /// Configuration path to the default simulation time period
        /// </summary>
        protected override string SimTimeConfigSetting
        {
            get { return "MCFlexibleSimTime"; }
        }

        /// <summary>
        /// Configuration path to the default simulation step period
        /// </summary>
        protected override string SimStepConfigSetting
        {
            get { return "MCFlexibleSimStep"; }
        }

        /// <summary>
        /// Derived classes should override the app.config key for persisting the random seed
        /// </summary>
        protected override string RandomSeedConfigSetting
        {
            get { return "MCFlexibleRandomSeed"; }
        }

        /// <summary>
        /// Derived classes should override the app.config key for persisting the pipe repair rate file
        /// </summary>
        protected override string PipeRepairConfigSetting
        {
            get { return "MCFlexiblePipeRepair"; }
        }

        /// <summary>
        /// Derived classes should override the app.config key for persisting the nodal demand calibration flag
        /// </summary>
        protected override string NodalDemandConfigSetting
        {
            get { return "MCFlexibleNodalDemand"; }
        }

        /// <summary>
        /// Derived classes should override the app.config key for persisting the regression equation
        /// </summary>
        protected override string RegressionConfigSetting
        {
            get { return "MCFlexibleRegression"; }
        }

        /// <summary>
        /// Derived classes should override the app.config key for persisting the mean pressure file
        /// </summary>
        protected override string MeanPressureConfigSetting
        {
            get { return "MCFlexibleMeanPressure"; }
        }

        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        public SimulationDialogMonteCarloFlexible()
        {
            InitializeComponent();
        }

        #region Methods

        /// <summary>
        /// Override this method to call the simulation method
        /// </summary>
        protected override void StartSimulation()
        {
            base.StartSimulation();

            // Load parameter defines
            ParameterDefines parameterDefines = new ParameterDefines();
            parameterDefines.LoadConfiguration(ParameterDefinesFile);

            // Assign simulation parameters
            SimulationParameters simulationParameters = new SimulationParameters();
            simulationParameters.Category = SimulationType.MonteFlexible;
            // Get UI control settings
            simulationParameters.ProjectName = textBoxSimFolder.Text;
            simulationParameters.Timestamp = checkBoxTimestamp.Checked;
            simulationParameters.OutputPath = textBoxOutputFolder.Text;
            simulationParameters.SystemFile = textBoxSysDefFile.Text;
            simulationParameters.MinimumPressureToEliminate = Convert.ToInt16(textBoxMinPressure.Text);
            simulationParameters.NumberOfHours = Convert.ToInt16(textBoxSimTime.Text);
            simulationParameters.NumberOfStepsPerHour = Convert.ToInt16(textBoxSimStep.Text);
            simulationParameters.NumberOfSimulations = 1;
            simulationParameters.MeanPressureFile = textBoxMeanPressureFile.Text;
            simulationParameters.PipeRepairRateFile = textBoxPipeRepairRateFile.Text;
            simulationParameters.CalibrateNodalDemand = checkBoxNodalDemandCalibration.Checked;
            simulationParameters.RegressionEquation = (RegressionType)comboBoxRegressionEquation.SelectedIndex;
            simulationParameters.RandomSeed = Convert.ToInt16(textBoxRandomSeed.Text);
            // Get OTHER settings
            simulationParameters.Debug = false;
            simulationParameters.Wsort = true;
            // Default the rest - don't care about these (use defaults in data class?)
            simulationParameters.PipeDamageFile = string.Empty;

            SimWorker.RunWorkerAsync(new GiraffeSimulation(parameterDefines, simulationParameters));
        }

        /// <summary>
        /// Show context-sensitive help in the help file.
        /// </summary>
        protected override void ShowHelp()
        {
            base.ShowHelp();

            HelpDialog dlg = new HelpDialog("MCFLEXSIM");
            dlg.Show();
        }

        #endregion
    }
}