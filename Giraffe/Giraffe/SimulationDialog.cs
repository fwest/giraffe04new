﻿using System;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Giraffe;
using GiraffeUI;

namespace Giraffe
{
    /// <summary>
    /// Base form class for simulation dialogs
    /// </summary>
    public partial class SimulationDialog : Form
    {
        #region Properties

        /// <summary>
        /// ErrorProvider to notify the user that validation has failed
        /// </summary>
        protected readonly ErrorProvider _errorProvider;

        /// <summary>
        /// BackgroundWorker used to run the simulation on a background thread
        /// </summary>
        protected BackgroundWorker SimWorker;

        /// <summary>
        /// Flag to delay a Form Close while the SimWorker is busy
        /// </summary>
        private bool cancel = false;

        /// <summary>
        /// App.config settings
        /// </summary>
        protected Configuration Configuration;

        /// <summary>
        /// Derived classes should override the app.config key for persisting the project folder
        /// </summary>
        protected virtual string ProjectFolderConfigSetting { get { return string.Empty; }}
        /// <summary>
        /// Gets/sets the default project folder
        /// </summary>
        protected virtual string ProjectFolder
        {
            get
            {
                return Configuration.AppSettings.Settings[ProjectFolderConfigSetting].Value;
            }

            set
            {
                Configuration.AppSettings.Settings[ProjectFolderConfigSetting].Value = value;
            }
        }

        /// <summary>
        /// Derived classes should override the app.config key for persisting the timestamp flag
        /// </summary>
        protected virtual string TimestampConfigSetting { get { return string.Empty; }}
        /// <summary>
        /// Gets/sets the default timestamp flag
        /// </summary>
        protected virtual bool TimestampFolder
        {
            get
            {
                return Convert.ToBoolean(Configuration.AppSettings.Settings[TimestampConfigSetting].Value);
            }

            set
            {
                Configuration.AppSettings.Settings[TimestampConfigSetting].Value = value.ToString();
            }
        }

        /// <summary>
        /// Derived classes should override the app.config key for persisting the output folder
        /// </summary>
        protected virtual string OutputFolderConfigSetting { get { return string.Empty; }}
        /// <summary>
        /// Gets/sets the default output folder
        /// </summary>
        protected virtual string OutputFolderSavedPath
        {
            get
            {
                return Configuration.AppSettings.Settings[OutputFolderConfigSetting].Value;
            }

            set
            {
                Configuration.AppSettings.Settings[OutputFolderConfigSetting].Value = value;
            }
        }

        /// <summary>
        /// Derived classes should override the app.config key for persisting the system definition file
        /// </summary>
        protected virtual string SysDefFileConfigSetting { get { return string.Empty; } }
        /// <summary>
        /// Gets/sets the default system definition file
        /// </summary>
        protected virtual string SysDefFileSavedPath
        {
            get
            {
                return Configuration.AppSettings.Settings[SysDefFileConfigSetting].Value;
            }
            set
            {
                Configuration.AppSettings.Settings[SysDefFileConfigSetting].Value = value;
            }
        }

        /// <summary>
        /// Derived classes should override the app.config key for persisting the minimum pressure to eliminate
        /// </summary>
        protected virtual string MinPressureConfigSetting { get { return string.Empty; } }
        /// <summary>
        /// Gets/sets the default minimum pressure to eliminate
        /// </summary>
        protected virtual string MinPressure
        {
            get
            {
                return Configuration.AppSettings.Settings[MinPressureConfigSetting].Value;
            }
            set
            {
                Configuration.AppSettings.Settings[MinPressureConfigSetting].Value = value;
            }
        }

        /// <summary>
        /// Derived classes should override the app.config key for persisting the simulation time
        /// </summary>
        protected virtual string SimTimeConfigSetting { get { return string.Empty; } }
        /// <summary>
        /// Gets/sets the default simulation time period
        /// </summary>
        protected virtual string SimulationTime
        {
            get
            {
                return Configuration.AppSettings.Settings[SimTimeConfigSetting].Value;
            }
            set
            {
                Configuration.AppSettings.Settings[SimTimeConfigSetting].Value = value;
            }
        }

        /// <summary>
        /// Derived classes should override the app.config key for persisting the simulation time step
        /// </summary>
        protected virtual string SimStepConfigSetting { get { return string.Empty; } }
        /// <summary>
        /// Gets/sets the default simulation step period
        /// </summary>
        protected virtual string SimulationStep
        {
            get
            {
                return Configuration.AppSettings.Settings[SimStepConfigSetting].Value;
            }
            set
            {
                Configuration.AppSettings.Settings[SimStepConfigSetting].Value = value;
            }
        }

        protected virtual string ParameterDefinesFile
        {
            get { return Configuration.AppSettings.Settings["ParameterDefinesFile"].Value; }
        }
        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        public SimulationDialog()
        {
            _errorProvider = new ErrorProvider();

            // Create a BackgroundWorker that reports progress and allows cancellation
            SimWorker = new BackgroundWorker
            {
                WorkerReportsProgress = true,
                WorkerSupportsCancellation = true
            };

            SimWorker.DoWork += SimWorker_DoWork;
            SimWorker.ProgressChanged += SimWorker_ProgressChanged;
            SimWorker.RunWorkerCompleted += SimWorker_RunWorkerCompleted;
            InitializeComponent();
        }

        #region Control event handlers

        /// <summary>
        /// Handles the Browse button click for the Output Folder textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBrowseOutputFolder_Click(object sender, EventArgs e)
        {
            // Open the folder browser
            using (FolderBrowserDialog folderDlg = new FolderBrowserDialog())
            {
                // Set up the dialog
                folderDlg.Description = "Please select the output folder.";
                // Root folder is My Computer (highest root folder)
                //folderDlg.RootFolder = Environment.SpecialFolder.MyComputer;
                // Load the persisted output folder from the last user selection
                string outputFolderSavedPath = this.OutputFolderSavedPath;

                // If the persisted folder is not empty...
                if (!string.IsNullOrEmpty(outputFolderSavedPath))
                {
                    // ...initialize the selected path with the last selected folder
                    folderDlg.SelectedPath = outputFolderSavedPath;
                }

                if (folderDlg.ShowDialog() == DialogResult.OK)
                {
                    // User clicked OK - display selected folder...
                    textBoxOutputFolder.Text = folderDlg.SelectedPath;
                    // ...and persist to the app config file
                    OutputFolderSavedPath = folderDlg.SelectedPath;
                }
            }
        }

        /// <summary>
        /// Handles the Browse button click for the System Definition File textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBrowseSysDefFile_Click(object sender, EventArgs e)
        {
            // Create the open file dialog
            using (OpenFileDialog openDlg = new OpenFileDialog())
            {
                openDlg.InitialDirectory = ".";
                openDlg.Filter = "Definition Input files (*.inp)|*.inp";
                openDlg.RestoreDirectory = true;
                
                if (openDlg.ShowDialog() == DialogResult.OK)
                {
                    // User clicked OK - display the selected file
                    textBoxSysDefFile.Text = openDlg.FileName;
                }
            }
        }

        /// <summary>
        /// Handles the Edit button click event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonEditSysDefFile_Click(object sender, EventArgs e)
        {
            // If a system definition file is specified, edit it in a TextEditor dialog
            EditFile(textBoxSysDefFile.Text);
        }

        /// <summary>
        /// Handles the checkbox Mouse Up event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBoxStartStop_MouseUp(object sender, MouseEventArgs e)
        {
            // If the checkbox is checked (i.e. button is pressed) -> START
            if (checkBoxStartStop.Checked)
            {
                // Validate all child controls
                if (ValidateChildren() && ValidateOtherControls())
                {
                    // All input controls are valid so toggle the "button"
                    checkBoxStartStop.BackColor = Color.FromArgb(192, 0, 0);
                    checkBoxStartStop.Text = "S T O P !";
                    // Reset the progress bar
                    progressBarSimulation.Value = 0;

                    // If the BackgroundWorker isn't busy, start a new simulation thread
                    if (SimWorker != null && !SimWorker.IsBusy)
                    {
                        DisableAllControls();
                        StartSimulation();
                    }
                }
                else
                {
                    // Uncheck this control
                    checkBoxStartStop.Checked = false;
                }
            }
            else
            {
                // If the checkbox is unchecked (i.e. button is released) -> STOP, toggle the button
                checkBoxStartStop.BackColor = Color.FromArgb(0, 192, 0);
                checkBoxStartStop.Text = "S T A R T !";

                // If the BackgroundWorker is busy, cancel it
                if (SimWorker != null && SimWorker.WorkerSupportsCancellation && SimWorker.IsBusy)
                {
                    SimWorker.CancelAsync();
                }
            }
        }

        #endregion

        #region Form Event Handlers

        /// <summary>
        /// Handles the form's Help Button clicked event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SimulationDialog_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            ShowHelp();
        }

        /// <summary>
        /// Handles the Form Load event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SimulationDialog_Load(object sender, EventArgs e)
        {
            if (!DesignMode)
            {
                // Load default settings for the dialog
                LoadPersistedInputValues();
                AssignErrorProviders();
                AssignToolTips();
            }
        }

        /// <summary>
        /// Handles the Form Closing event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SimulationDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Delay the close if the SimWorker is busy
            if (SimWorker.IsBusy)
            {
                // Set the delayed closing flag
                cancel = true;
                // Cancel the worker
                SimWorker.CancelAsync();
                // Abort this closing
                e.Cancel = true;
            }
            else
            {
                // Persist current dialog settings for re-use
                PersistInputValues();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Virtual method to start a simulation
        /// </summary>
        protected virtual void StartSimulation()
        {
            // Calls to simulations go here
        }

        /// <summary>
        /// Shows context-sensitive help
        /// </summary>
        protected virtual void ShowHelp()
        {
            // Override this method to display context-sensitive help
        }

        /// <summary>
        /// Disables all controls during a simulation run.
        /// </summary>
        protected virtual void DisableAllControls()
        {
            textBoxSimFolder.Enabled = false;
            checkBoxTimestamp.Enabled = false;
            buttonBrowseOutputFolder.Enabled = false;
            buttonBrowseSysDefFile.Enabled = false;
            buttonEditSysDefFile.Enabled = false;
            textBoxMinPressure.Enabled = false;
            textBoxSimTime.Enabled = false;
            textBoxSimStep.Enabled = false;
        }

        /// <summary>
        /// Enables all controls after a simulation run.
        /// </summary>
        protected virtual void EnableAllControls()
        {
            textBoxSimFolder.Enabled = true;
            checkBoxTimestamp.Enabled = true;
            buttonBrowseOutputFolder.Enabled = true;
            buttonBrowseSysDefFile.Enabled = true;
            buttonEditSysDefFile.Enabled = true;
            textBoxMinPressure.Enabled = true;
            textBoxSimTime.Enabled = true;
            textBoxSimStep.Enabled = true;
        }

        /// <summary>
        /// Loads persisted default values.
        /// </summary>
        protected virtual void LoadPersistedInputValues()
        {
            // Cache the app.config
            Configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            textBoxSimFolder.Text = ProjectFolder;
            checkBoxTimestamp.Checked = TimestampFolder;
            textBoxOutputFolder.Text = OutputFolderSavedPath;
            textBoxSysDefFile.Text = SysDefFileSavedPath;
            textBoxMinPressure.Text = MinPressure;
            textBoxSimTime.Text = SimulationTime;
            textBoxSimStep.Text = SimulationStep;
        }

        /// <summary>
        /// Persists default values for the dialog.
        /// </summary>
        protected virtual void PersistInputValues()
        {
            ProjectFolder = textBoxSimFolder.Text;
            TimestampFolder = checkBoxTimestamp.Checked;
            OutputFolderSavedPath = textBoxOutputFolder.Text;
            SysDefFileSavedPath = textBoxSysDefFile.Text;
            MinPressure = textBoxMinPressure.Text;
            SimulationTime = textBoxSimTime.Text;
            SimulationStep = textBoxSimStep.Text;

            // Write to app.config
            Configuration.Save(ConfigurationSaveMode.Minimal);
        }

        /// <summary>
        /// Assigns the ErrorProvider to each of the child ValidatingTextBox controls.
        /// </summary>
        private void AssignErrorProviders()
        {
            foreach (ValidatingTextBox validatingTextBox in Controls.OfType<ValidatingTextBox>())
            {
                validatingTextBox.ErrorProvider = _errorProvider;
            }
        }

        /// <summary>
        /// Assigns static tooltip text to each of the specified controls.
        /// </summary>
        protected virtual void AssignToolTips()
        {
            SetToolTip(textBoxSimFolder, TooltipText.SimulationFolder);
            SetToolTip(checkBoxTimestamp, TooltipText.TimestampFolder);
            SetToolTip(buttonBrowseOutputFolder, TooltipText.OutputFolder);
            SetToolTip(buttonBrowseSysDefFile, TooltipText.SelectSystemDefinitionFile);
            SetToolTip(buttonEditSysDefFile, TooltipText.EditSystemDefinitionFile);
            SetToolTip(textBoxMinPressure, TooltipText.MinPressureToEliminate);
            SetToolTip(textBoxSimTime, TooltipText.SimulationTime);
            SetToolTip(textBoxSimStep, TooltipText.SimulationTimeStep);
        }

        /// <summary>
        /// Sets tooltip text on the specified control.
        /// There is a special case for handling ValidationTextBoxes.
        /// </summary>
        /// <param name="control"></param>
        /// <param name="text"></param>
        protected void SetToolTip(System.Windows.Forms.Control control, string text)
        {
            if (control != null)
            {
                // Set the tooltip text for the specified control
                toolTip1.SetToolTip(control, text);

                if (control is ValidatingTextBox)
                {
                    // The ValidatingTextBox control is composed of a fully-Docked TextBox control
                    // that fills the base UserControl.  Although the tooltip text is assigned to the 
                    // control, it is the component TextBox control that gets the MouseHover event.
                    // SetToolTip is an extension provider so shadowing the property won't work.  A custom
                    // property is set instead, and the ValidatingTextBox will handle the internal assignment
                    // on first hover (see ValidatingTextBox MouseHover event handler).
                    (control as ValidatingTextBox).ToolTipProvider = toolTip1;
                }
            }
        }

        /// <summary>
        /// Updates the text displayed in the status bar.  This is used to show simulation progress events.
        /// </summary>
        /// <param name="updateText"></param>
        protected void UpdateStatusText(string updateText)
        {
            toolStripStatusLabel1.Text = updateText;
        }

        /// <summary>
        /// Opens the text editor dialog with the specified file
        /// </summary>
        /// <param name="filename">The full path of the file to be edited.</param>
        protected void EditFile(string filename)
        {
            // If a system definition file is specified, edit it in a TextEditor dialog
            if (!string.IsNullOrEmpty(filename))
            {
                if (File.Exists(filename))
                {
                    try
                    {
                        TextEditor editor = new TextEditor(filename);
                        editor.ShowDialog();
                    }
                    catch (Exception err)
                    {
                        MessageBoxEx.Show(this, err.Message);
                    }
                }
                else
                {
                    MessageBoxEx.Show(this, string.Format("The file {0} does not exist.  Please specify a different file.", filename), "File Not Found!",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        /// <summary>
        /// Validates other controls with unique needs.
        /// </summary>
        /// <returns>False if any control fails validation; otherwise, true is returned.</returns>
        protected virtual bool ValidateOtherControls()
        {
            bool valid = true;
            int n;

            // Validate minimum pressure to eliminate
            if (!(int.TryParse(textBoxMinPressure.Text, out n)))
            {
                _errorProvider.SetError(textBoxMinPressure, "Please enter a valid pressure.");
                valid = false;
            }
            else
            {
                _errorProvider.SetError(textBoxMinPressure, "");
            }

            // Validate number of hours
            if ((!(int.TryParse(textBoxSimTime.Text, out n))) || (n <= 0))
            {
                _errorProvider.SetError(textBoxSimTime, "Please enter a valid number of hours.");
                valid = false;
            }
            else
            {
                _errorProvider.SetError(textBoxSimTime, "");
            }

            // Validate simulation step
            if ((!(int.TryParse(textBoxSimStep.Text, out n))) || (n <= 0))
            {
                _errorProvider.SetError(textBoxSimStep, "Please enter a valid hour step value.");
                valid = false;
            }
            else
            {
                _errorProvider.SetError(textBoxSimStep, "");
            }

            return valid;
        }

        #endregion

        #region BackgroundWorker Management

        /// <summary>
        /// Handles the BackgroundWorker's DoWork event; occurs when RunWorkerAsync is called.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SimWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            // Get the passwed worker
            BackgroundWorker worker = sender as BackgroundWorker;
            // Execute the simulation on the worker thread
//            ((SimulationWithGui)e.Argument).ExecuteThreadedSimulation(worker, e); ORIGINAL MOCK CALL TO SIMULATION
            ((GiraffeSimulation)e.Argument).ExecuteSimulation(worker, e);
        }

        /// <summary>
        /// Handles the BackgroundWorker's ProgressChanged event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The ProgressChangedEventArgs may contain status text and/or progress percentage.</param>
        private void SimWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            // If user status has been passed, update the status text
            if (e.UserState != null)
            {                
                UpdateStatusText((string) e.UserState);
            }

            // If progress percentage has been passed, update the progress bar
            if (e.ProgressPercentage >= 0)
            {
                progressBarSimulation.Value = e.ProgressPercentage;
            }
        }

        /// <summary>
        /// Handles the Background Worker's RunWorkerCompleted; occurs when the thread has completed (either successfully or failed).
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SimWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            string message = "Successful Simulation!";
            MessageBoxIcon icon = MessageBoxIcon.Information;

            // NOTE: Cancelled is checked first.  In some situations, a cancelled worker
            // will throw if the Result member is accessed.
            if (e.Cancelled)
            {
                // If the thread was cancelled, notify the user
                message = "Simulation Cancelled!";
                icon = MessageBoxIcon.Exclamation;
            }
            else if (e.Error != null)
            {
                // If an error was returned, display error text
                message = string.Format("Simulation FAILED [Error: {0}]", e.Error.Message);
                icon = MessageBoxIcon.Error;
            }
            else if (e.Result != null)
            {
                // If an error was returned, display error text
                Exception ex = (Exception)e.Result;
                message = string.Format("Simulation FAILED [Error: {0}]", ex.Message);
                icon = MessageBoxIcon.Error;
            }

            UpdateStatusText(message);
            MessageBoxEx.Show(this, message, "Simulation Result", MessageBoxButtons.OK, icon);

            // Uncheck the start/stop "button"
            checkBoxStartStop.Checked = false;

            EnableAllControls();

            // Did we suppress a Form.Close?  If so, close the form now.
            if (cancel)
            {
                Close();
            }
        }  

        #endregion
    }
}