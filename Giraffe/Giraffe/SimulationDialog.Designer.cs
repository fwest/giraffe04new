﻿namespace Giraffe
{
    public partial class SimulationDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SimulationDialog));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonBrowseOutputFolder = new System.Windows.Forms.Button();
            this.buttonBrowseSysDefFile = new System.Windows.Forms.Button();
            this.checkBoxTimestamp = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.progressBarSimulation = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.checkBoxStartStop = new System.Windows.Forms.CheckBox();
            this.buttonEditSysDefFile = new System.Windows.Forms.Button();
            this.textBoxSimStep = new GiraffeUI.ValidatingTextBox();
            this.textBoxSimTime = new GiraffeUI.ValidatingTextBox();
            this.textBoxMinPressure = new GiraffeUI.ValidatingTextBox();
            this.textBoxSysDefFile = new GiraffeUI.ValidatingTextBox();
            this.textBoxOutputFolder = new GiraffeUI.ValidatingTextBox();
            this.textBoxSimFolder = new GiraffeUI.ValidatingTextBox();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(43, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Simulation Folder";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(59, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Output Folder";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 101);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "System Definition File";
            // 
            // buttonBrowseOutputFolder
            // 
            this.buttonBrowseOutputFolder.Location = new System.Drawing.Point(578, 54);
            this.buttonBrowseOutputFolder.Name = "buttonBrowseOutputFolder";
            this.buttonBrowseOutputFolder.Size = new System.Drawing.Size(75, 23);
            this.buttonBrowseOutputFolder.TabIndex = 2;
            this.buttonBrowseOutputFolder.Text = "Browse...";
            this.buttonBrowseOutputFolder.UseVisualStyleBackColor = true;
            this.buttonBrowseOutputFolder.Click += new System.EventHandler(this.buttonBrowseOutputFolder_Click);
            // 
            // buttonBrowseSysDefFile
            // 
            this.buttonBrowseSysDefFile.Location = new System.Drawing.Point(578, 96);
            this.buttonBrowseSysDefFile.Name = "buttonBrowseSysDefFile";
            this.buttonBrowseSysDefFile.Size = new System.Drawing.Size(75, 23);
            this.buttonBrowseSysDefFile.TabIndex = 3;
            this.buttonBrowseSysDefFile.Text = "Browse...";
            this.buttonBrowseSysDefFile.UseVisualStyleBackColor = true;
            this.buttonBrowseSysDefFile.Click += new System.EventHandler(this.buttonBrowseSysDefFile_Click);
            // 
            // checkBoxTimestamp
            // 
            this.checkBoxTimestamp.AutoSize = true;
            this.checkBoxTimestamp.Checked = true;
            this.checkBoxTimestamp.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxTimestamp.Location = new System.Drawing.Point(594, 14);
            this.checkBoxTimestamp.Name = "checkBoxTimestamp";
            this.checkBoxTimestamp.Size = new System.Drawing.Size(109, 17);
            this.checkBoxTimestamp.TabIndex = 1;
            this.checkBoxTimestamp.Text = "Timestamp Folder";
            this.checkBoxTimestamp.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(23, 138);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 29);
            this.label4.TabIndex = 20;
            this.label4.Text = "Minimum Pressure to Eliminate (psi)";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(14, 185);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(116, 13);
            this.label8.TabIndex = 21;
            this.label8.Text = "Simulation Time (hours)";
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(12, 222);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(118, 32);
            this.label9.TabIndex = 22;
            this.label9.Text = "Simulation Time Step (hours)";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.progressBarSimulation,
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 423);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(750, 22);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 29;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // progressBarSimulation
            // 
            this.progressBarSimulation.Name = "progressBarSimulation";
            this.progressBarSimulation.Size = new System.Drawing.Size(375, 16);
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(189, 17);
            this.toolStripStatusLabel1.Text = "Simulation events displayed here...";
            // 
            // toolTip1
            // 
            this.toolTip1.ShowAlways = true;
            // 
            // checkBoxStartStop
            // 
            this.checkBoxStartStop.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBoxStartStop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.checkBoxStartStop.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxStartStop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxStartStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxStartStop.Location = new System.Drawing.Point(578, 384);
            this.checkBoxStartStop.Name = "checkBoxStartStop";
            this.checkBoxStartStop.Size = new System.Drawing.Size(156, 24);
            this.checkBoxStartStop.TabIndex = 100;
            this.checkBoxStartStop.Text = "S T A R T !";
            this.checkBoxStartStop.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxStartStop.UseVisualStyleBackColor = false;
            this.checkBoxStartStop.MouseUp += new System.Windows.Forms.MouseEventHandler(this.checkBoxStartStop_MouseUp);
            // 
            // buttonEditSysDefFile
            // 
            this.buttonEditSysDefFile.Location = new System.Drawing.Point(659, 96);
            this.buttonEditSysDefFile.Name = "buttonEditSysDefFile";
            this.buttonEditSysDefFile.Size = new System.Drawing.Size(75, 23);
            this.buttonEditSysDefFile.TabIndex = 4;
            this.buttonEditSysDefFile.Text = "Edit...";
            this.buttonEditSysDefFile.UseVisualStyleBackColor = true;
            this.buttonEditSysDefFile.Click += new System.EventHandler(this.buttonEditSysDefFile_Click);
            // 
            // textBoxSimStep
            // 
            this.textBoxSimStep.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxSimStep.ErrorProvider = null;
            this.textBoxSimStep.Location = new System.Drawing.Point(136, 222);
            this.textBoxSimStep.Name = "textBoxSimStep";
            this.textBoxSimStep.ReadOnly = false;
            this.textBoxSimStep.Required = true;
            this.textBoxSimStep.Size = new System.Drawing.Size(201, 20);
            this.textBoxSimStep.TabIndex = 7;
            this.textBoxSimStep.ToolTipProvider = null;
            this.textBoxSimStep.ValidatingRegex = null;
            this.textBoxSimStep.ValidatingRegexMessage = null;
            // 
            // textBoxSimTime
            // 
            this.textBoxSimTime.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxSimTime.ErrorProvider = null;
            this.textBoxSimTime.Location = new System.Drawing.Point(136, 180);
            this.textBoxSimTime.Name = "textBoxSimTime";
            this.textBoxSimTime.ReadOnly = false;
            this.textBoxSimTime.Required = true;
            this.textBoxSimTime.Size = new System.Drawing.Size(201, 20);
            this.textBoxSimTime.TabIndex = 6;
            this.textBoxSimTime.ToolTipProvider = null;
            this.textBoxSimTime.ValidatingRegex = null;
            this.textBoxSimTime.ValidatingRegexMessage = null;
            // 
            // textBoxMinPressure
            // 
            this.textBoxMinPressure.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxMinPressure.ErrorProvider = null;
            this.textBoxMinPressure.Location = new System.Drawing.Point(136, 138);
            this.textBoxMinPressure.Name = "textBoxMinPressure";
            this.textBoxMinPressure.ReadOnly = false;
            this.textBoxMinPressure.Required = true;
            this.textBoxMinPressure.Size = new System.Drawing.Size(201, 20);
            this.textBoxMinPressure.TabIndex = 5;
            this.textBoxMinPressure.ToolTipProvider = null;
            this.textBoxMinPressure.ValidatingRegex = null;
            this.textBoxMinPressure.ValidatingRegexMessage = null;
            // 
            // textBoxSysDefFile
            // 
            this.textBoxSysDefFile.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxSysDefFile.ErrorProvider = null;
            this.textBoxSysDefFile.Location = new System.Drawing.Point(136, 96);
            this.textBoxSysDefFile.Name = "textBoxSysDefFile";
            this.textBoxSysDefFile.ReadOnly = true;
            this.textBoxSysDefFile.Required = true;
            this.textBoxSysDefFile.Size = new System.Drawing.Size(436, 20);
            this.textBoxSysDefFile.TabIndex = 25;
            this.textBoxSysDefFile.TabStop = false;
            this.textBoxSysDefFile.ToolTipProvider = null;
            this.textBoxSysDefFile.ValidatingRegex = null;
            this.textBoxSysDefFile.ValidatingRegexMessage = null;
            // 
            // textBoxOutputFolder
            // 
            this.textBoxOutputFolder.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxOutputFolder.ErrorProvider = null;
            this.textBoxOutputFolder.Location = new System.Drawing.Point(136, 54);
            this.textBoxOutputFolder.Name = "textBoxOutputFolder";
            this.textBoxOutputFolder.ReadOnly = true;
            this.textBoxOutputFolder.Required = true;
            this.textBoxOutputFolder.Size = new System.Drawing.Size(436, 20);
            this.textBoxOutputFolder.TabIndex = 24;
            this.textBoxOutputFolder.TabStop = false;
            this.textBoxOutputFolder.ToolTipProvider = null;
            this.textBoxOutputFolder.ValidatingRegex = null;
            this.textBoxOutputFolder.ValidatingRegexMessage = null;
            // 
            // textBoxSimFolder
            // 
            this.textBoxSimFolder.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxSimFolder.ErrorProvider = null;
            this.textBoxSimFolder.Location = new System.Drawing.Point(136, 12);
            this.textBoxSimFolder.Name = "textBoxSimFolder";
            this.textBoxSimFolder.ReadOnly = false;
            this.textBoxSimFolder.Required = true;
            this.textBoxSimFolder.Size = new System.Drawing.Size(436, 20);
            this.textBoxSimFolder.TabIndex = 0;
            this.textBoxSimFolder.ToolTipProvider = null;
            this.textBoxSimFolder.ValidatingRegex = "^[^\\\\\\./:\\*\\?\\\"<>\\|]{1}[^\\\\/:\\*\\?\\\"<>\\|]{0,254}$";
            this.textBoxSimFolder.ValidatingRegexMessage = "A folder name can\'t contain any of the following characters: \\ / : * ? \\\" < > |";
            // 
            // SimulationDialog
            // 
            this.ClientSize = new System.Drawing.Size(750, 445);
            this.Controls.Add(this.buttonEditSysDefFile);
            this.Controls.Add(this.checkBoxStartStop);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.textBoxSimStep);
            this.Controls.Add(this.textBoxSimTime);
            this.Controls.Add(this.textBoxMinPressure);
            this.Controls.Add(this.textBoxSysDefFile);
            this.Controls.Add(this.textBoxOutputFolder);
            this.Controls.Add(this.textBoxSimFolder);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.checkBoxTimestamp);
            this.Controls.Add(this.buttonBrowseSysDefFile);
            this.Controls.Add(this.buttonBrowseOutputFolder);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SimulationDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "SimulationDialog";
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.SimulationDialog_HelpButtonClicked);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SimulationDialog_FormClosing);
            this.Load += new System.EventHandler(this.SimulationDialog_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonBrowseOutputFolder;
        private System.Windows.Forms.Button buttonBrowseSysDefFile;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        public GiraffeUI.ValidatingTextBox textBoxSimFolder;
        public GiraffeUI.ValidatingTextBox textBoxOutputFolder;
        public GiraffeUI.ValidatingTextBox textBoxSysDefFile;
        public GiraffeUI.ValidatingTextBox textBoxMinPressure;
        public GiraffeUI.ValidatingTextBox textBoxSimTime;
        public GiraffeUI.ValidatingTextBox textBoxSimStep;
        public System.Windows.Forms.CheckBox checkBoxTimestamp;
        public System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripProgressBar progressBarSimulation;
        private System.Windows.Forms.ToolTip toolTip1;
        public System.Windows.Forms.CheckBox checkBoxStartStop;
        private System.Windows.Forms.Button buttonEditSysDefFile;
    }
}