﻿using System;
using System.Configuration;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using System.Xml.Serialization;
using Giraffe;

namespace Giraffe
{
    static class Program
    {
        static System.Threading.Mutex singleton = new Mutex(true, "GIRAFFE");

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            if (!singleton.WaitOne(TimeSpan.Zero, true))
            {
                Console.WriteLine("GIRAFFE is already running!");
                return;
            }

            if (args == null || args.Length == 0)
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new MainWindow());
            }
            else if (args.Length == 1)
            {
                string file = args[0];
                FileStream stream = null;

                try
                {
                    // Open the file
                    stream = new FileStream(file, FileMode.Open);
                    // Create the serializer
                    XmlSerializer serializer = new XmlSerializer(typeof (SimulationParameters));
                    // Deserialize the file into the simulation parameters object
                    SimulationParameters simulationParameters =
                        (SimulationParameters) serializer.Deserialize(stream);

                    string configPath = ConfigurationManager.AppSettings["ParameterDefinesFile"];
                    ParameterDefines parameterDefines = new ParameterDefines();
                    parameterDefines.LoadConfiguration(configPath);

                    AutoRunDialog autoRunDlg = new AutoRunDialog(simulationParameters, parameterDefines);
                    autoRunDlg.ShowDialog();
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (stream != null)
                    {
                        stream.Close();
                        stream.Dispose();
                    }
                }
            }
        }
    }
}