﻿namespace Giraffe
{
    class ValidatingExpressions
    {
        /// <summary>
        /// This regex matches all valid characters allowed in Windows file names.
        /// </summary>
        public const string ValidFileNameRegex = @"^[^\\\./:\*\?\""<>\|]{1}[^\\/:\*\?\""<>\|]{0,254}$";
        public const string InvalidFileNameMessage = @"A folder name can't contain any of the following characters: \ / : * ? \"" < > |";

        /// <summary>
        /// This regex matches a float between 0 and 1 with a maximum of two decimal places.
        /// </summary>
        public const string ValidProbabilityRegex = @"(^0($|\.{1}\d{1,2}$))|(^1($|\.{1}0{1,2}$))";
        public const string InvalidProbabilityMessage = @"Please enter a probability value between 0 and 1.";

        /// <summary>
        /// This regex matches a float between 0 and 1 with a maximum of five decimal places.
        /// </summary>
        public const string ValidConvergenceRegex = @"(^0($|\.{1}\d{1,5}$))|(^1($|\.{1}0{1,5}$))";
        public const string InvalidConvergenceMessage = @"Please enter a convergence value with up to five decimal places.";

        /// <summary>
        /// This regex matches a float (positive or negative) with a maximum of four decimal places.
        /// </summary>
        public const string ValidFloatRegex = @"^-{0,1}\d+(\.\d{1,4}){0,1}$";
        public const string InvalidFloatMessage = "Please enter a valid floating point number with up to four decimal places.";

        /// <summary>
        /// This regex matches an integer from 1 to 100 (with no leading zeroes).
        /// </summary>
        public const string ValidInteger1to100Regex = @"^(100|[1-9]{1}[0-9]?)$";
        public const string InvalidInteger1to100Message = "Please enter an integer between 1 and 100";
    }
}