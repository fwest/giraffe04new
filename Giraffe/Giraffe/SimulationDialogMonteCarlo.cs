﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using Giraffe;

namespace Giraffe
{
    /// <summary>
    /// Base class for Monte Carlo simulation dialogs
    /// </summary>
    public partial class SimulationDialogMonteCarlo : SimulationDialog
    {
        #region Properties

        /// <summary>
        /// Derived classes should override the app.config key for persisting the random seed
        /// </summary>
        protected virtual string RandomSeedConfigSetting { get { return string.Empty; } }
        /// <summary>
        /// Gets/sets the default random seed
        /// </summary>
        protected virtual string RandomSeed
        {
            get
            {
                return Configuration.AppSettings.Settings[RandomSeedConfigSetting].Value;
            }
            set
            {
                Configuration.AppSettings.Settings[RandomSeedConfigSetting].Value = value;
            }
        }

        /// <summary>
        /// Derived classes should override the app.config key for persisting the pipe repair rate file
        /// </summary>
        protected virtual string PipeRepairConfigSetting {get { return string.Empty; }}
        /// <summary>
        /// Gets/sets the default pipe repair rate file
        /// </summary>
        protected virtual string PipeRepairFileSavedPath
        {
            get
            {
                return Configuration.AppSettings.Settings[PipeRepairConfigSetting].Value;
            }
            set
            {
                Configuration.AppSettings.Settings[PipeRepairConfigSetting].Value = value;
            }
        }

        /// <summary>
        /// Derived classes should override the app.config key for persisting the nodal demand calibration flag
        /// </summary>
        protected virtual string NodalDemandConfigSetting { get { return string.Empty; } }
        /// <summary>
        /// Gets/sets the default timestamp flag
        /// </summary>
        protected virtual bool NodalDemandCalibration
        {
            get
            {
                return Convert.ToBoolean(Configuration.AppSettings.Settings[NodalDemandConfigSetting].Value);
            }

            set
            {
                Configuration.AppSettings.Settings[NodalDemandConfigSetting].Value = value.ToString();
            }
        }

        /// <summary>
        /// Derived classes should override the app.config key for persisting the regression equation
        /// </summary>
        protected virtual string RegressionConfigSetting { get { return string.Empty; } }
        /// <summary>
        /// Gets/sets the default regression equation
        /// </summary>
        protected virtual RegressionType RegressionEquation
        {
            get
            {
                string value = Configuration.AppSettings.Settings[RegressionConfigSetting].Value;

                if (!string.IsNullOrEmpty(value))
                {
                    return (RegressionType) Enum.Parse(typeof (RegressionType), value);
                }
                else
                {
                    return RegressionType.Mean;
                }
            }

            set
            {
                Configuration.AppSettings.Settings[RegressionConfigSetting].Value = Enum.GetName(typeof(RegressionType), value);
            }
        }

        /// <summary>
        /// Derived classes should override the app.config key for persisting the mean pressure file
        /// </summary>
        protected virtual string MeanPressureConfigSetting { get { return string.Empty; } }
        /// <summary>
        /// Gets/sets the default pipe repair rate file
        /// </summary>
        protected virtual string MeanPressureFileSavedPath
        {
            get
            {
                return Configuration.AppSettings.Settings[MeanPressureConfigSetting].Value;
            }
            set
            {
                Configuration.AppSettings.Settings[MeanPressureConfigSetting].Value = value;
            }
        }

        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        public SimulationDialogMonteCarlo()
        {
            InitializeComponent();
        }

        #region Control Event Handlers

        /// <summary>
        /// Handles the Browse button click for the Pipe Repair Rate File textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBrowsePipeRepair_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openDlg = new OpenFileDialog())
            {
                openDlg.InitialDirectory = ".";
                openDlg.Filter = "Definition Input files (*.inp)|*.inp";
                openDlg.RestoreDirectory = true;

                if (openDlg.ShowDialog() == DialogResult.OK)
                {
                    textBoxPipeRepairRateFile.Text = openDlg.FileName;
                }
            }
        }

        /// <summary>
        /// Handles the Edit button click event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonEditPipeRepair_Click(object sender, EventArgs e)
        {
            // If a pipe repair rate file is specified, edit it in a TextEditor dialog
            EditFile(textBoxPipeRepairRateFile.Text);
        }

        /// <summary>
        /// Handles the Nodal Demand Calibration checkbox to enable/disable nodal demand controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBoxNodalDemandCalibration_CheckedChanged(object sender, EventArgs e)
        {
            comboBoxRegressionEquation.Enabled = checkBoxNodalDemandCalibration.Checked;
            buttonBrowseMeanPressure.Enabled = checkBoxNodalDemandCalibration.Checked;
            buttonEditMeanPressure.Enabled = checkBoxNodalDemandCalibration.Checked;

            // Deselect the regression equation if not calibrating
            if (!checkBoxNodalDemandCalibration.Checked)
            {
                comboBoxRegressionEquation.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// Handles the Browse button click event for the Mean Pressure file.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBrowseMeanPressure_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openDlg = new OpenFileDialog())
            {
                openDlg.InitialDirectory = ".";
                openDlg.Filter = "Definition Input files (*.inp)|*.inp";
                openDlg.RestoreDirectory = true;

                if (openDlg.ShowDialog() == DialogResult.OK)
                {
                    textBoxMeanPressureFile.Text = openDlg.FileName;
                }
            }
        }

        /// <summary>
        /// Handles the Edit button click event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonEditMeanPressure_Click(object sender, EventArgs e)
        {
            // If a mean pressure file is specified, edit it in a TextEditor dialog
            EditFile(textBoxMeanPressureFile.Text);
        }

        /// <summary>
        /// Handles the Generate Pipe Repair Rate button click event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonGeneratePipeRepairRate_Click(object sender, EventArgs e)
        {
            Generate.GeneratePipeRepairRateFile(this);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Disables all controls during a simulation run.
        /// </summary>
        protected override void DisableAllControls()
        {
            base.DisableAllControls();
            textBoxRandomSeed.Enabled = false;
            textBoxPipeRepairRateFile.Enabled = false;
            buttonBrowsePipeRepair.Enabled = false;
            buttonEditPipeRepair.Enabled = false;
            checkBoxNodalDemandCalibration.Enabled = false;
            comboBoxRegressionEquation.Enabled = false;
            textBoxMeanPressureFile.Enabled = false;
            buttonBrowseMeanPressure.Enabled = false;
            buttonEditMeanPressure.Enabled = false;
            buttonGeneratePipeRepairRate.Enabled = false;
        }

        /// <summary>
        /// Enables all controls after a simulation run.
        /// </summary>
        protected override void EnableAllControls()
        {
            textBoxRandomSeed.Enabled = true;
            textBoxPipeRepairRateFile.Enabled = true;
            buttonBrowsePipeRepair.Enabled = true;
            buttonEditPipeRepair.Enabled = true;
            checkBoxNodalDemandCalibration.Enabled = true;
            comboBoxRegressionEquation.Enabled = true;
            textBoxMeanPressureFile.Enabled = true;
            buttonBrowseMeanPressure.Enabled = true;
            buttonEditMeanPressure.Enabled = true;
            buttonGeneratePipeRepairRate.Enabled = true;
            base.EnableAllControls();
        }

        /// <summary>
        /// Loads the persisted default input values
        /// </summary>
        protected override void LoadPersistedInputValues()
        {
            base.LoadPersistedInputValues();
            textBoxRandomSeed.Text = RandomSeed;
            textBoxPipeRepairRateFile.Text = PipeRepairFileSavedPath;
            checkBoxNodalDemandCalibration.Checked = NodalDemandCalibration;
            comboBoxRegressionEquation.SelectedIndex = (int) RegressionEquation;
            textBoxMeanPressureFile.Text = MeanPressureFileSavedPath;
        }

        /// <summary>
        /// Persists the current input values for re-use
        /// </summary>
        protected override void PersistInputValues()
        {
            RandomSeed = textBoxRandomSeed.Text;
            PipeRepairFileSavedPath = textBoxPipeRepairRateFile.Text;
            NodalDemandCalibration = checkBoxNodalDemandCalibration.Checked;
            RegressionEquation = (RegressionType) comboBoxRegressionEquation.SelectedIndex;
            MeanPressureFileSavedPath = textBoxMeanPressureFile.Text;
            base.PersistInputValues();
        }

        /// <summary>
        /// Assigns static tooltip text to the specified controls.
        /// </summary>
        protected override void AssignToolTips()
        {
            base.AssignToolTips();
            SetToolTip(textBoxRandomSeed, TooltipText.RandomSeed);
            SetToolTip(buttonBrowsePipeRepair, TooltipText.PipeRepairRateFile);
            SetToolTip(checkBoxNodalDemandCalibration, TooltipText.NodalDemandCal);
            SetToolTip(comboBoxRegressionEquation, TooltipText.RegressionEquation);
            SetToolTip(buttonBrowseMeanPressure, TooltipText.MeanPressureFile);
        }

        /// <summary>
        /// Validates other controls with unique needs.
        /// </summary>
        /// <returns>False if any control fails validation; otherwise, true is returned.</returns>
        protected override bool ValidateOtherControls()
        {
            bool valid = base.ValidateOtherControls();
            int n;

            // Validate random seed
            if (!(int.TryParse(textBoxRandomSeed.Text, out n)))
            {
                _errorProvider.SetError(textBoxRandomSeed, "Please enter a valid integer.");
                valid = false;
            }
            else
            {
                _errorProvider.SetError(textBoxRandomSeed, "");
            }

            return valid;
        }

        #endregion
    }
}