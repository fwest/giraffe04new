﻿using System;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace Giraffe
{
    public partial class HelpDialog : Form
    {
         /// <summary>
        /// Configuration path to web help file Name
        /// </summary>
        private const string WebHelpFileName = "WebHelpFileName";

        /// <summary>
        /// Holds fragment URI for jumping to links within web help file
        /// </summary>
        private string fragmentUri = string.Empty;
        /// <summary>
        /// Gets/set fragment URI for jumping to links within web help file
        /// </summary>
        private string FragmentUri
        {
            get
            {
                // If there is a fragment URI...
                if (!(string.IsNullOrEmpty(fragmentUri)))
                {
                    // ...check for a pre-pended pound character
                    if (!(fragmentUri.StartsWith("#")))
                    {
                        // Pre-pend the pound character if not provided
                       return "#" + fragmentUri;
                    }
                }

                return fragmentUri;
            }

            set
            {
                fragmentUri = value;
            }
        }

        /// <summary>
        ///  Default constructor
        /// </summary>
        /// <param name="fragmentUri">Context-sensitive URI for links within the help file</param>
        public HelpDialog(string fragmentUri)
        {
            InitializeComponent();

            try
            {
                // Set the fragment URI for context-sensitive link
                FragmentUri = fragmentUri;
                // Get the application folder
                string exeFolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                // Get the web help file from configuration
                string helpFile = ConfigurationManager.AppSettings[WebHelpFileName];
                // Create a Uri object with the information and append any fragment URI
                Uri helpFileUri = new Uri("file://" + exeFolder + "\\" + helpFile + FragmentUri);
                // Set the browser window to launch displaying the help file
                webBrowser1.Url = helpFileUri;
            }
            catch (Exception)
            {
                Uri errorUri = new Uri("file://c:\\dummy.htm");
                webBrowser1.Url = errorUri;
            }

        }
    }
}