﻿namespace Giraffe
{
    class TooltipText
    {
        // Simulation Dialog (base)
        public const string SimulationFolder = "Enter the name of the folder where simulation results will be written.";
        public const string TimestampFolder = "Check to automatically timestamp the simulation folder for each run.";
        public const string OutputFolder = "Select the folder where the simulation folder will be created.";
        public const string SelectSystemDefinitionFile = "Select the system definition file.";
        public const string EditSystemDefinitionFile = "Edit the system definition file.";
        public const string MinPressureToEliminate = "Enter the minimum pressure (in psi) to eliminate from the system.";
        public const string SimulationTime = "Enter the total number of hours to simulate.";
        public const string SimulationTimeStep = "Enter the simulation time step in hours.";

        // Deterministic Simulation
        public const string SelectPipeDamageFile = "Select the pipe damage file.";
        public const string EditPipeDamageFile = "Edit the pipe damage file.";

        // Monte Carlo (base)
        public const string RandomSeed = "Enter a seed value for the random number generator";
        public const string PipeRepairRateFile = "Select the pipe repair rate file.";
        public const string NodalDemandCal = "Check to enable nodal demand calibration.";
        public const string RegressionEquation = "Select the desired regression equation.";
        public const string MeanPressureFile = "Select the mean pressure file.";

        // Monte Carlo Fixed
        public const string NumberOfSimulations = "Enter the number of simulations to perform.";

        // Pipe Damage Probability - Break
        public const string BreakProCI = "Probability of pipe break, given pipe damage occurs, for cast iron pipes.";
        public const string BreakProDI = "Probability of pipe break, given pipe damage occurs, for ductile iron pipes.";
        public const string BreakProRS = "Probability of pipe break, given pipe damage occurs, for riveted steel pipes.";
        public const string BreakProCON = "Probability of pipe break, given pipe damage occurs, for concrete pipes.";
        public const string BreakProSTL = "Probability of pipe break, given pipe damage occurs, for welded steel pipes.";

        // Pipe Damage Probability - Leak
        public const string STLLeakRatio = "Probability of pipe leak, given pipe damage occurs, for welded steel pipes.";

        // Pipe Leakage Model - Annular Disengagement
        public const string Type1tD = "Thickness of annular space for leak type 1, annular disengagement, in the units of inches; t in Eqn. 4.5.";
        public const string Type1kD = "Ratio of actual leak area to the maximum possible leak area for leak type 1, annular disengagement; k in Eqn. 4.5.";

        // Pipe Leakage Model - Round Crack
        public const string Type2aD = "Opening angle of leak type 2, round crack, in the units of degrees; θ in Eqn. 4.7.";

        // Pipe Leakage Model - Longitudinal Crack
        public const string Type3kD = "Length of leak type 3, longitudinal crack, in the units of inches; L in Eqn. 4.11.";
        public const string Type3aD = "Opening angle of leak type 3, longitudinal crack, in the units of degrees; θ in Eqn. 4.11.";

        // Pipe Leakage Model - Local Loss of Pipe Wall
        public const string Type4kD = "Ratio of the length and width of leak type 4, local loss of pipe wall, to the pipe diameter and circumferential length, respectively; k1 and k2 in Eqn. 4.15.";

        // Pipe Leakage Model - Local Tear of Pipe Wall
        public const string Type5kD = "Ratio of the length of leak type 5, local tear of pipe wall, to the pipe circumferential length; k in Eqn. 4.19.";
        public const string Type5wD = "Width of the leak type 5, local tear of pipe wall, in the units of inches; w in Eqn. 4.19.";

        // Pipe Leakage Model - Leak - Cast Iron
        public const string CIType1D = "Probability of leak type 1 for cast iron pipelines.";
        public const string CIType2D = "Cumulative probability of leak types 1 to 2 for cast iron pipelines.";
        public const string CIType3D = "Cumulative probability of leak types 1 to 3 for cast iron pipelines.";
        public const string CIType4D = "Cumulative probability of leak types 1 to 4 for cast iron pipelines.";
        public const string CIType5D = "Cumulative probability of leak types 1 to 5 for cast iron pipelines.";

        // Pipe Leakage Model - Leak - Riveted Steel
        public const string RSType1D = "Probability of leak type 1 for riveted steel pipelines.";
        public const string RSType2D = "Cumulative probability of leak types 1 to 2 for riveted steel pipelines.";
        public const string RSType3D = "Cumulative probability of leak types 1 to 3 for riveted steel pipelines.";
        public const string RSType4D = "Cumulative probability of leak types 1 to 4 for riveted steel pipelines.";
        public const string RSType5D = "Cumulative probability of leak types 1 to 5 for riveted steel pipelines.";

        // Pipe Leakage Model - Leak - Concrete
        public const string CONType1D = "Probability of leak type 1 for concrete pipelines.";
        public const string CONType2D = "Cumulative probability of leak types 1 to 2 for concrete pipelines.";
        public const string CONType3D = "Cumulative probability of leak types 1 to 3 for concrete pipelines.";
        public const string CONType4D = "Cumulative probability of leak types 1 to 4 for concrete pipelines.";
        public const string CONType5D = "Cumulative probability of leak types 1 to 5 for concrete pipelines.";

        // Pipe Leakage Model - Leak - Ductile Iron
        public const string DIType1D = "Probability of leak type 1 for ductile iron pipelines.";
        public const string DIType2D = "Cumulative probability of leak types 1 to 2 for ductile iron pipelines.";
        public const string DIType3D = "Cumulative probability of leak types 1 to 3 for ductile iron pipelines.";
        public const string DIType4D = "Cumulative probability of leak types 1 to 4 for ductile iron pipelines.";
        public const string DIType5D = "Cumulative probability of leak types 1 to 5 for ductile iron pipelines.";

        // Pipe Leakage Model - Leak - Welded Steel
        public const string STLType1D = "Probability of leak type 1 for welded steel pipelines.";
        public const string STLType2D = "Cumulative probability of leak types 1 to 2 for welded steel pipelines.";
        public const string STLType3D = "Cumulative probability of leak types 1 to 3 for welded steel pipelines.";
        public const string STLType4D = "Cumulative probability of leak types 1 to 4 for welded steel pipelines.";
        public const string STLType5D = "Cumulative probability of leak types 1 to 5 for welded steel pipelines.";

        // Pipe Leakage Model - Leak - Other Materials
        public const string OtherType1D = "Probability of leak type 1 for welded pipelines with other materials.";
        public const string OtherType2D = "Cumulative probability of leak types 1 to 2 for pipelines with other materials.";
        public const string OtherType3D = "Cumulative probability of leak types 1 to 3 for pipelines with other materials.";
        public const string OtherType4D = "Cumulative probability of leak types 1 to 4 for pipelines with other materials.";
        public const string OtherType5D = "Cumulative probability of leak types 1 to 5 for pipelines with other materials.";
        
        // Nodal Demand Calibration - Mean
        public const string MiiMP = "Intercept of the intercept term of the linear regression between normalized demand and repair rate for mean regression; II in Eqn. 5.2.";
        public const string MisMP = "Slope of the intercept term of the linear regression between normalized demand and repair rate for mean regression; IS in Eqn. 5.2.";
        public const string MsiMP = "Intercept of the slope term of the linear regression between normalized demand and repair rate for mean regression; SI in Eqn. 5.2.";
        public const string MssMP = "Slope of the slope term of the linear regression between normalized demand and repair rate for mean regression; SS in Eqn. 5.2.";
        public const string MiiSD = "Intercept of the linear regression between the standard deviation of mean intercept and mean pressure; see Eqn. 5.2.";
        public const string MisSD = "Slope of the linear regression between the standard deviation of mean intercept and mean pressure; see Eqn. 5.2.";
        public const string MsiSD = "Intercept of the linear regression between the standard deviation of mean slope and mean pressure; see Eqn. 5.2.";
        public const string MssSD = "Slope of the linear regression between the standard deviation of mean slope and mean pressure; see Eqn. 5.2.";

        // Nodal Demand Calibration - 90% Confidence
        public const string UiiMP = "Intercept of the intercept term of the linear regression between normalized demand and repair rate for 90% confidence level regression; II in Eqn. 5.3.";
        public const string UisMP = "Slope of the intercept term of the linear regression between normalized demand and repair rate for 90% confidence level regression; IS in Eqn. 5.3.";
        public const string UsiMP = "Intercept of the intercept term of the linear regression between normalized demand and repair rate for 90% confidence level regression; SI in Eqn. 5.3.";
        public const string UssMP = "Slope of the intercept term of the linear regression between normalized demand and repair rate for 90% confidence level regression; SS in Eqn. 5.3.";
        public const string mRRCap = "Lower bound of repair rate for Monte Carlo simulation, below which it is assumed that no pipe damage occurs. The lower bound is to avoid the numerical stability problems when using the Eqn. 4.20 to generation locations of pipe damage.";
    }
}