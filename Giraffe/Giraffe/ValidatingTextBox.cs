﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace GiraffeUI
{
    /// <summary>
    /// This composite control combines a Textbox with built-in validation
    /// and a label for displaying messages to the user regarding invalid input.
    /// </summary>
    public partial class ValidatingTextBox : UserControl
    {
        #region Properties

        /// <summary>
        /// Flag used to indicate the first mouse hover over the TextBox control.
        /// </summary>
        private bool _firstMouseHover = true;

        /// <summary>
        /// ErrorProvider used to notify the user of failed validation
        /// </summary>
        public ErrorProvider ErrorProvider { get; set; }

        /// <summary>
        /// ToolTipProvider used to display tooltip text to the user
        /// </summary>
        public ToolTip ToolTipProvider { get; set; }

        /// <summary>
        /// Text property passes through to the Textbox.Text property
        /// </summary>
        [Description("The text displayed in the textbox."), Category("Appearance")]
        public new string Text
        {
            get
            {
                return textBoxValue.Text;
            }

            set
            {
                textBoxValue.Text = value;
            }
        }

        /// <summary>
        /// ReadOnly property passes through to the Textbox.ReadOnly property
        /// </summary>
        [Description("Controls whether the text in the textbox control can be changed or not."), Category("Behavior")]
        public bool ReadOnly
        {
            get
            {
                return textBoxValue.ReadOnly;
            }

            set
            {
                textBoxValue.ReadOnly = value;
            }
        }

        /// <summary>
        /// If true, the textbox must contain a value (i.e. it cannot be left blank)
        /// </summary>
        [Description("Textbox must contain a valid value if required."), Category("Behavior")]
        public bool Required { get; set; }

        /// <summary>
        /// A regular expression used to validate values and/or formatting
        /// </summary>
        [Description("Regular expression used to match valid content."), Category("Behavior")]
        public string ValidatingRegex { get; set; }

        /// <summary>
        /// If content fails regex validation, this message will be displayed to the user.
        /// </summary>
        /// <example>Given the ValidatingRegex value of "^[a-z0-9_-]{3,16}$", the accompanying message
        /// could be, "Please enter 3-16 alphanumeric characters."</example>
        /// <remarks>The example above assumes that the Regex.Match is being perform with RegexOptions.IgnoreCase
        /// set to true.  The implementation of Validate() does not ignore case so case insensitivity must be
        /// explicitly defined in the regular expression.</remarks>
        [Description("Display message to accompany validating regular expression."), Category("Behavior")]
        public string ValidatingRegexMessage { get; set; }

        /// <summary>
        /// Occurs when the control fails validation.
        /// </summary>
        [Description("Occurs when the control fails validation."), Category("Focus"), Browsable(true)]
        public event ValidationEventHandler FailedValidation;

        /// <summary>
        /// Occurs when the control passes validation.
        /// </summary>
        [Description("Occurs when the control passes validation."), Category("Focus"), Browsable(true)]
        public event ValidationEventHandler PassedValidation;

        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        public ValidatingTextBox()
        {
            InitializeComponent();
        }

        #region Control Event Handlers

        /// <summary>
        /// Handles the Validated event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxValue_Validated(object sender, EventArgs e)
        {
            if (ErrorProvider != null)
            {
                ErrorProvider.SetError((Control) this, "");
            }
        }

        /// <summary>
        /// Handles the Validating event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">If the textbox content is invalid, e.Cancel is set to true</param>
        private void textBoxValue_Validating(object sender, CancelEventArgs e)
        {
            // Display message if the textbox content is invalid
            string errorMessage = string.Empty;

            try
            {
                // If the call to Validate returns false, set the Cancel flag
                if (!Validate(textBoxValue.Text, out errorMessage))
                {
                    e.Cancel = true;
                }

            }
            finally
            {
                if (e.Cancel == true)
                {
                    // If the validation failed (i.e. cancel = true) then fire the failed event...
                    OnFailedValidation(new ValidationEventArgs(ValidationState.Invalid, errorMessage));

                    // ...and set the error message if an ErrorProvider was set
                    if (ErrorProvider != null)
                    {
                        ErrorProvider.SetError(this, errorMessage);
                    }
                }
                else
                {
                    OnPassedValidation(new ValidationEventArgs(ValidationState.Valid, ""));
                }
            }
        }

        /// <summary>
        /// Handles the TextBox's MouseHover event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxValue_MouseHover(object sender, EventArgs e)
        {
            // If this is the first mouse hover detected and there is an assigned ToolTipProvider...
            if (_firstMouseHover && ToolTipProvider != null)
            {
                // Set the TextBox tooltip by coping the parent control's tooltip text
                ToolTipProvider.SetToolTip(textBoxValue, ToolTipProvider.GetToolTip(this));
                // Clear the flag after first use
                _firstMouseHover = false;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// This method performs the validation of the
        /// </summary>
        /// <param name="value">The value to validate i.e. the contents of the textbox.</param>
        /// <param name="errorMessage">If the value is invalid, an associated message will be returned for display to the user.</param>
        /// <returns>True if the value is valid; false, if not.</returns>
        protected bool Validate(string value, out string errorMessage)
        {
            // Assume valid (Validating method must explicitly Cancel to fail validation)
            bool flag = true;
            errorMessage = string.Empty;

            // If this textbox contains a required input field, check for blank
            if (Required && value == string.Empty)
            {
                errorMessage = "Please enter value for required field.";
                flag = false;
            }
            else if (!string.IsNullOrEmpty(ValidatingRegex)) // If a validating expression has been defined attempt to match the textbox content
            {
                // Match the value with the regex
                Match match = Regex.Match( value, ValidatingRegex);

                // If no match...
                if (!match.Success)
                {
                    // ...pass back the defined validating regex message and set the flag to invalid
                    errorMessage = ValidatingRegexMessage;
                    flag = false;
                }
            }

            return flag;
        }

        /// <summary>
        /// Fires the FailedValidation event.
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnFailedValidation(ValidationEventArgs e)
        {
            if (FailedValidation != null)
            {
                FailedValidation(this, e);
            }
        }

        /// <summary>
        /// Fires the PassedValidation event.
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnPassedValidation(ValidationEventArgs e)
        {
            if (PassedValidation != null)
            {
                PassedValidation(this, e);
            }
        }

        #endregion
    }
}

/// <summary>
/// Event delegate for Validation-related events
/// </summary>
/// <param name="sender"></param>
/// <param name="e"></param>
public delegate void ValidationEventHandler(object sender, ValidationEventArgs e);

/// <summary>
/// Validation state enumeration
/// </summary>
public enum ValidationState
{
    Invalid,
    Valid,
}

/// <summary>
/// Event arguments for Validation events
/// </summary>
public class ValidationEventArgs
{
    /// <summary>
    /// Validation state (i.e. valid or invalid)
    /// </summary>
    public ValidationState State = ValidationState.Invalid;

    /// <summary>
    /// Message that explains validation state, usually an error message if invalid.
    /// </summary>
    public string Message = string.Empty;

    /// <summary>
    /// Constructor
    /// </summary>
    public ValidationEventArgs()
    {
    }

    /// <summary>
    /// Overloaded constructor
    /// </summary>
    /// <param name="state">Validation state to pass with the event.</param>
    /// <param name="message">Message to pass with the event.</param>
    public ValidationEventArgs(ValidationState state, string message)
    {
        this.State = state;
        this.Message = message;
    }
}