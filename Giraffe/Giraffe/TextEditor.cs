﻿using System;
using System.Windows.Forms;

namespace Giraffe
{
    public partial class TextEditor : Form
    {
        #region Properties

        /// <summary>
        /// Indicates if the text has changed since it was loaded
        /// </summary>
        private bool _textHasBeenChanged = false;
        /// <summary>
        /// The file path of the currently loaded file
        /// </summary>
        private readonly string _currentFile = string.Empty;

        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="filePath">Full path of file to load</param>
        public TextEditor(string filePath)
        {
            InitializeComponent();

            // Save the file path
            _currentFile = filePath;
        }

        #region Control Event Handlers

        /// <summary>
        /// Handles the text changed event for the RichTextBox control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            // Set the "text changed" flag
            _textHasBeenChanged = true;
            // Enable the Save button
            buttonSave.Enabled = true;
        }

        #endregion

        #region Form Event Handlers

        /// <summary>
        /// Handles the form load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextEditor_Load(object sender, EventArgs e)
        {
            // Load the specified file
            richTextBox1.LoadFile(_currentFile, RichTextBoxStreamType.PlainText);
            // Display the file path in the dialog title
            this.Text = _currentFile;
            // Reset the changed flag (due to file loading)
            _textHasBeenChanged = false;
            // Disable the Save button
            buttonSave.Enabled = false;
        }

        /// <summary>
        /// Handles the form closing event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextEditor_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Perform actions based on the form's dialog result
            switch (this.DialogResult)
            {
                    // Save button
                case DialogResult.OK:
                    SaveFile(_currentFile);
                    break;

                    // Cancel button or form close button
                case DialogResult.Cancel:
                    // If the text has changed, warn the user
                    if (_textHasBeenChanged)
                    {
                        // Prompt the user
                        DialogResult dr = MessageBoxEx.Show(this, "The text has changed. Do you want to save the changes?",
                            "Save Changes?",
                            MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);

                        switch (dr)
                        {
                                // Yes, save the file
                            case DialogResult.Yes:
                                SaveFile(_currentFile);
                                break;
                                // Cancel, don't close the form
                            case DialogResult.Cancel:
                                e.Cancel = true;
                                break;
                        }
                    }
                    break;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Saves the specified file
        /// </summary>
        /// <param name="filePath">Full file path where the text will be saved</param>
        private void SaveFile(string filePath)
        {
            // Save the content as plain text
            richTextBox1.SaveFile(filePath, RichTextBoxStreamType.PlainText);
        }

        #endregion
    }
}
