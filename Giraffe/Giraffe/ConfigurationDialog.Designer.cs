﻿namespace Giraffe
{
    partial class ConfigurationDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigurationDialog));
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonClose = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControlConfiguration = new System.Windows.Forms.TabControl();
            this.tabPageOptions = new System.Windows.Forms.TabPage();
            this.groupBoxConvergence = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxCOVFlexible = new GiraffeUI.ValidatingTextBox();
            this.textBoxMeanFlexible = new GiraffeUI.ValidatingTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxMaxNSimu = new GiraffeUI.ValidatingTextBox();
            this.tabPageNodalDemand = new System.Windows.Forms.TabPage();
            this.groupBoxConfidence = new System.Windows.Forms.GroupBox();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.textBoxmRRCap = new GiraffeUI.ValidatingTextBox();
            this.textBoxUssMP = new GiraffeUI.ValidatingTextBox();
            this.textBoxUsiMP = new GiraffeUI.ValidatingTextBox();
            this.textBoxUisMP = new GiraffeUI.ValidatingTextBox();
            this.textBoxUiiMP = new GiraffeUI.ValidatingTextBox();
            this.groupBoxMean = new System.Windows.Forms.GroupBox();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.textBoxMssSD = new GiraffeUI.ValidatingTextBox();
            this.textBoxMsiSD = new GiraffeUI.ValidatingTextBox();
            this.textBoxMisSD = new GiraffeUI.ValidatingTextBox();
            this.textBoxMiiSD = new GiraffeUI.ValidatingTextBox();
            this.textBoxMssMP = new GiraffeUI.ValidatingTextBox();
            this.textBoxMsiMP = new GiraffeUI.ValidatingTextBox();
            this.textBoxMisMP = new GiraffeUI.ValidatingTextBox();
            this.textBoxMiiMP = new GiraffeUI.ValidatingTextBox();
            this.tabPagePipeLeakage = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.textBoxType5wD = new GiraffeUI.ValidatingTextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.textBoxType5kD = new GiraffeUI.ValidatingTextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label29 = new System.Windows.Forms.Label();
            this.textBoxType4kD = new GiraffeUI.ValidatingTextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label32 = new System.Windows.Forms.Label();
            this.textBoxType3aD = new GiraffeUI.ValidatingTextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.textBoxType3kD = new GiraffeUI.ValidatingTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label33 = new System.Windows.Forms.Label();
            this.textBoxType2aD = new GiraffeUI.ValidatingTextBox();
            this.groupBoxLeakOther = new System.Windows.Forms.GroupBox();
            this.textBoxOtherType3D = new GiraffeUI.ValidatingTextBox();
            this.textBoxOtherType2D = new GiraffeUI.ValidatingTextBox();
            this.textBoxOtherType4D = new GiraffeUI.ValidatingTextBox();
            this.textBoxOtherType5D = new GiraffeUI.ValidatingTextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.textBoxOtherType1D = new GiraffeUI.ValidatingTextBox();
            this.groupBoxLeakSTL = new System.Windows.Forms.GroupBox();
            this.textBoxSTLType3D = new GiraffeUI.ValidatingTextBox();
            this.textBoxSTLType2D = new GiraffeUI.ValidatingTextBox();
            this.textBoxSTLType4D = new GiraffeUI.ValidatingTextBox();
            this.textBoxSTLType5D = new GiraffeUI.ValidatingTextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.textBoxSTLType1D = new GiraffeUI.ValidatingTextBox();
            this.groupBoxLeakRS = new System.Windows.Forms.GroupBox();
            this.textBoxRSType3D = new GiraffeUI.ValidatingTextBox();
            this.textBoxRSType2D = new GiraffeUI.ValidatingTextBox();
            this.textBoxRSType4D = new GiraffeUI.ValidatingTextBox();
            this.textBoxRSType5D = new GiraffeUI.ValidatingTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.textBoxRSType1D = new GiraffeUI.ValidatingTextBox();
            this.groupBoxLeakDI = new System.Windows.Forms.GroupBox();
            this.textBoxDIType3D = new GiraffeUI.ValidatingTextBox();
            this.textBoxDIType2D = new GiraffeUI.ValidatingTextBox();
            this.textBoxDIType4D = new GiraffeUI.ValidatingTextBox();
            this.textBoxDIType5D = new GiraffeUI.ValidatingTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.textBoxDIType1D = new GiraffeUI.ValidatingTextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.textBoxType1kD = new GiraffeUI.ValidatingTextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.textBoxType1tD = new GiraffeUI.ValidatingTextBox();
            this.groupBoxLeakCI = new System.Windows.Forms.GroupBox();
            this.textBoxCIType3D = new GiraffeUI.ValidatingTextBox();
            this.textBoxCIType2D = new GiraffeUI.ValidatingTextBox();
            this.textBoxCIType4D = new GiraffeUI.ValidatingTextBox();
            this.textBoxCIType5D = new GiraffeUI.ValidatingTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.textBoxCIType1D = new GiraffeUI.ValidatingTextBox();
            this.tabPagePipeDamage = new System.Windows.Forms.TabPage();
            this.groupBoxLeakDamage = new System.Windows.Forms.GroupBox();
            this.textBoxRSLeakRatio = new GiraffeUI.ValidatingTextBox();
            this.textBoxDILeakRatio = new GiraffeUI.ValidatingTextBox();
            this.textBoxSTLLeakRatio = new GiraffeUI.ValidatingTextBox();
            this.textBoxCONLeakRatio = new GiraffeUI.ValidatingTextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.textBoxCILeakRatio = new GiraffeUI.ValidatingTextBox();
            this.groupBoxBreakDamage = new System.Windows.Forms.GroupBox();
            this.textBoxBreakProRS = new GiraffeUI.ValidatingTextBox();
            this.textBoxBreakProDI = new GiraffeUI.ValidatingTextBox();
            this.textBoxBreakProSTL = new GiraffeUI.ValidatingTextBox();
            this.textBoxBreakProCON = new GiraffeUI.ValidatingTextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.textBoxBreakProCI = new GiraffeUI.ValidatingTextBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.buttonLoad = new System.Windows.Forms.Button();
            this.textBoxConfigFile = new GiraffeUI.ValidatingTextBox();
            this.tabControlConfiguration.SuspendLayout();
            this.tabPageOptions.SuspendLayout();
            this.groupBoxConvergence.SuspendLayout();
            this.tabPageNodalDemand.SuspendLayout();
            this.groupBoxConfidence.SuspendLayout();
            this.groupBoxMean.SuspendLayout();
            this.tabPagePipeLeakage.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBoxLeakOther.SuspendLayout();
            this.groupBoxLeakSTL.SuspendLayout();
            this.groupBoxLeakRS.SuspendLayout();
            this.groupBoxLeakDI.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBoxLeakCI.SuspendLayout();
            this.tabPagePipeDamage.SuspendLayout();
            this.groupBoxLeakDamage.SuspendLayout();
            this.groupBoxBreakDamage.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonSave
            // 
            this.buttonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSave.Location = new System.Drawing.Point(534, 454);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(75, 23);
            this.buttonSave.TabIndex = 101;
            this.buttonSave.Text = "&Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonClose
            // 
            this.buttonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClose.Location = new System.Drawing.Point(615, 454);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(75, 23);
            this.buttonClose.TabIndex = 102;
            this.buttonClose.Text = "&Close";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 425);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 102;
            this.label1.Text = "Configuration File";
            // 
            // tabControlConfiguration
            // 
            this.tabControlConfiguration.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControlConfiguration.Controls.Add(this.tabPageOptions);
            this.tabControlConfiguration.Controls.Add(this.tabPageNodalDemand);
            this.tabControlConfiguration.Controls.Add(this.tabPagePipeLeakage);
            this.tabControlConfiguration.Controls.Add(this.tabPagePipeDamage);
            this.tabControlConfiguration.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.tabControlConfiguration.ItemSize = new System.Drawing.Size(175, 18);
            this.tabControlConfiguration.Location = new System.Drawing.Point(2, 2);
            this.tabControlConfiguration.Name = "tabControlConfiguration";
            this.tabControlConfiguration.SelectedIndex = 0;
            this.tabControlConfiguration.Size = new System.Drawing.Size(708, 412);
            this.tabControlConfiguration.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControlConfiguration.TabIndex = 99;
            this.tabControlConfiguration.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.tabControlConfiguration_DrawItem);
            // 
            // tabPageOptions
            // 
            this.tabPageOptions.Controls.Add(this.groupBoxConvergence);
            this.tabPageOptions.Controls.Add(this.label4);
            this.tabPageOptions.Controls.Add(this.textBoxMaxNSimu);
            this.tabPageOptions.Location = new System.Drawing.Point(4, 22);
            this.tabPageOptions.Name = "tabPageOptions";
            this.tabPageOptions.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageOptions.Size = new System.Drawing.Size(700, 386);
            this.tabPageOptions.TabIndex = 0;
            this.tabPageOptions.Text = "Monte Carlo Flexible Options";
            this.tabPageOptions.UseVisualStyleBackColor = true;
            // 
            // groupBoxConvergence
            // 
            this.groupBoxConvergence.Controls.Add(this.label3);
            this.groupBoxConvergence.Controls.Add(this.label2);
            this.groupBoxConvergence.Controls.Add(this.textBoxCOVFlexible);
            this.groupBoxConvergence.Controls.Add(this.textBoxMeanFlexible);
            this.groupBoxConvergence.Location = new System.Drawing.Point(6, 12);
            this.groupBoxConvergence.Name = "groupBoxConvergence";
            this.groupBoxConvergence.Size = new System.Drawing.Size(209, 79);
            this.groupBoxConvergence.TabIndex = 0;
            this.groupBoxConvergence.TabStop = false;
            this.groupBoxConvergence.Text = "Convergence Criteria";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Difference of COV";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Difference of Mean";
            // 
            // textBoxCOVFlexible
            // 
            this.textBoxCOVFlexible.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxCOVFlexible.ErrorProvider = null;
            this.textBoxCOVFlexible.Location = new System.Drawing.Point(110, 45);
            this.textBoxCOVFlexible.Name = "textBoxCOVFlexible";
            this.textBoxCOVFlexible.ReadOnly = false;
            this.textBoxCOVFlexible.Required = true;
            this.textBoxCOVFlexible.Size = new System.Drawing.Size(85, 20);
            this.textBoxCOVFlexible.TabIndex = 2;
            this.textBoxCOVFlexible.ToolTipProvider = null;
            this.textBoxCOVFlexible.ValidatingRegex = null;
            this.textBoxCOVFlexible.ValidatingRegexMessage = null;
            this.textBoxCOVFlexible.FailedValidation += new ValidationEventHandler(this.tabPageOptions_FailedValidation);
            this.textBoxCOVFlexible.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // textBoxMeanFlexible
            // 
            this.textBoxMeanFlexible.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxMeanFlexible.ErrorProvider = null;
            this.textBoxMeanFlexible.Location = new System.Drawing.Point(110, 19);
            this.textBoxMeanFlexible.Name = "textBoxMeanFlexible";
            this.textBoxMeanFlexible.ReadOnly = false;
            this.textBoxMeanFlexible.Required = true;
            this.textBoxMeanFlexible.Size = new System.Drawing.Size(85, 20);
            this.textBoxMeanFlexible.TabIndex = 1;
            this.textBoxMeanFlexible.ToolTipProvider = null;
            this.textBoxMeanFlexible.ValidatingRegex = null;
            this.textBoxMeanFlexible.ValidatingRegexMessage = null;
            this.textBoxMeanFlexible.FailedValidation += new ValidationEventHandler(this.tabPageOptions_FailedValidation);
            this.textBoxMeanFlexible.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(262, 46);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(159, 13);
            this.label4.TabIndex = 104;
            this.label4.Text = "Maximum Number of Simulations";
            // 
            // textBoxMaxNSimu
            // 
            this.textBoxMaxNSimu.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxMaxNSimu.ErrorProvider = null;
            this.textBoxMaxNSimu.Location = new System.Drawing.Point(427, 46);
            this.textBoxMaxNSimu.Name = "textBoxMaxNSimu";
            this.textBoxMaxNSimu.ReadOnly = false;
            this.textBoxMaxNSimu.Required = true;
            this.textBoxMaxNSimu.Size = new System.Drawing.Size(85, 20);
            this.textBoxMaxNSimu.TabIndex = 1;
            this.textBoxMaxNSimu.ToolTipProvider = null;
            this.textBoxMaxNSimu.ValidatingRegex = null;
            this.textBoxMaxNSimu.ValidatingRegexMessage = null;
            this.textBoxMaxNSimu.FailedValidation += new ValidationEventHandler(this.tabPageOptions_FailedValidation);
            this.textBoxMaxNSimu.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // tabPageNodalDemand
            // 
            this.tabPageNodalDemand.Controls.Add(this.groupBoxConfidence);
            this.tabPageNodalDemand.Controls.Add(this.groupBoxMean);
            this.tabPageNodalDemand.Location = new System.Drawing.Point(4, 22);
            this.tabPageNodalDemand.Name = "tabPageNodalDemand";
            this.tabPageNodalDemand.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageNodalDemand.Size = new System.Drawing.Size(700, 386);
            this.tabPageNodalDemand.TabIndex = 1;
            this.tabPageNodalDemand.Text = "Nodal Demand Calibration";
            this.tabPageNodalDemand.UseVisualStyleBackColor = true;
            // 
            // groupBoxConfidence
            // 
            this.groupBoxConfidence.Controls.Add(this.label48);
            this.groupBoxConfidence.Controls.Add(this.label49);
            this.groupBoxConfidence.Controls.Add(this.label50);
            this.groupBoxConfidence.Controls.Add(this.label51);
            this.groupBoxConfidence.Controls.Add(this.label52);
            this.groupBoxConfidence.Controls.Add(this.textBoxmRRCap);
            this.groupBoxConfidence.Controls.Add(this.textBoxUssMP);
            this.groupBoxConfidence.Controls.Add(this.textBoxUsiMP);
            this.groupBoxConfidence.Controls.Add(this.textBoxUisMP);
            this.groupBoxConfidence.Controls.Add(this.textBoxUiiMP);
            this.groupBoxConfidence.Location = new System.Drawing.Point(6, 149);
            this.groupBoxConfidence.Name = "groupBoxConfidence";
            this.groupBoxConfidence.Size = new System.Drawing.Size(160, 157);
            this.groupBoxConfidence.TabIndex = 3;
            this.groupBoxConfidence.TabStop = false;
            this.groupBoxConfidence.Text = "90% Confidence";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(33, 128);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(21, 14);
            this.label48.TabIndex = 34;
            this.label48.Text = "RR";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(40, 102);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(14, 14);
            this.label49.TabIndex = 33;
            this.label49.Text = "S";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(33, 76);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(21, 14);
            this.label50.TabIndex = 32;
            this.label50.Text = "SI";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(40, 50);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(14, 14);
            this.label51.TabIndex = 31;
            this.label51.Text = "I";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(33, 24);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(21, 14);
            this.label52.TabIndex = 30;
            this.label52.Text = "II";
            // 
            // textBoxmRRCap
            // 
            this.textBoxmRRCap.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxmRRCap.ErrorProvider = null;
            this.textBoxmRRCap.Location = new System.Drawing.Point(60, 123);
            this.textBoxmRRCap.Name = "textBoxmRRCap";
            this.textBoxmRRCap.ReadOnly = false;
            this.textBoxmRRCap.Required = true;
            this.textBoxmRRCap.Size = new System.Drawing.Size(85, 20);
            this.textBoxmRRCap.TabIndex = 5;
            this.textBoxmRRCap.ToolTipProvider = null;
            this.textBoxmRRCap.ValidatingRegex = null;
            this.textBoxmRRCap.ValidatingRegexMessage = null;
            this.textBoxmRRCap.FailedValidation += new ValidationEventHandler(this.tabPageNodalDemand_FailedValidation);
            this.textBoxmRRCap.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // textBoxUssMP
            // 
            this.textBoxUssMP.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxUssMP.ErrorProvider = null;
            this.textBoxUssMP.Location = new System.Drawing.Point(60, 97);
            this.textBoxUssMP.Name = "textBoxUssMP";
            this.textBoxUssMP.ReadOnly = false;
            this.textBoxUssMP.Required = true;
            this.textBoxUssMP.Size = new System.Drawing.Size(85, 20);
            this.textBoxUssMP.TabIndex = 4;
            this.textBoxUssMP.ToolTipProvider = null;
            this.textBoxUssMP.ValidatingRegex = null;
            this.textBoxUssMP.ValidatingRegexMessage = null;
            this.textBoxUssMP.FailedValidation += new ValidationEventHandler(this.tabPageNodalDemand_FailedValidation);
            this.textBoxUssMP.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // textBoxUsiMP
            // 
            this.textBoxUsiMP.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxUsiMP.ErrorProvider = null;
            this.textBoxUsiMP.Location = new System.Drawing.Point(60, 71);
            this.textBoxUsiMP.Name = "textBoxUsiMP";
            this.textBoxUsiMP.ReadOnly = false;
            this.textBoxUsiMP.Required = true;
            this.textBoxUsiMP.Size = new System.Drawing.Size(85, 20);
            this.textBoxUsiMP.TabIndex = 3;
            this.textBoxUsiMP.ToolTipProvider = null;
            this.textBoxUsiMP.ValidatingRegex = null;
            this.textBoxUsiMP.ValidatingRegexMessage = null;
            this.textBoxUsiMP.FailedValidation += new ValidationEventHandler(this.tabPageNodalDemand_FailedValidation);
            this.textBoxUsiMP.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // textBoxUisMP
            // 
            this.textBoxUisMP.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxUisMP.ErrorProvider = null;
            this.textBoxUisMP.Location = new System.Drawing.Point(60, 45);
            this.textBoxUisMP.Name = "textBoxUisMP";
            this.textBoxUisMP.ReadOnly = false;
            this.textBoxUisMP.Required = true;
            this.textBoxUisMP.Size = new System.Drawing.Size(85, 20);
            this.textBoxUisMP.TabIndex = 2;
            this.textBoxUisMP.ToolTipProvider = null;
            this.textBoxUisMP.ValidatingRegex = null;
            this.textBoxUisMP.ValidatingRegexMessage = null;
            this.textBoxUisMP.FailedValidation += new ValidationEventHandler(this.tabPageNodalDemand_FailedValidation);
            this.textBoxUisMP.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // textBoxUiiMP
            // 
            this.textBoxUiiMP.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxUiiMP.ErrorProvider = null;
            this.textBoxUiiMP.Location = new System.Drawing.Point(60, 19);
            this.textBoxUiiMP.Name = "textBoxUiiMP";
            this.textBoxUiiMP.ReadOnly = false;
            this.textBoxUiiMP.Required = true;
            this.textBoxUiiMP.Size = new System.Drawing.Size(85, 20);
            this.textBoxUiiMP.TabIndex = 1;
            this.textBoxUiiMP.ToolTipProvider = null;
            this.textBoxUiiMP.ValidatingRegex = null;
            this.textBoxUiiMP.ValidatingRegexMessage = null;
            this.textBoxUiiMP.FailedValidation += new ValidationEventHandler(this.tabPageNodalDemand_FailedValidation);
            this.textBoxUiiMP.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // groupBoxMean
            // 
            this.groupBoxMean.Controls.Add(this.label53);
            this.groupBoxMean.Controls.Add(this.label54);
            this.groupBoxMean.Controls.Add(this.label55);
            this.groupBoxMean.Controls.Add(this.label56);
            this.groupBoxMean.Controls.Add(this.label57);
            this.groupBoxMean.Controls.Add(this.textBoxMssSD);
            this.groupBoxMean.Controls.Add(this.textBoxMsiSD);
            this.groupBoxMean.Controls.Add(this.textBoxMisSD);
            this.groupBoxMean.Controls.Add(this.textBoxMiiSD);
            this.groupBoxMean.Controls.Add(this.textBoxMssMP);
            this.groupBoxMean.Controls.Add(this.textBoxMsiMP);
            this.groupBoxMean.Controls.Add(this.textBoxMisMP);
            this.groupBoxMean.Controls.Add(this.textBoxMiiMP);
            this.groupBoxMean.Location = new System.Drawing.Point(6, 12);
            this.groupBoxMean.Name = "groupBoxMean";
            this.groupBoxMean.Size = new System.Drawing.Size(287, 131);
            this.groupBoxMean.TabIndex = 2;
            this.groupBoxMean.TabStop = false;
            this.groupBoxMean.Text = "Mean";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(166, 76);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(14, 14);
            this.label53.TabIndex = 32;
            this.label53.Text = "I";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(40, 102);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(14, 14);
            this.label54.TabIndex = 31;
            this.label54.Text = "S";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(33, 76);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(21, 14);
            this.label55.TabIndex = 30;
            this.label55.Text = "SI";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(40, 50);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(14, 14);
            this.label56.TabIndex = 29;
            this.label56.Text = "I";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(33, 24);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(21, 14);
            this.label57.TabIndex = 28;
            this.label57.Text = "II";
            // 
            // textBoxMssSD
            // 
            this.textBoxMssSD.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxMssSD.ErrorProvider = null;
            this.textBoxMssSD.Location = new System.Drawing.Point(186, 97);
            this.textBoxMssSD.Name = "textBoxMssSD";
            this.textBoxMssSD.ReadOnly = false;
            this.textBoxMssSD.Required = true;
            this.textBoxMssSD.Size = new System.Drawing.Size(85, 20);
            this.textBoxMssSD.TabIndex = 8;
            this.textBoxMssSD.ToolTipProvider = null;
            this.textBoxMssSD.ValidatingRegex = null;
            this.textBoxMssSD.ValidatingRegexMessage = null;
            this.textBoxMssSD.FailedValidation += new ValidationEventHandler(this.tabPageNodalDemand_FailedValidation);
            this.textBoxMssSD.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // textBoxMsiSD
            // 
            this.textBoxMsiSD.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxMsiSD.ErrorProvider = null;
            this.textBoxMsiSD.Location = new System.Drawing.Point(186, 71);
            this.textBoxMsiSD.Name = "textBoxMsiSD";
            this.textBoxMsiSD.ReadOnly = false;
            this.textBoxMsiSD.Required = true;
            this.textBoxMsiSD.Size = new System.Drawing.Size(85, 20);
            this.textBoxMsiSD.TabIndex = 7;
            this.textBoxMsiSD.ToolTipProvider = null;
            this.textBoxMsiSD.ValidatingRegex = null;
            this.textBoxMsiSD.ValidatingRegexMessage = null;
            this.textBoxMsiSD.FailedValidation += new ValidationEventHandler(this.tabPageNodalDemand_FailedValidation);
            this.textBoxMsiSD.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // textBoxMisSD
            // 
            this.textBoxMisSD.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxMisSD.ErrorProvider = null;
            this.textBoxMisSD.Location = new System.Drawing.Point(186, 45);
            this.textBoxMisSD.Name = "textBoxMisSD";
            this.textBoxMisSD.ReadOnly = false;
            this.textBoxMisSD.Required = true;
            this.textBoxMisSD.Size = new System.Drawing.Size(85, 20);
            this.textBoxMisSD.TabIndex = 6;
            this.textBoxMisSD.ToolTipProvider = null;
            this.textBoxMisSD.ValidatingRegex = null;
            this.textBoxMisSD.ValidatingRegexMessage = null;
            this.textBoxMisSD.FailedValidation += new ValidationEventHandler(this.tabPageNodalDemand_FailedValidation);
            this.textBoxMisSD.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // textBoxMiiSD
            // 
            this.textBoxMiiSD.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxMiiSD.ErrorProvider = null;
            this.textBoxMiiSD.Location = new System.Drawing.Point(186, 19);
            this.textBoxMiiSD.Name = "textBoxMiiSD";
            this.textBoxMiiSD.ReadOnly = false;
            this.textBoxMiiSD.Required = true;
            this.textBoxMiiSD.Size = new System.Drawing.Size(85, 20);
            this.textBoxMiiSD.TabIndex = 5;
            this.textBoxMiiSD.ToolTipProvider = null;
            this.textBoxMiiSD.ValidatingRegex = null;
            this.textBoxMiiSD.ValidatingRegexMessage = null;
            this.textBoxMiiSD.FailedValidation += new ValidationEventHandler(this.tabPageNodalDemand_FailedValidation);
            this.textBoxMiiSD.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // textBoxMssMP
            // 
            this.textBoxMssMP.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxMssMP.ErrorProvider = null;
            this.textBoxMssMP.Location = new System.Drawing.Point(60, 97);
            this.textBoxMssMP.Name = "textBoxMssMP";
            this.textBoxMssMP.ReadOnly = false;
            this.textBoxMssMP.Required = true;
            this.textBoxMssMP.Size = new System.Drawing.Size(85, 20);
            this.textBoxMssMP.TabIndex = 4;
            this.textBoxMssMP.ToolTipProvider = null;
            this.textBoxMssMP.ValidatingRegex = null;
            this.textBoxMssMP.ValidatingRegexMessage = null;
            this.textBoxMssMP.FailedValidation += new ValidationEventHandler(this.tabPageNodalDemand_FailedValidation);
            this.textBoxMssMP.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // textBoxMsiMP
            // 
            this.textBoxMsiMP.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxMsiMP.ErrorProvider = null;
            this.textBoxMsiMP.Location = new System.Drawing.Point(60, 71);
            this.textBoxMsiMP.Name = "textBoxMsiMP";
            this.textBoxMsiMP.ReadOnly = false;
            this.textBoxMsiMP.Required = true;
            this.textBoxMsiMP.Size = new System.Drawing.Size(85, 20);
            this.textBoxMsiMP.TabIndex = 3;
            this.textBoxMsiMP.ToolTipProvider = null;
            this.textBoxMsiMP.ValidatingRegex = null;
            this.textBoxMsiMP.ValidatingRegexMessage = null;
            this.textBoxMsiMP.FailedValidation += new ValidationEventHandler(this.tabPageNodalDemand_FailedValidation);
            this.textBoxMsiMP.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // textBoxMisMP
            // 
            this.textBoxMisMP.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxMisMP.ErrorProvider = null;
            this.textBoxMisMP.Location = new System.Drawing.Point(60, 45);
            this.textBoxMisMP.Name = "textBoxMisMP";
            this.textBoxMisMP.ReadOnly = false;
            this.textBoxMisMP.Required = true;
            this.textBoxMisMP.Size = new System.Drawing.Size(85, 20);
            this.textBoxMisMP.TabIndex = 2;
            this.textBoxMisMP.ToolTipProvider = null;
            this.textBoxMisMP.ValidatingRegex = null;
            this.textBoxMisMP.ValidatingRegexMessage = null;
            this.textBoxMisMP.FailedValidation += new ValidationEventHandler(this.tabPageNodalDemand_FailedValidation);
            this.textBoxMisMP.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // textBoxMiiMP
            // 
            this.textBoxMiiMP.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxMiiMP.ErrorProvider = null;
            this.textBoxMiiMP.Location = new System.Drawing.Point(60, 19);
            this.textBoxMiiMP.Name = "textBoxMiiMP";
            this.textBoxMiiMP.ReadOnly = false;
            this.textBoxMiiMP.Required = true;
            this.textBoxMiiMP.Size = new System.Drawing.Size(85, 20);
            this.textBoxMiiMP.TabIndex = 1;
            this.textBoxMiiMP.ToolTipProvider = null;
            this.textBoxMiiMP.ValidatingRegex = null;
            this.textBoxMiiMP.ValidatingRegexMessage = null;
            this.textBoxMiiMP.FailedValidation += new ValidationEventHandler(this.tabPageNodalDemand_FailedValidation);
            this.textBoxMiiMP.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // tabPagePipeLeakage
            // 
            this.tabPagePipeLeakage.Controls.Add(this.groupBox5);
            this.tabPagePipeLeakage.Controls.Add(this.groupBox3);
            this.tabPagePipeLeakage.Controls.Add(this.groupBox2);
            this.tabPagePipeLeakage.Controls.Add(this.groupBox1);
            this.tabPagePipeLeakage.Controls.Add(this.groupBoxLeakOther);
            this.tabPagePipeLeakage.Controls.Add(this.groupBoxLeakSTL);
            this.tabPagePipeLeakage.Controls.Add(this.groupBoxLeakRS);
            this.tabPagePipeLeakage.Controls.Add(this.groupBoxLeakDI);
            this.tabPagePipeLeakage.Controls.Add(this.groupBox4);
            this.tabPagePipeLeakage.Controls.Add(this.groupBoxLeakCI);
            this.tabPagePipeLeakage.Location = new System.Drawing.Point(4, 22);
            this.tabPagePipeLeakage.Name = "tabPagePipeLeakage";
            this.tabPagePipeLeakage.Size = new System.Drawing.Size(700, 386);
            this.tabPagePipeLeakage.TabIndex = 2;
            this.tabPagePipeLeakage.Text = "Pipe Leakage Model";
            this.tabPagePipeLeakage.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.textBoxType5wD);
            this.groupBox5.Controls.Add(this.label30);
            this.groupBox5.Controls.Add(this.label31);
            this.groupBox5.Controls.Add(this.textBoxType5kD);
            this.groupBox5.Location = new System.Drawing.Point(6, 300);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(179, 79);
            this.groupBox5.TabIndex = 14;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Type 5: Local Tear of Pipe Wall";
            // 
            // textBoxType5wD
            // 
            this.textBoxType5wD.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxType5wD.ErrorProvider = null;
            this.textBoxType5wD.Location = new System.Drawing.Point(60, 45);
            this.textBoxType5wD.Name = "textBoxType5wD";
            this.textBoxType5wD.ReadOnly = false;
            this.textBoxType5wD.Required = true;
            this.textBoxType5wD.Size = new System.Drawing.Size(85, 20);
            this.textBoxType5wD.TabIndex = 2;
            this.textBoxType5wD.ToolTipProvider = null;
            this.textBoxType5wD.ValidatingRegex = null;
            this.textBoxType5wD.ValidatingRegexMessage = null;
            this.textBoxType5wD.FailedValidation += new ValidationEventHandler(this.tabPagePipeLeakage_FailedValidation);
            this.textBoxType5wD.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(24, 24);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(30, 13);
            this.label30.TabIndex = 105;
            this.label30.Text = "k (%)";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(22, 50);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(32, 13);
            this.label31.TabIndex = 106;
            this.label31.Text = "w (in)";
            // 
            // textBoxType5kD
            // 
            this.textBoxType5kD.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxType5kD.ErrorProvider = null;
            this.textBoxType5kD.Location = new System.Drawing.Point(60, 19);
            this.textBoxType5kD.Name = "textBoxType5kD";
            this.textBoxType5kD.ReadOnly = false;
            this.textBoxType5kD.Required = true;
            this.textBoxType5kD.Size = new System.Drawing.Size(85, 20);
            this.textBoxType5kD.TabIndex = 1;
            this.textBoxType5kD.ToolTipProvider = null;
            this.textBoxType5kD.ValidatingRegex = null;
            this.textBoxType5kD.ValidatingRegexMessage = null;
            this.textBoxType5kD.FailedValidation += new ValidationEventHandler(this.tabPagePipeLeakage_FailedValidation);
            this.textBoxType5kD.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label29);
            this.groupBox3.Controls.Add(this.textBoxType4kD);
            this.groupBox3.Location = new System.Drawing.Point(6, 241);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(179, 53);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Type 4: Local Loss of Pipe Wall";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(11, 24);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(43, 13);
            this.label29.TabIndex = 106;
            this.label29.Text = "k1 = k2";
            // 
            // textBoxType4kD
            // 
            this.textBoxType4kD.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxType4kD.ErrorProvider = null;
            this.textBoxType4kD.Location = new System.Drawing.Point(60, 19);
            this.textBoxType4kD.Name = "textBoxType4kD";
            this.textBoxType4kD.ReadOnly = false;
            this.textBoxType4kD.Required = true;
            this.textBoxType4kD.Size = new System.Drawing.Size(85, 20);
            this.textBoxType4kD.TabIndex = 1;
            this.textBoxType4kD.ToolTipProvider = null;
            this.textBoxType4kD.ValidatingRegex = null;
            this.textBoxType4kD.ValidatingRegexMessage = null;
            this.textBoxType4kD.FailedValidation += new ValidationEventHandler(this.tabPagePipeLeakage_FailedValidation);
            this.textBoxType4kD.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label32);
            this.groupBox2.Controls.Add(this.textBoxType3aD);
            this.groupBox2.Controls.Add(this.label28);
            this.groupBox2.Controls.Add(this.textBoxType3kD);
            this.groupBox2.Location = new System.Drawing.Point(6, 156);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(179, 79);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Type 3: Longitudinal Crack";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(41, 50);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(13, 13);
            this.label32.TabIndex = 106;
            this.label32.Text = "θ";
            // 
            // textBoxType3aD
            // 
            this.textBoxType3aD.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxType3aD.ErrorProvider = null;
            this.textBoxType3aD.Location = new System.Drawing.Point(60, 45);
            this.textBoxType3aD.Name = "textBoxType3aD";
            this.textBoxType3aD.ReadOnly = false;
            this.textBoxType3aD.Required = true;
            this.textBoxType3aD.Size = new System.Drawing.Size(85, 20);
            this.textBoxType3aD.TabIndex = 2;
            this.textBoxType3aD.ToolTipProvider = null;
            this.textBoxType3aD.ValidatingRegex = null;
            this.textBoxType3aD.ValidatingRegexMessage = null;
            this.textBoxType3aD.FailedValidation += new ValidationEventHandler(this.tabPagePipeLeakage_FailedValidation);
            this.textBoxType3aD.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(24, 22);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(30, 13);
            this.label28.TabIndex = 105;
            this.label28.Text = "L (in)";
            // 
            // textBoxType3kD
            // 
            this.textBoxType3kD.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxType3kD.ErrorProvider = null;
            this.textBoxType3kD.Location = new System.Drawing.Point(60, 19);
            this.textBoxType3kD.Name = "textBoxType3kD";
            this.textBoxType3kD.ReadOnly = false;
            this.textBoxType3kD.Required = true;
            this.textBoxType3kD.Size = new System.Drawing.Size(85, 20);
            this.textBoxType3kD.TabIndex = 1;
            this.textBoxType3kD.ToolTipProvider = null;
            this.textBoxType3kD.ValidatingRegex = null;
            this.textBoxType3kD.ValidatingRegexMessage = null;
            this.textBoxType3kD.FailedValidation += new ValidationEventHandler(this.tabPagePipeLeakage_FailedValidation);
            this.textBoxType3kD.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label33);
            this.groupBox1.Controls.Add(this.textBoxType2aD);
            this.groupBox1.Location = new System.Drawing.Point(6, 97);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(179, 53);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Type 2: Round Crack";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(41, 24);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(13, 13);
            this.label33.TabIndex = 107;
            this.label33.Text = "θ";
            // 
            // textBoxType2aD
            // 
            this.textBoxType2aD.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxType2aD.ErrorProvider = null;
            this.textBoxType2aD.Location = new System.Drawing.Point(60, 19);
            this.textBoxType2aD.Name = "textBoxType2aD";
            this.textBoxType2aD.ReadOnly = false;
            this.textBoxType2aD.Required = true;
            this.textBoxType2aD.Size = new System.Drawing.Size(85, 20);
            this.textBoxType2aD.TabIndex = 1;
            this.textBoxType2aD.ToolTipProvider = null;
            this.textBoxType2aD.ValidatingRegex = null;
            this.textBoxType2aD.ValidatingRegexMessage = null;
            this.textBoxType2aD.FailedValidation += new ValidationEventHandler(this.tabPagePipeLeakage_FailedValidation);
            this.textBoxType2aD.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // groupBoxLeakOther
            // 
            this.groupBoxLeakOther.Controls.Add(this.textBoxOtherType3D);
            this.groupBoxLeakOther.Controls.Add(this.textBoxOtherType2D);
            this.groupBoxLeakOther.Controls.Add(this.textBoxOtherType4D);
            this.groupBoxLeakOther.Controls.Add(this.textBoxOtherType5D);
            this.groupBoxLeakOther.Controls.Add(this.label21);
            this.groupBoxLeakOther.Controls.Add(this.label22);
            this.groupBoxLeakOther.Controls.Add(this.label23);
            this.groupBoxLeakOther.Controls.Add(this.label24);
            this.groupBoxLeakOther.Controls.Add(this.label25);
            this.groupBoxLeakOther.Controls.Add(this.textBoxOtherType1D);
            this.groupBoxLeakOther.Location = new System.Drawing.Point(357, 12);
            this.groupBoxLeakOther.Name = "groupBoxLeakOther";
            this.groupBoxLeakOther.Size = new System.Drawing.Size(160, 157);
            this.groupBoxLeakOther.TabIndex = 17;
            this.groupBoxLeakOther.TabStop = false;
            this.groupBoxLeakOther.Text = "P (Leak) - Unknown";
            // 
            // textBoxOtherType3D
            // 
            this.textBoxOtherType3D.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxOtherType3D.ErrorProvider = null;
            this.textBoxOtherType3D.Location = new System.Drawing.Point(60, 71);
            this.textBoxOtherType3D.Name = "textBoxOtherType3D";
            this.textBoxOtherType3D.ReadOnly = false;
            this.textBoxOtherType3D.Required = true;
            this.textBoxOtherType3D.Size = new System.Drawing.Size(85, 20);
            this.textBoxOtherType3D.TabIndex = 3;
            this.textBoxOtherType3D.ToolTipProvider = null;
            this.textBoxOtherType3D.ValidatingRegex = null;
            this.textBoxOtherType3D.ValidatingRegexMessage = null;
            this.textBoxOtherType3D.FailedValidation += new ValidationEventHandler(this.tabPagePipeLeakage_FailedValidation);
            this.textBoxOtherType3D.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // textBoxOtherType2D
            // 
            this.textBoxOtherType2D.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxOtherType2D.ErrorProvider = null;
            this.textBoxOtherType2D.Location = new System.Drawing.Point(60, 45);
            this.textBoxOtherType2D.Name = "textBoxOtherType2D";
            this.textBoxOtherType2D.ReadOnly = false;
            this.textBoxOtherType2D.Required = true;
            this.textBoxOtherType2D.Size = new System.Drawing.Size(85, 20);
            this.textBoxOtherType2D.TabIndex = 2;
            this.textBoxOtherType2D.ToolTipProvider = null;
            this.textBoxOtherType2D.ValidatingRegex = null;
            this.textBoxOtherType2D.ValidatingRegexMessage = null;
            this.textBoxOtherType2D.FailedValidation += new ValidationEventHandler(this.tabPagePipeLeakage_FailedValidation);
            this.textBoxOtherType2D.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // textBoxOtherType4D
            // 
            this.textBoxOtherType4D.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxOtherType4D.ErrorProvider = null;
            this.textBoxOtherType4D.Location = new System.Drawing.Point(60, 97);
            this.textBoxOtherType4D.Name = "textBoxOtherType4D";
            this.textBoxOtherType4D.ReadOnly = false;
            this.textBoxOtherType4D.Required = true;
            this.textBoxOtherType4D.Size = new System.Drawing.Size(85, 20);
            this.textBoxOtherType4D.TabIndex = 4;
            this.textBoxOtherType4D.ToolTipProvider = null;
            this.textBoxOtherType4D.ValidatingRegex = null;
            this.textBoxOtherType4D.ValidatingRegexMessage = null;
            this.textBoxOtherType4D.FailedValidation += new ValidationEventHandler(this.tabPagePipeLeakage_FailedValidation);
            this.textBoxOtherType4D.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // textBoxOtherType5D
            // 
            this.textBoxOtherType5D.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxOtherType5D.ErrorProvider = null;
            this.textBoxOtherType5D.Location = new System.Drawing.Point(60, 123);
            this.textBoxOtherType5D.Name = "textBoxOtherType5D";
            this.textBoxOtherType5D.ReadOnly = false;
            this.textBoxOtherType5D.Required = true;
            this.textBoxOtherType5D.Size = new System.Drawing.Size(85, 20);
            this.textBoxOtherType5D.TabIndex = 5;
            this.textBoxOtherType5D.ToolTipProvider = null;
            this.textBoxOtherType5D.ValidatingRegex = null;
            this.textBoxOtherType5D.ValidatingRegexMessage = null;
            this.textBoxOtherType5D.FailedValidation += new ValidationEventHandler(this.tabPagePipeLeakage_FailedValidation);
            this.textBoxOtherType5D.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(14, 24);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(40, 13);
            this.label21.TabIndex = 105;
            this.label21.Text = "Type 1";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(14, 50);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(40, 13);
            this.label22.TabIndex = 106;
            this.label22.Text = "Type 2";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(14, 76);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(40, 13);
            this.label23.TabIndex = 107;
            this.label23.Text = "Type 3";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(14, 102);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(40, 13);
            this.label24.TabIndex = 108;
            this.label24.Text = "Type 4";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(14, 128);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(40, 13);
            this.label25.TabIndex = 109;
            this.label25.Text = "Type 5";
            // 
            // textBoxOtherType1D
            // 
            this.textBoxOtherType1D.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxOtherType1D.ErrorProvider = null;
            this.textBoxOtherType1D.Location = new System.Drawing.Point(60, 19);
            this.textBoxOtherType1D.Name = "textBoxOtherType1D";
            this.textBoxOtherType1D.ReadOnly = false;
            this.textBoxOtherType1D.Required = true;
            this.textBoxOtherType1D.Size = new System.Drawing.Size(85, 20);
            this.textBoxOtherType1D.TabIndex = 1;
            this.textBoxOtherType1D.ToolTipProvider = null;
            this.textBoxOtherType1D.ValidatingRegex = null;
            this.textBoxOtherType1D.ValidatingRegexMessage = null;
            this.textBoxOtherType1D.FailedValidation += new ValidationEventHandler(this.tabPagePipeLeakage_FailedValidation);
            this.textBoxOtherType1D.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // groupBoxLeakSTL
            // 
            this.groupBoxLeakSTL.Controls.Add(this.textBoxSTLType3D);
            this.groupBoxLeakSTL.Controls.Add(this.textBoxSTLType2D);
            this.groupBoxLeakSTL.Controls.Add(this.textBoxSTLType4D);
            this.groupBoxLeakSTL.Controls.Add(this.textBoxSTLType5D);
            this.groupBoxLeakSTL.Controls.Add(this.label16);
            this.groupBoxLeakSTL.Controls.Add(this.label17);
            this.groupBoxLeakSTL.Controls.Add(this.label18);
            this.groupBoxLeakSTL.Controls.Add(this.label19);
            this.groupBoxLeakSTL.Controls.Add(this.label20);
            this.groupBoxLeakSTL.Controls.Add(this.textBoxSTLType1D);
            this.groupBoxLeakSTL.Location = new System.Drawing.Point(523, 12);
            this.groupBoxLeakSTL.Name = "groupBoxLeakSTL";
            this.groupBoxLeakSTL.Size = new System.Drawing.Size(160, 157);
            this.groupBoxLeakSTL.TabIndex = 19;
            this.groupBoxLeakSTL.TabStop = false;
            this.groupBoxLeakSTL.Text = "P (Leak) - Welded Steel";
            // 
            // textBoxSTLType3D
            // 
            this.textBoxSTLType3D.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxSTLType3D.ErrorProvider = null;
            this.textBoxSTLType3D.Location = new System.Drawing.Point(60, 71);
            this.textBoxSTLType3D.Name = "textBoxSTLType3D";
            this.textBoxSTLType3D.ReadOnly = false;
            this.textBoxSTLType3D.Required = true;
            this.textBoxSTLType3D.Size = new System.Drawing.Size(85, 20);
            this.textBoxSTLType3D.TabIndex = 3;
            this.textBoxSTLType3D.ToolTipProvider = null;
            this.textBoxSTLType3D.ValidatingRegex = null;
            this.textBoxSTLType3D.ValidatingRegexMessage = null;
            this.textBoxSTLType3D.FailedValidation += new ValidationEventHandler(this.tabPagePipeLeakage_FailedValidation);
            this.textBoxSTLType3D.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // textBoxSTLType2D
            // 
            this.textBoxSTLType2D.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxSTLType2D.ErrorProvider = null;
            this.textBoxSTLType2D.Location = new System.Drawing.Point(60, 45);
            this.textBoxSTLType2D.Name = "textBoxSTLType2D";
            this.textBoxSTLType2D.ReadOnly = false;
            this.textBoxSTLType2D.Required = true;
            this.textBoxSTLType2D.Size = new System.Drawing.Size(85, 20);
            this.textBoxSTLType2D.TabIndex = 2;
            this.textBoxSTLType2D.ToolTipProvider = null;
            this.textBoxSTLType2D.ValidatingRegex = null;
            this.textBoxSTLType2D.ValidatingRegexMessage = null;
            this.textBoxSTLType2D.FailedValidation += new ValidationEventHandler(this.tabPagePipeLeakage_FailedValidation);
            this.textBoxSTLType2D.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // textBoxSTLType4D
            // 
            this.textBoxSTLType4D.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxSTLType4D.ErrorProvider = null;
            this.textBoxSTLType4D.Location = new System.Drawing.Point(60, 97);
            this.textBoxSTLType4D.Name = "textBoxSTLType4D";
            this.textBoxSTLType4D.ReadOnly = false;
            this.textBoxSTLType4D.Required = true;
            this.textBoxSTLType4D.Size = new System.Drawing.Size(85, 20);
            this.textBoxSTLType4D.TabIndex = 4;
            this.textBoxSTLType4D.ToolTipProvider = null;
            this.textBoxSTLType4D.ValidatingRegex = null;
            this.textBoxSTLType4D.ValidatingRegexMessage = null;
            this.textBoxSTLType4D.FailedValidation += new ValidationEventHandler(this.tabPagePipeLeakage_FailedValidation);
            this.textBoxSTLType4D.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // textBoxSTLType5D
            // 
            this.textBoxSTLType5D.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxSTLType5D.ErrorProvider = null;
            this.textBoxSTLType5D.Location = new System.Drawing.Point(60, 123);
            this.textBoxSTLType5D.Name = "textBoxSTLType5D";
            this.textBoxSTLType5D.ReadOnly = false;
            this.textBoxSTLType5D.Required = true;
            this.textBoxSTLType5D.Size = new System.Drawing.Size(85, 20);
            this.textBoxSTLType5D.TabIndex = 5;
            this.textBoxSTLType5D.ToolTipProvider = null;
            this.textBoxSTLType5D.ValidatingRegex = null;
            this.textBoxSTLType5D.ValidatingRegexMessage = null;
            this.textBoxSTLType5D.FailedValidation += new ValidationEventHandler(this.tabPagePipeLeakage_FailedValidation);
            this.textBoxSTLType5D.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(14, 24);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(40, 13);
            this.label16.TabIndex = 105;
            this.label16.Text = "Type 1";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(14, 50);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(40, 13);
            this.label17.TabIndex = 106;
            this.label17.Text = "Type 2";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(14, 76);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(40, 13);
            this.label18.TabIndex = 107;
            this.label18.Text = "Type 3";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(14, 102);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(40, 13);
            this.label19.TabIndex = 108;
            this.label19.Text = "Type 4";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(14, 128);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(40, 13);
            this.label20.TabIndex = 109;
            this.label20.Text = "Type 5";
            // 
            // textBoxSTLType1D
            // 
            this.textBoxSTLType1D.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxSTLType1D.ErrorProvider = null;
            this.textBoxSTLType1D.Location = new System.Drawing.Point(60, 19);
            this.textBoxSTLType1D.Name = "textBoxSTLType1D";
            this.textBoxSTLType1D.ReadOnly = false;
            this.textBoxSTLType1D.Required = true;
            this.textBoxSTLType1D.Size = new System.Drawing.Size(85, 20);
            this.textBoxSTLType1D.TabIndex = 1;
            this.textBoxSTLType1D.ToolTipProvider = null;
            this.textBoxSTLType1D.ValidatingRegex = null;
            this.textBoxSTLType1D.ValidatingRegexMessage = null;
            this.textBoxSTLType1D.FailedValidation += new ValidationEventHandler(this.tabPagePipeLeakage_FailedValidation);
            this.textBoxSTLType1D.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // groupBoxLeakRS
            // 
            this.groupBoxLeakRS.Controls.Add(this.textBoxRSType3D);
            this.groupBoxLeakRS.Controls.Add(this.textBoxRSType2D);
            this.groupBoxLeakRS.Controls.Add(this.textBoxRSType4D);
            this.groupBoxLeakRS.Controls.Add(this.textBoxRSType5D);
            this.groupBoxLeakRS.Controls.Add(this.label11);
            this.groupBoxLeakRS.Controls.Add(this.label12);
            this.groupBoxLeakRS.Controls.Add(this.label13);
            this.groupBoxLeakRS.Controls.Add(this.label14);
            this.groupBoxLeakRS.Controls.Add(this.label15);
            this.groupBoxLeakRS.Controls.Add(this.textBoxRSType1D);
            this.groupBoxLeakRS.Location = new System.Drawing.Point(191, 175);
            this.groupBoxLeakRS.Name = "groupBoxLeakRS";
            this.groupBoxLeakRS.Size = new System.Drawing.Size(160, 157);
            this.groupBoxLeakRS.TabIndex = 16;
            this.groupBoxLeakRS.TabStop = false;
            this.groupBoxLeakRS.Text = "P (Leak) - Riveted Steel";
            // 
            // textBoxRSType3D
            // 
            this.textBoxRSType3D.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxRSType3D.ErrorProvider = null;
            this.textBoxRSType3D.Location = new System.Drawing.Point(60, 71);
            this.textBoxRSType3D.Name = "textBoxRSType3D";
            this.textBoxRSType3D.ReadOnly = false;
            this.textBoxRSType3D.Required = true;
            this.textBoxRSType3D.Size = new System.Drawing.Size(85, 20);
            this.textBoxRSType3D.TabIndex = 3;
            this.textBoxRSType3D.ToolTipProvider = null;
            this.textBoxRSType3D.ValidatingRegex = null;
            this.textBoxRSType3D.ValidatingRegexMessage = null;
            this.textBoxRSType3D.FailedValidation += new ValidationEventHandler(this.tabPagePipeLeakage_FailedValidation);
            this.textBoxRSType3D.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // textBoxRSType2D
            // 
            this.textBoxRSType2D.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxRSType2D.ErrorProvider = null;
            this.textBoxRSType2D.Location = new System.Drawing.Point(60, 45);
            this.textBoxRSType2D.Name = "textBoxRSType2D";
            this.textBoxRSType2D.ReadOnly = false;
            this.textBoxRSType2D.Required = true;
            this.textBoxRSType2D.Size = new System.Drawing.Size(85, 20);
            this.textBoxRSType2D.TabIndex = 2;
            this.textBoxRSType2D.ToolTipProvider = null;
            this.textBoxRSType2D.ValidatingRegex = null;
            this.textBoxRSType2D.ValidatingRegexMessage = null;
            this.textBoxRSType2D.FailedValidation += new ValidationEventHandler(this.tabPagePipeLeakage_FailedValidation);
            this.textBoxRSType2D.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // textBoxRSType4D
            // 
            this.textBoxRSType4D.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxRSType4D.ErrorProvider = null;
            this.textBoxRSType4D.Location = new System.Drawing.Point(60, 97);
            this.textBoxRSType4D.Name = "textBoxRSType4D";
            this.textBoxRSType4D.ReadOnly = false;
            this.textBoxRSType4D.Required = true;
            this.textBoxRSType4D.Size = new System.Drawing.Size(85, 20);
            this.textBoxRSType4D.TabIndex = 4;
            this.textBoxRSType4D.ToolTipProvider = null;
            this.textBoxRSType4D.ValidatingRegex = null;
            this.textBoxRSType4D.ValidatingRegexMessage = null;
            this.textBoxRSType4D.FailedValidation += new ValidationEventHandler(this.tabPagePipeLeakage_FailedValidation);
            this.textBoxRSType4D.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // textBoxRSType5D
            // 
            this.textBoxRSType5D.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxRSType5D.ErrorProvider = null;
            this.textBoxRSType5D.Location = new System.Drawing.Point(60, 123);
            this.textBoxRSType5D.Name = "textBoxRSType5D";
            this.textBoxRSType5D.ReadOnly = false;
            this.textBoxRSType5D.Required = true;
            this.textBoxRSType5D.Size = new System.Drawing.Size(85, 20);
            this.textBoxRSType5D.TabIndex = 5;
            this.textBoxRSType5D.ToolTipProvider = null;
            this.textBoxRSType5D.ValidatingRegex = null;
            this.textBoxRSType5D.ValidatingRegexMessage = null;
            this.textBoxRSType5D.FailedValidation += new ValidationEventHandler(this.tabPagePipeLeakage_FailedValidation);
            this.textBoxRSType5D.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(14, 24);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(40, 13);
            this.label11.TabIndex = 105;
            this.label11.Text = "Type 1";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(14, 50);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(40, 13);
            this.label12.TabIndex = 106;
            this.label12.Text = "Type 2";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(14, 76);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(40, 13);
            this.label13.TabIndex = 107;
            this.label13.Text = "Type 3";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(14, 102);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(40, 13);
            this.label14.TabIndex = 108;
            this.label14.Text = "Type 4";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(14, 128);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(40, 13);
            this.label15.TabIndex = 109;
            this.label15.Text = "Type 5";
            // 
            // textBoxRSType1D
            // 
            this.textBoxRSType1D.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxRSType1D.ErrorProvider = null;
            this.textBoxRSType1D.Location = new System.Drawing.Point(60, 19);
            this.textBoxRSType1D.Name = "textBoxRSType1D";
            this.textBoxRSType1D.ReadOnly = false;
            this.textBoxRSType1D.Required = true;
            this.textBoxRSType1D.Size = new System.Drawing.Size(85, 20);
            this.textBoxRSType1D.TabIndex = 1;
            this.textBoxRSType1D.ToolTipProvider = null;
            this.textBoxRSType1D.ValidatingRegex = null;
            this.textBoxRSType1D.ValidatingRegexMessage = null;
            this.textBoxRSType1D.FailedValidation += new ValidationEventHandler(this.tabPagePipeLeakage_FailedValidation);
            this.textBoxRSType1D.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // groupBoxLeakDI
            // 
            this.groupBoxLeakDI.Controls.Add(this.textBoxDIType3D);
            this.groupBoxLeakDI.Controls.Add(this.textBoxDIType2D);
            this.groupBoxLeakDI.Controls.Add(this.textBoxDIType4D);
            this.groupBoxLeakDI.Controls.Add(this.textBoxDIType5D);
            this.groupBoxLeakDI.Controls.Add(this.label6);
            this.groupBoxLeakDI.Controls.Add(this.label7);
            this.groupBoxLeakDI.Controls.Add(this.label8);
            this.groupBoxLeakDI.Controls.Add(this.label9);
            this.groupBoxLeakDI.Controls.Add(this.label10);
            this.groupBoxLeakDI.Controls.Add(this.textBoxDIType1D);
            this.groupBoxLeakDI.Location = new System.Drawing.Point(357, 175);
            this.groupBoxLeakDI.Name = "groupBoxLeakDI";
            this.groupBoxLeakDI.Size = new System.Drawing.Size(160, 157);
            this.groupBoxLeakDI.TabIndex = 18;
            this.groupBoxLeakDI.TabStop = false;
            this.groupBoxLeakDI.Text = "P (Leak) - Ductile Iron";
            // 
            // textBoxDIType3D
            // 
            this.textBoxDIType3D.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxDIType3D.ErrorProvider = null;
            this.textBoxDIType3D.Location = new System.Drawing.Point(60, 71);
            this.textBoxDIType3D.Name = "textBoxDIType3D";
            this.textBoxDIType3D.ReadOnly = false;
            this.textBoxDIType3D.Required = true;
            this.textBoxDIType3D.Size = new System.Drawing.Size(85, 20);
            this.textBoxDIType3D.TabIndex = 3;
            this.textBoxDIType3D.ToolTipProvider = null;
            this.textBoxDIType3D.ValidatingRegex = null;
            this.textBoxDIType3D.ValidatingRegexMessage = null;
            this.textBoxDIType3D.FailedValidation += new ValidationEventHandler(this.tabPagePipeLeakage_FailedValidation);
            this.textBoxDIType3D.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // textBoxDIType2D
            // 
            this.textBoxDIType2D.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxDIType2D.ErrorProvider = null;
            this.textBoxDIType2D.Location = new System.Drawing.Point(60, 45);
            this.textBoxDIType2D.Name = "textBoxDIType2D";
            this.textBoxDIType2D.ReadOnly = false;
            this.textBoxDIType2D.Required = true;
            this.textBoxDIType2D.Size = new System.Drawing.Size(85, 20);
            this.textBoxDIType2D.TabIndex = 2;
            this.textBoxDIType2D.ToolTipProvider = null;
            this.textBoxDIType2D.ValidatingRegex = null;
            this.textBoxDIType2D.ValidatingRegexMessage = null;
            this.textBoxDIType2D.FailedValidation += new ValidationEventHandler(this.tabPagePipeLeakage_FailedValidation);
            this.textBoxDIType2D.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // textBoxDIType4D
            // 
            this.textBoxDIType4D.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxDIType4D.ErrorProvider = null;
            this.textBoxDIType4D.Location = new System.Drawing.Point(60, 97);
            this.textBoxDIType4D.Name = "textBoxDIType4D";
            this.textBoxDIType4D.ReadOnly = false;
            this.textBoxDIType4D.Required = true;
            this.textBoxDIType4D.Size = new System.Drawing.Size(85, 20);
            this.textBoxDIType4D.TabIndex = 4;
            this.textBoxDIType4D.ToolTipProvider = null;
            this.textBoxDIType4D.ValidatingRegex = null;
            this.textBoxDIType4D.ValidatingRegexMessage = null;
            this.textBoxDIType4D.FailedValidation += new ValidationEventHandler(this.tabPagePipeLeakage_FailedValidation);
            this.textBoxDIType4D.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // textBoxDIType5D
            // 
            this.textBoxDIType5D.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxDIType5D.ErrorProvider = null;
            this.textBoxDIType5D.Location = new System.Drawing.Point(61, 123);
            this.textBoxDIType5D.Name = "textBoxDIType5D";
            this.textBoxDIType5D.ReadOnly = false;
            this.textBoxDIType5D.Required = true;
            this.textBoxDIType5D.Size = new System.Drawing.Size(85, 20);
            this.textBoxDIType5D.TabIndex = 5;
            this.textBoxDIType5D.ToolTipProvider = null;
            this.textBoxDIType5D.ValidatingRegex = null;
            this.textBoxDIType5D.ValidatingRegexMessage = null;
            this.textBoxDIType5D.FailedValidation += new ValidationEventHandler(this.tabPagePipeLeakage_FailedValidation);
            this.textBoxDIType5D.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(14, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 13);
            this.label6.TabIndex = 105;
            this.label6.Text = "Type 1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 50);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 13);
            this.label7.TabIndex = 106;
            this.label7.Text = "Type 2";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(14, 76);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 13);
            this.label8.TabIndex = 107;
            this.label8.Text = "Type 3";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(14, 102);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(40, 13);
            this.label9.TabIndex = 108;
            this.label9.Text = "Type 4";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(14, 128);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(40, 13);
            this.label10.TabIndex = 109;
            this.label10.Text = "Type 5";
            // 
            // textBoxDIType1D
            // 
            this.textBoxDIType1D.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxDIType1D.ErrorProvider = null;
            this.textBoxDIType1D.Location = new System.Drawing.Point(60, 19);
            this.textBoxDIType1D.Name = "textBoxDIType1D";
            this.textBoxDIType1D.ReadOnly = false;
            this.textBoxDIType1D.Required = true;
            this.textBoxDIType1D.Size = new System.Drawing.Size(85, 20);
            this.textBoxDIType1D.TabIndex = 1;
            this.textBoxDIType1D.ToolTipProvider = null;
            this.textBoxDIType1D.ValidatingRegex = null;
            this.textBoxDIType1D.ValidatingRegexMessage = null;
            this.textBoxDIType1D.FailedValidation += new ValidationEventHandler(this.tabPagePipeLeakage_FailedValidation);
            this.textBoxDIType1D.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.textBoxType1kD);
            this.groupBox4.Controls.Add(this.label26);
            this.groupBox4.Controls.Add(this.label27);
            this.groupBox4.Controls.Add(this.textBoxType1tD);
            this.groupBox4.Location = new System.Drawing.Point(6, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(179, 79);
            this.groupBox4.TabIndex = 10;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Type 1: Annular Disengagement";
            // 
            // textBoxType1kD
            // 
            this.textBoxType1kD.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxType1kD.ErrorProvider = null;
            this.textBoxType1kD.Location = new System.Drawing.Point(60, 45);
            this.textBoxType1kD.Name = "textBoxType1kD";
            this.textBoxType1kD.ReadOnly = false;
            this.textBoxType1kD.Required = true;
            this.textBoxType1kD.Size = new System.Drawing.Size(85, 20);
            this.textBoxType1kD.TabIndex = 2;
            this.textBoxType1kD.ToolTipProvider = null;
            this.textBoxType1kD.ValidatingRegex = null;
            this.textBoxType1kD.ValidatingRegexMessage = null;
            this.textBoxType1kD.FailedValidation += new ValidationEventHandler(this.tabPagePipeLeakage_FailedValidation);
            this.textBoxType1kD.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(27, 24);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(27, 13);
            this.label26.TabIndex = 105;
            this.label26.Text = "t (in)";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(41, 50);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(13, 13);
            this.label27.TabIndex = 106;
            this.label27.Text = "k";
            // 
            // textBoxType1tD
            // 
            this.textBoxType1tD.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxType1tD.ErrorProvider = null;
            this.textBoxType1tD.Location = new System.Drawing.Point(60, 19);
            this.textBoxType1tD.Name = "textBoxType1tD";
            this.textBoxType1tD.ReadOnly = false;
            this.textBoxType1tD.Required = true;
            this.textBoxType1tD.Size = new System.Drawing.Size(85, 20);
            this.textBoxType1tD.TabIndex = 1;
            this.textBoxType1tD.ToolTipProvider = null;
            this.textBoxType1tD.ValidatingRegex = null;
            this.textBoxType1tD.ValidatingRegexMessage = null;
            this.textBoxType1tD.FailedValidation += new ValidationEventHandler(this.tabPagePipeLeakage_FailedValidation);
            this.textBoxType1tD.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // groupBoxLeakCI
            // 
            this.groupBoxLeakCI.Controls.Add(this.textBoxCIType3D);
            this.groupBoxLeakCI.Controls.Add(this.textBoxCIType2D);
            this.groupBoxLeakCI.Controls.Add(this.textBoxCIType4D);
            this.groupBoxLeakCI.Controls.Add(this.textBoxCIType5D);
            this.groupBoxLeakCI.Controls.Add(this.label5);
            this.groupBoxLeakCI.Controls.Add(this.label34);
            this.groupBoxLeakCI.Controls.Add(this.label35);
            this.groupBoxLeakCI.Controls.Add(this.label36);
            this.groupBoxLeakCI.Controls.Add(this.label37);
            this.groupBoxLeakCI.Controls.Add(this.textBoxCIType1D);
            this.groupBoxLeakCI.Location = new System.Drawing.Point(191, 12);
            this.groupBoxLeakCI.Name = "groupBoxLeakCI";
            this.groupBoxLeakCI.Size = new System.Drawing.Size(160, 157);
            this.groupBoxLeakCI.TabIndex = 15;
            this.groupBoxLeakCI.TabStop = false;
            this.groupBoxLeakCI.Text = "P (Leak) - Cast Iron";
            // 
            // textBoxCIType3D
            // 
            this.textBoxCIType3D.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxCIType3D.ErrorProvider = null;
            this.textBoxCIType3D.Location = new System.Drawing.Point(60, 71);
            this.textBoxCIType3D.Name = "textBoxCIType3D";
            this.textBoxCIType3D.ReadOnly = false;
            this.textBoxCIType3D.Required = true;
            this.textBoxCIType3D.Size = new System.Drawing.Size(85, 20);
            this.textBoxCIType3D.TabIndex = 3;
            this.textBoxCIType3D.ToolTipProvider = null;
            this.textBoxCIType3D.ValidatingRegex = null;
            this.textBoxCIType3D.ValidatingRegexMessage = null;
            this.textBoxCIType3D.FailedValidation += new ValidationEventHandler(this.tabPagePipeLeakage_FailedValidation);
            this.textBoxCIType3D.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // textBoxCIType2D
            // 
            this.textBoxCIType2D.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxCIType2D.ErrorProvider = null;
            this.textBoxCIType2D.Location = new System.Drawing.Point(60, 45);
            this.textBoxCIType2D.Name = "textBoxCIType2D";
            this.textBoxCIType2D.ReadOnly = false;
            this.textBoxCIType2D.Required = true;
            this.textBoxCIType2D.Size = new System.Drawing.Size(85, 20);
            this.textBoxCIType2D.TabIndex = 2;
            this.textBoxCIType2D.ToolTipProvider = null;
            this.textBoxCIType2D.ValidatingRegex = null;
            this.textBoxCIType2D.ValidatingRegexMessage = null;
            this.textBoxCIType2D.FailedValidation += new ValidationEventHandler(this.tabPagePipeLeakage_FailedValidation);
            this.textBoxCIType2D.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // textBoxCIType4D
            // 
            this.textBoxCIType4D.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxCIType4D.ErrorProvider = null;
            this.textBoxCIType4D.Location = new System.Drawing.Point(60, 97);
            this.textBoxCIType4D.Name = "textBoxCIType4D";
            this.textBoxCIType4D.ReadOnly = false;
            this.textBoxCIType4D.Required = true;
            this.textBoxCIType4D.Size = new System.Drawing.Size(85, 20);
            this.textBoxCIType4D.TabIndex = 4;
            this.textBoxCIType4D.ToolTipProvider = null;
            this.textBoxCIType4D.ValidatingRegex = null;
            this.textBoxCIType4D.ValidatingRegexMessage = null;
            this.textBoxCIType4D.FailedValidation += new ValidationEventHandler(this.tabPagePipeLeakage_FailedValidation);
            this.textBoxCIType4D.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // textBoxCIType5D
            // 
            this.textBoxCIType5D.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxCIType5D.ErrorProvider = null;
            this.textBoxCIType5D.Location = new System.Drawing.Point(60, 123);
            this.textBoxCIType5D.Name = "textBoxCIType5D";
            this.textBoxCIType5D.ReadOnly = false;
            this.textBoxCIType5D.Required = true;
            this.textBoxCIType5D.Size = new System.Drawing.Size(85, 20);
            this.textBoxCIType5D.TabIndex = 5;
            this.textBoxCIType5D.ToolTipProvider = null;
            this.textBoxCIType5D.ValidatingRegex = null;
            this.textBoxCIType5D.ValidatingRegexMessage = null;
            this.textBoxCIType5D.FailedValidation += new ValidationEventHandler(this.tabPagePipeLeakage_FailedValidation);
            this.textBoxCIType5D.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 13);
            this.label5.TabIndex = 105;
            this.label5.Text = "Type 1";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(14, 50);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(40, 13);
            this.label34.TabIndex = 106;
            this.label34.Text = "Type 2";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(14, 76);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(40, 13);
            this.label35.TabIndex = 107;
            this.label35.Text = "Type 3";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(14, 102);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(40, 13);
            this.label36.TabIndex = 108;
            this.label36.Text = "Type 4";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(14, 128);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(40, 13);
            this.label37.TabIndex = 109;
            this.label37.Text = "Type 5";
            // 
            // textBoxCIType1D
            // 
            this.textBoxCIType1D.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxCIType1D.ErrorProvider = null;
            this.textBoxCIType1D.Location = new System.Drawing.Point(60, 19);
            this.textBoxCIType1D.Name = "textBoxCIType1D";
            this.textBoxCIType1D.ReadOnly = false;
            this.textBoxCIType1D.Required = true;
            this.textBoxCIType1D.Size = new System.Drawing.Size(85, 20);
            this.textBoxCIType1D.TabIndex = 1;
            this.textBoxCIType1D.ToolTipProvider = null;
            this.textBoxCIType1D.ValidatingRegex = null;
            this.textBoxCIType1D.ValidatingRegexMessage = null;
            this.textBoxCIType1D.FailedValidation += new ValidationEventHandler(this.tabPagePipeLeakage_FailedValidation);
            this.textBoxCIType1D.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // tabPagePipeDamage
            // 
            this.tabPagePipeDamage.Controls.Add(this.groupBoxLeakDamage);
            this.tabPagePipeDamage.Controls.Add(this.groupBoxBreakDamage);
            this.tabPagePipeDamage.Location = new System.Drawing.Point(4, 22);
            this.tabPagePipeDamage.Name = "tabPagePipeDamage";
            this.tabPagePipeDamage.Size = new System.Drawing.Size(700, 386);
            this.tabPagePipeDamage.TabIndex = 3;
            this.tabPagePipeDamage.Text = "Pipe Damage Probability";
            this.tabPagePipeDamage.UseVisualStyleBackColor = true;
            // 
            // groupBoxLeakDamage
            // 
            this.groupBoxLeakDamage.Controls.Add(this.textBoxRSLeakRatio);
            this.groupBoxLeakDamage.Controls.Add(this.textBoxDILeakRatio);
            this.groupBoxLeakDamage.Controls.Add(this.textBoxSTLLeakRatio);
            this.groupBoxLeakDamage.Controls.Add(this.textBoxCONLeakRatio);
            this.groupBoxLeakDamage.Controls.Add(this.label38);
            this.groupBoxLeakDamage.Controls.Add(this.label39);
            this.groupBoxLeakDamage.Controls.Add(this.label40);
            this.groupBoxLeakDamage.Controls.Add(this.label41);
            this.groupBoxLeakDamage.Controls.Add(this.label42);
            this.groupBoxLeakDamage.Controls.Add(this.textBoxCILeakRatio);
            this.groupBoxLeakDamage.Location = new System.Drawing.Point(215, 12);
            this.groupBoxLeakDamage.Name = "groupBoxLeakDamage";
            this.groupBoxLeakDamage.Size = new System.Drawing.Size(203, 157);
            this.groupBoxLeakDamage.TabIndex = 3;
            this.groupBoxLeakDamage.TabStop = false;
            this.groupBoxLeakDamage.Text = "P (Leak | Damage)";
            // 
            // textBoxRSLeakRatio
            // 
            this.textBoxRSLeakRatio.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxRSLeakRatio.ErrorProvider = null;
            this.textBoxRSLeakRatio.Location = new System.Drawing.Point(102, 71);
            this.textBoxRSLeakRatio.Name = "textBoxRSLeakRatio";
            this.textBoxRSLeakRatio.ReadOnly = true;
            this.textBoxRSLeakRatio.Required = true;
            this.textBoxRSLeakRatio.Size = new System.Drawing.Size(85, 20);
            this.textBoxRSLeakRatio.TabIndex = 112;
            this.textBoxRSLeakRatio.TabStop = false;
            this.textBoxRSLeakRatio.ToolTipProvider = null;
            this.textBoxRSLeakRatio.ValidatingRegex = null;
            this.textBoxRSLeakRatio.ValidatingRegexMessage = null;
            this.textBoxRSLeakRatio.FailedValidation += new ValidationEventHandler(this.tabPagePipeDamage_FailedValidation);
            this.textBoxRSLeakRatio.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // textBoxDILeakRatio
            // 
            this.textBoxDILeakRatio.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxDILeakRatio.ErrorProvider = null;
            this.textBoxDILeakRatio.Location = new System.Drawing.Point(102, 45);
            this.textBoxDILeakRatio.Name = "textBoxDILeakRatio";
            this.textBoxDILeakRatio.ReadOnly = true;
            this.textBoxDILeakRatio.Required = true;
            this.textBoxDILeakRatio.Size = new System.Drawing.Size(85, 20);
            this.textBoxDILeakRatio.TabIndex = 111;
            this.textBoxDILeakRatio.TabStop = false;
            this.textBoxDILeakRatio.ToolTipProvider = null;
            this.textBoxDILeakRatio.ValidatingRegex = null;
            this.textBoxDILeakRatio.ValidatingRegexMessage = null;
            this.textBoxDILeakRatio.FailedValidation += new ValidationEventHandler(this.tabPagePipeDamage_FailedValidation);
            this.textBoxDILeakRatio.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // textBoxSTLLeakRatio
            // 
            this.textBoxSTLLeakRatio.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxSTLLeakRatio.ErrorProvider = null;
            this.textBoxSTLLeakRatio.Location = new System.Drawing.Point(102, 97);
            this.textBoxSTLLeakRatio.Name = "textBoxSTLLeakRatio";
            this.textBoxSTLLeakRatio.ReadOnly = false;
            this.textBoxSTLLeakRatio.Required = true;
            this.textBoxSTLLeakRatio.Size = new System.Drawing.Size(85, 20);
            this.textBoxSTLLeakRatio.TabIndex = 1;
            this.textBoxSTLLeakRatio.ToolTipProvider = null;
            this.textBoxSTLLeakRatio.ValidatingRegex = null;
            this.textBoxSTLLeakRatio.ValidatingRegexMessage = null;
            this.textBoxSTLLeakRatio.FailedValidation += new ValidationEventHandler(this.tabPagePipeDamage_FailedValidation);
            this.textBoxSTLLeakRatio.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // textBoxCONLeakRatio
            // 
            this.textBoxCONLeakRatio.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxCONLeakRatio.ErrorProvider = null;
            this.textBoxCONLeakRatio.Location = new System.Drawing.Point(102, 123);
            this.textBoxCONLeakRatio.Name = "textBoxCONLeakRatio";
            this.textBoxCONLeakRatio.ReadOnly = true;
            this.textBoxCONLeakRatio.Required = true;
            this.textBoxCONLeakRatio.Size = new System.Drawing.Size(85, 20);
            this.textBoxCONLeakRatio.TabIndex = 114;
            this.textBoxCONLeakRatio.TabStop = false;
            this.textBoxCONLeakRatio.ToolTipProvider = null;
            this.textBoxCONLeakRatio.ValidatingRegex = null;
            this.textBoxCONLeakRatio.ValidatingRegexMessage = null;
            this.textBoxCONLeakRatio.FailedValidation += new ValidationEventHandler(this.tabPagePipeDamage_FailedValidation);
            this.textBoxCONLeakRatio.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(44, 24);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(49, 13);
            this.label38.TabIndex = 115;
            this.label38.Text = "Cast Iron";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(32, 50);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(61, 13);
            this.label39.TabIndex = 116;
            this.label39.Text = "Ductile Iron";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(22, 76);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(71, 13);
            this.label40.TabIndex = 117;
            this.label40.Text = "Riveted Steel";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(22, 102);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(71, 13);
            this.label41.TabIndex = 118;
            this.label41.Text = "Welded Steel";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(6, 128);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(87, 13);
            this.label42.TabIndex = 119;
            this.label42.Text = "Jointed Concrete";
            // 
            // textBoxCILeakRatio
            // 
            this.textBoxCILeakRatio.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxCILeakRatio.ErrorProvider = null;
            this.textBoxCILeakRatio.Location = new System.Drawing.Point(102, 19);
            this.textBoxCILeakRatio.Name = "textBoxCILeakRatio";
            this.textBoxCILeakRatio.ReadOnly = true;
            this.textBoxCILeakRatio.Required = true;
            this.textBoxCILeakRatio.Size = new System.Drawing.Size(85, 20);
            this.textBoxCILeakRatio.TabIndex = 110;
            this.textBoxCILeakRatio.TabStop = false;
            this.textBoxCILeakRatio.ToolTipProvider = null;
            this.textBoxCILeakRatio.ValidatingRegex = null;
            this.textBoxCILeakRatio.ValidatingRegexMessage = null;
            this.textBoxCILeakRatio.FailedValidation += new ValidationEventHandler(this.tabPagePipeDamage_FailedValidation);
            this.textBoxCILeakRatio.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // groupBoxBreakDamage
            // 
            this.groupBoxBreakDamage.Controls.Add(this.textBoxBreakProRS);
            this.groupBoxBreakDamage.Controls.Add(this.textBoxBreakProDI);
            this.groupBoxBreakDamage.Controls.Add(this.textBoxBreakProSTL);
            this.groupBoxBreakDamage.Controls.Add(this.textBoxBreakProCON);
            this.groupBoxBreakDamage.Controls.Add(this.label43);
            this.groupBoxBreakDamage.Controls.Add(this.label44);
            this.groupBoxBreakDamage.Controls.Add(this.label45);
            this.groupBoxBreakDamage.Controls.Add(this.label46);
            this.groupBoxBreakDamage.Controls.Add(this.label47);
            this.groupBoxBreakDamage.Controls.Add(this.textBoxBreakProCI);
            this.groupBoxBreakDamage.Location = new System.Drawing.Point(6, 12);
            this.groupBoxBreakDamage.Name = "groupBoxBreakDamage";
            this.groupBoxBreakDamage.Size = new System.Drawing.Size(203, 157);
            this.groupBoxBreakDamage.TabIndex = 2;
            this.groupBoxBreakDamage.TabStop = false;
            this.groupBoxBreakDamage.Text = "P (Break | Damage)";
            // 
            // textBoxBreakProRS
            // 
            this.textBoxBreakProRS.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxBreakProRS.ErrorProvider = null;
            this.textBoxBreakProRS.Location = new System.Drawing.Point(102, 71);
            this.textBoxBreakProRS.Name = "textBoxBreakProRS";
            this.textBoxBreakProRS.ReadOnly = false;
            this.textBoxBreakProRS.Required = true;
            this.textBoxBreakProRS.Size = new System.Drawing.Size(85, 20);
            this.textBoxBreakProRS.TabIndex = 3;
            this.textBoxBreakProRS.ToolTipProvider = null;
            this.textBoxBreakProRS.ValidatingRegex = null;
            this.textBoxBreakProRS.ValidatingRegexMessage = null;
            this.textBoxBreakProRS.FailedValidation += new ValidationEventHandler(this.tabPagePipeDamage_FailedValidation);
            this.textBoxBreakProRS.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            this.textBoxBreakProRS.Leave += new System.EventHandler(this.textBoxBreakProRS_Leave);
            // 
            // textBoxBreakProDI
            // 
            this.textBoxBreakProDI.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxBreakProDI.ErrorProvider = null;
            this.textBoxBreakProDI.Location = new System.Drawing.Point(102, 45);
            this.textBoxBreakProDI.Name = "textBoxBreakProDI";
            this.textBoxBreakProDI.ReadOnly = false;
            this.textBoxBreakProDI.Required = true;
            this.textBoxBreakProDI.Size = new System.Drawing.Size(85, 20);
            this.textBoxBreakProDI.TabIndex = 2;
            this.textBoxBreakProDI.ToolTipProvider = null;
            this.textBoxBreakProDI.ValidatingRegex = null;
            this.textBoxBreakProDI.ValidatingRegexMessage = null;
            this.textBoxBreakProDI.FailedValidation += new ValidationEventHandler(this.tabPagePipeDamage_FailedValidation);
            this.textBoxBreakProDI.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            this.textBoxBreakProDI.Leave += new System.EventHandler(this.textBoxBreakProDI_Leave);
            // 
            // textBoxBreakProSTL
            // 
            this.textBoxBreakProSTL.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxBreakProSTL.ErrorProvider = null;
            this.textBoxBreakProSTL.Location = new System.Drawing.Point(102, 97);
            this.textBoxBreakProSTL.Name = "textBoxBreakProSTL";
            this.textBoxBreakProSTL.ReadOnly = false;
            this.textBoxBreakProSTL.Required = true;
            this.textBoxBreakProSTL.Size = new System.Drawing.Size(85, 20);
            this.textBoxBreakProSTL.TabIndex = 4;
            this.textBoxBreakProSTL.ToolTipProvider = null;
            this.textBoxBreakProSTL.ValidatingRegex = null;
            this.textBoxBreakProSTL.ValidatingRegexMessage = null;
            this.textBoxBreakProSTL.FailedValidation += new ValidationEventHandler(this.tabPagePipeDamage_FailedValidation);
            this.textBoxBreakProSTL.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            // 
            // textBoxBreakProCON
            // 
            this.textBoxBreakProCON.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxBreakProCON.ErrorProvider = null;
            this.textBoxBreakProCON.Location = new System.Drawing.Point(102, 123);
            this.textBoxBreakProCON.Name = "textBoxBreakProCON";
            this.textBoxBreakProCON.ReadOnly = false;
            this.textBoxBreakProCON.Required = true;
            this.textBoxBreakProCON.Size = new System.Drawing.Size(85, 20);
            this.textBoxBreakProCON.TabIndex = 5;
            this.textBoxBreakProCON.ToolTipProvider = null;
            this.textBoxBreakProCON.ValidatingRegex = null;
            this.textBoxBreakProCON.ValidatingRegexMessage = null;
            this.textBoxBreakProCON.FailedValidation += new ValidationEventHandler(this.tabPagePipeDamage_FailedValidation);
            this.textBoxBreakProCON.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            this.textBoxBreakProCON.Leave += new System.EventHandler(this.textBoxBreakProCON_Leave);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(44, 24);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(49, 13);
            this.label43.TabIndex = 105;
            this.label43.Text = "Cast Iron";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(32, 50);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(61, 13);
            this.label44.TabIndex = 106;
            this.label44.Text = "Ductile Iron";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(22, 76);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(71, 13);
            this.label45.TabIndex = 107;
            this.label45.Text = "Riveted Steel";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(22, 102);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(71, 13);
            this.label46.TabIndex = 108;
            this.label46.Text = "Welded Steel";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(6, 128);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(87, 13);
            this.label47.TabIndex = 109;
            this.label47.Text = "Jointed Concrete";
            // 
            // textBoxBreakProCI
            // 
            this.textBoxBreakProCI.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxBreakProCI.ErrorProvider = null;
            this.textBoxBreakProCI.Location = new System.Drawing.Point(102, 19);
            this.textBoxBreakProCI.Name = "textBoxBreakProCI";
            this.textBoxBreakProCI.ReadOnly = false;
            this.textBoxBreakProCI.Required = true;
            this.textBoxBreakProCI.Size = new System.Drawing.Size(85, 20);
            this.textBoxBreakProCI.TabIndex = 1;
            this.textBoxBreakProCI.ToolTipProvider = null;
            this.textBoxBreakProCI.ValidatingRegex = null;
            this.textBoxBreakProCI.ValidatingRegexMessage = null;
            this.textBoxBreakProCI.FailedValidation += new ValidationEventHandler(this.tabPagePipeDamage_FailedValidation);
            this.textBoxBreakProCI.PassedValidation += new ValidationEventHandler(this.textBox_PassedValidation);
            this.textBoxBreakProCI.Leave += new System.EventHandler(this.textBoxBreakBreakProCI_Leave);
            // 
            // buttonLoad
            // 
            this.buttonLoad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonLoad.Location = new System.Drawing.Point(453, 454);
            this.buttonLoad.Name = "buttonLoad";
            this.buttonLoad.Size = new System.Drawing.Size(75, 23);
            this.buttonLoad.TabIndex = 100;
            this.buttonLoad.Text = "&Load";
            this.buttonLoad.UseVisualStyleBackColor = true;
            this.buttonLoad.Click += new System.EventHandler(this.buttonLoad_Click);
            // 
            // textBoxConfigFile
            // 
            this.textBoxConfigFile.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textBoxConfigFile.ErrorProvider = null;
            this.textBoxConfigFile.Location = new System.Drawing.Point(111, 420);
            this.textBoxConfigFile.Name = "textBoxConfigFile";
            this.textBoxConfigFile.ReadOnly = true;
            this.textBoxConfigFile.Required = false;
            this.textBoxConfigFile.Size = new System.Drawing.Size(579, 20);
            this.textBoxConfigFile.TabIndex = 100;
            this.textBoxConfigFile.TabStop = false;
            this.textBoxConfigFile.ToolTipProvider = null;
            this.textBoxConfigFile.ValidatingRegex = null;
            this.textBoxConfigFile.ValidatingRegexMessage = null;
            // 
            // ConfigurationDialog
            // 
            this.ClientSize = new System.Drawing.Size(711, 489);
            this.Controls.Add(this.buttonLoad);
            this.Controls.Add(this.tabControlConfiguration);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxConfigFile);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.buttonSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConfigurationDialog";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Configuration";
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.ConfigurationDialog_HelpButtonClicked);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ConfigurationDialog_FormClosing);
            this.Load += new System.EventHandler(this.ConfigurationDialog_Load);
            this.tabControlConfiguration.ResumeLayout(false);
            this.tabPageOptions.ResumeLayout(false);
            this.tabPageOptions.PerformLayout();
            this.groupBoxConvergence.ResumeLayout(false);
            this.groupBoxConvergence.PerformLayout();
            this.tabPageNodalDemand.ResumeLayout(false);
            this.groupBoxConfidence.ResumeLayout(false);
            this.groupBoxConfidence.PerformLayout();
            this.groupBoxMean.ResumeLayout(false);
            this.groupBoxMean.PerformLayout();
            this.tabPagePipeLeakage.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBoxLeakOther.ResumeLayout(false);
            this.groupBoxLeakOther.PerformLayout();
            this.groupBoxLeakSTL.ResumeLayout(false);
            this.groupBoxLeakSTL.PerformLayout();
            this.groupBoxLeakRS.ResumeLayout(false);
            this.groupBoxLeakRS.PerformLayout();
            this.groupBoxLeakDI.ResumeLayout(false);
            this.groupBoxLeakDI.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBoxLeakCI.ResumeLayout(false);
            this.groupBoxLeakCI.PerformLayout();
            this.tabPagePipeDamage.ResumeLayout(false);
            this.groupBoxLeakDamage.ResumeLayout(false);
            this.groupBoxLeakDamage.PerformLayout();
            this.groupBoxBreakDamage.ResumeLayout(false);
            this.groupBoxBreakDamage.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonClose;
        private GiraffeUI.ValidatingTextBox textBoxConfigFile;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabControlConfiguration;
        private System.Windows.Forms.TabPage tabPageOptions;
        private System.Windows.Forms.TabPage tabPageNodalDemand;
        private System.Windows.Forms.TabPage tabPagePipeLeakage;
        private System.Windows.Forms.TabPage tabPagePipeDamage;
        private System.Windows.Forms.GroupBox groupBoxConvergence;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private GiraffeUI.ValidatingTextBox textBoxCOVFlexible;
        private GiraffeUI.ValidatingTextBox textBoxMeanFlexible;
        private System.Windows.Forms.Label label4;
        private GiraffeUI.ValidatingTextBox textBoxMaxNSimu;
        private System.Windows.Forms.GroupBox groupBox5;
        private GiraffeUI.ValidatingTextBox textBoxType5wD;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private GiraffeUI.ValidatingTextBox textBoxType5kD;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label29;
        private GiraffeUI.ValidatingTextBox textBoxType4kD;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label32;
        private GiraffeUI.ValidatingTextBox textBoxType3aD;
        private System.Windows.Forms.Label label28;
        private GiraffeUI.ValidatingTextBox textBoxType3kD;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label33;
        private GiraffeUI.ValidatingTextBox textBoxType2aD;
        private System.Windows.Forms.GroupBox groupBoxLeakOther;
        private GiraffeUI.ValidatingTextBox textBoxOtherType3D;
        private GiraffeUI.ValidatingTextBox textBoxOtherType2D;
        private GiraffeUI.ValidatingTextBox textBoxOtherType4D;
        private GiraffeUI.ValidatingTextBox textBoxOtherType5D;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private GiraffeUI.ValidatingTextBox textBoxOtherType1D;
        private System.Windows.Forms.GroupBox groupBoxLeakSTL;
        private GiraffeUI.ValidatingTextBox textBoxSTLType3D;
        private GiraffeUI.ValidatingTextBox textBoxSTLType2D;
        private GiraffeUI.ValidatingTextBox textBoxSTLType4D;
        private GiraffeUI.ValidatingTextBox textBoxSTLType5D;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private GiraffeUI.ValidatingTextBox textBoxSTLType1D;
        private System.Windows.Forms.GroupBox groupBoxLeakRS;
        private GiraffeUI.ValidatingTextBox textBoxRSType3D;
        private GiraffeUI.ValidatingTextBox textBoxRSType2D;
        private GiraffeUI.ValidatingTextBox textBoxRSType4D;
        private GiraffeUI.ValidatingTextBox textBoxRSType5D;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private GiraffeUI.ValidatingTextBox textBoxRSType1D;
        private System.Windows.Forms.GroupBox groupBoxLeakDI;
        private GiraffeUI.ValidatingTextBox textBoxDIType3D;
        private GiraffeUI.ValidatingTextBox textBoxDIType2D;
        private GiraffeUI.ValidatingTextBox textBoxDIType4D;
        private GiraffeUI.ValidatingTextBox textBoxDIType5D;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private GiraffeUI.ValidatingTextBox textBoxDIType1D;
        private System.Windows.Forms.GroupBox groupBox4;
        private GiraffeUI.ValidatingTextBox textBoxType1kD;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private GiraffeUI.ValidatingTextBox textBoxType1tD;
        private System.Windows.Forms.GroupBox groupBoxLeakCI;
        private GiraffeUI.ValidatingTextBox textBoxCIType3D;
        private GiraffeUI.ValidatingTextBox textBoxCIType2D;
        private GiraffeUI.ValidatingTextBox textBoxCIType4D;
        private GiraffeUI.ValidatingTextBox textBoxCIType5D;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private GiraffeUI.ValidatingTextBox textBoxCIType1D;
        private System.Windows.Forms.GroupBox groupBoxLeakDamage;
        private GiraffeUI.ValidatingTextBox textBoxRSLeakRatio;
        private GiraffeUI.ValidatingTextBox textBoxDILeakRatio;
        private GiraffeUI.ValidatingTextBox textBoxSTLLeakRatio;
        private GiraffeUI.ValidatingTextBox textBoxCONLeakRatio;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private GiraffeUI.ValidatingTextBox textBoxCILeakRatio;
        private System.Windows.Forms.GroupBox groupBoxBreakDamage;
        private GiraffeUI.ValidatingTextBox textBoxBreakProRS;
        private GiraffeUI.ValidatingTextBox textBoxBreakProDI;
        private GiraffeUI.ValidatingTextBox textBoxBreakProSTL;
        private GiraffeUI.ValidatingTextBox textBoxBreakProCON;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private GiraffeUI.ValidatingTextBox textBoxBreakProCI;
        private System.Windows.Forms.GroupBox groupBoxConfidence;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private GiraffeUI.ValidatingTextBox textBoxmRRCap;
        private GiraffeUI.ValidatingTextBox textBoxUssMP;
        private GiraffeUI.ValidatingTextBox textBoxUsiMP;
        private GiraffeUI.ValidatingTextBox textBoxUisMP;
        private GiraffeUI.ValidatingTextBox textBoxUiiMP;
        private System.Windows.Forms.GroupBox groupBoxMean;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private GiraffeUI.ValidatingTextBox textBoxMssSD;
        private GiraffeUI.ValidatingTextBox textBoxMsiSD;
        private GiraffeUI.ValidatingTextBox textBoxMisSD;
        private GiraffeUI.ValidatingTextBox textBoxMiiSD;
        private GiraffeUI.ValidatingTextBox textBoxMssMP;
        private GiraffeUI.ValidatingTextBox textBoxMsiMP;
        private GiraffeUI.ValidatingTextBox textBoxMisMP;
        private GiraffeUI.ValidatingTextBox textBoxMiiMP;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button buttonLoad;
    }
}