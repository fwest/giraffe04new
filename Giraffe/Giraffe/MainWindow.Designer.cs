﻿namespace Giraffe
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.simulationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deterministicToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.monteCarloFixedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.monteCarloFlexibleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generatePipeDamageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generatePipeRepairRateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.helpRelNotesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpAppEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpAppDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpAppCToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpAppBToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpAppAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.helpManualToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.simulationsToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(800, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // simulationsToolStripMenuItem
            // 
            this.simulationsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deterministicToolStripMenuItem,
            this.monteCarloFixedToolStripMenuItem,
            this.monteCarloFlexibleToolStripMenuItem});
            this.simulationsToolStripMenuItem.Name = "simulationsToolStripMenuItem";
            this.simulationsToolStripMenuItem.Size = new System.Drawing.Size(81, 20);
            this.simulationsToolStripMenuItem.Text = "&Simulations";
            // 
            // deterministicToolStripMenuItem
            // 
            this.deterministicToolStripMenuItem.Name = "deterministicToolStripMenuItem";
            this.deterministicToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.deterministicToolStripMenuItem.Text = "&Deterministic...";
            this.deterministicToolStripMenuItem.Click += new System.EventHandler(this.deterministicToolStripMenuItem_Click);
            // 
            // monteCarloFixedToolStripMenuItem
            // 
            this.monteCarloFixedToolStripMenuItem.Name = "monteCarloFixedToolStripMenuItem";
            this.monteCarloFixedToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.monteCarloFixedToolStripMenuItem.Text = "Monte Carlo Fixed...";
            this.monteCarloFixedToolStripMenuItem.Click += new System.EventHandler(this.monteCarloFixedToolStripMenuItem_Click);
            // 
            // monteCarloFlexibleToolStripMenuItem
            // 
            this.monteCarloFlexibleToolStripMenuItem.Name = "monteCarloFlexibleToolStripMenuItem";
            this.monteCarloFlexibleToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.monteCarloFlexibleToolStripMenuItem.Text = "Monte Carlo Flexible...";
            this.monteCarloFlexibleToolStripMenuItem.Click += new System.EventHandler(this.monteCarloFlexibleToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configurationToolStripMenuItem,
            this.generatePipeDamageToolStripMenuItem,
            this.generatePipeRepairRateToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.toolsToolStripMenuItem.Text = "&Tools";
            // 
            // configurationToolStripMenuItem
            // 
            this.configurationToolStripMenuItem.Name = "configurationToolStripMenuItem";
            this.configurationToolStripMenuItem.Size = new System.Drawing.Size(347, 22);
            this.configurationToolStripMenuItem.Text = "Configuration...";
            this.configurationToolStripMenuItem.Click += new System.EventHandler(this.optionsToolStripMenuItem_Click);
            // 
            // generatePipeDamageToolStripMenuItem
            // 
            this.generatePipeDamageToolStripMenuItem.Name = "generatePipeDamageToolStripMenuItem";
            this.generatePipeDamageToolStripMenuItem.Size = new System.Drawing.Size(347, 22);
            this.generatePipeDamageToolStripMenuItem.Text = "Generate Pipe Damage File...";
            this.generatePipeDamageToolStripMenuItem.Click += new System.EventHandler(this.generatePipeDamageToolStripMenuItem_Click);
            // 
            // generatePipeRepairRateToolStripMenuItem
            // 
            this.generatePipeRepairRateToolStripMenuItem.Name = "generatePipeRepairRateToolStripMenuItem";
            this.generatePipeRepairRateToolStripMenuItem.Size = new System.Drawing.Size(347, 22);
            this.generatePipeRepairRateToolStripMenuItem.Text = "Generate Pipe Repair Rate and Mean Pressure Files...";
            this.generatePipeRepairRateToolStripMenuItem.Click += new System.EventHandler(this.generatePipeRepairRateToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpManualToolStripMenuItem,
            this.helpAppAToolStripMenuItem,
            this.helpAppBToolStripMenuItem,
            this.helpAppCToolStripMenuItem,
            this.helpAppDToolStripMenuItem,
            this.helpAppEToolStripMenuItem,
            this.toolStripSeparator2,
            this.helpRelNotesToolStripMenuItem,
            this.toolStripSeparator1,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.aboutToolStripMenuItem.Text = "&About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(153, 6);
            // 
            // helpRelNotesToolStripMenuItem
            // 
            this.helpRelNotesToolStripMenuItem.Name = "helpRelNotesToolStripMenuItem";
            this.helpRelNotesToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.helpRelNotesToolStripMenuItem.Text = "Release Notes...";
            this.helpRelNotesToolStripMenuItem.Click += new System.EventHandler(this.helpRelNotesToolStripMenuItem_Click);
            // 
            // helpAppEToolStripMenuItem
            // 
            this.helpAppEToolStripMenuItem.Name = "helpAppEToolStripMenuItem";
            this.helpAppEToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.helpAppEToolStripMenuItem.Text = "Appendix E...";
            this.helpAppEToolStripMenuItem.Click += new System.EventHandler(this.helpAppEToolStripMenuItem_Click);
            // 
            // helpAppDToolStripMenuItem
            // 
            this.helpAppDToolStripMenuItem.Name = "helpAppDToolStripMenuItem";
            this.helpAppDToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.helpAppDToolStripMenuItem.Text = "Appendix D...";
            this.helpAppDToolStripMenuItem.Click += new System.EventHandler(this.helpAppDToolStripMenuItem_Click);
            // 
            // helpAppCToolStripMenuItem
            // 
            this.helpAppCToolStripMenuItem.Name = "helpAppCToolStripMenuItem";
            this.helpAppCToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.helpAppCToolStripMenuItem.Text = "Appendix C...";
            this.helpAppCToolStripMenuItem.Click += new System.EventHandler(this.helpAppCToolStripMenuItem_Click);
            // 
            // helpAppBToolStripMenuItem
            // 
            this.helpAppBToolStripMenuItem.Name = "helpAppBToolStripMenuItem";
            this.helpAppBToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.helpAppBToolStripMenuItem.Text = "Appendix B...";
            this.helpAppBToolStripMenuItem.Click += new System.EventHandler(this.helpAppBToolStripMenuItem_Click);
            // 
            // helpAppAToolStripMenuItem
            // 
            this.helpAppAToolStripMenuItem.Name = "helpAppAToolStripMenuItem";
            this.helpAppAToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.helpAppAToolStripMenuItem.Text = "Appendix A...";
            this.helpAppAToolStripMenuItem.Click += new System.EventHandler(this.helpAppAToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(153, 6);
            // 
            // helpManualToolStripMenuItem
            // 
            this.helpManualToolStripMenuItem.Name = "helpManualToolStripMenuItem";
            this.helpManualToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.helpManualToolStripMenuItem.Text = "User Manual...";
            this.helpManualToolStripMenuItem.Click += new System.EventHandler(this.helpManualToolStripMenuItem_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Giraffe.Properties.Resources.giraffeLogo2014;
            this.ClientSize = new System.Drawing.Size(800, 533);
            this.Controls.Add(this.menuStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.MaximizeBox = false;
            this.Name = "MainWindow";
            this.Text = "Giraffe 6.0";
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem simulationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deterministicToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem monteCarloFixedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem monteCarloFlexibleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem generatePipeDamageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem generatePipeRepairRateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpManualToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpAppAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpAppBToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpAppCToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpAppDToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpAppEToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem helpRelNotesToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
    }
}

