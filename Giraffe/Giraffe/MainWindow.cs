﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Windows.Forms;

namespace Giraffe
{
    public partial class MainWindow : Form
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        #region Menu click handlers

        // File->Exit menu
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Close the main window
            Close();
        }

        // Simulations->Deterministic menu
        private void deterministicToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SimulationDialogDeterministic dlg = new SimulationDialogDeterministic();
            dlg.ShowDialog();
        }

        // Simulations->Monte Carlo Fixed menu
        private void monteCarloFixedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SimulationDialogMonteCarloFixed dlg = new SimulationDialogMonteCarloFixed();
            dlg.ShowDialog();
        }

        // Simulations->Monte Carlo Flexible menu
        private void monteCarloFlexibleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SimulationDialogMonteCarloFlexible dlg = new SimulationDialogMonteCarloFlexible();
            dlg.ShowDialog();
        }

        // Tools->Configuration menu
        private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConfigurationDialog dlg = new ConfigurationDialog();
            dlg.ShowDialog();
        }

        // Help->About menu
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutDialog dlg = new AboutDialog();
            dlg.ShowDialog();
        }

        // Tools->Generate Pipe Damage menu
        private void generatePipeDamageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Generate.GeneratePipeDamageFile(this);
        }

        // Tools->Generate Pipe Repair Rate menu
        private void generatePipeRepairRateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Generate.GeneratePipeRepairRateFile(this);
        }

        // Help->User Manual menu
        private void helpManualToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string filename = ConfigurationManager.AppSettings["UserManualPath"];
            OpenHelpFile(filename);
        }

        // Help->Appendix A menu
        private void helpAppAToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string filename = ConfigurationManager.AppSettings["AppendixAPath"];
            OpenHelpFile(filename);
        }

        // Help->Appendix B menu
        private void helpAppBToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string filename = ConfigurationManager.AppSettings["AppendixBPath"];
            OpenHelpFile(filename);
        }

        // Help->Appendix C menu
        private void helpAppCToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string filename = ConfigurationManager.AppSettings["AppendixCPath"];
            OpenHelpFile(filename);
        }

        // Help->Appendix D menu
        private void helpAppDToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string filename = ConfigurationManager.AppSettings["AppendixDPath"];
            OpenHelpFile(filename);
        }

        // Help->Appendix E menu
        private void helpAppEToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string filename = ConfigurationManager.AppSettings["AppendixEPath"];
            OpenHelpFile(filename);
        }

        // Help->Release Notes menu
        private void helpRelNotesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string filename = ConfigurationManager.AppSettings["ReleaseNotesPath"];
            OpenHelpFile(filename);           
        }

        #endregion

        #region Methods

        /// <summary>
        /// Open a help file using default process for the document type.
        /// </summary>
        /// <param name="filename">Full path of the file to be opened.</param>
        private void OpenHelpFile(string filename)
        {
            try
            {
                Process process = new Process();
                process.StartInfo.FileName = filename;
                process.StartInfo.Verb = "open";
                process.StartInfo.CreateNoWindow = false;
                process.Start();
            }
            catch (Exception)
            {
                MessageBoxEx.Show(this, "Help file not found.", "File Not Found", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        #endregion
    }
}