﻿namespace Giraffe
{
    partial class SimulationDialogMonteCarlo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxPipeRepairRateFile = new GiraffeUI.ValidatingTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.buttonBrowsePipeRepair = new System.Windows.Forms.Button();
            this.textBoxRandomSeed = new GiraffeUI.ValidatingTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.comboBoxRegressionEquation = new System.Windows.Forms.ComboBox();
            this.textBoxMeanPressureFile = new GiraffeUI.ValidatingTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.buttonBrowseMeanPressure = new System.Windows.Forms.Button();
            this.checkBoxNodalDemandCalibration = new System.Windows.Forms.CheckBox();
            this.buttonEditPipeRepair = new System.Windows.Forms.Button();
            this.buttonEditMeanPressure = new System.Windows.Forms.Button();
            this.buttonGeneratePipeRepairRate = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this._errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // checkBoxTimestamp
            // 
            this.checkBoxTimestamp.Checked = false;
            this.checkBoxTimestamp.CheckState = System.Windows.Forms.CheckState.Unchecked;
            // 
            // textBoxPipeRepairRateFile
            // 
            this.textBoxPipeRepairRateFile.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxPipeRepairRateFile.ErrorProvider = null;
            this.textBoxPipeRepairRateFile.Location = new System.Drawing.Point(136, 264);
            this.textBoxPipeRepairRateFile.Name = "textBoxPipeRepairRateFile";
            this.textBoxPipeRepairRateFile.ReadOnly = true;
            this.textBoxPipeRepairRateFile.Required = true;
            this.textBoxPipeRepairRateFile.Size = new System.Drawing.Size(436, 20);
            this.textBoxPipeRepairRateFile.TabIndex = 29;
            this.textBoxPipeRepairRateFile.TabStop = false;
            this.textBoxPipeRepairRateFile.ToolTipProvider = null;
            this.textBoxPipeRepairRateFile.ValidatingRegex = null;
            this.textBoxPipeRepairRateFile.ValidatingRegexMessage = null;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 269);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(107, 13);
            this.label5.TabIndex = 30;
            this.label5.Text = "Pipe Repair Rate File";
            // 
            // buttonBrowsePipeRepair
            // 
            this.buttonBrowsePipeRepair.Location = new System.Drawing.Point(578, 264);
            this.buttonBrowsePipeRepair.Name = "buttonBrowsePipeRepair";
            this.buttonBrowsePipeRepair.Size = new System.Drawing.Size(75, 23);
            this.buttonBrowsePipeRepair.TabIndex = 11;
            this.buttonBrowsePipeRepair.Text = "Browse...";
            this.buttonBrowsePipeRepair.UseVisualStyleBackColor = true;
            this.buttonBrowsePipeRepair.Click += new System.EventHandler(this.buttonBrowsePipeRepair_Click);
            // 
            // textBoxRandomSeed
            // 
            this.textBoxRandomSeed.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxRandomSeed.ErrorProvider = null;
            this.textBoxRandomSeed.Location = new System.Drawing.Point(442, 138);
            this.textBoxRandomSeed.Name = "textBoxRandomSeed";
            this.textBoxRandomSeed.ReadOnly = false;
            this.textBoxRandomSeed.Required = true;
            this.textBoxRandomSeed.Size = new System.Drawing.Size(201, 20);
            this.textBoxRandomSeed.TabIndex = 8;
            this.textBoxRandomSeed.ToolTipProvider = null;
            this.textBoxRandomSeed.ValidatingRegex = null;
            this.textBoxRandomSeed.ValidatingRegexMessage = null;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(331, 308);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(105, 13);
            this.label6.TabIndex = 34;
            this.label6.Text = "Regression Equation";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(361, 143);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(75, 13);
            this.label7.TabIndex = 35;
            this.label7.Text = "Random Seed";
            // 
            // comboBoxRegressionEquation
            // 
            this.comboBoxRegressionEquation.Enabled = false;
            this.comboBoxRegressionEquation.FormattingEnabled = true;
            this.comboBoxRegressionEquation.Items.AddRange(new object[] {
            "Mean Regression",
            "90% Confidence Interval"});
            this.comboBoxRegressionEquation.Location = new System.Drawing.Point(442, 303);
            this.comboBoxRegressionEquation.Name = "comboBoxRegressionEquation";
            this.comboBoxRegressionEquation.Size = new System.Drawing.Size(201, 21);
            this.comboBoxRegressionEquation.TabIndex = 14;
            // 
            // textBoxMeanPressureFile
            // 
            this.textBoxMeanPressureFile.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxMeanPressureFile.ErrorProvider = null;
            this.textBoxMeanPressureFile.Location = new System.Drawing.Point(136, 340);
            this.textBoxMeanPressureFile.Name = "textBoxMeanPressureFile";
            this.textBoxMeanPressureFile.ReadOnly = true;
            this.textBoxMeanPressureFile.Required = true;
            this.textBoxMeanPressureFile.Size = new System.Drawing.Size(436, 20);
            this.textBoxMeanPressureFile.TabIndex = 37;
            this.textBoxMeanPressureFile.TabStop = false;
            this.textBoxMeanPressureFile.ToolTipProvider = null;
            this.textBoxMeanPressureFile.ValidatingRegex = null;
            this.textBoxMeanPressureFile.ValidatingRegexMessage = null;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(35, 345);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(97, 13);
            this.label10.TabIndex = 38;
            this.label10.Text = "Mean Pressure File";
            // 
            // buttonBrowseMeanPressure
            // 
            this.buttonBrowseMeanPressure.Enabled = false;
            this.buttonBrowseMeanPressure.Location = new System.Drawing.Point(578, 340);
            this.buttonBrowseMeanPressure.Name = "buttonBrowseMeanPressure";
            this.buttonBrowseMeanPressure.Size = new System.Drawing.Size(75, 23);
            this.buttonBrowseMeanPressure.TabIndex = 15;
            this.buttonBrowseMeanPressure.Text = "Browse...";
            this.buttonBrowseMeanPressure.UseVisualStyleBackColor = true;
            this.buttonBrowseMeanPressure.Click += new System.EventHandler(this.buttonBrowseMeanPressure_Click);
            // 
            // checkBoxNodalDemandCalibration
            // 
            this.checkBoxNodalDemandCalibration.AutoSize = true;
            this.checkBoxNodalDemandCalibration.Location = new System.Drawing.Point(136, 307);
            this.checkBoxNodalDemandCalibration.Name = "checkBoxNodalDemandCalibration";
            this.checkBoxNodalDemandCalibration.Size = new System.Drawing.Size(155, 17);
            this.checkBoxNodalDemandCalibration.TabIndex = 13;
            this.checkBoxNodalDemandCalibration.Text = "Nodal Demand Calibration?";
            this.checkBoxNodalDemandCalibration.UseVisualStyleBackColor = true;
            this.checkBoxNodalDemandCalibration.CheckedChanged += new System.EventHandler(this.checkBoxNodalDemandCalibration_CheckedChanged);
            // 
            // buttonEditPipeRepair
            // 
            this.buttonEditPipeRepair.Location = new System.Drawing.Point(659, 264);
            this.buttonEditPipeRepair.Name = "buttonEditPipeRepair";
            this.buttonEditPipeRepair.Size = new System.Drawing.Size(75, 23);
            this.buttonEditPipeRepair.TabIndex = 12;
            this.buttonEditPipeRepair.Text = "Edit...";
            this.buttonEditPipeRepair.UseVisualStyleBackColor = true;
            this.buttonEditPipeRepair.Click += new System.EventHandler(this.buttonEditPipeRepair_Click);
            // 
            // buttonEditMeanPressure
            // 
            this.buttonEditMeanPressure.Enabled = false;
            this.buttonEditMeanPressure.Location = new System.Drawing.Point(659, 340);
            this.buttonEditMeanPressure.Name = "buttonEditMeanPressure";
            this.buttonEditMeanPressure.Size = new System.Drawing.Size(75, 23);
            this.buttonEditMeanPressure.TabIndex = 16;
            this.buttonEditMeanPressure.Text = "Edit...";
            this.buttonEditMeanPressure.UseVisualStyleBackColor = true;
            this.buttonEditMeanPressure.Click += new System.EventHandler(this.buttonEditMeanPressure_Click);
            // 
            // buttonGeneratePipeRepairRate
            // 
            this.buttonGeneratePipeRepairRate.Location = new System.Drawing.Point(578, 207);
            this.buttonGeneratePipeRepairRate.Name = "buttonGeneratePipeRepairRate";
            this.buttonGeneratePipeRepairRate.Size = new System.Drawing.Size(156, 51);
            this.buttonGeneratePipeRepairRate.TabIndex = 10;
            this.buttonGeneratePipeRepairRate.Text = "Generate Pipe Repair Rate and Mean Pressure files using Manifold GIS";
            this.buttonGeneratePipeRepairRate.UseVisualStyleBackColor = true;
            this.buttonGeneratePipeRepairRate.Click += new System.EventHandler(this.buttonGeneratePipeRepairRate_Click);
            // 
            // SimulationDialogMonteCarlo
            // 
            this.ClientSize = new System.Drawing.Size(750, 445);
            this.Controls.Add(this.buttonGeneratePipeRepairRate);
            this.Controls.Add(this.buttonEditMeanPressure);
            this.Controls.Add(this.buttonEditPipeRepair);
            this.Controls.Add(this.checkBoxNodalDemandCalibration);
            this.Controls.Add(this.buttonBrowseMeanPressure);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.textBoxMeanPressureFile);
            this.Controls.Add(this.comboBoxRegressionEquation);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBoxRandomSeed);
            this.Controls.Add(this.buttonBrowsePipeRepair);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBoxPipeRepairRateFile);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "SimulationDialogMonteCarlo";
            this.Text = "SimulationDialogMonteCarlo";
            this.Controls.SetChildIndex(this.checkBoxStartStop, 0);
            this.Controls.SetChildIndex(this.checkBoxTimestamp, 0);
            this.Controls.SetChildIndex(this.textBoxSimFolder, 0);
            this.Controls.SetChildIndex(this.textBoxOutputFolder, 0);
            this.Controls.SetChildIndex(this.textBoxSysDefFile, 0);
            this.Controls.SetChildIndex(this.textBoxMinPressure, 0);
            this.Controls.SetChildIndex(this.textBoxSimTime, 0);
            this.Controls.SetChildIndex(this.textBoxSimStep, 0);
            this.Controls.SetChildIndex(this.textBoxPipeRepairRateFile, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.buttonBrowsePipeRepair, 0);
            this.Controls.SetChildIndex(this.textBoxRandomSeed, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.label7, 0);
            this.Controls.SetChildIndex(this.comboBoxRegressionEquation, 0);
            this.Controls.SetChildIndex(this.textBoxMeanPressureFile, 0);
            this.Controls.SetChildIndex(this.label10, 0);
            this.Controls.SetChildIndex(this.buttonBrowseMeanPressure, 0);
            this.Controls.SetChildIndex(this.checkBoxNodalDemandCalibration, 0);
            this.Controls.SetChildIndex(this.buttonEditPipeRepair, 0);
            this.Controls.SetChildIndex(this.buttonEditMeanPressure, 0);
            this.Controls.SetChildIndex(this.buttonGeneratePipeRepairRate, 0);
            ((System.ComponentModel.ISupportInitialize)(this._errorProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label10;
        public GiraffeUI.ValidatingTextBox textBoxPipeRepairRateFile;
        public GiraffeUI.ValidatingTextBox textBoxRandomSeed;
        public System.Windows.Forms.ComboBox comboBoxRegressionEquation;
        public GiraffeUI.ValidatingTextBox textBoxMeanPressureFile;
        public System.Windows.Forms.CheckBox checkBoxNodalDemandCalibration;
        private System.Windows.Forms.Button buttonEditPipeRepair;
        public System.Windows.Forms.Button buttonEditMeanPressure;
        public System.Windows.Forms.Button buttonBrowsePipeRepair;
        public System.Windows.Forms.Button buttonBrowseMeanPressure;
        private System.Windows.Forms.Button buttonGeneratePipeRepairRate;
    }
}
