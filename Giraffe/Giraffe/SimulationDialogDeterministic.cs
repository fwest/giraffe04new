﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using System.Configuration;
using Giraffe;

namespace Giraffe
{
    /// <summary>
    /// Deterministic Simulation dialog
    /// </summary>
    public partial class SimulationDialogDeterministic : SimulationDialog
    {
        #region Properties

        /// <summary>
        /// Configuration path to the default project folder
        /// </summary>
        protected override string ProjectFolderConfigSetting
        {
            get { return "DeterministicProjectFolder"; }
        }

        /// <summary>
        /// Configuration path to the default timestamp flag
        /// </summary>
        protected override string TimestampConfigSetting
        {
            get { return "DeterministicTimestamp"; }
        }

        /// <summary>
        /// Configuration path to the default output folder
        /// </summary>
        protected override string OutputFolderConfigSetting
        {
            get { return "DeterministicOutputFolder"; }
        }

        /// <summary>
        /// Configuration path to the system definition file
        /// </summary>
        protected override string SysDefFileConfigSetting
        {
            get { return "DeterministicSysDefFile"; }
        }

        /// <summary>
        /// Configuration path to the default minimum pressure to eliminate
        /// </summary>
        protected override string MinPressureConfigSetting
        {
            get { return "DeterministicMinPressure"; }
        }

        /// <summary>
        /// Configuration path to the default simulation time period
        /// </summary>
        protected override string SimTimeConfigSetting
        {
            get { return "DeterministicSimTime"; }
        }

        /// <summary>
        /// Configuration path to the default simulation step period
        /// </summary>
        protected override string SimStepConfigSetting
        {
            get { return "DeterministicSimStep"; }
        }

        /// <summary>
        /// Configuration path to the default pipe damage file
        /// </summary>
        private const string PipeDamageConfigSetting = "DeterministicPipeDamage";
        /// <summary>
        /// Gets/sets the default pipe damage file
        /// </summary>
        protected virtual string PipeDamageFileSavedPath
        {
            get
            {
                return Configuration.AppSettings.Settings[PipeDamageConfigSetting].Value;
            }
            set
            {
                Configuration.AppSettings.Settings[PipeDamageConfigSetting].Value = value;
            }
        }

        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        public SimulationDialogDeterministic()
        {
            InitializeComponent();
        }

        #region Control Event Handlers

        /// <summary>
        /// Handles the Browse button click for the Pipe Damage File textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBrowsePipeDamage_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = ".";
                openFileDialog.Filter = "Definition Input files (*.inp)|*.inp";
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    textBoxPipeDamageFile.Text = openFileDialog.FileName;
                }
            }
        }

        /// <summary>
        /// Handles the Edit button click event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonEditPipeDamage_Click(object sender, EventArgs e)
        {
            // If a pipe damage file is specified, edit it in a TextEditor dialog
            EditFile(textBoxPipeDamageFile.Text);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonGeneratePipeDamage_Click(object sender, EventArgs e)
        {
            Generate.GeneratePipeDamageFile(this);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Disables all controls during a simulation run.
        /// </summary>
        protected override void DisableAllControls()
        {
            base.DisableAllControls();
            textBoxPipeDamageFile.Enabled = false;
            buttonBrowsePipeDamage.Enabled = false;
            buttonEditPipeDamage.Enabled = false;
            buttonGeneratePipeDamage.Enabled = false;
        }

        /// <summary>
        /// Enables all controls after a simulation run.
        /// </summary>
        protected override void EnableAllControls()
        {
            textBoxPipeDamageFile.Enabled = true;
            buttonBrowsePipeDamage.Enabled = true;
            buttonEditPipeDamage.Enabled = true;
            buttonGeneratePipeDamage.Enabled = true;
            base.EnableAllControls();
        }

        /// <summary>
        /// Loads the persisted default input values
        /// </summary>
        protected override void LoadPersistedInputValues()
        {
            base.LoadPersistedInputValues();
            textBoxPipeDamageFile.Text = PipeDamageFileSavedPath;
        }

        /// <summary>
        /// Persists the current input values for re-use
        /// </summary>
        protected override void PersistInputValues()
        {
            PipeDamageFileSavedPath = textBoxPipeDamageFile.Text;
            base.PersistInputValues();
        }

        /// <summary>
        /// Override this method to call the simulation method
        /// </summary>
        protected override void StartSimulation()
        {
            base.StartSimulation();

            // Load parameter defines
            ParameterDefines parameterDefines = new ParameterDefines();
            parameterDefines.LoadConfiguration(ParameterDefinesFile);

            // Assign simulation parameters
            SimulationParameters simulationParameters = new SimulationParameters();
            simulationParameters.Category = SimulationType.Deterministic;
            // Get UI control settings
            simulationParameters.ProjectName = textBoxSimFolder.Text;
            simulationParameters.Timestamp = checkBoxTimestamp.Checked;
            simulationParameters.OutputPath = textBoxOutputFolder.Text;
            simulationParameters.SystemFile = textBoxSysDefFile.Text;
            simulationParameters.MinimumPressureToEliminate = Convert.ToInt16(textBoxMinPressure.Text);
            simulationParameters.NumberOfHours = Convert.ToInt16(textBoxSimTime.Text);
            simulationParameters.NumberOfStepsPerHour = Convert.ToInt16(textBoxSimStep.Text);
            simulationParameters.PipeDamageFile = textBoxPipeDamageFile.Text;
            // Get OTHER settings
            simulationParameters.Debug = false;
            simulationParameters.RandomSeed = 10;
            simulationParameters.NumberOfSimulations = 1;
            simulationParameters.Wsort = true;
            // Default the rest - don't care about these (use defaults in data class?)
            simulationParameters.MeanPressureFile = string.Empty;
            simulationParameters.PipeRepairRateFile = string.Empty;
            simulationParameters.CalibrateNodalDemand = false;
            simulationParameters.RegressionEquation = RegressionType.Mean; // N/A

            SimWorker.RunWorkerAsync(new GiraffeSimulation(parameterDefines, simulationParameters));
        }

        /// <summary>
        /// Shows context-sensitive help in the help file.
        /// </summary>
        protected override void ShowHelp()
        {
            base.ShowHelp();
            HelpDialog dlg = new HelpDialog("DETSIM");
            dlg.Show();
        }

        /// <summary>
        /// Assigns static tooltip text to the specified controls.
        /// </summary>
        protected override void AssignToolTips()
        {
            base.AssignToolTips();
            SetToolTip(buttonBrowsePipeDamage, TooltipText.SelectPipeDamageFile);
            SetToolTip(buttonEditPipeDamage, TooltipText.EditPipeDamageFile);
        }

        #endregion
    }
}