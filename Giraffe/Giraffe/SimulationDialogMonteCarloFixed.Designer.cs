﻿namespace Giraffe
{
    partial class SimulationDialogMonteCarloFixed
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxNumSimulations = new GiraffeUI.ValidatingTextBox();
            this.label11 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this._errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxNumSimulations
            // 
            this.textBoxNumSimulations.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.textBoxNumSimulations.ErrorProvider = null;
            this.textBoxNumSimulations.Location = new System.Drawing.Point(442, 180);
            this.textBoxNumSimulations.Name = "textBoxNumSimulations";
            this.textBoxNumSimulations.ReadOnly = false;
            this.textBoxNumSimulations.Required = true;
            this.textBoxNumSimulations.Size = new System.Drawing.Size(201, 20);
            this.textBoxNumSimulations.TabIndex = 9;
            this.textBoxNumSimulations.ToolTipProvider = null;
            this.textBoxNumSimulations.ValidatingRegex = null;
            this.textBoxNumSimulations.ValidatingRegexMessage = null;
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(367, 180);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(69, 32);
            this.label11.TabIndex = 42;
            this.label11.Text = "Number of Simulations";
            // 
            // SimulationDialogMonteCarloFixed
            // 
            this.ClientSize = new System.Drawing.Size(750, 445);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.textBoxNumSimulations);
            this.HelpButton = true;
            this.Name = "SimulationDialogMonteCarloFixed";
            this.Text = "Monte Carlo - Fixed";
            this.Controls.SetChildIndex(this.buttonBrowsePipeRepair, 0);
            this.Controls.SetChildIndex(this.buttonBrowseMeanPressure, 0);
            this.Controls.SetChildIndex(this.buttonEditMeanPressure, 0);
            this.Controls.SetChildIndex(this.checkBoxStartStop, 0);
            this.Controls.SetChildIndex(this.checkBoxTimestamp, 0);
            this.Controls.SetChildIndex(this.textBoxSimFolder, 0);
            this.Controls.SetChildIndex(this.textBoxOutputFolder, 0);
            this.Controls.SetChildIndex(this.textBoxSysDefFile, 0);
            this.Controls.SetChildIndex(this.textBoxMinPressure, 0);
            this.Controls.SetChildIndex(this.textBoxSimTime, 0);
            this.Controls.SetChildIndex(this.textBoxSimStep, 0);
            this.Controls.SetChildIndex(this.textBoxPipeRepairRateFile, 0);
            this.Controls.SetChildIndex(this.textBoxRandomSeed, 0);
            this.Controls.SetChildIndex(this.comboBoxRegressionEquation, 0);
            this.Controls.SetChildIndex(this.textBoxMeanPressureFile, 0);
            this.Controls.SetChildIndex(this.checkBoxNodalDemandCalibration, 0);
            this.Controls.SetChildIndex(this.textBoxNumSimulations, 0);
            this.Controls.SetChildIndex(this.label11, 0);
            ((System.ComponentModel.ISupportInitialize)(this._errorProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private GiraffeUI.ValidatingTextBox textBoxNumSimulations;
        private System.Windows.Forms.Label label11;
    }
}
