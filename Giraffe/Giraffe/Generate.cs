﻿using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace Giraffe
{
    /// <summary>
    /// Static class for re-usable calls to Manifold for generating pipe damage, repair rate, and mean pressure files.
    /// </summary>
    static class Generate
    {
        /// <summary>
        /// Prompts user for a .MAP file to generate a pipe damage file.
        /// </summary>
        /// <param name="parent">Form calling this method for centering the message box.</param>
        public static void GeneratePipeDamageFile(Form parent)
        {
            const string message =
                "Please select a map file to generate a pipe damage file using Manifold.\n\nWhen you are done, please browse to your new file and select it prior to starting the simulation.\n\nPlease refer to GIRAFFE Appendix A documentation for detailed instructions.";
            const string caption = "Generate Pipe Damage File?";
            GeneratePipeFile(parent, message, caption);
        }

        /// <summary>
        /// Prompts user for a .MAP file to generate pipe repair rate and mean pressure files.
        /// </summary>
        /// <param name="parent">Form calling this method for centering the message box.</param>
        public static void GeneratePipeRepairRateFile(Form parent)
        {
            const string message =
                "Please select a map file to generate a repair rate and mean pressure files using Manifold.\n\nWhen you are done, please browse to your new files and select them prior to starting the simulation.\n\nPlease refer to GIRAFFE Appendix C documentation for detailed instructions.";
            const string caption = "Generate Pipe Repair Rate and Mean Pressure Files?";
            GeneratePipeFile(parent, message, caption);
        }

        /// <summary>
        /// Generic method to prompt the user to specify a .MAP file for generate pipe files.
        /// </summary>
        /// <param name="parent">Form calling this method for centering the message box.</param>
        /// <param name="message">Message displayed to the user in the message box.</param>
        /// <param name="caption">Caption displayed in the title bar of the message box.</param>
        private static void GeneratePipeFile(Form parent, string message, string caption)
        {
            if (MessageBoxEx.Show(parent, message, caption, MessageBoxButtons.OKCancel) ==
                DialogResult.OK)
            {
                using (OpenFileDialog openFileDialog = new OpenFileDialog())
                {
                    openFileDialog.InitialDirectory = ".";
                    openFileDialog.Filter = "Manifold Map files (*.map)|*.map";
                    openFileDialog.RestoreDirectory = true;

                    if (openFileDialog.ShowDialog() == DialogResult.OK)
                    {
                        if (Path.GetExtension(openFileDialog.FileName) == ".map")
                        {
                            Process process = new Process();
                            process.StartInfo.FileName = openFileDialog.FileName;
                            process.StartInfo.Verb = "open";
                            process.StartInfo.CreateNoWindow = false;
                            process.Start();
                        }
                        else
                        {
                            MessageBoxEx.Show(parent, "The selected file is not a Manifold Map file.", "Invalid File Type",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
        }
    }
}