' *  2006, Cornell University
' *  This code is part of the GIRAFFE project.
' *  Manifold Toolbar Add-in

' *  Created By:  Carolin Lewis - October 2006
' *  Modified By:

'*********************************************************

' Purpose: Calculates soil-crrected PGV values and repair rate for  

'          pipes in the system.	 	  

' Assumptions: All tables should be formatted as described in

'         Appendix C of GIRAFFE User Manual.
 
' Inputs: epa_pipes.shp: shapefile containing system pipes (links).

'         Pgvsurf: surface containing interpolated PGV values.

'         Soil_Category_NAD83 Drawing.shp: Shapefile containing soil data for the area.

' Returns: RRInput.inp: Monte Carlo simulation input for

'	   pipe damage. Exported to C:/temp.

'*********************************************************

Sub Main
Set theDoc = Application.ActiveDocument
Set theCompSet =  theDoc.ComponentSet
Set theQry = theDoc.NewQuery("Pipes_Query")

' #######################CREATE SM_PGV_Pipes DATABASE##########################

'  Compute PGV for pipes by interpolating the PGV values from Pgvsurf
'  for each pipe segment.

theQry.text = 		" SELECT [pgv_pipes].PipeID,pgv_pipes.PGV,PipeLength,pgv_pipes.Material, " & _
			"  [Soil_Category_NAD83 Drawing].[VSCAT],0 RR " & _
			" INTO SM_PGV_Pipes FROM " & _
			"	(SELECT ([epa_pipes].id), [Epa_pipes].[ID 2] as PipeID, [Epa_pipes].[Length (I)] as pipelength, " & _
			"			[Epa_pipes].[MATERIAL],[Geom (I)], " & _
			"			Height(pgvsurf,[epa_pipes].[Geom (I)]) as PGV " & _
			"	FROM epa_pipes) as pgv_pipes, [Soil_Category_NAD83 Drawing] " & _
			"	WHERE touches(pgv_pipes.id,[Soil_Category_NAD83 Drawing].id) "
theQry.Run

' #######################CORRECT PGV VALUES IN SM_PGV_Pipes##########################
'   Correct the interpolated PGV values of the pipes
'   using the appropriate soil amplification factors. 

theQry.text = 	" UPDATE [SM_PGV_Pipes] " & _
		" SET PGV = " & _
		" CASE " & _
		" 	WHEN VSCAT = ""A"" THEN pgv*.8 " & _
		" 	WHEN VSCAT = ""B"" THEN pgv*1 " & _
		" 	WHEN VSCAT = ""C"" AND pgv < 14 THEN pgv * 1.7 " & _
		" 	WHEN VSCAT = ""C"" AND pgv BETWEEN 14 AND 23.67 THEN pgv * 1.6 " & _
		" 	WHEN VSCAT = ""C"" AND pgv BETWEEN 23.67 AND 33.13 THEN pgv * 1.5 " & _
		" 	WHEN VSCAT = ""C"" AND pgv BETWEEN 33.13 AND 42.5 THEN pgv * 1.4 " & _
		" 	WHEN VSCAT = ""C"" AND pgv > 42.5 THEN pgv * 1.3 " & _
		" 	WHEN VSCAT = ""D"" AND pgv < 14 THEN pgv * 2.4 " & _
		" 	WHEN VSCAT = ""D"" AND pgv BETWEEN 14 AND 23.67 THEN pgv * 2 " & _
		" 	WHEN VSCAT = ""D"" AND pgv BETWEEN 23.67 AND 33.13 THEN pgv * 1.8 " & _
		" 	WHEN VSCAT = ""D"" AND pgv BETWEEN 33.13 AND 42.5 THEN pgv * 1.6 " & _
		" 	WHEN VSCAT = ""D"" AND pgv > 42.5 THEN pgv * 1.6 " & _
		" 	WHEN VSCAT = ""E"" AND pgv < 14 THEN pgv * 3.5 " & _
		" 	WHEN VSCAT = ""E"" AND pgv BETWEEN 14 AND 23.67 THEN pgv * 3.2 " & _
		" 	WHEN VSCAT = ""E"" AND pgv BETWEEN 23.67 AND 33.13 THEN pgv * 2.8 " & _
		" 	WHEN VSCAT = ""E"" AND pgv BETWEEN 33.13 AND 42.5 THEN pgv * 2.4 " & _
		" 	WHEN VSCAT = ""E"" AND pgv > 42.5 THEN pgv * 2.4 " & _
		" 	WHEN VSCAT = ""AB"" THEN pgv * .9 " & _
		" 	WHEN VSCAT = ""BC"" THEN pgv * 1 " & _
		" 	WHEN VSCAT = ""CD"" AND pgv < 14 THEN pgv * 2.05 " & _
		" 	WHEN VSCAT = ""CD"" AND pgv BETWEEN 14 AND 23.67 THEN pgv * 1.8 " & _
		" 	WHEN VSCAT = ""CD"" AND pgv BETWEEN 23.67 AND 33.13 THEN pgv * 1.65 " & _
		" 	WHEN VSCAT = ""CD"" AND pgv BETWEEN 33.13 AND 42.5 THEN pgv * 1.5 " & _
		" 	WHEN VSCAT = ""CD"" AND pgv > 42.5 THEN pgv * 1.4 " & _
		" 	WHEN VSCAT = ""DE"" AND pgv < 14 THEN pgv * 2.95 " & _
		" 	WHEN VSCAT = ""DE"" AND pgv BETWEEN 14 AND 23.67 THEN pgv * 2.6 " & _
		" 	WHEN VSCAT = ""DE"" AND pgv BETWEEN 23.67 AND 33.13 THEN pgv * 2.3 " & _
		" 	WHEN VSCAT = ""DE"" AND pgv BETWEEN 33.13 AND 42.5 THEN pgv * 2 " & _
		" 	WHEN VSCAT = ""DE"" AND pgv > 42.5 THEN pgv * 1.95 " & _
		" 	END "
theQry.Run

' #######################UPDATE MATERIAL IN SM_PGV_PIPES##########################
'  Update the material type of each pipe segment according to the 5
'  material types used by LADWP.

theQry.text = 		" UPDATE SM_PGV_Pipes " & _
			" SET MATERIAL = " & _
			" CASE [SM_PGV_Pipes].[MATERIAL] " & _
			" 	WHEN ""COP"" then ""CON"" "  & _
			" 	WHEN """" THEN ""N/A"" " & _
			" 	WHEN ""AC"" then ""N/A"" "  & _
			" 	WHEN ""RIV"" THEN ""RS"" " & _
			" 	WHEN ""B&S WS"" THEN ""STL"" " & _
			" 	WHEN ""MANN"" THEN ""STL"" " & _
			" 	WHEN ""MATH"" THEN ""STL"" " & _
			" 	WHEN ""Molox Bell & Ball Joint"" then ""STL"" " & _
			" 	WHEN ""n/a"" THEN ""N/A"" " & _
			" 	WHEN ""N/A"" THEN ""N/A"" " & _
			" 	WHEN ""ST"" THEN ""STL"" " & _
			" 	WHEN ""STD"" THEN ""STL"" " & _
			" 	WHEN ""steel"" then ""STL"" " & _
			" 	WHEN ""Steel"" then ""STL"" " & _
			" 	WHEN ""STL GALV"" THEN ""STL"" " & _
			" 	WHEN ""VICT"" THEN ""STL"" " & _
			" 	WHEN ""WCJ"" THEN ""STL"" " & _
			" 	WHEN ""WRG"" THEN ""STL"" " & _
			" 	WHEN ""WS"" THEN ""STL"" " & _
			" 	WHEN ""WSJ"" THEN ""STL"" " & _
			" 	WHEN ""WWJ"" THEN ""STL"" " & _
			" ELSE [MATERIAL] " & _
			" END "
theQry.Run

' #######################CALCULATE RR IN SM_PGV_PIPES##########################
'  Calculate the repair rate for each pipe using the appropriate
'  RR vs. PGV regression formulas.

theQry.text = 		" UPDATE SM_PGV_Pipes " & _
			" SET RR = " & _
			" CASE [SM_PGV_Pipes].[material] " & _
			"	WHEN ""CI"" THEN exp(1.21 * log(PGV) - 6.81) " & _
			"	WHEN ""DI"" THEN exp(1.84 * log(PGV) - 9.4) " & _
			"	WHEN ""CON"" THEN exp(2.59 * log(PGV) - 12.11) " & _
			"	WHEN ""RS"" THEN exp(1.41 * log(PGV) - 8.19) " & _
			"	WHEN ""STL"" THEN exp(2.59 * log(PGV) - 14.16) " & _
			"	WHEN ""N/A"" " & _
			"		THEN ((Exp(1.21 * LOG(PGV) - 6.81)) + (Exp(1.84 * Log(PGV) - 9.4)) + " & _
			"		(Exp(2.59 * Log(PGV) - 12.11)) + (Exp(1.41 * Log(PGV) - 8.19)) + " & _
			"		(Exp(2.59 * Log(PGV) - 14.16)))/5 " & _
			"	END "
theQry.Run


' #######################PREPARE SM_PGV_PIPES FOR EXPORT##########################
'  Prepare the SM_PGV_Pipes file for export as a text file, which can be 
'  inputted into GIRAFFE.

theQry.text = 		" SELECT PipeID, [Length], RR, Material " & _
			" INTO RRInput FROM " & _
			" 	(SELECT [SM_PGV_Pipes].[pipeid] as PipeID,First([MATERIAL]) " & _
			" 		AS Material,Avg([RR]) as RR, Sum([pipelength])/1000 as [Length] " & _
			" 	From [SM_PGV_Pipes] " & _
			" 	Group by [pipeid]) "
theQry.Run

' REMOVE QUERIES FROM PROJECT
'   Clean up the project pane by removing the queries generated during the script.

theInd = theCompSet.ItemByName("Pipes_Query")
theCompSet.Remove(theInd)

' #######################EXPORT RESULTS FOR INPUT INTO GIRAFFE##########################

Set thePipeComp = theCompSet.Item("RRInput")
Set XLExp = theDoc.NewExport("csv")
XLExp.Export thePipeComp,"c:\temp\RRInput.inp",PromptNone 

End Sub