' *  2006, Cornell University
' *  This code is part of the GIRAFFE project.
' *  Manifold Toolbar Add-in

' *  Created By:  Carolin Lewis - October 2006
' *  Modified By:

'*********************************************************

' Purpose: Formats scenario data PGV values and interpolates

'	   PGV values onto a surface to be used in other tools.    
 
' Inputs: Pgvsurf: empty, user-generated surface.

'         Scenario shapefile: contains 572 grid points for 

'	  strong ground motion data.

' Returns: Pgvsurf: contains interpolated PGV values.

'*********************************************************

Sub Main
 Set theApp = Application
 Set theCompSet =  Application.ActiveDocument.ComponentSet
 Set theQry = Application.ActiveDocument.NewQuery("SnPrep_Query")

' ################PREPARE STRONG GROUND MOTION DATA FOR INTERPOLATION.#################
'  After user inputs scenario file name, the file is renamed for use in the queries.
'  This tool MUST be run before calculating the pipe and node repair rates. However, it only needs to be run once
'  per scenario (as long as "scenario_data" is not deleted and imported again).

theScName = theApp.InputBox("Enter Name of Scenario Shapefile:", "", "175_data Drawing")

Set theSN = theCompSet.Item(theScName)
theName = "Scenario_data"
theSN.Name = theName

theQry.text =  	        " UPDATE [Scenario_data] " & _
			" SET [PGV] = [PGV] * exp(.31) "
theQry.Run

' #######################RASTER INTERPOLATION##########################
'  Get the pixels from the raster surface, and then interpolate the PGV value for each pixel.
'
Set theSurfPS = Application.ActiveDocument.ComponentSet.Item("pgvsurf").PixelSet
theSurfPS.TransformWith("Kriging([Scenario_data],PGV,-1)")

' #######################REMOVE QUERIES FROM PROJECT##########################
'  Clean up the project pane by removing queries generated during the script.
'  Rename the scenario file to its orignal name.


theInd = theCompSet.ItemByName("SnPrep_Query")
theCompSet.Remove(theInd)
theSN.Name = theScName

End Sub