' *  2006, Cornell University
' *  This code is part of the GIRAFFE project.
' *  Manifold Toolbar Add-in

' *  Created By:  Carolin Lewis - January 2007
' *  Modified By:

'*********************************************************

' Purpose: Categorizes pipes by flow (flow, no flow before, no flow after, removed)

' 	   to allow cartographic display of pipe flow.

' Assumptions: All tables should be formatted as described in

'         Appendix C of GIRAFFE User Manual.

' Inputs: epa_pipes: shapefile containing system pipes (links).

'	  PipesNoFlow_BeforeEQ: shapefile containing pipes that have no

'	      flow before earthquake or pipes for which flow data is unavailable (assumed = 0).

'	  LinkResults_Time24.out: GIRAFFE output file describing

'	      pipe flow in the system 24 hours after earthquake damage.

' Returns: New column (Flow_Category_24) in epa_pipes describing pipe flow.

'*********************************************************

Sub Main
Set theApp = Application
Set theDoc = Application.ActiveDocument
Set theCompS = theDoc.ComponentSet
Set theQry = theDoc.NewQuery("Query")

' #######################FORMAT LINK RESULTS TABLE##########################

'  Groups duplicate listings of damage pipes (prefix "A__") and creates a new list.

theQry.text = "		ALTER TABLE [LinkResults_Time24] ALTER COLUMN [LinkID] TEXT(20) "
theQry.run

theQry.text = "		SELECT [Epa_pipes].* into [DmgPipeTable] from [Epa_pipes] " & _ 
		"	where [Epa_pipes].[ID 2] in " & _
		"		 (select DmgID from " & _ 
					" (select DmgID, count(*) from " & _
					"	(select replace([LinkID],left([LinkID],3),"""") DmgID  " & _
					"	from [LinkResults_Time24]  " & _
					"	where left([LinkID],1) = ""A"") " & _
					"	group by DmgID)) "
theQry.run

' #######################CATEGORIZE PIPES BY FLOW##########################

'  Add flow category column to epa_pipes.

Set PipeColS = theCompS.Item("epa_pipes").OwnedTable.ColumnSet

If PipeColS.ItemByName("Flow_Category_24") < 0 then
	Set Col = PipeColS.NewColumn
    Col.Name = "Flow_Category_24"
    Col.Type = ColumnTypeAText
    PipeColS.Add(Col)
End If

theQry.text = "		ALTER TABLE [epa_pipes] ALTER COLUMN [Flow_Category_24] TEXT(20) "
theQry.run

' Categorize pipes by pipe flow. Removed pipes are not listed in LinkResults (removed due to negative pressures) and are listed
'   in the damage tables. Pipes with flow have a flow <> 0 and have no leaks/breaks. Pipes with no flow after the EQ have a flow = 0
'   and have no leaks/breaks.

theQry.text = 		" UPDATE epa_pipes " & _
			" SET [Flow_Category_24] = " & _
			" CASE " & _
			" 	WHEN [ID 2] in (Select [LinkID] from [LinkResults_Time24] Where [Flow_gpm] = 0) THEN ""No Flow After"" " & _
			" 	WHEN [ID 2] in (SELECT [LinkID] FROM [LinkResults_Time24] WHERE [Flow_gpm] <> 0) THEN ""Flow"" " & _
			" 	WHEN [ID 2] not in (Select [LinkID] FROM [LinkResults_Time24]) THEN ""Removed"" " & _
			" 	END "
theQry.run

theQry.text = 		" UPDATE epa_pipes " & _
			" SET [Flow_Category_24] = " & _
			" CASE " & _
			" 	WHEN [ID 2] in (Select [ID 2] from [DmgPipeTable]) THEN ""Damaged"" " & _
			" 	WHEN [ID 2] in (SELECT [ID 2] from [PipesNoFlow_BeforeEQ]) THEN ""No Flow Before"" " & _
			" 	END "
theInd = theCompS.ItemByName("Query")
theQry.run
theQry.Clear()
theCompS.Remove(theInd)

' Remove extraneous tables.
DmgInd = theCompS.ItemByName("DmgPipeTable")
theCompS.Remove(DmgInd)

End Sub
