' *  2006, Cornell University
' *  This code is part of the GIRAFFE project.
' *  Manifold Toolbar Add-in

' *  Created By:  Carolin Lewis - October 2006
' *  Modified By:

'*********************************************************

' Purpose: Creates a new layer containing serviceability data

'	      for all nodes immediately after earthquake damage.    
 
' Inputs: epa_junctions.shp: contains junction data for entire system.

'         Serviceability0.out: GIRAFFE output file describing 

'	      nodal demand and serviceability at time 0.

' Returns: NodeServiceability_0.shp

'*********************************************************

Sub Main
Set theDoc = Application.ActiveDocument
Set theCompS = theDoc.ComponentSet
Set theQry = theDoc.NewQuery("Serviceability0_Query")

' #######################FORMAT SERVICEABILITY TABLE##########################

Set Serv = theCompS.Item("Serviceability0")
Set ServRec = Serv.RecordSet

Set theExtra = ServRec.EqualTo("Node_ID", "Sum")
	ServRec.Remove(theExtra)
Set theExtra = ServRec.EqualTo("Node_ID", "")
	ServRec.Remove(theExtra)


' Make sure that no other Serviceability at Time 0 layers exist.
for each item in theCompS
if item.Name = "NodeServiceability_0" then
	theClick = msgbox("NodeServiceability_0 layer already exists. Do you want to overwrite it?",4)
	if (theClick = 6) then
		theCompS.Remove("NodeServiceability_0")
	else 
		msgbox "Process terminated." & chr(13) & "Please rename existing NodeServiceability_24 layer, and then rerun the tool."
		Exit Sub
	end if
end if
Next	

' #######################CREATE SERVICEABILITY LAYER##########################

' Create Serviceability layer by copying a column from the output file and pasting
'  it into the layer file. Value reported is mean serviceability of nodes for all runs.
theQry.text = "		select [epa_junctions].*, [Serviceability0].* " & _
		"	from [epa_junctions], [Serviceability0] " & _
		"	where [id 2] = [Node_ID] "
theQry.run
theQry.Table.Open

Set NodeServ = theDoc.NewDrawing("NodeServiceability_0")
NodeServ.CoordinateSystem = theCompS.Item("epa_junctions").CoordinateSystem
theCompS.Item("epa_junctions").Copy(true)
NodeServ.paste()

theQry.text = "		ALTER TABLE [NodeServiceability_0] ADD COLUMN [Servicability0] double "
theQry.run

theQry.text = "		update [NodeServiceability_0] set [NodeServiceability_0].Servicability0 = " & _
		"		(SELECT [Serviceability0].[Node_Serviceability] " & _
		"		FROM [Serviceability0] " & _
		"		where [NodeServiceability_0].[ID 2] = [Serviceability0].[node_id]) "
theInd = theCompS.ItemByName("Serviceability0_Query")
theQry.run
theCompS.Item("NodeServiceability_0").Open()
theCompS.Remove(theInd)

End Sub