' *  2006, Cornell University
' *  This code is part of the GIRAFFE project.
' *  Manifold Toolbar Add-in

' *  Created By:  Carolin Lewis - October 2006
' *  Modified By:

'*********************************************************

' Purpose: Creates a new layer containing serviceability data

'	     for all nodes 24 hours after earthquake damage.    
 
' Inputs: epa_junctions.shp: contains junction data for entire system.

'         Serviceability24.out: GIRAFFE output file describing 

'	     nodal demand and serviceability at time 24.

' Returns: NodeServiceability_24.shp

'*********************************************************

Sub Main
Set theDoc = Application.ActiveDocument
Set theCompS = theDoc.ComponentSet
Set theQry = theDoc.NewQuery("Serviceability24_Query")

' #######################FORMAT SERVICEABILITY TABLE##########################

Set Serv = theCompS.Item("Serviceability24")
Set ServRec = Serv.RecordSet

Set theExtra = ServRec.EqualTo("Node_ID", "Sum")
	ServRec.Remove(theExtra)
Set theExtra = ServRec.EqualTo("Node_ID", "")
	ServRec.Remove(theExtra)


' Make sure that no other Serviceability at Time 0 layers exist. This ensures that all calculations are 
'  performed correctly.
for each item in theCompS
if item.Name = "NodeServiceability_24" then
	theClick = msgbox("NodeServiceability_24 layer already exists. Do you want to overwrite it?",4)
	if (theClick = 6) then
		theCompS.Remove("NodeServiceability_24")
	else 
	msgbox "Process terminated." & chr(13) & "Please rename existing NodeServiceability_24 layer, and then rerun the tool."
		theInd = theCompS.ItemByName("Serviceability24_Query")
		theCompS.Remove(theInd)		
		Exit Sub
	end if
end if
Next	

' #######################CREATE SERVICEABILITY LAYER##########################

' Create Serviceability layer by copying a column from the output file and pasting
'  it into the layer file. Value reported is mean serviceability of nodes for all runs.
theQry.text = "		select [epa_junctions].*, [Serviceability24].* " & _
		"	from [epa_junctions], [Serviceability24] " & _
		"	where [id 2] = [Node_ID] "
theQry.run
theQry.Table.Open

Set NodeServ = theDoc.NewDrawing("NodeServiceability_24")
NodeServ.CoordinateSystem = theCompS.Item("epa_junctions").CoordinateSystem
theCompS.Item("epa_junctions").Copy(true)
NodeServ.paste()

theQry.text = "		ALTER TABLE [NodeServiceability_24] ADD COLUMN [Servicability24] double "
theQry.run

theQry.text = "		update [NodeServiceability_24] set [NodeServiceability_24].Servicability24 = " & _
		"		(SELECT [Serviceability24].[Node_Serviceability] " & _
		"		FROM [Serviceability24] " & _
		"		where [NodeServiceability_24].[ID 2] = [Serviceability24].[node_id]) "
theInd = theCompS.ItemByName("Serviceability24_Query")
theQry.run
theCompS.Item("NodeServiceability_24").Open()
theCompS.Remove(theInd)

End Sub