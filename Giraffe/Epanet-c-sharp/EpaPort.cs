﻿using System;
using System.Text;
using Epanet;

namespace Giraffe
{
    /// <summary>
    /// this class is a class that mimics the EPAnet class used by Giraffe 08.
    /// Use it, don't use it -- whatever works best; singleton pattern
    /// </summary>
    public class EpaPort
    {
        /// <summary>
        /// hold start and end node information
        /// </summary>
        public class StartEnd
        {
            public int StartNodeIndex { get; set; }
            public int EndNodeIndex { get; set; }
        }

        /// <summary>
        /// the one and only instance
        /// </summary>
        private static EpaPort singleton;

        /// <summary>
        /// being static - singleton rules apply
        /// </summary>
        public bool Opened { get; set; }

        /// <summary>
        /// private c'tor
        /// </summary>
        private EpaPort()
        {

        }

        /// <summary>
        /// use in place of public c'tor
        /// </summary>
        /// <returns></returns>
        public static EpaPort GetEpaInstance()
        {
            if (singleton == null)
                singleton = new EpaPort();

            return singleton;
        }

        /// <summary>
        /// Open the EPA net DLL
        /// </summary>
        /// <param name="EpaInputFile">defines the system</param>
        /// <param name="outputReportfile">defines the simulation result</param>
        /// <param name="optionalBinaryFile">not sure</param>
        public bool Open(string EpaInputFile, string outputReportfile, string optionalBinaryFile)
        {
            bool returnValue = false;

            if (Opened)            
                UnsafeNativeMethods.ENclose();

            var ret = UnsafeNativeMethods.ENopen(EpaInputFile, outputReportfile, optionalBinaryFile);
            if ((ret == ErrorCode.Ok) || (ret == ErrorCode.Err200)) // 200 is a problem, keep eye on
            {
                Opened = true;
                returnValue = true;
            }
            else
            {
                var temp = "Epanet Open error " + ret.ToString().Trim() + " consult EPA net error codes.";
                //throw new SystemException(temp);               
            }

            return returnValue;
        }

        /// <summary>
        /// close the EPA net handle
        /// </summary>
        public void Close()
        {
            ErrorCode ret = ErrorCode.Ok;

            if (Opened)
                ret = UnsafeNativeMethods.ENclose();

            Opened = false;

            if (ret != ErrorCode.Ok)
            {
                var temp = "Epanet Close error " + ret.ToString().Trim() + " consult EPA net error codes.";
                throw new SystemException(temp);
            }
        }

        /// <summary>
        /// same as ENsolveH
        /// </summary>
        public void SolveHydraulic()
        {
            var ret = UnsafeNativeMethods.ENsolveH();

            // the original code does not check the value of this
            // return -- be aware
            
            if (ret != ErrorCode.Ok)
            {
                Console.WriteLine("EPA Net not OK calling ENsolveH " + ret.ToString().Trim());
                var temp = "EPA Net not OK calling ENsolveH " + ret.ToString().Trim();
                //throw new SystemException(temp);
            }
        }

        /// <summary>
        /// same as ENsaveH
        /// </summary>
        public ErrorCode SaveHydraulic()
        {
            var ret = UnsafeNativeMethods.ENsaveH();

            if (ret == ErrorCode.Err104)
            {
                var temp = "EPA Net error calling ENsaveH; no hydraulics for water quality analysis " + ret.ToString().Trim();                
            }
            else if (ret != ErrorCode.Ok)
            {
                var temp = "EPA Net error calling ENsaveH; " + ret.ToString().Trim();                
            }

            return ret;
        }

        /// <summary>
        /// same as ENreport
        /// </summary>
        public ErrorCode SaveReport()
        {           
            var ret = UnsafeNativeMethods.ENreport();

            // the original code does not check this return
            // be aware - console print
            if (ret != ErrorCode.Ok)
            {
                Console.WriteLine("EPA Net error calling ENreport " + ret.ToString().Trim());
                var temp = "EPA Net error calling ENreport " + ret.ToString().Trim();                
            }

            return ret;
        }

        /// <summary>
        /// returns the number of nodes in the EPA Net system
        /// </summary>
        public int NodeCount(CountType nodeType)
        {           
            int count = 0;

            UnsafeNativeMethods.ENgetcount(nodeType, out count);

            return count;            
        }

        /// <summary>
        /// return the id of the node identified by index
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public string GetNodeID(int index)
        {
            StringBuilder result = new StringBuilder();

            UnsafeNativeMethods.ENgetnodeid(index, result);

            return result.ToString();
        }

        /// <summary>
        /// equivalent to ENgetnodevalue
        /// </summary>
        /// <param name="index"></param>
        /// <param name="nodeValue"></param>
        /// <returns></returns>
        public float GetNodeValue(int index, NodeValue nodeValue)
        {
            float result;

            UnsafeNativeMethods.ENgetnodevalue(index, nodeValue, out result);

            return result;
        }

        /// <summary>
        /// equivalent to ENgetnodetype
        /// </summary>
        /// <param name="index"></param>
        /// <param name="nodeValue"></param>
        /// <returns></returns>
        public NodeType GetNodeType(int index)
        {
            NodeType result;

            UnsafeNativeMethods.ENgetnodetype(index, out result);

            return result;
        }

        /// <summary>
        /// get the start and end nodes of a link
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public StartEnd GetLinkNodes(int index)
        {
            int startNodeIndex;
            int endNodeIndex;

            ErrorCode err = UnsafeNativeMethods.ENgetlinknodes(index, out startNodeIndex, out endNodeIndex);

            if (err == ErrorCode.Ok)
            {
                return new StartEnd { EndNodeIndex = endNodeIndex, StartNodeIndex = startNodeIndex };
            }
            else
                throw new SystemException("Error getting link nodes; GetLinkNodes; err = " + err.ToString());
        }

        /// <summary>
        /// get a link's link type
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public LinkType GetLinkType(int index)
        {
            LinkType type;

            ErrorCode err = UnsafeNativeMethods.ENgetlinktype(index, out type);

            if (err == ErrorCode.Ok)
            {
                return type;
            }
            else
            {
                var temp = new StringBuilder();
                temp.AppendFormat("Error getting link type; GetLinkType; err = {0} index= {1}", err.ToString(), index);
                throw new SystemException(temp.ToString());
            }
        }

        /// <summary>
        /// get a link's id
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public string GetLinkID(int index)
        {
            StringBuilder builder = new StringBuilder();

            ErrorCode err = UnsafeNativeMethods.ENgetlinkid(index, builder);

            if (err == ErrorCode.Ok)
            {
                return builder.ToString();
            }
            else
            {
                var temp = new StringBuilder();
                temp.AppendFormat("Error getting link id; GetLinkID; err = {0} index= {1}", err.ToString(), index);
                throw new SystemException(temp.ToString());
            }
        }

        /// <summary>
        /// get a link's index
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public int GetLinkIndex(string id)
        {
            int returnResult;

            ErrorCode err = UnsafeNativeMethods.ENgetlinkindex(id, out returnResult);

            if (err == ErrorCode.Ok)
            {
                return returnResult;
            }
            else
            {
                var temp = new StringBuilder();
                temp.AppendFormat("Error getting link index; GetLinkIndex; err = {0} id = {1}", err.ToString(), id);
                throw new SystemException(temp.ToString());
            }
        }

        /// <summary>
        /// get a node's index
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public int GetNodeIndex(string id)
        {
            int returnResult;

            ErrorCode err = UnsafeNativeMethods.ENgetnodeindex(id, out returnResult);

            if (err == ErrorCode.Ok)
            {
                return returnResult;
            }
            else
            {
                var temp = new StringBuilder();
                temp.AppendFormat("Error getting node index; GetNodeIndex; err = {0} id = {1}", err.ToString(), id);
                throw new SystemException(temp.ToString());
            }
        }


        /// <summary>
        /// equivalent to ENgetlinkvalue
        /// </summary>
        /// <param name="index"></param>
        /// <param name="nodeValue"></param>
        /// <returns></returns>
        public float GetLinkValue(int index, LinkValue linkValue)
        {
            float result;

            UnsafeNativeMethods.ENgetlinkvalue(index, linkValue, out result);

            return result;
        }

        /// <summary>
        /// equivalent to ENgetlinkvalue, add params as needed
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public ErrorCode GetControl(int index, out int nindex)
        {
            ControlType ctype;
            int lindex;
            float setting;            
            float level;

            return UnsafeNativeMethods.ENgetcontrol(index, out ctype, out lindex, 
                                                    out setting, out nindex, out level);
        }
    }
}
