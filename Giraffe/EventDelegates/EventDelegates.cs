﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// contains the delegates for events coming out of the 
// simulation instance

namespace Giraffe
{
    public interface ISimulationEvents
    {
        #region events

        /// <summary>
        /// supports the events that signals an error has occurred - inspect the args
        /// for details, especially the system exception information
        /// </summary>
        /// <param name="args"></param>
        event ErrorEventDel ErrorEvent;

        /// <summary>
        /// supports reporting that we are repairing the input file to EPANet
        /// </summary>
        /// <param name="args"></param>
        event RepairingFileDel RepairingFile;

        /// <summary>
        /// The noted time step has completed
        /// </summary>
        /// <param name="args"></param>
        event ProgressDel Progress;

        /// <summary>
        /// the simulation has ended -- success or failure indicated
        /// </summary>
        /// <param name="successful"></param>
        /// <param name="args"></param>
        event SimulationCompleteDel SimulationComplete;

        /// <summary>
        /// negative pressures are being eliminated
        /// </summary>
        /// <param name="callCount">an indicator on the number of calls to EPANet so far</param>
        event RemovingNegativePressureDel RemovingNegativePressure;

        /// <summary>
        /// We are continuing on "normal" simulation looping
        /// </summary>
        event ResumingSimulationDel ResumingSimulation;

        /// <summary>
        /// Simulation has begun
        /// </summary>
        /// <param name="hours">the number of hours to simulate</param>
        /// <param name="steps">the number of steps per hour</param>
        event SimulationStartedDel SimulationStarted;

        #endregion
    }

    public class GiraffeEventArgs : EventArgs
    {
        public string Context { get; set; }
        public SystemException SysEx { get; set; }
    }

    /// <summary>
    /// supports the events that signals an error has occurred - inspect the args
    /// for details, especially the system exception information
    /// </summary>
    /// <param name="args"></param>
    public delegate void ErrorEventDel(GiraffeEventArgs args);

    /// <summary>
    /// supports reporting that we are repairing the input file to EPANet
    /// </summary>
    /// <param name="args"></param>
    public delegate void RepairingFileDel(GiraffeEventArgs args);

    /// <summary>
    /// A set number of loops has completed
    /// </summary>
    /// <param name="args"></param>
    public delegate void ProgressDel(int loops, int numberCompleted, int maximum);

    /// <summary>
    /// the simulation has ended -- success or failure indicated
    /// </summary>
    /// <param name="successful"></param>
    /// <param name="args"></param>
    public delegate void SimulationCompleteDel(SuccessCategories successful, GiraffeEventArgs args);

    /// <summary>
    /// negative pressures are being eliminated
    /// </summary>
    /// <param name="callCount">an indicator on the number of calls to EPANet so far</param>
    public delegate void RemovingNegativePressureDel(int callCount);

    /// <summary>
    /// We are continuing on "normal" simulation looping
    /// </summary>
    public delegate void ResumingSimulationDel(int loopCount);
  
    /// <summary>
    /// Simulation has begun
    /// </summary>
    /// <param name="hours">the number of hours to simulate</param>
    /// <param name="steps">the number of steps per hour</param>
    public delegate void SimulationStartedDel(int hours, int steps);
}
