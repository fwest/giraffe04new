﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Giraffe
{
    public static class Utilities
    {
        /// <summary>
        /// make sure all directory names are well formatted and have the 
        /// trailing back-slash
        /// </summary>
        public static string VerifyDirectory(string directory)
        {
            string temp = string.Empty;

            if (!string.IsNullOrEmpty(directory))
            {
                if (directory[directory.Length - 1] != '\\')
                {
                    directory = directory.Trim() + "\\";
                }
            }
            else
                temp = "OutputPath badly formatted " + directory;

            if (temp != string.Empty)
                throw new SystemException(temp);

            string result = directory;

            // if the directory does not exist -- create it
            try
            {
                if (!Directory.Exists(result))
                    Directory.CreateDirectory(result);
            }
            catch (SystemException sysex)
            {
                var err = "Exception attempting to create " + result + " " + sysex.Message.Trim();
                throw new SystemException(err);
            }

            return result;
        }
    }
}